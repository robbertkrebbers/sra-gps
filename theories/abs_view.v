From iris.base_logic.lib Require Import own invariants.
From iris.algebra Require Import agree.
From stdpp Require Import gmap.
From iris.proofmode Require Import tactics.

From igps Require Import viewpred proofmode.
From igps.base Require Import ghosts.


Class absViewG Σ := AbsViewG {
  absV_savedViewG :> inG Σ (agreeR View_ofe);
}.

Definition absViewΣ : gFunctors :=
  #[ GFunctor (agreeR View_ofe) ].

Instance subG_absViewΣ {Σ} : subG absViewΣ Σ → absViewG Σ.
Proof. solve_inG. Qed.

Section AbstractView.

  Context `{!foundationG Σ, !absViewG Σ}.

  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).

  Definition absView (β: gname) (V: View): iProp
    := own β (to_agree V).

  Global Instance absView_persistent β V :
    PersistentP (absView β V) := _.

  Global Instance absView_timeless β V :
    TimelessP (absView β V).
  Proof. apply _. Qed.

  Definition aSeen_aux β V : iProp := (∃ V0, ⌜V0 ⊑ V⌝ ∗ absView β V0)%I.

  Lemma aSeen_aux_mono β :
    vPred_Mono (aSeen_aux β).
  Proof.
    iIntros (V1 V2 Le) "aSeen".
    iDestruct "aSeen" as (V0) "(% & absV)".
    iExists _. iFrame "absV". iPureIntro. by etrans.
  Qed.

  Canonical Structure aSeen β
    := Build_vPred _ (aSeen_aux_mono β).

  Global Instance aSeen_persistent β V :
    PersistentP (aSeen β V) := _.

  Definition abs_pred_aux (P: vPred) β (V: View): iProp :=
    (∃ V0, absView β V0 ∗ P V0)%I.

  Lemma abs_pred_aux_mono P β: vPred_Mono (abs_pred_aux P β).
  Proof. done. Qed.

  Canonical Structure vPred_abs P β
    := Build_vPred _ (abs_pred_aux_mono P β).

  Definition obj_pred_aux (P: vPred) (V: View): iProp :=
    (∃ V0, P V0)%I.

  Lemma obj_pred_aux_mono P : vPred_Mono (obj_pred_aux P).
  Proof. done. Qed.

  Canonical Structure vPred_obj P := Build_vPred _ (obj_pred_aux_mono P).
  Global Instance vPred_obj_proper : Proper ((≡) ==> (≡)) vPred_obj.
  Proof.
    rewrite /vPred_obj /obj_pred_aux. intros ? ? H ?. rewrite /vPred_app /=.
    by apply uPred.exist_proper.
  Qed.

End AbstractView.

(* "⌞" U231E # BOTTOM LEFT CORNER *)
(* "⌟" U231F # BOTTEM RIGHT CORNER *)
Notation "⌞ P ⌟" := (vPred_abs P)
   (at level 20, right associativity, format "⌞ P ⌟"): vPred_scope.

Notation "▲ P" := (vPred_obj P)
  (at level 20, right associativity, format "▲ P"): vPred_scope.

Section proof.

  Context `{!foundationG Σ, !absViewG Σ}.

  Set Default Proof Using "Type".

  Lemma absView_agree β V1 V2 :
    absView β V1 -∗ absView β V2 -∗ ⌜V1 = V2⌝.
  Proof.
    iIntros "V1 V2".
    iDestruct (own_valid_2 with "V1 V2") as %HV.
    iPureIntro. by apply agree_op_invL' in HV.
  Qed.

  Lemma abs_splitjoin β (Q1 Q2: vPred) V:
    ⌞Q1 ∗ Q2⌟%VP β V ⊣⊢ ⌞Q1⌟%VP β V ∗ ⌞Q2⌟%VP β V.
  Proof.
    iSplit.
    - iIntros "Sep". iDestruct "Sep" as (V') "[#Abs [Q1 Q2]]".
      iSplitL "Q1"; iExists (V'); by iFrame.
    - iIntros "[S1 S2]".
      iDestruct "S1" as (V1) "[A1 ?]".
      iDestruct "S2" as (V2) "[A2 ?]".
      iDestruct (absView_agree with "A1 A2") as %?.
      subst V2. iExists V1. by iFrame.
  Qed.

  Lemma abs_exist {A : Type} (P: A -> vPred) β V:
    ⌞∃ (a: A), P a⌟%VP β V ⊣⊢ ∃ (a: A), ⌞P a⌟%VP β V.
  Proof.
    iSplit; iIntros "P".
    - iDestruct "P" as (V') "[abs P]".
      iDestruct "P" as (a) "P". iExists a, V'. by iFrame.
    - iDestruct "P" as (a) "P".
      iDestruct "P" as (V') "[? ?]". iExists V'. iFrame.
      by iExists a.
  Qed.

  Lemma absView_alloc_strong V (G : gset gname) :
      True ==∗ ∃ β, ⌜β ∉ G⌝ ∧ absView β V.
  Proof. by apply own_alloc_strong. Qed.

  Lemma absView_alloc V : True ==∗ ∃ β, absView β V.
  Proof. by apply own_alloc. Qed.

  Lemma aSeen_elim P β:
    aSeen β ∗ ⌞P⌟ β ⊧ P.
  Proof.
    constructor. intros.
    iIntros "[aSeen P]".
    iDestruct "aSeen" as (V1) "[% abs1]".
    iDestruct "P" as (V2) "[abs2 P]".
    iDestruct (absView_agree with "abs1 abs2") as %?. subst V2.
    by iApply (vPred_mono V1).
  Qed.

End proof.
