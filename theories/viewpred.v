From stdpp Require Import base.
From iris.base_logic.lib Require Import viewshifts.
From iris.proofmode Require Import tactics.
From igps Require Export infrastructure types.

Canonical Structure View_ofe : ofeT := leibnizC View.

Section ViewPredicates.
  Context {Σ : gFunctors}.
  Notation iProp := (iProp Σ).
  Implicit Type (V : View).
  Set Default Proof Using "Type*".

  Definition vProp_Mono (P : View -> iProp) :=  ∀ V V', V ⊑ V' -> P V ⊢ P V'.
  Definition vPred_Mono (P : View -c> iProp) :=  ∀ V V', V ⊑ V' -> P V ⊢ P V'.

  Structure vPred :=
    { vPred_pred : View -c> iProp;
      _ :  vPred_Mono vPred_pred }.

  Lemma vPred_mono' (P : vPred) : vPred_Mono (vPred_pred P).
  Proof. by destruct P. Qed.

  Definition vPred_app : vPred -> View -> iProp := vPred_pred.
  Coercion vPred_app : vPred >-> Funclass.

  Global Instance vPred_proper `(P : vPred) : Proper ((⊑) ==> (⊢)) (vPred_pred P) := vPred_mono' _.
  Lemma vPred_mono `{P : vPred} : ∀ V1 V2, V1 ⊑ V2 → vPred_app P V1 ⊢ vPred_app P V2.
  Proof. intros ? ? H. by rewrite /vPred_app H. Qed.
  (* Global Instance vPred_app_Proper P : Proper ((⊑) ==> (⊢)) (vPred_app P) := vPred_mono' _. *)

  Global Instance vPred_equiv : Equiv vPred := λ P1 P2, (vPred_app P1 : View -c> iProp) ≡ vPred_app P2.
  Global Instance vPred_dist : Dist vPred := λ n P1 P2, (vPred_app P1 : View -c> iProp) ≡{n}≡ vPred_app P2.
  Definition vPred_ofe_mixin : OfeMixin vPred.
  Proof.
    split.
    - intros. unfold equiv, vPred_equiv, dist, vPred_dist.
      exact: (mixin_equiv_dist _ ofe_fun_ofe_mixin).
    - intros. unfold dist, vPred_dist. split; [auto|auto|].
      intros ? ? ? H. by rewrite H.
    - intros. exact: (mixin_dist_S _ ofe_fun_ofe_mixin).
  Defined.
  Canonical Structure vPred_ofe : ofeT := OfeT vPred vPred_ofe_mixin.


  Global Instance vPred_app_dist_proper : Proper (dist n ==> eq ==> dist n) vPred_app.
  Proof. intros ? ? ? ? ? ? ?. subst. apply H. Qed.

  Global Instance vPred_app_proper : Proper ((≡) ==> (⊑) ==> (⊢)) (vPred_app).
  Proof.
    intros ? ? H ? ? H1. specialize (H x0). rewrite H. by f_equiv.
  Qed.

  Instance vPred_pred_dist_proper : Proper (dist n ==> dist n) vPred_pred.
  Proof. intros ? ? ? ?. subst. apply H. Qed.

  Program Definition vPred_compl c : vPred :=
    Build_vPred (compl (chain_map vPred_pred c)) _.
  Next Obligation.
    intros ? ? ? H. eapply uPred.entails_lim. intros.
    exact: (vPred_mono' (c n) _ _ H).
  Qed.

  Global Instance vPred_cofe : Cofe vPred_ofe :=
    {| compl := vPred_compl |}.
  Proof. intros; apply: conv_compl. Qed.

  Bind Scope vPred_scope with vPred.
  Delimit Scope vPred_scope with VP.

  Global Instance vProp_upclose_aux : UpClose (View -c> iProp) (View_ofe -c> iProp) :=
    λ P, λ V, (∀ V', ⌜V ⊑ V'⌝ -∗ P V')%I.
  Definition vProp_upclose_mono (P : View -> iProp) : vPred_Mono (↑ P).
  Proof.
    iIntros (V0 V1 H01) "P". iIntros (V2 H12).
    iApply ("P" $! _ with "[%]"). etrans; eauto.
  Qed.
  Canonical Structure vProp_upclose P := Build_vPred _ (vProp_upclose_mono P).
  Global Instance vProp_upclose_proper : Proper (((≡) : relation (View -c> iProp)) ==> ((≡) : relation (View -c> iProp))) vProp_upclose.
  Proof.
    move => ? ? H ? /=. rewrite /up_close /vProp_upclose_aux.
    iIntros. iSplit; iIntros "H"; iIntros (V) "E".
    - rewrite -(H _). by iApply "H".
    - rewrite (H _). by iApply "H".
  Qed.

  Definition vPred_close_aux (P : vPred) : View_ofe -c> iProp :=
    ↑((vPred_app P)).
  Canonical Structure vPred_close P := Build_vPred (vPred_close_aux P) (vProp_upclose_mono ((vPred_app P))).
  Global Instance vPred_upclose : UpClose (vPred) (vPred) := vPred_close.
  Global Instance vPred_vProp_upclose : UpClose (vPred) (View -> iProp) := λ P, {| ofe_mor_car := ((vPred_app P)) |}.

  Definition vPred_impl_aux (P Q : vPred) :=
    ↑(λ V, vPred_app P V → vPred_app Q V)%I.
  Definition vPred_impl_mono (P Q : vPred) :
    vPred_Mono (vPred_impl_aux P Q) := vProp_upclose_mono _.
  Canonical Structure vPred_impl P Q := Build_vPred _ (vPred_impl_mono P Q).
  Global Instance vPred_impl_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_impl.
  Proof.
    intros ? ? H ? ? H1 ?. apply vProp_upclose_proper.
    intros ?. by rewrite (H _) (H1 _).
  Qed.

  Definition vPred_wand_aux P Q :=
    ↑(λ V, vPred_app P V -∗ vPred_app Q V)%I.
  Definition vPred_wand_mono P Q :
    vPred_Mono (vPred_wand_aux P Q) := vProp_upclose_mono _.
  Canonical Structure vPred_wand P Q := Build_vPred _ (vPred_wand_mono P Q).
  Global Instance vPred_wand_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_wand.
  Proof.
    intros ? ? H ? ? H1 ?. apply vProp_upclose_proper.
    intros ?. by rewrite (H _) (H1 _).
  Qed.

  Definition vPred_pvs_aux {inG} E1 E2 (P : vPred)
    := (λ V, @fupd Σ inG E1 E2 (vPred_app P V)).
  Definition vPred_pvs_mono inG E1 E2 (P : vPred) :
    vPred_Mono (@vPred_pvs_aux inG E1 E2 P).
  Proof.
    repeat intro; iIntros "H". iMod "H". iModIntro.
    by iApply (vPred_mono with "H").
  Qed.
  Canonical Structure vPred_pvs {inG} E1 E2 (P : vPred) :=
    Build_vPred _ (vPred_pvs_mono inG E1 E2 P).
  Global Instance vPred_pvs_proper {inG : invG Σ} E1 E2 : Proper ((≡) ==> (≡)) (vPred_pvs E1 E2).
  Proof.
    intros ? ? H ?. by rewrite /vPred_pvs /= /vPred_pvs_aux (H _).
  Qed.

  Definition vPred_forall_aux {X} (P : X -c> vPred_ofe) :=
    (λ V, ∀ x, vPred_app (P x) V)%I.
  Definition vPred_forall_mono {X} (P : X -c> vPred_ofe) :
    vPred_Mono (vPred_forall_aux P).
  Proof.
    repeat intro; iIntros "H"; iIntros "%".
    iSpecialize ("H" $! a).
    by iApply (vPred_mono with "H").
  Qed.
  Canonical Structure vPred_forall {X} (P : X -c> vPred_ofe) :=
    Build_vPred _ (vPred_forall_mono P).
  Global Instance vPred_forall_proper {X} : Proper ((≡) ==> (≡)) (@vPred_forall X).
  Proof.
    intros ? ? H ?. apply uPred.forall_proper => ?.
    iIntros; iSplit; [rewrite (H _)|rewrite -(H _)]; auto.
  Qed.

  Definition vPred_exists_aux {X} (P : X -c> vPred_ofe) :=
    (λ V, ∃ x, vPred_app (P x) V)%I.
  Definition vPred_exists_mono {X} (P : X -c> vPred_ofe) :
    vPred_Mono (vPred_exists_aux P).
  Proof.
    repeat intro; iIntros "H". iDestruct "H" as (x) "H".
    iExists x.
    by iApply (vPred_mono with "H").
  Qed.
  Canonical Structure vPred_exists {X} (P : X -c> vPred_ofe) :=
    Build_vPred _ (vPred_exists_mono P).
  Global Instance vPred_exists_proper {X} : Proper ((≡) ==> (≡)) (@vPred_exists X).
  Proof.
    intros ? ? H ?. apply uPred.exist_proper => ?.
    iIntros; iSplit; [rewrite (H _)|rewrite -(H _)]; auto.
  Qed.

  Definition vPred_sep_aux P Q := (λ V, vPred_app P V ∗ vPred_app Q V)%I.
  Definition vPred_sep_mono P Q :
    vPred_Mono (vPred_sep_aux P Q).
  Proof.
    repeat intro; iIntros "[oP oQ]".
    iSplitL "oP"; by iApply vPred_mono.
  Qed.
  Canonical Structure vPred_sep P Q :=
    Build_vPred _ (vPred_sep_mono P Q).
  Global Instance vPred_sep_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_sep.
  Proof. intros ? ? H ? ? H1 ?. by apply uPred.sep_proper. Qed.

  Lemma vPred_sep_fold (P Q: vPred) V:
    (P V ∗ Q V)%I ≡ vPred_sep P Q V.
  Proof. done. Qed.

  Definition vPred_and_aux P Q := (λ V, vPred_app P V ∧ vPred_app Q V)%I.
  Definition vPred_and_mono P Q :
    vPred_Mono (vPred_and_aux P Q).
  Proof.
    repeat intro; unfold vPred_and_aux. iIntros "oPQ".
    iSplit.
    - iApply vPred_mono; first eassumption. by iApply uPred.and_elim_l.
    - iApply vPred_mono; first eassumption. by iApply uPred.and_elim_r.
  Qed.
  Canonical Structure vPred_and P Q :=
    Build_vPred _ (vPred_and_mono P Q).
  Global Instance vPred_and_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_and.
  Proof. intros ? ? H ? ? H1 ?. by apply uPred.and_proper. Qed.

  Definition vPred_or_aux P Q := (λ V, vPred_app P V ∨ vPred_app Q V)%I.
  Definition vPred_or_mono P Q :
    vPred_Mono (vPred_or_aux P Q).
  Proof.
    repeat intro; unfold vPred_and_aux. iIntros "[oP|oQ]".
    - iLeft. by iApply vPred_mono; first eassumption.
    - iRight. by iApply vPred_mono; first eassumption.
  Qed.
  Canonical Structure vPred_or P Q :=
    Build_vPred _ (vPred_or_mono P Q).
  Global Instance vPred_or_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_or.
  Proof. intros ? ? H ? ? H1 ?. by apply uPred.or_proper. Qed.

  Inductive vPred_entails (P Q : vPred) : Prop :=
    { vPred_in_entails V_l : ∀ V', V_l ⊑ V' → vPred_app P V' ⊢ vPred_app Q V' }.
  Coercion vPred_in_entails : vPred_entails >-> Funclass.
  Global Instance vPred_entails_proper : Proper ((≡) ==> (≡) ==> (↔)) vPred_entails.
  Proof.
    intros ? ? H ? ? H1. split; destruct 1; constructor; rewrite /vPred_app.
    - setoid_rewrite <-(H _). setoid_rewrite <-(H1 _). done.
    - setoid_rewrite ->(H _). setoid_rewrite ->(H1 _). done.
  Qed.

  Definition vPred_pure_aux (P : Prop) := (λ V, ⌜P⌝ : iProp)%I.
  Definition vPred_pure_mono P : vPred_Mono (vPred_pure_aux P).
  Proof. done. Qed.
  Canonical Structure vPred_pure P := Build_vPred _ (vPred_pure_mono P).
  Global Instance vPred_pure_proper : Proper ((↔) ==> (≡)) (vPred_pure).
  Proof. intros ? ? H ?. exact: uPred.pure_proper. Qed.

  Definition vPred_ipure_aux (P : iProp) := (λ V, P : iProp)%I.
  Definition vPred_ipure_mono P : vPred_Mono (vPred_ipure_aux P).
  Proof. done. Qed.
  Canonical Structure vPred_ipure P := Build_vPred _ (vPred_ipure_mono P).
  Global Instance vPred_ipure_proper : Proper ((≡) ==> (≡)) (vPred_ipure).
  Proof. by intros ? ? H ?. Qed.
  
  Definition vPred_later_aux (P : vPred) := (λ V, uPred_later (vPred_app P V) : iProp)%I.
  Definition vPred_later_mono P : vPred_Mono (vPred_later_aux P).
  Proof. repeat intro. iIntros. iNext. by iApply vPred_mono. Qed.
  Canonical Structure vPred_later P := Build_vPred _ (vPred_later_mono P).
  Global Instance vPred_later_proper : Proper ((≡) ==> (≡)) (vPred_later).
  Proof. intros ? ? H ?. exact: uPred.later_proper. Qed.

  Definition vPred_always_aux (P : vPred) := (λ V, uPred_always (vPred_app P V)).
  Lemma vPred_always_mono P : vPred_Mono (vPred_always_aux P).
  Proof. repeat intro. iIntros. iIntros "!#". by iApply (vPred_mono' P). Qed.
  Canonical Structure vPred_always P := Build_vPred _ (vPred_always_mono P).
  Global Instance vPred_always_proper : Proper ((≡) ==> (≡)) (vPred_always).
  Proof. intros ? ? H ?. exact: uPred.always_proper. Qed.

  Definition vPred_everywhere_aux (P : vPred) := (λ V, ∀ V', P V')%I.
  Lemma vPred_everywhere_mono P : vPred_Mono (vPred_everywhere_aux P).
  Proof. repeat intro. auto. Qed.
  Canonical Structure vPred_everywhere P := Build_vPred _ (vPred_everywhere_mono P).
  Global Instance vPred_everywhere_proper : Proper ((≡) ==> (≡)) (vPred_everywhere).
  Proof. intros ? ? H ?. rewrite /= /vPred_everywhere_aux. by setoid_rewrite (H _). Qed.

  Definition vPred_Seen_aux V_0 : _ -c> iProp := (λ V, ⌜V_0 ⊑ V⌝%I).
  Arguments vPred_Seen_aux _ / _.
  Definition vPred_Seen_mono V_0 : vPred_Mono (vPred_Seen_aux V_0).
  Proof.
    repeat intro; iIntros "%"; iPureIntro. etrans; by eauto.
  Qed.
  Canonical Structure vSeen V_0 := Build_vPred _ (vPred_Seen_mono V_0).
  Global Instance vPred_Seen_proper : Proper ((≡) ==> (≡)) (vSeen).
  Proof. intros ? ? H ?. rewrite /= /vPred_Seen_aux. by rewrite (H). Qed.

End ViewPredicates.

Bind Scope vPred_scope with vPred.
Delimit Scope vPred_scope with VP.
Global Notation "P ⊧  Q" := (vPred_entails P Q)
  (at level 99, Q at level 200, right associativity) : C_scope.
Notation "(⊧)" := vPred_entails (only parsing) : C_scope.
Notation "P ⊩ Q" := ((∀ V : View, P V) ⊢ (∀ V : View, Q V))%I
  (at level 100, no associativity) : C_scope.
(* Notation "(⊣⊢)" := (equiv (A:=uPred _)) (only parsing) : C_scope. *)
Notation "■ φ" := (vPred_pure φ%C%type)
  (at level 20, right associativity) : vPred_scope.
Notation "☐ P" := (vPred_ipure P%I)
  (at level 20, right associativity) : vPred_scope.
Notation "▷ P" := (vPred_later P%VP)
  (at level 20, right associativity) : vPred_scope.
Notation "□ P" := (vPred_always P%VP)
  (at level 20, right associativity) : vPred_scope.
Notation "'ɐ' P" := (vPred_everywhere P%VP)
  (at level 20, right associativity) : vPred_scope.
Notation "P → Q" := (vPred_impl P Q) : vPred_scope.
Notation "P -∗ Q" := (vPred_wand P Q)
  (at level 99, Q at level 200, right associativity) : vPred_scope.
Notation "∀ x .. y , P" := (vPred_forall (λ x, .. (vPred_forall (λ y, P%VP)) ..)) : vPred_scope.
Notation "∃ x .. y , P" := (vPred_exists (λ x, .. (vPred_exists (λ y, P%VP)) ..)) : vPred_scope.
Notation "P ∗ Q" := (vPred_sep P Q) : vPred_scope.
Notation "x = y" := (vPred_pure (x%C%type = y%C%type)) : vPred_scope.
Notation "x ⊥ y" := (vPred_pure (x%C%type ⊥ y%C%type)) : vPred_scope.
Notation "'False'" := (vPred_pure False) : vPred_scope.
Notation "'True'" := (vPred_pure True) : vPred_scope.
Infix "∧" := vPred_and : vPred_scope.
Notation "(∧)" := vPred_and (only parsing) : vPred_scope.
Infix "∨" := vPred_or : vPred_scope.
Notation "(∨)" := vPred_or (only parsing) : vPred_scope.

Notation "⊨ P" := (∀ V : View, (P)%VP V)%I
  (at level 20, right associativity) : uPred_scope.
(* Notation "P ⊧ Q" := (∀ V, (P%VP -∗ Q%VP)%VP V) (only parsing) : uPred_scope. *)


Notation "|={ E1 , E2 }=> Q" := (vPred_pvs E1 E2 Q%VP)
  (at level 99, E1, E2 at level 50, Q at level 200,
   format "|={ E1 , E2 }=>  Q") : vPred_scope.
Notation "P ={ E1 , E2 }=∗ Q" := (P -∗ |={E1,E2}=> Q)%VP
  (at level 99, E1,E2 at level 50, Q at level 200,
   format "P  ={ E1 , E2 }=∗  Q") : vPred_scope.
Notation "P ={ E1 , E2 }=> Q" := (P ⊧ |={E1,E2}=> Q)
  (at level 99, E1, E2 at level 50, Q at level 200, only parsing) : C_scope.

Notation "|={ E }=> Q" := (vPred_pvs E E Q%VP)
  (at level 99, E at level 50, Q at level 200,
   format "|={ E }=>  Q") : vPred_scope.
Notation "P ={ E }=∗ Q" := (P -∗ |={E}=> Q)%VP
  (at level 99, E at level 50, Q at level 200,
   format "P  ={ E }=∗  Q") : vPred_scope.
Notation "P ={ E }=> Q" := (P ⊧ |={E}=> Q)%VP
  (at level 99, E at level 50, Q at level 200, only parsing) : C_scope.
Notation "P ={ E }=> Q" := (P ={E,E}=> Q)%VP
  (at level 99, E at level 50, Q at level 200,
   format "P  ={ E }=>  Q") : vPred_scope.

Arguments vPred [_].
(* Arguments vPred_pred [_] _ : simpl never. *)
Arguments vPred_app [_] _ _ : simpl never.
Arguments vPred_mono [_ _] _ _ _.