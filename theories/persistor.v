From iris.base_logic Require Import lib.invariants big_op.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth frac excl gmap.

Import uPred.

From igps Require Import iris_lemmas.
From igps Require Export lang.
From igps.base Require Import accessors.

Definition persistN : namespace := nroot .@ "persistor".
Notation persist_invN := (persistN .@ "inv").
Notation persist_locN := (persistN .@ "loc").

Definition PerInfos := gmapUR loc (dec_agreeR InfoT).

Class persistorG Σ :=
  PersistorG {
    persistorG_perinfo :> inG Σ (authR PerInfos);
    persistorG_perinfo_name :> gname;
  }.

Definition persistorΣ : gFunctors := #[GFunctor (constRF (authR PerInfos))].

Class persistorPreG Σ :=
  PersistorPreG {
    persistorPreG_perinfo :> inG Σ (authR PerInfos)
  }.
Instance subG_persistorPreG {Σ} : subG persistorΣ Σ → persistorPreG Σ.
Proof. solve_inG. Qed.

Section Persistor.
  Context `{!foundationG Σ, !persistorG Σ}.

  Definition PER := persistorG_perinfo_name.

  Implicit Types (l: loc) (φ Ψ: loc -c> InfoT -c> iProp Σ).

  Definition persistor_inv γ : iProp Σ :=
    (∃ (ρ: PerInfos), own γ (● ρ) ∗  [∗ map] l↦v ∈ ρ, Info l 1 v)%I.

  Definition persistor_ctx l X: iProp Σ :=
    (inv persist_invN (persistor_inv PER) ∗ own PER (◯ {[l := DecAgree X]}))%I.

  Global Instance persistor_ctx_persistent l X:
    PersistentP (persistor_ctx l X).
  Proof. apply _. Qed.

  Section Location.
    Definition persisted l φ Ψ : iProp Σ :=
      (∃ X, Ψ l X ∗ inv (persist_locN .@ l) (φ l X) ∗ persistor_ctx l X)%I.


    Global Instance persisted_proper l : Proper ((≡) ==> (≡) ==> (≡)) (persisted l).
    Proof.
      intros ? ? H1 ? ? H2. apply uPred.exist_proper => ? /=.
      apply uPred.sep_proper; [exact: H2|].
      apply uPred.sep_proper; [|auto].
      f_equiv. exact: H1.
    Qed.

    Lemma persistor_mono l φ Ψ Ψ':
      (∀ X, Ψ l X → Ψ' l X) ∗ persisted l φ Ψ ⊢ persisted l φ Ψ'.
    Proof.
      iIntros "[Imp Per]".
      iDestruct "Per" as (X) "[Pre #Pers]".
      iExists X. iFrame "Pers".
      by iApply "Imp".
    Qed.

    Lemma persistor_drop l φ Ψ:
      persisted l φ Ψ ⊢ ∃ X, Ψ l X.
    Proof.
      iIntros "Per". iDestruct "Per" as (X) "[Per _]".
      by iExists X.
    Qed.

    Lemma persistor_splitjoin l φ Ψ1 Ψ2:
      persisted l φ Ψ1 ∗ persisted l φ Ψ2 
      ⊣⊢ persisted l φ (λ l X, Ψ1 l X ∗ Ψ2 l X).
    Proof.
      iSplit.
      - iIntros "[Per1 Per2]".
        iDestruct "Per1" as (X1) "[Per1 [#Inv1 #[PerCtx1 PerOwn1]]]".
        iDestruct "Per2" as (X2) "[Per2 [#Inv2 #[PerCtx2 PerOwn2]]]".
        iCombine "PerOwn1" "PerOwn2" as "PerOwn".
        iDestruct (own_valid with "PerOwn") as %Valid.
        move: Valid.
        rewrite op_singleton
           => /singleton_valid /dec_agree_op_inv Valid.
        inversion Valid. subst.
        iExists X2. iFrame. iFrame "Inv2". by iSplit.
      - iIntros "Per".
        iDestruct "Per" as (X) "[[Int1 Int2] #Inv]".
        iSplitL "Int1".
        + iExists X. iFrame. auto.
        + iExists X. iFrame. auto.
    Qed.

    Lemma persistor_join_l l φ1 φ2 Ψ1 Ψ2:
      persisted l φ1 Ψ1 ∗ persisted l φ2 Ψ2
      ⊢ persisted l φ1 (λ l X, Ψ1 l X ∗ Ψ2 l X).
    Proof.
      iIntros "[P1 P2]".
      iDestruct "P1" as (X1) "(P1 & #Inv1 & #PC1 & #oP1)".
      iDestruct "P2" as (X2) "(P2 & #Inv2 & #PC2 & #oP2)".
      iCombine "oP1" "oP2" as "oP".
      iDestruct (own_valid with "oP") as %Valid.
      rewrite op_singleton in Valid.
        apply singleton_valid, dec_agree_op_inv in Valid.
      inversion Valid. subst.
      iExists X2. iFrame "P1 P2 Inv1 oP1 PC1".
    Qed.

    Lemma persistor_join_r l φ1 φ2 Ψ1 Ψ2:
      persisted l φ1 Ψ1 ∗ persisted l φ2 Ψ2
      ⊢ persisted l φ2 (λ l X, Ψ1 l X ∗ Ψ2 l X).
    Proof.
      iIntros "[P1 P2]".
      iDestruct "P1" as (X1) "(P1 & #Inv1 & #PC1 & #oP1)".
      iDestruct "P2" as (X2) "(P2 & #Inv2 & #PC2 & #oP2)".
      iCombine "oP1" "oP2" as "oP".
      iDestruct (own_valid with "oP") as %Valid.
      rewrite op_singleton in Valid.
        apply singleton_valid, dec_agree_op_inv in Valid.
      inversion Valid. subst.
      iExists X2. iFrame "P1 P2 Inv2 oP1 PC1".
    Qed.

    Lemma persistor_join_later_rl l φ1 φ2 Ψ1 Ψ2 E:
      persisted l φ1 Ψ1 ∗ ▷ persisted l φ2 Ψ2
      ={E}=∗ persisted l φ1 (λ l X, Ψ1 l X ∗ ▷ Ψ2 l X).
    Proof.
      iIntros "[P1 P2]".
      iDestruct "P1" as (X1) "(P1 & #Inv1 & #PC1 & #oP1)".
      iDestruct "P2" as (X2) "(P2 & #Inv2 & #PC2 & >#oP2)".
      iCombine "oP1" "oP2" as "oP".
      iDestruct (own_valid with "oP") as %Valid.
      rewrite op_singleton in Valid.
        apply singleton_valid, dec_agree_op_inv in Valid.
      inversion Valid. subst.
      iExists X2. iModIntro. iFrame "P1 P2 Inv1 oP1 PC1".
    Qed.

    Global Instance persistor_persistent l φ Ψ (Per: ∀ X, PersistentP (Ψ l X))
      : PersistentP (persisted l φ Ψ).
    Proof. apply _. Qed.

    Lemma persistor_inv_alloc' E v X l φ Ψ:
      nclose (physN) ⊆ E → nclose persistN ⊆ E →
      PSCtx ∗ inv persist_invN (persistor_inv PER)
        ∗ Info l 1 v
      ={E,E∖↑persist_locN.@l}=∗  (Ψ l X -∗ persisted l φ Ψ)
                            ∗ (▷ φ l X ={E∖↑persist_locN.@l,E}=∗ True).
    Proof.
      intros Sub Sub2.
      iIntros "(#PSCtx & #Ctx & Info)".
      iInv persist_invN as (ρ) ">(HA & Hρ)" "HClose" .
      iMod (PSInv_Info_update _ _ _ (DecAgree X) with "[$PSCtx $Info]") as "Info";
      [solve_ndisj|done|..].

      iAssert ⌜ρ !! l = None⌝%I with "[#]" as "%". {
        destruct (ρ !! l) eqn:Eq => //.
        rewrite (big_sepM_delete _ _ l _); last by apply Eq.
        iDestruct "Hρ" as "[Info' Hρ]".
        iExFalso. iApply Info_Exclusive. iSplitL "Info" => //. }
      iMod (own_update with "[$HA]") as "[HA FA]".
        { apply auth_update_alloc.
          apply: (alloc_singleton_local_update _ _ (DecAgree X)) => //. }

      iMod ("HClose" with "[HA Hρ Info]").
        { iNext. iExists _.
          iSplitL "HA" => //.
          rewrite big_sepM_insert; auto. by iFrame. }

      iMod (inv_alloc_open (persist_locN .@ l) _ (φ l X) ) as "[#? $]"; [solve_ndisj|].
      iModIntro. iIntros "?". iExists X. iFrame "∗ #".
    Qed.

    Lemma persistor_inv_alloc E v X l φ Ψ:
      nclose (physN) ⊆ E → nclose persistN ⊆ E →
      PSCtx ∗ inv persist_invN (persistor_inv PER)
        ∗ Info l 1 v ∗ ▷ φ l X ∗ Ψ l X
        ={E}=∗ persisted l φ Ψ.
    Proof.
      intros Sub Sub2.
      iIntros "[#PSCtx [#Ctx [Info [Pred Per]]]]".
      iMod (persistor_inv_alloc' with "[$PSCtx $Ctx $Info]") as "[W VS]";
      [solve_ndisj|solve_ndisj|].
      iMod ("VS" with "Pred"). by iApply "W".
    Qed.

    Lemma persistor_open' E l φ Ψ:
      ↑persist_locN .@ l ⊆ E →
      True ⊢
           persisted l φ Ψ ={E,E∖↑persist_locN .@ l}=∗
           ∃ X, ▷ φ l X ∗ Ψ l X
                  ∗ □ (∀ Ψ', Ψ' l X -∗ persisted l φ Ψ')
                  ∗ (▷ φ l X ={E∖↑persist_locN .@ l, E}=∗ True).
    Proof.
      intros Sub.
      iIntros "Per".
      iDestruct "Per" as (X) "[Pred [#Inv #Ctx]]".
      iInv (persist_locN .@ l) as "PInv" "HClose".
      iModIntro. iExists X.
      iFrame "Pred PInv HClose".
      iIntros "!#". iIntros (Ψ') "Pred".
      iExists X. iFrame "∗ #".
    Qed.

    Lemma persistor_open E l φ Ψ:
      ↑persist_locN .@ l ⊆ E →
      True ⊢
        persisted l φ Ψ ={E,E∖↑persist_locN .@ l}=∗
        ∃ X, ▷ φ l X ∗ Ψ l X
             ∗ ∀ Ψ',  ▷ φ l X ∗ Ψ' l X ={E∖↑persist_locN .@ l,E}=∗ persisted l φ Ψ'.
    Proof.
      intros Sub.
      iIntros "Per".
      iMod (persistor_open' with "Per") as (X) "(φ & Ψ & T & HClose)"; [done|].
      iExists X. iFrame "∗". iModIntro. iIntros (Ψ') "[PInv Pred]".
      iDestruct ("T" with "Pred") as "$".
      by iApply "HClose".
    Qed.

  End Location.

End Persistor.

Notation PersistorCtx := (inv persist_invN (persistor_inv PER)).

Lemma persistor_init `{!foundationG Σ} `{!persistorPreG Σ} :
  PSCtx ={↑persist_invN}=∗ ∃ _ : persistorG Σ, PersistorCtx.
Proof.
  iIntros.
  iMod (own_alloc (● ∅)) as (γ) "H".
  { done. }
  set PG := (PersistorG _ _ γ).
  iExists PG.
  iMod (inv_alloc persist_invN (↑persist_invN) (persistor_inv PER) with "[-]") as "$"; last done.
  iNext. rewrite /persistor_inv. iExists _. iFrame.
  by rewrite big_sepM_empty.
Qed.
