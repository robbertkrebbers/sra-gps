From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth frac excl gmap.
From iris.program_logic Require Export ownp.
From iris.algebra Require Import deprecated.
Export deprecated.dec_agree.
From iris.base_logic Require Import big_op lib.invariants.
From igps Require Import lang history.

Set Bullet Behavior "Strict Subproofs".

Notation History := (gset (@Val * View))%type.

Definition InfoT := positive.
Definition Views := (gmapUR thread (exclR (leibnizC View))).
Definition Hists := ((gmapUR loc (exclR (leibnizC History)))).
Definition Infos := ((gmapUR loc (prodR fracR (dec_agreeR InfoT)))).

Class foundationG Σ :=
  FoundationG {
      foundationG_iris_inG :> ownPG ra_lang Σ;
      foundationG_view :> inG Σ (authR Views);
      foundationG_hist :> inG Σ (authR Hists);
      foundationG_info :> inG Σ (authR Infos);
      foundationG_view_gname :> gname;
      foundationG_hist_gname :> gname;
      foundationG_info_gname :> gname;
    }.

Definition AuthType {Σ} {T} (X : inG Σ (authR T)) := T.

(* TODO: figure out why this is needed and get rid of it *)
Global Instance foundationG_hist_insert `{foundationG Σ} : 
  Insert loc (excl (gset (Val * View))) (AuthType foundationG_hist) := _.
Global Instance foundationG_hist_singleton `{foundationG Σ} :
  SingletonM loc (excl (gset (Val * View))) (AuthType foundationG_hist) := _.

Section Hist.

  Definition map_vt := map_gset (λ msg, (mval msg, mview msg)).
  Definition hTime l (x: Val * View) := match x with | (_,V) => V !! l end.
  Global Arguments map_vt _ /.
  Global Arguments hTime _ _ /.

  Lemma map_vt_hist_msg v V l M:
    (v,V) ∈ map_vt (hist M l) ↔
    ∃m, m ∈ M ∧ mval m = v ∧ mloc m = l ∧ mview m = V.
  Proof. set_solver. Qed.

  Definition hTime_disj l vV1 vV2 := hTime l vV1 = hTime l vV2 → vV1 = vV2.
  Definition hTime_pw_disj l (h: History) :=
    ∀ vV vV', vV ∈ h → vV' ∈ h → hTime_disj l vV vV'.
  Definition hTime_ok l h :=
    (∀ vV, vV ∈ h → ∃ t, vV.2 !! l = Some t) ∧ hTime_pw_disj l h.

  Instance hTime_ok_proper l:
    Proper ((≡) ==> (flip impl)) (hTime_ok l).
  Proof. solve_proper. Qed.

  Definition adj_opt (ot1 ot2: option positive) :=
    ∃ t, ot1 = Some t ∧ ot2 = Some (t + 1)%positive.

  Definition no_adj_right l (h: History) (V : View) :=
    ¬ ∃ p, p ∈ h ∧ adj_opt (V !! l) (p.2 !! l).

  Lemma adj_eq l (h: History) p1 p2 V
    (In1: p1 ∈ h) (In2: p2 ∈ h) (Disj: hTime_pw_disj l h)
    (Adj1: adj_opt (p1.2 !! l) (V !! l))
    (Adj2: adj_opt (p2.2 !! l) (V !! l))
    : p1 = p2.
  Proof.
    destruct Adj1 as [t1 [Eq1 Eqp1]].
    destruct Adj2 as [t2 [Eq2 Eqp2]].
    rewrite Eqp2 in Eqp1. inversion Eqp1 as [Eqt].
    apply Pos.add_reg_r in Eqt. subst t2.
    apply (Disj _ _ In1 In2).
    destruct p1. destruct p2. simpl in *.
    by rewrite Eq1 Eq2.
  Qed.

  Lemma hTime_pw_disj_map_vt (M: memory) h l t
    (Eqh: h ≡ map_vt (hist_from M l t))
    (MSOK: msgs_ok M)
    : hTime_pw_disj l h.
  Proof.
    move => vV1 vV2. set_unfold.
    rewrite 2!Eqh /hist_from_opt /hTime_disj.
    move => [m1 [-> [_ [Eq1 Inm1]]]] [m2 [-> [_ [Eq2 Inm2]]]] /=.
    destruct (MSOK _ Inm1) as [VT1 _].
    destruct (MSOK _ Inm2) as [VT2 _].
    rewrite -{1}Eq1 -Eq2 VT1 VT2. move => [EqT].
    destruct (memory_ok M _ _ Inm1 Inm2) as [-> |[|]] => //.
    exfalso. by rewrite -Eq2 in Eq1.
  Qed.

  Lemma hTime_pw_disj_mono l (h h': History)
    (Sub: h ⊆ h') (PW: hTime_pw_disj l h')
    : hTime_pw_disj l h.
  Proof.
    rewrite -> elem_of_subseteq in Sub.
    move => ? ? /Sub ? /Sub ?. by apply PW.
  Qed.

  Lemma hTime_ok_mono l (h h': History) (Sub: h ⊆ h')
    : hTime_ok l h' → hTime_ok l h.
  Proof.
    move => [Ok Disj]. split; last by apply (hTime_pw_disj_mono _ _ h') => //.
    move => ? ?. apply Ok. rewrite -> elem_of_subseteq in Sub. auto.
  Qed.

  Lemma elem_of_hist_view_ok v V (M: memory) l ot
    (MSOK: msgs_ok M)
    : (v, V) ∈ map_vt (hist_from_opt M l ot)
      → ∃ t, V !! l = Some t.
  Proof.
    rewrite elem_of_map_gset.
    move => [m0 [[_ ->] /elem_of_filter [Le /elem_of_hist [<- Inm0]]]].
     eexists. apply (MSOK _ Inm0).
  Qed.

  Definition val_but_min l (h: History) :=
    ∃min_[rel_of (hTime l) (⊑)] vV ∈ h, ∀ vV', vV' ∈ h ∖ {[vV]} → isval (fst vV').

  Lemma hist_val_mono l (h: History) v v' V V'
    (HVal : val_but_min l h)
    (In: (v, V) ∈ h)
    (In': (v', V') ∈ h)
    (IsVal: isval v)
    (Lt: V !! l ⊏ V' !! l)
    : isval v'.
  Proof.
    destruct HVal as [[v0 V0] [In0 [Min0 Val]]].
    case (decide ((v', V') = (v0,V0))) => [Eq|NEq]; last first.
    - apply (Val (v',V')). rewrite elem_of_difference elem_of_singleton.
      abstract naive_solver.
    - exfalso. apply Min0 in In. simpl in In. inversion Eq. subst.
      destruct (V !! l); destruct (V0 !! l) => //.
      cbn in Lt. cbn in In. cbn. eapply Pos.lt_irrefl, Pos.le_lt_trans; eauto.
  Qed.

End Hist.

Section GhostDefs.
  Context `{fG : !foundationG Σ}.

  Implicit Types
           (ς : state)
           (VIEW : AuthType foundationG_view)
           (HIST : AuthType foundationG_hist)
           (INFO : AuthType foundationG_info).

  Definition VN := foundationG_view_gname.
  Definition HN := foundationG_hist_gname.
  Definition IN := foundationG_info_gname.

  Definition at_safe (h: History) (V_l: View) :=
    ∃ v V, (v, V) ∈ h ∧ V ⊑ V_l.

  Definition na_safe l (h: History) (V_l: View) :=
    ∃max_[rel_of (hTime l) (⊑)] vV ∈ h, snd vV ⊑ V_l.

  Definition init_local (h: History) (V_l: View) :=
    ∃ v V, (v, V) ∈ h ∧ V ⊑ V_l ∧ isval v.

  Definition init l h :=
    (∃ v V, (v, V) ∈ h ∧ isval v) ∧ val_but_min l h.

  Definition uninit (h: History) := ∃ V, {[(A,V)]} = h.

  Definition alloc_local (h: History) V_l :=
    ∃ (v : Val) (V : View), (v, V) ∈ h ∧ V ⊑ V_l ∧ v ≠ D.

  Definition value_at (h: History) V v :=
    (v,V) ∈ h.

  Definition value_at_hd l (h: History) V v :=
    ∃max_[rel_of (hTime l) (⊑)] vV ∈ h, vV = (v, V).

  Definition Seen : thread -> View -> iProp Σ :=
    λ π V, own VN (◯ ({[π := Excl V]}: Views)).
  Definition Hist : loc -> History -> iProp Σ :=
    λ l h, own HN (◯ ({[l := Excl h]})).
  Definition Info : loc -> frac -> dec_agree InfoT -> iProp Σ :=
    λ l q v, own IN (◯ ({[l := (q, v)]})).

  Definition LInv ς l h := 
           ∃min_[rel_of (hTime l) (⊑)] vV ∈ h,
             nats ς !! l ⊑ snd vV !! l
             ∧ h ≡ map_vt (hist_from_opt (mem ς) l (snd vV !! l))
             ∧ ∀ v V, (v, V) ∈ h ∖ {[vV]} → isval v
  .

  Definition HInv ς HIST : Prop :=
    dom _ (HIST) ≡ locs (mem ς) ∧
    ∀ l h, HIST !! l = Some (Excl h) → LInv ς l h.

  Definition VInv ς VIEW : Prop :=
    ∀ π V, VIEW !! π = Some (Excl V) → view_ok (mem ς) V.

  Definition IInv ς INFO: Prop :=
    dom (gset _) INFO ≡ locs (mem ς).
  Global Arguments IInv _ _ /.

  Lemma HInv_IInv_agree ς HIST INFO
    (Inv1: HInv ς HIST) (Inv2: IInv ς INFO):
    dom (gset loc) HIST ≡ dom (gset (loc)) INFO.
  Proof.
    destruct Inv1 as [H1 H2].
    simpl in Inv2.
    by rewrite H1 -Inv2.
  Qed.

  Local Open Scope I.

  Definition DInv HIST : iProp Σ :=
    [∗ map] l↦hh ∈ HIST,
    match hh with
    | Excl h => ∀ V, ⌜(h: History) ≡ {[D, V]}⌝ → Hist l h ∗ ∃ v, Info l 1 v
    | _ => False
    end
  .

  Definition PSInv : iProp Σ :=
    ∃ ς VIEW HIST INFO,
      ownP ς ∗ own VN (● VIEW)
             ∗ own HN (● HIST)
             ∗ own IN (● INFO)
             ∗ ⌜phys_inv ς⌝
             ∗ DInv HIST
             ∗ ⌜VInv ς VIEW⌝
             ∗ ⌜HInv ς HIST⌝
             ∗ ⌜IInv ς INFO⌝.

  Close Scope I.

End GhostDefs.

Arguments LInv _ _ _ /.

Definition physN : namespace := nroot .@ "phys".
Notation PSCtx := (inv physN PSInv).

Section ValidGhosts.

  Implicit Types (π ρ : thread) (l : loc) (V : View) (h : History).

  Lemma Views_frag_valid_le `{fG : !foundationG Σ} π V (VIEW: Views)
    (Valid: ✓ (● VIEW ⋅ ◯ {[π := Excl V]})) : VIEW !! π = Some (Excl V).
  Proof.
    move/auth_valid_discrete_2 : Valid => /= [[VIEW' /(_ π)]] Eq /(_ π).
    apply leibniz_equiv in Eq.
    rewrite Eq !lookup_op lookup_singleton.
    by move/exclusive_Some_l => ->.
  Qed.

  Lemma Hists_frag_valid_eq l h (HIST: Hists)
    (Valid: ✓ (● HIST ⋅ ◯ {[l := Excl h]}))
    : HIST !! l = Some $ Excl h.
  Proof.
    move/auth_valid_discrete_2 : Valid => /= [[HIST' /(_ l)]] Eq /(_ l).
    apply leibniz_equiv in Eq.
    rewrite Eq !lookup_op lookup_singleton.
    by move/exclusive_Some_l => ->.
  Qed.

  Lemma Infos_frag_valid_eq l (q: Qp) v (INFO: Infos)
    (Valid: ✓ (● INFO ⋅ ◯ {[l := (q, v)]}))
    : ∃(q': Qp), INFO !! l = Some (q',v) ∧ (q ≤ q')%Qc.
  Proof.
    move/auth_valid_discrete_2 : Valid => [/lookup_included /(_ l) Incl Valid].
    rewrite lookup_singleton in Incl.
    apply option_included in Incl as [|[qv [qv' [Eq1 [Eq2 Incl]]]]]; first done.
    apply leibniz_equiv_iff in Eq2.
    assert (Valid2 := lookup_valid_Some _ _ _ Valid Eq2).
    apply leibniz_equiv in Eq2.
    destruct Incl as [Eq|Incl].
    - apply leibniz_equiv in Eq. rewrite -Eq -Eq1 in Eq2.
      by exists q.
    - inversion Eq1. subst. destruct Valid2 as [Validq Validv'].
      destruct qv' as [q' v'].
      destruct Incl as [? [Inclq Incl]]. simpl in *.
      apply leibniz_equiv in Incl. rewrite Incl in Validv'.
      apply dec_agree_op_inv in Validv'.
      rewrite -Validv' dec_agree_idemp in Incl. subst v'.
      exists q'. split; first auto.
      apply leibniz_equiv in Inclq.
      rewrite Inclq frac_op' -{1}(Qcanon.Qcplus_0_r q).
      apply (Qcplus_le_mono_l 0 (x.1) q), Qcanon.Qclt_le_weak.
      destruct x => /=. by destruct c.
  Qed.

End ValidGhosts.


Section Properties.

  Lemma na_safe_Mono l h (V1 V2: View) (LE: V2 ⊑ V1):
    na_safe l h V2 → na_safe l h V1.
  Proof.
    rewrite /na_safe => [[m [In [Max Ext]]]].
    exists m; repeat split; auto.
    by rewrite -LE.
  Qed.

  Lemma at_safe_Mono h (V1 V2: View) (LE: V2 ⊑ V1) :
    at_safe h V2 → at_safe h V1.
  Proof.
    move => [? [? [? ?]]]. do 2 eexists. split; first eauto. etrans; eauto.
  Qed.

  Lemma init_local_Mono h (V1 V2: View) (LE: V2 ⊑ V1) :
    init_local h V2 → init_local h V1.
  Proof.
    move => [? [? [? [? ?]]]]. do 2 eexists.
    split; first eauto. split; [etrans; eauto|auto].
  Qed.

  Lemma alloc_init_local h V:
    init_local h V → alloc_local h V.
  Proof.
    move => [v' [V' [In [Le Val]]]].
    exists v', V'. repeat split => //.
    destruct v' => //. by inversion Val.
  Qed.

  Lemma alloc_local_Mono h (V1 V2: View) (LE: V2 ⊑ V1) :
    alloc_local h V2 → alloc_local h V1.
   Proof.
    move => [v [V [In [Le ND]]]].
    exists v, V. repeat split; [auto| |auto]. etrans; eauto.
  Qed.

  Lemma init_local_at_safe h V:
    init_local h V → at_safe h V.
  Proof.
    move => [? [? [? [? _]]]]. by do 2 eexists.
  Qed.

  Lemma na_at_safe l h V : na_safe l h V → at_safe h V.
  Proof.
    move => [vV [In [Max Ext]]].
    exists (fst vV), (snd vV) => //.
  Qed.

  Lemma na_init_local l h V
    (HVal : val_but_min l h)
    (Disj: hTime_pw_disj l h)
    : na_safe l h V
      → init_local h V
      → ∃max_[rel_of (hTime l) (⊑)] vV1 ∈ h, isval (fst vV1) ∧ snd vV1 ⊑ V.
  Proof.
    case: HVal => [[v0 V0] [In0 [Max0 Val]]].
    move => [[v1 V1] [In [Max Ext]]] [vI [VI [InI [ExtI ValI]]]].
    exists (v1,V1); repeat split => //.
    case (decide ((v1, V1) = (v0, V0))) => [Eq|NEq]; last first.
    - apply (Val (v1, V1)). rewrite elem_of_difference elem_of_singleton.
      abstract naive_solver.
    - inversion Eq. subst.
      case (decide ((vI, VI) = (v0,V0))) => [[<- <-] //|NEq'].
      assert (TEq: V0 !! l = VI !! l).
        { apply: anti_symm. apply (Max0 _ InI). apply (Max _ InI). }
      apply (Disj _ _ In InI) in TEq. exfalso. auto.
  Qed.

End Properties.

Section Exclusives.
  Context `{Countable K} {A : cmraT}.

  Lemma gmapR_singleton_Exclusive (i: K) (x y: A) `{!Exclusive x}:
    ✓ ({[i := x]} ⋅ {[i := y]}) → False.
  Proof.
    rewrite op_singleton. move/(_ i). rewrite lookup_singleton.
    by apply exclusive_l.
  Qed.

  Lemma gmapUR_singleton_Exclusive (i: K) (x y: A) `{!Exclusive x}:
   ✓ ({[i := x]} ⋅ {[i := y]} : gmapUR _ _) → False.
  Proof.
    rewrite op_singleton. move/(_ i). rewrite lookup_singleton.
    by apply exclusive_l.
  Qed.
  (* Why are these so slow? *)
End Exclusives.

Section Laws.
  Context `{fG : !foundationG Σ}.
  Collection level1 := Σ fG.
  Local Set Default Proof Using "level1".

  Implicit Type (h : History).

  Lemma Seen_Exclusive π V1 V2 : Seen π V1 ∗ Seen π V2 ⊢ False.
  Proof.
    rewrite /Seen -own_op.
    iIntros "Own".
    iDestruct (own_valid with "Own") as %Valid.
    exfalso. move: Valid.
    rewrite -auth_frag_op op_singleton.
    move/(_ π). by rewrite lookup_singleton.
  Qed.

  Lemma Hist_Exclusive l h1 h2 : Hist l h1 ∗ Hist l h2 ⊢ False.
  Proof.
    rewrite /Hist -own_op.
    iIntros "Own".
    iDestruct (own_valid with "Own") as %Valid.
    exfalso. move: Valid.
    rewrite -auth_frag_op op_singleton.
    move/(_ l). by rewrite lookup_singleton.
  Qed.

  Lemma Info_Exclusive l v v':
    Info l 1 v ∗ Info l 1 v' ⊢ False.
  Proof.
    rewrite /Info -own_op.
    iIntros "Info".
    iDestruct (own_valid with "Info") as %Valid.
    move/auth_valid_discrete : Valid => /=.
    rewrite op_singleton => Valid. exfalso.
    eapply (lookup_valid_Some _ l) in Valid;
      last by rewrite lookup_singleton.
    eapply exclusive_l; eauto.
      apply pair_exclusive_l. auto with typeclass_instances.
  Qed.

  Lemma Hist_hTime_ok E l h:
    ↑physN ⊆ E →
    PSCtx ∗ Hist l h ⊢ |={E}=> Hist l h ∗ ⌜hTime_ok l h⌝.
  Proof.
    intros. iIntros "(#? & Hist)".
    iInv physN as (ς VIEW HIST INFO)
              "(? & ? & >Own & ? & >% & ? & ? & Rem)" "HClose".
    iDestruct "Rem" as "[>HInv ?]".
    iDestruct "HInv" as %[Hi1 Hi2].
    iCombine "Own" "Hist" as "AH".
    iDestruct (own_valid with "AH") as %Hl%Hists_frag_valid_eq.
    iDestruct "AH" as "(Own & Hist)".
    iMod ("HClose" with "[-Hist]").
      { iExists _,_,_,_. iFrame. iNext. iSplit => //. }
    iFrame. iPureIntro.
    move : (Hi2 _ _ Hl) => [vV [In [? [? [Eq ?]]]]]. split.
    - move => [v V] /=. rewrite Eq elem_of_map_gset.
      move => [m]. rewrite elem_of_filter.
      move => [ [_ ->] [? /elem_of_hist [<- Inm]]].
      exists (mtime m).
      apply (IMOk _ H0 _ Inm).
    - move => [v1 V1] [v2 V2] In1.
      rewrite -> Eq in In1.
      rewrite Eq.
      move : In1.
      rewrite 2!elem_of_map_gset.
      setoid_rewrite elem_of_filter.
      setoid_rewrite elem_of_hist.
      move => [m1 [[-> ->] [_ [<- In1]]]] [m2 [[-> ->] [_ [Eq2 In2]]]].
      destruct (IMOk _ H0 _ In1) as [VT1 _].
      destruct (IMOk _ H0 _ In2) as [VT2 _].
      rewrite /hTime_disj /= -{2}Eq2 VT1 VT2. inversion 1 as [EqT].
      rewrite (_: m1 = m2); auto.
      eapply hist_time_unique => //; by apply elem_of_hist.
  Qed.
End Laws.


Lemma Qp_ge_1 (q: Qp): ¬ ((1 + q)%Qp ≤ 1%Qp)%Qc.
Proof.
  intros Hle.
  apply (Qcplus_le_mono_l q 0 1) in Hle.
  apply Qcle_ngt in Hle. by destruct q.
Qed.

Lemma frac_invalid_plus_1: ∀ (q: Qp), (¬ ✓ (1 + q)%Qp)%C.
Proof.
  intros q H.
  by apply (Qp_ge_1 q).
Qed.

Section UpdateGhosts.
  Context `{fG : !foundationG Σ}.
  Local Notation iProp := (iProp Σ).
  Collection level1 := Σ fG.
  Local Set Default Proof Using "level1".

  Implicit Types (π ρ : thread) (l : loc) (V : View) (h : History).
  Local Open Scope I.

  Lemma Views_update_override π V (VIEW: Views) V'
    : own VN (● VIEW ⋅ ◯ {[π := Excl V]})
      ⊢ |==> own VN (● (<[π := Excl V']> VIEW) ⋅ ◯ {[π := Excl V']}).
  Proof.
    iIntros "Own".
    iDestruct (own_valid with "Own") as %Valid.
    apply Views_frag_valid_le in Valid.
    destruct (VIEW !! π) as [Va|] eqn:VEq; last done.
    iMod (own_update with "[$Own]"); last iAssumption.
    apply auth_update.
    apply: singleton_local_update; first by exact VEq.
    by apply exclusive_local_update. 
  Qed.

  Lemma Views_alloc (VIEW: Views) ρ V'
    (New: VIEW !! ρ = None)
    : own VN (● VIEW) ⊢ (|==> own VN (● (<[ρ := Excl V']> VIEW) ⋅ ◯ {[ρ := Excl V']})).
  Proof.
    apply own_update, auth_update_alloc.
    apply: alloc_singleton_local_update => //.
  Qed.

  Lemma Hists_update_override l h h' (HIST: Hists)
    : own HN (● HIST ⋅ ◯ {[l := Excl h]})
      ⊢ (|==> own HN (● <[l := Excl h']>HIST ⋅ ◯ {[l := Excl h']})).
  Proof.
    iIntros "Own".
    iDestruct (own_valid with "Own") as %Valid.
    apply Hists_frag_valid_eq in Valid.
    iMod (own_update with "[$Own]"); last iAssumption.
    apply auth_update.
    apply: singleton_local_update; [eauto| exact: exclusive_local_update].
  Qed.

  Lemma Hists_alloc (HIST: Hists) l (h: History)
    (New: HIST !! l = None)
    : own HN (● HIST)
      ⊢ (|==> own HN (● (<[l := Excl h]> HIST) ⋅ ◯ {[l := Excl h]})).
  Proof.
    apply own_update, auth_update_alloc.
    apply: alloc_singleton_local_update => //.
  Qed.

  Lemma Infos_update_override l v v' (INFO: Infos)
    : (✓ v')%C →
      own IN (● INFO ⋅ ◯ {[l := (1%Qp, v)]})
      ⊢ (|==> own IN (● <[l := (1%Qp, v')]>INFO ⋅ ◯ {[l := (1%Qp, v')]})).
  Proof.
    move => vV. iIntros "FI".
    iDestruct (own_valid with "FI") as %Valid.
    apply Infos_frag_valid_eq in Valid as [q' [Valid1 Valid2]].
    iMod (own_update with "[$FI]"); last iAssumption.
    apply auth_update.
    apply: singleton_local_update; first eauto.
    by apply exclusive_local_update.
  Qed.

  Lemma Infos_alloc (INFO: Infos) l v
    (New: INFO !! l = None)
    : (✓ v)%C →
      own IN (● INFO)
      ⊢ (|==> own IN (● (<[l := (1%Qp, v)]> INFO) ⋅ ◯ {[l := (1%Qp, v)]})).
  Proof.
    move => Valid.
    apply own_update, auth_update_alloc, alloc_singleton_local_update => //.
  Qed.

  Lemma DInv_update_value l (h h': History) (HIST: Hists)
    (NotD: ∀ V, h' ≠ {[(D,V)]})
    (Old: HIST !! l = Excl' h)
    : DInv HIST -∗ DInv (<[l := Excl h']>HIST).
  Proof.
    assert (HDel: delete l (<[l:=Excl h']> HIST) = delete l HIST).
      { symmetry. apply (partial_alter_compose (λ _, None) _ HIST). }
    rewrite /DInv.
    rewrite (big_sepM_delete _ (HIST) l (Excl h)); last assumption.
    rewrite (big_sepM_delete _ (<[l:=Excl h']> HIST) l (Excl h'));
       last by rewrite (lookup_insert HIST l (Excl h')).
    rewrite HDel.
    iIntros "[_ $]".
    iIntros (?) "%". exfalso. eapply NotD. by apply/leibniz_equiv.
  Qed.

  Lemma DInv_update_return l h (h': History) (HIST: Hists)
    (Old: HIST !! l = Excl' h)
    : (∃ v, Info l 1 v) ∗ Hist l h' ∗ DInv HIST 
              -∗ DInv (<[l := Excl h']>HIST).
  Proof.
    assert (HDel: delete l (<[l:=Excl h']> HIST) = delete l HIST).
      { symmetry. apply (partial_alter_compose (λ _, None) _ HIST). }
    rewrite /DInv.
    rewrite (big_sepM_delete _ HIST l (Excl h)); last assumption.
    rewrite (big_sepM_delete _ (<[l:=Excl h']> HIST) l (Excl h'));
                last by rewrite lookup_insert.
    rewrite HDel.
    iIntros "[HInfo [HHist [_ $]]]".
    iIntros (?) "%". by iFrame "HInfo".
  Qed.

  Close Scope I.

  Lemma IInv_update
    ς INFO l v v'
    (Inv: IInv ς INFO)
    (Hl: INFO !! l = Some v)
    : IInv ς (<[l:=v']> INFO).
  Proof.
    cbn in *.
    rewrite <-Inv.
    have H : l ∈ dom (gset loc) INFO by apply: elem_of_dom_2.
    rewrite dom_insert.
    abstract set_solver+H Inv.
  Qed.

  Lemma IInv_update_addins
    ς ς' HIST INFO l h h'
    (HI1: HInv ς HIST)
    (II1: IInv ς INFO)
    (HI2: HInv ς' (<[l:=Excl h']> HIST))
    (HO: HIST !! l = Excl' h)
    (Upd: rel_inv ς ς')
    : IInv ς' INFO.
  Proof.
    cbn in *.
    have H : l ∈ dom (gset loc) HIST by apply: elem_of_dom_2.
    rewrite -HInv_IInv_agree //=; case: (HI2) => [E2 _]; 
      case: (HI1) => [E1 _] //.
    rewrite -E2 II1 dom_insert E1. abstract set_solver+H E1.
  Qed.

End UpdateGhosts.
