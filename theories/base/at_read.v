From igps.base Require Import at_shared.

Lemma f_read_at `{fG : !foundationG Σ} π V_l l h :
  {{{ PSCtx
    ∗ ▷ Seen π V_l ∗ ▷ Hist l h ∗ ⌜init_local h V_l⌝ }}}
    ([ #l ]_at, V_l) @ ↑physN
  {{{ (v : Z) V', RET (#v, V');
      ⌜V_l ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h ∗ ⌜at_safe h V'⌝
        ∗ ∃ V_1,  ⌜V_1 ⊑ V'⌝
                  ∗ ⌜V_1 !! l = V' !! l⌝
                  ∗ ⌜value_at h V_1 (VInj v)⌝ }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H3 as [HInv1 HInv2]. symmetry in H2.
  move: (HInv2 _ _ H2) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  destruct H as [v' [V' [In' [Local' IsVal']]]].
  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }
  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from(mem ς) l t)).
    { move: Eqh. abstract set_solver+. }
  iApply (wp_load_at_pst with "oPS"); [by auto|by auto|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move: (Min0 _ In') => /= ->.
  - (* init_pre ς π l *)
    eapply init_pre_helper with (t:=t) (v:=v') (V:=V'); eauto.
    + exists (v0, V0). split; [|split] => //. by move => [].
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (v1 ς' V'') "(% & % & % & % & HP)".

    inversion H5.

    iApply ("Post" $! _ _).
    iMod ("HClose" $! ς' _ h with "[DInv $HP]") as "[Seen' Hist']".
      { rewrite (insert_id HIST); last auto.
        (* re-establish PSInv *)
        iFrame (H H3 H4) "∗". by rewrite /HInv /= MOld NATSOld. }
    (* show post-condition *)
    iModIntro. iFrame "Hist' Seen'". repeat iSplit.
    + iPureIntro. rewrite VUpd. solve_jsl.
    + iPureIntro. exists v', V'. split; auto. rewrite VUpd Local'. solve_jsl.
    + iExists (mview m).
       destruct (IMOk _ H0 _ In) as [EqT _]. subst.
      rewrite EqT. repeat iSplit; iPureIntro; [solve_jsl|..].
      * rewrite lookup_join_max Local EqT. solve_jsl.
      * red. rewrite -EqVal Eqh2 /value_at /map_vt elem_of_map_gset.
        exists m. split; auto.
          apply elem_of_hist_from. repeat (split; auto).
          etrans; last by apply TimeLe.
          change (Some t ⊑ Some t').
          rewrite -Local -EqV0. etrans; eauto.
          etrans; first by apply (Min0 _ In'). by simpl.
Qed.