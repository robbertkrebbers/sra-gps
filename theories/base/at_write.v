From igps.base Require Import at_shared.

Lemma f_write_at `{fG : !foundationG Σ} π V_l l h v:
  {{{ PSCtx 
        ∗ ▷ Seen π V_l ∗ ▷ Hist l h ∗ ⌜alloc_local h V_l⌝ }}}
    ([ #l ]_at <- #v, V_l) @ ↑physN
    {{{ V' h', RET (LitV LitUnit, V');
        ⌜V_l ⊑ V'⌝ ∗ Seen π V' 
         ∗ Hist l h' ∗ ⌜init_local h' V'⌝ ∗ ⌜init l h'⌝
         ∗ ⌜h' ≡ {[VInj v, V']} ∪ h⌝
         ∗ ⌜h ⊥ {[VInj v, V']}⌝
         ∗ ⌜value_at h' V' (VInj v)⌝}}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H3 as [HInv1 HInv2]. symmetry in H2.
  move: (HInv2 _ _ H2) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  destruct H as [v' [V' [In' [Local' IsND']]]].

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }
  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from(mem ς) l t)).
    { move: Eqh. abstract set_solver+. }
  iApply (wp_store_at_pst with "oPS"); [by auto|by auto|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move: (Min0 _ In') => /= ->.
  - (* alloc_pre ς π l *) 
    eapply alloc_pre_helper; eauto.
    + eapply alloc_local_Mono; first by auto. exists v', V'. by erewrite <-Eqh2.
    + exists (v0, V0). repeat setoid_rewrite <-Eqh2.
      split; last (split); auto; by move => [].
    + apply H0.

  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (ς' V'') "(% & % & % & % & HP)".

    inversion H5.

    assert (Le: V0 !! l ⊑ V_l !! l). 
      { rewrite -(Local' _). by apply :(Min0 (v', V')). }
    assert (V0 !! l ⊏ Some (mtime m)).
      { by move: Le Local ->. }

    (* get h' *)
    destruct ((write_at_update_hist _ _ V0 _ _ _ v _ Eqh2 Disj MUpd EqLoc))
         as [h' [Inh' [Eqh' [Eqh'2 Disjh']]]]; try auto.
        by do 2 eexists. apply H0. apply H.

    assert (HInv': HInv ς' (<[l:=Excl h']> HIST)). {
      eapply (write_at_update_HInv ς _ V0 h h') => //.
      - by rewrite EqVal.
      - by do 2 eexists.
      - apply H. }


    iApply ("Post" $! _ h').
    iMod ("HClose" $! ς' _ h' with "[DInv $HP]") as "[Seen' Hist']".
      { (* re-establish PSInv *)
        iFrame (H H3 H4 HInv').
        iNext. iApply (DInv_update_value with "DInv"); last eauto.
        move => ? Eqh3. move : Inh'.
        rewrite Eqh3 elem_of_singleton. by inversion 1. }


    (* show post-condition *)
    iModIntro. rewrite -EqView. iFrame (Eqh'2 Disjh' Inh') "# ∗".
    iSplit; [|iSplit]; iPureIntro.
    + rewrite EqView VUpd. solve_jsl.
    + by exists (VInj v), (mview m).
    + split; first by do 2 eexists.
      assert (EqL: (<[l:=Excl h']> HIST !! l = Excl' h')).
        { by rewrite lookup_insert. }
      move: (proj2 HInv' l h' EqL) => [vV [? [? [_ [_ Ha]]]]].
      exists vV. repeat (split; auto).
      move => [? ?]. apply Ha.
Qed.