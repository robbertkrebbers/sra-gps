From igps.base Require Import na_shared.

Lemma f_write_na `{fG : !foundationG Σ} π V_l l h v:
  {{{ PSCtx
      ∗ ▷ Seen π V_l ∗ ▷ Hist l h ∗ ⌜alloc_local h V_l⌝ }}}
    ([ #l ]_na <- #v, V_l) @ ↑physN
  {{{ V' h', RET (#(), V');
        ⌜V_l ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h'
        ∗ ⌜init_local h' V'⌝ ∗ ⌜na_safe l h' V'⌝ ∗ ⌜init l h'⌝
        ∗ ⌜value_at_hd l h' V' (VInj v)⌝ 
        ∗ ⌜h' = {[(VInj v),V']}⌝ }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H3 as [HInv1 HInv2]. symmetry in H2.
  move: (HInv2 _ _ H2) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }
  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from(mem ς) l t)).
    { move: Eqh; set_solver+. }

  destruct H as [v' [V' [In' [Local' IsND']]]].
  (* move: H1 => [H1]. *)
  iApply (wp_store_na_pst with "oPS"); [eauto|auto|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move : (Min0 _ In') => /= ->.
  - (* alloc_pre ς π l *) 
    eapply alloc_pre_helper; eauto.
    + eapply alloc_local_Mono; first by auto. exists v', V'. by erewrite <- Eqh2.
    + exists (v0, V0). repeat setoid_rewrite <-Eqh2.
      split; first auto. split; first auto. by move => [].
    + apply H0.
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (ς' V'') "(% & % & % & % & HP)".

    inversion H5.

    (* get h' *)
    pose (h' := {[(mval m, mview m)]}: History).

    assert (HInv': HInv ς' (<[l:=Excl h']> HIST)). {
      eapply (write_na_update_HInv ς) => //. apply H. }

    iApply ("Post" $! _ _).
    iMod ("HClose" $! ς' _ h' with "[DInv $HP]") as "[Seen' Hist']".
      { (* re-establish PSInv *)
        iFrame (H H3 H4 HInv').
        iNext. iApply (DInv_update_value with "DInv"); last eauto.
        move => VD. rewrite /h' EqVal => Eq.
        assert (EqD: (VInj v, mview m) = (D, VD)).
        { by rewrite -(elem_of_singleton (C:= History))
             -Eq elem_of_singleton. }
          by inversion EqD.
      }

    (* show post-condition *)
    (* TODO : redundant proofs *)
    iModIntro. iFrame "Hist' Seen'". rewrite /h'.
    rewrite EqVal EqView.
    repeat iSplitL ""; last done; iPureIntro.
    + rewrite VUpd. solve_jsl.
    + eexists (VInj v), _. by rewrite elem_of_singleton.
    + exists (VInj v, mview m). rewrite -EqView. abstract set_solver+.
    + eexists (VInj v), _. by rewrite elem_of_singleton.
    + exists (VInj v, V''). abstract set_solver+.
    + exists (VInj v, V''). abstract set_solver+.
Qed.