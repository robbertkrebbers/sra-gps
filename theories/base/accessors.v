From iris.base_logic Require Export lib.invariants lib.viewshifts.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth excl gmap.

From igps Require Import infrastructure machine lang history.
From igps Require Export ghosts.

Set Bullet Behavior "Strict Subproofs".

Section Accessors.

  Context `{fG : !foundationG Σ}.
  Local Notation iProp := (iProp Σ).
  Collection level1 := Σ fG.
  Local Set Default Proof Using "level1".

  Implicit Types (l : loc) (V : View).
  Implicit Types
           (ς : state ra_lang)
           (VIEW : AuthType foundationG_view)
           (HIST : AuthType foundationG_hist)
           (INFO : AuthType foundationG_info).

  Lemma PSInv_Info_update E l v v':
    ↑physN ⊆ E → (✓ v')%C →
    PSCtx ∗ Info l 1 v
      ={E}=> Info l 1 v'.
  Proof.
    iIntros (? Valid) "!# [#PS Info]".
    iInv physN as (ς VIEW HIST INFO)
              "(? & ? & ? & >AI & ? & ? & >(? & ? & %))" "HClose".
    iCombine "AI" "Info" as "FI".
    iDestruct (own_valid with "FI") as %[? [? _]]%Infos_frag_valid_eq.

    iMod (Infos_update_override with "[$FI]") as "[AI Info]"; first eauto.
    iMod ("HClose" with "[-Info]").
      { iNext. iExists ς, VIEW, HIST, (<[l:=(1%Qp, v')]> INFO).
        iFrame "∗". iPureIntro. by eapply IInv_update. }
    iExact "Info".
  Qed.

  Lemma PSInv_open_alloc E π V:
    ↑physN ⊆ E →
    PSCtx ∗ ▷ Seen π V ={E,E∖↑physN}=∗
      ∃ ς HIST INFO,
      ▷ ownP ς
        ∗ ⌜phys_inv ς⌝
        ∗ ⌜view_ok (mem ς) V⌝
        ∗ ⌜HInv ς HIST⌝
        ∗ ⌜IInv ς INFO⌝
        ∗ own IN (● INFO)
        (* ∗ own HN (● HIST) *)
        ∗ ∀ l v ς' V' t,
          (ownP ς'
            ∗ ⌜phys_inv ς'⌝
            ∗ ⌜rel_inv ς ς'⌝
            ∗ ⌜view_ok (mem ς') V'⌝
            ∗ ⌜HIST !! l = None⌝
            ∗ ⌜msgs (mem ς') ≡ msgs (mem ς) ∪ {[<l → A @ t, (V ⊔ {[l := t]}) >]}⌝
            ∗ ⌜nats ς' ≡ <[l := t]>(nats ς)⌝
            ∗ ⌜LInv ς' l {[A, V']}⌝
            (* ∗ ⌜HInv ς' (<[l := Excl {[A,V']}]> HIST)⌝ *)
            ∗ ⌜IInv ς' (<[l := (1%Qp, v)]> INFO)⌝
            (* ∗ (DInv HIST -∗ DInv (<[l := Excl {[(A,V')]}]> HIST)) *)
            ∗ own IN (● (<[l := (1%Qp, v)]> INFO) ⋅ ◯ {[l := (1%Qp, v)]})
            (* ∗ own HN (● (<[l := Excl {[A,V']}]> HIST) ⋅ ◯ {[l := Excl {[A,V']}]}) *)
          )
          ={E∖↑physN,E}=∗ Seen π V' ∗ Hist l {[(A,V')]} ∗ Info l 1 v.
  Proof.
    iIntros (?) "[#HInv >FV]".
    iInv physN as (ς VIEW HIST INFO) 
                "(HoP & >AV & >AH & >AI & >% & DInv & >% & >% & >%)"
                "HClose".

    iCombine "AV" "FV" as "HV".
    iDestruct (own_valid with "HV") as %?%Views_frag_valid_le.

    iModIntro.
    iExists ς, HIST, INFO. iFrame "HoP AI".
    repeat (iSplitL ""; first auto).
    { iPureIntro. exact: (H1 _ _ H5). }
    iIntros (l v ς' V' t)
            "(HoP' & % & % & % & % & % & % & % & DVs & [AI FI])".
    iFrame "FI".
    iMod (Views_update_override with "[$HV]") as "[HV1 FV']".
    iFrame "FV'".
    iMod ((Hists_alloc _ _ {[A,V']}) with "AH") as "[AH $]"; first eauto.
    iApply "HClose". iNext.
    (* iDestruct ("DVs" with "DInv") as "DInv". *)

    iExists _,_,_,_. iFrame. repeat (iSplit; auto).
    - rewrite /DInv. rewrite big_op.big_opM_insert => //. iFrame.
      iIntros (?) "%". exfalso. specialize (H12 (A,V')). by set_solver+H12.
    - iPureIntro => ? ?.
      rewrite lookup_insert_Some => [] [[_ [<- //]]|[_ /H1]].
      apply: view_ok_mono => //. by apply H6.
    - iPureIntro. rewrite dom_insert. rewrite (proj1 (H2)) /locs H9.
      rewrite -> (comm (∪)). by rewrite gset_map_union gset_map_singleton.
    - iPureIntro => l' ?.
      rewrite lookup_insert_Some => [] [[<- [<- //]]|[NEq /(proj2 H2)]].
      intros ([v'' V''] & In & ? & ? & ? & ?).
      eexists; split; first eassumption.
      split; [|split; [|split]]; try auto.
      + rewrite -H13.
        case C: (nats ς' !! _); [|];
        move: C (H10 l') => -> C;
        symmetry in C;
        (rewrite lookup_insert_ne in C; last by auto);
        apply leibniz_equiv in C;
           by rewrite C.
      + rewrite H14. f_equiv.
        rewrite /hist_from_opt.
        move => ?.
        rewrite !elem_of_filter H9.
        split; intros (? & Eq & ?); (split; [|split]; try done).
        * rewrite elem_of_union; by left.
        * move: H17 NEq. rewrite elem_of_union ?elem_of_singleton => [] [//|].
          by rewrite <- Eq => [] ->.
  Qed.

  Lemma PSInv_open_VH E π V l h :
    ↑physN ⊆ E →
    PSCtx ∗ ▷ Seen π V ∗ ▷ Hist l h ={E,E∖↑physN}=∗
     ∃ ς HIST,
      ▷ ownP ς ∗ ⌜phys_inv ς⌝
         ∗ ⌜view_ok (mem ς) V⌝
         ∗ ⌜Some $ Excl h = HIST !! l⌝
         ∗ ▷ DInv HIST
         ∗ ⌜HInv ς HIST⌝
         ∗ ∀ ς' V' h',
          ownP ς'
          ∗ ⌜phys_inv ς'⌝
          ∗ ⌜rel_inv ς ς'⌝
          ∗ ▷ DInv (<[l := Excl h']> HIST)
          ∗ ⌜view_ok (mem ς') V'⌝
          ∗ ⌜HInv ς' (<[l := Excl h']> HIST)⌝
    ={E∖↑physN,E}=∗ Seen π V' ∗ Hist l h'.
  Proof.
    iIntros (?) "(#HInv & >FV & >FH)".
    iInv physN as (ς VIEW HIST INFO) 
                "(HoP & >AV & >AH & >AI & >% & DInv & >% & >% & >%)" "HClose".
    iCombine "AV" "FV" as "HV".
    iCombine "AH" "FH" as "HH".
    iDestruct (own_valid with "HV") as %?%Views_frag_valid_le.
    iDestruct (own_valid with "HH") as %?%Hists_frag_valid_eq.

    iModIntro.
    iExists ς, HIST. iFrame "HoP DInv". repeat (iSplit; auto).
    { iPureIntro. by apply: H1. }
    iIntros (ς' V' h') "(HoP' & % & % & DInv' & % & %)".
    iMod (Views_update_override with "[$HV]") as "[HV1 FV']".
    iFrame "FV'".
    iMod (Hists_update_override with "[$HH]") as "[HH1 HH2]".
    iFrame "HH2".
    iApply "HClose".
    iNext. iExists _, _, _ ,_.
    iFrame (H4 H9) "∗". iPureIntro. 
    split; last by apply (IInv_update_addins ς _ HIST _ l h h').
    move => ? ?. rewrite lookup_insert_Some => [] [[_ [<- //]]|[_ /H1]].
    apply: view_ok_mono => //. by apply H7.
  Qed.

  Lemma PSInv_open_V E π V l h :
    ↑physN ⊆ E →
    PSCtx ∗ ▷ Seen π V ∗ ▷ Hist l h ={E,E∖↑physN}=∗
     ∃ ς HIST,
        ▷ ownP ς ∗ ⌜phys_inv ς⌝
         ∗ ⌜Some $ Excl h = HIST !! l⌝
         ∗ ⌜view_ok (mem ς) V⌝
         ∗ ▷ DInv HIST
         ∗ ⌜HInv ς HIST⌝
         ∗ ∀ ς' V' h',
          (Hist l h'
          -∗  ownP ς'
            ∗ ⌜phys_inv ς'⌝
            ∗ ⌜rel_inv ς ς'⌝
            ∗ ▷ DInv (<[l := Excl h']> HIST)
            ∗ ⌜view_ok (mem ς') V'⌝
            ∗ ⌜HInv ς' (<[l := Excl h']> HIST)⌝)
    ={E∖↑physN,E}=∗ Seen π V'.
  Proof.
    iIntros (?) "(#HInv & >FV & >FH)".
    iInv physN as (ς VIEW HIST INFO) 
                "(HoP & >AV & >AH & >AI & >% & DInv & >% & >% & >%)" "HClose".
    iCombine "AV" "FV" as "HV".
    iCombine "AH" "FH" as "HH".
    iDestruct (own_valid with "HV") as %?%Views_frag_valid_le.
    iDestruct (own_valid with "HH") as %?%Hists_frag_valid_eq.

    iModIntro.
    iExists ς, HIST. iFrame (H0 H2 (H1 _ _ H5) (eq_sym H6)) "HoP DInv".
    iIntros (ς' V' h') "Hist'".

    iMod (Hists_update_override with "[$HH]") as "[HH1 HH2]".

    iDestruct ("Hist'" with "HH2") as "(HoP' & % & % & DInv' & % & %)".

    iMod (Views_update_override with "[$HV]") as "[HV1 FV']".
    iFrame "FV'".
    iApply "HClose".
    iNext.
    iExists _, _, _, _. iFrame (H4 H9) "∗".
    iPureIntro. split; last by apply (IInv_update_addins ς _ HIST _ l h h').
    move => ? ?. rewrite lookup_insert_Some => [] [[_ [<- //]]|[_ /H1]].
    apply: view_ok_mono => //. by apply H7.
  Qed.

End Accessors.