From igps.base Require Import na_shared.

Lemma f_dealloc `{fG : !foundationG Σ} π V_l l h v :
  {{{ PSCtx ∗ ▷ Seen π V_l 
      ∗ ▷ Hist l h ∗ ▷ Info l 1 v ∗ ⌜alloc_local h V_l⌝ }}}
    (Dealloc #l, V_l) @ ↑physN
  {{{ V', RET (#(), V');
      ⌜V_l ⊑ V'⌝ ∗ Seen π V' }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & Info & %) Post".

  (* open invariant *)
  iMod (PSInv_open_V with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H3 as [HInv1 HInv2].
  move: (HInv2 _ _ (eq_sym H1)) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }
  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from(mem ς) l t)). { set_solver+Eqh. }

  destruct H as [v' [V' [In' [Local' IsND']]]].

  iApply (wp_dealloc_pst with "oPS"); [eauto|auto|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move : (Min0 _ In') => /= ->.
  - (* alloc_pre ς π l *) 
    eapply alloc_pre_helper; eauto.
    + eapply alloc_local_Mono; first by auto. exists v', V'. by erewrite <- Eqh2.
    + exists (v0, V0). repeat setoid_rewrite <-Eqh2. do 2 (split; first auto).
      by move => [].
    + apply H0.
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (ς' V'') "[% [% [% [% HP]]]]".

    inversion H5.

    (* get h' *)
    pose (h' := {[(mval m, mview m)]}: History).

    assert (HInv': HInv ς' (<[l:=Excl h']> HIST)). {
      eapply (write_na_update_HInv ς) => //. apply H. }

    iApply ("Post" $! _).
    iMod ("HClose" $! ς' _ h' with "[DInv $HP Info]") as "Seen'".
      { iIntros "Hist'". iFrame (H H3 H4 HInv').
        (* re-establish PSInv *)
        iNext.
        iApply (DInv_update_return with "[$Hist' $DInv Info]"); first eauto.
        by iExists _. }

    (* show post-condition *)
      iFrame "Seen'". iPureIntro. subst. rewrite VUpd. solve_jsl.
Qed.