From igps.base Require Import na_shared.

Lemma f_read_na `{fG : foundationG Σ} π V_l l h :
  {{{ PSCtx ∗ ▷ Seen π V_l ∗ ▷ Hist l h
        ∗ ⌜na_safe l h V_l⌝ ∗ ⌜init_local h V_l⌝ }}}
    ([ #l ]_na, V_l) @ ↑physN
  {{{ (v: Z) V', RET (#v, V');
        ⌜V_l ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h ∗ ⌜na_safe l h V'⌝
          ∗ ∃ V_1,  ⌜V_1 ⊑ V'⌝ ∗ ⌜V_1 !! l = V' !! l⌝
                  ∗ ⌜value_at_hd l h V_1 (VInj v)⌝ }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & % & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H4 as [HInv1 HInv2]. symmetry in H3.
  move: (HInv2 _ _ H3) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  assert (VBM: val_but_min l h).
    { exists (v0, V0). repeat split; [auto|auto|by move => []]. }

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H1.
      - by rewrite /map_vt -Eqh. }
  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from(mem ς) l t)).
    { move: Eqh; set_solver+. }

  assert (PW: hTime_pw_disj l h).
    { apply (hTime_pw_disj_map_vt (mem ς) _ _ t); [auto|apply H1]. }

  destruct (na_init_local _ _ _ VBM PW H H0)
    as [[v V] [InV [Max [IsVal Local]]]]. simpl in Local, IsVal.

  (* TODO : so many weird obligations *)
  assert (V0 !! l ⊑ V !! l).
    { apply (Min0 _ InV). }
  assert (Le0: V0 !! l ⊑ V_l !! l).
    { rewrite H4 (Local _). by subst. }
  assert (∃ t1, V_l !! l = Some t1) as [t1 EqV1].
    { destruct (V_l !! l) eqn:Eqt1; [auto| by rewrite EqV0 in Le0].
      by eexists. }
  assert (HVl: V_l !! l = V !! l).
    { apply: anti_symm; last by auto.
      destruct (H2 _ _ EqV1)
        as [m' [Hm'1 [Hm'2 [Hm'3 Hm'4]]]].
      destruct (IMOk _ H1 _ Hm'1) as [VT' _].
      rewrite EqV1 -Hm'3.
      destruct (decide (t ⊑ mtime m')) as [TLe|TGt].
      - rewrite -VT' Hm'2. apply (Max (mval m',_)).
        rewrite Eqh2 elem_of_map_gset. exists m'.
        by rewrite elem_of_hist_from.
      - apply Pos.lt_nle in TGt. etrans; eauto. rewrite EqV0.
        by apply Pos.lt_le_incl. }

  have : (∃ m, (v,V) = (mval m, mview m) ∧ m ∈ (hist_from (mem ς) l t))
    => [|[m [[Eqv EqV] Inm]]].
    { move: InV. by rewrite Eqh2 elem_of_map_gset. }
  apply elem_of_hist_from in Inm as [EqL [Inm Lem]].

  (* assert (Eq: Some t1 = Some (mtime m)). *)
  (*   { rewrite -EqV1 HVl -EqL. *)
  (*     inversion Eqm. subst. apply (IMOk _ H1 _ Inm). } *)
  (* inversion Eq. subst t1. *)

  iApply (wp_load_na_pst with "oPS") => //.
  - (* drf_pre_read_na ς π l *)
    econstructor; [..|apply: (MT_Some m)]; eauto.
    + apply Some_inj. rewrite -EqV1 HVl -(_ : msg_ok m) ?EqL ?EqV //.
      by apply H1.
    + rewrite -EqL. by apply set_unfold_2.
    + move => m0 /elem_of_filter [EqL0 Inm0].
      destruct (decide (t ⊑ mtime m0)) as [TLe|TGt].
      * change (Some (mtime m0) ⊑ Some (mtime m)).
        destruct (IMOk _ H1 _ Inm0) as [VT0 _].
        rewrite -VT0 EqL0 -(_ : msg_ok m) -?EqV ?EqL; last by apply H1.
        apply: (Max (mval m0,_)).
        rewrite Eqh2 elem_of_map_gset. exists m0.
        by rewrite elem_of_hist_from.
      * apply Pos.lt_nle in TGt. etrans; eauto. by apply Pos.lt_le_incl.
  - exact: (init_pre_helper).
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (v1 ς' V') "(% & % & % & % & HP)".
    inversion H8.
    (* inversion H7. rewrite HV1 in ViewOf. inversion ViewOf. subst V1. *)

    (* assert (VIEW ς' !! π = Some V' ∧ Vl ⊑ V') as [HV'1 HV'2]. *)
    (*   { split. *)
    (*     - by rewrite TUpd lookup_insert. *)
    (*     - rewrite VUpd; apply join_ext_l. } *)

    iApply ("Post" $! _ V').
    iMod ("HClose" $! ς' V' h with "[DInv $HP]") as "[Seen' Hist']".
      { rewrite (insert_id HIST); last auto.
        (* re-establish PSInv *)
        iFrame (H5 H6 H7) "∗".
        by rewrite /HInv /= MOld NATSOld. }

    (* show post-condition *)
    iModIntro. iFrame "Hist' Seen'". repeat (iSplit; auto).
    + iPureIntro. subst. solve_jsl.
    + iPureIntro. exists (v, V). repeat (split; auto). subst. do 2 (etrans; eauto). by subst. solve_jsl.
    + iExists (mview m0). destruct (IMOk _ H1 _ In) as [EqT _]. subst.
      rewrite -EqLoc EqT. repeat iSplit; iPureIntro.
      * solve_jsl.
      * rewrite lookup_join_max EqT.
        apply (anti_symm extension); first by apply join_ext_r.
        apply join_least; last by reflexivity.
        by rewrite EqLoc Local0.
      * exists (mval m0, mview m0).
        assert (EqVT: Some (mtime m0) = Some (mtime m)).
        { rewrite -(_ : msg_ok m) -?HVl; last by apply H1.
          rewrite Local0.
          apply: anti_symm; eauto.
          destruct (IMOk _ H1 _ In) as [VT _].
          rewrite -VT -Local0 HVl EqLoc.
          apply (Max (mval m0, _)).
          rewrite Eqh2 elem_of_map_gset. exists m0.
          rewrite elem_of_hist_from. repeat (split; auto).
          change (Some t ⊑ Some (mtime m0)).
          rewrite -EqV0. etrans; first by apply Le0. by rewrite Local0. }
        assert (m0 = m).
          { destruct (memory_ok (mem ς) _ _ In Inm) as [|[|]] => //.
            exfalso. by inversion EqVT. }
        subst. by rewrite -EqVal.
Qed.
