From iris.program_logic Require Export weakestpre.
From iris.base_logic Require Import lib.invariants big_op.
From iris.proofmode Require Export tactics coq_tactics notation.
From iris.algebra Require Export auth frac excl gmap.

Import uPred.

From igps Require Export notation history proofmode.
From igps.base Require Export ghosts accessors helpers.

Lemma write_na_update_HInv `{fG : !foundationG Σ}
  ς ς' (h  h2: History) (l : loc) (m: message) HIST
  (HInvς: HInv ς HIST)
  (Disj : MS_msg_disj (msgs (mem ς)) m)
  (MUpd : mem ς' = add_ins (mem ς) m Disj)
  (EqLoc : mloc m = l)
  (NATSUpd: nats ς' = <[l := mtime m]> (nats ς))
  (Max: MaxTime (mem ς') l (Some (mtime m)))
  (MSOK: msgs_ok (mem ς'))
  : HInv ς' (<[l:=Excl {[mval m,mview m]}]> HIST).
Proof.
  split.
  - rewrite dom_insert (proj1 HInvς) MUpd /=.
    apply set_unfold_2; abstract naive_solver.
  - move => l0 V0.
    case: (decide (l = l0)) => [<- {l0}|?]; last first.
    + rewrite /LInv NATSUpd lookup_insert_ne; last auto.
      move/(proj2 HInvς) => [m' [? [? [? [H ?]]]]].
      exists m'. rewrite lookup_insert_ne; last auto.
      split; last split; last split; last split; auto.
      rewrite MUpd H. rewrite /map_vt /hist_from_opt. clear H.
      apply set_unfold_2. abstract naive_solver.
    + rewrite /LInv MUpd NATSUpd lookup_insert => [[<- {V0}]].
      exists (mval m, mview m).
      rewrite lookup_insert.
      setoid_rewrite elem_of_singleton.
      assert (Inm: m ∈ msgs $ mem ς').
        { rewrite MUpd elem_of_union elem_of_singleton. by right. }
      destruct (MSOK _ Inm) as [VT _].
      split; first auto. split ; last split; last split.
      * abstract naive_solver.
      * by rewrite -EqLoc VT.
      * apply set_unfold_2; split.
        { move => ?. exists m. rewrite -VT EqLoc. abstract naive_solver. }
        { move => [m' [Eq [Le [EqL [In'|<- //]]]]].
          assert (In'': m' ∈ msgs $ mem ς').
            { rewrite MUpd elem_of_union. by left. }
          assert (Ge := MT_Some_Lt Max _ In'' EqL).
          rewrite -EqLoc VT in Le. cbn in Le.
          assert (mtime m = mtime m'). { by apply: anti_symm. }
          rewrite (_: m = m'). exact Eq.
            { destruct (memory_ok (mem ς') _ _ Inm In'') as [|[]] => //.
              exfalso. rewrite -EqLoc in EqL. auto. } }
      * move => ? ?. abstract set_solver+.
Qed.