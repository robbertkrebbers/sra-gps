From igps.base Require Import na_shared.

Lemma f_fork `{fG : foundationG Σ} (Φ : val ra_lang → iProp _) (P: iProp _) π V e:
    PSCtx
    ∗ ▷ Seen π V ∗ ▷ P
    ∗ ▷ (∀ V', ⌜V ⊑ V'⌝ -∗ Seen π V' -∗ Φ (#(), V'))
    ∗ ▷ (∀ ρ V', ⌜V ⊑ V'⌝ -∗ PSCtx -∗ Seen ρ V' -∗ P -∗ WP (e, V') {{ _, True }})
  ⊢ WP (Fork e, V) @ ↑physN {{ Φ }}.
Proof.
  iIntros "[#? [>FV [HP [Post HFork]]]]".
  iInv physN as (ς VIEW HIST INFO)
              "[>HoP [>AV [>AH [>AI [>% [DInv [>% [>% >%]]]]]]]]" "HClose".

  iCombine "AV" "FV" as "HV".
  iDestruct (own_valid with "HV") as %?%Views_frag_valid_le.

  iDestruct "HV" as "[HV FV]".

  iApply wp_fork_pst => //; first by exact: H0.
  - iNext. iFrame "HoP".
    iIntros (ς' V') "(% & % & % & % & Oς')". inversion H7. subst.

    have: (∃ ρ : thread, VIEW !! ρ = None) => [|[ρ Hρ]].
    { exists (fresh (dom _ (VIEW))).
      by generalize (is_fresh (dom (gset positive) VIEW)) => /not_elem_of_dom. }

    iMod ((Views_alloc _ ρ) with "[$HV]") as "[AV' FV']"; first done.

    iSplitR "HP FV' HFork"; last by (iApply ("HFork" with "[//] [//] [$FV']")).

    do 2 iModIntro.
    iMod ("HClose" with "[-Post FV]"); last by iApply "Post".
    iNext. iExists ς, (<[ρ:=Excl V]> VIEW). iExists HIST, INFO.
    iFrame (H3) "∗".
    iSplit;[|iSplit]; iPureIntro; [|done|done].
    move => ? ?.
    by rewrite lookup_insert_Some => [] [[_ [<- //]]|[_ /H0 //]].
Qed.