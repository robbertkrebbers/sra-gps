From igps.base Require Import at_shared.

Lemma f_FAI `{fG : !foundationG Σ} C π V l h:
  {{{ PSCtx ∗ ▷ Seen π V ∗ ▷ Hist l h ∗ ⌜init_local h V⌝ }}}
    (FAI C #l, V) @ ↑physN
  {{{ (v: Z) V' h', RET (LitV $ LitInt v, V');
      ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h' ∗ ⌜init_local h' V'⌝
          ∗ ∃ (V1: View) (v': Z), ⌜V1 ⊑ V'⌝
            ∗ ⌜value_at h V1 (VInj v)⌝
            ∗ ⌜V !! l ⊑ V1 !! l⌝
            ∗ ⌜v' = (v + 1) `mod` Z.pos C⌝%Z
            ∗ ⌜value_at h' V' (VInj v')⌝
            ∗ ⌜h' ≡ {[VInj v', V']} ∪ h⌝
            ∗ ⌜h ⊥ {[VInj v', V']}⌝
            ∗ ⌜init l h'⌝
            ∗ ⌜adj_opt (V1 !! l) (V' !! l)⌝
            ∗ ⌜no_adj_right l h V1⌝ }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.

  destruct H3 as [HInv1 HInv2]. symmetry in H2.
  move: (HInv2 _ _ H2) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  destruct H as [vx [Vx [Inx [Localx IsValx]]]].

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }

  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from (mem ς) l t)).
    { move: Eqh. abstract set_solver+. }
  iApply (wp_FAI_pst with "oPS"); [done|done|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move: (Min0 _ Inx) => /= ->.
  - (* init_pre ς π l *)
    eapply init_pre_helper with (t:=t) (v:=vx) (V:=Vx); eauto.
    exists (v0, V0). split;[|split]; auto. by move => [].
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (v ς' V'') "(% & % & % & % & HP)".
    inversion H5.

    assert (V0 !! l ⊏ Some (mtime m')).
      { rewrite EqV0. cbn. rewrite EqTime'.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r. etrans; eauto.
        change (Some t ⊑ Some tl).
        rewrite -EqV0 -Local.
        by move : (Min0 (vx, Vx)) => /= ->. }

      (* get h' *)

      destruct ((write_at_update_hist _ _ V0 _ _ _ ((v + 1) `mod` Z.pos C)
                                      _ Eqh2 Disj MUpd EqLoc'))
         as [h' [Inh' [Eqh' [Eqh'2 Disjh']]]]; auto.
        by do 2 eexists. apply H0. apply H.

      assert (HInv': HInv ς' (<[l:=Excl h']> HIST)). {
        eapply (write_at_update_HInv ς _ V0 h h'); eauto.
        - done.
        - by rewrite EqVal'.
        - by do 2 eexists.
        - apply H. }

      assert (Eqtr: mview m !! l = Some tr).
        { destruct (IMOk _ H0 _ InR) as [VTEq _].
          by rewrite -EqLoc VTEq EqTime. }
      assert (t ⊑ mtime m).
        { rewrite EqTime. etrans; last by apply TimeLe.
          change (Some t ⊑ Some tl).
          rewrite -Local -EqV0.
          etrans; first by apply (Min0 _ Inx). by simpl. }
      assert (Inm': m' ∈ msgs (mem ς')).
        { rewrite MUpd elem_of_union elem_of_singleton. by right. }
      assert (EqVt: Some (mtime m') = V'' !! l).
        { destruct (IMOk _ H _ Inm') as [VTEq _].
          by rewrite -EqView' -EqLoc' VTEq. }

      iApply ("Post" $! _ _ h').

      iMod ("HClose" $! ς' _ h' with "[DInv $HP]") as "[Seen' Hist']".
        { (* re-establish PSInv *)
          iFrame (H H3 H4 HInv'). 
          iNext. iApply (DInv_update_value with "DInv"); last eauto.
          move => ? Eq2. move: Inh'.
          rewrite Eq2 elem_of_singleton. by inversion 1. }

      set v': Z := ((v + 1) `mod` Z.pos C)%Z.
      (* show post-condition *)
      iModIntro. iFrame "Seen' Hist'".
      repeat iSplit; first auto.
      + iPureIntro. rewrite VUpd. solve_jsl.
      + iPureIntro. exists (VInj v'), (mview m'). by rewrite {2}EqView'.
      + iExists (mview m), v'.
        rewrite -EqView'. iFrame ((Logic.eq_refl v') Eqh'2 Disjh' Inh').
        repeat (iSplit); iPureIntro.
        * rewrite EqView' VUpd. etrans; [done|apply join_ext_r].
        * rewrite -EqVal /value_at Eqh2 elem_of_map_gset.
          exists m. by rewrite elem_of_hist_from.
        * by rewrite Eqtr Local.
        * by do 2 eexists.
        * assert (EqL: (<[l:=Excl h']> HIST !! l = Excl' h')).
            { by rewrite lookup_insert. }
          move: (proj2 HInv' l h' EqL) => [vV [? [? [_ [_ IS]]]]].
          exists vV. repeat (split; auto).
          move => [? ?]. by apply IS. 
        * exists tr. by rewrite EqView' -EqVt EqTime'.
        * move => [[v2 V2] [In2 [t2 [Eq2 /= Eq2']]]].
          rewrite Eqtr in Eq2.
          inversion Eq2. subst t2. clear Eq2.
          move : In2. 
          rewrite Eqh2 elem_of_map_gset.
          move  => [m2 [[Eqv2 EqV2] /elem_of_hist_from [Eql2 [Inm2 _]]]].
          destruct (IMOk _ H0 _ Inm2) as [VT2 _].
          destruct (Disj _ Inm2) as [NEq|NEq]; apply NEq.
          { by rewrite EqLoc' Eql2. }
          { rewrite EqTime'. rewrite EqV2 -Eql2 VT2 in Eq2'.
            by inversion Eq2'. }
Qed.