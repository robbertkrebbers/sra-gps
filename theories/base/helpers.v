From iris.algebra Require Import excl.

From igps Require Import infrastructure machine history lifting.
From igps Require Export ghosts.

Set Bullet Behavior "Strict Subproofs".


Lemma init_pre_helper ς h t l V_l v V
  (Hinv: phys_inv ς)
  (HH : h ≡ map_vt (hist_from (mem ς) l t))
  (In: (v, V) ∈ h)
  (IsVal: isval v)
  (Local: V !! l ⊑ V_l !! l)
  (HVal : val_but_min l h)
  : init_pre ς V_l l.
Proof.
  assert ((v, V) ∈ h). { by apply In. }
  move: In. rewrite HH /map_vt elem_of_map_gset.
  move => [m [Hm1 /elem_of_hist_from [Hm2 [Hm3 Hm4]]]].
  inversion Hm1. subst.
  destruct (IMOk _ Hinv _ Hm3) as [VT _].
  rewrite /msg_ok in VT. rewrite VT in Local.
  destruct (V_l !! mloc m) as [tl|] eqn: HVT; last done.
  econstructor; eauto.
  apply (init_time_mono _ _ (mtime m)); [..|auto|by auto].
  assert (Alloc := IAlloc _ Hinv _ Hm3). inversion IsVal as [v0 VEq].
  rewrite /AllocInv -VEq in Alloc.
  eapply allocated_next_init; eauto.
  move => m' Inm' EqLoc TLt.
  destruct  (IMOk _ Hinv _ Inm') as [VT' _].
  assert (mview m !! mloc m ⊏ mview m' !! mloc m).
    { by rewrite VT -EqLoc VT'. }
  eapply hist_val_mono; eauto.
  rewrite HH /map_vt elem_of_map_gset.
  exists m'.
  rewrite /hist_from elem_of_filter elem_of_hist. repeat split; auto.
  eapply Pos.lt_le_incl, Pos.le_lt_trans; eauto.
Qed.

Lemma alloc_pre_helper ς h t l V_l
  (Hinv: phys_inv ς)
  (HH : h = map_vt (hist_from (mem ς) l t))
  (Alloc : alloc_local h V_l)
  (HVal : val_but_min l h)
  (MSOK: msgs_ok (mem ς))
  : alloc_pre ς V_l l.
Proof.
  destruct Alloc as [v [V [In [Local ND]]]].
  assert (In1: (v, V) ∈ h). { by apply In. }
  move: In. rewrite HH /map_vt elem_of_map_gset.
  move => [m [Hm1 /elem_of_hist_from [Hm2 [Hm3 Hm4]]]].
  inversion Hm1. subst.
  destruct (IMOk _ Hinv _ Hm3) as [VT _].
  rewrite /msg_ok in VT.
  destruct (V_l !! mloc m) as [tl|] eqn: HVT; last first.
  - specialize (Local (mloc m)). by rewrite HVT VT in Local.
  - econstructor; eauto.
    assert (Alloc := IAlloc _ Hinv _ Hm3). rewrite /AllocInv in Alloc.
    destruct (mval m) eqn: vEq => //; last first.
    + apply init_is_alloc.
      have Init: init_pre ς (mview m) (mloc m) by eapply init_pre_helper.
      inversion Init.
      apply: init_time_mono; first exact: Init0.
      have := (Local (mloc m)). by rewrite HVT Local0.
    + destruct HVal as [[v0 V0] [In0 [Min0 Val]]].
      assert (EqvV: (v0, V0) = (A, mview m)).
        { case (decide ((v0, V0) = (A, mview m))) => [//|NEq].
          assert (IS: isval ((A, mview m).1)).
            { apply Val. by rewrite elem_of_difference elem_of_singleton. }
          by inversion IS. }
      inversion EqvV. subst. clear EqvV.
      assert (HD: ∀ m',
                  (mval m', mview m') ∈ map_vt (hist_from (mem ς) (mloc m) t) 
                  → mval m' ≠ D).
        { move => m0.
          case (decide ((mval m0, mview m0) = (A, mview m))) => [[->] //|NEq].
          move => Inm0 EqD.
          assert (IS: isval D).
            { rewrite -EqD.
              apply (Val (mval m0, mview m0)).
              by rewrite elem_of_difference elem_of_singleton. }
          by inversion IS. }
      inversion Alloc. clear Alloc.
      eapply (is_allocated) with (t_d := t_d) (t_a := mtime m).
      * eapply (MT_impl TD).
        clear Val Min0 TA In0 In1 TD.
        apply set_unfold_2. move => m'. split; first by abstract naive_solver.
        move => [EqL [EqD In']]. repeat split; auto.
        case (decide (mtime m' < mtime m)%positive) => [//|Le].
        apply Pos.le_nlt in Le. exfalso. apply (HD m'); last auto.
        apply set_unfold_2. exists m'. repeat split; auto. by rewrite Hm4.
      * apply (MT_Some m); first auto.
        { rewrite elem_of_filter /=. by apply set_unfold_2. }
        { repeat setoid_rewrite elem_of_filter.
          move => m' [EqLoc [EqA Inm']] EqL.
          case (decide (mtime m' ≤ mtime m)%positive) => [//|Le].
          apply Pos.lt_nle in Le. exfalso.
          assert (IS: isval A).
          { rewrite -EqA. apply (Val (mval m', mview m')).
            rewrite elem_of_difference. split.
            - rewrite /map_vt elem_of_map_gset. exists m'.
                apply set_unfold_2. repeat split; auto.
                eapply  Pos.lt_le_incl, Pos.le_lt_trans; eauto.
              - rewrite elem_of_singleton. move => [Eqv EqV].
                destruct (IMOk _ Hinv _ Inm') as [VT' _].
                assert (Eqm: Some (mtime m) = Some (mtime m')).
                  { by rewrite -VT -VT' EqLoc EqV. }
                inversion Eqm as [Eqm2]. rewrite Eqm2 in Le.
                by eapply Pos.lt_irrefl. }
          by inversion IS. }
      * destruct t_d; last done.
        move: Local0. rewrite tight_extension; move => [Eq|//].
        rewrite Eq in TD. exfalso. move: (MT_msg_In TD).
        apply set_unfold_2 => [[_]]. rewrite MT_msg_time => [[H _]].
        by apply: (irreflexive_fw _ _ H).
      * specialize (Local (mloc m)). rewrite VT HVT in Local. by cbn in Local.
Qed.
