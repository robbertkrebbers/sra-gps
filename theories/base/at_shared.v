From iris.program_logic Require Export weakestpre.
From iris.base_logic Require Import lib.invariants big_op.
From iris.proofmode Require Export tactics.
From iris.algebra Require Export auth frac excl gmap.

Import uPred.

From igps Require Export notation.
From igps.base Require Export accessors helpers ghosts.

Lemma write_at_update_hist ς ς' V h t l v (m: message)
  (HOld: h ≡ map_vt (hist_from (mem ς) l t))
  (Disj : MS_msg_disj (msgs (mem ς)) m)
  (MUpd : mem ς' = add_ins (mem ς) m Disj)
  (EqLoc : mloc m = l)
  (Safe: ∃ v' V', (v', V') ∈ h ∧ V' !! l ⊑ V !! l)
  (TimeGt: V !! l ⊏ Some (mtime m))
  (EqVal: mval m = VInj v)
  (MSOK: msgs_ok (mem ς))
  (MSOK': msgs_ok (mem ς'))
  : ∃ (h': History),
        (VInj v, mview m) ∈ h'
        ∧ h' ≡ map_vt (hist_from (mem ς') l t)
        ∧ h' ≡ {[VInj v, mview m]} ∪ h
        ∧ h ⊥ {[VInj v, mview m]}.
Proof.
  destruct Safe as [v' [V_l [Hv'1 Hv'2]]].
  assert (t ⊑ mtime m).
    { move : Hv'1. rewrite HOld /map_vt elem_of_map_gset.
      move => [m' [Eq']].
      rewrite elem_of_filter elem_of_hist.
      move => [Le [EqL' In']].
      etrans; first by apply Le.
      change (Some (mtime m') ⊑ Some (mtime m)).
      destruct (MSOK _ In') as [VT' _].
      inversion Eq' as [[Eqv' EqV']].
      rewrite -VT' EqL' -EqV'.
      etrans; first by apply Hv'2.
      destruct (V !! l); last auto. by apply Pos.lt_le_incl. }
  assert (Inm: m ∈ msgs $ mem ς').
    { rewrite MUpd elem_of_union elem_of_singleton. by right. }
  exists (map_vt (hist_from (mem ς') l t)). split; last split; auto.
  - rewrite /map_vt -EqVal elem_of_map_gset.
    exists m.
    by rewrite elem_of_filter elem_of_hist.
  - rewrite -EqVal MUpd. split.
    + abstract set_solver+H EqLoc HOld.
    + apply disjoint_singleton_r. rewrite HOld elem_of_map_gset.
      move => [m' [[Eqv EqV] /elem_of_hist_from [Eq' [In' ?]]]].
      destruct (Disj _ In') as [NEqL|NEqT].
      * apply NEqL. by rewrite EqLoc.
      * destruct (MSOK _ In') as [VT' _].
        destruct (MSOK' _ Inm) as [VT _].
        apply NEqT.
        assert (Eqm: Some (mtime m) = Some (mtime m')).
        { by rewrite -VT -VT' EqV Eq' EqLoc. }
          by inversion Eqm.
Qed.

Lemma write_at_update_HInv `{fG : !foundationG Σ}
  ς ς' V (h h': History) (l : loc) v (m: message) HIST
  (HInvς: HInv ς HIST)
  (HH: HIST !! l = Excl' h)
  (HUpd: h' ≡ {[mval m, mview m]} ∪ h)
  (Disj : MS_msg_disj (msgs (mem ς)) m)
  (MUpd : mem ς' = add_ins (mem ς) m Disj)
  (EqLoc : mloc m = l)
  (EqVal : mval m = VInj v)
  (Safe: ∃ v' V', (v', V') ∈ h ∧ V' !! l ⊑ V !! l)
  (TimeGt: V !! l  ⊏ Some (mtime m))
  (NATSOld: nats ς' = nats ς)
  (MSOK': msgs_ok (mem ς'))
  : HInv ς' (<[l:=Excl h']> HIST).
Proof.
  split.
  - rewrite dom_insert (proj1 HInvς) MUpd /=.
    apply set_unfold_2; abstract naive_solver.
  - move => l0 V0.
    case: (decide (l = l0)) => [<- {l0}|?]; last first.
    + rewrite /LInv NATSOld lookup_insert_ne; last auto.
      move/(proj2 HInvς) => [m' [? [? [? [H ?]]]]].
      exists m'. do 4!(try split); auto.
      rewrite MUpd H. rewrite /map_vt /hist_from_opt.
      apply set_unfold_2. abstract naive_solver.
    + rewrite /LInv MUpd NATSOld lookup_insert => [[<- {V0}]].
      move: (proj2 HInvς _ _ HH) => [[v1 V1] [? [Min1 [? [H IS]]]]].
      assert (Inm: m ∈ msgs $ mem ς').
        { rewrite MUpd elem_of_union elem_of_singleton. by right. }
      assert (V1 !! l ⊑ Some (mtime m)).
        { destruct Safe as [v' [V' [In' Le']]].
          etrans; first by apply (Min1 _ In').
          etrans; first by apply Le'.
          destruct (V !! l); last auto.
          by apply Pos.lt_le_incl. }
      exists (v1, V1). split; last split; last split; last split; auto.
      * rewrite HUpd elem_of_union. by right.
      * move => [v0 V0]. rewrite HUpd elem_of_union elem_of_singleton.
        move => [[-> ->] /=|]; last by auto.
        destruct (MSOK' _ Inm) as [VT _]. by rewrite -{2}EqLoc VT.
      * rewrite HUpd. clear Min1 IS HUpd MSOK' Safe.
        abstract set_solver+H H0 EqLoc.
      * move => v0 V0. rewrite HUpd.
        rewrite elem_of_difference elem_of_union ?elem_of_singleton EqVal.
        move => [[[-> //]|?] ?]. apply (IS v0 V0).
        by rewrite elem_of_difference elem_of_singleton.
Qed.