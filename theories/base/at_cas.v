From igps.base Require Import at_shared.

Lemma f_CAS `{fG : !foundationG Σ} π V l h (v_r v_w: Z):
  {{{ PSCtx ∗ ▷ Seen π V ∗ ▷ Hist l h ∗ ⌜init_local h V⌝ }}}
    (CAS #l #v_r #v_w, V) @ ↑physN
  {{{ b V' h', RET (LitV $ LitInt b, V');
      ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h' ∗ ⌜init_local h' V'⌝
      ∗ ∃ (V1: View) v, ⌜V1 ⊑ V'⌝
        ∗ ⌜value_at h V1 (VInj v)⌝
        ∗ ⌜V !! l ⊑ V1 !! l⌝
        ∗ ( ⌜b = true⌝  ∗ ⌜v = v_r⌝
                      ∗ ⌜value_at h' V' (VInj v_w)⌝
                      ∗ ⌜h' ≡ {[VInj v_w, V']} ∪ h⌝
                      ∗ ⌜h ⊥ {[VInj v_w, V']}⌝
                      ∗ ⌜init l h'⌝
                      ∗ ⌜adj_opt (V1 !! l) (V' !! l)⌝
                      ∗ ⌜no_adj_right l h V1⌝
          ∨ ⌜b = false⌝ ∗ ⌜(v ≠ v_r)⌝ ∗ ⌜h' = h⌝ ∗ ⌜V1 !! l = V' !! l⌝) }}}.
Proof.
  iIntros (Φ) "(#I & Seen & Hist & %) Post".

  (* open invariant *)
  iMod (PSInv_open_VH with "[$I $Seen $Hist]")
    as (ς HIST) "(oPS & % & % & % & DInv & % & HClose)"; first auto.


  destruct H3 as [HInv1 HInv2]. symmetry in H2.
  move: (HInv2 _ _ H2) => [[v0 V0] [In0 [Min0 [/= NA0 [Eqh Val]]]]].

  destruct H as [v' [V' [In' [Local' IsVal']]]].

  assert (∃ t, V0 !! l = Some t) as [t EqV0].
    { apply (elem_of_hist_view_ok v0 _ (mem ς) _ (V0 !! l)).
      - apply H0.
      - by rewrite /map_vt -Eqh. }

  rewrite EqV0 in Eqh.
  assert (Eqh2: h ≡ map_vt (hist_from (mem ς) l t)).
    { move: Eqh. abstract set_solver+. }
  iApply (wp_CAS_pst with "oPS"); [done|done|..].
  - (* drf_pre ς π l *)
    econstructor; eauto. rewrite NA0. by move: (Min0 _ In') => /= ->.
  - (* init_pre ς π l *)
    eapply init_pre_helper with (t:=t) (v:=v') (V:=V'); eauto.
    exists (v0, V0). split;[|split]; auto. by move => [].
  - (* frame update and re-establish the PSInv *)
    iNext. iIntros (b ς' V'') "(% & % & % & % & HP)".

    destruct H5 as [[bTrue CPost]|[bFalse CPost]]; inversion CPost.
    { (* CAS succeeds *)
      assert (V0 !! l ⊏ Some (mtime m')).
      { rewrite EqV0. cbn. rewrite EqTime'.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        etrans; eauto.
        change (Some t ⊑ Some tl).
        rewrite -EqV0 -Local.
        by move: (Min0 (v', V')) => /= ->. }

      (* get h' *)

      destruct ((write_at_update_hist _ _ V0 _ _ _ v_w _ Eqh2 Disj MUpd EqLoc'))
         as [h' [Inh' [Eqh' [Eqh'2 Disjh']]]]; auto.
        by do 2 eexists. apply H0. apply H.

      assert (HInv': HInv ς' (<[l:=Excl h']> HIST)). {
        eapply (write_at_update_HInv ς _ V0 h h'); eauto.
        - done.
        - by rewrite EqVal'.
        - by eexists _, _.
        - apply H.
      }

      assert (Eqtr: mview m !! l = Some tr).
        { destruct (IMOk _ H0 _ InR) as [VTEq _].
            by rewrite -EqLoc VTEq EqTime. }
      assert (t ⊑ mtime m).
        { rewrite EqTime. rewrite -TimeLe.
          change (Some t ⊑ Some tl).
          rewrite -Local -EqV0.
          etrans; first by apply (Min0 _ In'). by simpl. }
      assert (Inm': m' ∈ msgs (mem ς')).
        { rewrite MUpd elem_of_union elem_of_singleton. by right. }
      assert (EqVt: Some (mtime m') = V'' !! l).
        { destruct (IMOk _ H _ Inm') as [VTEq _].
          by rewrite -EqView' -EqLoc' VTEq. }

      iApply ("Post" $! b _ h').

      iMod ("HClose" $! ς' _ h' with "[DInv $HP]") as "[Seen' Hist']".
        { (* re-establish PSInv *)
          iFrame (H H3 H4 HInv'). 
          iNext. iApply (DInv_update_value with "DInv"); last eauto.
            move => ? Eq2. move: Inh'.
            rewrite Eq2 elem_of_singleton. by inversion 1. }

      (* show post-condition *)
      iModIntro. iFrame "Hist' Seen'".
      repeat (iSplit; first auto).
      - iPureIntro. rewrite VUpd. solve_jsl.
      - iPureIntro. exists (VInj v_w), (mview m'). repeat (split; auto).
        by rewrite EqView'.
      - iExists (mview m), v_r. repeat (iSplit).
        + iPureIntro. rewrite VUpd. solve_jsl.
        + iPureIntro. rewrite -EqVal /value_at.
          rewrite Eqh2 elem_of_map_gset. exists m. split; auto.
          by apply elem_of_hist_from.
        + iPureIntro. by rewrite Eqtr Local.
        + iLeft. rewrite -EqView'.
          iFrame (bTrue (Logic.eq_refl v_r) Eqh'2 Disjh' Inh').
          iSplit; [|iSplit]; iPureIntro.
          * split; first by do 2 eexists.
            assert (EqL: (<[l:=Excl h']> HIST !! l = Excl' h')).
              { by rewrite lookup_insert. }
            move: (proj2 HInv' l h' EqL) => [vV [? [? [_ [_ IS]]]]].
            exists vV. repeat (split; auto). move => [? ?]. by apply IS.
          * exists tr. split; last by rewrite EqView' -EqVt EqTime'. auto.
          * move => [[v2 V2] [In2 [t2 [Eq2 /= Eq2']]]].
            rewrite Eqtr in Eq2. inversion Eq2. subst t2. clear Eq2.
            move : In2. rewrite Eqh2 elem_of_map_gset.
            move  => [m2 [[Eqv2 EqV2] /elem_of_hist_from [Eql2 [Inm2 _]]]].
            destruct (IMOk _ H0 _ Inm2) as [VT2 _].
            destruct (Disj _ Inm2) as [NEq|NEq]; apply NEq.
              { by rewrite EqLoc' Eql2. }
              { rewrite EqTime'. rewrite EqV2 -Eql2 VT2 in Eq2'.
                by inversion Eq2'. } }

    { (* CAS fails *)

      iApply ("Post" $! _ _ h).

      iMod ("HClose" $! ς' _ h with "[DInv $HP]") as "[Seen' Hist]".
        { rewrite (insert_id HIST); last auto.
          (* re-establish PSInv *)
          iFrame (H H3 H4) "∗".
          by rewrite /HInv /= MOld NATSOld. }

      assert (Eqtr: mview m !! l = Some (mtime m)).
        { destruct (IMOk _ H0 _ InR) as [VTEq _].
            by rewrite -EqLoc VTEq. }

      assert (V !! l ⊑ Some (mtime m)).
        { by rewrite Local. }

      assert ((VInj v, mview m) ∈ h).
        { rewrite -EqVal Eqh2 elem_of_map_gset. exists m. split; auto.
          apply elem_of_hist_from. repeat (split; auto).
          etrans; last by apply TimeLe.
          change (Some t ⊑ Some t0).
          rewrite -Local -EqV0.
          etrans; first by apply (Min0 _ In'). by simpl. }

      (* show post-condition *)
      iModIntro. iFrame "Hist Seen'".
      repeat iSplit.
      - iPureIntro. rewrite VUpd. solve_jsl.
      - iPureIntro. exists (VInj v), (mview m). repeat (split; auto).
        rewrite VUpd. solve_jsl.
      - iExists (mview m), v. repeat iSplit; auto.
        + iPureIntro. rewrite VUpd. solve_jsl.
        + by rewrite Eqtr Local.
        + iRight. repeat (iSplit; auto). iPureIntro.
          rewrite VUpd lookup_join_max. apply: (anti_symm (⊑)); [solve_jsl|].
          rewrite Local Eqtr. solve_jsl. }
Qed.