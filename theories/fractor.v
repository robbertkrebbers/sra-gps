From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth frac excl gmap .
From iris.base_logic Require Import big_op.
From iris.base_logic.lib Require Export cancelable_invariants.

Import uPred.

From igps Require Import lang.
From igps.base Require Import accessors.

Definition fracN : namespace := nroot .@ "fractor".

Section Fractor.
  Context `{!foundationG Σ, !cinvG Σ}.

  Implicit Types (l: loc) (φ: loc → InfoT → iProp Σ)
                          (Ψ: loc → frac → InfoT -> iProp Σ).

  Definition fractored l φ Ψ p: iProp Σ :=
    (∃ X, Ψ l p X
      ∗ ∃ γ, Info l p (DecAgree (encode (X,γ)))
      ∗ cinv_own γ p ∗ cinv (fracN .@ l) γ (φ l X))%I.

  Lemma fractor_mono l φ Ψ Ψ' p:
      (∀ X, Ψ l p X → Ψ' l p X) ∗ fractored l φ Ψ p ⊢ fractored l φ Ψ' p.
  Proof.
    iIntros "[Imp Frac]".
    iDestruct "Frac" as (X) "[Pred Inv]".
    iExists X. iFrame "Inv".
    by iApply "Imp".
  Qed.

  Lemma fractor_drop l φ Ψ p:
      fractored l φ Ψ p ⊢ ∃ X, Ψ l p X.
  Proof.
    iIntros "Frac". iDestruct "Frac" as (X) "[Frac _]".
    by iExists X.
  Qed.

  Lemma Info_join l p1 p2 v1 v2:
    Info l p1 (DecAgree v1) ∗ Info l p2 (DecAgree v2)
    ⊢ ⌜v1 = v2⌝ ∗ Info l (p1 + p2) (DecAgree v1).
  Proof.
    iIntros "[Info1 Info2]".
    iCombine "Info1" "Info2" as "Info".
    rewrite op_singleton pair_op.
    iDestruct (own_valid with "Info") as %Valid.
    apply singleton_valid in Valid.
    destruct Valid as [_ Valid2]. simpl in Valid2.
    apply dec_agree_op_inv in Valid2. inversion Valid2 as [Eq].
    iSplitR "Info"; auto.
    by rewrite dec_agree_idemp.
  Qed.

  Lemma fractor_splitjoin l φ Ψ1 Ψ2 p1 p2:
      fractored l φ Ψ1 p1 ∗ fractored l φ Ψ2 p2
      ⊣⊢ fractored l φ (λ l p X, Ψ1 l p1 X ∗ Ψ2 l p2 X) (p1 + p2).
  Proof.
    iSplit.
    - iIntros "[Frac1 Frac2]".
      iDestruct "Frac1" as (X1) "[Frac1 Inv1]".
      iDestruct "Inv1" as (γ1) "[Info1 [FOwn1 #FCtx1]]".
      iDestruct "Frac2" as (X2) "[Frac2 Inv2]".
      iDestruct "Inv2" as (γ2) "[Info2 [FOwn2 #FCtx2]]".
      iPoseProof (Info_join with "[Info1 Info2]") as "Info".
        { iSplitL "Info1"; auto. }
      iDestruct "Info" as "[Eq Info]". iDestruct "Eq" as %Eq%encode_inj.
      inversion Eq. subst.
      iExists X2. iFrame. iExists γ2.
      iCombine "FOwn1" "FOwn2" as "FOwn".
      rewrite fractional. by iFrame.
    - iIntros "Frac".
      iDestruct "Frac" as (X) "[[Pred1 Pred2] Frac]".
      iDestruct "Frac" as (γ) "[Info [FOwn #FCtx]]".
      rewrite fractional /Info.
      rewrite -(dec_agree_idemp (DecAgree (encode (X,γ)))).
      rewrite -pair_op -op_singleton auth_frag_op.
      iDestruct "Info" as "[Info1 Info2]".
      iDestruct "FOwn" as "[FOwn1 FOwn2]".
      iSplitL "Pred1 Info1 FOwn1".
      + iExists X. iFrame. iExists γ. iFrame. auto.
      + iExists X. iFrame. iExists γ. iFrame. auto.
  Qed.

  Lemma fractor_join_l l φ1 φ2 Ψ1 Ψ2 p1 p2:
      fractored l φ1 Ψ1 p1 ∗ fractored l φ2 Ψ2 p2
      ⊢ fractored l φ1 (λ l p X, Ψ1 l p1 X ∗ Ψ2 l p2 X) (p1 + p2).
  Proof.
    iIntros "[Frac1 Frac2]".
    iDestruct "Frac1" as (X1) "[Frac1 Inv1]".
    iDestruct "Inv1" as (γ1) "[Info1 [FOwn1 #FCtx1]]".
    iDestruct "Frac2" as (X2) "[Frac2 Inv2]".
    iDestruct "Inv2" as (γ2) "[Info2 [FOwn2 #FCtx2]]".
    iPoseProof (Info_join with "[Info1 Info2]") as "Info".
        { iSplitL "Info1"; auto. }
      iDestruct "Info" as "[Eq Info]". iDestruct "Eq" as %Eq%encode_inj.
      inversion Eq. subst.
    iCombine "FOwn1" "FOwn2" as "FOwn". rewrite fractional.
    iExists X2. iFrame. iExists γ2. rewrite fractional. by iFrame.
  Qed.

  Lemma fractor_join_r l φ1 φ2 Ψ1 Ψ2 p1 p2:
    fractored l φ1 Ψ1 p1 ∗ fractored l φ2 Ψ2 p2
    ⊢ fractored l φ2 (λ l p X, Ψ1 l p1 X ∗ Ψ2 l p2 X) (p1 + p2).
  Proof.
    iIntros "[Frac1 Frac2]".
    iDestruct "Frac1" as (X1) "[Frac1 Inv1]".
    iDestruct "Inv1" as (γ1) "[Info1 [FOwn1 #FCtx1]]".
    iDestruct "Frac2" as (X2) "[Frac2 Inv2]".
    iDestruct "Inv2" as (γ2) "[Info2 [FOwn2 #FCtx2]]".
    iPoseProof (Info_join with "[Info1 Info2]") as "Info".
        { iSplitL "Info1"; auto. }
      iDestruct "Info" as "[Eq Info]". iDestruct "Eq" as %Eq%encode_inj.
      inversion Eq. subst.
    iCombine "FOwn1" "FOwn2" as "FOwn". rewrite fractional.
    iExists X2. iFrame "Frac1 Frac2". iExists γ2. rewrite fractional. by iFrame.
  Qed.


  Lemma fractor_join_later_rl l φ1 φ2 Ψ1 Ψ2 p1 p2 E:
      fractored l φ1 Ψ1 p1 ∗ ▷ fractored l φ2 Ψ2 p2
      ={E}=∗ fractored l φ1 (λ l p X, Ψ1 l p1 X ∗ ▷ Ψ2 l p2 X) (p1 + p2).
    Proof.
      iIntros "[Frac1 Frac2]".
      iDestruct "Frac1" as (X1) "[Frac1 Inv1]".
      iDestruct "Inv1" as (γ1) "[Info1 [FOwn1 #FCtx1]]".
      iDestruct "Frac2" as (X2) "[Frac2 Inv2]".
      iDestruct "Inv2" as (γ2) "[>Info2 [>FOwn2 #FCtx2]]".
      iPoseProof (Info_join with "[Info1 Info2]") as "Info".
        { iSplitL "Info1"; auto. }
      iDestruct "Info" as "[Eq Info]". iDestruct "Eq" as %Eq%encode_inj.
      inversion Eq. subst.
      iCombine "FOwn1" "FOwn2" as "FOwn". rewrite fractional.
      iModIntro. iExists X2. iFrame "Frac1 Frac2". iExists γ2.
      rewrite fractional. by iFrame.
    Qed.

  Lemma fractor_alloc (E : coPset) v X l φ Ψ:
    ↑physN ⊆ E → 
    PSCtx ∗ Info l 1 v ∗ ▷ φ l X ∗ Ψ l 1%Qp X
      ={E}=∗ fractored l φ Ψ 1.
  Proof.
    intros Sub.
    iIntros "[#PS [Info [Inv Pred]]]".
    iMod ((cinv_alloc E (fracN .@ l) (φ l X)) with "Inv") as (γ) "[FOwn Ctx]".
    iAssert (|={E}=> Info l 1 (DecAgree (encode (X,γ))))%I
      with "[Info]" as ">Info".
      { iApply PSInv_Info_update => //; auto. }
    iModIntro.
    iExists X. iFrame "Pred". iExists γ. by iFrame.
  Qed.

  Lemma fractor_dealloc (E : coPset) l φ Ψ:
    ↑fracN .@ l ⊆ E →
    True ⊢ fractored l φ Ψ 1 ={E}=∗ ∃ X v, Info l 1 v ∗ Ψ l 1%Qp X ∗ ▷ φ l X.
  Proof.
    intros Sub.
    iIntros "Frac". iDestruct "Frac" as (X) "[Pred Frac]".
    iDestruct "Frac" as (γ) "[Info [FOwn #Ctx]]".
    iMod ((cinv_cancel _ _ _ _ Sub) with "Ctx FOwn") as "Inv".
    iModIntro.
    iExists X, (DecAgree (encode (X,γ))). by iFrame.
  Qed.

  Lemma fractor_open' E l φ Ψ p:
    ↑ fracN .@ l ⊆ E →
    True ⊢
      fractored l φ Ψ p ={E,E∖↑fracN .@ l}=∗
      ∃ X, ▷ φ l X ∗ Ψ l p X
             ∗ (∀ Ψ', Ψ' l p X -∗ fractored l φ Ψ' p)
             ∗ (▷ φ l X ={E∖↑fracN .@ l, E}=∗ True).
  Proof.
    intros Sub.
    iIntros "Frac". iDestruct "Frac" as (X) "[Pred Frac]".
    iDestruct "Frac" as (γ) "[Info [FOwn #Ctx]]".
    iMod ((cinv_open _ _ _ _ _ Sub) with "Ctx FOwn") as "[Inv [FOwn Hclose]]".
    iModIntro. iExists X. iFrame.
    iIntros (Ψ') "Pred".
    iExists X. iFrame. iExists γ. by iFrame.
  Qed.

  Lemma fractor_open E l φ Ψ p:
    ↑ fracN .@ l ⊆ E →
    True ⊢
      fractored l φ Ψ p ={E,E∖↑fracN .@ l}=∗
      ∃ X, ▷ φ l X ∗ Ψ l p X
           ∗ ∀ Ψ',  ▷ φ l X ∗ Ψ' l p X ={E∖↑fracN .@ l,E}=∗ fractored l φ Ψ' p.
  Proof.
    intros Sub.
    iIntros "Frac".
    iMod (fractor_open' with "Frac") as (X) "(Inv & Pred & HT & HClose)"; [done|].
    iModIntro. iExists X. iFrame. iIntros (Ψ') "[Inv Pred]".
    iMod ("HClose" with "Inv"). by iApply "HT".
  Qed.

End Fractor.
