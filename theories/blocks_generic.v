Require Import Setoid.
From mathcomp Require Import ssreflect.
From stdpp Require Import gmap.
From iris.base_logic Require Import big_op lib.iprop.
From iris.proofmode Require Import tactics.
From igps Require Import infrastructure.

Section Blocks.
  Context `{Count : Countable A} {R : relation A}
          {DecR : ∀ a b, Decision (R a b)}.

  Definition part_funct (S : gset A) (R : relation A) :=
    ∀ a b1 b2, b1 ∈ S -> R a b1 -> R a b2 -> b1 = b2 ∨ b2 ∉ S.

  Implicit Type (a b : A) (S B : gset A).

  Definition block_ends S := {[ a <- S | set_Forall (λ b, b ≠ a → ¬ (R a b)) S ]}.
  Arguments block_ends _ /.
  Instance block_ends_Proper : Proper ((≡) ==> (≡)) block_ends.
  Proof. solve_proper. Qed.

  Lemma block_ends_singleton a : block_ends {[a]} ≡ {[a]}.
  Proof.
    move => ?.
    rewrite /block_ends elem_of_filter 
            set_Forall_singleton elem_of_singleton (comm (∧)).
    split => [[->]//|->]; last by split.
  Qed.

  Lemma block_ends_ins_mono a S: 
    block_ends ({[ a ]} ∪ S) ⊆ {[ a ]} ∪ block_ends S.
  Proof.
    apply set_unfold_2. move => b [Hyp [Eq|In]].
    - subst b. by left.
    - right. split; last exact In.
      move => c Inc. apply Hyp. by apply elem_of_union_r.
  Qed.

  Inductive block_ends_ins_spec S a B B' : Prop :=
  | be_ins_nL_sR (* prepend a block *)
                 (nL: set_Forall (not ∘ (flip R a)) S)
                 (sR: set_Exists (R a) S)
                 (BE: B' ≡ B)
    : block_ends_ins_spec S a B B'
  | be_ins_sL_nR b (* extend a block *)
                 (BE: B' ≡  {[a]} ∪ B ∖ {[b]})
                 (sL: R b a)
                 (nR: set_Forall (not ∘ (R a)) S)
                 (In: b ∈ S)
    : block_ends_ins_spec S a B B'
  | be_ins_nL_nR (* fresh block *)
                 (BE: B' ≡ {[a]} ∪ B)
                 (nL: set_Forall (not ∘ (flip R a)) S)
                 (nR: set_Forall (not ∘ (R a)) S)
    : block_ends_ins_spec S a B B'
  | be_ins_sL_sR b (* connect 2 blocks *)
                 (BE: B' ≡ B ∖ {[b]})
                 (sL: R b a)
                 (sR: set_Exists (R a) S)
                 (In: b ∈ S)
    : block_ends_ins_spec S a B B'.

  Instance block_ends_ins_Proper :
    Proper ((≡) ==> (=) ==> (≡) ==> (≡) ==> impl) block_ends_ins_spec.
  Proof.
    move => ? ? H1 ? ? <- ? ? H3 x y H4; induction 1;
            [constructor 1|econstructor 2|econstructor 3|econstructor 4];
            (try (by apply: set_Forall_Proper; first eassumption));
            (try (by apply: set_Exists_Proper; first eassumption));
             try (by rewrite -H4 -H3); try done; by rewrite -H1.
  Qed.

  Lemma block_ends_ins_update {Σ} (Ψ: A → iProp Σ) a S
   (NIn: a ∉ S)
   (B: block_ends_ins_spec S a (block_ends S) (block_ends ({[a]} ∪ S)))
    : Ψ a ∗ ([∗ set] b ∈ block_ends S, Ψ b)
      ⊢ ([∗ set] b ∈ block_ends ({[a]} ∪ S), Ψ b).
  Proof.
    rewrite -big_sepS_insert; last first.
    - move => In.
      apply (elem_of_subseteq (block_ends S) S) in In => //.
      by apply subseteq_gset_filter.
    - apply big_sepS_mono => //.
      inversion B.
      + rewrite BE. apply union_subseteq_r.
      + rewrite BE.
        apply union_mono_l, gset_difference_subseteq.
      + by rewrite BE.
      + rewrite BE. etrans; last by apply union_subseteq_r.
        apply gset_difference_subseteq.
  Qed.

  Lemma block_ends_ins S a
        (PF : part_funct S R)
        (PFf : part_funct S (flip R))
    : a ∉ S → block_ends_ins_spec S a (block_ends S) (block_ends ({[a]} ∪ S)).
  Proof.
    move => NIn.
    case: (decide (set_Exists (R a) S));
    case: (decide (set_Exists (flip R $ a) S)).
    - move => [b1 [In1 HR1]] [b2 [In2 HR2]].
      econstructor 4; [|eassumption|by eexists|by assumption].
      move => b /=. apply set_unfold_2.
      split.
      + move => [FA [?|]]; first subst.
        * exfalso. apply: FA; last eauto;
            [abstract set_solver+In2|abstract set_solver+In2 NIn].
        * split.
          { split => [|//]. exact: set_Forall_union_inv_2. }
          { move => ?; subst. 
            apply: FA; last eauto; 
              [abstract set_solver+|abstract set_solver+In1 NIn]. }
      + move => [[FA In] NEq]; split; last auto.
        apply: set_Forall_union; last auto. apply set_Forall_singleton => ?.
        move => HR. edestruct (PFf _ _ _ In1 HR1 HR); auto.
    - move/gset_neg_Exists => FA EX.
      constructor 1 => //. move => x /=. apply set_unfold_2.
      split => [[FA' [?|In]]|[FA' In]]; subst.
      * exfalso. move: EX => [? [In Ra]]. apply: FA'; last eauto.
        abstract set_solver+In. abstract set_solver+In NIn.
      * split => [|//]. exact: set_Forall_union_inv_2.
      * split; last auto. apply: set_Forall_union; last auto.
        apply set_Forall_singleton => _. by eapply FA.
    - move => [b1 [In1 HR1]] /gset_neg_Exists FA.
      econstructor 2 => //. move => x /=. apply set_unfold_2.
      split => [[FA' [?|In]]|[?|[[FA' In] NEq]]]; subst.
      * by left.
      * right. split; [split; last auto|].
        { exact: set_Forall_union_inv_2. }
        { move => ?; subst. apply: FA'; last eauto.
          abstract set_solver+. abstract set_solver+In NIn. }
      * split; last auto. apply: set_Forall_union.
        { apply set_Forall_singleton => _ //. }
        { apply: set_Forall_impl; first eassumption. auto. }
      * split; last auto. apply: set_Forall_union; last auto.
        apply set_Forall_singleton => ? HR.
        edestruct (PFf _ _ _ In1 HR1 HR); auto.
    - move => /gset_neg_Exists FA1 /gset_neg_Exists FA2.
      constructor 3 => // x. apply set_unfold_2.
      split => [[FA [?|In]]|[Eq|[FA In]]]; subst.
      * by left.
      * right; split; last auto. exact: set_Forall_union_inv_2.
      * split; last auto. apply: set_Forall_union; last auto.
        { apply set_Forall_singleton => ? //. }
        { apply: set_Forall_impl; first eassumption. auto. }
      * split; last auto. apply: set_Forall_union => //.
        apply set_Forall_singleton => _. by eapply FA1.
  Qed.

End Blocks.
Arguments block_ends [_ _ _] R [_] _.
Arguments block_ends_ins_spec [_ _ _] R _ _ _ _.
Arguments block_ends_ins [_ _ _ _ _] _ _ [_ _] _.

Local Instance gset_Forall_SetUnfold `{Countable A} P (S : gset A) :
  SetUnfold (set_Forall P S) (∀ x : A, x ∈ S → P x) | 0.
Proof. by constructor. Qed.

Lemma block_ends_fmap `{Countable A} `{Countable B}
      (f : A -> B)
      (R : relation A) (R' : relation B)
      (S : gset A)
      (RInj : ∀ a a', a ∈ S → a' ∈ S → R' (f a) (f a') ↔ R a a')
      (FInj : ∀ a a', a ∈ S → a' ∈ S → f a = f a' ↔ a = a')
      `{∀ a1 a2, Decision (R a1 a2)}
      `{∀ b1 b2, Decision (R' b1 b2)} :
  block_ends R' (map_gset f S) ≡ map_gset f (block_ends R S).
Proof.
  repeat apply set_unfold_2. setoid_rewrite elem_of_map_gset => b. split.
  - move => [HR [a [E ?]]]. rewrite E. exists a; (repeat split; auto).
    move => a' Ina' NEqa' HRaa'.
    apply: HR.
    + exists a'; by auto.
    + rewrite E. exact/FInj.
    + rewrite E. exact/RInj.
  - move => [a [->{b} [HR Ina]]]. split; eauto.
    move => b [a' [->{b} Ina' NEqa]] HRfa.
    apply: HR; first eauto.
    + exact/FInj.
    + exact/RInj.
Qed.


(* Currently not used *)
(* Lemma block_ends_ins_fmap `{Countable A} `{Countable B}
      (f : A -> B)
      (R : relation A) (R' : relation B)
      (S S1 S2 : gset A) a
      (Sub1 : S1 ⊆ S) 
      (Sub2 : S2 ⊆ {[a]} ∪ S) 
      (RInj : ∀ b b', b ∈ {[a]} ∪ S → b' ∈ {[a]} ∪ S → R' (f b) (f b') ↔ R b b')
      (FInj : ∀ b b', b ∈ {[a]} ∪ S → b' ∈ {[a]} ∪ S → f b = f b' ↔ b = b')
      `{∀ a1 a2, Decision (R a1 a2)}
      `{∀ b1 b2, Decision (R' b1 b2)} :
  block_ends_ins_spec R' (map_gset f S) (f a) (map_gset f S1) (map_gset f S2)
  → block_ends_ins_spec R S a S1 S2.
Proof.
  inversion_clear 1.
  - constructor 1.
    + move => x In.
      apply/RInj; [by apply elem_of_union_r
                  |by apply elem_of_union_l, elem_of_singleton
                  |apply: nL;set_solver+In].
    + destruct sR as [? [? ?]]. (* set_unfold. *)
      apply elem_of_map_gset in H3 as [y [Hy Iny]].
      exists y. split; first auto.
      apply/RInj; [by apply elem_of_union_l, elem_of_singleton
                  |by apply elem_of_union_r
                  |by subst].
    + move => b. specialize (BE (f b)). destruct BE as [BE1 BE2]. split => In.
      * move: BE1. apply set_unfold_2 => BE1.
        destruct BE1 as [? [Eq In']]; first by eauto.
        apply FInj in Eq; [by subst|auto|set_solver+In' Sub1].
      * move: BE2. apply set_unfold_2 => BE2.
        destruct BE2 as [? [Eq In']]; first by eauto.
        apply FInj in Eq; [by subst|set_solver+In Sub1|auto].
  - apply elem_of_map_gset in In as [a' [? ?]]. subst.
    apply (be_ins_sL_nR _ a _ _ a'); last auto.
    + move => x.
      move : (BE (f x)) => [].
      apply set_unfold_2 => BE1 BE2. split => [In|[Eqxa|[In Neqxa]]].
      * destruct BE1 as [|[[y [Eq ?]] ?]]; first by eauto.
        { left. apply FInj; auto. by apply elem_of_union_l, elem_of_singleton. }
        { right; split => //.
          - apply FInj in Eq; subst; auto. set_solver+H3 Sub1.
          - apply/FInj; auto. set_solver+H4 Sub1. }
      * subst. destruct BE2 as [y [Eq ?]]; first by left.
        apply FInj in Eq; auto. by subst. set_solver+.
      * destruct BE2 as [y [Eq ?]].
        { right. split; eauto. apply/FInj; subst; auto.
          set_solver+In Sub1. set_solver+H4. }
        { apply FInj in Eq; auto. by subst. set_solver+In Sub1. }
    + apply RInj; auto. set_solver+H4. set_solver+.
    + move => ? In.
      apply/RInj; [set_solver+|set_solver+In|apply:nR; set_solver+In].
  - apply (be_ins_nL_nR).
    + move => b. move: (BE (f b)) => [].
      apply set_unfold_2 => [BE1 BE2]. split => [In|[Eq|In]].
      * destruct BE1 as [Eq|[? [Eq In']]]; first by eauto.
        { left. apply FInj; subst; auto. set_solver+. }
        { right. apply FInj in Eq; first by subst.
          abstract set_solver+In Sub2. abstract set_solver+In' Sub1. }
      * subst. destruct BE2 as [y [Eq ?]]; first by left.
        apply FInj in Eq; auto. by subst. set_solver+.
      * destruct BE2 as [? [Eq In']]; first by eauto.
        apply FInj in Eq; first by subst.
        abstract set_solver+In Sub1. abstract set_solver+In' Sub2.
    + move => ? In.
      apply/RInj; [set_solver+In|set_solver+|apply:nL; set_solver+In]. 
    + move => ? In.
      apply/RInj; [set_solver+|set_solver+In|apply:nR; set_solver+In].
  - apply elem_of_map_gset in In as [a' [? ?]]. subst.
    apply (be_ins_sL_sR _ a _ _ a'); last auto.
    + move => x. move: (BE (f x)) => []. apply set_unfold_2 => [BE1 BE2].
      split => [In|[In Neqxa]].
      * destruct BE1 as [[y [Eq ?]] ?]; first by eauto.
        split. apply FInj in Eq; subst; auto.
        set_solver+H3 Sub1. apply/FInj; auto. set_solver+H4.
      * destruct BE2 as [y [Eq ?]].
        { split. by eexists. apply/FInj; auto.
          set_solver+In Sub1. set_solver+H4. }
        { apply FInj in Eq; subst; auto. set_solver+In Sub1. }
    + apply RInj; eauto. set_solver+H4. set_solver+.
    + destruct sR as [b [Inb Rab]].
      move : Inb => /elem_of_map_gset [a2 [Fa2 In]]. subst b.
      exists a2. split => //. apply RInj; [set_solver+|set_solver+In|auto].
Qed. *)