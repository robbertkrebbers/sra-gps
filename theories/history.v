From mathcomp Require Import ssreflect ssrbool seq.
From stdpp Require Export strings list numbers sorting gmap finite set mapset.

Global Generalizable All Variables.
Global Set Automatic Coercions Import.
Global Set Asymmetric Patterns.
Global Set Bullet Behavior "Strict Subproofs".
From Coq Require Export Utf8.

From igps Require Import infrastructure types machine.

Section History.
  Notation MessageSet := (gset message).
  Implicit Type (m : message) (M : MessageSet).

  Definition locs (M: MessageSet) : gset loc := {[ mloc m | m <- M ]}.

  Lemma fresh_location (M: gset message):
    ∃ l, ∀ m, m ∈ M → mloc m ≠ l.
  Proof.
    exists (fresh (locs M)).
    assert (NIn := is_fresh (locs M)).
    move => m In Eq. apply NIn. 
    rewrite elem_of_map_gset. eauto.
  Qed.

  Definition hist M x := {[ m <- M | mloc m = x ]}.

  Definition hist_upto M x t := {[ m <- hist M x | mtime m ⊏ t ]}.

  Global Arguments hist _ _ /.
  Global Arguments hist_upto _ _ t /.

  Lemma hist_subseteq M x : ∀ m, m ∈ hist M x → m ∈ M.
  Proof. move => m. by rewrite /=; set_solver. Qed.
  
  Lemma hist_upto_subseteq M x t : hist_upto M x t ⊆ hist M x.
  Proof. rewrite /=. set_solver. Qed.

  Lemma elem_of_hist (M : MessageSet) x m :
    m ∈ hist M x ↔ (mloc m = x ∧ m ∈ M).
  Proof. by rewrite /=; set_solver. Qed.

  Lemma hist_upto_time (M : MessageSet) x m t:
    m ∈ hist_upto M x t → mtime m ⊏ t.
  Proof. by rewrite /=; set_solver. Qed.

  Lemma elem_of_hist_upto (M : MessageSet) x t m:
    m ∈ hist_upto M x t ↔ (mloc m = x ∧ m ∈ M ∧ mtime m ⊏ t).
  Proof. by rewrite /=; set_solver. Qed.

  Lemma hist_max_time (M : MessageSet) l (NE: hist M l ≢ ∅):
    ∃ m, m ∈ hist M l ∧ ∀ m', m' ∈ hist M l → mtime m' ⊑ mtime m.
  Proof.
    pose R m1 m2 := mtime m2 ≤ mtime m1.
    assert (TR : Total R). { move => ? ?. apply positive_Total. }
    assert (TaR: Transitive R). { repeat intro. eapply Pos.le_trans; eauto. }
    assert (RR: Reflexive R). { repeat intro. by eapply Pos.le_refl. }
    assert (DR: ∀ m m', Decision (R m m')).
      { rewrite /R. eauto with typeclass_instances. }
    destruct (@gset_nonempty_min _ _ _ R TaR RR TR DR _ NE).
    abstract naive_solver.
  Qed.

  Lemma hist_time_unique (M: memory) x m m':
    m ∈ hist M x → m' ∈ hist M x → mtime m = mtime m' → m = m'.
  Proof.
    move => /elem_of_hist [Hm ?] /elem_of_hist [Hm' ?].
    eapply pairwise_disj_unique => //.
    - by apply memory_ok.
    - by rewrite -Hm' in Hm.
  Qed.

  Lemma hist_add_ins_old M M' {disj: pairwise_disj (M ∪ M')} x
    (NEq : ∀ m, m ∈ M' → x ≠ mloc m)
    : hist M x ≡ hist (mkMemory (M ∪ M') disj) x.
  Proof. set_solver. Qed.

  Lemma hist_upto_add_ins_old (M M' : MessageSet) x t
    {disj: pairwise_disj M}
    (NEq : ∀ m, m ∈ M' → (mloc m ≠ x ∨ t ⊑ mtime m)):
    hist_upto (M ∪ M') x t ≡ hist_upto M x t.
  Proof. 
    apply set_unfold_2; move => m; intuition.
    edestruct NEq => //.
    assert (mtime m ⊏ mtime m).
    { apply: compat_ext_l => //. }
    exfalso. exact: (irreflexive_fw _ _ H3).
  Qed.

  Lemma hist_complete {M : MessageSet} {x} {m} 
    (In : m ∈ M) (EqLoc : mloc m = x)
    : m ∈ hist M x.
  Proof. by apply elem_of_hist. Qed.
  
  Definition hist_from M x t :=
    {[ m <- hist M x | t ⊑ mtime m ]}.

  Definition hist_from_opt M x ot :=
    {[ m <- hist M x | ot ⊑ Some (mtime m) ]}.

  Lemma elem_of_hist_hist_from
    (M : MessageSet) x m t :
    m ∈ hist_from M x t → m ∈ hist M x.
  Proof. set_solver. Qed.

  Lemma hist_from_opt_Some M x t :
    hist_from_opt M x (Some t) ≡
    hist_from M x t.
  Proof. set_solver. Qed.

  Lemma hist_from_time (M : MessageSet) x m t:
    m ∈ hist_from M x t → t ⊑ mtime m .
  Proof. set_solver. Qed.

  Lemma elem_of_hist_from (M : MessageSet) x t m:
    m ∈ hist_from M x t ↔ (mloc m = x ∧ m ∈ M ∧ t ⊑ mtime m).
  Proof. set_solver. Qed.

  Lemma hist_split M x t :
    hist_upto M x t ∪ hist_from M x t ≡ hist M x.
  Proof.
    set_unfold; move => m; intuition.
    case: (decide (t ⊑ mtime m)).
    - by right.
    - left. intuition. exact/Pos.lt_nle.
  Qed.

  Lemma allocated_next_init (M: memory) m v
    (Alloc: allocated (filter (λ m', mtime m' ⊏ mtime m) (msgs M))
                      (mloc m) (mtime m))
    (Inm: m ∈ msgs M)
    (Valm: mval m = VInj v)
    (ValGE: ∀ m', m' ∈ msgs M → mloc m' = mloc m 
                  → mtime m ⊏ mtime m' → isval (mval m'))
    : initialized M (mloc m) (mtime m).
  Proof.
    inversion Alloc. econstructor; eauto.
    - apply (MT_impl TD).
      apply set_unfold_2. move => m'. split.
      + abstract naive_solver.
      + move => [EqLoc [EqD Inm']]. repeat split => //.
        destruct (Pos.lt_total (mtime m') (mtime m)) as [|[Eq|Gt]] => //;
          exfalso.
          * destruct (memory_ok _ _ _ Inm' Inm) as [Eqm|[|]] => //.
            by rewrite Eqm Valm in EqD.
          * specialize (ValGE _ Inm' EqLoc Gt). rewrite EqD in ValGE.
            by inversion ValGE.
    - apply (MT_impl TA).
      apply set_unfold_2. move => m'. split.
      + abstract naive_solver.
      + move => [EqLoc [EqD Inm']]. repeat split => //.
        destruct (Pos.lt_total (mtime m') (mtime m)) as [|[Eq|Gt]] => //;
          exfalso.
          * destruct (memory_ok _ _ _ Inm' Inm) as [Eqm|[|]] => //.
            by rewrite Eqm Valm in EqD.
          * specialize (ValGE _ Inm' EqLoc Gt). rewrite EqD in ValGE.
            by inversion ValGE.
    - assert (In:= MT_msg_In TA).
      rewrite -(MT_msg_time TA).
      rewrite -> 2!elem_of_filter in In. by destruct In as [_ []].
  Qed.

End History.