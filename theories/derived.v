From igps Require Export lifting.

Import uPred.

(** Define some derived forms, and derived lemmas about them. *)
Notation Lam x e := (Rec BAnon x e).
Notation Let x e1 e2 := (App (Lam x e2) e1).
Notation Seq e1 e2 := (Let BAnon e1 e2).
Notation LamV x e := (RecV BAnon x e).
Notation LetCtx x e2 := (AppRCtx (LamV x e2)).
Notation SeqCtx e2 := (LetCtx BAnon e2).
Notation Skip := (Seq (Lit LitUnit) (Lit LitUnit)).
(* Notation Match e0 x1 e1 x2 e2 := (Case e0 (Lam x1 e1) (Lam x2 e2)). *)

Section derived.
Context {Σ: gFunctors} {Iris : ownPG ra_lang Σ}.
Local Set Default Proof Using "Σ Iris".
Implicit Types P Q : iProp Σ.
Implicit Types Φ : val → iProp Σ.

(** Proof rules for the sugar *)

Lemma wp_lam E π x ef e Φ :
  is_Some (base.to_val e) → base.Closed (x :b: []) ef →
  ▷ WP (subst' x e ef, π) @ E {{ Φ }} ⊢ WP (App (Lam x ef) e, π) @ E {{ Φ }}.
Proof. intros. by rewrite -(wp_rec _ π BAnon) //. Qed.

Lemma wp_let E π x e1 e2 Φ :
  is_Some (base.to_val e1) → base.Closed (x :b: []) e2 →
  ▷ WP (subst' x e1 e2, π) @ E {{ Φ }} ⊢ WP (Let x e1 e2, π) @ E {{ Φ }}.
Proof. apply wp_lam. Qed.

Lemma wp_seq E π e1 e2 Φ :
  is_Some (base.to_val e1) → base.Closed [] e2 →
  ▷ WP (e2, π) @ E {{ Φ }} ⊢ WP (Seq e1 e2, π) @ E {{ Φ }}.
Proof. intros ??. by rewrite -wp_let. Qed.

Lemma wp_skip E π Φ : ▷ Φ (LitV LitUnit, π) ⊢ WP (Skip, π) @ E {{ Φ }}.
Proof.
  rewrite -wp_seq; last eauto. by rewrite -wp_value. 
Qed.


Lemma wp_le E π (n1 n2 : Z) P Φ :
  (n1 ≤ n2 → P ⊢ ▷ Φ (LitV (LitInt true), π)) →
  (n2 < n1 → P ⊢ ▷ Φ (LitV (LitInt false), π)) →
  P ⊢ WP (BinOp LeOp (Lit (LitInt n1)) (Lit (LitInt n2)), π) @ E {{ Φ }}.
Proof.
  intros. rewrite -wp_bin_op //; [].
  destruct (bool_decide_reflect (n1 ≤ n2)); by eauto with omega.
Qed.

Lemma wp_lt E π (n1 n2 : Z) P Φ :
  (n1 < n2 → P ⊢ ▷ Φ (LitV (LitInt true), π)) →
  (n2 ≤ n1 → P ⊢ ▷ Φ (LitV (LitInt false), π)) →
  P ⊢ WP (BinOp LtOp (Lit (LitInt n1)) (Lit (LitInt n2)), π) @ E {{ Φ }}.
Proof.
  intros. rewrite -wp_bin_op //; [].
  destruct (bool_decide_reflect (n1 < n2)); by eauto with omega.
Qed.

Lemma wp_eq E π n1 n2 P Φ :
  (n1 = n2 → P ⊢ ▷ Φ (LitV (LitInt true), π)) →
  (n1 ≠ n2 → P ⊢ ▷ Φ (LitV (LitInt false), π)) →
  P ⊢ WP (BinOp EqOp (Lit $ LitInt n1) (Lit $ LitInt n2), π) @ E {{ Φ }}.
Proof.
  intros. rewrite -wp_bin_op //; [].
  destruct (bool_decide_reflect (n1 = n2)); by eauto.
Qed.

End derived.
