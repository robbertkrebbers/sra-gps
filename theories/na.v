From iris.program_logic Require Import weakestpre.
From iris.base_logic Require Import lib.invariants.
From iris.proofmode Require Import tactics.

From igps Require Import viewpred proofmode weakestpre fractor abs_view.
From igps Require Export notation bigop.
From igps.base Require Export ghosts na_read na_write.

Section Helpers.
  Context {Σ : gFunctors}.
  Set Default Proof Using "Type".
  Definition valloc_local_aux h V: iProp Σ := ⌜alloc_local h V⌝%I.

  Definition vPred_valloc_local h :
    @vPred_Mono Σ (valloc_local_aux h).
  Proof.
    repeat intro; iIntros "%". iPureIntro.
    eapply (alloc_local_Mono h _ _ H); eauto.
  Qed.

  Canonical Structure valloc_local h :=
  @Build_vPred Σ _ (vPred_valloc_local h).
End Helpers.

Section Exclusive.
  Context {Σ : gFunctors} `{fG : foundationG Σ}.
  Set Default Proof Using "Type".

  Definition own_loc_prim l h: @vPred Σ
    := (∃ i, valloc_local h ∧ (☐ Hist l h) ∗ (☐ Info l 1%Qp i)).

  Definition own_loc_na l v: @vPred Σ
    := ∃ V, own_loc_prim l {[v, V]}.

  Definition own_loc l : @vPred Σ :=
    (∃ h, own_loc_prim l h).

  Global Instance From_own_loc_prim_exist:
    ∀ V, FromExist (own_loc_prim l h V) 
                   (λ i, (valloc_local h ∧ (☐ Hist l h) ∗ (☐ Info l 1%Qp i)) V)%VP.
  Proof. apply _. Qed.

  Global Instance Into_own_loc_prim_exist:
    ∀ V, IntoExist (own_loc_prim l h V) 
                   (λ i, (valloc_local h ∧ (☐ Hist l h) ∗ (☐ Info l 1%Qp i)) V)%VP.
  Proof. apply _. Qed.

  Global Instance From_own_loc_exist:
    ∀ V, FromExist (own_loc l V) (λ h, (own_loc_prim l h) V).
  Proof. apply _. Qed.

  Global Instance Into_own_loc_exist:
    ∀ V, IntoExist (own_loc l V)  (λ h, (own_loc_prim l h) V).
  Proof. apply _. Qed.

  Global Instance From_own_loc_na_exist:
    ∀ V, FromExist (own_loc_na l v V) (λ V0, (own_loc_prim l {[v, V0]}) V).
  Proof. apply _. Qed.

  Global Instance Into_own_loc_na_exist:
    ∀ V, IntoExist (own_loc_na l v V) (λ V0, (own_loc_prim l {[v, V0]}) V).
  Proof. apply _. Qed.

  Global Instance Frame_na_ownloc:
    ∀ V, Frame false ((own_loc_na l v) V) (own_loc l V) (True%I : iProp Σ) | 1.
  Proof.
    intros. rewrite /own_loc_na /own_loc.
    iIntros "[H _]". iDestruct "H" as (?) "?". iExists _. eauto.
  Qed.

  Global Instance own_loc_prim_timeless l h V 
    : TimelessP (own_loc_prim l h V).
  Proof. apply _. Qed.

  Global Instance own_loc_timeless l V : TimelessP (own_loc l V).
  Proof. apply _. Qed.

  Typeclasses Opaque own_loc_prim own_loc_na own_loc.

  Global Instance own_loc_na_timeless `{foundationG Σ} l v V
    : TimelessP (own_loc_na l v V).
  Proof. rewrite /own_loc_na. apply _. Qed.

End Exclusive.

Typeclasses Opaque own_loc_prim own_loc_na own_loc.

Section Fractional.
  Context {Σ : gFunctors} `{fG : foundationG Σ} `{cG : cinvG Σ}.
  Set Default Proof Using "Type".

  Notation frac_inv h := (λ l _, Hist l h)%I.
  Notation frac_local v := (λ _ _ X, ⌜X = encode v⌝)%I.

  Definition own_loc_na_frac l q v: @vPred Σ
    := ∃ V, (valloc_local {[v, V]}
             ∧ ☐fractored l (frac_inv {[v, V]}) (frac_local v) q). 

End Fractional.

Notation "l ↦ 'A'"
  := (own_loc_na l A)
  (at level 20, format "l  ↦  A") : vPred_scope.
Notation "l ↦ v"
  := (own_loc_na l (VInj v))
  (at level 20, format "l  ↦  v") : vPred_scope.

Notation "l ↦{ q } v"
  := (own_loc_na_frac l q (VInj v))
  (at level 20, format "l  ↦{ q }  v") : vPred_scope.

Notation "l ↦ ?"
  := (own_loc l)
  (at level 20, format "l  ↦  ?") : vPred_scope.


(* TODO: move to some other file *)
Lemma value_at_hd_singleton l v1 V1 v2 V2 :
  value_at_hd l {[v1, V1]} V2 (v2) -> v1 = v2 ∧ V1 = V2.
Proof.
  destruct 1 as (vx & Vx & ? & ?). abstract set_solver.
Qed.

Lemma alloc_local_singleton_na_safe l v V V':
  alloc_local {[v, V]} V' → na_safe l {[v, V]} V'.
Proof.
  move => [v1 [V1 [H1 [H2 _]]]].
  exists (v1, V1). repeat split; [auto|abstract set_solver+H1|auto].
Qed.

Lemma alloc_local_singleton_value_init v V V':
  alloc_local {[VInj v, V]} V' → init_local {[VInj v, V]} V'.
Proof.
  move => [v1 [V1 [H1 [H2 _]]]].
  exists v1, V1. repeat split; [auto|auto|].
  apply elem_of_singleton_1 in H1. by inversion H1.
Qed.


Section ExclusiveRules.
  Context `{foundationG Σ}.
  Set Default Proof Using "Type".

  Lemma na_excl l v w V : (l ↦ v ∗ l ↦ w -∗ False)%VP V.
  Proof.
    iViewUp; iIntros "[H1 H2]".
    iDestruct "H1" as (? ?) "(_ & ? & _)". 
    iDestruct "H2" as (? ?) "(_ & ? & _)". 
    iApply (Hist_Exclusive with "[$H1 $H2]").
  Qed.

  Lemma na_read (E : coPset) l v :
    ↑ physN ⊆ E →
    {{{{ l ↦ v }}}}
      ([ #l]_na) @ E 
    {{{{ z, RET (#z); ■ (z = v) ∗ (l ↦ v)  }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS oNA Post".
    iDestruct "oNA" as (V_0 i) "(% & oH & oI)".
    iApply wp_mask_mono;
      last (iApply (f_read_na with "[$kI $kS $oH]")); first auto.
    - iSplit; iPureIntro;
      by [apply alloc_local_singleton_na_safe|
          apply alloc_local_singleton_value_init].
    - iNext. iIntros (v_r V') "(% & kS' & oH & % & H)".
      iDestruct "H" as (V_1) "(% & % & %)".
      move: H6 => /value_at_hd_singleton [[->] ->].
      iApply ("Post" with "[%] kS'"); [done|]. iFrame ((Logic.eq_refl v_r)).
      iExists _, _. iFrame "oH oI". iPureIntro.
      exists (VInj v_r), V_1. by rewrite elem_of_singleton.
  Qed.

  Lemma na_write (E : coPset) l v_w :
    ↑physN ⊆ E →
    {{{{ l ↦ ? }}}}
      ([ #l]_na <- #v_w) @ E
    {{{{ RET (#()); (l ↦ v_w) }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS oL Post".
    iDestruct "oL" as (h) "oL".
    iDestruct "oL" as (i) "(% & oH & oI)".
    iApply wp_mask_mono; last (iApply (f_write_na with "[$kI $kS $oH]"));
      [auto|auto|].
    iNext. iIntros (V' h') "(% & kS & oH & % & % & % & _ & %)". subst.
    iApply ("Post" with "[%] kS"); [done|].
    iExists _, _. iFrame "oH oI". iPureIntro. by apply alloc_init_local.
  Qed.

End ExclusiveRules.

Section FractionalRules.
  Context {Σ : gFunctors} `{fG : foundationG Σ} `{cG : cinvG Σ}.
  Set Default Proof Using "Type".

  Global Instance na_fractional l v V:
    Fractional (λ q, (l ↦{q} v)%VP V).
  Proof.
    intros q1 q2. iSplit.
    - iIntros "na".
      iDestruct "na" as (V') "(#Local & na)". 
      iDestruct (fractor_splitjoin _ _ (λ _ _ X, ⌜X = encode (VInj v)⌝)%I
                                       (λ _ _ X, ⌜X = encode (VInj v)⌝)%I
        with "[na]") as "[na1 na2]".
      { iApply (fractor_mono with "[$na]"). iIntros (?) "?". by iSplit. }
      iSplitL "na1"; iExists V'; by iFrame.
    - iIntros "(na1 & na2)".
      iDestruct "na1" as (V1) "(Local1 & na1)".
      iDestruct "na2" as (V2) "(Local2 & na2)".
      iCombine "na1" "na2" as "na".
      rewrite fractor_join_l.
      iExists _. iSplit; first by iExact "Local1".
      iApply (fractor_mono with "[$na]").
      by iIntros (?) "[? _]".
  Qed.

  Lemma na_frac_split l v q1 q2:
     l ↦{q1+q2} v ⊧ l ↦{q1} v ∗ l ↦{q2} v.
  Proof. constructor => V. iViewUp. by rewrite na_fractional. Qed.

  Lemma na_frac_split_2 l v q:
     l ↦{q} v ⊧ l ↦{q/2} v ∗ l ↦{q/2} v.
  Proof. rewrite -{1}(Qp_div_2 q). apply na_frac_split. Qed.

  Lemma na_frac_agree l v1 v2 q1 q2: 
    l ↦{q1} v1 ∗ l ↦{q2} v2 ⊧ ■ (v1 = v2).
  Proof.
    constructor => V. iViewUp. iIntros "(na1 & na2)".
    iDestruct "na1" as (V1) "(Local1 & na1)".
    iDestruct "na2" as (V2) "(Local2 & na2)".
    iCombine "na1" "na2" as "na".
    rewrite fractor_join_l.
    iDestruct (fractor_drop with "na") as (X) "[% %]".
    iPureIntro. subst X. apply encode_inj in H0. by inversion H0.
  Qed.

  Global Instance na_exists_fractional l V:
    Fractional (λ q, (∃ v, l ↦{q} v)%VP V).
  Proof.
    intros q1 q2. iSplit.
    - iIntros "na".
      iDestruct "na" as (v) "na". rewrite na_fractional.
      iDestruct "na" as "[na1 na2]".
      iSplitL "na1"; by iExists v.
    - iIntros "(na1 & na2)".
      iDestruct "na1" as (v1) "na1". iDestruct "na2" as (v2) "na2".
      destruct (na_frac_agree l v1 v2 q1 q2) as [NA].
      iDestruct (NA with "[$na1 $na2]") as %Eq; [reflexivity|subst v2].
      iExists v1. iApply (fractional with "[$na1 $na2]").
  Qed.

  Lemma na_frac_join l v q1 q2:
    l ↦{q1} v ∗ l ↦{q2} v ⊧ l ↦{q1+q2} v.
  Proof. constructor => V. iViewUp. by rewrite na_fractional. Qed.

  Lemma na_frac_join_1 l v1 v2 q1 q2:
    l ↦{q1} v1 ∗ l ↦{q2} v2 ⊧ l ↦{q1+q2} v1.
  Proof.
    constructor => V. iViewUp. iIntros "na".
    destruct (na_frac_agree l v1 v2 q1 q2) as [NA].
    iDestruct (NA with "na") as "%"; first done.
    subst v2.
    destruct (na_frac_join l v1 q1 q2) as [NA2].
    by iApply (NA2 with "na").
  Qed.

  Lemma na_frac_mult_splitL l v (n: positive) q:
    (l ↦{Pos2Qp n * q} v ⊧ [∗ list] _ ∈ seq 0 (Pos.to_nat n), l ↦{q} v)%VP.
  Proof.
    constructor => V. iViewUp.
    rewrite -{1}(Pos2Nat.id n).
    change ((l ↦{_} v)%VP V)
      with ((λ q, (l ↦{q} v)%VP V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma na_frac_mult_splitL_1 l v (n: positive):
    (l ↦{1} v ⊧ [∗ list] _ ∈ seq 0 (Pos.to_nat n), l ↦{1/n} v)%VP.
  Proof. rewrite {1}(Pos2Qp_1_div n). by apply na_frac_mult_splitL. Qed.

  Lemma na_frac_mult_joinL l v (n: positive) q:
    (([∗ list] _ ∈ seq 0 (Pos.to_nat n), l ↦{q} v)  ⊧  l ↦{Pos2Qp n * q} v)%VP.
  Proof.
    constructor => V. iViewUp.
    rewrite -{2}(Pos2Nat.id n).
    change ((l ↦{_} v)%VP V)
      with ((λ q, (l ↦{q} v)%VP V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma na_frac_mult_exists_joinL l (n: positive) q V:
    ([∗ list] _ ∈ seq 0 (Pos.to_nat n), ∃ v, l ↦{q} v)%VP V
    ⊢ (∃ v, l ↦{Pos2Qp n * q} v)%VP V.
  Proof.
    rewrite -vPred_big_opL_fold
            -(big_sepL_mult_fractional
                (Φ := (λ q, (∃ v, l ↦{q} v)%VP V))); last lia.
    by rewrite Pos2Nat.id.
  Qed.

  Lemma na_frac_mult_joinL_1 l v (n: positive):
    ([∗ list] _ ∈ seq 0 (Pos.to_nat n), l ↦{1/n} v)%VP ⊧ l ↦{1} v.
  Proof. rewrite {2}(Pos2Qp_1_div n). by apply na_frac_mult_joinL. Qed.

  Lemma na_read_frac (E : coPset) l q v :
    ↑ physN ⊆ E →
    ↑fracN .@ l ⊆ E →
    {{{{ l ↦{q} v }}}}
      [ #l ]_na @ E 
    {{{{ z, RET (#z); ■ (z = v) ∗ l ↦{q} v }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS oNA Post".
    iDestruct "oNA" as (V_0) "(% & oNA)".
    iMod (fractor_open with "oNA") as (X) "(oH & HX & HClose)"; [solve_ndisj|].
    iApply wp_mask_mono; last (iApply (f_read_na with "[$kI $kS $oH]"));
      first solve_ndisj.
    - iSplit; iPureIntro;
      by [apply alloc_local_singleton_na_safe|
          apply alloc_local_singleton_value_init].
    - iNext. iIntros (v_r V') "(% & kS' & oH & % & H)".
      iDestruct "H" as (V_1) "(% & % & %)".
      move: H6 => /value_at_hd_singleton [[->] ->].
      iApply ("Post" with "[%] kS'"); [done|]. iFrame ((Logic.eq_refl v_r)).
      iMod ("HClose" $! (λ _ _ X, ⌜X = encode (VInj v_r)⌝)%I with "[$oH $HX]")
        as "oNA".
      iModIntro.
      iExists _. iFrame "oNA". iPureIntro.
      exists (VInj v_r), V_1. by rewrite elem_of_singleton.
  Qed.

  Lemma na_read_abs_frac `{absG : absViewG Σ} (E : coPset) l q v β :
    ↑ physN ⊆ E →
    ↑fracN .@ l ⊆ E →
    {{{{ ⌞l ↦{q} v⌟ β ∗ aSeen β }}}}
      [ #l ]_na @ E 
    {{{{ z, RET (#z); ■ (z = v) ∗ ⌞l ↦{q} v⌟ β }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS (ol & Seen) Post".
    iDestruct "ol" as (V0) "(abs & ol)".
    iDestruct "Seen" as (V1) "(% & abs2)".
    iDestruct (absView_agree with "abs abs2") as %?. subst V0.
    iDestruct "ol" as (V0) "[% ol]".
    iMod (fractor_open with "ol") as (X) "(oH & HX & HClose)"; [solve_ndisj|].
    iApply wp_mask_mono; last (iApply (f_read_na with "[$kI $kS $oH]"));
      first solve_ndisj.
    - iSplit; iPureIntro;
       [apply alloc_local_singleton_na_safe|
          apply alloc_local_singleton_value_init];
       by apply (alloc_local_Mono _ _ V1).
    - iNext. iIntros (v_r V') "(% & kS' & oH & % & H)".
      iDestruct "H" as (V_1) "(% & % & %)".
      move: H7 => /value_at_hd_singleton [[?] ?]. subst v_r V_1.
      iApply ("Post" with "[%] kS'"); [done|].
      iMod ("HClose" $! (λ _ _ X, ⌜X = encode (VInj v)⌝)%I with "[$oH $HX]")
        as "ol".
      iModIntro. iSplitL ""; first done.
      iExists _. iFrame "abs". iExists _. by iFrame "ol".
  Qed.

  Lemma na_frac_from_non (E: coPset) l v:
    ↑ physN ⊆ E →
    (☐ PSCtx ∗ l ↦ v ={E}=> l ↦{1} v)%VP.
  Proof.
    intros. constructor => ? V _.
    iIntros "[kI na]".
    iDestruct "na" as (V' i) "(Local & Hist & Info)".
    iExists V'. iFrame "Local".
    iApply (fractor_alloc _ _ (encode (VInj v))); [done|by iFrame].
  Qed.

  Lemma na_frac_dealloc (E: coPset) l v:
    ↑fracN.@l ⊆ E →
    (l ↦{1} v ={E}=> l ↦ ?)%VP.
  Proof.
    intros. constructor => ? V _.
    iIntros "oL".
    iDestruct "oL" as (V') "[Local oL]".
    iMod (fractor_dealloc with "oL") as (? ?) "(oI & _ & >oH)"; first done.
    iExists _, _. by iFrame.
  Qed.

  Lemma na_write_frac_1 (E : coPset) l v:
    ↑ physN ⊆ E →
    {{{{ l ↦ ? }}}}
      ([ #l]_na <- #v) @ E
    {{{{ RET (#()); (l ↦{1} v) }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS oL Post".
    iApply wp_fupd.
    iApply (na_write with "[%] kI kS oL"); [auto|done|..].
    iNext. iIntros (V' h') "kS oL".
    destruct (na_frac_from_non E l v) as [NA]; first done.
    iDestruct (NA with "[$kI $oL]") as ">oL"; first done.
    by iApply ("Post" with "[%] kS oL").
  Qed.

  Lemma na_write_frac (E : coPset) l v v_w:
    ↑ physN ⊆ E →
    ↑fracN .@ l ⊆ E →
    {{{{ l ↦{1} v }}}}
      ([ #l]_na <- #v_w) @ E
    {{{{ RET (#()); (l ↦{1} v_w) }}}}.
  Proof.
    intros. iViewUp; iIntros "#kI kS oL Post".
    destruct (na_frac_dealloc E l v) as [NA]; first done.
    iDestruct (NA with "oL") as ">oL"; first done.
    by iApply (na_write_frac_1 with "[%] kI kS oL Post").
  Qed.


End FractionalRules.


Arguments na_read [_ _ _ _ _] _ [_ _ _].
Arguments na_write [_ _ _ _ _] _ [_ _ _].