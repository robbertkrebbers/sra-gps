From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From iris.algebra Require Import frac.
From stdpp Require Import functions.
From iris.base_logic.lib Require Import cancelable_invariants.
From iris.proofmode Require Import tactics.

Import uPred.

From igps Require Export notation viewpred.
From igps Require Import rsl fractor persistor na proofmode weakestpre.

Section proof.

Context `{!foundationG Σ, !rslG Σ}.
Set Default Proof Using "Type".

Local Notation iProp := (iProp Σ).
Local Notation vPred := (@vPred Σ).
Implicit Types  (l: loc) (V: View)  (q: frac)
                (Ψ: gname → Z → View → iProp) (φ: vPred).

Local Open Scope I.

  Section Plain.
  Context `{!persistorG Σ}.

  Definition Rel' l (φ φd: Z → vPred) V
    := ∃ (φR: Z → vPred), persisted l (RSLInv φR φd) (λ _ n, RelRaw V φ φR n).

  Definition Acq' l (φ φd: Z → vPred) V
    := ∃ (φR: Z → vPred), persisted l (RSLInv φR φd) (λ _ n, AcqRaw V φ n).

  Definition RMWAcq' l (φR φd: Z → vPred) V
    := persisted l (RSLInv φR φd) (λ _ n, RMWAcqRaw V n).

  Definition Init' l V
    := ∃ (φR φd: Z → vPred), persisted l (RSLInv φR φd) (λ _ n, InitRaw V n).

  Lemma vPred_Rel'_mono l (φ φd: Z → vPred): vPred_Mono (Rel' l φ φd).
  Proof.
    repeat intro. iIntros "Rel".
    iDestruct "Rel" as (φR) "FR". iDestruct "FR" as (jγ) "(FV & ?)".
    iExists φR, jγ. iFrame.
    iDestruct "FV" as (? ? ?) "(? & ? & % & ?)".
    iExists _, _, _. iFrame.
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure Rel l (φ φd: Z → vPred)
    := Build_vPred _ (vPred_Rel'_mono l φ φd).

  Lemma vPred_Acq'_mono l (φ φd: Z → vPred): vPred_Mono (Acq' l φ φd).
  Proof.
    repeat intro. iIntros "Acq".
    iDestruct "Acq" as (φR) "FA". iDestruct "FA" as (jγ) "(FV & ?)".
    iExists φR, jγ. iFrame.
    iDestruct "FV" as (? ? ? ? ?) "(? & ? & ? & % & ?)".
    iExists _,_,_,_,_. iFrame.
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure Acq l (φ φd: Z → vPred)
    := Build_vPred _ (vPred_Acq'_mono l φ φd).

  Lemma vPred_RMWAcq'_mono l (φ φd: Z → vPred): vPred_Mono (RMWAcq' l φ φd).
  Proof.
    repeat intro. iIntros "FA".
    iDestruct "FA" as (jγ) "(FV & ?)". iExists jγ. iFrame.
    iDestruct "FV" as (s j γ) "(? & ? & Eq)".
    iDestruct "Eq" as (? ?) "(? & %)".
    iExists _,_,_. iFrame. iExists _, _. iFrame "Eq".
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure RMWAcq l (φ φd: Z → vPred)
    := Build_vPred _ (vPred_RMWAcq'_mono l φ φd).

  Lemma vPred_Init'_mono l: vPred_Mono (Init' l).
  Proof.
    repeat intro. iIntros "Init".
    iDestruct "Init" as (φR φd) "FA". iDestruct "FA" as (jγ) "(FV & ?)".
    iExists φR, φd, jγ. iFrame.
    iDestruct "FV" as (s j γ) "(? & ? & Eq)".
    iDestruct "Eq" as %[? [? ?]].
    iExists _,_,_. iFrame. iPureIntro. eexists.
    split => //. by apply (init_local_Mono _ _ V).
  Qed.

  Canonical Structure Init l
    := Build_vPred _ (vPred_Init'_mono l).

  Lemma Rel_write l (φ φd: Z → vPred) v (E : coPset)
    (HN: ↑physN ⊆ E) (Hl: ↑persist_locN .@ l ⊆ E):
    {{{{ ▷ φ v ∗ ▷ φd v ∗ Rel l φ φd }}}}
      ([ #l ]_at <- #v) @ E
    {{{{ RET #() ; Rel l φ φd ∗ Init l }}}}.

  Proof.
    intros; iViewUp. iIntros "kI kS (Itpr & ItprD & Rel) Post".

    iDestruct "Rel" as (φR) "Rel".
    iMod (persistor_open E l with "Rel") as (X) "(Inv & Rel & PClose)";
      [done|].
    iApply (RelRaw_write l φ φd φR with "[$kI $kS $Itpr $ItprD $Inv $Rel]");
      [solve_ndisj|].
    iNext. iIntros (V') "(VV' & kS & Inv & Rel & Init)".
    iMod ("PClose" $! (λ l X, RelRaw V' φ φR X ∗ InitRaw V' X)
              with "[$Inv $Rel $Init]") as "RI".
    iDestruct (persistor_splitjoin with "RI") as "[Rel Init]".
    iApply ("Post" $! V' with "VV' kS").
    iSplitL "Rel"; [by iExists _|by iExists _,_].
  Qed.

  Lemma Acq_read l (φ φd: Z → vPred) (E: coPset)
    (HN: ↑physN ⊆ E) (Hl: ↑persist_locN .@ l ⊆ E):
    (□ ∀ v, φd v -∗ φd v ∗ φd v)%VP
    ⊩ {{{{ Acq l φ φd ∗ Init l }}}}
        ([(#l)]_at) @ E
      {{{{ (v:Z), RET #v;
          Acq l (<[v:=True%VP]>φ) φd ∗ Init l ∗ ▷ φ v ∗ ▷ φd v }}}}%VP.
  Proof.
    iIntros "#Dup".
    intros. iIntros (?) "!#". iIntros (π Φ). iViewUp.
    iIntros "kI kS (Acq & Init) Post".

    iDestruct "Acq" as (φR) "Acq".
    iDestruct "Init" as (φR' φd') "Init".
    iDestruct (persistor_join_l with "[$Acq $Init]") as "AI".
    iMod (persistor_open with "[$AI]") as (jγ) "(Inv & (AR & IR) & PClose)";
      [done|].
    iApply (AcqRaw_read  l φ φd φR _ jγ with "[$kI $kS $Inv $AR $IR Dup]");
      [solve_ndisj|..].
      { iIntros (V v). by iApply ("Dup" $! V v V). }
    iNext. iIntros (v V') "(VV' & kS' & Inv & AR & IR & Intr & IntrD)".
    iMod ("PClose" $! (λ l X, AcqRaw V' (<[v:=True%VP]>φ) X ∗ InitRaw V' X)
            with "[$Inv $AR $IR]") as "AI".
     iDestruct (persistor_splitjoin with "AI") as "[Acq Init]".
     iApply ("Post" $! V' with "VV' kS'"). iFrame "Intr IntrD".
     iSplitL "Acq"; [by iExists _|by iExists _,_].
  Qed.

  Lemma RMWAcq_CAS
    l (φ φd: Z → vPred) (P: vPred) (R: bool → Z → vPred) (v_r v_w: Z) (E: coPset)
    (HN: ↑physN ⊆ E) (Hl: ↑persist_locN .@ l ⊆ E):
    (□ (∀ v, φd v -∗ φd v ∗ φd v))%VP
    ⊩ {{{{ (P ∗ ▷ φ v_r ∗ ▷ φd v_r
            ={ E ∖ ↑persist_locN .@ l }=∗ ▷ φ v_w ∗ ▷ φd v_w ∗ R true v_r)
       ∗ (∀ v, ■ (v ≠ v_r) → P ∗ ▷ φd v ={ E ∖ ↑persist_locN .@ l }=∗ R false v)
       ∗ RMWAcq l φ φd ∗ Init l ∗ P}}}}
        (CAS #l #v_r #v_w) @ E
    {{{{ (b: bool) (v': Z), RET #b; RMWAcq l φ φd ∗ Init l ∗ R b v'}}}}%VP.
  Proof.
    iIntros "#Dup".
    intros. iIntros (?) "!#". iIntros (π Φ). iViewUp.
    iIntros "kI kS (CS & CF & RMW & Init & P) Post".

    iDestruct "Init" as (φR' φd') "Init".
    iDestruct (persistor_join_l with "[$RMW $Init]") as "AI".
    iMod (persistor_open with "[$AI]") as (jγ) "(Inv & (AR & IR) & PClose)";
      [done|].
    iPoseProof (RMWAcqRaw_CAS l φ φd P R v_r v_w _ jγ (E ∖ ↑persist_locN .@ l) 
      with "[]")
      as "RMW"; [solve_ndisj|..].
    { iIntros "!#". iIntros (V v) "?". by iApply ("Dup" $! V v with "[]"). }

    iApply ("RMW" with "[$kI $kS $Inv $AR $IR $P $CS $CF]").

    iNext. iIntros (b v' V') "(VV' & kS' & Inv & AR & IR & R)".
    iMod ("PClose" $! (λ l X, RMWAcqRaw V' X ∗ InitRaw V' X)
            with "[$Inv $AR $IR]") as "AI".
     iDestruct (persistor_splitjoin with "AI") as "[RMW Init]".
     iApply ("Post" $! V' with "VV' kS'"). iFrame "R RMW".
     by iExists _,_.
  Qed.

  Lemma Acq_alloc (φ φd: Z → vPred) (E: coPset)
    (HN: ↑physN ⊆ E) (Hp: ↑persistN ⊆ E):
    {{{{ ☐ PersistorCtx }}}}
      Alloc @ E
    {{{{ (l: loc), RET (LitV $ LitLoc l); Rel l φ φd ∗ Acq l φ φd }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS #kP Post". iApply wp_fupd.
    iApply (AcqRaw_alloc φ φd _ _ _ HN with "[$kI $kS]"). iNext.
    iIntros (l V' n iv) "(VV' & kS' & Info & Inv & Rel & Acq)".
    iMod (persistor_inv_alloc E iv n l (RSLInv φ φd)
                              (λ l jγ, RelRaw V' φ φ jγ ∗ AcqRaw V' φ jγ)
                              with "[$kI $kP $Info $Inv $Rel $Acq]") as "AR";
      [auto|auto|].
    iDestruct (persistor_splitjoin with "AR") as "[Rel Acq]".
    iApply ("Post" $! V' with "VV' kS'").
    iSplitL "Rel"; by iExists _.
  Qed.

  Lemma RMWAcq_alloc (φ φd: Z → vPred)(E: coPset)
    (HN: ↑physN ⊆ E) (Hp: ↑persistN ⊆ E):
    {{{{ ☐ PersistorCtx  }}}}
      Alloc @ E
    {{{{ (l: loc), RET (LitV $ LitLoc l); Rel l φ φd ∗ RMWAcq l φ φd }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS #kP Post". iApply wp_fupd.
    iApply (RMWAcqRaw_alloc φ φd _ _ _ HN with "[$kI $kS]"). iNext.
    iIntros (l V' n iv) "(VV' & kS' & Info & Inv & Rel & RMW)".
    iMod (persistor_inv_alloc E iv n l (RSLInv φ φd)
                              (λ l jγ, RelRaw V' φ φ jγ ∗ RMWAcqRaw V' jγ)
                              with "[$kI $kP $Info $Inv $Rel $RMW]") as "AR";
      [auto|auto|].
    iDestruct (persistor_splitjoin with "AR") as "[Rel RMW]".
    iApply ("Post" $! V' with "VV' kS'"). iFrame "RMW".
    by iExists _.
  Qed.

  Lemma RMWAcq_init l v (φ φd: Z → vPred) (E : coPset)
    (HN: ↑physN ⊆ E) (Hp: ↑persistN ⊆ E):
    {{{{ ☐ PersistorCtx ∗ own_loc l ∗ ▷ φ v ∗ ▷ φd v }}}}
      ([ #l ]_na <- #v) @ E
    {{{{ RET #(); Rel l φ φd ∗ RMWAcq l φ φd ∗ Init l }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (#kP & oL & Hφ & Hφd) Post".
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (RMWAcqRaw_init l v _ φ φd _ _ _ HN
            with "[$kI $kS $Hist $Alloc $Hφ $Hφd]").
    iNext.
    iIntros (V' n) "(VV' & kS' & Inv & Rel & RMW & Init)".
    iMod (persistor_inv_alloc E i n l (RSLInv φ φd)
                        (λ l n, RelRaw V' φ φ n ∗ RMWAcqRaw V' n ∗ InitRaw V' n)
          with "[$kI $kP $Info $Inv $Rel $RMW $Init]") as "AR"; [auto|auto|].
    iDestruct (persistor_splitjoin with "AR") as "[Rel RMW]".
    iDestruct (persistor_splitjoin with "RMW") as "[RMW Init]".
    iApply ("Post" $! V' with "VV' kS'"). iFrame "RMW".
    iSplitL "Rel"; [by iExists _|by iExists _,_].
  Qed.

  Lemma Acq_init l v (φ φd: Z → vPred) (E : coPset)
    (HN: ↑physN ⊆ E) (Hp: ↑persistN ⊆ E):
    {{{{ ☐ PersistorCtx ∗ own_loc l ∗ ▷ φ v ∗ ▷ φd v }}}}
      ([ #l ]_na <- #v) @ E
    {{{{ RET #(); Rel l φ φd ∗ Acq l φ φd ∗ Init l }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (#kP & oL & Hφ & Hφd) Post".
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (AcqRaw_init l v _ φ φd _ _ _ HN
            with "[$kI $kS $Hist $Alloc $Hφ $Hφd]").
    iNext.
    iIntros (V' n) "(VV' & kS' & Inv & Rel & Acq & Init)".
    iMod (persistor_inv_alloc E i n l (RSLInv φ φd)
                        (λ l n, RelRaw V' φ φ n ∗ AcqRaw V' φ n ∗ InitRaw V' n)
          with "[$kI $kP $Info $Inv $Rel $Acq $Init]") as "AR"; [auto|auto|].
    iDestruct (persistor_splitjoin with "AR") as "[Rel Acq]".
    iDestruct (persistor_splitjoin with "Acq") as "[Acq Init]".
    iApply ("Post" $! V' with "VV' kS'").
    iSplitL "Rel"; last iSplitL "Acq"; by [iExists _|iExists _|iExists _,_].
  Qed.

  Lemma Acq_join l (φ1 φ2 φd: Z → vPred) (E : coPset)
      (HNl : ↑persist_locN .@ l ⊆ E):
      (Acq l φ1 φd ∗ Acq l φ2 φd ={E}=> Acq l (λ v, φ1 v ∗ φ2 v) φd)%VP.
  Proof.
    constructor => ? V _.
    iIntros "(AR1 & AR2)".
    iDestruct "AR1" as (φR) "AR1". iDestruct "AR2" as (φR') "AR2".
    iDestruct (persistor_join_l l with "[$AR1 $AR2]") as "AR".
    iMod (persistor_open with "AR") as (n) "(Inv & (AR1 & AR2) & PClose)";
      first auto.
    iMod (AcqRaw_join l with "[$Inv $AR1 $AR2]") as "(Inv & AR)".
    iMod ("PClose" $! (λ l n, AcqRaw V (λ v : Z, (φ1 v ∗ φ2 v)%VP) n)
          with "[$Inv $AR]") as "AR".
    by iExists _.
  Qed.


  Lemma Acq_split l (φ1 φ2 φd: Z → vPred) (E : coPset)
      (HNl : ↑persist_locN .@ l ⊆ E):
    (Acq l (λ v, φ1 v ∗ φ2 v) φd ={E}=> Acq l φ1 φd ∗ Acq l φ2 φd)%VP.
  Proof.
    constructor => ? V _.
    iIntros "AR". iDestruct "AR" as (φR) "AR".
    iMod (persistor_open with "AR") as (n) "(Inv & AR & PClose)";
      first auto.
    iMod (AcqRaw_split l with "[$Inv $AR]") as "(Inv & AR1 & AR2)".
    iMod ("PClose" $! (λ l n, AcqRaw V φ1 n ∗ AcqRaw V φ2 n)
          with "[$Inv $AR1 $AR2]") as "AR".
    iDestruct (persistor_splitjoin with "AR") as "(AR1 & AR2)".
    iSplitL "AR1"; by iExists _.
  Qed.

  Lemma Rel_split l (φ1 φ2 φd: Z → vPred):
      Rel l (λ v, φ1 v ∨ φ2 v)%VP φd ⊧  Rel l φ1 φd ∗ Rel l φ2 φd.
  Proof.
    constructor => ? V _.
    iIntros "RR". iDestruct "RR" as (φR) "RR".
    iDestruct "RR" as (n) "(RR & #kI)".
    iDestruct (RelRaw_split with "RR") as "(RR1 & RR2)".
    iSplitL "RR1"; iExists φR, n; by iFrame.
  Qed.

  Lemma Rel_join l (φ1 φ2 φd: Z → vPred):
    Rel l φ1 φd ∗ Rel l φ2 φd ⊧ Rel l (λ v, φ1 v ∨ φ2 v)%VP φd.
  Proof.
    constructor => ? V _.
    iIntros "(RR1 & RR2)".
    iDestruct "RR1" as (φR) "RR1". iDestruct "RR2" as (φR') "RR2".
    iDestruct (persistor_join_l l with "[$RR1 $RR2]") as "RR".
    iDestruct "RR" as (n) "((RR1 & RR2) & #kI)".
    iDestruct (RelRaw_join with "[$RR1 $RR2]") as "RR".
    iExists φR, n. by iFrame.
  Qed.

  Lemma RMWAcq_split l (φ φd: Z → vPred) (E : coPset)
    (HNl: ↑persist_locN .@ l ⊆ E):
    (RMWAcq l φ φd ={E}=>  RMWAcq l φ φd ∗ Acq l (λ _, True) φd)%VP.
  Proof.
    constructor => ? V _.
    iIntros "RMW".
    iMod (persistor_open with "RMW") as (n) "(Inv & RMW & PClose)"; first auto.
    iMod (RMWAcqRaw_split with "[$Inv $RMW]") as "(Inv & RMW & Acq)".
    iMod ("PClose" $! (λ _ n, RMWAcqRaw V n ∗ AcqRaw V (λ _, True)%VP n)
      with "[$Inv $RMW $Acq]") as "AR".
    iDestruct (persistor_splitjoin with "AR") as "(RMW & Acq)".
    iFrame "RMW". by iExists _.
  Qed.

  End Plain.

  Section Frac.

  Context `{!cinvG Σ}.
  Definition Relp' l q (φ φd: Z → vPred) V
    := ∃ (φR: Z → vPred), fractored l (RSLInv φR φd) (λ _ _ n, RelRaw V φ φR n) q.

  Definition Acqp' l q (φ φd: Z → vPred) V
    := ∃ (φR: Z → vPred), fractored l (RSLInv φR φd) (λ _ _ n, AcqRaw V φ n) q.

  Definition Initp' l q V
    := ∃ (φR φd: Z → vPred), fractored l (RSLInv φR φd) (λ _ _ n, InitRaw V n) q.

  Definition Emptyp l q V
    := ∃ (φR φd: Z → vPred), fractored l (RSLInv φR φd) (λ _ _ _, True) q.

  Definition RMWAcqp' l q (φR φd: Z → vPred) V
    := fractored l (RSLInv φR φd) (λ _ _ n, RMWAcqRaw V n) q.

  Definition vPred_Relp'_mono l q (φ φd: Z → vPred):
    vPred_Mono (Relp' l q φ φd).
  Proof.
    repeat intro. iIntros "Rel".
    iDestruct "Rel" as (φR) "FR". iDestruct "FR" as (jγ) "(FV & ?)".
    iExists φR, jγ. iFrame.
    iDestruct "FV" as (? ? ?) "(? & ? & % & ?)".
    iExists _, _, _. iFrame.
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure Relp l q (φ φd: Z → vPred)
    := Build_vPred _ (vPred_Relp'_mono l q φ φd).


  Definition vPred_Acqp'_mono l q (φ φd: Z → vPred):
    vPred_Mono (Acqp' l q φ φd).
  Proof.
    repeat intro. iIntros "Acq".
    iDestruct "Acq" as (φR) "FA". iDestruct "FA" as (jγ) "(FV & ?)".
    iExists φR, jγ. iFrame.
    iDestruct "FV" as (? ? ? ? ?) "(? & ? & ? & % & ?)".
    iExists _,_,_,_,_. iFrame.
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure Acqp l q (φ φd: Z → vPred)
    := Build_vPred _ (vPred_Acqp'_mono l q φ φd).

  Definition vPred_Initp'_mono l q:
    vPred_Mono (Initp' l q).
  Proof.
    repeat intro. iIntros "Init".
    iDestruct "Init" as (φR φd) "FA". iDestruct "FA" as (jγ) "(FV & ?)".
    iExists φR, φd, jγ. iFrame.
    iDestruct "FV" as (s j γ) "(? & ? & Eq)".
    iDestruct "Eq" as %[? [? ?]].
    iExists _,_,_. iFrame. iPureIntro. eexists.
    split => //. by apply (init_local_Mono _ _ V).
  Qed.

  Canonical Structure Initp l q
    := Build_vPred _ (vPred_Initp'_mono l q).

  Definition vPred_RMWAcqp'_mono l q (φ φd: Z → vPred):
    vPred_Mono (RMWAcqp' l q φ φd).
  Proof.
    repeat intro. iIntros "FA".
    iDestruct "FA" as (jγ) "(FV & ?)". iExists jγ. iFrame.
    iDestruct "FV" as (s j γ) "(? & ? & Eq)".
    iDestruct "Eq" as (? ?) "(? & %)".
    iExists _,_,_. iFrame. iExists _, _. iFrame "Eq".
    iPureIntro. by apply (alloc_local_Mono _ _ V).
  Qed.

  Canonical Structure RMWAcqp l q (φ φd: Z → vPred)
    := Build_vPred _ (vPred_RMWAcqp'_mono l q φ φd).


  Lemma Relp_write l q1 q2 (φ φd: Z → vPred) v (E: coPset) 
    (HN: ↑physN ⊆ E) (Hl: ↑fracN .@ l ⊆ E):
    {{{{ ▷ φ v ∗ ▷ φd v ∗ Relp l (q1 + q2) φ φd }}}}
      ([(#l)]_at <- #v) @ E
    {{{{ RET #() ; Relp l q1 φ φd ∗ Initp l q2 }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (Itpr & ItprD & Rel) Post".
    iDestruct "Rel" as (φR) "Rel".
    iMod (fractor_open E l _ _ _ Hl with "Rel") as (X) "(Inv & #Rel & PClose)".

    iApply (RelRaw_write _ φ φd φR with "[$kI $kS $Itpr $ItprD $Inv $Rel]");
      [solve_ndisj|].
    iNext. iIntros (V') "(VV' & kS' & Inv & Rel' & Init)".
    iMod ("PClose" $! (λ l _ X, RelRaw V' φ φR X ∗ InitRaw V' X)
              with "[$Inv $Rel' $Init]") as "RI".
    iDestruct (fractor_splitjoin l _ 
                (λ l _ X, RelRaw V' φ φR X)
                (λ l _ X, InitRaw V' X) with "RI") as "[Rel' Init]".
    iApply ("Post" $! V' with "VV' kS'").
    iSplitL "Rel'"; [by iExists _|by iExists _,_].
  Qed.

  Lemma Acqp_read l (φ φd: Z → vPred) q1 q2 (E: coPset) 
    (HN: ↑physN ⊆ E) (Hl: ↑fracN .@ l ⊆ E):
    (□ (∀ v, φd v -∗ φd v ∗ φd v))%VP
    ⊩ {{{{ Acqp l q1 φ φd ∗ Initp l q2 }}}}
        ([(#l)]_at) @ E
    {{{{ (v:Z), RET #v;
          Acqp l q1 (<[v:=True%VP]>φ) φd ∗ Initp l q2 ∗ ▷ φ v ∗ ▷ φd v }}}}%VP.
  Proof.
    iIntros "#Dup".
    intros. iIntros (?) "!#". iIntros (π Φ). iViewUp.
    iIntros"kI kS (Acq & Init) Post".
    iDestruct "Acq" as (φR) "Acq".
    iDestruct "Init" as (φR' φd') "Init".
    iDestruct (fractor_join_l with "[$Acq $Init]") as "AI".
    iMod (fractor_open with "[$AI]") as (jγ) "(Inv & (AR & IR) & PClose)";
      first auto.
    iApply (AcqRaw_read l φ φd φR _ jγ
              with "[$kI $kS $Inv $AR $IR Dup]"); [solve_ndisj|..].
    { iIntros (V v). by iApply ("Dup" $! V v V). }
    iNext. iIntros (v V') "(VV' & kS' & Inv & AR & IR & Intr & IntrD)".
    iMod ("PClose" $! (λ l _ X, AcqRaw V' (<[v:=True%VP]>φ) X ∗ InitRaw V' X)
            with "[$Inv $AR $IR]") as "AI".
     iDestruct (fractor_splitjoin _ _
                  (λ l _ X, AcqRaw V' (<[v:=True%VP]>φ) X)
                  (λ l _ X, InitRaw V' X)  with "AI") as "[Acq Init]".
     iApply ("Post" $! V' with "VV' kS'"). iFrame "Intr IntrD".
     iSplitL "Acq"; [by iExists _|by iExists _,_].
  Qed.

  Lemma RMWAcqp_CAS
    l (φ φd: Z → vPred) (P: vPred) (R: bool → Z → vPred) (v_r v_w: Z) q1 q2
      (E: coPset) (HN: ↑physN ⊆ E) (Hl: ↑fracN .@ l ⊆ E):
    (□ (∀ v, φd v -∗ φd v ∗ φd v))%VP
    ⊩ {{{{ (P ∗ ▷ φ v_r ∗ ▷ φd v_r
            ={ E ∖ ↑fracN .@ l }=∗ ▷ φ v_w ∗ ▷ φd v_w ∗ R true v_r)
         ∗ (∀ v, ■ (v ≠ v_r) → P ∗ ▷ φd v ={ E ∖ ↑fracN .@ l }=∗ R false v)
         ∗ RMWAcqp l q1 φ φd ∗ Initp l q2 ∗ P}}}}
       (CAS #l #v_r #v_w) @ E
    {{{{ (b: bool) (v': Z), RET #b;
         RMWAcqp l q1 φ φd ∗ Initp l q2 ∗ R b v'}}}}%VP.
  Proof.
    iIntros "#Dup".
    intros. iIntros (?) "!#". iIntros (π Φ). iViewUp.
    iIntros "kI kS (CS & CF & RMW & Init & P) Post".

    iDestruct "Init" as (φR' φd') "Init".
    iDestruct (fractor_join_l with "[$RMW $Init]") as "AI".
    iMod (fractor_open with "[$AI]") as (jγ) "(Inv & (AR & IR) & PClose)";
      first auto.
    iPoseProof (RMWAcqRaw_CAS l φ φd P R v_r v_w _ jγ (E ∖ ↑fracN .@ l) with "[]")
      as "RMW"; [solve_ndisj|..].
    { iIntros "!#". iIntros (V v) "?". by iApply ("Dup" $! V v with "[]"). }

    iApply ("RMW" with "[$kI $kS $Inv $AR $IR $P $CS $CF]").
    iNext. iIntros (b v' V') "(VV' & kS' & Inv & AR & IR & R)".
    iMod ("PClose" $! (λ l _ X, RMWAcqRaw V' X ∗ InitRaw V' X)
            with "[$Inv $AR $IR]") as "AI".
     iDestruct (fractor_splitjoin _ _
                  (λ l _ X, RMWAcqRaw V' X)
                  (λ l _ X, InitRaw V' X) with "AI") as "[RMW Init]".
     iApply ("Post" $! V' with "VV' kS'"). iFrame "RMW R".
     by iExists _,_.
  Qed.


  Lemma Acqp_alloc (φ φd: Z → vPred) (E: coPset) (HN: ↑physN ⊆ E):
    {{{{ True }}}}
      Alloc @ E
    {{{{ (l: loc), RET (LitV $ LitLoc l);
        Relp l (1/2) φ φd ∗ Acqp l (1/2) φ φd }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS _ Post". iApply wp_fupd.
    iApply (AcqRaw_alloc φ φd _ _ _ HN with "[$kI $kS]").
    iNext.
    iIntros (l V' n v) "(VV' & kS' & Info & Inv & Rel & Acq)".
    iMod (fractor_alloc E v n l (RSLInv φ φd)
                        (λ l _ jγ, RelRaw V' φ φ jγ ∗ AcqRaw V' φ jγ)
                        with "[$kI $Info $Inv $Rel $Acq]") as "AR"; first auto.
    rewrite -{3}(Qp_div_2 1).
    iDestruct (fractor_splitjoin l _
                (λ l _ jγ, RelRaw V' φ φ jγ)
                (λ l _ jγ, AcqRaw V' φ jγ) with "AR") as "[Rel Acq]".
    iApply ("Post" $! V' with "VV' kS'").
    iSplitL "Rel"; by iExists _.
  Qed.

  Lemma RMWAcqp_alloc (φ φd: Z → vPred) (E: coPset) (HN: ↑physN ⊆ E):
    {{{{ True }}}}
      Alloc @ E
    {{{{ (l: loc), RET (LitV $ LitLoc l);
          Relp l (1/2) φ φd ∗ RMWAcqp l (1/2) φ φd }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS _ Post". iApply wp_fupd.
    iApply (RMWAcqRaw_alloc φ φd _ _ _ HN with "[$kI $kS]").
    iNext. iIntros (l V' n v) "(VV' & kS' & Info & Inv & Rel & RMW)".
    iMod (fractor_alloc E v n l (RSLInv φ φd)
                        (λ l _ jγ, RelRaw V' φ φ jγ ∗ RMWAcqRaw V' jγ)
                        with "[$kI $Info $Inv $Rel $RMW]") as "AR"; first auto.
    rewrite -{3}(Qp_div_2 1).
    iDestruct (fractor_splitjoin l _
                (λ l _ jγ, RelRaw V' φ φ jγ)
                (λ l _ jγ, RMWAcqRaw V' jγ) with "AR") as "[Rel RMW]".
    iApply ("Post" $! V' with "VV' kS'"). iFrame "RMW".
    by iExists _.
  Qed.

  Lemma RMWAcqp_init l v (φ φd: Z → vPred) (E: coPset) (HN: ↑physN ⊆ E):
    {{{{ own_loc l ∗ ▷ φ v ∗ ▷ φd v }}}}
      ([ #l ]_na <- #v) @ E
    {{{{ RET #();
         Relp l (1/2) φ φd ∗ RMWAcqp l ((1/2)/2) φ φd ∗ Initp l ((1/2)/2) }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (oL & Hφ & Hφd) Post". iApply wp_fupd.
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".
    iApply (RMWAcqRaw_init l v _ φ φd _ _ _ HN
            with "[$kI $kS $Hist $Alloc $Hφ $Hφd]").
    iNext. iIntros (V' n) "(VV' & kS' & Inv & Rel & RMW & Init)".
    iMod (fractor_alloc E i n l (RSLInv φ φd)
                    (λ l _ n, RelRaw V' φ φ n ∗ RMWAcqRaw V' n ∗ InitRaw V' n)
          with "[$kI $Info $Inv $Rel $RMW $Init]") as "AR"; [auto|].
    rewrite -{4}(Qp_div_2 1).
    iDestruct (fractor_splitjoin l _
        (λ l _ jγ, RelRaw V' φ φ jγ)
        (λ l _ jγ,  RMWAcqRaw V' jγ ∗ InitRaw V' jγ) with "AR") as "[Rel RMW]".
    rewrite -{5}(Qp_div_2 (1/2)).
    iDestruct (fractor_splitjoin l _
        (λ l _ jγ, RMWAcqRaw V' jγ)
        (λ l _ jγ,  InitRaw V' jγ) with "RMW") as "[RMW Init]".
    iApply ("Post" $! V' with "VV' kS'"). iFrame "RMW".
    iSplitL "Rel"; [by iExists _|by iExists _,_].
  Qed.

  Lemma Acqp_init l v (φ φd: Z → vPred) (E: coPset) (HN: ↑physN ⊆ E):
    {{{{ own_loc l ∗ ▷ φ v ∗ ▷ φd v }}}}
      ([ #l ]_na <- #v) @ E
    {{{{ RET #();
         Relp l (1/2) φ φd ∗ Acqp l ((1/2)/2) φ φd ∗ Initp l ((1/2)/2) }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (oL & Hφ & Hφd) Post". iApply wp_fupd.
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".
    iApply (AcqRaw_init l v _ φ φd _ _ _ HN
            with "[$kI $kS $Hist $Alloc $Hφ $Hφd]").
    iNext. iIntros (V' n) "(VV' & kS' & Inv & Rel & Acq & Init)".
    iMod (fractor_alloc E i n l (RSLInv φ φd)
                   (λ l _ n, RelRaw V' φ φ n ∗ AcqRaw V' φ n ∗ InitRaw V' n)
          with "[$kI $Info $Inv $Rel $Acq $Init]") as "AR"; [auto|].
    rewrite -{4}(Qp_div_2 1).
    iDestruct (fractor_splitjoin l _
        (λ l _ jγ, RelRaw V' φ φ jγ)
        (λ l _ jγ,  AcqRaw V' φ  jγ ∗ InitRaw V' jγ) with "AR") as "[Rel Acq]".
    rewrite -{5}(Qp_div_2 (1/2)).
    iDestruct (fractor_splitjoin l _
        (λ l _ jγ, AcqRaw V' φ jγ)
        (λ l _ jγ,  InitRaw V' jγ) with "Acq") as "[Acq Init]".
    iApply ("Post" $! V' with "VV' kS'").
    iSplitL "Rel"; last iSplitL "Acq"; by [iExists _|iExists _|iExists _,_].
  Qed.

  Lemma Acqp_join l (φ1 φ2 φd: Z → vPred) q1 q2 (E : coPset)
      (HNl : ↑fracN .@ l ⊆ E):
      (Acqp l q1 φ1 φd ∗ Acqp l q2 φ2 φd 
          ={E}=> Acqp l (q1+q2) (λ v, φ1 v ∗ φ2 v) φd)%VP.
  Proof.
    constructor => ? V _.
    iIntros "(AR1 & AR2)".
    iDestruct "AR1" as (φR) "AR1". iDestruct "AR2" as (φR') "AR2".
    iDestruct (fractor_join_l l with "[$AR1 $AR2]") as "AR".
    iMod (fractor_open with "AR") as (n) "(Inv & (AR1 & AR2) & PClose)";
      first auto.
    iMod (AcqRaw_join l with "[$Inv $AR1 $AR2]") as "(Inv & AR)".
    iMod ("PClose" $! (λ l _ n, AcqRaw V (λ v : Z, (φ1 v ∗ φ2 v)%VP) n)
          with "[$Inv $AR]") as "AR".
    by iExists _.
  Qed.

  Lemma Acqp_split l (φ1 φ2 φd: Z → vPred) q1 q2 (E : coPset)
      (HNl : ↑fracN .@ l ⊆ E):
      (Acqp l (q1+q2) (λ v, φ1 v ∗ φ2 v)%VP φd
        ={E}=> Acqp l q1 φ1 φd ∗ Acqp l q2 φ2 φd)%VP.
  Proof.
    constructor => ? V _.
    iIntros "AR". iDestruct "AR" as (φR) "AR".
    iMod (fractor_open with "AR") as (n) "(Inv & AR & PClose)";
      first auto.
    iMod (AcqRaw_split l with "[$Inv $AR]") as "(Inv & AR1 & AR2)".
    iMod ("PClose" $! (λ l _ n, AcqRaw V φ1 n ∗ AcqRaw V φ2 n)
          with "[$Inv $AR1 $AR2]") as "AR".
    iDestruct (fractor_splitjoin _ _
                (λ l _ jγ, AcqRaw V φ1 jγ)
                (λ l _ jγ, AcqRaw V φ2 jγ) with "AR") as "(AR1 & AR2)".
    iSplitL "AR1"; by iExists _.
  Qed.

  Lemma Relp_split l (φ1 φ2 φd: Z → vPred) q1 q2:
    Relp l (q1+q2) (λ v, φ1 v ∨ φ2 v)%VP φd ⊧  Relp l q1 φ1 φd ∗ Relp l q2 φ2 φd.
  Proof.
    constructor => ? V _.
    iIntros "RR". iDestruct "RR" as (φR) "RR".
    iDestruct (fractor_mono l _ _
                (λ l _ jγ, RelRaw V φ1 φR jγ ∗ RelRaw V φ2 φR jγ)
                with "[$RR]") as "RR".
    { iIntros (n) "RR". iApply (RelRaw_split with "RR"). }
    iDestruct (fractor_splitjoin _ _
                (λ l _ jγ, RelRaw V φ1 φR jγ)
                (λ l _ jγ, RelRaw V φ2 φR jγ) with "RR") as "(RR1 & RR2)".
    iSplitL "RR1"; by iExists _.
  Qed.

  Lemma Relp_join l (φ1 φ2 φd: Z → vPred) q1 q2:
    Relp l q1 φ1 φd ∗ Relp l q2 φ2 φd ⊧ Relp l (q1+q2) (λ v, φ1 v ∨ φ2 v)%VP φd.
  Proof.
    constructor => ? V _. iIntros "(RR1 & RR2)".
    iDestruct "RR1" as (φR) "RR1". iDestruct "RR2" as (φR') "RR2".
    iDestruct (fractor_join_l l with "[$RR1 $RR2]") as "RR".
    iDestruct (fractor_mono l _ _
                (λ l _ jγ, RelRaw V (λ v : Z, (φ1 v ∨ φ2 v)%VP) φR jγ )
                with "[$RR]") as "RR".
    { iIntros (n) "RR". iApply (RelRaw_join with "RR"). }
    by iExists _.
  Qed.

  Lemma RMWAcqp_split l (φ φd: Z → vPred) q1 q2 (E : coPset)
    (HNl: ↑fracN .@ l ⊆ E):
    (RMWAcqp l (q1+q2) φ φd
      ={E}=> RMWAcqp l q1 φ φd ∗ Acqp l q2 (λ _, True) φd)%VP.
  Proof.
    constructor => ? V _. iIntros "RMW".
    iMod (fractor_open with "RMW") as (n) "(Inv & RMW & PClose)"; first auto.
    iMod (RMWAcqRaw_split with "[$Inv $RMW]") as "(Inv & RMW & Acq)".
    iMod ("PClose" $! (λ _ _ n, RMWAcqRaw V n ∗ AcqRaw V (λ _, True)%VP n)
      with "[$Inv $RMW $Acq]") as "AR".
    iDestruct (fractor_splitjoin _ _
              (λ _ _ jγ, RMWAcqRaw V jγ)
              (λ _ _ jγ, AcqRaw V (λ _, True%VP) jγ) with "AR") as "(RMW & Acq)".
    iFrame "RMW". by iExists _.
  Qed.

  End Frac.

Close Scope I.

End proof.
