From mathcomp Require Import ssreflect ssrbool seq.
From stdpp Require Export strings list numbers sorting gmap finite mapset.

Global Generalizable All Variables.
Global Set Automatic Coercions Import.
Global Set Asymmetric Patterns.
Global Set Bullet Behavior "Strict Subproofs".
From Coq Require Export Utf8.

Open Scope positive.

From igps Require Export infrastructure types.

Notation loc := (positive) (only parsing).
Section RAMachine.
  Set Default Proof Using "Type*".

  Implicit Type (x : loc) (t : positive) (R V : View) (m : message).

  Section Machine.
    Definition msg_disj m m' := m.(mloc) ≠ m'.(mloc) ∨ m.(mtime) ≠ m'.(mtime).
    Global Arguments msg_disj _ _ /.
    Lemma msg_disj_symm m1 m2 : msg_disj m1 m2 -> msg_disj m2 m1.
    Proof. now firstorder. Qed.

    Notation MessageSet := (gset message).

    Definition MS_disj (M M' : MessageSet) :=
      ∀ m m', m ∈ M -> m' ∈ M' -> msg_disj m m'.

    Definition MS_msg_disj (M : MessageSet) m :=
      ∀ m', m' ∈ M -> msg_disj m m'.

    Definition pairwise_disj (M : MessageSet) :=
      ∀ m m', m ∈ M -> m' ∈ M -> m = m' ∨ msg_disj m m'.

    Lemma pairwise_disj_unique (M: MessageSet) {Disj: pairwise_disj M}
      : ∀ m m', m ∈ M → m' ∈ M → mloc m = mloc m' → mtime m = mtime m' → m = m'.
    Proof.
      move => m m' Inm Inm' lEq tEq.
      destruct (Disj _ _ Inm Inm') as [|[|]] => //.
    Qed.

    Lemma pairwise_disj_MS (M: MessageSet) m:
      pairwise_disj M -> MS_msg_disj M m -> pairwise_disj (M ∪ {[m]}).
    Proof.
      move => PDisj MDisj m1 m2 
              /elem_of_union [In1|/elem_of_singleton Eq1]
              /elem_of_union [In2|/elem_of_singleton Eq2].
      - destruct (PDisj m1 m2 In1 In2); abstract naive_solver.
      - subst. right. destruct (MDisj _ In1); abstract naive_solver.
      - subst. right. destruct (MDisj _ In2); abstract naive_solver.
      - subst. by left.
    Qed.


    Definition msg_ok (m : message) := m.(mview) !! m.(mloc) = Some m.(mtime).
    Global Arguments msg_ok _ /.

    (* M justifies V *)
    Definition view_ok (M : MessageSet) V :=
      ∀ x t, V !! x = Some t → ∃ m, m ∈ M ∧ m.(mloc) = x ∧ m.(mtime) = t ∧ m.(mview) ⊑ V.

    Definition View_ok (M : MessageSet) (T : View) :=
      ∀ x t, T !! x = Some t → ∃ m, m ∈ M ∧ m.(mloc) = x ∧ m.(mtime) = t.
    Definition View_complete (M : MessageSet) (T : View) :=
      ∀ m, m ∈ M → is_Some (T !! m.(mloc)).

    (* MaxTime represents a message in the memory M and a corresponding proof
      that it is maximal for its location.
      - MaxTime M x (Some t) holds if such a maximal message exists 
        for location x and its timestamp is t.
      - MaxTime M x None holds if no message exists for x.
    *)
    Notation of_loc := (λ (x : loc) (M : MessageSet), filter ((= x) ∘ mloc) M).
    Definition MaxTime (M : MessageSet) (x : loc) :=
      MaxDep (rel_of mtime (≤)) mtime (of_loc x M).
    Definition MT_None {M : MessageSet} {x} HEmp : MaxTime M x None
      := maxdep (max_none HEmp) eq_refl.
    Definition MT_Some {M : MessageSet} {x t} m
         (EqT : mtime m = t)
         (HIn : m ∈ filter ((= x) ∘ mloc) M)
         (HRel : ∀ m', m' ∈ of_loc x M -> mtime m' ≤ mtime m) :
      MaxTime M x (Some t).
    Proof.
      apply: (maxdep (max_some m HIn HRel)). by rewrite -EqT /=.
    Defined.

    Global Arguments MT_None [_ _] _.
    Global Arguments MT_Some [_ _ _] _ _ _ _.

    Lemma MT_equiv_loc {M1 M2 x ot} : 
      MaxTime M1 x ot →
      of_loc x M1 ≡ of_loc x M2 →
      MaxTime M2 x ot.
    Proof. exact: MaxDep_equiv. Qed.

    (* TODO : MaxTime with equivalence *)
    Lemma MT_equiv M1 M2 x ot : MaxTime M1 x ot → M1 ≡ M2 → MaxTime M2 x ot.
    Proof. intros ? Eq. apply: MT_equiv_loc; first eauto. by rewrite Eq. Qed.

    Definition MT_empty `(Max : MaxTime M x None) : of_loc x M ≡ ∅
      := MaxDep_emp Max.

    Definition MT_msg `(Max : MaxTime M x (Some t)) : message := of_MaxDep Max.

    Definition MT_msg_loc `(Max : MaxTime M x (Some t)) : mloc (MT_msg Max) = x.
    Proof. destruct Max, m; first done. unfold MT_msg. abstract set_solver+. Qed.

    Definition MT_msg_time `(Max : MaxTime M x (Some t)) : mtime (MT_msg Max) = t.
    Proof. destruct Max, m; first done. unfold MT_msg. abstract set_solver+. Qed.

    Lemma MT_msg_In_of_loc `(Max : MaxTime M x (Some t))
      : MT_msg Max ∈ of_loc x M.
    Proof. unfold MT_msg, of_MaxDep. case_match. exact: (proj1 a). Qed.

    Lemma MT_msg_In' {M x ot} :
      ∀ Max : MaxTime M x ot,
      match ot as ot' return MaxTime M x ot' → Prop with
        Some t => λ Max, MT_msg Max ∈ M
      | None => λ Max, ∀ m, m ∉ of_loc x M
      end Max.
    Proof.
      destruct ot.
      - induction Max, m. discriminate. unfold MT_msg. set_solver+.
      - intros. by rewrite MT_empty.
    Qed.

    Lemma MT_msg_In `(Max : MaxTime M x (Some t)) : MT_msg Max ∈ M.
    Proof. exact (MT_msg_In' Max). Qed.

    Hint Extern 0 (mloc (@MT_msg _ ?x _ _) = ?x) => apply MT_msg_loc.
    Hint Extern 0 (mtime (@MT_msg _ _ ?t _) = ?t) => apply MT_msg_time.
    Hint Extern 0 (Some ?t = mtime <$> of_Max ?m) => apply eq_of_MaxDep.
    Hint Extern 0 ((@MT_msg ?M _ _ ?Max) ∈ ?M) => apply (MT_msg_In Max).
    Hint Extern 0 ((@MT_msg ?M ?x _ ?Max) ∈ of_loc ?x ?M) => apply (MT_msg_In_of_loc Max).

    Lemma MT_max `(Max : MaxTime M x ot) :
      ∀ m, m ∈ of_loc x M -> Some (mtime m) ⊑ ot.
    Proof.
      destruct Max, m; rewrite EqO; simpl.
      - by setoid_rewrite HEmp.
      - exact HRel.
    Qed.

    Lemma MT_msg_max_filter `(Max : MaxTime M x ot) :
      of_loc x M ≡ of_loc x {[ m <- M | Some (mtime m) ⊑ ot ]}.
    Proof.
      set_unfold => m. have := (MT_max Max m). abstract set_solver+.
    Qed.

    Lemma MT_msg_max `(Max : MaxTime M x (Some t)) :
      ∀ m, m ∈ M -> mloc m = x -> mtime m ≤ mtime (MT_msg Max).
    Proof. intros. rewrite MT_msg_time. apply (MT_max Max). set_solver. Qed.

    Lemma MT_Some_Lt `(Max : MaxTime M x (Some t)) :
       ∀ m, m ∈ M -> m.(mloc) = x -> m.(mtime) ⊑ t.
    Proof. rewrite <-(MT_msg_time Max). exact: MT_msg_max. Qed.

    Definition MT_fun `(Max1 : MaxTime M x ot1) `(Max2 : MaxTime M x ot2)
      : ot1 = ot2.
    Proof.
      destruct ot1 as [t1|], ot2 as [t2|] => //;
      rewrite -?(MT_msg_time Max1) -?(MT_msg_time Max2).
      - f_equal. apply: anti_symm; apply: MT_msg_max; auto.
      - exfalso. apply: (MT_msg_In' Max2). exact: (MT_msg_In_of_loc).
      - exfalso. apply: (MT_msg_In' Max1). exact: (MT_msg_In_of_loc).
    Qed.

    Lemma MT_pred_mono 
      `{∀ m, Decision (P1 m)} `{∀ m, Decision (P2 m)} {M x ot1 ot2} 
      : MaxTime (filter P1 M) x ot1 -> MaxTime (filter P2 M) x ot2 
        -> (∀ m, P1 m -> P2 m) -> ot1 ⊑ ot2.
    Proof.
      case ot1, ot2 => MT1 MT2 HP //.
      - rewrite -(MT_msg_time MT1) -(MT_msg_time MT2).
        apply: MT_msg_max; last exact: MT_msg_loc.
        move: (MT_msg_In MT1). abstract set_solver+HP.
      - exfalso. apply: (MT_msg_In' MT2 (MT_msg MT1)).
        move: (MT_msg_In_of_loc MT1). abstract set_solver+HP.
    Qed.

    Lemma MT_set_mono {M1 M2 x ot1 ot2} :
      MaxTime M1 x ot1 -> MaxTime M2 x ot2 -> MaxTime (M1 ∪ M2) x (ot1 ⊔ ot2).
    Proof.
      move: ot1 ot2 => [t1|] [t2|] MT1 MT2; cbn.
      - apply: (MT_Some (if decide (t1 < t2) then MT_msg MT2 else MT_msg MT1)).
        + case: (decide _) => ?; rewrite MT_msg_time; symmetry.
          * apply: Pos.max_r. by apply Pos.lt_le_incl.
          * apply: Pos.max_l. by apply Pos.le_nlt.
        + case: (decide _) => ? /=.
          * move: (MT_msg_In_of_loc MT2). abstract set_solver+.
          * move: (MT_msg_In_of_loc MT1); abstract set_solver+.
        + case: (decide _) => ? /=; set_unfold ; intuition; rewrite MT_msg_time.
          * etrans. apply: (MT_Some_Lt MT1) => //. by apply Pos.lt_le_incl.
          * apply: (MT_Some_Lt) => //.
          * apply: (MT_Some_Lt) => //.
          * etrans. apply: (MT_Some_Lt MT2) => //. by apply Pos.le_nlt.
      - apply (MT_equiv_loc MT1). rewrite gset_filter_union (MT_empty MT2).
        abstract set_solver+.
      - apply (MT_equiv_loc MT2). rewrite gset_filter_union (MT_empty MT1).
        abstract set_solver+.
      - apply MT_None. rewrite gset_filter_union (MT_empty MT1) (MT_empty MT2).
        abstract set_solver+.
    Qed.

    Lemma MT_mono {M1 M2 x ot} :
      MaxTime M1 x ot -> MaxTime M2 x ot -> MaxTime (M1 ∪ M2) x ot.
    Proof.
      rewrite {3}(_ : ot = ot ⊔ ot);
      [exact: MT_set_mono|solve_jsl].
    Qed.

    Lemma MT_mono_l {M1 M2 x ot} :
     ∀ Max : MaxTime M1 x ot, (∀ m, m ∈ of_loc x M2 -> Some (mtime m) ⊑ ot) -> MaxTime (M1 ∪ M2) x ot.
    Proof.
      intros. destruct ot.
      - apply: MT_Some.
        + exact: MT_msg_time.
        + rewrite gset_filter_union. apply/elem_of_union. left.
          exact: MT_msg_In_of_loc.
        + rewrite gset_filter_union. move => ? /elem_of_union [].
          move => ?. apply MT_msg_max; try abstract set_solver.
          rewrite MT_msg_time. exact: H.
      - apply: MT_None. rewrite gset_filter_union (MT_empty Max).
        abstract set_solver.
    Qed.

    Lemma MT_impl `{fhl1 : @FH_list (message) _ _ M1}
                  `{fhl2 : @FH_list (message) _ _ M2} {x ot} :
      MaxTime (fh_term _ fhl1) x ot ->
      lfold M1 $ lPred ((= x) ∘ mloc) :: fh_list _ fhl1
        ≡ lfold M2 $ lPred ((= x) ∘ mloc) :: fh_list _ fhl2 ->
      MaxTime (fh_term _ fhl2) x ot.
    Proof.
      move => /= M H. apply: (MT_equiv_loc M). rewrite !fh_eq. by rewrite H.
    Qed.


    Record memory :=
      mkMemory {
          msgs :> MessageSet;
          _ : pairwise_disj msgs
        }.

    Definition memory_ok : ∀ M, pairwise_disj M.(msgs) := ltac:(by destruct 0).

    Program Definition add_ins (M : memory) m : MS_msg_disj M m -> memory :=
      fun H => mkMemory (msgs M ∪ {[m]}) _.
    Next Obligation.
      intros M m ? m1 m2.
      move => /elem_of_union [El1|/elem_of_singleton Eq1];
                move => /elem_of_union [El2|/elem_of_singleton Eq2].
      - case: (memory_ok _ _ _ El1 El2); by [left | right].
      - rewrite -> Eq2. right. apply: msg_disj_symm. auto.
      - rewrite -> Eq1. right; auto.
      - rewrite -> Eq1, Eq2. by left.
    Qed.

    Lemma MT_add_ins_old (M M2 : MessageSet) x ot
          (NEq : of_loc x M2 ≡ ∅)
      : MaxTime M x ot -> MaxTime (M ∪ M2) x ot.
    Proof.
      move => MT1. apply (MT_equiv_loc MT1); rewrite gset_filter_union NEq.
      abstract set_solver+.
    Qed.

    Inductive thread_red (V : View) (M : memory) 
    : option positive -> option positive -> event -> View -> memory -> Prop :=
    | Th_Read m acc
              (InM : m ∈ msgs M)
              (Lt : ∃t', V !! m.(mloc) = Some t' ∧ t' ≤ m.(mtime))
      : thread_red  V M (Some (mtime m)) None
                    (ERead acc m.(mloc) m.(mval)) (V ⊔ m.(mview)) M
    | Th_Write x v t V' m acc
               (Lt : V !! x ⊏ Some t)
               (EV' : V' = V ⊔ {[x := t]})
               (Em : m = < x → v @ t, V' >)
               (Disj : MS_msg_disj M m)
      : thread_red  V M None (Some t)
                    (EWrite acc x v) V' (add_ins M m Disj)
    | Th_Update x v_r t R V' m v_w
                (InM : < x → v_r @ t, R > ∈ msgs M)
                (Lt : ∃t', V !! x = Some t' ∧ t' ≤ t)
                (EV' : V' = V ⊔ {[x := t+1]} ⊔ R)
                (Em : m = < x → v_w @ t + 1, V' >)
                (Disj : MS_msg_disj M m)
      : thread_red  V M (Some t) (Some (t+1))
                    (EUpdate x v_r v_w) V' (add_ins M m Disj)
    .

    Record state :=
      mkState {
          mem : memory;
          nats : View
        }.

    Implicit Type (σ : state).

    Global Instance memory_Inhabited : Inhabited memory.
    Proof.
      econstructor.
      - apply: mkMemory.
        move => ? ? /elem_of_empty [].
    Qed.

    Global Instance state_Inhabited : Inhabited state := _.
    Proof.
      do 2!econstructor; exact: inhabitant. 
    Qed.

    Definition msgs_ok M := ∀ m, m ∈ msgs M → msg_ok m ∧ view_ok (msgs M) m.(mview).
    (* Definition thread_ok σ V :=view_ok (mem σ) V. *)
    Notation view_mono V V' := (V ⊑ V').
    Definition thread_inv (V : View) (M : memory) := msgs_ok M ∧ view_ok M V.
    Global Arguments msgs_ok _ /.
    (* Global Arguments thread_ok _ _ /. *)
    Global Arguments thread_inv _ _ /.

    Definition msgs_mono M M' := msgs M ⊆ msgs M'.

    Section ThreadReduction.

      Lemma view_ok_mono : Proper ((⊆) ==> (=) ==> impl) view_ok.
      Proof.
        move => M1 M2 /elem_of_subseteq HM V1 V2 <- {V2} VOk x t HVt2.
        edestruct (VOk) as (m & A & B & C & D); first eassumption.
        exists m; repeat split; by auto.
      Qed.

      Lemma lookup_join (X Y : View) x ot : 
        (X ⊔ Y) !! x = ot → X !! x = ot ∨ Y !! x = ot.
      Proof.
        rewrite lookup_union_with.
        case : (X !! x) => [tX|];
                             case : (Y !! x) => [tY|]; cbn; try auto.
        move => <-. case: (Pos.max_dec tX tY); [left|right]; f_equal; auto.
      Qed.

      Lemma lookup_join_max (X Y : View) x : (X ⊔ Y) !! x = X !! x ⊔ Y !! x.
      Proof.
        rewrite lookup_union_with.
        case : (X !! x) => [tX|];
        case : (Y !! x) => [tY|]; cbn; by auto.
      Qed.

      Lemma view_join_assoc V R S : V ⊔ (R ⊔ S) = (V ⊔ R) ⊔ S.
      Proof.
        apply: map_eq => x.
        rewrite !lookup_union_with.
        repeat case : (_ !! x) => [?|]; cbn; try auto.
        f_equal. solve_jsl.
      Qed.
      
      Lemma msg_ok_write V (x : loc) v t
        (Lt : V !! x ⊏ Some t)
        : msg_ok (<x→v@t,(V ⊔ {[x := t]})>).
      Proof.
        cbv [msg_ok] => /=. rewrite lookup_join_max lookup_singleton.
        apply: anti_symm; [rewrite -> Lt|]; solve_jsl.
      Qed.

      Lemma view_ok_write (M : memory) V x v t
        (VOk : view_ok M V)
        (Disj : MS_msg_disj M (<x→v@t,(V ⊔ {[x := t]})>))
        : view_ok (add_ins M (<x→v@t,(V ⊔ {[x := t]})>) Disj)
                  (mview (<x→v@t,(V ⊔ {[x := t]})>)).
      Proof.
        move => z tz /lookup_join [/VOk|/lookup_singleton_Some [<- <-]].
        - move => [[y vy ty Ry] /= [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
          exists (<z → vy @ tz, Ry>) => /=.
          repeat (split; first try (reflexivity || eassumption)).
          + apply elem_of_union; by left.
          + rewrite Hm4. solve_jsl.
        - eexists; repeat split;
            first (apply/elem_of_union; right; by apply elem_of_singleton);
            by auto.
      Qed.

      Lemma msg_ok_update M V R x v_r v_w t
        (MOk : msg_ok (<x→v_r@t,R>))
        (InM : <x→v_r@t,R> ∈ msgs M)
        (Lt : ∃t', V !! x = Some t' ∧ t' ≤ t)
        : msg_ok (<x→v_w@(t + 1),(V ⊔ {[x := t + 1]} ⊔ R)>).
      Proof with eauto using Pos.lt_add_diag_r, Pos.lt_le_incl.
        cbv [msg_ok] => /=. rewrite !lookup_join_max lookup_singleton MOk.
        case HVx : (V !! _) => [?|].
        - destruct Lt as [t' [Eq Lt]]. rewrite Eq in HVx. inversion HVx.
          subst. cbn. f_equal.
          apply: Pos.le_antisym.
          + repeat apply: Pos.max_lub.
            * transitivity t...
            * reflexivity.
            * trivial ...
          + rewrite (_ : _ ⊔ (t + 1) = t + 1); 
              first rewrite (_ : t + 1 ⊔ t = t + 1).
            * reflexivity.
            * apply/Pos.max_l_iff...
            * apply/Pos.max_r_iff. transitivity t...
        - cbn. f_equal. apply/Pos.max_l_iff...
      Qed.

      Lemma view_ok_update M V R x v_r v_w t
        (InM : <x→v_r@t,R> ∈ msgs M)
        (VOk : view_ok M V)
        (ROk : view_ok M R)
        (Disj : MS_msg_disj M (<x→v_w@(t + 1),(V ⊔ {[x := t + 1]} ⊔ R)>))
        : view_ok (add_ins M (<x→v_w@(t + 1),(V ⊔ {[x := t + 1]} ⊔ R)>) Disj)
                  (mview (<x→v_w@(t + 1),(V ⊔ {[x := t + 1]} ⊔ R)>)).
      Proof.
        move => z tz /lookup_join
                [/lookup_join [/VOk|/lookup_singleton_Some [<- <-]]|/ROk].
        - move => [[y vy ty Ry] /= [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
          exists (<z → vy @ tz, Ry>) => /=.
          repeat (split; first try (reflexivity || eassumption)).
          + apply elem_of_union; by left.
          + rewrite Hm4. solve_jsl.
        - eexists; repeat split;
            first (apply/elem_of_union; right; by apply elem_of_singleton);
            by auto.
        - move => [[y vy ty Ry] /= [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
          exists (<z → vy @ tz, Ry>) => /=. 
          repeat (split; first try (reflexivity || eassumption)).
          + apply elem_of_union; by left.
          + rewrite Hm4. solve_jsl.
      Qed.

      Lemma msg_ok_update_new M V x v_w t
        (Empty: ∀ m, m ∈ msgs M → mloc m ≠ x)
        (VOk : view_ok M V)
        : msg_ok (<x→v_w@t,(V ⊔ {[x := t]})>).
      Proof.
        cbv [msg_ok] => /=. rewrite !lookup_join_max lookup_singleton.
        case HVx : (V !! _) => [?|//].
        destruct (VOk _ _ HVx) as (m & Hm1 & Hm2 & _).
        exfalso. by apply (Empty m).
      Qed.

      Lemma view_ok_update_new M V x v_w t
        (Empty: ∀ m, m ∈ msgs M → mloc m ≠ x)
        (VOk : view_ok M V)
        (Disj : MS_msg_disj M (<x→v_w@t,(V ⊔ {[x := t]})>))
        : view_ok (add_ins M (<x→v_w@t,(V ⊔ {[x := t]})>) Disj)
                  (mview (<x→v_w@t,(V ⊔ {[x := t]})>)).
      Proof.
        move => z tz /lookup_join [/VOk|/lookup_singleton_Some [<- <-]].
        - move => [[y vy ty Ry] /= [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
          exists (<z → vy @ tz, Ry>) => /=.
          repeat (split; first try (reflexivity || eassumption)).
          + apply elem_of_union; by left.
          + rewrite Hm4. solve_jsl.
        - eexists; repeat split;
            first (apply/elem_of_union; right; by apply elem_of_singleton);
            by auto.
      Qed.

      Lemma msgs_ok_add_ins x v t M V m' R
        (MOk : msgs_ok M)
        (VOk : view_ok M V)
        (Disj : MS_msg_disj M (<x→v@t,(V ⊔ R)>))
        : m' ∈ msgs M → msg_ok m' ∧ view_ok (add_ins M _ Disj) (mview m').
      Proof.
        move/MOk => []; split; first by auto.
        apply: view_ok_mono; last by eassumption.
        { abstract set_solver+. }
        { reflexivity. }
      Qed.

      Lemma thread_red_safe V M otr otw evt V' M':
        thread_inv V M → thread_red V M otr otw evt V' M' → thread_inv V' M'.
      Proof.
        move => [MOk VOk].
        induction 1; subst.
        - split; first assumption.
          move => y ty Hyty.
          case/lookup_join: Hyty.
          + move/VOk => [[? vy ? Ry] /= [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
            exists (<y → vy @ ty, Ry>) => /=. 
            repeat (split; first (reflexivity || eassumption)).
            rewrite Hm4. solve_jsl.
          + move/MOk : InM => [_] ROk /ROk [[? vy ? Ry] /=
                        [Hm1 [Hm2 [Hm3 Hm4]]]]; subst.
            exists (<y → vy @ ty, Ry>) => /=. 
            repeat (split; first (reflexivity || eassumption)).
            rewrite Hm4. solve_jsl.
        - split.
          + move => m' /elem_of_union [|/elem_of_singleton ->].
            * exact: msgs_ok_add_ins.
            * split; [exact: msg_ok_write|exact: view_ok_write].
          + exact: view_ok_write.
        - case: (MOk _ InM) => [? ?].
          split.
          + move => m' /elem_of_union [|/elem_of_singleton ->].
            * rewrite -view_join_assoc. exact: msgs_ok_add_ins.
            * split; [exact: msg_ok_update|exact: view_ok_update].
          + exact: view_ok_update.
      Qed.

      Lemma add_ins_mono M m Disj : msgs M ⊆ msgs (add_ins M m Disj).
      Proof.
        move => ? ?. apply/elem_of_union. by left.
      Qed.

      Lemma thread_red_msgs_mono V M otr otw evt V' M' : 
        thread_red V M otr otw evt V' M' → msgs_mono M M'.
      Proof.
        inversion 1; subst.
        - by rewrite /msgs_mono.
        - exact: add_ins_mono.
        - exact: add_ins_mono.
      Qed.

      Lemma thread_red_view_mono  V M otr otw evt V' M' :
        thread_inv V M → thread_red V M otr otw evt V' M' → view_mono V V'.
      Proof.
        move => ThrdInv. inversion 1; subst; solve_jsl.
      Qed.

    End ThreadReduction.

    Section DataRaces.
      Inductive drf_red (σ σ' : state) (V : View) :
        option positive → option positive → event → Prop :=
      | drf_read_at tl otna x v otr
                    (LV : V !! x = Some tl)
                    (LNA : (nats σ) !! x = otna)
                    (GE : otna ⊑ Some tl)
                    (Hσ' : nats σ' = nats σ)
        : drf_red σ σ' V otr None (ERead at_hack x v)
      | drf_read_na tl x v t
                    (LV : V !! x = Some tl)
                    (GE : Some t ⊑ Some tl)
                    (Hσ' : nats σ' = nats σ)
                    (Max: MaxTime (mem σ) x (Some t))
        : drf_red σ σ' V (Some t) None (ERead na x v)
      | drf_update tl otna x v_r v_w otr otw
                   (LV : V !! x = Some tl)
                   (LNA : (nats σ) !! x = otna)
                   (GE : otna ⊑ Some tl)
                   (Hσ' : nats σ' = nats σ)
        : drf_red σ σ' V otr otw (EUpdate x v_r v_w)
      | drf_write_at tl otna x v otw
                     (LV : V !! x = Some tl)
                     (LNA : (nats σ) !! x = otna)
                     (GE : otna ⊑ Some tl)
                     (Hσ' : nats σ' = nats σ)
        : drf_red σ σ' V None otw (EWrite at_hack x v)
      | drf_write_na otl otna t x v
                     (LV : V !! x = otl)
                     (LNA : (nats σ) !! x = otna)
                     (LE : otna ⊑ otl)
                     (Max' : MaxTime (mem σ') x (Some t))
                     (LT' : otl ⊏ Some t)
                     (Hσ' : nats σ' = <[x:=t]>(nats σ))
        : drf_red σ σ' V None (Some t) (EWrite na x v)
      .

      Definition nats_ok (M : MessageSet) nats := View_ok (M) (nats).
      Definition nats_complete (M : MessageSet) nats := View_complete M nats.

      Lemma nats_ok_mono : Proper ((⊆) ==> (=) ==> impl) nats_ok.
      Proof.
        move => M1 M2 /elem_of_subseteq HM V1 V2 <- {V2} NOk x t HVt2.
        edestruct NOk as (m & ? & ? & ?); first eassumption.
        exists m; repeat split; by auto.
      Qed.

      Lemma nats_complete_mono : Proper ((=) ==> (⊑) ==> impl) nats_complete.
      Proof.
        move => M1 M2 <- V1 V2 Subs NCom m /NCom [] t Hmt.
        eapply option_ext_is_Some; by eauto.
      Qed.

      Lemma nats_complete_add_ins (M : memory) m (D : MS_msg_disj M m) T :
        (∃ m', m' ∈ msgs M ∧ m'.(mloc) = m.(mloc))
        → nats_complete M T 
        → nats_complete (add_ins M _ D) T.
      Proof.
        move => [m' [In EqLoc]] NCom m'' /=.
        move/elem_of_union => [/NCom //|/elem_of_singleton ->].
        rewrite -EqLoc. exact: NCom.
      Qed.

      Lemma drf_red_safe σ σ' V otr otw e (Mono: msgs_mono (mem σ) (mem σ')):
        drf_red σ σ' V otr otw e
        → nats_ok (mem σ) (nats σ)
        → nats_ok (mem σ') (nats σ').
      Proof.
        inversion 1; subst; move => Inv y t';
          try (rewrite Hσ' => /Inv [? [? [? ?]]];
                eexists; repeat split; first exact: Mono; by assumption).
        rewrite Hσ' lookup_insert_Some. move => [[Eq Eq']|[_ /Inv]].
        - subst. eexists; repeat split; first apply (MT_msg_In Max'); auto. 
        - move => [? [? [? ?]]].
          eexists; repeat split; first (exact: Mono); by assumption.
      Qed.

      Lemma drf_red_nats_mono σ σ' V otr otw e
        (Mono: msgs_mono (mem σ) (mem σ'))
        (HNO: nats_ok (mem σ) (nats σ))
        (Hdrf: drf_red σ σ' V otr otw e):
        nats σ ⊑ nats σ'.
      Proof.
        inversion Hdrf; rewrite Hσ' => //.
        move => x'.
        case (decide (x' = x)) => [Eq|NEq];
          last by rewrite lookup_insert_ne.
        destruct (nats σ !! x) as [tl|] eqn:Hna;
          simplify_option_eq; rewrite lookup_insert Hna => //.
        destruct (HNO x tl Hna) as [m [Hm1 [Hm2 Hm3]]].
        rewrite -(MT_msg_time Max') -Hm3. apply MT_msg_max; auto.
      Qed.

    End DataRaces.

    Section Allocation.
      Inductive allocated (M : MessageSet) x t : Prop :=
      | is_allocated t_d t_a
                     (TD : MaxTime (filter (λ m, (m.(mval) = D)) M) x t_d)
                     (TA : MaxTime (filter (λ m, (m.(mval) = A)) M) x (Some t_a))
                     (Alloc : t_d ⊏ Some t_a)
                     (Local: t_a ⊑ t).

      Inductive deallocated (M : MessageSet) x ot : Prop :=
      | is_deallocated t_d t_a
                       (TD : MaxTime (filter (λ m, (m.(mval) = D)) M) x t_d)
                       (TA : MaxTime (filter (λ m, (m.(mval) = A)) M) x t_a)
                       (DeAlloc : t_a ⊑ t_d)
                       (Local: t_d ⊑ ot).

      Inductive initialized (M : MessageSet) x t : Prop :=
      | is_initialized t_d t_a
                       (TD : MaxTime (filter (λ m, (m.(mval) = D)) M) x t_d)
                       (TA : MaxTime (filter (λ m, (m.(mval) = A)) M) x (Some t_a))
                       (Alloc : t_d ⊏ Some t_a)
                       (Local: t_a ⊏ t).

      Inductive alloc_red (M : MessageSet) (V : View) (x : loc)
      : event -> Prop :=
      | alloc_read acc v' t
                   (LT : V !! x = Some t)
                   (Init : initialized M x t)
        : alloc_red M V x (ERead acc x (VInj v'))
      | alloc_write acc v t
                    (LT : V !! x = Some t)
                    (Alloc : allocated M x t)
        : alloc_red M V x (EWrite acc x (VInj v))
      | alloc_cas_succ v_r v_w t
                       (LT : V !! x = Some t)
                       (Init : initialized M x t)
        : alloc_red M V x (EUpdate x (VInj v_r) (VInj v_w))
      | alloc_alloc
                       (Fresh: of_loc x M ≡ ∅)
        : alloc_red M V x (EWrite na x A)
      | alloc_dealloc t
                    (LT : V !! x = Some t)
                    (Alloc : allocated M x t)
        : alloc_red M V x (EWrite na x D)
      .

      Lemma init_is_alloc M x t : initialized M x t → allocated M x t.
      Proof.
        inversion 1. econstructor; eauto.
        exact: Pos.lt_le_incl.
      Qed.

      Lemma alloc_time_mono M x t1 t2:
        allocated M x t1
        → t1 ⊑ t2
        → allocated M x t2.
      Proof.
        inversion 1. econstructor; eauto.
        etrans; by eauto.
      Qed.

      Lemma dealloc_time_mono M x ot1 ot2:
        deallocated M x ot1
        → ot1 ⊑ ot2
        → deallocated M x ot2.
      Proof.
        inversion 1. econstructor; eauto.
        etrans; by eauto.
      Qed.

      Lemma init_time_mono M x t1 t2:
        initialized M x t1
        → t1 ⊑ t2
        → initialized M x t2.
      Proof.
        inversion 1. econstructor; eauto.
        eapply Pos.lt_le_trans; eauto.
      Qed.

      Definition AllocInv (M : MessageSet) (m : message) :=
        match mval m with
        | VInj _ => allocated (filter (λ m', mtime m' ⊏ mtime m) M)
                              (mloc m) (mtime m)
        | A =>    deallocated (filter (λ m', mtime m' ⊏ mtime m) M)
                              (mloc m) (Some (mtime m))
        | D =>      allocated (filter (λ m', mtime m' ⊏ mtime m) M)
                              (mloc m) (mtime m)
        end.

      Inductive adjacent m1 m2 :=
        adj (EqLoc : m2.(mloc) = m1.(mloc)) (GtTime : m2.(mtime) = m1.(mtime)+1).

      Definition dealloc_inv (M : MessageSet) :=
        ∀ m (In : m ∈ M) (IsD : m.(mval) = D),
          inhabited (MaxTime M m.(mloc) (Some m.(mtime)))
        ∨ ∃ m', m ∈ M ∧ adjacent m m' ∧ m'.(mval) = A.

      Definition alloc_inv (M : MessageSet) :=
        ∀ m (In : m ∈ M), AllocInv M m.

      Section alloc_inv_helpers.

        Lemma alloc_add_ins_val_old
          P `{Dec : ∀ m : message, Decision (P m)}
          M m x v t R
          (HA: allocated {[m <- M | P m]} (mloc m) (mtime m))
          : allocated {[m <- M ∪ {[<x→VInj v@t,R>]} | P m]} (mloc m) (mtime m).
        Proof.
          inversion HA as [t_d t_a TD TA ? ?].
          econstructor; eauto;
          rewrite !gset_filter_union;
          apply MT_mono_l => // ?; abstract set_solver+.
        Qed.

        Lemma dealloc_add_ins_val_old
          P `{Dec : ∀ m : message, Decision (P m)}
          M m x v t R
          (HD: deallocated {[m <- M | P m]} (mloc m) (Some (mtime m)))
          : deallocated {[m <- M ∪ {[<x→VInj v@t,R>]} | P m]}
                        (mloc m) (Some (mtime m)).
        Proof.
          inversion HD as [t_d t_a TD TA ? ?].
          rewrite gset_filter_union;
          apply (is_deallocated _ _ _ t_d t_a) => //;
          (apply: (MT_equiv _); first eassumption); abstract set_solver+.
        Qed.

        Lemma alloc_add_ins_filtered_old
          P `{Dec : ∀ m : message, Decision (P m)}
          M m x t
          (HA: allocated {[m <- M | P m]} x t)
          (NS: mloc m = x → P m → False)
          : allocated {[m <- M ∪ {[m]} | P m]} x t.
        Proof.
          inversion HA as [t_d t_a TD TA ? ?].
          apply (is_allocated _ _ _ t_d t_a) => //;
            [rewrite -(join_None t_d)|rewrite -(join_None (Some t_a))];
            rewrite 2!gset_filter_union;
            apply MT_set_mono => //; apply MT_None; abstract set_solver.
        Qed.

        Lemma dealloc_add_ins_filtered_old
          P `{Dec : ∀ m : message, Decision (P m)}
          M m x ot
          (HD: deallocated {[m <- M | P m]} x ot)
          (NS: mloc m = x → P m → False)
          : deallocated {[m <- M ∪ {[m]} | P m]} x ot.
        Proof.
          inversion HD as [t_d t_a TD TA ? ?].
          apply (is_deallocated _ _ _ t_d t_a) => //;
            [rewrite -(join_None t_d) | rewrite -(join_None t_a)];
            rewrite 2!gset_filter_union;
            apply MT_set_mono => //; apply MT_None; abstract set_solver.
        Qed.

        Lemma alloc_filter_lt (M: MessageSet) x t1 t2
          (Alloc : allocated M x t1)
          (Lt: t1 ⊏ t2)
          : allocated {[m <- M | mtime m ⊏ t2]} x t1.
        Proof.
          inversion Alloc as [t_d t_a TD TA ? ?].
          econstructor; eauto.
          - apply: (MT_impl (MT_equiv_loc TD (MT_msg_max_filter TD))).
            apply set_unfold_2 => /=. split. 
            + move => [-> [H]]. move: H Alloc0 Local => <- H1 H2. intuition.
              cbn in H1; by rewrite H1 H2.
            + intuition. apply (MT_max TD). abstract set_solver.
          - apply: (MT_impl (MT_equiv_loc TA (MT_msg_max_filter TA))).
            apply set_unfold_2; cbn; intuition.
            + rewrite <- H in Local; by rewrite Local.
            + apply (MT_max TA); abstract set_solver.
        Qed.

        Lemma dealloc_filter_lt (M: MessageSet) x t1 t2
          (HD : deallocated M x (Some t1))
          (Lt: t1 ⊏ t2)
          : deallocated{[m <- M | mtime m ⊏ t2]} x (Some t1).
        Proof.
          inversion HD as [t_d t_a TD TA ? ?].
          econstructor; last first; eauto; last first.
          - apply: (MT_impl (MT_equiv_loc TD (MT_msg_max_filter TD))).
            apply set_unfold_2 => /=; intuition.
            + rewrite <- H in Local. cbn in Local; by rewrite Local.
            + apply (MT_max TD); abstract set_solver.
          - apply: (MT_impl (MT_equiv_loc TA (MT_msg_max_filter TA))).
            apply set_unfold_2 => /=. split.
            + move => [-> [H]]. move: H DeAlloc Local => <- H1 H2. intuition.
              rewrite -> H2 in H1. cbn in H1; by rewrite H1.
            + intuition. apply (MT_max TA). abstract set_solver.
        Qed.

        Lemma alloc_inv_add_ins_val_time_gt M x t1 t2 v R
          (AInv : alloc_inv M)
          (Hlt: t1 < t2)
          (Alloc : allocated M x t1)
          : alloc_inv (M ∪ {[<x→VInj v@t2,R>]}).
        Proof.
          move => m /elem_of_union [/AInv HAm|/elem_of_singleton In].
          + rewrite /AllocInv. rewrite /AllocInv in HAm.
            destruct (mval m) eqn:Hmv; try (apply alloc_add_ins_val_old => //).
            apply dealloc_add_ins_val_old => //.
          + rewrite In /AllocInv /= -/In.
            apply alloc_add_ins_filtered_old; last first.
            * move => _. apply Pos.lt_irrefl.
            * eapply alloc_time_mono; last first.
              { by apply Pos.lt_le_incl. }
              { by apply alloc_filter_lt. }
        Qed.

      End alloc_inv_helpers.

      Lemma alloc_thread_red_alloc σ x otr otw evt V V' σ' acc 
        (HAcc : acc_of evt = Some acc)
        (ThRed : thread_red V (mem σ) otr otw evt V' (mem σ'))
        (DRFRed : drf_red σ σ' V otr otw evt)
        (ARed : alloc_red (mem σ) V x evt)
        (AInv : alloc_inv (mem σ))
        : alloc_inv (mem σ').
      Proof.
        inversion ThRed; clear ThRed; simplify_eq; [done|..];
        inversion DRFRed; clear DRFRed;
        inversion ARed; clear ARed; simplify_eq.
        - eapply alloc_inv_add_ins_val_time_gt => //. (* at write *)
          by rewrite LV in Lt.
        - eapply alloc_inv_add_ins_val_time_gt => //. (* na write *)
          by rewrite LT in Lt.
        - (* allocation *)
          move => m /elem_of_union [HAm|/elem_of_singleton In].
          + apply AInv in HAm as HA.
            rewrite /AllocInv. rewrite /AllocInv in HA.
            destruct (mval m) eqn:Hmv;
              [   apply dealloc_add_ins_filtered_old
                | apply   alloc_add_ins_filtered_old
                | apply   alloc_add_ins_filtered_old] => //;
            move => ? _;
              setoid_rewrite <-(elem_of_empty (A := message) (C:=MessageSet) m);
              rewrite <-Fresh; by abstract set_solver.
          + rewrite In /AllocInv /= -/In.
            apply dealloc_add_ins_filtered_old => /=; last first.
            * move => _. apply Pos.lt_irrefl.
            * apply (dealloc_time_mono _ _ None) => //.
              econstructor; last first.
              { reflexivity. }
              { reflexivity. }
              { apply MT_None.
                rewrite gset_filter_comm (gset_filter_comm (M := mem σ)) Fresh.
                abstract set_solver+. }
              { apply MT_None.
                rewrite gset_filter_comm (gset_filter_comm (M := mem σ)) Fresh.
                abstract set_solver+. }
        - assert (t1 < t). (* deallocation *)
            { by rewrite LT in Lt. }
          move => m /elem_of_union [HAm|/elem_of_singleton In].
          + apply AInv in HAm as HA.
            rewrite /AllocInv. rewrite /AllocInv in HA.
            assert (In : m ∈ msgs (mem σ')).
             { rewrite -H4. apply/elem_of_union. by left. }
            destruct (mval m) eqn:Hmv;
              [   apply dealloc_add_ins_filtered_old
                | apply   alloc_add_ins_filtered_old
                | apply   alloc_add_ins_filtered_old] => //;
                move => /= EqLoc Lt';
            erewrite -> (MT_Some_Lt Max' m In (eq_sym EqLoc)) in Lt';
              exact: Pos.lt_irrefl.
          + rewrite In /AllocInv /= -/In.
            apply alloc_add_ins_filtered_old; last first.
            * move => _. apply Pos.lt_irrefl.
            * apply (alloc_time_mono _ _ t1); last first.
              { by apply Pos.lt_le_incl. }
              { by apply alloc_filter_lt. }
        - eapply alloc_inv_add_ins_val_time_gt => //. (* update *)
          + destruct Lt as [t' [Lt1 Lt2]]. rewrite Lt1 in LV.
            simplify_option_eq.
            eapply Pos.le_lt_trans; eauto.
            apply Pos.lt_add_diag_r.
          + by apply init_is_alloc.
      Qed.

      Lemma dealloc_inv_helper1 m (M1 M2 : MessageSet) :
        (∃ m', m ∈ M1 ∧ adjacent m m' ∧ mval m' = A) ->
        ∃ m', m ∈ M1 ∪ M2 ∧ adjacent m m' ∧ mval m' = A.
      Proof.
        destruct 1 as (? & In' & ?). eexists. split; last eassumption.
        apply/elem_of_union; left; assumption.
      Qed.

      Lemma dealloc_inv_helper2 M1 
            `(HMax : MaxTime M1 (mloc m) (Some (mtime m)))
            `(NEqx : x ≠ mloc m) v t R:
        inhabited (MaxTime (M1 ∪ {[<x→v@t,R>]}) 
                           (mloc m) (Some (mtime m))).
      Proof.
        rewrite (_ : Some (mtime m) = Some (mtime m) ⊔ None); last done.
        constructor. apply: MT_set_mono; first auto.
        apply: MT_None. move => ?; split; last by move/elem_of_empty.
        move => /elem_of_filter /= [?] /elem_of_singleton ?. by simplify_eq.
      Qed.

      Lemma max_D_not_allocated (M: MessageSet) m t
        (In: m ∈ M) (EqD: mval m = D) (HMax: MaxTime M (mloc m) (Some (mtime m)))
        : ¬ allocated M (mloc m) t.
      Proof.
        move => [t_d t_a TD TA Alloc LE].
        have In' : m ∈ of_loc (mloc m) ({[ m <- M | mval m = D]}).
        { abstract set_solver. }
        rewrite <-(MT_max TD _ In') in Alloc.
        cbn in Alloc.
        rewrite <-(MT_Some_Lt HMax (MT_msg TA)), <-(MT_msg_time TA) in Alloc.
        - exact: (irreflexive_fw _ _ Alloc). 
        - move: (MT_msg_In TA). abstract set_solver+.
        - exact: MT_msg_loc.
      Qed.

      Lemma alloc_thread_red_dealloc σ x otr otw evt V V' σ' acc
        (HAcc : acc_of evt = Some acc)
        (ThRed : thread_red V (mem σ) otr otw evt V' (mem σ'))
        (DRFRed : drf_red σ σ' V otr otw evt)
        (ARed : alloc_red (mem σ) V x evt)
        (DInv : dealloc_inv (mem σ))
        : dealloc_inv (mem σ').
      Proof.
        inversion ThRed; clear ThRed;
          inversion DRFRed; clear DRFRed;
            inversion ARed; clear ARed;
              simplify_eq;
              simpl in HAcc; try by (simplify_option_eq; tauto);
                try (rewrite LT in LV; inversion LV; subst; clear LV);
                (apply Lt in LT || apply Lt in LV); try by [].
        - move => m /elem_of_union [|/elem_of_singleton ->] /=;
            last (by inversion 0);
            move => In Eq; move : (DInv _ In Eq); intros [[HMax]|];
                    [left|right; exact: dealloc_inv_helper1].
          case: (decide (x0 = mloc m)) => [Eqx0|NEqx0];
            last exact: dealloc_inv_helper2.
          subst. exfalso. eapply max_D_not_allocated => //.
        - move => m /elem_of_union [|/elem_of_singleton ->] /=; 
            last (by inversion 0);
            move => In Eq; move : (DInv _ In Eq); intros [[HMax]|]; 
                    [left|right; exact: dealloc_inv_helper1].
          case: (decide (x0 = mloc m)) => [Eqx0|NEqx0];
            last exact: dealloc_inv_helper2.
          subst. exfalso. eapply max_D_not_allocated => //.
        - move => m /elem_of_union [|/elem_of_singleton ->] //=.
          move => In Eq; move : (DInv _ In Eq) => [[HMax]|].
          + case: (decide (x0 = mloc m)) => [Eqx0|NEqx0];
              last (left; apply dealloc_inv_helper2 => //).
            exfalso. apply (not_elem_of_empty (C:=MessageSet) m).
            rewrite -Fresh. abstract set_solver.
          + move => [m' [? [? ?]]]. right. exists m'. repeat split => //.
            apply elem_of_union. by left.
        - move => m /elem_of_union [|/elem_of_singleton ->] /=.
          + move => In Eq; move : (DInv _ In Eq); intros [[HMax]|];
                    [left|right; exact: dealloc_inv_helper1].
            case: (decide (x0 = mloc m)) => [Eqx0|NEqx0];
              last exact: dealloc_inv_helper2.
            subst. exfalso. eapply max_D_not_allocated => //.
          + move => ?. left. constructor. apply: (MT_Some); last 2 first.
            * rewrite gset_filter_union elem_of_union
                      !(elem_of_filter {[_]}) elem_of_singleton.
              right; by split; last reflexivity.
            * move => ? /elem_of_filter [Eqx0]
                        /elem_of_union [In|/elem_of_singleton ->];
                last reflexivity.
              apply (MT_Some_Lt Max') => //.
              rewrite -H4 elem_of_union. by left.
            * reflexivity.
        - move => m /elem_of_union [|/elem_of_singleton ->] /=.
          + move => In Eq; move : (DInv _ In Eq); intros [[HMax]|];
                    [left|right; exact: dealloc_inv_helper1].
            case: (decide (x0 = mloc m)) => [Eqx0|NEqx0];
              last exact: dealloc_inv_helper2.
            apply init_is_alloc in Init as Alloc. clear InM.
            subst. exfalso. eapply max_D_not_allocated => //.
          + move => ?. left. constructor. apply: MT_Some; last 2 first.
            * rewrite elem_of_filter elem_of_union elem_of_singleton.
              split; by eauto.
            * move => ?.
              rewrite gset_filter_union elem_of_union !elem_of_filter.
              move => [?|[?] /elem_of_singleton ->] ? => //.
            * reflexivity.
      Qed.

    End Allocation.

    (* Invariants for σ
        σ.nats
        + any timestamp of <nats> corresponds to some message in <mem>
            nats_ok <mem> <nats>
        + any loc in <mem> has been registered in <nats>
            nats_complete <mem> <nats>

        σ.threads
        + view by any thread agrees with <mem>,
          therefore transitively agrees with nats
            threads_ok σ

        σ.mem
        + pairwise_disj <mem>
        + any message is well-formed
            msgs_ok <mem> 
          TODO: view_ok is useless!!!
        + any message is compatible with the history before it
            alloc_inv M
    *)
    Record phys_inv σ :=
      Inv {
        INOk : nats_ok (mem σ) (nats σ);
          (* any timestamp of nats corresponds to some message in mem *)
        (* INCom : nats_complete (mem σ) (nats σ); *)
          (* any loc ever occurs must have been registered in nats *)
        (* ITOk : threads_ok σ; *)
          (* view by any thread agrees with mem *)
        IMOk : msgs_ok (mem σ); (* TODO: view_ok is useless!!! *)
          (* any message in mem is well-formed and agrees with mem *)
        IAlloc : alloc_inv (mem σ);
          (* any message is compatible with the history before it *)
        IDeAlloc : dealloc_inv (mem σ);
      }.

    (* Definition views_mono σ σ'  *)
    (* := ∀ π, (threads σ !! π : option View) ⊑ (threads σ' !! π). *)
    Definition rel_inv σ σ'
    := msgs_mono (mem σ) (mem σ') ∧ (nats σ) ⊑ (nats σ').

    Inductive fork_red (σ : state) V : event -> state -> Prop :=
    | fork_fork σ'
                (Hσ : σ' = σ)
      : fork_red σ V (EFork V) σ'.

    Definition NoWrite (e: event): Prop :=
      match e with
      | ERead _ _ _ => True
      | EWrite _ _ _ => False
      | EUpdate _ _ _ => False
      | EFork _ => True
      end.

    Inductive machine_red (σ : state) (evt : event) V (σ' : state) V' : Prop :=
    | machine_thread otr otw
                     (ARed : ∀ x, loc_of evt = Some x -> alloc_red (mem σ) V x evt)
                     (DRFRed : drf_red σ σ' V otr otw evt)
                     (NoWriteNats : NoWrite evt -> nats σ' = nats σ)
                     (NoWriteMsgs : NoWrite evt -> msgs (mem σ') = msgs (mem σ))
                     (ThreadRed : thread_red V (mem σ) otr otw evt V' (mem σ'))
    | machine_fork
                   (HEvt : evt = EFork V')
                   (FRed : fork_red σ V evt σ')
    .

    (* Lemma fork_red_neq σ π ρ σ' : fork_red σ π (EFork ρ) σ' -> ρ ≠ π. *)
    (* Proof. *)
    (*   inversion 1. *)
    (*   move => ?; subst. by rewrite Fresh in ViewOf. *)
    (* Qed. *)

    Lemma machine_red_safe σ evt V σ' V' :
      phys_inv σ →
      view_ok (mem σ) V →
      machine_red σ evt V σ' V' ->
      phys_inv σ'
      ∧ view_ok (mem σ') V'
      ∧ msgs_mono (mem σ) (mem σ')
      ∧ nats σ ⊑ nats σ'
      ∧ V ⊑ V'.
    Proof.
      intros [INOk IMOk IAlloc IDealloc] VOk.
      inversion 1; last first.
      - inversion FRed. subst. inversion H0. subst.
        repeat split; auto.
        + by apply IMOk.
        + by apply IMOk.
        + reflexivity.
        + reflexivity.
        + reflexivity.
      - assert (msgs_mono (mem σ) (mem σ')) by by eapply thread_red_msgs_mono.
        have: (thread_inv V' (mem σ')) => [|[IMOk' VOk']].
        { eapply thread_red_safe; eauto. by split; auto. }
        assert (nats σ ⊑ nats σ'). (* nats_mono *)
        { inversion ThreadRed; subst; first rewrite NoWriteNats; (try by auto);
          eapply drf_red_nats_mono; by eauto. }
        repeat (split; last split); first split; auto.
        + inversion ThreadRed. (* nats_ok *)
          * by rewrite NoWriteNats; subst => //.
          * replace (add_ins (mem σ) m Disj) with (mem σ').
            by eapply drf_red_safe; eauto.
          * replace (add_ins (mem σ) m Disj) with (mem σ').
            by eapply drf_red_safe; eauto.
        + inversion ThreadRed; try (by subst; apply IAlloc); (* alloc_inv *)
          replace (add_ins (mem σ) m Disj) with (mem σ'); subst;
          eapply alloc_thread_red_alloc; eauto; subst; auto; apply Invσ.
        + inversion ThreadRed; try (by subst; apply IDealloc); (* dealloc_inv *)
          replace (add_ins (mem σ) m Disj) with (mem σ'); subst;
          eapply alloc_thread_red_dealloc; eauto; subst; auto; apply Invσ.
        + eapply thread_red_view_mono; eauto. by split.
    Qed.
  End Machine.

End RAMachine.

Arguments machine_red_safe [_ _ _ _ _] _ _ _.