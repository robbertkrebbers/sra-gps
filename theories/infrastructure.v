From mathcomp Require Import ssreflect ssrbool seq.
From stdpp Require Export list numbers set gmap finite mapset fin_collections.
Global Generalizable All Variables.
Global Set Automatic Coercions Import.
Global Set Asymmetric Patterns.
Global Set Bullet Behavior "Strict Subproofs".
From Coq Require Export Utf8.
Require Import Setoid.

Lemma seq_set_size (s n: nat):  size (seq_set s n : gset nat) = n.
Proof.
  induction n; [done|].
  rewrite seq_set_S_union_L /=.
  rewrite size_union.
    + rewrite IHn size_singleton. omega.
    + apply disjoint_singleton_l.
      move => /elem_of_seq_set [_ ?]. omega.
Qed.

Lemma seq_S i n : seq i (S n) = (seq i n ++ (i + n)%nat::nil)%list.
Proof.
  elim: n i => [//|n /= IHn] i.
  - by rewrite -plus_n_O.
  - by rewrite IHn -plus_n_Sm.
Qed.

Lemma seq_elem_of t n (Lt : (t < n)%nat): t ∈ seq 0 n.
Proof.
  induction n; first omega.
  rewrite seq_S elem_of_app. apply lt_n_Sm_le, le_lt_or_eq in Lt as [Lt|Eq].
  - left. by apply IHn.
  - right. rewrite Eq. left.
Qed.

Definition  inj_dec_eq
  `{H : ∀ x y : A, Decision (x = y)} {B : Type}
  f (g : A -> option B) (Inj : ∀ x, g (f x) = Some x) 
  : ∀ x y : B, Decision (x = y).
Proof.
  move => x y. case: (decide (f x = f y)).
  - move/(f_equal g). rewrite !Inj => [[]]. by left.
  - move => NEq. right => Eq. apply: NEq. by rewrite Eq.
Qed.


Section suffixes.
  Context {A: Type}.
  Implicit Types (B: Type) (l: list A).
  Set Default Proof Using "Type".

  Lemma suffix_cons_inv_2 l1 l2 a:
    l1 `suffix_of` a :: l2 → l1 = a :: l2 ∨ l1 `suffix_of` l2.
  Proof.
    destruct l1 as [|b l1].
    - right. apply suffix_nil.
    - apply suffix_cons_inv.
  Qed.

  Lemma suffix_share_total l1 l2 l3:
    l2 `suffix_of` l1 → l3 `suffix_of` l1 
    → l2 `suffix_of` l3 ∨ l3 `suffix_of` l2.
  Proof.
    revert l2 l3. induction l1.
    - move => ? ? /suffix_nil_inv -> /suffix_nil_inv ->. by left.
    - move => l2 l3 H1 H2.
      apply suffix_cons_inv_2 in H1 as [H1|H1].
      + right. by rewrite H1.
      + apply suffix_cons_inv_2 in H2 as [H2|H2].
        * left. rewrite H2. by apply suffix_cons_r.
        * by apply IHl1.
  Qed.

  Lemma elem_of_suffix l1 l2:
    l1 `suffix_of` l2 → ∀ i, i ∈ l1 → i ∈ l2.
  Proof. move => [? ->] ?. rewrite elem_of_app. auto. Qed.

  Lemma suffix_fmap {B: Type} (f: A → B) l1 l2:
    l1 `suffix_of` l2 → f <$> l1 `suffix_of` f<$> l2.
  Proof. move => [k ->]. exists (f <$> k). by rewrite fmap_app. Qed.

  Lemma filter_app (P : A → Prop) `{∀ x : A, Decision (P x)} l1 l2:
    filter P (l1 ++ l2)%list = (filter P l1 ++ filter P l2)%list.
  Proof. induction l1; [auto|cbn]. case (decide (P a)); by rewrite IHl1. Qed.

  Lemma suffix_filter
    (P : A → Prop) `{∀ x : A, Decision (P x)} (l1 l2: list A):
    l1 `suffix_of` l2 -> filter P l1 `suffix_of` filter P l2.
  Proof. move => [k ->]. exists (filter P k). by apply filter_app. Qed.

  Global Instance suffix_partialorder : @PartialOrder (list A) suffix.
  Proof.
    split; first auto with typeclass_instances.
    intros x y [x1 H1] [y1 H2]. rewrite H2 app_assoc in H1.
    apply (app_inv_tail _ nil) in H1.
    symmetry in H1. apply app_nil in H1 as [_ ?]. by subst y1.
  Qed.

  Lemma of_list_nil_eq
    {C : Type} `{Empty C} `{ElemOf A C} `{Singleton A C} `{Union C}
    `{SimpleCollection A C} (l: list A):
    of_list l ≡ (∅ : C) ↔ l = nil.
  Proof.
    split.
    - move => Eq. destruct l; first done.
      rewrite of_list_cons in Eq. exfalso.
      apply empty_union in Eq as [Eq _].
      eapply not_elem_of_empty. rewrite -Eq elem_of_singleton. done.
    - move => ->. by rewrite of_list_nil.
  Qed.

  Lemma hd_error_some_elem a l :
    hd_error l = Some a → a ∈ l.
  Proof. destruct l as [|a' l]; first done. move => [->]. left. Qed.

  Lemma list_filter_cons
    (P: A → Prop) `{∀ x, Decision (P x)} a l :
    P a → filter P (a :: l) = a :: filter P l.
  Proof. move => Pa. rewrite {1}/filter/=. rewrite decide_True; done. Qed.

  Lemma list_filter_singleton (P: A → Prop) `{∀ x, Decision (P x)} (a : A):
    P a → filter P [a] = [a].
  Proof. move => Pa. by rewrite list_filter_cons. Qed.

  Lemma list_filter_cons_not (P: A → Prop) `{∀ x, Decision (P x)} a l :
    ¬ P a → filter P (a :: l) =  filter P l.
  Proof. move => NPa. rewrite {1}/filter/=. rewrite decide_False; done. Qed.

  Lemma list_filter_pred_proper
    (P Q: A → Prop) `{∀ x, Decision (P x)} `{∀ x, Decision (Q x)} l:
    (∀ x, x ∈ l → P x ↔ Q x) → filter P l  = filter Q l.
  Proof.
    induction l as [|a l]; first done.
    move => EqPQ.
    rewrite /filter /list_filter.
    case (decide (P a)) => [HP|NP].
    - rewrite decide_True.
      + f_equal. apply IHl. move => ? ?. apply EqPQ. by right.
      + apply EqPQ; [by left|auto].
    - rewrite decide_False.
      + f_equal. apply IHl. move => ? ?. apply EqPQ. by right.
      + move => Qa. apply NP. apply EqPQ; [by left|auto].
  Qed.

  Lemma list_filter_nil (P: A → Prop) `{∀ x, Decision (P x)} l:
    (∀ a, a ∈ l → ¬ P a) → filter P l = nil.
  Proof.
    move => Pa. induction l as [|a l]; first done.
    rewrite /filter /list_filter.
    rewrite decide_False.
    - apply IHl. move => a' In. apply Pa. by right.
    - apply Pa. by left.
  Qed.

End suffixes.

Section Max.
  Context {A C : Type} `{Collection A C}.

  Inductive Max (R : relation A) (c : C) : Type :=
  | max_none (HEmp : c ≡ ∅)
  | max_some a (HIn : a ∈ c) (HRel : ∀ a', a' ∈ c → R a' a).
  Global Arguments max_none [_ _] _.
  Global Arguments max_some [_ _] _ _ _.

  Lemma Max_equiv {c2} `(m : Max R c1) (Equiv : c1 ≡ c2) : Max R c2.
  Proof.
    elim: m => [HEmp|? HIn HRel].
    - left. by rewrite -Equiv HEmp.
    - eright; setoid_rewrite <-Equiv; eauto.
  Defined.

  Definition of_Max `(m : Max R c) : option A :=
    if m is max_some a _ _ then Some a else None.

  Inductive MaxDep R {B} F c (ob : option B) : Type :=
  | maxdep (m : Max R c) (EqO : ob = F <$> of_Max m) : MaxDep R F c ob.
  Global Arguments maxdep [_ _ _ _ _] _ _.

  Definition max_of_MaxDep `(md : @MaxDep R B F c ob) : Max R c :=
    let (m, _) := md in m.

  Lemma eq_of_MaxDep `(md : @MaxDep R B F c ob) : ob = F <$> of_Max (max_of_MaxDep md).
  Proof. by destruct md. Qed.

  Ltac MD_tac := match goal with
                 | [md : MaxDep _ _ ?c ?o |- _ ] =>
                   let m := fresh "m" in
                   induction md as [m EqO]; destruct m; try discriminate
                 end.

  Definition MaxDep_emp `(md : @MaxDep R B F c None) : c ≡ ∅ := ltac:(by MD_tac).

  Definition of_MaxDep' `(md : @MaxDep R B F c (Some b))
    : { a : A | a ∈ c ∧ ∀ a', a' ∈ c → R a' a} := ltac:(MD_tac; eauto).
  Definition of_MaxDep `(md : @MaxDep R B F c (Some b)) 
    := let (m, _) := of_MaxDep' md in m.
  Global Arguments of_MaxDep' [_ _ _ _ _] _ /.
  Global Arguments of_MaxDep [_ _ _ _ _] _ /.

  Lemma MaxDep_equiv {c2} `(md : @MaxDep R B F c1 ob) (Equiv : c1 ≡ c2) 
    : @MaxDep R B F c2 ob.
  Proof.
    destruct md. pose (Max_equiv m Equiv).
    econstructor. rewrite EqO.
    instantiate (1 := m0).
      by destruct m.
  Defined.
End Max.

Section Filter_GSet.
  Context `{C : Countable A}.
  Set Default Proof Using "Type* EqDec C".

  Context {P : A -> Prop} `{DecP : ∀ m, Decision (P m)}.

  Lemma subseteq_gset_filter (M: gset A)
    : filter P M ⊆ M.
  Proof. by move => ? /elem_of_filter [_]. Qed.

  Lemma gset_filter_union (M1 M2 : gset A) :
    filter P (M1 ∪ M2) = filter P M1 ∪ filter P M2.
  Proof. apply/mapset_eq. set_solver. Qed.

  Lemma gset_filter_difference (X Y : gset A) :
      filter P (X ∖ Y) = filter P X ∖ filter P Y.
  Proof. apply/mapset_eq. set_solver. Qed.

  Lemma gset_filter_singleton (a: A):
    P a → filter P ({[ a ]}: gset A) ≡ {[ a ]}.
  Proof. set_solver. Qed.

  Lemma gset_filter_subseteq (X Y: gset A) :
    X ⊆ Y → filter P X ⊆ filter P Y.
  Proof. set_solver. Qed.

  Global Instance filter_gset_Proper : 
    Proper (((≡) : relation (gset A)) ==> ((≡) : relation (gset A))) (filter P).
  Proof. solve_proper. Qed.

End Filter_GSet.

Lemma gset_filter_comm `{Countable A} {P P2 : A -> Prop}
  `{DecP : ∀ m, Decision (P m)} `{DecP2 : ∀ m, Decision (P2 m)} {M : gset A} :
  filter P (filter P2 M) ≡ filter P2 (filter P M).
Proof. set_solver. Qed.

Lemma gset_filter_impl `{Countable A} {P P2 : A -> Prop} 
  `{DecP : ∀ m, Decision (P m)} `{DecP2 : ∀ m, Decision (P2 m)} {M : gset A} :
  (∀ a, P a -> P2 a) -> filter P (filter P2 M) ≡ filter P M.
Proof. set_solver. Qed.


(* Extra lemmas for set difference *)

Lemma difference_mono `{Collection A C}:
  Proper ((⊆) ==> (≡) ==> (⊆)) (@difference C _).
Proof.
  intros X1 X2 HX Y1 Y2 HY. rewrite HY.
  move => ? /elem_of_difference [? ?]. apply elem_of_difference.
  split; last auto. by apply HX.
Qed.

Lemma gset_difference_subseteq `{Countable A} (X Y: gset A):
  X ∖ Y ⊆ X.
Proof.
  by move => ? /elem_of_difference [? _].
Qed.

Lemma gset_difference_mono `{Countable A} (X Y Z: gset A):
  X ⊆ Y → X ∖ Z ⊆ Y ∖ Z.
Proof. intro Sub. by apply (difference_mono). Qed.

Lemma difference_twice_union `{Collection A C} (X Y Z: C):
  (X ∖ Y ∖ Z ≡ X ∖ (Y ∪ Z))%C.
Proof.
  apply elem_of_equiv. move => ?.
  rewrite !elem_of_difference elem_of_union. split.
  - move => [[? ?] ?]. split; first auto. move => [?|?] //.
  - move => [? NE].
    repeat split; first auto; move => ?; apply NE; by [left|right].
Qed.


Section Filter_Help.
  Context `{Countable A}.
  Structure FH_tag := { fh_tag :> gset A }.
  Definition fh_tag0 M := {| fh_tag := M|}.
  Canonical Structure fh_tagrec M := fh_tag0 M.
  Definition lType := (sigT (λ (P : A -> Prop), ∀ a, Decision (P a))).
  Definition lPred P {DecP : ∀ a : A, Decision (P a)} : lType := existT P DecP.
  Bind Scope lType_scope with lType.
  Definition lfold := ( 
                    fold_right
                      (λ s : lType, let (P, DecP) := s in @filter _ (gset _) _ P DecP)
                  ).
  Structure FH_list (M : gset A) :=
    mklist { fh_term : FH_tag;
             fh_list : list lType;
             _ : fh_tag fh_term = lfold M fh_list
           }.
  Arguments fh_term [_] _.
  Arguments fh_list [_] _.
  Definition fh_eq `(fhl : FH_list M) :
    fh_tag (fh_term fhl) = lfold M (fh_list fhl) := ltac:(by destruct fhl).
  Canonical Structure FH_list0 M := mklist M (fh_tag0 M) nil eq_refl.
  Program Canonical Structure FH_list1 P DecP `(fhl : FH_list M) :=
    mklist M (fh_tagrec (@filter _ _ _ P DecP (fh_tag (fh_term fhl)))) (@lPred P DecP :: fh_list fhl) _.
  Next Obligation. intros. simpl. by rewrite fh_eq. Qed.

  Global Instance fold_Proper (M : gset A) : Proper (((≡ₚ)
    : relation (list lType)) ==> (≡)) (lfold M).
  Proof.
    move => ? ? Perm;
      elim: Perm => /= [//|[? ?] ? ? _ -> //|[? ?] [? ?] ?|? ? ? _ -> _ -> //].
    by rewrite gset_filter_comm.
  Qed.

  Lemma filter_reorder `(fhl : FH_list M) l
    : fh_list fhl ≡ₚ l -> fh_tag (fh_term fhl) ≡ lfold M l.
  Proof. move => <-. by rewrite fh_eq. Qed.

  Global Instance lfold_Unfold `{fhl1 : list lType} M2 m Q:
    SetUnfold (m ∈ M2) (Q) ->
    SetUnfold (m ∈ lfold M2 (fhl1))
              (fold_right (λ s : lType, let (P, _) := s in and (P m)) (Q) (fhl1)).
  Proof.
    intros SU. constructor. revert m SU.
    induction fhl1 => /=; first abstract set_solver+.
    destruct a. abstract set_solver.
  Qed.

End Filter_Help.


Section Map_GSet.
  Context {A B : Type}
           `{EqDecA : EqDecision A}
           {CA : @Countable A EqDecA}
           `{EqDecB : EqDecision B}
           {CB : @Countable B EqDecB}.
  Set Default Proof Using "Type* EqDecA EqDecB CA CB".
  Definition map_gset f (M : gset A) : gset B
    := of_list (f <$> (elements M)).
  Typeclasses Opaque map_gset.

  Lemma elem_of_map_gset f M b : b ∈ map_gset f M ↔ ∃ a, b = f a ∧ a ∈ M.
  Proof.
    rewrite elem_of_of_list elem_of_list_fmap.
      by setoid_rewrite elem_of_elements.
  Qed.
  Global Instance set_unfold_map_gset f M b Q :
    `{∀ m, SetUnfold (m ∈ M) (Q m)}
    → SetUnfold (b ∈ map_gset f M) (∃ a, b = f a ∧ Q a) | 0.
  Proof. intros. constructor. rewrite elem_of_map_gset. by set_unfold. Qed.

  Global Instance gset_map_Proper (f : A -> B) : Proper ((≡) ==> (≡)) (map_gset f).
  Proof. solve_proper. Qed.

  Lemma gset_map_union f M1 M2 :
    map_gset f (M1 ∪ M2) ≡ map_gset f M1 ∪ map_gset f M2.
  Proof. set_unfold; abstract naive_solver. Qed.

  Lemma gset_map_singleton (f : A -> B) (a: A):
    map_gset f {[ a ]} ≡ {[ f a ]}.
  Proof. set_solver. Qed.

  Lemma gset_map_difference_inj (f : A -> B) (X Y: gset A)
    (FInj : ∀ a a', a ∈ X ∪ Y → a' ∈ X ∪ Y → f a = f a' ↔ a = a')
    : (map_gset f X) ∖ (map_gset f Y) ≡ map_gset f (X ∖ Y).
  Proof.
    apply set_unfold_2. move => b. split.
    - move => [[a [Hb Ina]] HR]. exists a. repeat split; [auto|auto|].
      move => Ina2. apply HR. by exists a.
    - move => [a [Hb [Ina Ina2]]]. split.
      + by exists a.
      + move => [a' [Hb' Ina']]. rewrite Hb in Hb'.
        apply FInj in Hb'.
        * by subst.
        * by apply elem_of_union_l.
        * by apply elem_of_union_r.
  Qed.

  Lemma gset_map_subseteq_inj (f : A -> B) (X Y: gset A)
    (FInj : ∀ a a', a ∈ X ∪ Y → a' ∈ X ∪ Y → f a = f a' ↔ a = a')
    : map_gset f X ⊆ map_gset f Y → X ⊆ Y.
  Proof.
    apply set_unfold_2.
    move => HR a Ina.
    destruct (HR (f a)) as [a' [Hf Ina']].
      { by exists a. }
    apply FInj in Hf.
    - by rewrite Hf.
    - by apply elem_of_union_l.
    - by apply elem_of_union_r.
  Qed.

End Map_GSet.
Typeclasses Opaque map_gset.

Notation "'{[' E | m '<-' M ']}'" := (map_gset (λ m, E) M) (at level 1).
Notation "'{[' m '<-' M | E ']}'" := (filter (λ m, E) M) (at level 1).


Lemma irreflexive_fw `{Irreflexive T R} x y : R x y → x = y → False.
Proof.
  move => ? ?; subst. by apply: H.
Qed.
Lemma irreflexive_bw `{Irreflexive T R} x y : R x y → y = x → False.
Proof.
  move => ? ?; subst. by apply: H.
Qed.

Class Extension A := extension : relation A.
Instance: Params (@extension) 2.
Infix "⊑" := extension (at level 70) : C_scope.
Notation "(⊑)" := extension (only parsing) : C_scope.
Notation "( X ⊑ )" := (extension X) (only parsing) : C_scope.
Notation "( ⊑ X )" := (λ Y, Y ⊑ X) (only parsing) : C_scope.
Infix "⊑*" := (Forall2 (⊑)) (at level 70) : C_scope.
Notation "(⊑*)" := (Forall2 (⊑)) (only parsing) : C_scope.
Infix "⊑**" := (Forall2 (⊑*)) (at level 70) : C_scope.
Infix "⊑1*" := (Forall2 (λ p q, p.1 ⊑ q.1)) (at level 70) : C_scope.
Infix "⊑2*" := (Forall2 (λ p q, p.2 ⊑ q.2)) (at level 70) : C_scope.
Infix "⊑1**" := (Forall2 (λ p q, p.1 ⊑* q.1)) (at level 70) : C_scope.
Infix "⊑2**" := (Forall2 (λ p q, p.2 ⊑* q.2)) (at level 70) : C_scope.
Instance Extension_Rewrite `{Extension T} : @RewriteRelation T (⊑).

Class StrictExtension A := strict_extension : relation A.
Instance: Params (@strict_extension) 2.
Infix "⊏" := strict_extension (at level 70) : C_scope.
Notation "(⊏)" := strict_extension (only parsing) : C_scope.
Notation "( X ⊏ )" := (strict_extension X) (only parsing) : C_scope.
Notation "( ⊏ X )" := (λ Y, Y ⊏ X) (only parsing) : C_scope.
Infix "⊏*" := (Forall2 (⊏)) (at level 70) : C_scope.
Notation "(⊏*)" := (Forall2 (⊏)) (only parsing) : C_scope.
Infix "⊏**" := (Forall2 (⊏*)) (at level 70) : C_scope.
Infix "⊏1*" := (Forall2 (λ p q, p.1 ⊏ q.1)) (at level 70) : C_scope.
Infix "⊏2*" := (Forall2 (λ p q, p.2 ⊏ q.2)) (at level 70) : C_scope.
Infix "⊏1**" := (Forall2 (λ p q, p.1 ⊏* q.1)) (at level 70) : C_scope.
Infix "⊏2**" := (Forall2 (λ p q, p.2 ⊏* q.2)) (at level 70) : C_scope.
Instance StrictExtension_Rewrite `{StrictExtension T} : @RewriteRelation T (⊏).

Class Join A := join : A → A → A.
Instance: Params (@join) 2.
Infix "⊔" := join (at level 50, left associativity) : C_scope.
Notation "(⊔)" := join (only parsing) : C_scope.
Notation "( x ⊔)" := (join x) (only parsing) : C_scope.
Notation "(⊔ x )" := (λ y, join y x) (only parsing) : C_scope.
Infix "⊔*" := (zip_with (⊔)) (at level 50, left associativity) : C_scope.
Notation "(⊔*)" := (zip_with (⊔)) (only parsing) : C_scope.
Infix "⊔**" := (zip_with (zip_with (⊔)))
  (at level 50, left associativity) : C_scope.
Infix "⊔*⊔**" := (zip_with (prod_zip (⊔) (⊔*)))
  (at level 50, left associativity) : C_scope.

Class TightExtensions A `{Extension A} `{StrictExtension A} : Prop := {
  tight_extension x y : x ⊑ y ↔ x = y ∨ x ⊏ y
}.

Instance TE_SubRel `{@TightExtensions A E SE} : subrelation SE E.
Proof. repeat intro. apply/tight_extension. abstract naive_solver. Qed.

Class CompatibleExtensions A `{Extension A} `{StrictExtension A} : Prop := {
  compat_ext_l {x y z} : x ⊏ y → y ⊑ z → x ⊏ z;
  compat_ext_r {x y z} : x ⊑ y → y ⊏ z → x ⊏ z;
}.

Global Instance TE_Proper `{@TightExtensions A E SE} `{Transitive A SE} 
  : Proper (flip (⊏) ==> (⊏) ==> impl) (⊑). 
Proof.
  move => ? ? /= H1 ? ? H2 /=. rewrite !tight_extension => ?. intuition; subst.
  - right. by etrans.
  - right. by etrans; first etrans.
Qed.

(* TODO: why both? *)
Global Instance CE_Proper `{C : @CompatibleExtensions A E SE} 
  : Proper (flip (⊑) ==> (⊑) ==> impl) (⊏).
Proof.
  repeat intro. apply: compat_ext_l; last eauto.
  apply: compat_ext_r => //.
Qed.

Global Instance CE_Proper_flip `{C : @CompatibleExtensions A E SE} 
  : Proper ((⊑) ==> flip (⊑) ==> flip impl) (⊏). 
Proof.
  repeat intro. apply: compat_ext_l; last eauto.
  apply: compat_ext_r => //.
Qed.

Global Instance PreOrder_Proper `{@PreOrder A R}
  : Proper (flip R ==> R ==> impl) R.
Proof. move => ? ? /= H1 ? ? H2 ?. etrans; first etrans; eauto. Qed.

Class JoinSemiLattice A `{Extension A, Join A} : Prop := {
  join_semi_lattice_pre :>> PreOrder (⊑);
  join_ext_l X Y : X ⊑ X ⊔ Y;
  join_ext_r X Y : Y ⊑ X ⊔ Y;
  join_least X Y Z : X ⊑ Z → Y ⊑ Z → X ⊔ Y ⊑ Z
}.

Global Instance JoinSemiLattice_Transitive `{JoinSemiLattice A} : Transitive (⊑) := _.
Global Instance JoinSemiLattice_Reflexive `{JoinSemiLattice A} : Reflexive (⊑) := _.

Create HintDb jsl.
Ltac solve_jsl :=
  now match goal with
  | |- _ ⊑ _ => auto with jsl
  | |- _ = _ ⊔ _ => auto with jsl
  | |- _ ⊔ _ = _ => auto with jsl
  end.
Hint Extern 0 (?x ⊑ ?x) => reflexivity : jsl.
Hint Extern 0 (?x ⊑ ?x ⊔ _) => apply join_ext_l : jsl.
Hint Extern 0 (?x ⊑ _ ⊔ ?x) => apply join_ext_r : jsl.
Hint Extern 2 (_ ⊔ _ ⊑ _) => apply join_least : jsl.
Hint Resolve join_ext_l : jsl.
Hint Resolve join_ext_r : jsl.
Hint Resolve join_least : jsl.

Lemma join_idempotent `{JSL : @JoinSemiLattice A E J} `{AS : AntiSymm A R E} (x : A) :
  R x (x ⊔ x).
Proof.
  eapply anti_symm; eauto.
  - by eapply join_ext_l. 
  - by eapply join_least.
Qed.

Hint Extern 1 (?x = ?x ⊔ ?x) => apply join_idempotent : jsl.
(* Hint Resolve (@join_idempotent _ _ _ _ (=)). *)
Hint Extern 2 (_ = _ ⊔ _) => apply (anti_symm (⊑)) : jsl.
Hint Extern 2 (_ ⊔ _ = _) => symmetry; apply (anti_symm (⊑)) : jsl.

Instance JSL_Comm `{E : Extension A} `{J : Join A} `{@PartialOrder A (⊑)} `{@JoinSemiLattice A E J} : @Comm A _ (=) (⊔).
Proof. move => ? ?. solve_jsl. Qed.
  
Instance JSL_Assoc `{E : Extension A} `{J : Join A} `{@PartialOrder A (⊑)} `{@JoinSemiLattice A E J} : @Assoc A (=) (⊔).
Proof.
  move => ? ? ?.
  apply: anti_symm; repeat apply: join_least; eauto using PreOrder_Transitive with jsl.
Qed.

Hint Extern 0 (?x ⊑ ?x ⊔ _ ⊔ _) => rewrite <- (assoc (⊔)) : jsl.
Hint Extern 0 (?x ⊔ ?y ⊔ ?z = ?x ⊔ (?y ⊔ ?z)) => rewrite <- (assoc (⊔)) : jsl.
Hint Extern 0 (?x ⊔ (?y ⊔ ?z) = ?x ⊔ ?y ⊔ ?z) => rewrite <- (assoc (⊔)) : jsl.


Instance JSL_join_Proper `{JoinSemiLattice A} : Proper ((⊑) ==> (⊑) ==> (⊑)) (⊔).
Proof. intros ? ? ? ? ? ?. apply join_least; etrans; eauto with jsl. Qed.

(* Generic Instances for Extension, Join and JoinSemiLattice *)
Instance prod_Extension `{Extension A} `{Extension B} : Extension (A * B)
  := λ p1 p2, p1.1 ⊑ p2.1 ∧ p1.2 ⊑ p2.2.
Instance prod_PreOrder 
  `{Extension A} `{Extension B} `{PreOrder A (⊑)} `{PreOrder B (⊑)}
  : @PreOrder (A * B) (⊑).
Proof.
  econstructor.
  - move => ?; cbv; split; by apply PreOrder_Reflexive.
  - move => ? ? ? [H11 H12] [H21 H22]; split.
    + by apply (PreOrder_Transitive _ _ _ H11 H21).
    + by apply (PreOrder_Transitive _ _ _ H12 H22).
Qed.

Instance prod_PartialOrder `{Extension A} `{Extension B} `{PartialOrder A (⊑)} `{PartialOrder B (⊑)} : @PartialOrder (A * B) (⊑).
Proof.
  econstructor.
  - by apply prod_PreOrder.
  - move => [? ?] [? ?] [? ?] [? ?]. f_equal; by apply: anti_symm.
Qed.
  
Instance prod_Join `{Join A} `{Join B} : Join (A * B) :=
  λ p1 p2, (p1.1 ⊔ p2.1, p1.2 ⊔ p2.2).
Instance prod_JSL `{JoinSemiLattice A} `{JoinSemiLattice B} : JoinSemiLattice (A * B).
Proof.
  econstructor.
  - exact: prod_PreOrder.
  - move => ? ?; split => /=; solve_jsl.
  - move => ? ?; split => /=; solve_jsl.
  - move => ? ? ? [? ?] [? ?]. split => /=; solve_jsl.
Qed.
Instance prod_extension_eq_dec
         `{Extension A} `{∀ a a' : A, Decision (a ⊑ a')}
         `{Extension B} `{∀ a a' : B, Decision (a ⊑ a')} :
         ∀ ab ab' : (A * B), Decision (ab ⊑ ab').
Proof.
  move => ab ab'.
  case: (decide (fst ab ⊑ fst ab'));
  case: (decide (snd ab ⊑ snd ab'));
    [left => //|right|right|right]; move => []; abstract naive_solver.
Qed.


Instance option_Extension `{Extension T} : Extension (option T) :=
  λ o1 o2, match o1 with
           | None => True
           | Some t1 => match o2 with
                        | None => False
                        | Some t2 => t1 ⊑ t2
                        end
           end.

Instance option_StrictExtension `{StrictExtension T} : StrictExtension (option T) :=
  λ o1 o2, match o1 with
           | None => match o2 with
                     | None => False
                     | Some t2 => True
                     end
           | Some t1 => match o2 with
                        | None => False
                        | Some t2 => t1 ⊏ t2
                        end
           end.

Instance option_TightExtensions `{TightExtensions T} : TightExtensions (option T).
Proof.
  constructor; intros ? ?; split; cbv;
    repeat case_match; simplify_option_eq => //; try auto.
  - move/tight_extension => [->|]; by auto.
  - move => [[?]|?].
    + apply/tight_extension; by auto.
    + apply/tight_extension; by auto.
  - by intuition.
Qed.

Instance option_CompatibleExtensions `{CompatibleExtensions T}
  : CompatibleExtensions (option T).
Proof.
  econstructor; intros ? ? ?; cbv; repeat case_match;
    simplify_option_eq => //.
  - exact: compat_ext_l.
  - exact: compat_ext_r.
Qed.

Instance option_strict_subrel
  `{StrictExtension T} `{Extension T} 
  `{subrelation _ ((⊏) : relation T) ((⊑) : relation T)} 
  : subrelation ((⊏) : relation (option T)) ((⊑) : relation (option T)).
Proof. move => [?|] [?|] //=. cbn. eauto with typeclass_instances. Qed.

Instance option_StrictOrder `{StrictExtension T} `{StrictOrder T (⊏)}
  : @StrictOrder (option T) (⊏). 
Proof.
  econstructor.
  - move => ?. cbv; repeat case_match; last auto.
    by apply: StrictOrder_Irreflexive.
  - move => ? ? ?. cbv; repeat case_match => //.
    by apply: StrictOrder_Transitive.
Qed.

Instance option_Join `{Join T} : Join (option T) :=
  λ o1 o2, match o1 with
           | None => o2
           | Some t1 => match o2 with
                        | None => o1
                        | Some t2 => Some $ t1 ⊔ t2
                        end
           end.
Instance option_PreOrder `{Extension T} `{PreOrder T (⊑)} : @PreOrder (option T) (⊑).
Proof.
  econstructor.
  - move => ?; cbv; repeat case_match; last auto.
    exact: PreOrder_Reflexive.
  - move => ? ? ?; cbv; repeat case_match => //.
    exact: PreOrder_Transitive.
Qed.

Instance option_PartialOrder `{Extension T} `{PartialOrder T (⊑)} 
  : @PartialOrder (option T) (⊑).
Proof.
  econstructor.
  - by eauto with typeclass_instances.
  - move => [?|] [?|] //; cbv => ? ?. f_equal. exact: anti_symm.
Qed.

Instance option_Reflexive `{Extension T} `{Reflexive T (⊑)}
  : @Reflexive (option T) (⊑).
Proof. move => ?. cbv. by case_match. Qed.

Instance option_AntiSymm `{Extension T} `{AntiSymm T (=) (⊑)}
  : AntiSymm (=) ((⊑) : relation (option T)).
Proof.
  move => ? ?; cbv; repeat case_match => //; move => H1 H2. apply/f_equal.
  by apply (@anti_symm _ _ _ H0).
Qed.

Instance option_Comm `{Join T} `{@Comm T _ (=) join} : @Comm (option T) _ (=) (join).
Proof.
  move => ? ?; cbv; repeat case_match => //. f_equal. by apply comm.
Qed.

Instance option_Total `{Extension T} `{Total T (⊑)}: (@Total (option T) (⊑)).
Proof.
  move => [?|] [?|]; eauto with jsl.
  - by right.
  - by left.
Qed.

(* Instance option_Assoc `{Join T} `{@Assoc T (=) join} : @Assoc (option T) (=) (join). *)
(* Proof. *)
(*   move => ? ? ?. cbv; repeat case_match => //; f_equal; simplify_option_eq. *)
(*   - by apply assoc. *)
(*   - reflexivity. *)
(*   - reflexivity. *)
(* Qed. *)

Instance option_JSL `{JoinSemiLattice T} : JoinSemiLattice (option T).
Proof.
  econstructor.
  - exact: option_PreOrder.
  - move => ? ?. cbv; repeat case_match => //; simplify_option_eq.
    + by apply join_ext_l.
    + by apply PreOrder_Reflexive.
  - move => ? ?. cbv; repeat case_match => //; simplify_option_eq.
    + by apply join_ext_r.
    + by apply PreOrder_Reflexive.
  - move => ? ? ?; cbv; repeat case_match => //; simplify_option_eq.
    by apply join_least.
Qed.

Instance option_extension_eq_dec
         `{Extension A} `{∀ a a' : A, Decision (a ⊑ a')} :
         ∀ oa oa' : (option A), Decision (oa ⊑ oa').
Proof.
  move => [oa|] [oa'|].
  - case: (decide (oa ⊑ oa')); abstract naive_solver.
  - right => [[]].
  - left => //.
  - left => //.
Qed.

Instance option_tightextension_eq_dec
         `{StrictExtension A} `{∀ a a' : A, Decision (a ⊏ a')} :
         ∀ oa oa' : (option A), Decision (oa ⊏ oa').
Proof.
  move => [oa|] [oa'|].
  - case: (decide (oa ⊏ oa')); abstract naive_solver.
  - right => [[]].
  - by left.
  - right => [[]].
Qed.

(* Some auxilliary lemmas about option instances *)
Lemma option_ext_is_Some `{Extension T} (o1 o2 : option T)
  : o1 ⊑ o2 -> is_Some o1 -> is_Some o2.
Proof.
  cbv; (repeat case_match).
  - by eauto.
  - by eauto.
  - by move => _ [].
Qed.

Lemma option_ext_strict_None `{StrictExtension T} (o : option T):
  ¬ o ⊏ None.
Proof. destruct o; by cbv. Qed.

Lemma join_None `{Join T} (o1 : option T) : o1 ⊔ None = o1.
Proof. by cbv; repeat case_match. Qed.
Lemma join_Some_not_None `{Join T} t1 o1 : None = Some t1 ⊔ o1 -> False.
Proof. by cbv; repeat case_match. Qed.
Lemma option_subseteq_None_l `{Extension T} (o1 o2 : option T) 
  : o1 ⊑ None ↔ o1 = None.
Proof. by cbv; repeat case_match. Qed.


(* Positive instances *)
Instance positive_Extension : Extension positive := (≤)%positive.
Instance positive_StrictExtension : StrictExtension positive := (<)%positive.
Instance positive_TightExtensions : TightExtensions positive.
Proof.
  constructor => ? ?. split.
  - move/Pos.le_lteq. by intuition.
  - move => ?. apply/Pos.le_lteq. by intuition.
Qed.
Instance positive_CompatibleExtensions : CompatibleExtensions positive.
Proof.
  constructor.
  - exact: Pos.lt_le_trans.
  - exact: Pos.le_lt_trans.
Qed.

Instance positive_strict_subrel : @subrelation positive (⊏) (⊑).
Proof. move => ? ?. exact: Pos.lt_le_incl. Qed.
Instance positive_Join : Join positive := Pos.max.
Instance positive_PreOrder : PreOrder (⊑)%positive.
Proof.
  econstructor; unfold extension, positive_Extension;
    by eauto using Pos.le_refl, Pos.le_trans.
Qed.
Instance positive_PartialOrder : PartialOrder (⊑)%positive.
Proof.
  econstructor.
  - by eauto with typeclass_instances.
  - exact: Pos.le_antisym.
Qed.
Instance positive_StrictOrder : StrictOrder (⊏)%positive.
Proof.
  econstructor; unfold strict_extension, positive_StrictExtension.
  - move => ? ?. exact: Pos.lt_irrefl.
  - exact: Pos.lt_trans.
Qed.
Instance positive_Total : Total (⊑)%positive.
Proof.
  move => x y. case: (decide (x ≤ y)%positive); try tauto.
  move/Pos.lt_nle/Pos.lt_le_incl. tauto.
Qed.

Instance positive_Reflexive : Reflexive (⊑)%positive.
Proof. exact: Pos.le_refl. Qed.
Instance positive_AntiSymm : AntiSymm (=) (⊑)%positive.
Proof. exact: Pos.le_antisym. Qed.
(* Instance positive_Comm : Comm (=) (join)%positive. *)
(* Proof. exact: Pos.max_comm. Qed. *)
(* Instance positive_Assoc : Assoc (=) (join)%positive. *)
(* Proof. exact: Pos.max_assoc. Qed. *)

Instance poisitive_JSL : JoinSemiLattice positive.
Proof.
  econstructor.
  - by eauto with typeclass_instances.
  - move => ? ?. exact: Pos.le_max_l.
  - move => ? ?. exact: Pos.le_max_r.
  - move => ? ? ?. exact: Pos.max_lub.
Qed.


(* gmap instances *)
Instance gmap_countable `{Countable K} `{Countable A} : Countable (gmap K A) :=
  inj_countable (map_to_list : _ -> list (K * A))
                      ((λ l : list (_ * _), Some (map_of_list l)) : list _ -> option _) _.
Proof.
  intros. f_equal. exact: map_of_to_list.
Qed.

Instance gmap_Join `{Countable K} `{Join A} : Join (gmap K A) :=
  union_with (λ a1 a2, Some $ a1 ⊔ a2).

Lemma lookup_join `{Countable K} `{Join A} (f1 f2 : gmap K A) k
  : (f1 ⊔ f2) !! k = f1 !! k ⊔ f2 !! k.
Proof.
  rewrite lookup_union_with.
  by do 2!case: (_ !! k).
Qed.

Instance gmap_Assoc `{Countable K} `{Join A} `{@Assoc A (=) join}
  : @Assoc (gmap K A) (=) join.
Proof.
  move => ? ? ?. apply: map_eq => k.
  rewrite !lookup_union_with.
  repeat case: (_ !! k) => //=.
  move => ? ? ?. f_equal. by apply assoc.
Qed.

Instance gmap_Extension `{Countable K} `{Extension A} : Extension (gmap K A) :=
  λ f1 f2, ∀ k, f1 !! k ⊑ f2 !! k.

Instance gmap_PreOrder `{Countable K} `{Extension A} `{PreOrder A (⊑)} 
  : @PreOrder (gmap K A) (⊑).
Proof.
  econstructor.
  - move => ? ?; reflexivity.
  - move => ? ? ? ? ? ?. by etransitivity; eauto.
Qed.
Instance gmap_PartialOrder `{Countable K} `{Extension A} `{PartialOrder A (⊑)} 
  : @PartialOrder (gmap K A) (⊑).
Proof.
  econstructor.
  - by eauto with typeclass_instances.
  - move => ? ? ? ?. apply: map_eq => k. by apply: anti_symm.
Qed.

Instance gmap_JSL `{Countable K} `{JoinSemiLattice A} : JoinSemiLattice (gmap K A).
Proof.
  econstructor.
  - exact: gmap_PreOrder.
  - move => ? ? ?. rewrite lookup_join. by apply join_ext_l.
  - move => ? ? ?. rewrite lookup_join. by apply join_ext_r.
  - move => ? ? ? ? ? ?. rewrite lookup_join. by apply join_least.
Qed.


Lemma gset_neg_Forall `{Countable A} (P : A -> Prop)
  `{∀ a, Decision (P a)} (s : gset A)
  : ¬ set_Forall P s ↔ set_Exists (λ x, ¬ P x) s.
Proof.
  split.
  - case: (collection_choose_or_empty (filter (λ x, ¬ P x) s)) => [[x]|].
    + set_unfold => [[? ?]] _. by exists x.
    + set_unfold => H1 H2. exfalso. apply: H2 => x Inx.
      specialize (H1 x).
      case: (decide (P x)); abstract naive_solver.
  - unfold set_Exists, set_Forall. abstract naive_solver.
Qed.

Lemma gset_neg_Exists `{Countable A} (P : A -> Prop) 
  `{∀ a, Decision (P a)} (s : gset A)
  : ¬ set_Exists P s ↔ set_Forall (λ x, ¬ P x) s.
Proof.
  split => [NEX|FA].
  - apply dec_stable => /gset_neg_Forall EX.
    apply: NEX. apply: (set_Exists_impl _ _ _ EX). intros. exact: dec_stable.
  - move => [? [? ?]]. exact: FA.
Qed.

Instance set_Forall_Impl `{Countable A} P
  : Proper (((⊆): relation (gset A)) --> impl) (set_Forall P).
Proof. unfold set_Forall => ? ? /= H1 ? x /H1. auto. Qed.
Instance set_Exists_Impl `{Countable A} P
  : Proper (((⊆) : relation (gset A)) ++> impl) (set_Exists P).
Proof. unfold set_Exists => ? ? /= H1 [x [/H1]]. eauto. Qed.
Instance set_Forall_Proper `{Countable A} P
  : Proper (((≡) : relation (gset A)) ==> impl) (set_Forall P) | 0.
Proof. solve_proper. Qed.
Instance set_Exists_Proper `{Countable A} P
  : Proper (((≡) : relation (gset A)) ==> impl) (set_Exists P) | 0.
Proof. solve_proper. Qed.


Instance gmap_extension_eq_dec
         `{Countable K}
         `{Extension A} `{∀ a a' : A, Decision (a ⊑ a')} :
         ∀ f f' : (gmap K A), Decision (f ⊑ f').
Proof.
  move => f f'.
  destruct (decide (set_Forall (λ k, f !! k ⊑ f' !! k) (dom (gset K) f))) as [Y|N].
  - left => k.
    case: (decide (k ∈ dom (gset K) f)).
    + by move/Y.
    + move/not_elem_of_dom => -> //.
  - right. case/gset_neg_Forall : N => x [/elem_of_dom [a ?]] NExt ?.
    by apply NExt.
Qed.

Lemma gmap_join_insert_l {A} `{Countable K}
  `{JoinSemiLattice A} `{AntiSymm A (=) (⊑)} {m1 m2 : gmap K A} k v :
  <[k:=v]>(m1 ⊔ m2) = <[k:=v]>m1 ⊔ <[k:=v]>m2.
Proof.
  apply/map_eq => k'. case: (decide (k' = k)) => [->|].
  - rewrite !lookup_join.
    rewrite !lookup_insert.
    cbn; f_equal. apply: anti_symm.
    + by apply join_ext_l.
    + by apply join_least.
  - move => ?.
    rewrite !lookup_join.
    rewrite !lookup_insert_ne //. by rewrite lookup_join.
Qed.


Lemma ext_insert `{Countable K} `{JoinSemiLattice A} (m : gmap K A) k a :
  m !! k ⊑ Some a ->
  m ⊑ <[k := a]>m.
Proof.
  move => E k'.
  case (decide (k' = k)) => [->|?].
  - by rewrite lookup_insert.
  - by rewrite lookup_insert_ne //.
Qed.


(* unit instances *)
Instance unit_Extension : Extension () :=
  λ u1 u2, True.
Instance unit_Join : Join () :=
  λ u1 u2, tt.
Instance unit_PreOrder : @PreOrder () (⊑).
Proof. econstructor; by auto. Qed.
Instance unit_JSL : JoinSemiLattice ().
Proof. econstructor; first exact: unit_PreOrder; by auto. Qed.


(* Lifting relations via projections *)
Definition rel_of {T X : Type} (f : T -> X) (r : relation X) : relation T :=
  λ (t1 t2 : T), r (f t1) (f t2).
Arguments rel_of [_ _] _ _ _ _ /.
Instance rel_of_PreOrder
  : (@PreOrder T r) → (@PreOrder X (@rel_of X T f r)) | 10.
Proof.
  econstructor; repeat intro; unfold rel_of in *;
    eauto using PreOrder_Reflexive, PreOrder_Transitive.
Qed.
Instance rel_of_Transitive
  : (@Transitive T r) → (@Transitive X (@rel_of X T f r)) | 10.
Proof.
  repeat intro; unfold rel_of in *.
    by eauto with typeclass_instances.
Qed.
Instance rel_of_Irreflexive 
  : (@Irreflexive T r) → (@Irreflexive X (@rel_of X T f r)) | 10.
Proof.
  repeat intro; unfold rel_of in *.
    by eapply irreflexive_eq.
Qed.
Instance rel_of_Total : (@Total T r) → (@Total X (@rel_of X T f r)) | 10.
Proof.
  repeat intro; unfold rel_of in *.
  eauto with typeclass_instances.
Qed.
Instance flip_Total : (@Total T r) → (@Total T (flip r)) | 10.
Proof.
  repeat intro; eauto with typeclass_instances.
Qed.

(* Miscellaneous *)
Lemma elem_of_contains (T : Type) (l1 l2 : list T) (t : T) 
  : l2 ⊆+ l1 → t ∈ l2 → t ∈ l1.
Proof.
  induction 1 => //.
  - move/elem_of_cons => [->|?]; apply/elem_of_cons; tauto.
  - move/elem_of_cons => [->|/elem_of_cons [->|?]]; apply/elem_of_cons.
    + right; apply/elem_of_cons; tauto. 
    + by left.
    + right; apply/elem_of_cons; tauto.
  - move/IHsubmseteq => ?. apply/elem_of_cons. tauto.
  - by move/IHsubmseteq1/IHsubmseteq2.
Qed.

Notation "'∃min_[' R ']' m ∈ M , P" :=
  (∃ m, m ∈ M ∧ (∀ m', m' ∈ M → R m m') ∧ P)
    (at level 200, m at level 1, format "∃min_[ R ]  m  ∈  M ,  P").
Notation "'∃max_[' R ']' m ∈ M , P" :=
  (∃min_[flip R] m ∈ M, P) (at level 200, m at level 1).
Notation "'∀min_[' R ']' m ∈ M , P" :=
  (∀ m, m ∈ M ∧ (∀ m', m' ∈ M → R m' m) → P)
    (at level 200, m at level 1, format "∀min_[ R ]  m  ∈  M ,  P").
Notation "'∀max_[' R ']' m ∈ M , P" :=
  (∀min_[flip R] m ∈ M, P) (at level 200, m at level 1).


Lemma gset_nonempty_min_strict
  `{Countable A} `{Transitive A R} `{Total A R} 
  `{∀ a a', Decision (R a a')} (M : gset A) (NE: M ≢ ∅)
  : ∃ m, m ∈ M ∧ (∀ m', m' ∈ M ∖ {[m]} → R m m').
Proof.
  case: (minimal_exists R M NE) => [x [? Min]].
  exists x; split; auto. intros m In.
  case: (H2 x m) => // ?. have: R m x by case: (H1 x m).
  apply Min. set_solver+In.
Qed.

Lemma gset_nonempty_min
  `{Countable A} `{Transitive A R} `{Reflexive A R} `{Total A R} 
  `{∀ a a', Decision (R a a')} (M : gset A) (NE: M ≢ ∅)
  : ∃min_[R] m ∈ M, True : Prop.
Proof.
  destruct (gset_nonempty_min_strict _ NE) as [m [In SR]].
  exists m. repeat split => //.
  move => m'. case (decide (m = m')) => [-> //|]. abstract set_solver+SR.
Qed.

Instance gset_SimpleCollection `{Countable K} : SimpleCollection K (gset K) := _.
Instance gset_Collection `{Countable K} : Collection K (gset K) := _.
