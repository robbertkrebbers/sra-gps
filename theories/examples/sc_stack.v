From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.

From igps Require Import notation malloc na proofmode weakestpre.

Import uPred.

(** An SC stack that is built up by non-atomics.
    This is used to batch deallocations in RCU. **)

Section SCStack.

  Local Notation next := 0.
  Local Notation data := 1.

  Definition newStack : base.val :=
    λ: <>,
      let: "s" := Alloc in
      ["s"]_na <- #0  ;; "s".

  Definition push : base.val :=
    λ: "s" "x",
      let: "n" := malloc (#2) in
      ["n" + #data]_na <- "x" ;;
      let: "h" := ["s"]_na in
      ["n" + #next]_na <- "h";;
      ["s"]_na <- ((int)"n").

  Definition pop : base.val :=
    λ: "s", 
      let: "h" := ["s"]_na in
      if: "h" = #0
      then #0
      else
        let: "v" := [((loc)"h") + #data]_na in
        let: "h'" := [((loc)"h") + #next]_na in
        ["s"]_na <- "h'" ;;
        Dealloc ((loc)"h" + #data) ;;
        Dealloc ((loc)"h" + #next) ;;
        "v".

  Section proof.
    Context `{fG : foundationG Σ}.
    Set Default Proof Using "Type".
    Notation vPred := (@vPred Σ).

    Context (P: Z → vPred).

    Local Open Scope VP.

    Definition SCStack': (loc -c> list Z -c> @vPred_ofe Σ) 
                             -c> loc -c> list Z -c> @vPred_ofe Σ
    := (fun F l A =>
        ∃ l', ZplusPos next l ↦ l' ∗
        match A with
        | nil => l' = 0
        | v::A' => ■ (0 < l') ∗ Z.to_pos (l' + data) ↦ v ∗ P v 
                              ∗ ▷ F (Z.to_pos l') A'
        end).

    Close Scope VP.
    Instance SCStack'_inhabited: Inhabited (loc -c> list Z -c> @vPred_ofe Σ)
      := populate (λ _ _, True%VP).

    Instance SCStack'_contractive:
        Contractive (SCStack').
    Proof.
      intros ? ? ? H ? A ?.
      repeat (apply uPred.exist_ne => ?).
      apply uPred.sep_ne; [done|].
      destruct A as [|v A]; [done|].
      repeat (apply uPred.sep_ne; [done|]).
      apply later_contractive. destruct n => //. by apply (H).
    Qed.

    Definition SCStack := fixpoint (SCStack').

    Lemma newStack_spec:
      {{{{ True }}}}
         newStack #()
      {{{{ (s: loc), RET #s; SCStack s nil }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS _ Post".
      wp_seq. iNext. wp_bind Alloc.
      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (x) "kS os".
      wp_seq. iNext.
      wp_bind ([_]_na <-_)%E.
      iApply (na_write with "[%] kI kS [$os]"); [done|done|].
      iNext. iViewUp; iIntros "kS os".
      wp_seq. iNext. wp_value.
      iApply ("Post" with "[%] kS [os]"); first done.
      rewrite (fixpoint_unfold (SCStack') _ _ _ ).
      iExists _. by iFrame "os".
    Qed.

    Lemma push_spec s v A:
      {{{{ SCStack s A ∗ P v }}}}
         push #s #v
      {{{{ RET #(); SCStack s (v :: A) }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS (Stack & P) Post".

      wp_lam. iNext. wp_value. wp_let. iNext.
      wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].
      iNext. iViewUp. iIntros (n) "kS oLs".
      rewrite -bigop.vPred_big_opL_fold
              big_op.big_sepL_cons big_op.big_opL_singleton.
      iDestruct "oLs" as "(ol & od)".
      wp_let. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write with "[%] kI kS od"); [done|done|].
      iNext. iViewUp. iIntros "kS od".
      wp_seq. iNext. wp_bind ([_]_na)%E.

      rewrite (fixpoint_unfold (SCStack') _ _ _ ).
      iDestruct "Stack" as (h) "[oH oT]".
      iApply (na_read with "[%] kI kS oH"); [done|done|].
      iNext. iViewUp. iIntros (z) "kS [% oH]". subst z.
      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write with "[%] kI kS ol"); [done|done|].
      iNext. iViewUp. iIntros "kS ol".
      wp_seq. iNext. wp_op. iNext.
      iApply (na_write with "[%] kI kS [$oH]"); [done|done|].

      iNext. iViewUp. iIntros "kS oH".
      iApply ("Post" with "[%] kS"); first done.

      rewrite (fixpoint_unfold (SCStack') _ _ _ ).
      iExists _. iFrame "oH P". iSplitL ""; first (iPureIntro; lia).
      iSplitL "od".
      - rewrite (_: Z.to_pos (Z.pos n + data) = ZplusPos data n); first done.
        rewrite /ZplusPos. f_equal. omega.
      - iNext. rewrite (fixpoint_unfold (SCStack') _ _ _ ).
        iExists h. iFrame "oT ol".
    Qed.

    Lemma pop_spec s A:
      {{{{ SCStack s A  }}}}
         pop #s
      {{{{ (z: Z), RET #z;
           match A with
           | nil => ■ (z = 0) ∗ SCStack s A
           | v :: A' =>  ■ (z = v) ∗ SCStack s A' ∗ P v
           end }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS Stack Post".
      wp_lam. iNext.
      wp_bind ([_]_na)%E.
      rewrite (fixpoint_unfold (SCStack') _ _ _ ).
      iDestruct "Stack" as (h) "[oH oT]".
      iApply (na_read with "[%] kI kS oH"); [done|done|].
      iNext. iViewUp. iIntros (z) "kS [% oH]". subst z.
      wp_seq. iNext.
      destruct A as [|v A'].
      - iDestruct "oT" as "%". subst h.
        wp_op => [_|//]. iNext.
        iApply wp_if_true. iNext.
        wp_value.
        iApply ("Post" with "[%] kS"); first done.
        iSplitL ""; first done.
        rewrite (fixpoint_unfold (SCStack') _ _ _ ). iExists _. by iFrame "oH".
      - iDestruct "oT" as "(% & od & P & oT)".
        wp_op => [?|_]; first omega. iNext.
        iApply wp_if_false. iNext.
        wp_bind ([_]_na)%E. wp_op. iNext. wp_op. iNext.

        rewrite (_: ZplusPos data (Z.to_pos h) = Z.to_pos (h + 1)); last first.
        { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|auto]. }
        iApply (na_read with "[%] kI kS od"); [done|done|].

        iNext. iViewUp. iIntros (z) "kS [% od]". subst z.
        wp_seq. iNext.
        wp_bind ([_]_na)%E. wp_op. iNext. wp_op. iNext.

        rewrite (fixpoint_unfold (SCStack') _ _ _ ).
        iDestruct "oT" as (h') "[oP oT]".
        iApply (na_read with "[%] kI kS oP"); [done|done|].
        iNext. iViewUp.  iIntros (z) "kS [% oP]". subst z.
        wp_seq. iNext. wp_bind ([_]_na <- _)%E.
        iApply (na_write with "[%] kI kS [$oH]"); [done|done|..].
        iNext. iViewUp. iIntros "kS oH".
        wp_seq. iNext.
        wp_bind (Dealloc _). wp_op. iNext. wp_op. iNext.

        iApply (wp_mask_mono); first auto.
        rewrite (_: ZplusPos data (Z.to_pos h) = Z.to_pos (h + data)); last first.
        { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|auto]. }
        iApply (dealloc with "[%] kI kS [$od]"); first done.

        iNext. iViewUp. iIntros "kS _".
        wp_seq. iNext.
        wp_bind (Dealloc _). wp_op. iNext. wp_op. iNext.
        iApply (wp_mask_mono); first auto.
        iApply (dealloc with "[%] kI kS [$oP]"); first done.
        iNext. iViewUp. iIntros "kS _".
        wp_seq. iNext.

        wp_value.
        iApply ("Post" with "[%] kS"); first done.
        iSplitL ""; first done. iFrame "P".
        rewrite (fixpoint_unfold (SCStack') _ _ _ ).
        iExists _. by iFrame.
    Qed.
  End proof.
End SCStack.
