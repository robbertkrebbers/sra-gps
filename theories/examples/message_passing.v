From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import excl.

From igps Require Export malloc repeat notation wp_tactics escrows.
From igps Require Import persistor viewpred proofmode weakestpre protocols.
From igps.base Require Import fork.
From igps.gps Require Import plain.
From igps.examples Require Import unit_token.

Import uPred.

Definition message_passing : base.expr :=
  let: "x" := Alloc in
  let: "y" := Alloc in
    ["x"]_na <- #0 ;;
    ["y"]_na <- #0 ;;
    Fork (["x"]_na <- #37 ;; ["y"]_at <- #1) ;;
    repeat (["y"]_at) ;;
    ["x"]_na.

Section Interpretation.
  Context `{foundationG Σ} `{!absViewG Σ} `{!uTokG Σ}.

  Set Default Proof Using "Type".

  Definition XE (x : loc) (γ: gname) : @vPred Σ
    := [es Tok γ ⇝ x ↦ 37 ].

  Definition YP x (γ: gname) (s: pr_state boolProtocol) (v: Z) : @vPred Σ :=
    match s with
    | false => ■ (v = 0)
    | true => ■ (v = 1) ∗ XE x γ
    end.

  Global Instance YP_persistent x γ s v V: PersistentP (YP x γ s v V).
  Proof. destruct s; by iIntros "#YP". Qed.

  Local Open Scope VP.

  Definition mpInt x γ : interpC Σ boolProtocol
    := λ b _ s v, if b then True else YP x γ s v.

  Global Instance interp_persistent : PersistentP (mpInt x γ b l s v V).
  Proof. destruct b; intros; apply _. Qed.
End Interpretation.

Section proof.
  Context `{fG : foundationG Σ}
          `{aG : absViewG Σ}
          `{mG : uTokG Σ}
          `{persG : persistorG Σ}
          `{agreeG : !gps_agreeG Σ boolProtocol}
          `{gG: !gpsG Σ boolProtocol _}.

  Set Default Proof Using "Type*".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Open Scope vPred_scope.

  Lemma message_passing_spec:
    {{{{ ☐ PersistorCtx }}}}
        (message_passing)
    {{{{ (v: Z), RET #v; ■ (v = 37) }}}}%C.
  Proof.
    intros. iViewUp; iIntros "#kI kS #kIp Post". rewrite /message_passing.
    wp_bind Alloc.
    iApply (alloc with "[%] kI kS []"); [done|done|done|].
    iNext. iViewUp. iIntros (x) "kS Nax".
    wp_seq. iNext.
    wp_bind Alloc.
    iApply (alloc with "[%] kI kS []"); [done|done|done|].
    iNext. iViewUp. iIntros (y) "kS Nay".
    wp_seq. iNext.
    wp_bind ([_]_na <- _)%E.

    iApply (na_write with "[%] kI kS [$Nax]"); [done|done|].
    iNext. iViewUp; iIntros "kS Nax".
    wp_seq. iNext.
    wp_bind ([_]_na <- _)%E.

    iMod (own_alloc (Excl ())) as (γ) "Tok"; first done.

    iApply (GPS_PP_Init_default (mpInt x γ) false with "[%] kI kS [$kIp $Nay]");
    [done|done|done|..].
    { iNext. by iSplitL "". }

    iNext. iViewUp; iIntros "kS #yPRT".
    wp_seq. iNext.

    wp_bind (Fork _)%E.
    iApply (wp_mask_mono (↑physN)); first done.
    iCombine "Nax" "yPRT" as "SndT".
    iApply (f_fork _ ((x ↦ 0) V_l ∗ [PP y in false @ () | mpInt x γ ] V_l)%I
      with "[$kI $kS $SndT Post Tok]").
    iSplitL "Post Tok".
    - iNext. iViewUp. iIntros "kS".
      wp_seq. iNext. wp_bind ((rec: "f" <> := _ ) _)%E.
      (iLöb as "IH" forall (V_l) "kIp yPRT ∗").
      (* TODO: kIp is a spurious dependency *)
      iApply wp_repeat; first auto.
      iApply (GPS_PP_Read (p:=()) (mpInt x γ) True (YP x γ)
        with "[%] kI kS [$yPRT]"); [done|done|done|..].
      + iSplitL.
        * iNext. iIntros (?). iViewUp. iIntros "_".
          iLeft. iIntros (??) "_ [YP  _]". by simpl.
        * iNext. iIntros (? s v). iViewUp. by iIntros "[? #$]".
      + iNext. iViewUp; iIntros (s v) "kS (_ & #yPRT5 & #YP)".
        destruct s.
        * iDestruct "YP" as "(% & XE)".
          iExists true. iSplit; first by rewrite H.
          iDestruct (escrow_apply (Tok γ) (x ↦ 37)) as "EA".
          iSpecialize ("EA" with "[]").
          { iIntros (??) "_ ?". by iApply unit_tok_exclusive. }
          erewrite decide_False; last auto.
          iSpecialize ("EA" $! _ _ with "[%] [$XE $Tok]"); first done.
          iDestruct (fupd_mask_mono with "EA") as "EA"; first auto.
          iNext. iNext. iMod "EA" as "Nax".
          wp_seq. iNext.
          iApply (na_read with "[%] kI kS Nax"); [done|done|].
          iNext. iViewUp; iIntros (z) "kS (% & Nax)".
          subst z. by iApply ("Post" with "[%] kS").
        * iDestruct "YP" as "%". 
          iExists false. iSplit; first by rewrite H.
          erewrite decide_True; last done.
          iNext. iNext.
          iApply ("IH" $! _ with "[kIp] [yPRT5] Post Tok kS").
          { iIntros "!#"; iFrame "kIp". }
          { iIntros "!#"; iFrame "yPRT5". }
    - iNext. iIntros (ρ). iViewUp. iIntros "_ kS (Nax & #yPRT')". move: HV => ->.
      wp_bind ([_]_na <- _)%E.
      iApply (na_write with "[%] kI kS [$Nax]"); [done|done|].
      iNext. iViewUp; iIntros "kS Nax".
      wp_seq. iNext.

      (* allocate escrow *)
      destruct (escrow_alloc (Tok γ) (x ↦ 37)) as [EA].
      iDestruct (EA _ with "[$Nax]") as "XE"; [done|].

      iDestruct (fupd_mask_mono _ ⊤ with "XE") as ">XE"; first auto.

      (* write release to final state *)
      iApply (GPS_PP_Write (p:=()) (mpInt x γ) true (XE x γ) (λ _, True)
              with "[%] kI kS [yPRT' XE]"); [done|done|move=>[|]; done|done|..].
      { iSplitL ""; [|by iFrame "XE"].
        iNext. iViewUp; iIntros "[oL _]". iModIntro.
        iSplitL ""; [done|]. iSplit; iNext; auto. by iFrame "oL". }

      (* trivial post cond *)
      iNext. iViewUp. auto.
  Qed.

End proof.
