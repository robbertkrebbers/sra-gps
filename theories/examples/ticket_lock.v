From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import coPset gmap auth excl.
From iris.base_logic Require Import big_op.

From igps Require Import escrows notation malloc repeat.
From igps Require Import persistor viewpred weakestpre proofmode bigop arith.
From igps.examples Require Import nat_tokens protocols.
From igps.gps Require Import singlewriter plain recursive.

(** Formalization of bounded ticket-lock examples.
    The proof is simplified by using single-writer protocols and 
      a simpler ticket tracker. **)

Definition ticketLockR
  := prodUR (coPset_disjUR) (authUR (gmapUR nat (exclR (optionC natC)))).

Class tklG Σ := TklG { tkl_tokG :> inG Σ ticketLockR }.
Definition tklΣ : gFunctors := #[GFunctor (constRF ticketLockR)].

Instance subG_tklΣ {Σ} : subG tklΣ Σ → tklG Σ.
Proof. solve_inG. Qed.

Section TicketLock.

  Context (C: positive).

  Local Notation ns := 0.
  Local Notation tc := 1.

  Definition newLock : base.val :=
    λ: <>,
      let: "x" := malloc (#2) in
        ["x" + #ns]_na <- #0 ;;
        ["x" + #tc]_na <- #0 ;;
        "x".

  Definition lock : base.val :=
    λ: "x",
      let: "y" := FAI C ("x" + #tc) in
      (repeat (let: "z" := ["x" + #ns]_at in "y" = "z")).

  Definition unlock : base.val :=
    λ: "x",
      let: "z" := ["x" + #ns]_at in
        ["x" + #ns]_at <- (("z" + #1) `mod` #(Z.pos C)).


  Notation Excl2 t := (Excl' (Some t)).
  Notation ExclNone := (Excl' None).
  Notation gmapNOpN := (gmap nat (excl $ option nat)).
  Section Arith.

    Instance Mtkt_dec (M: gmapNOpN) (i n : nat):
      Decision (∃ (t: nat), M !! i = Excl2 t ∧ n ≤ t).
    Proof.
      case (M !! i).
      - move => [[t|]|].
        + case (decide (n ≤ t)) => [?|?].
          * left. exists t. done.
          * right. by move => [? [[<-] ?]].
        + right. by move => [? [? ]].
        + right. by move => [? [? ]].
      - right. by move => [? [? ]].
    Qed.

    Definition Waiting (M: gmapNOpN) (n : nat) : gset nat
      := {[ i <- dom (gset nat) M
              | ∃ (t: nat), M !! i = Excl2 t ∧ n ≤ t ]}.

    Definition Ws M n : nat := size (Waiting M n).

    Definition ns_tkt_bound M (t n: nat) : Prop := 
      n ≤ t ∧ t ≤ n + Ws M n
      ∧ ∀ (i k: nat), M !! i = Excl2 k →
          (k < t) ∧ ∀ (j: nat), M !! j = Excl2 k → i = j.

    Lemma Waiting_ns_nonupdate M (n t i: nat) (ot: option nat)
      (Eq: M !! i = Excl' ot)
      (Le : n ≤ t):
      {[i]} ∪ Waiting M n  ⊆ Waiting (<[i:= Excl $ Some t]> M) n.
    Proof.
      move => j. case (decide (j = i)) => [->|NEq].
      - move => _. apply set_unfold_2. split.
        + exists t. rewrite lookup_insert. split; [auto|lia].
        + apply elem_of_dom. rewrite lookup_insert /=. by eexists.
      - move => /elem_of_union [/elem_of_singleton //|].
        apply set_unfold_2.
        setoid_rewrite lookup_insert_ne; last done.
        move => [? ?]. split; [auto|by apply dom_insert_subseteq].
    Qed.

    Lemma Waiting_subseteq_update M (n t t0 i: nat)
      (Eq: M !! i = Excl2 t0) (Le : n ≤ t0):
      Waiting (<[i:=Excl $ Some t]> M) (S t0) ⊆  Waiting M n.
    Proof.
      move => j. apply set_unfold_2.
      case (decide (j = i)) => [->|NEq].
      - split; [by eexists|apply elem_of_dom; by eexists].
      - move => [[t1 [ ]]].
        rewrite dom_insert_L elem_of_union
                elem_of_singleton lookup_insert_ne; last auto.
        move => ? ? [? //|?]. split; last done. exists t1. split; [done|lia].
    Qed.

    Local Instance Mtkt_dec2 (M: gmapNOpN) (i n t0 : nat):
      Decision (∃ (t: nat), M !! i = Excl2 t ∧ n ≤ t ∧ t < t0).
    Proof.
      case (M !! i).
      - move => [[t|]|].
        + case (decide (n ≤ t)) => [?|?];
          case (decide (t < t0)) => [?|?]; [by left; eexists|..];
          right; by move => [? [[<-] [? ?]]].
        + right. by move => [? [? ]].
        + right. by move => [? [? ]].
      - right. by move => [? [? ]].
    Qed.

    Definition Mtkt_range (M: gmapNOpN) (n t0 : nat) : gset nat
      := {[ i <- dom (gset nat) M |
            ∃ (t: nat), M !! i = Excl2 t ∧ n ≤ t ∧ t < t0 ]}.

    Definition Mtkt_at M n : gset nat
      := Mtkt_range M n (S n).

    Lemma Waiting_diff M (n t t0 i: nat)
      (Eq: M !! i = Excl2 t0)
      (Inj : ∀ i k : nat, M !! i = Excl2 k → k < t
              ∧ (∀ j : nat, M !! j = Excl2 k → i = j)):
      Waiting M n ∖ Waiting (<[i:=Excl $ Some t]> M) (S t0)
        ⊆  Mtkt_range M n t0.
    Proof.
      move => j.
      apply set_unfold_2.
      destruct (Inj _ _ Eq) as [Le Inj2].
      move => [[[t1 [Eq1 Le1]] E3] E2]. split; last done.
      exists t1. repeat split; [auto|auto|].
      case (decide (t1 < t0)) => [//|/Z.nlt_ge Le3]. exfalso.
      apply E2. split; last by apply dom_insert_subseteq.
      case (decide (j = i)) => [->|NEq].
      - exists t. rewrite lookup_insert. split; [done|lia].
      - exists t1. rewrite lookup_insert_ne; last done. split; first done.
        apply Zle_lt_or_eq in Le3 as [|Eq3]; first lia.
        apply Nat2Z.inj_iff in Eq3. subst t1. by apply Inj2 in Eq1.
    Qed.

    Lemma Mtkt_split M (t1 t2 t3: nat)
      (Le1: t1 ≤ t2) (Le2: t2 ≤ t3):
      Mtkt_range M t1 t3 ⊆ Mtkt_range M t1 t2 ∪ Mtkt_range M t2 t3.
    Proof.
      intros j. apply set_unfold_2.
      move => [[t [Eq [Le3 Le4]]] InD].
      case (decide (t2 ≤ t)) => [Le5|/Z.nle_gt Le6]; [right|left];
        (split; last auto); by eexists.
    Qed.

    Local Instance Mtkt_dec3 M (t: nat):
      Decision (∃ i, i ∈ dom (gset nat) M ∧ M !! i = Excl2 t).
    Proof.
      apply set_Exists_dec. intro j.
      case (M !! j); last by right.
      move => [[k|]|]; [|by right|by right].
      case (decide (k = t)) => [->|NEq]; by [left|right; move => []].
    Qed.

    Lemma Mtkt_singleton M (t: nat)
      (Inj : ∀ i k : nat, M !! i = Excl2 k →
              (∀ j : nat, M !! j = Excl2 k → i = j)):
      size (Mtkt_range M t (S t)) ≤ 1.
    Proof.
      case (decide (∃ i, i ∈ dom (gset nat) M  ∧ M !! i = Excl2 t)).
      - move => [i [InD Eq]].
        etrans;
          [instantiate (1:= size ({[i]}: gset nat))|by rewrite size_singleton].
        apply inj_le, subseteq_size. apply set_unfold_2.
        move => j [[t' [? [? ?]]] _]. apply (Inj _ t); last auto.
        rewrite (_: (t = t')%nat); first done. lia.
      - move => NEq. etrans; [instantiate (1:= 0%nat)|done].
        apply inj_le, Nat.le_0_r, size_empty_iff. apply set_unfold_2.
        move => j [[t' [Eq [? ?]]] ?]. apply NEq.
        exists j. split; first auto. rewrite Eq.
        rewrite (_: (t = t')%nat); [done|lia].
    Qed.

    Lemma Waiting_bound M (n t0: nat)
      (Inj : ∀ i k : nat, M !! i = Excl2 k →
              (∀ j : nat, M !! j = Excl2 k → i = j))
      (Le : n ≤ t0):
      n + size (Mtkt_range M n t0) ≤ t0.
    Proof.
      induction t0.
      - rewrite (_: n = 0%nat); last lia.
        apply Nat2Z.inj_le, Nat.le_0_r, size_empty_iff. apply set_unfold_2.
        move => ? [[? [_ [? ?]]] _]. omega.
      - rewrite Nat2Z.inj_succ in Le. apply Z.le_succ_r in Le as [Le|Eq].
        + assert (LeS: t0 ≤ S t0) by lia.
          assert (LeSub:= subseteq_size _ _ (Mtkt_split M n t0 (S t0) Le LeS)).
          etrans.
          * instantiate (1:= n +
                            size (Mtkt_range M n t0 ∪ Mtkt_range M t0 (S t0))).
            lia.
          * rewrite size_union_alt.
            rewrite Nat2Z.inj_succ Nat2Z.inj_add Zplus_assoc.
            apply Z.add_le_mono; first by apply IHt0.
            etrans.
            { instantiate (1:=  size (Mtkt_range M t0 (S t0))).
              apply inj_le, subseteq_size, gset_difference_subseteq. }
            { by apply Mtkt_singleton. }
        + rewrite Nat2Z.inj_succ -Eq.
          rewrite {2}(_: Z.of_nat n = n + 0%nat); last lia.
          apply Zplus_le_compat_l, inj_le, Nat.le_0_r, size_empty_iff.
          apply set_unfold_2. move => ? [[? [_ [? ?]]] _]. lia.
    Qed.

    Lemma Waiting_dom_size (M: gmapNOpN)
      (DS : dom (gset nat) M ≡ seq_set 0 (Pos.to_nat C)):
      Z.pos C = size (dom (gset nat) M).
    Proof.
      by rewrite (collection_size_proper _ _ DS) -positive_nat_Z seq_set_size.
    Qed.

    Lemma Waiting_size_le (M: gmapNOpN) (n: nat)
      (DS: dom (gset nat) M ≡ seq_set 0 (Pos.to_nat C)):
      Ws M n ≤ Z.pos C.
    Proof.
      rewrite (_: Z.pos C = size (dom (gset nat) M)).
      - apply inj_le, subseteq_size, subseteq_gset_filter.
      - by apply Waiting_dom_size.
    Qed.

    Lemma Waiting_size_lt (M: gmapNOpN) (n i: nat) (ot: option nat)
      (DS: dom (gset nat) M ≡ seq_set 0 (Pos.to_nat C))
      (Ini: M !! i = Excl' ot) (Lt: ot = None ∨ ∃ t, ot = Some t ∧ t < n):
      Ws M n < Z.pos C.
    Proof.
      rewrite (Waiting_dom_size M); last done.
      apply inj_lt, subset_size. apply set_unfold_2.
      split.
      - move => j [_ ?] //.
      - move => /(_ i) Eq. destruct Eq as [[t' [Eq ?]] _].
        + apply elem_of_dom. by eexists.
        + rewrite Eq in Ini.
          destruct Lt as [?|[? [? ?]]]; subst; inversion Ini. omega.
    Qed.

    Lemma ns_inj_insert (M: gmapNOpN) (t i: nat)
     (FA: ∀ i0 k : nat,
         M !! i0 = Excl2 k → k < t ∧ (∀ j : nat, M !! j = Excl2 k → i0 = j)):
     ∀ i0 k : nat,
      <[i:=Excl (Some t)]> M !! i0 = Excl2 k
      → k < (t + 1)%nat
        ∧ (∀ j : nat, <[i:=Excl (Some t)]> M !! j = Excl2 k → i0 = j).
    Proof.
      move => j k. case (decide (j = i)) => [->|NEq].
      - rewrite lookup_insert. move => [<-].
        split; first lia.
        move => j'. case (decide (j' = i)) => [-> //| NEq].
        rewrite lookup_insert_ne; last done.
        move => /FA [tLe _]. lia.
      - rewrite lookup_insert_ne; last done.
        move => Eqk. destruct (FA _ _ Eqk) as (kLe & kInj).
        split; first lia.
        move => j'. case (decide (j' = i)) => [->| NEq2].
        + rewrite lookup_insert. move => [?]. subst k. lia.
        + rewrite lookup_insert_ne; [apply kInj|done].
    Qed.

    Lemma ns_tkt_bound_update (M: gmapNOpN) (t n i: nat) (ot : option nat)
      (Ini: M !! i = Excl' ot) (BO: ns_tkt_bound M t n)
      (DS: dom (gset nat) M ≡ seq_set 0 (Pos.to_nat C)):
      ∃ (n': nat),
        (ot = None → n' = n) ∧
        (∀ t0, ot = Some t0 → n' = S t0 ∨ n' = n) ∧ t < n' + Z.pos C
        ∧ ns_tkt_bound (<[i:= Excl (Some t)]> M) (t+1) n'.
    Proof.
      destruct BO as (LE1 & LE2 & FA).
      destruct ot as [t0|].
      - destruct (FA _ _ Ini) as (iLe & Inj).
        case (decide (n ≤ t0)) => [Le|/Z.nle_gt Gt].
        + exists (S t0). split; first done.
          split; first (move => ? [?]; subst; by left).
          split; [|split; first lia; split].
          * eapply Z.le_lt_trans; first by apply LE2.
            apply Z.add_lt_le_mono; [lia|]. by apply Waiting_size_le.
          * rewrite Nat2Z.inj_succ Nat2Z.inj_add Zplus_assoc_reverse 
                    (Z.add_comm 1) Zplus_assoc.
            apply Zplus_le_compat_r. etrans; first apply LE2.
            assert (Eqs :=
            subseteq_union_1 _ _ (Waiting_subseteq_update _ _ t _ _ Ini Le)).
            rewrite /Ws -(collection_size_proper _ _ Eqs) size_union_alt.
            rewrite Nat2Z.inj_add (Z.add_comm (size _)) Zplus_assoc.
            apply Zplus_le_compat_r. etrans.
            { instantiate (1:= n + size (Mtkt_range M n t0)).
              apply Zplus_le_compat_l, Nat2Z.inj_le, subseteq_size.
              apply Waiting_diff; auto. }
            { apply Waiting_bound; [naive_solver|done]. }
          * by apply ns_inj_insert.
        + exists n. split; first done.
          split; first by right.
          split; [|split; first lia; split].
          * eapply Z.le_lt_trans; first by apply LE2. apply Zplus_lt_compat_l.
            apply (Waiting_size_lt _ n i (Some t0) DS);
              [done|right;by eexists].
          * etrans; last first.
            { instantiate (1:= n + size ({[i]} ∪ Waiting M n)).
              apply Zplus_le_compat_l.
              assert (Hle := subseteq_size _ _
                      (Waiting_ns_nonupdate M n t _ _ Ini LE1)).
              by apply inj_le in Hle. }
            { rewrite size_union ; last first.
              { rewrite disjoint_singleton_l /Waiting elem_of_filter.
                rewrite Ini. move => [[? [[<-] ?]] _]. omega. }
              rewrite Nat2Z.inj_add size_singleton Nat2Z.inj_add
                      (Z.add_comm 1) Zplus_assoc. 
              by apply Zplus_le_compat_r. }
          * by apply ns_inj_insert.
      - exists n. split; [done|split]; first by right.
        split; [|split; first lia; split].
        * eapply Z.le_lt_trans; first by apply LE2. apply Zplus_lt_compat_l.
          apply (Waiting_size_lt _ n i None DS); [done|by left].
        * etrans; last first.
          { instantiate (1:= n + size ({[i]} ∪ Waiting M n)).
            apply Zplus_le_compat_l.
            assert (Hle := subseteq_size _ _
                    (Waiting_ns_nonupdate M n t _ _ Ini LE1)).
            by apply inj_le in Hle. }
          { rewrite size_union ; last first.
            { rewrite disjoint_singleton_l /Waiting elem_of_filter.
              rewrite Ini. move => [[? [//]]]. }
            rewrite Nat2Z.inj_add size_singleton Nat2Z.inj_add
                    (Z.add_comm 1) Zplus_assoc. 
            by apply Zplus_le_compat_r. }
        * by apply ns_inj_insert.
    Qed.

  End Arith.

  Section Interpretation.
    Context `{fG : foundationG Σ}
            `{aG: absViewG Σ, tickG: !tklG Σ}
            `{gpG: !gpsG Σ natProtocol _}
            `{persG : persistorG Σ}.

    Set Default Proof Using "Type".
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).

    Definition Perm (γ: gname) (t: nat) : iProp
      := own γ (CoPset {[Pos.of_succ_nat t]} , ∅).

    Definition Perms (γ: gname) (t: nat)
      := own γ (CoPset $ coPset_from_ex t, ∅).

    Lemma Perm_exclusive γ t: Perm γ t ∗ Perm γ t ⊢ False.
    Proof.
      iIntros "Perms". rewrite -own_op pair_op.
      iDestruct (own_valid with "Perms") as %[Valid _]. exfalso.
      simpl in Valid.
      rewrite -> coPset_disj_valid_op, disjoint_singleton_l in Valid.
      by apply Valid, elem_of_singleton.
    Qed.

    Lemma Perms_get γ t :
      Perms γ t ⊢ Perm γ t ∗ Perms γ (S t).
    Proof.
      iIntros "Perms". rewrite -own_op pair_op.
      rewrite !/op /cmra_op /=.
      case (decide ({[Pos.of_succ_nat t]} ⊥ coPset_from_ex (S t))) => [_|ND];
        last first.
        { exfalso. apply ND, coPset_from_disjoint. }
      iApply (own_mono with "Perms"). apply prod_included.
      by rewrite /= !coPset_disj_included -coPset_from_insert
                 auth_included /= /op /cmra_op /= /ucmra_op /=
                 /gmap_op merge_empty.
    Qed.

    Definition MyTkt γ (i: nat) (t: option nat)
      := own γ (∅, ◯ {[i := Excl t]}).

    Definition AllTkts γ (M: gmapNOpN)
      := own γ (∅, ● M).

    Lemma MyAll_coherent γ i t M:
      AllTkts γ M ∗ MyTkt γ i t ⊢ ⌜M !! i = Excl' t⌝.
    Proof.
      iIntros "Perms". rewrite -own_op pair_op.
      iDestruct (own_valid with "Perms") as %[_ Valid].
      iPureIntro. 
      move: Valid => /= /auth_valid_discrete_2 [/lookup_included /(_ i)].
      rewrite lookup_singleton option_included.
      move => [//|[x [y [-> [Leq [/leibniz_equiv_iff -> //|Incl]]]]]] Valid.
      apply leibniz_equiv_iff in Leq.
      move : (lookup_valid_Some _ i y Valid Leq).
      by move : Incl =>  [z /leibniz_equiv_iff ->].
    Qed.

    Lemma MyTkt_get γ i t t' M:
      AllTkts γ M ∗ MyTkt γ i t
      ==∗ AllTkts γ (<[i := Excl t']> M) ∗ MyTkt γ i t'.
    Proof.
      iIntros "Own".
      iDestruct (MyAll_coherent with "Own") as %Eq.
      rewrite -!own_op !pair_op.
      iMod (own_update with "Own"); last iAssumption.
      apply prod_update => /=; first done.
      apply auth_update.
      apply: singleton_local_update; [eauto| exact: exclusive_local_update].
    Qed.

    Definition map_excl (S: gset nat) (t : option nat) := to_gmap (Excl t) S.
    Definition setC: gset nat := seq_set 0 (Pos.to_nat C).
    Definition firstMap := map_excl setC None.

    Lemma Tkt_set_alloc γ j n t:
       own γ (∅, ◯ map_excl (seq_set j n) t)
       ⊢ ([∗ set] i ∈ seq_set j n, MyTkt γ i t)%I.
    Proof.
      revert j. induction n => j /=; iIntros "MyTkts".
      - by rewrite big_sepS_empty.
      - rewrite big_sepS_insert; last first.
        { move => /elem_of_seq_set [Le _]. omega. }
        rewrite /map_excl to_gmap_union_singleton insert_singleton_op;
          last first.
        { apply lookup_to_gmap_None. move => /elem_of_seq_set [Le _]. omega. }
        iDestruct "MyTkts" as "[$ MyTkts]".
        by iApply IHn.
    Qed.

    Lemma Tkt_ghost_alloc :
      (True ==∗
        ∃ γ, Perms γ 0 ∗ AllTkts γ firstMap ∗ ([∗ set] i ∈ setC, MyTkt γ i None))%I.
    Proof.
      iIntros "_".
      iMod (own_alloc (CoPset $ coPset_from_ex 0, (● firstMap ⋅ ◯ firstMap)))
        as (γ) "Own".
      { split; first done. apply auth_valid_discrete_2. split; first done.
        move => i. destruct (firstMap !! i) eqn:Eq; last by rewrite Eq.
        rewrite Eq. by move :Eq => /lookup_to_gmap_Some [_ <-]. }
      iExists γ. rewrite pair_split.
      iDestruct "Own" as "[$ [$ MyTkts]]". iModIntro.
      rewrite /firstMap /setC. by iApply Tkt_set_alloc.
    Qed.


    Definition ES (P : vPred) F l γ n : vPred
      := □ ∀ (t: nat), ■ (t ≤ n) → [es ☐ Perm γ t ⇝ (P ∗ [XP l in t | F]_W)].
    Global Instance ES_proper : Proper ((≡) ==> (≡) ==> (=) ==> (=) ==> (=) ==> (≡)) ES.
    Proof.
      repeat intro; subst. apply vPred_always_proper, vPred_forall_proper => ?. by rewrite H H0.
    Qed.

    Local Open Scope VP.
    Definition NSP' P γ : _ -> interpC Σ natProtocol :=
        fun F b l n z =>
           if b : bool then True else (z = n `mod` (Z.pos C)) ∗ ES P F l γ n.

    Global Instance NSP'_contractive P γ:
      Contractive (NSP' P γ).
    Proof.
      intros ? ? ? H b ? ? ? ?.
      destruct b; [done|].
      apply uPred.sep_ne; [done|].
      apply uPred.always_ne, uPred.forall_ne =>? /=.
      apply uPred.forall_ne => ?. apply uPred.wand_ne; [done|].
      apply uPred.impl_ne; [done|].
      apply uPred.exist_ne => ?.
      apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply uPred.sep_ne; [done|].
      f_contractive.
      apply uPred.or_ne; [done|].
      apply uPred.sep_ne; [done|].
      apply vGPS_WSP_contractive.
      destruct n; [done|]. eapply dist_le; [done|omega].
    Qed.

    Program Definition NSP P (γ: gname) : interpC Σ natProtocol :=
      [rec NSP' P γ].

    Instance NSP'_persistent P γ F b l n z (V: View):
      PersistentP ((NSP' P γ F b l n z) V).
    Proof. iIntros "NSP". destruct b => /=.
      - iClear "NSP". by iIntros "!#".
      - iDestruct "NSP" as "(#? & #?)". iIntros "!#". by iFrame "#".
    Qed.

    Global Instance NSP_persistent P γ b l n z (V: View) :
      PersistentP (NSP P γ b l n z V).
    Proof.
      unfold PersistentP.
      rewrite (unf_int (NSP P γ)).
      apply (_ : PersistentP _).
    Qed.

    Definition TCP P γ (x: loc) : interpC Σ unitProtocol :=
    λ b _ _ y,
      if b
      then (∃ (t n: nat) (M: gmapNOpN),
            ☐ AllTkts γ M ∗ ☐ Perms γ t
            ∗ ■ (dom (gset nat) M ≡ seq_set 0 (Pos.to_nat C)
                ∧ y = t `mod` Z.pos C ∧ ns_tkt_bound M t n)
            ∗ [XP (ZplusPos ns x) in n | NSP P γ ]_R)
      else True.

  End Interpretation.

  Section proof.
    Context `{fG : foundationG Σ}
            `{aG: !absViewG Σ, tickG: !tklG Σ}
            `{gpnG: !gpsG Σ natProtocol _}
            `{gpuG: !gpsG Σ unitProtocol _}
            `{agreeG : !gps_agreeG Σ unitProtocol}
            `{persG : !persistorG Σ}.

    Set Default Proof Using "Type".
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).
    Context (P : vPred).

    Definition MayAcquire (x: loc): vPred := 
      ∃ γ i ot, ☐ MyTkt γ i ot
        ∗ [PP (ZplusPos tc x) in () | TCP P γ x ]
        ∗ □ ∀ (t: nat), ■ (ot = Some t) → [XP (ZplusPos ns x) in S t | NSP P γ ]_R.

    Definition MayRelease (x: loc): vPred :=
      ∃ γ i t, ☐ MyTkt γ i (Some t)
        ∗ [PP (ZplusPos tc x) in () | TCP P γ x ] 
        ∗ [XP (ZplusPos ns x) in t | NSP P γ ]_W.

    Lemma newLock_spec:
      {{{{ ☐ PersistorCtx ∗ ▷P }}}}
          (newLock #())
      {{{{ (x: loc), RET #x;
          vPred_big_opS setC (λ _, MayAcquire x) }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS (#kP & P) Post".
      wp_seq. iNext. wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].

      iNext. iViewUp. iIntros (x) "kS oLs".
      rewrite -vPred_big_opL_fold big_sepL_cons big_opL_singleton.
      iDestruct "oLs" as "(oNS & oTC)".

      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.

      iMod (Tkt_ghost_alloc with "[]")
        as (γ) "(Perms & AllTkts & MyTkts)"; first done.

      iApply (GPS_SW_Init (NSP P γ) (0)%nat ([XP ZplusPos 0 x in 0%nat | NSP P γ]_R)
               with "[%] kI kS [P kP $oNS]");
        [done|done|done| |].
      { iSplitL ""; first done. iViewUp. iIntros "oW".
        iDestruct (GPS_SW_Writer_Reader (IP := NSP P γ) _ V_l with "oW")
          as "[oW $]"; [done|].
        iApply fupd_mask_mono;
        last iMod (escrow_alloc (☐ Perm γ 0) 
                                (P ∗ [XP ZplusPos 0 x in O |NSP P γ ]_W) V_l
                     with "[$P $oW]") as "#?";
        [solve_ndisj|done|].
        iModIntro. iSplitL "".
        - iNext. by rewrite !(unf_int (NSP P γ)) /=.
        - iNext. rewrite !(unf_int (NSP P γ)) /=.
          rewrite Zmod_0_l. iSplitL ""; first done.
          iIntros "!#". iIntros (?). iViewUp. iIntros (?).
          rewrite (_ : a = 0%nat); [done|lia].
      }

      iNext. iViewUp.

      iIntros "kS NSR".

      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.

      iApply (GPS_PP_Init_default (TCP P γ x) () 
                with "[%] kI kS [$kP $oTC Perms AllTkts NSR]");
                [done|done|done| |].
      { iNext. iSplitR ""; last done.
        iExists O, O, firstMap. iFrame "AllTkts Perms".
        iSplitR "NSR".
        - iPureIntro. split; last split.
          + rewrite /firstMap /map_excl. apply set_unfold_2. move => i.
            rewrite elem_of_dom. split.
            * move => [e /lookup_to_gmap_Some [//]].
            * move => In. exists (Excl None). by apply lookup_to_gmap_Some.
          + by rewrite Zmod_0_l.
          + split; [done|split]; first omega.
            move => ? ? /lookup_to_gmap_Some [_ //].
        - iDestruct "NSR" as (?) "(S1 & S2 & _ & S3)".
          iExists _. iFrame. }

      iNext. iViewUp. iIntros "kS #TCP". wp_seq. iNext. wp_value.

      iApply ("Post"  with "[%] kS [MyTkts]"); first done.
      rewrite /vPred_big_opS {4}/vPred_app /=.

      iApply (big_sepS_impl _ _ setC with "[$MyTkts]").
      iIntros "!#". iIntros (i) "_ Tkt".
      iExists γ, i, None. iFrame "Tkt TCP".
      iIntros "!#". by iIntros (? ?) "_ %".
    Qed.

    Lemma lock_spec x:
      {{{{ MayAcquire x }}}}
        lock #x
      {{{{ RET #true; ▷ P ∗ MayRelease x }}}}.
    Proof.
      intros; iViewUp; iIntros "#kI kS MAcq Post".
      iDestruct "MAcq" as (γ i ot) "(Tkt & TC & NS)".
      iApply wp_fupd.
      wp_lam. iNext. wp_bind (FAI _ _)%E. wp_op. iNext.

      set P' : vPred := (☐ MyTkt γ i ot)%VP.
      set Q' : pr_state unitProtocol → Z → vPred 
        := (λ _ v, ∃ (t n0: nat), ■ (v = t `mod` Z.pos C ∧ t < n0 + Z.pos C)
                     ∗ ☐ MyTkt γ i (Some t) ∗ ☐ Perm γ t
                     ∗ ▷ [XP x in n0 | NSP P γ ]_R)%VP.
      iApply (GPS_PP_FAI (p := ()) (TCP P γ x) (s:=())
              P' Q'  with "[%] kI kS [Tkt TC NS]");
              [auto|auto|reflexivity|..].
      { iSplitR "Tkt TC"; last by iFrame "TC". iDestruct "NS" as "#NS".
        iNext. iIntros (v s'). iViewUp. iIntros "(_ & TC1 & Tkt)".
        iExists (). iSplitL ""; first done.
        iDestruct "TC1" as (t n0 M) "(All & Perms & % & #NSR)".
        iDestruct (MyAll_coherent with "[$Tkt $All]") as %Eq.
        destruct H as (EqD & Eqv & tB).
        destruct (ns_tkt_bound_update _ _ _ _ _ Eq tB EqD)
          as (n' & nEq1 & nEq2 & nLt & tB').
        iMod (MyTkt_get _ _ _ (Some t) with "[$All $Tkt]") as "(All' & Tkt')".
        iDestruct (Perms_get with "Perms") as "(Perm & Perms)".
        iSplitL "Perm Tkt'".
        - iExists t, n'. iFrame "Perm Tkt'". iSplitL ""; first done.
          iModIntro. iNext.
          destruct ot as [t0|]; first (move: (nEq2 t0) => [//|->|-> //]).
          + by iApply ("NS" $! t0 with "[%]").
          + by rewrite nEq1. 
        - iModIntro. iNext. iSplitR ""; last done.
          iExists (S t), n', (<[i := Excl (Some t)]> M).
          iFrame "All' Perms". iSplitR "NSR".
          + iPureIntro. split; last split.
            * rewrite -EqD. split; last apply dom_insert_subseteq.
              rewrite dom_insert_L elem_of_union.
              move => [/elem_of_singleton ?|//]. subst.
              apply elem_of_dom. by eexists.
            * rewrite Nat2Z.inj_succ Eqv. rewrite Zplus_mod_idemp_l. lia.
            * by rewrite -Nat.add_1_r.
          + destruct ot as [t0|]; first (move: (nEq2 t0) => [//|->|-> //]).
            * by iApply ("NS" $! t0 with "[%]").
            * by rewrite nEq1. }

      iNext. iViewUp. iIntros (s y) "kS (_ & #TCP & Q')".
      iDestruct "Q'" as (t n0) "(% & Tkt' & Perm & #NS)".
      destruct H as [yEq tLt].
      wp_value. wp_lam. iNext.

      iLöb as "IH" forall (V_l) "NS TCP ∗".
      iApply wp_repeat; first auto. wp_bind ([_]_at)%E. wp_op. iNext.

      set Q2 : pr_state natProtocol → Z → vPred 
        := (λ n z, ■ (z = n `mod` Z.pos C) ∗ ES P (NSP P γ) x γ n)%VP.

      iApply (GPS_SW_Read (NSP P γ) True Q2 with "[%] kI kS [$NS]");
        [auto|auto|reflexivity|..].
      { iSplitL "".
        - iNext. iIntros (s'). iViewUp. iIntros "%".
          iLeft. iIntros (z). iViewUp. iIntros "(NSP & _)".
          rewrite !(unf_int (NSP P γ)).
          by iDestruct "NSP" as "($ & $)".
        - iNext. iIntros (? s' v). iViewUp.
          iIntros "(? & #NSP)". by iSplitL "". }

      iNext. iViewUp. iIntros (n z) "kS (% & #NSR & Q2)".
      iDestruct "Q2" as "(% & #ES)".
      wp_lam. iNext. wp_op => [Eq|NEq].
      - iExists 1. iNext. iSplit; first auto. 
        case (decide (1 = 0)) => [//|_]. do 2 iNext.
        rewrite yEq H0 in Eq.

        (* big arithmetic and crazy Single-writer application *)
        iAssert (|={⊤}=> (⌜t = n⌝ ∗ (☐ Perm γ t)%VP V_l))%I with "[Perm]"
          as ">(% & Perm)".
        { case (decide (n ≤ t)) => [LE|/Z.nle_gt Gt].
          - iFrame "Perm". iModIntro. iPureIntro.
            assert (G0: Z.pos C > 0) by lia.
            assert (Ht := Z_div_mod t _ G0).
            assert (Hn := Z_div_mod n _ G0).
            rewrite/Z.modulo in Eq.
            destruct (Z.div_eucl t (Z.pos C)) as [qt rt].
            destruct (Z.div_eucl n (Z.pos C)) as [qn rn].
            destruct Ht as (tEq & Le1 & Lt1).
            destruct Hn as (nEq & Le2 & Lt2).
            apply Nat2Z.inj_iff.
            assert (LT: t < n + Z.pos C).
            { eapply Z.lt_le_trans; first apply tLt. apply Zplus_le_compat_r.
              cbn in H. omega. }
            move : LE LT. rewrite tEq nEq Eq.
            move => LE LT. do 2 f_equal.
            apply Z.add_le_mono_r in LE.
            apply Z.mul_le_mono_pos_l in LE; last lia.
            rewrite -Zplus_assoc (Z.add_comm rn) Zplus_assoc in LT.
            apply Z.add_lt_mono_r in LT. rewrite Zmult_succ_r_reverse in LT.
            apply Z.mul_lt_mono_pos_l in LT; last lia. omega.
          - iSpecialize ("ES" $! t V_l with "[] []"); [auto|iPureIntro; omega|].
            iDestruct (escrow_apply (☐ Perm γ t) (P ∗ [XP x in t | NSP P γ]_W)
              with "[]") as "EA".
            { iIntros (??) "_ pTok". iApply (Perm_exclusive with "pTok"). }

            iSpecialize ("EA" with "[] [$ES $Perm]"); first auto.
            iMod (fupd_mask_mono with "EA") as "(P & NSW)"; first auto.
            destruct (GPS_SW_Writer_max (IP := NSP P γ) x n t ⊤) as [WS];
              first done.
            iDestruct (WS V_l V_l with "[$NSR $NSW]") as ">(NSW & %)";
              first done. cbn in H1. exfalso. omega. }

        rewrite H1.
        iSpecialize ("ES" $! n with "[%] [%]"); [reflexivity|omega|].
        iDestruct (escrow_apply (☐ Perm γ n) (P ∗ [XP x in n | NSP P γ]_W)
              with "[]") as "EA".
          { iIntros (??) "_ pTok". iApply (Perm_exclusive with "pTok"). }
        iSpecialize ("EA" with "[] [$ES $Perm]"); first auto.
        iMod (fupd_mask_mono with "EA") as "(P & NSW)"; first auto.
        destruct (GPS_SW_Writer_max (IP := NSP P γ) x n n ⊤) as [WS];
          first done.
        iDestruct (WS V_l V_l with "[$NSR $NSW]") as ">(NSW & _)";
          first done.
        iApply ("Post" with "[%] kS [$P Tkt' NSW]"); [done|].
        iExists γ, i, n. destruct s. iFrame "Tkt' NSW TCP".

      - iExists 0. iNext. iSplit; first auto.
        case (decide (0 = 0)) => [_|//]. do 2 iNext.
        iApply ("IH" $! _ with "[] [] Post kS Tkt' Perm").
        + iIntros "!#". iFrame "NS".
        + iIntros "!#". iFrame "TCP".
    Qed.

    Lemma unlock_spec x :
      {{{{ ▷ P ∗ MayRelease x }}}}
        unlock #x
      {{{{ RET #(); MayAcquire x }}}}.
    Proof.
      intros; iViewUp; iIntros "#kI kS (P & MRel) Post".
      iDestruct "MRel" as (γ i t) "(Tkt & TC & NSW)".

      wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iApply (GPS_SW_Read_ex with "[%] kI kS [$NSW]"); [done|done|done|..].
      { iNext. iIntros (? ?). iViewUp. iIntros "#?". iModIntro. by iSplitL. }

      iNext. iViewUp. iIntros (z) "kS (NSW & NSP)".
      rewrite (unf_int (NSP P γ)).
      iDestruct "NSP" as "(% & #ES)".

      wp_seq. iNext. wp_op. iNext. wp_op. iNext. wp_op. iNext.

      rewrite (_ : (z + 1) `mod` Z.pos C = (t + 1) `mod` Z.pos C); last first.
      { rewrite H. by rewrite Z.add_mod_idemp_l; last lia. }

      iApply (GPS_SW_ExWrite (NSP P γ) (S t) True (λ _, True)%VP
        with "[%] kI kS [P $NSW]");
        [cbn; omega|auto|auto|reflexivity|..].
      { iNext. iIntros (y). iViewUp.
        iIntros "(_ & _ & NSW)".
        iSplitL ""; first auto.
        iSplitL ""; [iModIntro; iNext|].
        { by rewrite (unf_int (NSP P γ)). }
        destruct (escrow_alloc (☐ Perm γ (S t))
                               (P ∗ [XP x in (S t) | NSP P γ]_W)) as [EA].
        iDestruct (EA with "[$P $NSW]") as "CEi"; [done|].
        iMod (fupd_mask_mono with "CEi") as "#CEi"; first solve_ndisj.
        iModIntro. iNext.
        rewrite (unf_int (NSP P γ)).
        iSplitL "".
        - iPureIntro. lia.
        - iIntros "!#". iIntros (t0). iViewUp. iIntros (Le).
          apply Nat2Z.inj_le, Nat.le_succ_r in Le as [Le|EqS].
          + iApply ("ES" with "[] []"); [done|iPureIntro;omega].
          + rewrite EqS. done. }

      iNext. iViewUp. iIntros "kS (#NS & _)".
      iApply ("Post"  with "[%] kS  [NS TC Tkt]"); first done.
      iExists γ, i, (Some $ t). iFrame "Tkt TC".
      iIntros "!#". iIntros (?). iViewUp. iIntros "%". by inversion H0.
    Qed.
  End proof.

End TicketLock.