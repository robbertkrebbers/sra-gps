From iris.program_logic Require Export weakestpre.
From iris.base_logic.lib Require Import cancelable_invariants.
From iris.proofmode Require Import tactics.

From igps Require Export notation malloc repeat wp_tactics viewpred rsl_instances.
From igps Require Import rsl persistor proofmode weakestpre.
From igps.base Require Import base.
Import uPred.

(** Basic spin-lock proven with iRSL *)

Definition unlocked : base.val := #0.
Definition locked : base.val := #1.

Definition newLock : base.val  :=
  λ: <>, let: "l" := Alloc in ["l"]_na <- unlocked ;; "l".

Definition unlock : base.val :=
  λ: "l", ["l"]_at <- unlocked.

Definition lock : base.val :=
  λ: "l", repeat (CAS "l" unlocked locked).


Section proof.
  Context `{fG : !foundationG Σ, 
            rG : rslG Σ, perG : persistorG Σ}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Local Open Scope VP.
  Notation Q_dup := (λ v, (v = 1) ∨ (v = 0)).
  Notation Q J := (λ v, (v = 1) ∨ (v = 0 ∧ J)).
  Instance Frame_Into_Q (J : vPred) v : ∀ V, Frame false (J V) (Q J v V) (Q_dup v V)%V.
  Proof. iIntros (V) "[oJ [%|%]]"; [by iLeft|iRight]. by iFrame "oJ". Qed.

  Definition isLock (l : loc) J : vPred :=
    ((Rel l (Q J) Q_dup)
    ∗ (RMWAcq l (Q J) Q_dup)
    ∗ (Init l)
    ).

  (* TODO: make this quicker *)
  Instance isLock_persistent l J V: PersistentP (isLock l J V) := _.
  Local Close Scope VP.

  Lemma newLock_spec (P : vPred) :
    {{{{ ☐ PersistorCtx ∗ ▷ P }}}}
        (newLock #())
    {{{{ (l: loc), RET (#l);  (isLock l P) }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS [#kP P] Post".
    wp_seq. iNext. wp_bind Alloc.

    iApply (alloc with "[%] kI kS [%]"); [done|done|done|].
    iNext. iViewUp; iIntros (l) "kS oL".
    wp_seq. iNext. wp_bind ([_]_na <- _)%E.
    iApply (RMWAcq_init l 0 (Q P) Q_dup 
            with "[%] kI kS [$kP $oL P]"); [auto|auto|reflexivity|..].
    { iNext; iSplit. by iRight; iFrame. by iRight. }
    iNext. iViewUp. iIntros "kS (Rel & RMW & Init)".
    wp_seq. iNext. wp_value.
    by iApply ("Post" with "[] kS [$Rel $RMW $Init]").
  Qed.

  Lemma unlock_spec (l: loc) P:
    {{{{ isLock l P ∗ ▷ P }}}}
        (unlock #l)
    {{{{ RET #();  True }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS ((Rel & Acq & Init) & P) Post".
    wp_let. iNext.
    iApply (Rel_write l (Q P) Q_dup with "[%] kI kS [$Rel $P]");
      [auto|auto|reflexivity|..].
    { iNext; iSplit ; by iRight. }
    iNext.  iViewUp. iIntros "kS' (Rel' & Init')".
    by iApply ("Post" with "[] kS'").
  Qed.

  Lemma lock_spec (l: loc) P:
    {{{{ isLock l P }}}}
        (lock #l)
    {{{{ RET #true; ▷ P }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS (Rel & Acq & Init) Post".
    wp_let. iNext. (iLöb as "IH" forall (V_l) "kS ∗").
    iApply wp_repeat; first auto.
    iPoseProof (RMWAcq_CAS l (Q P) (Q_dup) (True)
                  (fun b v => if b then ■ (v = 0) ∧ ▷ P else (■ (v = 1)))%VP
                  0 1
                with "[]") as "T"; [auto|auto|..].
    { iIntros (?) "!#". iIntros (? ?) "_ #?". by iSplit. }

    iApply ("T" $! V_l with "[%] kI kS [$Acq $Init]"); first done.
    { iSplitL "".
      - iIntros (?) "_ (_ & [>%|[>% P]] & _)"; first discriminate.
        iModIntro. iSplitL ""; [iNext;by iLeft|]. iSplitL""; [iNext; by iLeft|].
        by iFrame "P".
      - iIntros (??) "_ %". iIntros (?) "_ (_ & [>%|>%])" ; last by (subst).
        by iModIntro. }

    iNext. iViewUp. iIntros (b v') "kS' (Acq & Init & R)".
    iExists b; iSplit; first auto. destruct b; [|iClear "R"].
    - iNext. iNext.
      iApply ("Post" with "[%] kS' [R]"); [done|by iDestruct "R" as "[? $]"].
    - iNext. iNext.
      iApply ("IH" with "kS' Rel Acq Init Post").
  Qed.

End proof.