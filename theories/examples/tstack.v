From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.

From igps Require Import notation malloc repeat protocols.
From igps.gps Require Import shared plain.

Import uPred.

(** Basic Treiber Stack in Release-Acquire **)

Section TreiberStack.

  Local Notation next := 0.
  Local Notation data := 1.

  Definition newStack : base.val :=
    λ: <>,
      let: "s" := Alloc in
      ["s"]_na <- #0  ;; "s".

  Definition tryPush : base.val :=
    λ: "s" "x",
      let: "n" := malloc (#2) in
      ["n" + #data]_na <- "x" ;;
      let: "h" := ["s"]_at in
      ["n" + #next]_na <- "h";;
      CAS "s" "h" ((int)"n").

  Definition tryPop : base.val :=
    λ: "s", 
      let: "h" := ["s"]_at in
      if: "h" = #0
      then #0
      else
        let: "n" := [((loc)"h") + #next]_na in
        if: (CAS "s" "h" "n")
        then [((loc)"h") + #data]_na
        else #0.

  Section Interpretation.
    Context `{fG : foundationG Σ}
            `{gpG: !gpsG Σ unitProtocol _}
            `{persG : persistorG Σ}
            `{cG : cinvG Σ}.
    Set Default Proof Using "Type".
    Notation vPred := (@vPred Σ).

    Local Open Scope VP.

    Definition Reachable' P: (Z -c> list Z -c> @vPred_ofe Σ) 
                             -c> Z -c> list Z -c> @vPred_ofe Σ
    := (fun F n A =>
        match A with
        | nil => n = 0
        | v::A' => ■ (0 < n) ∗ Z.to_pos (n + data) ↦ v ∗ P v ∗
                    ∃ q n', Z.to_pos (n + next) ↦{ q } n' ∗ ▷ F n' A' 
        end).

    Instance Reachable'_inhabited: Inhabited (Z -c> list Z -c> @vPred_ofe Σ)
      := populate (λ _ _, True%VP).

    Instance Reachable'_contractive P:
        Contractive (Reachable' P).
    Proof.
      intros ? ? ? H ? A ?.
      destruct A as [|v A]; [done|].
      repeat (apply uPred.sep_ne; [done|]).
      repeat (apply uPred.exist_ne => ?).
      apply uPred.sep_ne; [done|].
      apply later_contractive. destruct n => //. by apply (H).
    Qed.

    Definition Reachable P := fixpoint (Reachable' P).

    Definition ReachableD': (Z -c> list Z -c> @vPred_ofe Σ) 
                             -c> Z -c> list Z -c> @vPred_ofe Σ
    := (fun F n A =>
        match A with
        | nil => n = 0
        | n'::A' => ■ (0 < n) ∗ ∃ q, Z.to_pos (n + next) ↦{ q } n' ∗ ▷ F n' A' 
        end).

    Instance ReachableD'_contractive:
        Contractive (ReachableD').
    Proof.
      intros ? ? ? H ? A ?.
      destruct A as [|v A]; [done|].
      repeat (apply uPred.sep_ne; [done|]).
      apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply later_contractive. destruct n => //. by apply (H).
    Qed.

    Definition ReachableD := fixpoint (ReachableD').

    Definition Head P : interpC Σ unitProtocol
      := λ b _ _ n, if b then ∃ A, Reachable P n A else ∃ A, ReachableD n A.
    Close Scope VP.

    Lemma Head_dup P l s n V:
      Head P false l s n V -∗ Head P false l s n V ∗ Head P false l s n V.
    Proof.
      iIntros "Head".
      iDestruct "Head" as (A) "Head".
      rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
      revert n. induction A as [|n' A'] => n.
      - iDestruct "Head" as "#Head".
        iSplitL; iExists []; by rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
      - iDestruct "Head" as "(% & Head)".
        iDestruct "Head" as (q) "(next & Head)".
        iAssert (▷ ((Head P false l s n') V ∗ (Head P false l s n') V))%I
          with "[Head]" as "(Hn1 & Hn2)".
        { iNext. rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
          by iApply (IHA' n' with "[Head]"). }
        destruct (na_frac_split_2 (Z.to_pos (n + 0)) n' q) as [NA].
        iDestruct (NA with "next") as "[NA1 NA2]"; first done.
        iSplitL "Hn1 NA1";
          [iDestruct "Hn1" as (A1) "Hn1"; iExists (n' :: A1)
           | iDestruct "Hn2" as (A2) "Hn2"; iExists (n' :: A2)];
           rewrite /ReachableD !(fixpoint_unfold (ReachableD') _ _ _ );
           (iSplitL ""; first done); iExists (q/2)%Qp;
           [iFrame "NA1"| iFrame "NA2"];
           iNext; by rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
    Qed.

    Lemma Head_dup_main P l s n V:
      Head P true l s n V -∗ Head P true l s n V ∗ Head P false l s n V.
    Proof.
      iIntros "Head".
      iDestruct "Head" as (A) "Head".
      rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
      revert n. induction A as [|n' A'] => n.
      - iDestruct "Head" as "#Head".
        iSplitL; iExists [].
        + by rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
        + by rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
      - iDestruct "Head" as "(% & od & P & Head)".
        iDestruct "Head" as (q h') "(next & Head)".
        iAssert (▷ ((Head P true l s h') V ∗ (Head P false l s h') V))%I
          with "[Head]" as "(Hn1 & Hn2)".
        { iNext. rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
          by iApply (IHA' h' with "[Head]"). }
        destruct (na_frac_split_2 (Z.to_pos (n + 0)) h' q) as [NA].
        iDestruct (NA with "next") as "[NA1 NA2]"; first done.
        iSplitL "Hn1 NA1 od P".
        + iDestruct "Hn1" as (A1) "Hn1"; iExists (n' :: A1).
          rewrite !(fixpoint_unfold (Reachable' P) _ _ _ ).
          iSplitL ""; first done. iFrame "od P".
          iExists _, _. iFrame "NA1".
          iNext. by rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
        + iDestruct "Hn2" as (A2) "Hn2"; iExists (h' :: A2).
          rewrite !(fixpoint_unfold (ReachableD') _ _ _ ).
          iSplitL ""; first done.
          iExists _. iFrame "NA2".
          iNext; by rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
    Qed.
  End Interpretation.

  Section proof.
    Context `{fG : foundationG Σ}
            `{gpG: !gpsG Σ unitProtocol _}
            `{persG : persistorG Σ}
            `{agreeG : !gps_agreeG Σ unitProtocol}
            `{cG : cinvG Σ}.
    Set Default Proof Using "Type".
    Notation vPred := (@vPred Σ).

    Context (P : Z → vPred).

    Definition Stack s := [PP s in () | Head P].

    Lemma newStack_spec:
      {{{{ ☐ PersistorCtx }}}}
          (newStack #())
      {{{{ (s: loc), RET #s; Stack s }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS #kP Post".
      wp_lam. iNext. wp_bind Alloc.
      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (s) "kS os".
      wp_seq. iNext. wp_bind ([_]_na <- _)%E.
      iApply (GPS_PP_Init_default (Head P) () with "[%] kI kS [$kP $os]");
        [done|done|done|..].
      { iNext; iSplitL; iExists [].
        - by rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
        - by rewrite (fixpoint_unfold (ReachableD') _ _ _ ). }
      iNext. iViewUp. iIntros "kS Head".
      wp_seq. iNext. wp_value.
      by iApply ("Post" with "[%] kS Head").
    Qed.

    Lemma tryPush_spec s x:
      {{{{ Stack s ∗ P x}}}}
        tryPush #s #x
      {{{{ (z: Z), RET #z; ■ (z ≠ 0) ∨ P x }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS [Stack P] Post".
      wp_lam. iNext. wp_value. wp_let. iNext.
      wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].
      iNext. iViewUp. iIntros (n) "kS oLs".
      rewrite -bigop.vPred_big_opL_fold
              big_op.big_sepL_cons big_op.big_opL_singleton.
      iDestruct "oLs" as "(ol & od)".
      wp_let. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write with "[%] kI kS od"); [done|done|].
      iNext. iViewUp. iIntros "kS od".
      wp_seq. iNext. wp_bind ([_]_at)%E.
      iApply (GPS_PP_Read (p := ()) (Head P) True (λ _ _, True)%VP
          with "[%] kI kS [$Stack]");
        [done|done|done|..].
      { iNext. iSplitL.
        - iIntros (??) "_ _". iLeft. by iIntros (??) "_ _".
        - iIntros (?[]??) "_ (_ & Head)". by iApply Head_dup. }

      iNext. iViewUp. iIntros ([] h) "kS (_ & Head & _)".
      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write_frac_1 with "[%] kI kS ol"); [auto|done|].
      iNext. iViewUp. iIntros "kS ol".
      wp_seq. iNext. wp_op. iNext.

      set P' : vPred := (P x ∗ ZplusPos 1 n ↦ x ∗ ZplusPos 0 n ↦{1} h)%VP.
      iApply (GPS_PP_CAS (p := ()) (Head P) P' (λ _, True)%VP (λ _ _, P')
                with "[%] kI kS [P od ol $Head]"); [done|done|done|..].
      { iSplitL ""; last iSplitL ""; last iSplitL "".
        - iNext. iIntros ([]?) "_ (_ & Head & P & od & ol)".
          iExists (). do 2 (iSplitL ""; first done).
          iModIntro. iNext. iApply Head_dup_main.
          iDestruct "Head" as (A) "Head".
          iExists (x :: A).
          rewrite !(fixpoint_unfold (Reachable' P) _ _ _ ).
          iSplitL ""; first (iPureIntro; lia). iFrame "P".
          iSplitL "od".
          + rewrite (_: Z.to_pos (Z.pos n + 1) = ZplusPos 1 n); first done.
            rewrite /ZplusPos. f_equal. omega.
          + iExists _, _. iFrame "ol". iNext.
            by rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
        - iNext. by iIntros (???) "_ (_ & _ & _ & ?)".
        - iNext. iIntros (?[]??) "_ (_ & Head)". by iApply Head_dup.
        - by iFrame. }

      iNext. iViewUp. iIntros ([] [] _) "kS (_ & Head & IF)".
      - iApply ("Post" with "[%] kS []"); [done|by iLeft].
      - iDestruct "IF" as "[P ?]".
        iApply ("Post" with "[%] kS [P]"); [done|by iRight].
    Qed.

    Lemma tryPop_spec s:
      {{{{ Stack s }}}}
        tryPop #s
      {{{{ (x: Z), RET #x; ■ (x = 0) ∨ P x }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS Stack Post".
      wp_lam. iNext. wp_bind ([_]_at)%E.

      set Q : pr_state unitProtocol → Z → vPred :=
        λ _ h, (■ (h ≠ 0) →
                ■ (0 < h) ∗ ∃ q n, (Z.to_pos (h + next)) ↦{q} n)%VP.
      iApply (GPS_PP_Read (p := ()) (Head P) True Q with "[%] kI kS [$Stack]");
        [done|done|done|..].
      { iNext. iSplitL.
        - iIntros (??) "_ _". iLeft. iIntros (h ?) "_ [Head _]".
          iDestruct "Head" as (A) "Head".
          iModIntro. iViewUp. iIntros "%".
          rewrite (fixpoint_unfold (ReachableD') _ _ _ ).
          destruct A as [|h' A'].
          + by iDestruct "Head" as "%".
          + iDestruct "Head" as "[$ Head]". 
            iDestruct "Head" as (q) "[Head _]". by iExists _, _.
        - iIntros (?[]??) "_ (_ & Head)". by iApply Head_dup. }

      iNext. iViewUp. iIntros ([] h) "kS (_ & Head & Q)".
      wp_seq. iNext. wp_op => [Eqr|NEqr]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%] kS []"); [done|by iLeft].
      - iApply wp_if_false. iNext. wp_bind ([_]_na)%E.
        wp_op. iNext. wp_op.
        iDestruct ("Q" with "[%] [%]") as "[% oh]"; [done|done|].
        iDestruct "oh" as (q n) "oh".
        iNext.
        iApply (na_read_frac with "[%] kI kS [oh]"); [done|done|done|..].
        { rewrite (_: ZplusPos 0 (Z.to_pos h) = Z.to_pos (h + 0)); first done.
          rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|done]. }
        iNext. iViewUp. iIntros (z) "kS (% & oh)". subst z.

        wp_let. iNext. wp_bind (CAS _ _ _).

        set P' : vPred := (ZplusPos 0 (Z.to_pos h) ↦{q} n)%VP.
        set Q': pr_state unitProtocol → vPred :=
          λ _, (∃ v, (ZplusPos data (Z.to_pos h)) ↦ v ∗ P v)%VP.
        iApply (GPS_PP_CAS (p := ()) (Head P) P' Q' (λ _ _, True)%VP
                with "[%] kI kS [$oh $Head]"); [done|done|done|..].
        { iNext. iSplitL; last iSplitL.
          - iIntros ([]?) "_ (_ & Head & oh)".
            iExists (). iSplitL ""; first done.
            iDestruct "Head" as (A) "Head".
            rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
            destruct A as [|v A'].
            + by iDestruct "Head" as "%".
            + iDestruct "Head" as "(_ & od & P & Head)".
              iDestruct "Head" as (q' n') "[oh' Head]".
              rewrite /P'.
              destruct (na_frac_agree (Z.to_pos (h + 0)) n' n q' q) as [EQ].
              iDestruct (EQ with "[$oh' oh]") as "%"; first done.
              { rewrite (_: ZplusPos 0 (Z.to_pos h) = Z.to_pos (h + 0));
                  first done.
                rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|done]. }
              subst n'. iModIntro.
              iSplitL "od P".
              * iExists _. iFrame "P".
                rewrite (_: ZplusPos 1 (Z.to_pos h) = Z.to_pos (h + 1));
                  first done.
                rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|done].
              * iNext. iApply Head_dup_main.
                iExists A'.
                by rewrite (fixpoint_unfold (Reachable' P) _ _ _ ).
          - by iIntros (???) "_ _".
          - iIntros (?[]??) "_ (_ & Head)". by iApply Head_dup. }

        iNext. iViewUp. iIntros ([] [] _) "kS (_ & Head & IF)".
        + iApply wp_if_true. iNext.
          wp_op. iNext. wp_op. iNext.
          iDestruct "IF" as (v) "[od P]".
          iApply (na_read with "[%] kI kS od"); [done|done|].
          iNext. iViewUp. iIntros (z) "kS (% & od)". subst z.
          iApply ("Post" with "[%] kS [P]"); [done|by iRight].
        + iApply wp_if_false. iNext. wp_value.
          iApply ("Post" with "[%] kS []"); [done|by iLeft].
    Qed.

  End proof.
End TreiberStack.