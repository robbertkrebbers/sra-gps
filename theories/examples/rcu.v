From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth coPset.
From iris.base_logic Require Import big_op.

From igps Require Import notation malloc escrows repeat protocols bigop.
From igps.gps Require Import shared plain fractional singlewriter recursive.
From igps.examples Require Import sc_stack nat_tokens rcu_data.

Import uPred.

(** An RCU with fixed number (n) of readers, with batched deallocation **)

(*  This is currently the most substaintial example in the development,
    which is about 4.5kloc.

    The proof is based on the paper
      "Verifying Read-Copy-Update in a Logic for Weak Memory"
      Joseph Tassarotti, Derek Dreyer, and Viktor Vafeiadis.

    There are several differences:
    - The code here has a fixed number of readers
    - Although we have batched deallocation, the language does not have
      a random function to pick when to do actual deallocation.
      The rcuDealloc does batch deallocation, but immediately deallocate
      when the batch has only 1 element. This is a minor issue.
    - The proof is simplified thanks to single-writer protocols.
      List elements are govern by single-writer protocols with fractions
      to be able to be re-collected and deallocated.
    - The logical data structure for the history of the list is also simplified,
      and does not track cycles, since we don't recycle locations.
    - Exchanges used here are less general than those in the original paper.
      Specifically, they are always fixed to a certain view.
*)


Canonical Structure RCULinkProtocol : protocolT
  := ProtocolT (list AbsLoc) suffix.

Instance RCULinkPrt_facts : protocol_facts (RCULinkProtocol).
Proof. esplit; apply _. Qed.

Canonical Structure RCUCounterProtocol : protocolT
  := ProtocolT (RCUData * nat)
               (λ p1 p2, (p1 = p2) ∨ (p1.1 ⊑ p2.1 ∧ (p1.2 < p2.2)%nat)).

Instance RCUCounterPrt_facts : protocol_facts (RCUCounterProtocol).
Proof.
  esplit; try apply _.
  - by left.
  - intros ? ? ? [|[]] [|[]]; subst.
    + by left.
    + by right.
    + by right.
    + right. split; by etrans.
Qed.

Record RCUGhost :=
  mkRCUGhost {
    lhd :> loc;
    history :> gname;
    wxtok :> gname;
    rxtok :> gname;
    txtok :> gname;
  }.

Canonical Structure RCUGhostC := leibnizC RCUGhost.

Definition RCUGhost_to_tuple : RCUGhost → _
  := λ al, match al with mkRCUGhost l h w r t => (l,h,w,r,t) end.
Definition tuple_to_RCUGhost : _ → RCUGhost
  := λ t, match t with (l,h,w,r,t) => mkRCUGhost l h w r t end.

Instance RCUGhost_EqDec : EqDecision RCUGhost :=
  inj_dec_eq RCUGhost_to_tuple (Some ∘ tuple_to_RCUGhost) _.
Proof. by destruct 0. Qed.

Instance RCUGhost_Countable: Countable RCUGhost :=
  inj_countable RCUGhost_to_tuple (Some ∘ tuple_to_RCUGhost) _.
Proof. by destruct 0. Qed.


Section RCU.

  Context (n: positive).
  Local Notation link := 0.
  Local Notation data := 1.
  Local Notation del:= 1.
  Local Notation wc := 2.
  Local Notation rc := 3.
  Local Notation NPosn := (Pos.to_nat n).
  Set Default Proof Using "Type".

  Definition rcuNew : base.val :=
    λ: <>,
    let: "q" := malloc ((int)#n + #3) in
      ["q" + #link]_na <- #0 ;;
      ["q" + #del]_na <- ((int)(newStack #())) ;;
      ["q" + #wc  ]_na <- #0 ;;
      let: "sc" := Alloc in 
      ["sc"]_na <- #0 ;;
      repeat (
        let: "i" := ["sc"]_na in
        ["q" + #rc + "i"]_na <- #0 ;;
        ["sc"]_na <- ("i" + #1) ;;
        "i" + #1 = (int)#n
      ) ;;
      Dealloc "sc" ;;
      "q".

  Definition rcuQuiescentState : base.val :=
    λ: "q" "tid",
    let: "t" := ["q" + #wc]_at in
      ["q" + #rc + "tid"]_at <- "t".

  Definition rcuCollect : base.val :=
    λ: "q" "nr" "ngc",
    let: "sc" := Alloc in
      ["sc"]_na <- #0 ;;
      repeat (
        let: "i" := ["sc"]_na in
        repeat (["q" + #rc + "i"]_at = "ngc") ;;
        ["sc"]_na <- ("i" + #1) ;;
        "i" + #1 = "nr"
      ) ;;
      Dealloc "sc".

  Definition rcuSync : base.val :=
    λ: "q",
    let: "ogc" := ["q" + #wc]_at in
    let: "ngc" := "ogc" + #1 in
    ["q" + #wc]_at <- "ngc" ;;
    rcuCollect "q" #(Zpos n) "ngc".

  Definition rcuNodeAppend : base.val :=
    λ: "q" "p" "v",
    let: "x" := malloc #2 in
    ["x" + #data]_na <- "v" ;;
    ["x" + #link]_na <- #0 ;;
    [(loc)"p" + #link]_at <- ((int)"x") ;;
    (int)"x".

  Definition rcuDealloc : base.val :=
    λ: "q" "x",
    push ((loc)(["q" + #del]_na)) "x" ;;
    let: "c" := #1 in (* choose 0 1 *)
    if: "c" = #0 then #0
    else
      rcuSync "q" ;;
      repeat (
        let: "p" := pop ((loc)(["q" + #del]_na)) in
        if: "p" = #0 then #1
        else
          Dealloc (((loc)"p") + #data) ;;
          Dealloc (((loc)"p") + #link) ;;
          #0
      ).

  Definition rcuNodeUpdate : base.val :=
    λ: "q" "x" "p" "v",
    let: "c" := [(loc)("x" + #link)]_at in
    let: "x'" := malloc #2 in
    ["x'" + #data]_na <- "v" ;;
    ["x'" + #link]_na <- "c" ;;
    [(loc)("p" + #link)]_at <- ((int)"x'") ;;
    rcuDealloc "q" "x";;
    ((int)"x'").

  Definition rcuNodeDelete : base.val :=
    λ: "q" "x" "p",
    let: "c" := [(loc)("x" + #link)]_at in
    [(loc)("p" + #link)]_at <- "c" ;;
    rcuDealloc "q" "x".

  Definition rcuReadStart : base.val :=
    λ: "q", ["q" + #link]_at.

  Definition rcuReadNext : base.val :=
    λ: "q" "np" "rp",
    let: "p" := ["np"]_na in
    if: "p" = #0 then #1
    else
      let: "v" := [(loc)("p" + #data)]_na in
      let: "p'" := [(loc)("p" + #link)]_at in
      ["rp"]_na <- "v" ;;
      ["np"]_na <- "p'" ;;
      #0.

  (* There are n+1 pieces, n for readers, and 1 for writer *)
  Local Notation nf := (1 / (n+1))%Qp.

  Section Interpretation.
    Context `{fG : foundationG Σ}
          `{aG: absViewG Σ}
          `{gLG: !gpsG Σ RCULinkProtocol _}
          `{gCG: !gpsG Σ RCUCounterProtocol _}
          `{CInvG : cinvG Σ}
          `{rG: !rcuG Σ}
          `{agreeG : !gps_agreeG Σ RCULinkProtocol}
          `{persG : persistorG Σ}.

    Local Notation iProp := (@iProp Σ).
    Local Notation vPred := (@vPred Σ).
    Implicit Types (Γ : RCUGhost) (t: nat) (i : aloc) (D: RCUData).

    Context (P : Z → vPred).

    Notation smTok tid S := ({[ tid :=  CoPset $ coPset.of_gset S ]}).

    Definition rTok Γ tid i : vPred := ☐ own (rxtok Γ) (smTok tid {[i]}).
    Definition wTok Γ tid i : vPred := ☐ own (wxtok Γ) (smTok tid {[i]}).
    Definition tTok Γ tid i : vPred := ☐ own (txtok Γ) (smTok tid {[i]}).
    Definition rToks Γ tid (nS: gset aloc) : vPred :=
      ☐ own (rxtok Γ) (smTok tid nS).
    Definition wToks Γ tid (nS: gset aloc) : vPred :=
      ☐ own (wxtok Γ) (smTok tid nS).
     (* Negated set of tokens *)
    Definition rnToks Γ tid (nS: gset aloc) : vPred :=
      ☐ own (rxtok Γ) {[ tid := CoPset (⊤ ∖  coPset.of_gset nS) ]}.
    Definition wnToks Γ tid (nS: gset aloc) : vPred :=
      ☐ own (wxtok Γ) {[ tid := CoPset (⊤ ∖  coPset.of_gset nS) ]}.
    Definition tnToks Γ tid (nS: gset aloc) : vPred :=
      ☐ own (txtok Γ) {[ tid := CoPset (⊤ ∖  coPset.of_gset nS) ]}.


    Lemma rcuTok_own_big_sepS_1 γ t (nS: gset positive) (NE: nS ≠ ∅) :
      own γ ([^op set] i ∈ nS, smTok t {[i]})
      ≡ ([∗ set] i ∈ nS, own γ (smTok t {[i]}))%I.
    Proof. by apply (big_opS_commute1 _). Qed.

    Lemma rcuTok_coPset_split (nS: gset positive):
      CoPset (coPset.of_gset nS)
        = ([^op set] i ∈ nS, CoPset (coPset.of_gset {[i]})).
    Proof.
      move : nS. apply collection_ind.
      - by move => ?? /leibniz_equiv_iff ->.
      - by rewrite coPset_of_gset_empty big_opS_empty.
      - move => x X NIn Sub.
        rewrite big_opS_union; last set_solver+NIn.
        rewrite big_opS_singleton -Sub coPset_of_gset_union -coPset_disj_union;
          first done.
        rewrite elem_of_disjoint.
        move => x'. rewrite 2!coPset.elem_of_of_gset. set_solver+NIn.
    Qed.

    Instance own_cmra_sep_homomorphism t:
      WeakMonoidHomomorphism (op: Op coPset_disjC) (op: Op rcuTokR) (≡) (singletonM t).
    Proof.
      split; [apply _..|]. move => ? ? i. rewrite lookup_op.
      case (decide (t = i)) => [->|NEq].
      - by rewrite !lookup_singleton.
      - by rewrite !lookup_singleton_ne.
    Qed.

    Lemma rcuTok_big_sepS t (nS: gset positive) (NE: nS ≠ ∅):
      smTok t nS = [^op set] i ∈ nS, (smTok t {[i]} : rcuTokR).
    Proof.
      apply leibniz_equiv.
      rewrite (_ : CoPset (coPset.of_gset nS) =
                    [^op set] i ∈ nS, CoPset (coPset.of_gset {[i]})).
      - by eapply (big_opS_commute1 _).
      - by apply rcuTok_coPset_split.
    Qed.

    Lemma rcuTok_own_empty γ t :
      (|==> own γ ({[ t := CoPset ∅ ]} : rcuTokR))%I.
    Proof.
      iMod (own_empty rcuTokR γ) as "oE".
      iApply (own_update with "oE").
      apply (alloc_unit_singleton_update (CoPset ∅)); [done| |done].
      move => [c|] /=; last done. rewrite coPset_disj_union; last set_solver+.
      by rewrite union_empty_l_L.
    Qed.

    Lemma rcuTok_own_big_sepS γ t (nS: gset positive) (NE: nS ≠ ∅) :
      own γ (smTok t nS) ≡ ([∗ set] i ∈ nS, own γ (smTok t {[i]}))%I.
    Proof.
      rewrite rcuTok_big_sepS; [by rewrite rcuTok_own_big_sepS_1|auto].
    Qed.

    Lemma rcuTok_tid_exclusive_1 t i:
      ✓ (smTok t {[i]} ⋅ smTok t {[i]} : rcuTokR) → False.
    Proof.
      move => /(_ t).
      rewrite lookup_op lookup_singleton -Some_op Some_valid
        coPset_disj_valid_op elem_of_disjoint.
      move => H.
      apply (H i); by rewrite coPset.elem_of_of_gset elem_of_singleton.
    Qed.

    Lemma rcuTok_tid_exclusive γ t i:
      own γ (smTok t {[i]}) ∗ own γ (smTok t {[i]}) ⊢ False.
    Proof.
      iIntros "(r1 & r2)". iCombine "r1" "r2" as "r".
      by iDestruct (own_valid with "r") as %Valid%rcuTok_tid_exclusive_1.
    Qed.

    Local Notation tokFullMap m := (to_gmap (CoPset ⊤) (seq_set 0 m)).
    Local Notation fullToks t := ({[ t :=  CoPset ⊤ ]}).

    Lemma Tok_map_alloc_1 (m : nat):
      (|==> ∃ γ, own γ (tokFullMap m))%I.
    Proof.
      apply own_alloc.
      move => k. destruct (tokFullMap m !! k) eqn:Eq; last by rewrite Eq.
      rewrite Eq. by move :Eq => /lookup_to_gmap_Some [_ <-].
    Qed.

    Lemma Tok_map_alloc (m: nat):
      (|==> ∃ γ, [∗ list] t ∈ seq 0 m, own γ (fullToks t))%I.
    Proof.
      iMod (Tok_map_alloc_1 m) as (γ) "Full".
      iModIntro. iExists γ.
      induction m; first by rewrite big_sepL_nil.
      rewrite seq_S seq_set_S_union_L to_gmap_union_singleton
              big_sepL_app big_sepL_singleton insert_singleton_op; last first.
      { rewrite lookup_to_gmap_None elem_of_seq_set. omega. }
      by iDestruct "Full" as "[$ Full]".
    Qed.

    Lemma Tok_negation_split γ t (nS : gset positive) i (In: i ∉ nS):
      own γ {[ t := CoPset (⊤ ∖  coPset.of_gset nS) ]}
      ⊢ own γ {[ t := CoPset (⊤ ∖  coPset.of_gset ({[i]} ∪ nS))]}
        ∗ own γ (smTok t {[i]}).
    Proof.
      rewrite (coPset_of_gset_top_difference nS {[i]}); last set_solver+In.
      rewrite -coPset_disj_union; last by apply coPset_of_gset_top_disjoint.
      rewrite -op_singleton. iIntros "[$ $]".
    Qed.

    Lemma Tok_negation_join γ t (nS : gset positive) i (In: i ∉ nS):
      own γ {[ t := CoPset (⊤ ∖  coPset.of_gset ({[i]} ∪ nS))]}
        ∗ own γ {[ t :=  CoPset $ coPset.of_gset {[i]} ]}
      ⊢ own γ {[ t := CoPset (⊤ ∖  coPset.of_gset nS) ]}.
    Proof.
      rewrite -own_op op_singleton coPset_disj_union;
        last by apply coPset_of_gset_top_disjoint.
      rewrite (coPset_of_gset_top_difference nS {[i]}); [done|set_solver+In].
    Qed.

    Lemma Tok_negation_split_empty γ t (nS : gset positive) i:
      own γ {[ t := CoPset ⊤ ]}
      ⊢ own γ {[ t := CoPset (⊤ ∖  coPset.of_gset ({[i]})) ]}
        ∗ own γ (smTok t {[i]}).
    Proof.
      rewrite -{1}(union_empty_r_L {[i]}).
      iIntros "ow". iApply Tok_negation_split; first done.
      by rewrite coPset_of_gset_empty coPset_difference_top_empty.
    Qed.

    Lemma Tok_split_difference γ t (X Y: gset positive) (Sub: X ⊆ Y):
      own γ {[ t := CoPset (⊤ ∖  coPset.of_gset X) ]}
      ⊢ own γ {[ t := CoPset (⊤ ∖  coPset.of_gset Y) ]}
        ∗ own γ {[ t :=  CoPset $ coPset.of_gset (Y ∖ X) ]}.
    Proof.
      have Hj: (X ⊥ Y ∖ X) by set_solver+.
      rewrite (coPset_of_gset_top_difference _ _ Hj) -coPset_disj_union;
        last apply coPset_of_gset_top_disjoint.
      rewrite (union_comm_L (Y ∖ X)) -union_difference_L; last auto.
      rewrite -op_singleton.
      iIntros "[$ $]".
    Qed.

    Definition snapshot Γ D : vPred
      := ∃ W, ☐ own (history Γ) (◯ DList D W).
    Definition master Γ D : vPred
      := ∃ W, ☐ own (history Γ) (● DList D W).

    Lemma master_dictates_snapshot Γ D1 D2 V :
      snapshot Γ D1 V ∗ master Γ D2 V ⊢ ⌜D1 ⊑ D2⌝.
    Proof.
      iIntros "(SN & MS)".
      iDestruct "SN" as (?) "SN". iDestruct "MS" as (?) "MS".
      iCombine "MS" "SN" as "MS".
      by iDestruct (own_valid with "MS")
        as %[?%dlist_included _]%auth_valid_discrete_2.
    Qed.

    Lemma master_wellformed Γ D V:
      master Γ D V ⊢ ⌜uwellformed D⌝.
    Proof. iIntros "MS". by iDestruct "MS" as (?) "?". Qed.

    Lemma snapshot_wellformed Γ D V:
      snapshot Γ D V  ⊢ ⌜uwellformed D⌝.
    Proof. iIntros "MS". by iDestruct "MS" as (?) "?". Qed.

    Lemma snapshot_decided Γ D1 D2 V :
      snapshot Γ D1 V -∗ snapshot Γ D2 V -∗ ⌜D1 ⊑ D2 ∨ D2 ⊑ D1⌝.
    Proof.
      iIntros "SN1 SN2".
      iDestruct "SN1" as (W1) "SN1".
      iDestruct "SN2" as (W2) "SN2".
      iCombine "SN1" "SN2" as "SN".
      by iDestruct (own_valid with "SN") as %?%dlist_valid_op.
    Qed.

    Global Instance snapshot_persistent' D W γ: PersistentP (own γ (◯ DList D W)).
    Proof. apply own_core_persistent. by rewrite /Persistent. Qed.

    Global Instance snapshot_persistent Γ D V: PersistentP (snapshot Γ D V).
    Proof. apply exist_persistent => ?. apply _. Qed.

    Lemma master_update Γ D D' E (W': uwellformed D') (Le: D ⊑ D') V:
      master Γ D V ={E}=∗ master Γ D' V ∗ snapshot Γ D' V.
    Proof.
      iIntros "MS". iDestruct "MS" as (W) "MS".
      iAssert (|==> own Γ (● DList D' W' ⋅ ◯ DList D' W'))%I 
        with "[MS]" as ">(MS & SN)".
        { iApply (own_update with "MS").
          rewrite auth_both_op.
          by apply auth_update, dlist_local_update. }
      iModIntro. iSplitL "MS"; by iExists _.
    Qed.

    Definition ModX Γ t i : vPred := [es wTok Γ t i ⇝ rTok Γ t i].

    Definition PermX'_right
      (F: _ → _ → gname → interpC Σ RCULinkProtocol) Γ l t i γ: vPred :=
        tTok Γ t i
      ∗ (∃ v, □ P v ∗ (ZplusPos data l) ↦{nf} v)
      ∗ (∃ s, [nFXP (ZplusPos link l) in s @ γ | F Γ i]_R_nf).

    Definition PermX'
      (F: _ → _ → _ → interpC Σ RCULinkProtocol) Γ l t i γ: vPred
      := ∃ β, [ex rTok Γ t i ↭ PermX'_right F Γ l t i γ] β.

    Local Open Scope VP.
    Definition LLP' :
      (RCUGhost -c> aloc -c> gname -c> _)
      → RCUGhost -c> aloc -c> gname -c> interpC Σ RCULinkProtocol :=
      fun F Γ i γ b l L x =>
        if b : bool
        then True
        else
          ∃ D, snapshot Γ D ∗ ■ (hist D i = L ∧ info D i = Some (l, γ))
             ∗ match L with
               | Null :: L' => ■ (x = 0)
               | Abs j :: L' =>
                   ∃ γ, ■ (x > 0 ∧ info D j = Some (Z.to_pos x, γ))
                   ∗ □ ∀ (t: nat), ■ (t < NPosn)%nat
                                   → PermX' F Γ (Z.to_pos x) t j γ
               | _ => False
               end.
    Close Scope VP.

    Global Instance LLP'_contractive:
      Contractive (LLP').
    Proof.
      intros n0 ? ? H ? ? ? b ? L ? ?.
      destruct b; [done|].
      apply exist_ne => ?.
      do 2 (apply sep_ne; [done|]).
      destruct L as [|[| |]]; [done|done| |done].
      apply exist_ne => ?.
      apply sep_ne; [done|].
      apply always_ne.
      do 2 apply forall_ne => ?. apply wand_ne; [done|].
      apply impl_ne; [done|].
      do 2 apply exist_ne => ?.
      do 2 (apply sep_ne; [done|]).
      apply inv_contractive. destruct n0; first done.
      apply or_ne; [done|].
      do 2 (apply sep_ne; [done|]).
      apply exist_ne => ?.
      apply vGPS_nFRP_contractive.
      destruct n0 => //. eapply dist_le; [apply H|omega].
    Qed.

    Definition LLP : ∀ Γ i γ, interpC Σ RCULinkProtocol :=
      [rec LLP'].

    Global Instance LLP'_persistent F Γ i γ b l L x (V: View):
      PersistentP ((LLP' F Γ i γ b l L x) V).
    Proof. destruct b.
      - apply pure_persistent.
      - apply exist_persistent => D.
        apply sep_persistent; first apply snapshot_persistent.
        apply sep_persistent; first apply pure_persistent.
        destruct L as [|[| |]]; try apply pure_persistent.
        apply exist_persistent => ?.
        apply sep_persistent; [apply pure_persistent|apply always_persistent].
    Qed.

    Lemma LLP_unfold Γ i γ b l L x V:
      LLP Γ i γ b l L x V ≡ LLP' (LLP) Γ i γ b l L x V.
    Proof. by rewrite (unf_int LLP). Qed.

    Global Instance LLP_persistent Γ i γ b l L x (V: View) :
      PersistentP (LLP Γ i γ b l L x V).
    Proof.
      unfold PersistentP. rewrite LLP_unfold. apply (_ : PersistentP _).
    Qed.

    Definition PermX Γ p t i γ  := PermX' LLP Γ p t i γ.

    Definition SnapshotValid Γ D : vPred
      := snapshot Γ D
          ∗ ∃ b, ■ (hd_error (basel D) = Some b)
            ∗ (∃ p γ, ■ (info D b = Some (p,γ) ∧ lhd Γ = p)
                    ∗ [nXP (ZplusPos link (lhd Γ)) in (hist D b) @ (Γ,γ) | LLP Γ b]_R)
            ∗ □ (∀ i, ■ (i ∈ live D ∖ {[b]})
                   → ∃ p γ, ■ (info D i = Some (p,γ)) ∗ PrtSeen γ (hist D i)).

    Global Instance SnapshotValid_persistent Γ D V:
      PersistentP (SnapshotValid Γ D V) := _.

    Local Open Scope VP.
    Definition WCP Γ : interpC Σ RCUCounterProtocol := 
      λ b _ Dn x, if b then True
                  else ■ (x = Dn.2) ∗ SnapshotValid Γ (Dn.1).

    Definition RCP Γ t : interpC Σ RCUCounterProtocol := 
      λ b _ Dn x,
        if b then True
             else ■ (x = Dn.2) ∗ [XP (ZplusPos wc (lhd Γ)) in Dn | WCP Γ]_R
                  ∗ □ ∀ i, ■ (i ∈ dead (Dn.1)) → ModX Γ t i.
    Close Scope VP.
    Definition AccessList Γ D t : vPred :=
      SnapshotValid Γ D ∗ rnToks Γ t (dead D).

    Definition RCounterValid Γ D t : vPred :=
      ∃ n n' D', ■ ((D',n') ⊑ (D,n))
                 ∗ [XP (ZplusPos (rc + t) (lhd Γ)) in (D',n') | RCP Γ t]_W
                 ∗ [XP (ZplusPos wc (lhd Γ)) in (D,n) | WCP Γ]_R
                 ∗ □ ∀ i, ■ (i ∈ dead D) → ModX Γ t i.

    Definition ReaderSafe q D t : vPred :=
      ∃ Γ, ■ (q = lhd Γ ∧ (t < NPosn)%nat)
        ∗ AccessList Γ D t ∗ RCounterValid Γ D t.

    Definition PermXList Γ p i γ : vPred :=
      ∀ t, ■ (t < NPosn)%nat → PermX Γ p t i γ.

    Definition SafePtr q D (p: Z) : vPred :=
      ∃ Γ, ■ (q = lhd Γ) ∗ SnapshotValid Γ D
     ∗ (■ (p ≠ 0) → ∃ i γ,
           ■ (p > 0 ∧ i ∉ dead D
                    ∧ (i ∈ dom (gset _) D → info D i = Some (Z.to_pos p,γ)))
           ∗ □ PermXList Γ (Z.to_pos p) i γ).

    Definition RevokedUpTo Γ D : vPred :=
      ∃ m, [XP (ZplusPos wc (lhd Γ)) in (D,m) | WCP Γ]_W
           ∗ vPred_big_opL (seq 0 NPosn)
                (λ _ t, wnToks Γ t (dead D)
                ∗ [XP (ZplusPos (rc + t) (lhd Γ)) in (D,m) | RCP Γ t]_R).

    Local Open Scope VP.
    Definition DeadFrom_elem Γ l i : vPred :=
        ( ∃ v, (ZplusPos data l) ↦{nf} v)
      ∗ ∃ γ, (∃ s, [nFXP (ZplusPos link l) in s @ γ | LLP Γ i]_W_nf)
            ∗ □ PermXList Γ l i γ.

    Definition DeadFrom Γ (L : list (loc * aloc)) D1 D2 : vPred
      := ■ (of_list (snd <$> L) ⊆ dead D2 ∖ dead D1)
         ∗ vPred_big_opL L (λ _ p, DeadFrom_elem Γ (p.1) (p.2)).
                (* [∗ list] p ∈ L, DeadFrom_elem Γ (p.1) (p.2). *)
    Close Scope VP.

    Definition DeallocBetween Γ D1 D2 : vPred
      := ∃ L vd, DeadFrom Γ L D1 D2
              ∗ (ZplusPos del (lhd Γ)) ↦ vd
              ∗ SCStack (λ v, ■ (v > 0)) (Z.to_pos vd) (Zpos ∘ fst <$> L)
              ∗ ■ (NoDup (fst <$> L) ∧ NoDup (snd <$> L)).

    Definition Trace_own_head Γ D j p γ: vPred :=
      [nXP (Z.to_pos (p + link)) in hist D j @ (Γ,γ) | LLP Γ j]_W.
    Definition Trace_own_tail Γ D j p γ: vPred :=
      (Z.to_pos (p.1 + data)) ↦{nf} p.2
      ∗ [nFXP (Z.to_pos (p.1 + link)) in hist D j @ γ | LLP Γ j]_W_nf.

    Definition Trace_link_fact
      D (L: list (Z * Z)) (L': list aloc) p (i: nat) j γ : Prop :=
        0 < p
      ∧ L' !! i = Some j
      ∧ info D j = Some (Z.to_pos p, γ)
      ∧ ((i + 1 = length L)%nat → hd_error (hist D j) = Some Null)
      ∧ ((i + 1 < length L)%nat →
          ∃ j', L' !! (i + 1)%nat = Some j'
                ∧ hd_error (hist D j) = Some $ Abs j').

    Lemma Trace_link_fact_different D L L' p1 p2 (i1 i2: nat) j1 j2 γ1 γ2
      (H1: Trace_link_fact D L L' p1 i1 j1 γ1)
      (H2: Trace_link_fact D L L' p2 i2 j2 γ2)
      (ND: NoDup L'):
      i1 ≠ i2 → j1 ≠ j2.
    Proof.
      destruct H1 as (_&H1&_). destruct H2 as (_&H2&_).
      move => H3 ?. subst j2. by apply H3, (NoDup_lookup L' _ _ j1).
    Qed.

    Lemma Trace_link_fact_same D L L' p1 p2 (i: nat) j1 j2 γ1 γ2
      (H1: Trace_link_fact D L L' p1 i j1 γ1)
      (H2: Trace_link_fact D L L' p2 i j2 γ2):
      p1 = p2 ∧ j1 = j2 ∧ γ1 = γ2.
    Proof.
      destruct H1 as (H10&H11&H12&_). destruct H2 as (H20&H21&H22&_).
      rewrite H11 in H21. inversion H21. subst j2.
      rewrite H12 in H22. inversion H22.
      apply Z2Pos.inj in H0; [by subst p2|auto|auto].
    Qed.

    Definition TraceSpine_elem Γ D L L' (i: nat) p j γ: vPred :=
      ■ Trace_link_fact D L L' (p.1) i j γ
      ∗ (   ■ (i = 0)%nat ∗ Trace_own_head Γ D j (p.1) γ
          ∨ ■ (i > 0)%nat ∗ Trace_own_tail Γ D j p γ 
              ∗ □ PermXList Γ (Z.to_pos (p.1)) j γ).

    Definition TraceSpineAbs Γ D (L: list (Z * Z)) (L': list (aloc * (Z * Z)))
      : vPred
      := (■ (L'.*2 = L ∧ NoDup (L'.*1)
                ∧ (∃ b, (L'.*1) !! 0%nat = Some b ∧ b ∈ base D)
                ∧ ∀ j, j ∈ (L'.*1) → j ∈ live D)
        ∗ vPred_big_opL
            L (λ (i: nat) p, ∃ j γ, TraceSpine_elem Γ D L (L'.*1) i p j γ))%VP.
      (* [∗ list] i ↦ p ∈ L , ∃ j γ, TraceSpine_elem Γ D L (L'.*1) i p j γ)%VP. *)

    Definition TraceSpine Γ D (L: list (Z * Z)) : vPred
      :=  ■ (L !! 0%nat = Some (Zpos (lhd Γ), 0))
        ∗ ∃ L', TraceSpineAbs Γ D L L'.

    Definition RemainingThreadTokens Γ D: vPred :=
      vPred_big_opL (seq 0 NPosn) (λ _ t, tnToks Γ t (dom _ D)).
      (* [∗ list] t ∈ seq 0 NPosn, tnToks Γ t (dom _ D). *)

    Definition CurrentState Γ L D : vPred
      := master Γ D ∗ SnapshotValid Γ D 
        ∗ TraceSpine Γ D L ∗ ■ SinglePtr D
        ∗ RemainingThreadTokens Γ D.

    Definition WriterSafe q L : vPred
      := ∃ Γ D1 D2 D3,
            ■ (lhd Γ = q ∧ D1 ⊑ D2 ∧ D2 ⊑ D3)
          ∗ RevokedUpTo Γ D1
          ∗ DeallocBetween Γ D1 D2
          ∗ CurrentState Γ L D3.

    Lemma rcu_remain_txtokens_one Γ D D' V i
      (HD: dom (gset _) D' ≡ {[i]} ∪ dom (gset _) D)
      (NIn: i ∉ dom (gset _) D) :
      RemainingThreadTokens Γ D V 
      ⊢ RemainingThreadTokens Γ D' V ∗
        ([∗ list] t ∈ seq 0 NPosn, tTok Γ t i V).
    Proof.
      rewrite -big_sepL_sepL. apply big_sepL_mono.
      iIntros (? t H) "tx".
      rewrite /tnToks.
      rewrite (coPset_of_gset_top_difference (dom (gset aloc) D) {[i]});
        last set_solver +NIn.
      rewrite -coPset_disj_union; last apply coPset_of_gset_top_disjoint.
      apply leibniz_equiv in HD.
      rewrite -op_singleton HD. iDestruct "tx" as "[$ $]".
    Qed.

  End Interpretation.

  Section proof.
    Context `{fG : foundationG Σ}
          `{aG: absViewG Σ}
          `{gLG: !gpsG Σ RCULinkProtocol _}
          `{gCG: !gpsG Σ RCUCounterProtocol _}
          `{CInvG : cinvG Σ}
          `{rG: !rcuG Σ}
          `{agreeG : !gps_agreeG Σ RCULinkProtocol}
          `{persG : persistorG Σ}.

    Local Notation iProp := (@iProp Σ).
    Local Notation vPred := (@vPred Σ).
    Implicit Types (Γ : RCUGhost) (t: nat) (i: aloc) (D: RCUData).

    Context (P : Z → vPred).

    Lemma rcuNew_spec :
      {{{{ ☐ PersistorCtx}}}}
          (rcuNew #())
      {{{{ (q: loc), RET #q;
            WriterSafe P q ((Zpos q, 0) :: nil) 
            ∗ vPred_big_opL (seq 0 NPosn) (λ _ t, ∃ D, ReaderSafe P q D t) }}}}.
            (* ∗ [∗ list] t ∈ seq 0 NPosn, ∃ D, ReaderSafe P q D t *)
    Proof.
      intros. iViewUp. iIntros "#kI kS #kP Post".
      wp_lam. iNext. wp_bind (malloc _).
      wp_bind (_ + _)%E. wp_op. iNext. wp_op. iNext.

      (* allocating locations *)
      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [lia|done|done|].
      iNext. iViewUp. iIntros (q) "kS oLs". wp_let. iNext.

      rewrite (_ : Z.to_nat (Z.pos n + 3) = S $ S $ S $ NPosn);
        last first.
        { rewrite Z2Nat.inj_add; [|by apply Pos2Z.is_nonneg|omega].
          rewrite (_: Z.to_nat 3 = 3%nat); last auto.
          rewrite Z2Nat.inj_pos. omega. }
      rewrite -vPred_big_opL_fold 3!big_sepL_cons vPred_big_opL_fold -/seq.

      iDestruct "oLs" as "(ol & od & ow & oLs)".

      (* initializing list head *)
      wp_bind ([_]_na <- _)%E. wp_op. iNext.

      set i := 4%positive.
      set Dnil : RCUData := (nil,nil).
      set D : gname → RCUData 
        := λ γ, ((i, Null) :: nil, (i,ZplusPos 0 q,γ) :: nil).
      iMod (own_alloc (● dlist_empty)) as (γh) "Hist"; first done.
      iMod (Tok_map_alloc NPosn) as (γw) "WX".
      iMod (Tok_map_alloc NPosn) as (γr) "RX".
      iMod (Tok_map_alloc NPosn) as (γt) "TX".
      set Γ := mkRCUGhost (ZplusPos 0 q) γh γw γr γt.
      assert (UD: ∀ γ, uwellformed (D γ)).
      { intros. right. apply RCUData_wellformed_init. }
      set Q : gname → vPred :=
        (λ γ, master Γ (D γ) ∗ snapshot Γ (D γ)
              ∗ [nXP ZplusPos 0 q in hist (D γ) i @ (Γ,γ) | LLP P Γ i ]_W)%VP.
      iApply (GPS_nSW_Init_strong (LLP P Γ i) Γ (λ γ, hist (D γ) i) Q
          with "[%] kI kS [$ol kP Hist]"); [auto|auto|reflexivity|..].
      { iSplitL ""; first auto. iIntros (γ). iViewUp. iIntros "$".
        iMod (own_update γh (● dlist_empty)
                            (● DList (D γ) (UD γ) ⋅ ◯ DList (D γ) (UD γ))
          with "Hist") as "[MS #SN]".
        { apply auth_update_alloc, dlist_local_update, RCUData_nil_min. }
        iModIntro. iSplitL ""; last iSplitL "".
        - iNext. by rewrite LLP_unfold.
        - iNext. rewrite LLP_unfold. iExists (D γ).
          iSplit; last by iSplit. iExists (UD γ). iFrame "SN".
        - iSplitL "MS"; by iExists (UD γ). }

      (* initializing garbage list *)
      iNext. iViewUp. iIntros (γ) "kS (MS & #SN & ol)". clear Q.
      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      wp_bind ((int) _)%E.
      wp_bind (#();; let : _ := Alloc in _)%E.

      iApply (newStack_spec (λ v, ■ (v > 0))%VP with "[%] kI kS []");
        [reflexivity|auto|].

      iNext. iViewUp. iIntros (s) "kS SCS". wp_value. wp_op. iNext.

      iApply (na_write with "[%] kI kS od"); [auto|reflexivity|].
      iNext. iViewUp. iIntros "kS od". wp_seq. iNext.
      wp_bind ([_]_na <- _)%E. wp_op. iNext.

      (* getting SnapshotValid *)
      iDestruct (GPS_nSW_Writer_Reader (IP:=LLP P Γ i) with "ol") as "[ol #qr]". 
      iAssert ((SnapshotValid P Γ (D γ))%VP V_l) as "#SV".
      { iFrame "SN". iExists i. iSplitL ""; first iPureIntro; last iSplitR "".
        - by rewrite /basel RCUData_basel_singleton.
        - iExists (ZplusPos link q), γ. by iFrame "qr".
        - iIntros "!#". iIntros (i'). iViewUp. iIntros (Live'). exfalso.
          move : Live'.
          rewrite RCUData_live_singleton elem_of_difference elem_of_singleton.
          by move => []. }
      iApply (GPS_SW_Init (WCP P Γ) ((D γ),0%nat)
                          ([XP ZplusPos wc q in ((D γ),0%nat) | WCP P Γ ]_W)
                with "[%] kI kS [$ow]"); [done|done|done|..].
      { iSplitL ""; first auto.
        iViewUp. iIntros "$". iModIntro. iNext. by iFrame "SV". }

      (* initializing readers *)
      iNext. iViewUp. iIntros "kS ow". wp_seq. iNext.

      destruct (GPS_SW_Writer_Reader (IP := WCP P Γ)
                  (l := ZplusPos wc q) (s := (D γ, 0%nat))) as [RW].
      iDestruct (RW with "ow") as "[ow #WR]"; first reflexivity. clear RW.

      wp_bind Alloc.

      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (sc) "kS osc". wp_seq. iNext.
      wp_bind ([_]_na <- _)%E.
      iApply (na_write with "[%] kI kS [$osc]"); [done|done|].
      iNext. iViewUp; iIntros "kS osc". wp_seq. iNext.
      wp_bind ((rec: "f" <> := _) _)%E.

      assert (DD: dead (D γ) = ∅). { by apply RCUData_dead_singleton. }
      assert (SD: SinglePtr (D γ)). { by apply RCUData_SinglePtr_singleton. }
      assert (DoD: dom _ (D γ) = {[i]}). { by apply RCUData_dom_singleton. }
      (* preparing reader's loop *)
      move Eqn' : {1 2 4}NPosn => n'.
      move Eqj: {5}(0%nat) => j.
      rewrite [in X in (sc ↦ X)%VP](_: 0 = Z.of_nat j); last omega.
      move Eqmax : {1}(n') => max.
      rewrite {3}(_: n = Pos.of_nat max); last by rewrite -Eqmax -Eqn' Pos2Nat.id.
      have Lemaxj: (j < max)%Z by lia.
      have Le0j : 0 ≤ j by omega.
      have Eqjn' : (j + n' = max)%Z by omega.

      (* ([∗ list] t ∈ seq j n', own_loc (ZplusPos (rc + t) q))%VP *)
      iAssert (
        (vPred_big_opL (seq j n') (λ _ t, (ZplusPos (rc + t) q) ↦ ?))%VP V_l)
        with "[oLs]" as "oLs".
      { rewrite -3!fmap_seq -2!list_fmap_compose
                -2!vPred_big_opL_fold big_sepL_fmap Eqj.
        iApply (big_sepL_mono with "oLs").
        move => k t _ /=.
        rewrite (_: ZplusPos ((S ∘ S ∘ S) t) q = ZplusPos (3 + t) q);
          first done.
        f_equal. rewrite 3!Nat2Z.inj_succ. lia. }

      set RS : nat → vPred :=
        λ t, (ReaderSafe P q (D γ) t
              ∗ [XP (ZplusPos (rc + t) q) in (D γ, 0%nat) | RCP P Γ t ]_R)%VP.
      (* ([∗ list] t ∈ seq 0 j, RS t)%VP *)
      iAssert ((vPred_big_opL (seq 0 j) (λ _ t, RS t))%VP V_l) as "RS".
      { by rewrite -Eqj -vPred_big_opL_fold big_sepL_nil. }
      have Eqmax2 : (max = NPosn) by omega.
      clear Eqmax Eqj Eqn'.
      iLöb as "IH" forall (V_l j n' Lemaxj Le0j Eqjn') "kP SN SV qr WR ∗".

      (* reader's loop *)
      wp_rec.
      iNext. wp_bind  (let: "i" := _ in _)%E.
      wp_bind ([_]_na)%E.
      iApply (na_read with "[%] kI kS osc"); [auto|reflexivity|].
      iNext. iViewUp. iIntros (z) "kS [% osc]". subst z.
      wp_seq. iNext. wp_bind ([_]_na <- _)%E.
      wp_bind (_ + _)%E. wp_op. iNext. wp_op. iNext.

      destruct n' as [|n''] eqn:Hn'; first by (exfalso; lia).

      rewrite (_ : seq j (S n'') = j :: seq (S j) n''); last done.
      rewrite -vPred_big_opL_fold 2!big_sepL_cons vPred_big_opL_fold.
      iDestruct "RX" as "[Rj RX]". iDestruct "oLs" as "[oj oLs]".
      rewrite (_ : ZplusPos j (ZplusPos rc q) = ZplusPos (rc + j) q); last first.
      { rewrite /ZplusPos. f_equal.
        by rewrite (Zplus_comm rc j) Zplus_assoc_reverse. }

      iApply (GPS_SW_Init (RCP P Γ j) ((D γ),0%nat)
                  ([XP ZplusPos (rc + j) q in ((D γ),0%nat) | RCP P Γ j ]_W)
                  with "[%] kI kS [kP $oj]"); [done|done|done|..].
      { iSplitL ""; first done. iViewUp. iIntros "$". iModIntro. iNext.
        iSplitL ""; first done. iFrame "WR". iSplitL ""; first done.
        iIntros "!#". rewrite DD. iIntros (??? HD).
        by apply not_elem_of_empty in HD. }

      iNext. iViewUp. iIntros "kS jX". wp_seq. iNext.
      wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write with "[%] kI kS [$osc]"); [done|done|].
      iNext. iViewUp; iIntros "kS osc". wp_seq. iNext.

      iAssert ((vPred_big_opL (seq 0 (j + 1)) (λ _ t, RS t))%VP V_l)
        (* ([∗ list] t ∈ seq 0 (j + 1), RS t V_l) *)
        with "[RS jX Rj]" as "RS".
      { rewrite Nat.add_1_r seq_S -2!vPred_big_opL_fold big_sepL_app
                big_sepL_singleton.
        iFrame "RS".
        destruct (GPS_SW_Writer_Reader (IP := RCP P Γ j)
                  (l := ZplusPos (rc + j) q) (s := (D γ, 0%nat))) as [RW].
        iDestruct (RW with "jX") as "[jX jR]"; first reflexivity. clear RW.
        iFrame "jR". iExists Γ. iSplitL ""; last iSplitL "Rj".
        - iPureIntro. split; first auto. rewrite -Eqmax2. omega.
        - iFrame "SV".
          by rewrite DD /rnToks coPset_of_gset_empty coPset_difference_top_empty.
        - iExists 0%nat, 0%nat, (D γ). iFrame "jX WR". iSplit; first auto.
          iIntros "!#". rewrite DD. iIntros (??? HD).
          by apply not_elem_of_empty in HD. }

      wp_op. iNext. wp_op. iNext. wp_op => [Max|NMax].
      - iClear "IH". iNext. wp_let. iNext. wp_op => [? //|_]. iNext.
        iApply wp_if_false. iNext. wp_value. wp_seq. iNext.
        wp_bind (Dealloc _).
        iApply (wp_mask_mono); first auto.
        iApply (dealloc with "[%] kI kS [$osc]"); first reflexivity.
        iNext. iViewUp. iIntros "kS _". wp_seq. iNext. wp_value.
        iApply ("Post" with "[] kS [-]"); first done.
        assert (Hj : (j + 1 = NPosn)%nat).
        { apply Nat2Z.inj_iff.
          rewrite -Eqmax2 Nat2Z.inj_add Max -positive_nat_Z Nat2Pos.id;
            [done|omega]. }
        rewrite -(vPred_big_opL_fold (seq _ (j + 1))) /RS big_sepL_sepL.
        iDestruct "RS" as "[RS RS2]".
        iSplitR "RS".
        + iExists Γ, (D γ), (D γ), (D γ). iSplitL ""; first done.
          iSplitL "ow WX RS2"; last iSplitR "TX MS ol".
          * iExists 0%nat. rewrite -Hj. iFrame "ow".
            iCombine "WX" "RS2" as "WX". rewrite -big_sepL_sepL.
            iApply (big_sepL_mono with "WX").
            iIntros (???) "[WX $]".
            by rewrite DD /wnToks coPset_of_gset_empty
                       coPset_difference_top_empty.
          * iExists nil, (Z.pos s). rewrite Pos2Z.id. iFrame "od SCS".
            iSplitL ""; first iSplitL ""; first done.
            { by rewrite -vPred_big_opL_fold big_sepL_nil. }
            { iPureIntro. split; by apply NoDup_nil_2. }
          * iFrame (SD) "MS SV". iSplitR "TX".
            { iSplitL ""; first done. iExists ((i, (Z.pos q, 0)) :: nil).
              iSplitL ""; first iPureIntro.
              - split; first done.
                split; [apply NoDup_singleton|split].
                + exists i. split; [done|by rewrite RCUData_base_singleton].
                + rewrite RCUData_live_singleton.
                  by move => ? /elem_of_list_singleton ->.
              - rewrite -vPred_big_opL_fold big_sepL_singleton.
                iExists i, γ. iSplitL ""; first iPureIntro.
                + split; [apply Pos2Z.is_pos|split; first done].
                  split; [done|split].
                  * by rewrite /hist rcu_hist'_cons_now.
                  * simpl. omega.
                + iLeft. by iFrame "ol". }
            { rewrite /RemainingThreadTokens. iApply (big_sepL_mono with "TX").
              iIntros (k y ?). rewrite DoD. apply own_mono, singleton_included.
              exists (CoPset ⊤). split; first by rewrite lookup_singleton.
              apply Some_included. right. by apply coPset_disj_included. }
        + rewrite (_ : j + 1 = max)%nat; last first.
          { apply Nat2Z.inj_iff.
            rewrite Nat2Z.inj_add Max -positive_nat_Z Nat2Pos.id; [done|omega]. }
          iApply (big_sepL_mono with "RS"). iIntros (???) "?". by iExists _.
      - iNext. wp_let. iNext. wp_op => [_|?//].
        iNext. iApply wp_if_true. iNext. rewrite -Nat.add_1_r.
        iApply ("IH" $! _ (j+1)%nat (n'') with 
                      "[%] [%] [%] [kP] [] [] [] [] Post
                       WX RX TX MS SCS od ol ow kS [osc] oLs RS").
        + apply Z.le_neq. split; first lia.
          move => Eq. apply NMax. rewrite Nat2Z.inj_add in Eq.
          rewrite Eq -positive_nat_Z Nat2Pos.id; [done|omega].
        + omega.
        + lia.
        + by iIntros "!#".
        + by iIntros "!#".
        + by iIntros "!#".
        + by iIntros "!#".
        + by iIntros "!#".
        + by rewrite Nat2Z.inj_add.
    Qed.

    Lemma rcuReadStart_spec q D t :
      {{{{ ReaderSafe P q D t }}}}
        rcuReadStart #q
      {{{{ (p: Z), RET #p; ReaderSafe P q D t ∗ SafePtr P q D p }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS RS Post".
      iDestruct "RS" as (Γ) "(% & (#SV & RN) & RC)".
      iPoseProof ("SV") as "SV'". iDestruct "SV'" as "(SN & SB)".
      iDestruct "SB" as (b) "(% & SB & OB)".
      iDestruct "SB" as (p γ) "(% & BR)".
      destruct H1 as [H1 H2]. subst p. destruct H as [H H']. rewrite -H.

      wp_seq. iNext. wp_op. iNext.
      iApply (GPS_nSW_Read (LLP P Γ b) (γ:= γ) Γ True
              (λ s' v, LLP P Γ b γ false (ZplusPos link q) s' v)%VP
                with "[%] kI kS [$BR]"); [done|done|reflexivity|..].
      { iNext. iSplitL "".
        - iIntros (s'). iViewUp. iIntros (Le).
          iLeft. iIntros (v). iViewUp. by iIntros "[$ _]".
        - iIntros (???). iViewUp. iIntros "[_ #LLP]". by iFrame "LLP". }

      iNext. iViewUp. iIntros (L' v) "kS (% & #BR' & Q)".

      rewrite LLP_unfold. iDestruct "Q" as (D') "(#SN' & % & HL)".

      iDestruct (snapshot_decided with "SN SN'") as %SN.
      iDestruct (snapshot_wellformed with "SN") as %W1.
      iDestruct (snapshot_wellformed with "SN'") as %W2.

      iApply ("Post" with "[%] kS [-]"); first done. iSplitR "HL".
      - iExists Γ. by iFrame "SV RN RC".
      - iExists Γ. iFrame (H) "SV".
        destruct L' as [|bj]; first iDestruct "HL" as %[].
        destruct H3 as [H3 H3'].
        assert (D.1 ≠ nil).
        { move => Eqn. move : H0.
          by rewrite /base /base' /basel Eqn /=. }
        assert (D'.1 ≠ nil).
        { move => Eqn. move : H3. by rewrite /hist /hist' Eqn /=. }
        assert (WD: wellformed D).
        { move : W1 => [W1|//]. by rewrite W1 in H4. }
        assert (WD': wellformed D').
        { move : W2 => [W2|//]. by rewrite W2 in H5. }
        iViewUp. iIntros (Hv).
        assert (Hb: b ∈ live D').
        { apply rcu_base_live. destruct SN as [SN|SN].
          - destruct SN as [[SN|[SN Eqb]] _]; first done.
            move: H0. rewrite /base -Eqb /base' /basel elem_of_of_list.
            apply hd_error_some_elem.
          - destruct SN as [[SN|[SN Eqb]] _]; first done. 
            move: H0. rewrite /base Eqb /base' /basel elem_of_of_list.
            apply hd_error_some_elem. }
        destruct bj as [|j|];
          [by iDestruct "HL" as %?| |by iDestruct "HL" as %?].
        iDestruct "HL" as (γj) "(% & # Perms)".
        destruct H6 as [G0 Eqj].
        iExists j, γj.
        iSplitR "Perms"; last by iIntros "!#".
        iPureIntro. split; [auto|split].
        + assert (Hj': j ∈ live D').
          { destruct WD' as (_&_&_&_&_&Live).
            move : (Live _ Hb) => [j' [ ]].
            rewrite /hist in H3. rewrite H3 /=.
            by move => [<-] [//|[? [[<-] ?]]]. }
          destruct SN as [SN|SN].
          { destruct SN as [[?|[[D0 SN] ?]] ?]; first done.
            move => /(rcu_dead'_app D0). rewrite -SN.
            by apply elem_of_difference in Hj' as [? ?]. }
          { assert (Eqh: hist D b = Abs j :: L').
            { rewrite -H3. apply : anti_symm.
              - by rewrite -H3 in H2.
              - by apply RCUHist_ext_piecewise. }
            assert (Hb': b ∈ live D).
            { apply rcu_base_live.
              move: H0. rewrite /base /base' /basel elem_of_of_list.
              apply hd_error_some_elem. }
            destruct WD as (_&_&_&_&_&Live).
            move : (Live _ Hb') => [j' [ ]].
            rewrite /hist in Eqh. rewrite Eqh /=.
            move => [<-] [//|[? [[<-] /elem_of_difference[//]]]]. }
        + move => ?. destruct SN as [SN|SN].
          { destruct (rcu_wellformed_info_lookup D j) as [p Eqp]; [|auto|].
            - destruct W1 as [W1|]; last auto. by rewrite W1 in H0.
            - by rewrite (RCUData_info_lookup D D' j p W2 SN Eqp) -Eqp in Eqj. }
          { by apply (RCUData_info_lookup D'). }
    Qed.

    Lemma rcuSnapshotValid_extract Γ D V:
      SnapshotValid P Γ D V
      ⊢ ∃ b γ, [nXP ZplusPos link (lhd Γ) in hist D b @ (Γ,γ) | LLP P Γ b ]_R V.
    Proof.
      iIntros "[_ SN]". iDestruct "SN" as (b) "(_ & SN & _)".
      iDestruct "SN" as (p γ) "(_ & SN)". by iExists b, γ.
    Qed.

    Lemma rcu_ZplusPos (p k: Z) (Hp: 0 < p) :
      ZplusPos k (Z.to_pos p) = Z.to_pos (p + k).
    Proof. rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; omega. Qed.

    Lemma rcuReadNext_spec q D t nptr rptr :
      {{{{ ∃ p, ReaderSafe P q D t ∗ nptr ↦ p
                ∗ SafePtr P q D p ∗ ∃ v, rptr ↦ v }}}}
        rcuReadNext #q #nptr #rptr
      {{{{ (x : Z), RET #x;
            ∃ v p, ReaderSafe P q D t ∗ nptr ↦ p ∗ SafePtr P q D p
                   ∗ rptr ↦ v ∗ ((■ (x = 0) ∗ □ P v) ∨ ■ (x = 1)) }}}}.
    Proof using agreeG.
      intros. iViewUp. iIntros "#kI kS RD Post".
      iDestruct "RD" as (p) "(RS & np & Safe & rp)".
      iDestruct "rp" as (v0) "rp".
      wp_bind (_ #nptr)%E.
      wp_lam. iNext. wp_value. wp_lam. iNext. wp_value. wp_lam. iNext.
      wp_bind ([_]_na)%E.
      iApply (na_read with "[%] kI kS np"); [done|reflexivity|].

      iNext. iViewUp. iIntros (z) "kS (% & np)". subst z.
      wp_seq. iNext. wp_op => [Eq0|NEq0]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%] kS [-]"); first done.
        iExists v0, p. iFrame "RS rp Safe np". by iRight.
      - iApply wp_if_false.

        iDestruct "RS" as (Γ) "(% & (#SN1 & RT) & RC)".
        iDestruct "Safe" as (Γ') "(% & (#SN2 & PH))". destruct H as [H H'].
        iAssert (⌜Γ' = Γ⌝)%I with "[]" as %?.
        { iDestruct (rcuSnapshotValid_extract with "SN1") as (b1 γ1) "nH1".
          iDestruct (rcuSnapshotValid_extract with "SN2") as (b2 γ2) "nH2".
          rewrite -H -H0.
          by iDestruct (GPS_nSWs_param_agree 
                         (IP1:=LLP P Γ' b2) (IP2:=LLP P Γ b1) γ2 γ1 Γ' Γ
                         (hist D b2) (hist D b1) V_l with "[$nH2 $nH1]")
                         as %[? ?]. }
        subst Γ'. clear H0.

        iDestruct ("PH" with "[%] [%]") as (i γ) "[% #Perms]";
          [reflexivity|auto|].
        destruct H0 as [Hpp [HiD Hiγ]].

        iDestruct (Tok_negation_split _ _ _ _ HiD with "RT") as "[RT Ri]".

        iDestruct ("Perms" $! t with "[%] [%]") as (β) "Permi";
          [reflexivity|auto|].

        iDestruct (exchange_elim_l (rTok Γ t i)
                   (PermX'_right P (LLP P) Γ (Z.to_pos p) t i γ) β
          with "[] [%] [$Permi $Ri]") as "EX"; [|reflexivity|..].
        { iIntros (??) "_ rT". iApply (rcuTok_tid_exclusive with "rT"). }
        { iDestruct "Permi" as (V) "(_ & ABS & _)". iNext. by iExists V. }

        iNext.

        iMod (fupd_mask_mono with "EX") as "EX"; first done.
        wp_bind ([_]_na)%E. wp_bind ((loc) _)%E. wp_op. iNext. wp_op. iNext.

        rewrite 2!abs_splitjoin 2!abs_exist.
        iDestruct "EX" as "(tTok & od & or)".
        iDestruct "od" as (v) "od". rewrite abs_splitjoin.
        iDestruct "od" as "[#P od]".
        iDestruct "or" as (L) "or".

        rewrite rcu_ZplusPos; last omega.

        destruct (exchange_aSeen (rTok Γ t i)
                   (PermX'_right P (LLP P) Γ (Z.to_pos p) t i γ) β) as [ES].
        iDestruct (ES with "Permi") as "#aS"; first reflexivity. clear ES.

        iApply (na_read_abs_frac with "[%] kI kS [$od $aS]");
          [done|done|reflexivity|].
        iNext. iViewUp. iIntros (z) "kS [% od]". subst z.
        wp_seq. iNext. wp_bind ([_]_at)%E. wp_bind ((loc) _)%E.
        wp_op. iNext. wp_op. iNext.

        rewrite rcu_ZplusPos; last omega.

        iDestruct "SN1" as "(SN & SN1)".
        iDestruct "SN1" as (b) "(% & SN1 & #Seens)".
        iDestruct "SN1" as (pb γb) "(% & ob)".

        set s := if (decide (i ∈ dom (gset aloc) D))
                 then (hist D i)
                 else L.
        iAssert (|={top}=> PrtSeen γ s V_l
              ∗ (⌞[nFXP Z.to_pos (p + 0) in L@γ | LLP P Γ i ]_R_(nf)⌟)%VP β V_l)%I
          with "[or]" as ">[#PS or]".
        { iDestruct "or" as (Vβ) "[#abs or]".
          iDestruct "aS" as (Vβ') "[% #abs2]".
          iDestruct (absView_agree with "abs abs2") as %?. subst Vβ'.
          rewrite /s. case (decide (i ∈ dom (gset aloc) D)) => [InD|NInD].
          - case (decide (i = b)) => [?|?].
            + (* if i = b then p = q, so 2 protocols govern q, so False *)
              subst i. destruct H1 as [H1 H1'].
              rewrite (Hiγ InD) in H1. inversion H1. subst γb pb.
              rewrite -H4 rcu_ZplusPos; last omega.
              rewrite [X in [nFXP _ in _@_ |_]_R_nf X] H2.

              by iMod (GPS_nSW_nFRP_clash with "ob or") as %?; done.
            + iDestruct ("Seens" $! i with "[%] [%]") as (pi γi) "[% PS]";
              first reflexivity.
              { by rewrite elem_of_difference elem_of_singleton /live /live'
                           elem_of_difference. }
              rewrite (Hiγ InD) in H3. inversion H3.
              iFrame "PS or". iExists Vβ. by iFrame "abs".
          - destruct (GPS_nFRP_PrtSeen (IP := LLP P Γ i)
                                       γ (Z.to_pos (p + 0)) L nf) as [SE].
            iDestruct (SE with "or") as "#Ha"; first reflexivity.
            iFrame "or Ha". iExists Vβ. by iFrame "abs". }

        iApply (GPS_nFRP_Read_PrtSeen_abs (IP := LLP P Γ i) _ γ _ True
               (λ s' v', LLP P Γ i γ false (Z.to_pos (p + 0)) s' v')%VP L s β
                with "[%] kI kS [$aS $or $PS]"); [done|done|reflexivity|..].
        { iNext. iSplitL "".
          - iIntros (s'). iViewUp. iIntros (Hs').
            iLeft. iIntros (v'). iViewUp. by iIntros "[$ _]".
          - iIntros (?????). iIntros "[_ #LLP]". by iFrame "LLP". }

        iNext. iViewUp. iIntros (L' p') "kS (% & or & LLP)".

        rewrite LLP_unfold.
        iDestruct "LLP" as (D') "(#SN' & % & LLP)".

        iDestruct (snapshot_wellformed with "SN") as %W1.
        iDestruct (snapshot_wellformed with "SN'") as %W2.
        destruct L' as [|bj L']; first by iDestruct "LLP" as %?.
        destruct H3 as [H3 H3'].
        assert (D.1 ≠ nil).
        { move => Eqn. move : H0.
          by rewrite /base /base' /basel Eqn /=. }
        assert (D'.1 ≠ nil).
        { move => Eqn. move : H3. by rewrite /hist /hist' Eqn /=. }
        assert (WD: wellformed D).
        { move : W1 => [W1|//]. by rewrite W1 in H4. }
        assert (WD': wellformed D').
        { move : W2 => [W2|//]. by rewrite W2 in H5. }
        iDestruct (snapshot_decided with "SN SN'") as %SN.

        iAssert (SafePtr P q D p' V_l)%I with "[LLP]" as "SP'".
        { iExists Γ. iFrame (H) "SN2". iViewUp. iIntros (Hp').
          destruct bj as [|j|];
            [by iDestruct "LLP" as %?| |by iDestruct "LLP" as %?].
          iDestruct "LLP" as (γj) "[% #Permj]". destruct H6 as [H6 H7].
          iExists j, γj. iSplitR ""; last by iIntros "!#".
          iPureIntro. split; [auto|split].
          - rewrite /hist in H3.
            assert (Hi: i ∈ live D').
            { apply elem_of_difference. split.
              - apply elem_of_rcu_hist_dom. exists (Abs j). rewrite H3. by left.
              - destruct WD' as (_&_&_&_&DO&_).
                move => HDi. destruct (DO _ HDi) as [Li [HLi DLi]].
                by rewrite H3 in HLi. }
            assert (Hj: j ∈ live D').
            { destruct WD' as (_&_&_&_&_&LC).
              destruct (LC _ Hi) as [i' [Hi' Hji']].
              rewrite H3 in Hi'. inversion Hi'. subst i'.
              by move : Hji' => [//|[j' [[<-]]]]. }
            destruct SN as [SN|SN].
            { destruct SN as [[?|[[D0 SN] ?]] ?]; first done.
              move => /(rcu_dead'_app D0). rewrite -SN.
              by apply elem_of_difference in Hj as [? ?]. }
            { assert (i ∈ dom (gset _) D).
              { apply (RCUData_ext_dom _ _ SN), elem_of_rcu_hist_dom.
                  rewrite H3. exists (Abs j). by left. }
              assert (Eqh: hist D i = Abs j :: L').
              { rewrite -H3. apply : anti_symm.
                - destruct H2 as [_ H2].
                  by rewrite -H3 /s decide_True in H2.
                - by apply RCUHist_ext_piecewise. }
              destruct WD as (_&_&_&_&_&LC).
              destruct (LC i) as [i' [Hi' Hji']].
              - by apply elem_of_difference.
              - rewrite /hist in Eqh. rewrite Eqh in Hi'.
                inversion Hi'. subst i'.
                move : Hji' => [//|[? [[<-] /elem_of_difference[//]]]]. }
          - move => ?. destruct SN as [SN|SN].
            + destruct (rcu_wellformed_info_lookup D j) as [p1 Eqp1]; [|auto|].
              * destruct W1 as [W1|]; last auto. by rewrite W1 in H0.
              * by rewrite (RCUData_info_lookup D D' j p1 W2 SN Eqp1) -Eqp1 in H7.
            + by apply (RCUData_info_lookup D'). }

        iDestruct (exchange_elim_r (rTok Γ t i)
                   (PermX'_right P (LLP P) Γ (Z.to_pos p) t i γ) β
          with "[] [%] [$Permi tTok od or]") as "EX"; [|reflexivity|..].
        { iIntros (??) "_ ((rT & _) & (rT2 & _))".
          iApply (rcuTok_tid_exclusive with "[$rT $rT2]"). }
        { iNext. rewrite /PermX'_right. rewrite 2!abs_splitjoin 2!abs_exist.
          iFrame "tTok". iSplitL "od".
          - iExists v. rewrite abs_splitjoin. iFrame "P".
            rewrite rcu_ZplusPos; [done|omega].
          - iExists L. rewrite -rcu_ZplusPos; [iAssumption|omega]. }

        iMod (fupd_mask_mono with "EX") as ">EX"; first done.
        destruct (aSeen_elim (rTok Γ t i) β) as [SE].
        iDestruct (SE with "[$aS $EX]") as "rT"; [reflexivity|clear SE].
        destruct (aSeen_elim (□ P v) β) as [SE].
        iDestruct (SE with "[$aS $P]") as "Pl"; [reflexivity|clear SE].

        iDestruct (Tok_negation_join _ _ _ _ HiD with "[$RT $rT]") as "RT".
        wp_let. iNext. wp_bind ([_]_na <- _)%E.
        iApply (na_write with "[%] kI kS [$rp]"); [auto|reflexivity|].
        iNext. iViewUp. iIntros "kS rp". wp_seq. iNext.
        wp_bind ([_]_na <- _)%E.
        iApply (na_write with "[%] kI kS [$np]"); [done|done|].
        iNext. iViewUp. iIntros "kS np".
        wp_seq. iNext. wp_value.
        iApply ("Post" with "[%] kS [-]"); first done.
        iExists v, p'. iFrame "SP' rp np". iSplitR "Pl".
        + iExists Γ. by iFrame "RC SN2 RT".
        + iLeft. by iFrame "Pl".
    Qed.


    Lemma rcu_ZplusPos_assoc (i j: Z) p (Hj: 0 < j):
      ZplusPos i (ZplusPos j p) = ZplusPos (j + i) p.
    Proof.
      rewrite /ZplusPos. f_equal. rewrite Z2Pos.inj_add; [|auto|lia].
      rewrite Pos2Z.inj_add !Pos2Z.id Z2Pos.id; omega.
    Qed.

    Lemma rcuQuiescentState_spec q D t:
      {{{{ ReaderSafe P q D t }}}}
        rcuQuiescentState #q #t
      {{{{ RET #(); ∃ D', ■ (D ⊑ D') ∗ ReaderSafe P q D' t }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS RS Post".
      wp_seq. iNext. wp_value. wp_seq. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iDestruct "RS" as (Γ) "(% & (#SN & RN) & RC)".
      iDestruct "RC" as (n1 n2 D2) "(% & RW & WR & #MD)".
      destruct H as [H H']. rewrite -H.
      iApply (GPS_SW_Read (WCP P Γ) True
                  (λ s v, ■ (v = s.2) ∗ SnapshotValid P Γ (s.1))%VP
                with "[%] kI kS [$WR]"); [done|done|reflexivity|..].
      { iSplitR "".
        - iNext. iIntros (s' ??) "_". iLeft. iIntros (?).
          iViewUp. by iIntros "[[? $] ?]".
        - iNext. iIntros (? ? ?). iViewUp. iIntros "[? #?]".
          iModIntro. by iSplit. }

      iNext. iViewUp. iIntros (s' v) "kS (% & #WR & % & SN')".
      destruct s' as [D3 n3]. subst v.
      wp_seq. iNext. wp_bind (_ + _)%E. wp_op. iNext. wp_op. iNext.
      rewrite rcu_ZplusPos_assoc; last done.

      assert (HD: D ⊑ D3).
      { destruct H1 as [H1|[? ?]]; [by inversion H1| auto]. }

      iDestruct (Tok_split_difference _ _ _ (dead D3) with "RN") as "[RN RD]";
        first by apply rcu_dead_extend.


      (* (([∗ set] i ∈ (dead D3 ∖ dead D), ModX Γ t i)%VP V_l) *)
      iAssert (|={⊤}=> ((vPred_big_opS (dead D3 ∖ dead D) (λ i, ModX Γ t i))%VP V_l))%I 
          with "[RD]" as "RD".
      { iApply (fupd_big_sepS _ _ (dead D3 ∖ dead D)).
        iApply (big_sepS_mono (λ i, rTok Γ t i V_l)); first reflexivity.
        - iIntros (j Inj) "rT".
          destruct (escrow_alloc (wTok Γ t j) (rTok Γ t j)) as [ES].
          iApply fupd_mask_mono; last iApply ES; [done|reflexivity|by iNext].
        - case (decide (dead D3 ∖ dead D = ∅)) => [->|?].
          + by rewrite big_sepS_empty.
          + by rewrite /rToks rcuTok_own_big_sepS. }

      iMod "RD" as "#RD".
      iAssert ((□ (∀ i : aloc, ■ (i ∈ dead D3) → ModX Γ t i))%VP V_l) 
        with "[]" as "#MD'".
      { iIntros "!#". iIntros (i). iViewUp. iIntros (Ini).
        case (decide (i ∈ dead D)) => [InD|NInD].
        - iApply ("MD" with "[%] [%]"); done.
        - iApply (big_sepS_elem_of _ ((dead D3 ∖ dead D)) with "RD").
          by apply elem_of_difference. }

      iApply (GPS_SW_ExWrite (RCP P Γ t) (D3 ,n3) True 
                  (λ s, [XP ZplusPos (rc + t) q in s | RCP P Γ t ]_W)%VP
                with "[%] kI kS [$RW]"); [by etrans|done|done|reflexivity|..].
      { iNext. iIntros (v'). iViewUp. iIntros "(_ & $ & $)".
        iModIntro. iNext. rewrite H. iFrame "WR".
        iSplitL ""; first auto. by iIntros "!#". }

      iNext. iViewUp. iIntros "kS [_ RW]".

      iApply ("Post" with "[] kS [-]"); first done.
      iExists D3. iFrame (HD). iExists Γ. iFrame "SN' RN".
      iSplitL ""; first auto.
      iExists n3, n3, D3. rewrite H. iFrame "WR RW".
      iSplitL ""; first auto. by iIntros "!#".
    Qed.

    Lemma rcuCollect_spec q Γ D1 D2 nr ngc:
      lhd Γ = q → D1 ⊑ D2 → 0 < nr →
      {{{{  [XP (ZplusPos wc q) in (D2,ngc) | WCP P Γ]_W
            ∗ vPred_big_opL (seq 0 (Z.to_nat nr)) (λ _ t, wToks Γ t (dead D2 ∖ dead D1)
              ∗ ∃ s, [XP (ZplusPos (rc + t) q) in s | RCP P Γ t]_R)
          (* ([∗ list] t ∈ seq 0 (Z.to_nat nr), wToks Γ t (dead D2 ∖ dead D1)
              ∗ ∃ s, [XP (ZplusPos (rc + t) q) in s | RCP P Γ t]_R ) *) }}}}
        rcuCollect #q #nr #ngc
      {{{{ RET #();
            [XP (ZplusPos wc q) in (D2,ngc) | WCP P Γ]_W
          ∗ vPred_big_opL (seq 0 (Z.to_nat nr)) (λ _ t,
              rToks Γ t (dead D2 ∖ dead D1)
                ∗ [XP (ZplusPos (rc + t) q) in (D2,ngc) | RCP P Γ t]_R)
            (* ([∗ list] t ∈ seq 0 (Z.to_nat nr),
                rToks Γ t (dead D2 ∖ dead D1)
                ∗ [XP (ZplusPos (rc + t) q) in (D2,ngc) | RCP P Γ t]_R) *) }}}}.
    Proof.
      intros Hq Le Hnr. intros. iViewUp.
      iIntros "#kI kS [WCP wT] Post".
      wp_bind ((rcuCollect _) _)%E. wp_lam. iNext.
      do 2 (wp_value; wp_lam; iNext). wp_bind Alloc.
      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (sc) "kS osc". wp_seq. iNext.
      wp_bind ([_]_na <- _)%E.
      iApply (na_write with "[%] kI kS [$osc]"); [done|done|].
      iNext. iViewUp; iIntros "kS osc". wp_seq. iNext.

      (* preparing reader's loop *)
      move Eqn' : (Z.to_nat nr) => n'.
      move Eqj: {1}(0%nat) => j.
      rewrite [in X in (sc ↦ X)%VP](_: 0 = Z.of_nat j); last omega.
      move Eqmax : {2}(n') => max.
      rewrite (_: nr = Z.of_nat max); last first.
      { rewrite -Eqmax -Eqn' Z2Nat.id; [auto|omega]. }
      have Lemaxj: (j < max)%Z.
        { rewrite -Eqmax -Eqn' -Eqj.
          apply inj_lt, (Z2Nat.inj_lt 0 nr); omega. }
      have Le0j : 0 ≤ j by omega.
      have Eqjn' : (j + n' = max)%Z by omega.

      rewrite -Eqn' in Eqmax.
      set RS: nat → vPred :=
        λ t, (rToks Γ t (dead D2 ∖ dead D1)
               ∗ [XP ZplusPos (rc + t) q in (D2, ngc) | RCP P Γ t ]_R)%VP.
      (* [∗ list] t ∈ seq 0 j, RS t *)
      iAssert (vPred_big_opL (seq 0 j) (λ _ t, RS t) V_l) as "RS".
      { by rewrite -Eqj -vPred_big_opL_fold big_sepL_nil. }
      clear Eqj Eqn'.
      wp_bind ((rec: "f" <> := _ ) _)%E.
      iLöb as "IH" forall (V_l j n' Lemaxj Le0j Eqjn').

      (* reader's loop *)
      wp_rec.
      iNext. wp_bind  (let: "i" := _ in _)%E. wp_bind ([_]_na)%E.
      iApply (na_read with "[%] kI kS osc"); [auto|reflexivity|].
      iNext. iViewUp. iIntros (z) "kS [% osc]". subst z.
      wp_seq. iNext.

      destruct n' as [|n''] eqn:Hn'; first by (exfalso; lia).

      rewrite (_ : seq j (S n'') = j :: seq (S j) n''); last done.
      rewrite -vPred_big_opL_fold big_sepL_cons vPred_big_opL_fold.
      iDestruct "wT" as "[[wj Rj] wT]".
      iDestruct "Rj" as ([D' m']) "#Rj".
      wp_bind ((rec: "f" <> := _ ) _)%E.
      iLöb as "IH2" forall (V_l) "Rj ∗".
      iApply wp_repeat; first auto.
      wp_bind ([_]_at)%E. wp_bind (_ + _)%E. wp_op. iNext. wp_op. iNext.
      rewrite rcu_ZplusPos_assoc; last omega.

      iApply (GPS_SW_Read (RCP P Γ j) True
                  (λ s' v, RCP P Γ j false (ZplusPos (rc + j) q) s' v)%VP
                with "[%] kI kS [$Rj]"); [done|done|reflexivity|..].
      { iSplitR "".
        - iNext. iIntros (s' ??) "_". iLeft. iIntros (?).
          iViewUp. by iIntros "[[? $] ?]".
        - iNext. iIntros (? ? ?). iViewUp. iIntros "[? #?]".
          iModIntro. by iSplit. }

      iNext. iViewUp. iIntros ([D'' m''] z) "kS (% & Rj2 & (% & #Wr & #Mod))".
      subst z. wp_value.
      wp_op => [/Z_of_nat_inj Eqgc|?]; iNext; last first.
      { iExists 0. iSplit; first done.
        erewrite decide_True; last done. iNext. iNext.
        iApply ("IH2" with "[] WCP wj wT Post RS kS osc"). by iIntros "!#". }

      iClear "IH2".
      iExists 1. iSplit; first done. erewrite decide_False; last auto.
      iNext. iNext. wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.

      iApply (na_write with "[%] kI kS [$osc]"); [done|done|].
      iNext. iViewUp; iIntros "kS osc". wp_seq. iNext.

      destruct (GPS_SW_Writer_max (IP := WCP P Γ) (ZplusPos wc q)
                                  (D'', m'') (D2, ngc) top) as [WM]; first done.
      iMod (WM V_l V_l with "[Wr $WCP]") as "[WCP %]";
        [reflexivity|by rewrite Hq|]. clear WM.
      destruct H0 as [H0|[_ H0]]; last first.
      { rewrite Eqgc /= in H0. by omega. }
      inversion H0. subst D'' m''.

      (* ([∗ set] i ∈ (dead D2 ∖ dead D1), rTok Γ j i)%VP *)
      iAssert (|={⊤}=> (vPred_big_opS (dead D2 ∖ dead D1) (λ i, rTok Γ j i) V_l))%I 
          with "[wj]" as ">rT".
      { case (decide (dead D2 ∖ dead D1 = ∅)) => [->|?].
        - by rewrite -vPred_big_opS_fold big_sepS_empty.
        - rewrite /wToks rcuTok_own_big_sepS; last auto.
          iApply (fupd_big_sepS _ _ (dead D2 ∖ dead D1)).
          iApply (big_sepS_impl (λ i, wTok Γ j i V_l) with "[$wj]").
          iIntros "!#". iIntros (i Ini) "wT".
          iDestruct ("Mod" $! i with "[%] [%]") as "Modi"; [reflexivity|..].
          { by apply elem_of_difference in Ini as []. }

          iDestruct (escrow_apply (wTok Γ j i) (rTok Γ j i)
                      with "[] [%] [$Modi $wT]") as "rT"; [|reflexivity|].
          { iIntros (??) "_ wT". iApply (rcuTok_tid_exclusive with "wT"). }

          iMod (fupd_mask_mono with "rT") as ">$"; [done|auto]. }

      iAssert (|==> RS j V_l)%I with "[$Rj2 rT]" as ">RSj".
      { case (decide (dead D2 ∖ dead D1 = ∅)) => [->|?].
        - rewrite /rToks coPset_of_gset_empty.
          iApply rcuTok_own_empty.
        - by rewrite -vPred_big_opS_fold /rTok -rcuTok_own_big_sepS. }

      (* ([∗ list] t ∈ seq 0 (j + 1), RS t)%VP *)
      iAssert (vPred_big_opL (seq 0 (j + 1)) (λ _ t, RS t) V_l)%I
        with "[RS RSj]" as "RS".
      { rewrite Nat.add_1_r seq_S -2!vPred_big_opL_fold
                big_sepL_app big_sepL_singleton. by iFrame "∗". }
      wp_op. iNext. wp_op => [Max|NMax].
      - iClear "IH". iNext. wp_let. iNext. wp_op => [? //|_]. iNext.
        iApply wp_if_false. iNext. wp_value. wp_seq. iNext.
        iApply (wp_mask_mono); first auto.
        iApply (dealloc with "[%] kI kS [$osc]"); first reflexivity.
        iNext. iViewUp. iIntros "kS _".
        iApply ("Post" with "[] kS [-]"); first done.

        rewrite (_: (j + 1 = max)%nat); last by lia.
        iFrame "WCP RS".
      - iNext. wp_let. iNext. wp_op => [_|? //]. iNext.
        iApply wp_if_true. iNext. rewrite -Nat.add_1_r.
        iApply ("IH" $! _ (j+1)%nat (n'') with 
                      "[%] [%] [%] WCP wT Post kS [osc] RS");
          [lia|omega|lia|by rewrite Nat2Z.inj_add].
    Qed.

    Lemma rcuSync_spec q Γ D1 D2 :
      lhd Γ = q → D1 ⊑ D2 →
      {{{{  RevokedUpTo P Γ D1 ∗ SnapshotValid P Γ D2 ∗ master Γ D2 }}}}
        rcuSync #q
      {{{{ RET #();
            RevokedUpTo P Γ D2 ∗ master Γ D2
        ∗ vPred_big_opL (seq 0 NPosn) (λ _ t, rToks Γ t (dead D2 ∖ dead D1))
          (* ([∗ list] t ∈ seq 0 NPosn, rToks Γ t (dead D2 ∖ dead D1)) *) }}}}.
    Proof.
      intros Hq Le. intros. iViewUp. iIntros "#kI kS (RV & #SN2 & MS) Post".
      iDestruct "RV" as (m) "(WCP & wT)".
      wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.
      rewrite Hq.
      iApply (GPS_SW_Read_ex (WCP P Γ) with "[%] kI kS [WCP]");
        [done|done|reflexivity|..].
      { iSplitR "WCP"; last iFrame "WCP".
        iNext. iIntros (? ?). iViewUp. iIntros "#?". by iFrame "#". }

      iNext. iViewUp. iIntros (v) "kS (WCP & (% & #SN1))". subst v.
      wp_let. iNext. wp_op. iNext. wp_let. iNext.
      wp_bind ([_]_at <- _)%E. wp_op. iNext.

      iApply (GPS_SW_ExWrite (WCP P Γ) (D2, (m + 1)%nat) True
                (λ s, [XP ZplusPos wc q in s | WCP P Γ ]_W)%VP
                with "[%] kI kS [$WCP]"); [|done|done|reflexivity|..].
      { right. split; [auto|simpl;omega]. }
      { iNext. iIntros (v'). iViewUp.
        iIntros "(_ & _ & $)". iModIntro. iNext. iFrame "SN2".
        iPureIntro. simpl. lia. }
      iNext. iViewUp. iIntros "kS (_ & WCP)".
      wp_seq. iNext.

      rewrite (_: Z.of_nat m + 1 = Z.of_nat (m + 1)%nat); last by lia.

      rewrite -vPred_big_opL_fold big_sepL_sepL 2!vPred_big_opL_fold.
      iDestruct "wT" as "(wT & #wS)".

      (* ([∗ list] t ∈ seq 0 NPosn,
                  wnToks Γ t (dead D2) ∗ wToks Γ t (dead D2 ∖ dead D1))%VP *)
      iAssert (vPred_big_opL (seq 0 NPosn)
              (λ _ t, wnToks Γ t (dead D2) ∗ wToks Γ t (dead D2 ∖ dead D1))%VP V_l)
        with "[wT]" as "wT".
      { iApply (big_sepL_mono with "wT").
        iIntros (???). by apply Tok_split_difference, rcu_dead_extend. }
      rewrite -(vPred_big_opL_fold _ (λ _ _, _ ∗ _)%VP)
              big_sepL_sepL 2!vPred_big_opL_fold.
      iDestruct "wT" as "[wnT wT]".

      iApply (rcuCollect_spec q Γ D1 D2 (Zpos n) (m + 1)%nat Hq Le
              with "[%] kI kS [$WCP wT]"); [lia|reflexivity|..].
      { rewrite -(vPred_big_opL_fold (seq _ (Z.to_nat _))) big_sepL_sepL.
        iSplitL "wT"; first done.
        iApply (big_sepL_mono with "wS"). iIntros (???) "?". by iExists _. }

      iNext. iViewUp. iIntros "kS [WCP rTS]".
      rewrite -(vPred_big_opL_fold (seq _ (Z.to_nat _))) big_sepL_sepL.
      iDestruct "rTS" as "[rT rS]".
      iApply ("Post" with "[] kS [WCP $MS $rT rS wnT]"); first auto.
      iExists (m+1)%nat. rewrite Hq. iFrame "WCP".
      rewrite -(vPred_big_opL_fold _ (λ _ _, _ ∗ _)%VP)
              big_sepL_sepL.
      by iSplitR "rS".
    Qed.

    Lemma rcuDealloc_spec q Γ D1 D2 D3 p:
      lhd Γ = q → D1 ⊑ D2 → D2 ⊑ D3 →
      {{{{  RevokedUpTo P Γ D1 ∗ DeallocBetween P Γ D1 D2
          ∗ SnapshotValid P Γ D3 ∗ master Γ D3 
          ∗ ∃ i γ, ■ (i ∈ dead D3 ∖ dead D2
                          ∧ info D3 i = Some (p,γ))
                   ∗ □ PermXList P Γ p i γ
                   ∗ ∃ s v, [nFXP (ZplusPos link p) in s@γ | LLP P Γ i]_W_nf 
                          ∗ (ZplusPos data p) ↦{nf} v }}}}
        rcuDealloc #q #(Zpos p)
      {{{{ (r: Z), RET #r;
            ∃ D', ■ (D1 ⊑ D' ∧ D' ⊑ D3) ∗ RevokedUpTo P Γ D'
                ∗ DeallocBetween P Γ D' D3 ∗ master Γ D3 }}}}.
    Proof.
      intros Hq Le1 Le2. intros. iViewUp.
      iIntros "#kI kS (RV & DB & #SN & MS & oi) Post".
      iDestruct "oi" as (i γ) "(% & Perms & oi)".
      destruct H as [Ini Infoi].
      iDestruct "oi" as (Li vi) "[oLi oDi]".
      iDestruct "DB" as (Ld vd) "(DF & od & SC & %)".
      destruct H as [NDp NDa].
      iDestruct "DF" as "[% DF]".

      assert (NIni: i ∉ (Ld.*2)).
      { specialize (H i).
        move : Ini H. rewrite 2!elem_of_difference.
        move => [_ In2] Ini In. by apply In2, Ini, elem_of_of_list. }
      iAssert (⌜p ∉ (Ld.*1)⌝)%I with "[oLi DF]" as %NInp.
      { iIntros (Inp).
        assert (Hj: ∃ j, (p,j) ∈ Ld).
        { apply elem_of_list_fmap in Inp as [[p' j] [Hp In]].
          subst p. by exists j. }
        destruct Hj as [j Hj]. rewrite -vPred_big_opL_fold.
        iDestruct (big_sepL_elem_of _ _ _ Hj with "DF") as "(oDj & oLj)".
        iDestruct "oLj" as (γj) "[oLj Pj]".
        iDestruct "oLj" as (s) "oLj".
        iApply (GPS_nFWP_writer_exclusive2
                  (IP1 := LLP P Γ i) (IP2 := LLP P Γ j) with "[$oLi $oLj]"). }


      wp_lam. iNext. wp_value. wp_let. iNext. wp_op. iNext.
      wp_bind ([_]_na)%E. rewrite Hq.
      iApply (na_read with "[%] kI kS od"); [done|reflexivity|].
      iNext. iViewUp. iIntros (z) "kS (% & od)". subst z.
      wp_op. iNext. wp_bind (_ #(Z.pos p))%E.

      iApply (push_spec with "[%] kI kS [$SC]");
        [reflexivity|iPureIntro;lia|].

      iNext. iViewUp. iIntros "kS SC". wp_let. iNext.

      set Ld' := (p,i) :: Ld.

      assert (ND1: NoDup (Ld'.*1)). { by apply NoDup_cons_2. }
      assert (ND2: NoDup (Ld'.*2)). { by apply NoDup_cons_2. }

      (* ([∗ list] a ∈ Ld', DeadFrom_elem P Γ (a.1) (a.2))%VP *)
      iAssert (vPred_big_opL Ld' (λ _ a, DeadFrom_elem P Γ (a.1) (a.2)) V_l)
        with "[DF oLi oDi Perms]" as "DF".
      { rewrite -2!vPred_big_opL_fold big_sepL_cons.
        iFrame "DF". iSplitL "oDi"; [by iExists _|].
        iExists _. iFrame "Perms". by iExists _. }

      assert (HD : of_list (Ld'.*2) ⊆ dead D3 ∖ dead D1).
      { move => j. rewrite elem_of_of_list fmap_cons elem_of_cons.
        move => [->|Inj].
        - move:Ini. rewrite /= 2!elem_of_difference.
          move => [? In2]. split; first auto.
          move => In1. by apply In2, (rcu_dead_extend _ _ Le1).
        - move: (H j). rewrite elem_of_of_list 2!elem_of_difference.
          move => In. destruct (In Inj) as [? In1]. split; last auto.
          by apply (rcu_dead_extend _ _ Le2). }

      wp_let. iNext. wp_op => [//|_]. iNext.
      iApply wp_if_false. iNext. wp_bind (_ #q)%E.

      iApply (rcuSync_spec q Γ D1 D3 Hq with "[%] kI kS [$RV $SN $MS]");
        [by etrans|reflexivity|].

      iNext. iViewUp. iIntros "kS (RV & MS & rToks)". wp_let. iNext.

      rewrite (_ : Z.pos p :: Z.pos ∘ fst <$> Ld = Z.pos ∘ fst <$> Ld');
        last done.

      move EqL2 : Ld' => L2.
      move EqL1: (nil: list (positive * aloc)) => L1.
      have EqLd': (L1 ++ L2)%list = Ld'. { by rewrite -EqL1. }
      rewrite (_: dead D1 = of_list (snd <$> L1) ∪ dead D1); last first.
      { by rewrite -EqL1 /= union_empty_l_L. }
      clear EqL2 EqL1.

      iLöb as "IH" forall (V_l L1 L2 EqLd') "SN ∗".

      wp_rec. iNext. wp_bind ([_]_na)%E. wp_op. iNext.
      iApply (na_read with "[%] kI kS od"); [done|reflexivity|].
      iNext. iViewUp. iIntros (z) "kS (% & od)". subst z.
      wp_op. iNext.
      wp_bind (_  #(Z.to_pos vd))%E.

      iApply (pop_spec with "[%] kI kS [$SC]"); first reflexivity.
      iNext. iViewUp. iIntros (z) "kS PR".

      wp_let. iNext. destruct L2 as [|[p0 i0] L2].
      { iClear "IH". iDestruct "PR" as "[% SC]".
        subst z. wp_op => [_|//]. iNext.
        iApply wp_if_true. iNext. wp_value. wp_let. iNext. wp_op => [//|_].
        iNext. iApply wp_if_false. iNext.
        wp_value.
        iApply ("Post" with "[%] kS [-]"); first done.
        iExists D3. iFrame "RV MS".
        iSplitL ""; first iPureIntro.
        { split; [by etrans|reflexivity]. }
        iExists nil, vd. rewrite Hq. iFrame "DF od SC".
        iSplit; iPureIntro; [done|split]; by apply NoDup_nil_2. }

      iDestruct "PR" as "(% & SC & %)". simpl in H0, H1. subst z.
      wp_op =>[//|_]. iNext.
      iApply wp_if_false. iNext.
      rewrite -{1}vPred_big_opL_fold big_sepL_cons vPred_big_opL_fold.
      iDestruct "DF" as "[DFi0 DF]".

      set L1' := (L1 ++ ((p0,i0) :: nil))%list.

      (* ([∗ list] t ∈ seq 0 NPosn,
                rTok Γ t i0
              ∗ rToks Γ t (dead D3 ∖ (of_list (L1'.*2) ∪ dead D1)))%VP *)
      iAssert (vPred_big_opL (seq 0 NPosn)
                          (λ _ t, rTok Γ t i0
              ∗ rToks Γ t (dead D3 ∖ (of_list (L1'.*2) ∪ dead D1)))%VP V_l)
        with "[rToks]" as "rToks".
      { iApply (big_sepL_mono with "rToks").
        intros.
        rewrite {1}/rToks.
        assert (HDi0: i0 ∈ Ld'.*2).
        { rewrite -EqLd' fmap_app elem_of_app. right. left. }
        rewrite (coPset_of_gset_difference_union (dead D3) {[i0]}).
        - rewrite -coPset_disj_union;
            last apply coPset_of_gset_difference_disjoint.
          rewrite -op_singleton. iIntros "[rTok $]".
          rewrite (_: {[i0]} ∪ (of_list (L1.*2) ∪ dead D1)
                      = of_list (L1'.*2) ∪ dead D1); first done.
          apply leibniz_equiv.
          move => j. rewrite fmap_app of_list_app !elem_of_union /=.
          set_solver+.
        - rewrite elem_of_disjoint.
          move => ? /elem_of_singleton ->.
          rewrite elem_of_union elem_of_of_list.
          move => [Ini0|InD].
          + move : ND2. rewrite -EqLd' fmap_app NoDup_app.
            move => [_ [NIn _]]. apply (NIn _ Ini0). left.
          + move : (HD i0). rewrite elem_of_difference.
            move => HDi. apply HDi; [by rewrite elem_of_of_list|auto].
        - move : (HD i0). rewrite elem_of_difference.
          move => HDi ? /elem_of_singleton ->.
          by apply HDi, elem_of_of_list. }

      rewrite -(vPred_big_opL_fold (seq _ _)) big_sepL_sepL 2!vPred_big_opL_fold.
      iDestruct "rToks" as "[ri0 rToks]".

      iAssert (|={⊤}=> ▷ ((∃ v : Z, ZplusPos data p0 ↦{1} v)
         ∗ (∃ s γ, [nFXP ZplusPos link p0 in s@γ | LLP P Γ i0 ]_W_1))%VP V_l)%I
        with "[DFi0 ri0]" as ">[oDi0 oLi0]".
      { iClear "IH". iDestruct "DFi0" as "(oD & oL)".
        iDestruct "oD" as (v) "oD".
        iDestruct "oL" as (γ0) "[oL #Perms]".
        iDestruct "oL" as (L0) "oL".

        (* ([∗ list] t ∈ seq 0 NPosn,
                              ▷ PermX'_right P (LLP P) Γ p0 t i0 γ0)%VP*)
        iAssert (|={⊤}=> (vPred_big_opL (seq 0 NPosn)
                          (λ _ t, ▷ PermX'_right P (LLP P) Γ p0 t i0 γ0))%VP V_l)%I
          with "[ri0]" as ">rDL".
        { iApply fupd_big_sepL.
          iApply (big_sepL_impl with "[ $ri0]"). iIntros "!#".
          iIntros (k t Eqk) "rT".
          apply lookup_seq_inv in Eqk as [Hk Eqj]. simpl in Hk. subst k.
          iDestruct ("Perms" $! t with "[%] [%]") as (β) "EX";
            [reflexivity|auto|]. simpl.
          iDestruct (exchange_elim_l (rTok Γ t i0)
                    (PermX'_right P (LLP P) Γ p0 t i0 γ0) β
            with "[] [%] [$EX $rT]") as "PR"; [|reflexivity|..].
          { iIntros (??) "_ rT". iApply (rcuTok_tid_exclusive with "rT"). }
          { iDestruct "EX" as (V) "(_ & ABS & _)". iNext. by iExists V. }
          iMod (fupd_mask_mono with "PR") as "PR"; first done.
          iModIntro. iNext.
          destruct (exchange_aSeen (rTok Γ t i0)
                      (PermX'_right P (LLP P) Γ p0 t i0 γ0) β) as [ES].
          iDestruct (ES with "EX") as "aS"; first reflexivity. clear ES.
          destruct (aSeen_elim (PermX'_right P (LLP P) Γ p0 t i0 γ0) β) as [ES].
          iApply (ES with "[$aS $PR]"); reflexivity. }

        iModIntro. iNext. rewrite  2!big_sepL_sepL.
        iDestruct "rDL" as "(_ & rD0 & rL0)".
        rewrite 2!vPred_big_opL_fold.
        iSplitL "rD0 oD".
        - iAssert (vPred_big_opL (seq 0 NPosn)
                          (λ _ _, ∃ v0, ZplusPos 1 p0 ↦{nf} v0)%VP V_l)%I
            with "[rD0]" as "rD0". (*  [∗ list] _ ∈ seq 0 NPosn,
                        (∃ v0, ZplusPos 1 p0 ↦{nf} v0)%VP*)
          { iApply (big_sepL_mono with "rD0").
            iIntros (???) "rD". iDestruct "rD" as (v0) "[_ rD]". by iExists _. }
          iDestruct (na_frac_mult_exists_joinL with "rD0") as (v0) "rD0".
          destruct (na_frac_join_1 (ZplusPos data p0) v v0 nf
                                   (Pos2Qp n * nf)) as [NF].
          iDestruct (NF with "[$oD $rD0]") as "oD"; first reflexivity.
          rewrite -Pos2Qp_plus_mult. by iExists _.
        - iDestruct (GPS_nFRP_exists_mult_joinL (IP:= LLP P Γ i0) with "rL0")
            as (s') "rL0".
          iDestruct (GPS_nFWP_join_writer_2 (IP:= LLP P Γ i0) with "[$oL $rL0]")
            as "oL".
          rewrite -Pos2Qp_plus_mult. by iExists _,_. }

      iDestruct "oDi0" as (v0) "oDi0".
      iDestruct "oLi0" as (L0 γ0) "oLi0".

      wp_op. iNext. wp_op. iNext.
      destruct (na_frac_dealloc top (ZplusPos 1 p0) v0) as [FA]; first done.
      iMod (FA with "oDi0") as "oDi0"; first reflexivity. clear FA.
      wp_bind (Dealloc _). iApply (wp_mask_mono); first auto.
      iApply (dealloc with "[%] kI kS oDi0"); first reflexivity.

      iNext. iViewUp. iIntros "kS _". wp_seq. iNext. wp_op. iNext.
      wp_op. iNext.
      iMod (GPS_nFWP_dealloc (IP:= LLP P Γ i0) with "oLi0") as "oLi0";
        first done.
      wp_bind (Dealloc _). iApply (wp_mask_mono); first auto.
      iApply (dealloc with "[%] kI kS oLi0"); first reflexivity.

      iNext. iViewUp. iIntros "kS _". wp_seq. iNext. wp_value.
      wp_let. iNext. wp_op => [_|//]. iNext. iApply wp_if_true. iNext.
      iApply ("IH" $! _ L1' L2 with
                      "[] [] Post od SC DF kS RV MS rToks");
        [iPureIntro|by iIntros "!#"].
      by rewrite /L1' -EqLd' app_assoc_reverse.
    Qed.

    Lemma WriterSafe_accessor_single q L L' p v :
      let Lc := (L ++ (p,v)::L')%list in
      let i := length L  in
      True ⊧ WriterSafe P q Lc
            -∗ ∃ Γ D La j γ, master Γ D
              ∗ TraceSpine_elem P Γ D Lc La i (p,v) j γ
              ∗ (master Γ D ∗ TraceSpine_elem P Γ D Lc La i (p,v) j γ
                 -∗ WriterSafe P q Lc).
    Proof.
      constructor. intros. iIntros "_". iViewUp. iIntros "Writer".
      iDestruct "Writer" as (Γ D1 D2 D3) "(F0 & F1 & F2 & Mas & SV & (F5 & TS) & F3)".
      iDestruct "TS" as (La) "[F6 TS]".
      iExists Γ, D3, (La.*1). iFrame "Mas".
      rewrite -vPred_big_opL_fold big_sepL_app big_sepL_cons
               !vPred_big_opL_fold -plus_n_O.
      iDestruct "TS" as "(oL & TS & oR)".
      iDestruct "TS" as (j γ) "TS". iExists j, γ. iFrame "TS".
      iViewUp. iIntros "(Mas & TSe)".
      iExists Γ, D1, D2, D3. iFrame "F0 F1 F2 Mas SV F5 F3".
      iExists La. iFrame "F6".
      rewrite -!vPred_big_opL_fold big_sepL_app big_sepL_cons -plus_n_O.
      iFrame "oL oR". by iExists j, γ.
    Qed.

    Lemma WriterSafe_accessor_double q L L' p v p' v' :
      let Lc := (L ++ (p,v)::(p',v')::L')%list in
      let i := length L  in
      True ⊧ WriterSafe P q Lc
            -∗ ∃ Γ D La j j' γ γ', master Γ D
              ∗ TraceSpine_elem P Γ D Lc La i (p,v) j γ
              ∗ TraceSpine_elem P Γ D Lc La (i + 1)%nat (p',v') j' γ'
              ∗ (master Γ D 
                  ∗ TraceSpine_elem P Γ D Lc La i (p,v) j γ
                  ∗ TraceSpine_elem P Γ D Lc La (i + 1)%nat (p',v') j' γ'
                 -∗ WriterSafe P q Lc).
    Proof.
      constructor. intros. iIntros "_". iViewUp. iIntros "Writer".
      iDestruct "Writer" as (Γ D1 D2 D3) "(F0 & F1 & F2 & Mas & SV & (F5 & TS) & F3)".
      iDestruct "TS" as (La) "[F6 TS]".
      iExists Γ, D3, (La.*1). iFrame "Mas".
      rewrite -vPred_big_opL_fold big_sepL_app !big_sepL_cons
               !vPred_big_opL_fold -plus_n_O.
      iDestruct "TS" as "(oL & TS1 & TS2 & oR)".
      iDestruct "TS1" as (j1 γ1) "TS1". iDestruct "TS2" as (j2 γ2) "TS2".
      iExists j1, j2, γ1, γ2. iFrame "TS1 TS2".
      iViewUp. iIntros "(Mas & TS1 & TS2)".
      iExists Γ, D1, D2, D3. iFrame "F0 F1 F2 Mas SV F5 F3".
      iExists La. iFrame "F6".
      rewrite -!vPred_big_opL_fold big_sepL_app !big_sepL_cons -plus_n_O.
      iFrame "oL oR".
      iSplitL "TS1"; by iExists _,_.
    Qed.

    Lemma TraceSpine_elem_link_read Γ D Lc La (i: nat) p v j γ :
      {{{{ TraceSpine_elem P Γ D Lc La i (p,v) j γ }}}}
        [ (loc) (#p + #link) ]_at
      {{{{(p':Z), RET #p';
            TraceSpine_elem P Γ D Lc La i (p,v) j γ
            ∗ LLP P Γ j γ false (Z.to_pos p) (hist D j) p' }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS TS Post".
      wp_op. iNext. wp_op. iNext.
      iDestruct "TS" as "(% & [(% & XP)|(% & (TD & FXP) & Perm)])".
      - iApply (GPS_nSW_ExRead (LLP P Γ j) (γ:=γ) Γ with "[%] kI kS [$XP]");
            [auto|auto|reflexivity|..].
        { iNext. iIntros (? p'). iViewUp. iIntros "#?". by iFrame "#". }
        iNext. iViewUp. iIntros (p') "kS (XP & LLP)".
        rewrite -Zplus_0_r_reverse.
        iApply ("Post" with "[] kS [XP $LLP]"); first done.
        iFrame (H). iLeft. rewrite /Trace_own_head -Zplus_0_r_reverse.
        iFrame (H0) "XP". 
      - iApply (GPS_nFWP_ExRead (LLP P Γ j) γ with "[%] kI kS [$FXP]");
            [auto|auto|reflexivity|..].
        { iNext. iIntros (? p'). iViewUp. iIntros "#?". by iFrame "#". }
        iNext. iViewUp. iIntros (p') "kS (FXP & LLP)".
        rewrite -Zplus_0_r_reverse.
        iApply ("Post" with "[] kS [TD FXP $LLP Perm]"); first done.
        iFrame (H). iRight. rewrite /Trace_own_tail -Zplus_0_r_reverse.
        iFrame (H0) "TD FXP Perm".
    Qed.

    Lemma rcuWriter_read_link_raw Γ D L L' La p v:
      {{{{ master Γ D ∗ TraceSpineAbs P Γ D (L ++ (p,v)::L') La }}}}
        [ (loc) (#p + #link) ]_at
      {{{{(p':Z), RET #p';
          master Γ D ∗ TraceSpineAbs P Γ D (L ++ (p,v)::L') La
          ∗ ■ (p' = 0 ∧ L' = nil ∨ 0 < p' ∧ ∃ L'' v', L' = (p',v') :: L'')
          ∗ ∃ j γ,
            ■ Trace_link_fact D (L ++ (p,v)::L') (La.*1) p (length L) j γ
             ∗ LLP' P (LLP P) Γ j γ false (Z.to_pos p) (hist D j) p' }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS [MS [% TS]] Post".
      rewrite -vPred_big_opL_fold big_sepL_app big_sepL_cons 2!vPred_big_opL_fold.
      iDestruct "TS" as "(T1 & TE & T2)".
      iDestruct "TE" as (j γ) "(% & TE)".
      destruct H as (EqLa&ND).
      destruct (fmap_app_inv _ _ _ _ EqLa) as [L2 [L3 [EqL2 [EqL3 EqLa']]]].
      symmetry in EqL3.
      destruct (fmap_cons_inv _ _ _ _ EqL3) as [p4 [L4 [Eqp4 [EqL4 EqL4']]]].
      rewrite EqL4' in EqLa'. clear EqL4' EqL3.
      iApply (TraceSpine_elem_link_read with "[%] kI kS [$TE]");
          [done|by iFrame (H0)|].
      iNext. iViewUp. iIntros (p') "kS (TE & #LLP)".
      iApply ("Post" with "[] kS [-]"); first done.
      rewrite LLP_unfold.
      iPoseProof ("LLP") as "LLP'".
      iDestruct "LLP'" as (Dj) "(SNj & % & LLP')".
      iDestruct (master_wellformed with "MS") as %W.
      iDestruct (master_dictates_snapshot with "[$SNj $MS]") as %Dle.
      destruct H as [H01 H02].
      assert (H03 := RCUData_info_lookup _ _ _ _ W Dle H02).
      rewrite Nat.add_0_r in H0.
      destruct (hist D j) as [|bj Lj] eqn:HLj; first by iDestruct "LLP'" as %?.
      iFrame "MS".
      destruct L4 as [|[p1 v1] L4'] eqn: HL4; simpl in EqL4; subst L'.
      - iSplitR ""; last iSplitR "".
        + iSplitL ""; first done.
          rewrite -(vPred_big_opL_fold (_ ++ _::_))
                big_sepL_app big_sepL_cons.
          iFrame "T1 T2". by iExists _,_.
        + destruct H0 as (_&_&_&H1&_).
          rewrite app_length HLj in H1.
          rewrite (_: bj = Null); last first.
          { specialize (H1 eq_refl). by inversion H1. }
          iDestruct "LLP'" as %?. by iLeft.
        + iExists j,_; rewrite HLj; iFrame (H0) "LLP".
      - rewrite -(vPred_big_opL_fold (_::_)) big_sepL_cons.
        iDestruct "T2" as "[TF T2]". iDestruct "TF" as (j' γ') "[% TF]".
        iSplitR ""; last iSplitR "".
        + iSplitL ""; first done.
          rewrite -(vPred_big_opL_fold (_ ++ _::_))
                big_sepL_app 2!big_sepL_cons.
          iFrame "T1 T2". iSplitL "TE"; iExists _,_; by iFrame "∗".
        + destruct H0 as (_&_&_&_&H1).
          rewrite app_length in H1.
          destruct H1 as [j2 [HLaj HHj]]. { simpl. omega. }
          rewrite HLj in HHj. inversion HHj. iDestruct "LLP'" as (γ2) "(% & _)".
          iRight. iPureIntro. destruct H0 as [H0 H0'].
          split; first omega.
          exists (L4'.*2), (v1.2). f_equal.
          destruct H as (H23&H21&H22&_). rewrite H21 in HLaj.
          inversion HLaj. subst j2.
          rewrite (RCUData_info_lookup _ _ _ _ W Dle H0') in H22.
          inversion H22.
          apply Z2Pos.inj in H2; [subst p'; by destruct v1|omega|auto].
        + iExists j,_; rewrite HLj; iFrame (H0) "LLP".
    Qed.

    Lemma rcuWriter_read_link q L L' p v :
      {{{{  WriterSafe P q (L ++ (p,v)::L')%list }}}}
        [ (loc) (#p + #link) ]_at
      {{{{(p':Z), RET #p';
            WriterSafe P q (L ++ (p,v)::L')%list
          ∗ ■ (p' = 0 ∧ L' = nil ∨ 0 < p' ∧ ∃ L'' v', L' = (p',v') :: L'') }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS WS Post".
      iDestruct "WS" as (Γ D1 D2 D3) "(% & RV & DB & MS & SN & TS & Rem)".
      iDestruct "TS" as "[TH TS]".
      iDestruct "TS" as (La) "TS".
      iApply (rcuWriter_read_link_raw with "[%] kI kS [$MS $TS]"); first done.
      iNext. iViewUp. iIntros (p') "kS (MS & TS & FT & _)".
      iApply ("Post" with "[%] kS [-]"); first done.
      iSplitR "FT"; last done.
      iExists Γ, D1, D2, D3. iFrame (H) "RV DB MS SN TH Rem".
      by iExists _.
    Qed.

    Lemma rcuWriter_read_data q L L' p v : L ≠ nil →
      {{{{  WriterSafe P q (L ++ (p,v)::L')%list }}}}
        [ (loc) (#p + #data) ]_na
      {{{{(x:Z), RET #x;
            WriterSafe P q (L ++ (p,v)::L')%list ∗ ■ (x = v) }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS WS Post".
      destruct (WriterSafe_accessor_single q L L' p v) as [Acc].
      iDestruct (Acc with "[] [] WS")
        as (Γ D La j γ) "(MS & (% & TS) & HClose)"; [done|done|done|]. clear Acc.
      iDestruct "TS" as "[(% & _)| (% & (TD & TL) & Perm)]".
      - exfalso. apply length_zero_iff_nil in H1. done.
      - wp_op. iNext. wp_op. iNext.
        iApply (na_read_frac with "[%] kI kS TD"); [done|done|done|].
        iNext. iViewUp. iIntros (x) "kS (Eq & TD)".
        iApply ("Post" with "[] kS [-]"); first done.
        iFrame "Eq".
        iApply ("HClose" with "[] [$MS TL TD Perm]"); [done|].
        iFrame (H0). iRight. iFrame (H1) "TL TD Perm".
    Qed.

    Lemma rcu_escrows_alloc Γ x s i γ v V:
      (□ P v ∗
          vPred_big_opL (seq 0 NPosn)
            (λ _ t, tTok Γ t i ∗ ZplusPos data x ↦{nf} v
              ∗ [nFXP ZplusPos link x in s @ γ | LLP P Γ i ]_R_(nf)))%VP V
          (* [∗ list] t ∈ seq 0 NPosn,
          tTok Γ t i ∗ ZplusPos data x ↦{nf} v
          ∗ [nFXP ZplusPos link x in s @ γ | LLP P Γ i ]_R_(nf))%VP V *)
      ={↑escrowN}=∗
        vPred_big_opL (seq 0 NPosn) (λ _ t, PermX P Γ x t i γ)%VP V.
        (* ([∗ list] t ∈ seq 0 NPosn, PermX P Γ x t i γ)%VP V *)
    Proof.
      iIntros "[#P OL]". iApply fupd_big_sepL.
      iApply (big_sepL_impl with "[ $OL]"). iIntros "!#".
      iIntros (? t ?) "(tTok & oD & oL)".
      destruct (exchange_alloc_r (rTok Γ t i)
                                (PermX'_right P (LLP P) Γ x t i γ)) as [ESC].
      iApply ESC; first reflexivity.
      iNext. iFrame "tTok". iSplitL "oD".
      - iExists _. iFrame "oD". by iIntros "!#".
      - by iExists _.
    Qed.


    Lemma rcuNodeUpdate_link_write Γ D D' L L' La Ln p v x vx ix i γ j' p' v' γ'
      (HIi: info D' i = Some (Z.to_pos p, γ))
      (HHi: hist D' i = Abs j' :: hist D i)
      (HI: p' > 0 ∧ info D' j' = Some (Z.to_pos p', γ'))
      (EqLa : (L ++ (p, v) ::nil)%list = La.*2) (EqLn : L' = Ln.*2)

      (DLe: D ⊑ D') (UW: uwellformed D'):
      let L1 := (L ++ (p, v) :: (p', v') :: L')%list in
      let L1' := (La ++ (j', (p',v')) :: Ln)%list in
      {{{{ □ PermXList P Γ (Z.to_pos p') j' γ'
           ∗ snapshot Γ D'
           ∗ TraceSpine_elem P Γ D (L ++ (p, v) :: (x, vx) :: L')
                                   ((La ++ (ix, (x,vx)) :: Ln)%list.*1)
                                   (length L) (p, v) i γ }}}}
        [ #(Z.to_pos (p + link)) ]_at <- #p'
      {{{{ RET #();
            TraceSpine_elem P Γ D' L1 (L1'.*1) (length L) (p, v) i γ
           ∗ PrtSeen γ (hist D' i)
           ∗ □ (■ (length L = 0)%nat
              -∗ [nXP Z.to_pos (p + link) in hist D' i @ (Γ,γ) | LLP P Γ i ]_R) }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS (Perm & SN & (% & TS)) Post".
      assert (HLen: (length L + 1 = length (La.*1))%nat).
      { rewrite fmap_length -(fmap_length snd La) -EqLa app_length /=. omega. }
      have HLen2: (length L < length (La.*1))%nat by omega.
      assert (TF': Trace_link_fact D' L1 (L1'.*1) p (length L) i γ).
      { destruct H as (TF1 & TF2 & TF4 & TF5 & TF6). split; [auto|repeat split].
        - move : TF2. by rewrite 2!fmap_app 2!(lookup_app_l _ _ _ HLen2).
        - by rewrite HIi.
        - rewrite HHi /L1 app_length /=. omega.
        - intros _. exists j'. rewrite HHi. split; last done.
          rewrite fmap_app. by apply list_lookup_middle. }
      iDestruct "TS" as "[[% oL]|(% & (oD & oL) & Perms)]".
      - rewrite /Trace_own_head.
        set Q : gname → pr_state RCULinkProtocol → vPred := λ γ s',
          [nXP Z.to_pos (p + link) in s' @ (Γ,γ) | LLP P Γ i]_W.
        iApply (GPS_nSW_ExWrite (LLP P Γ i) (γ:= γ) Γ (hist D' i) True Q
                  with "[] kI kS [$oL SN Perm]");
          [rewrite HHi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "(_ & _ & $)".
          iModIntro. iNext. rewrite 2!LLP_unfold.
          iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HHi Zplus_0_r. iSplitL ""; first done.
          iExists γ'. iFrame (HI) "Perm". }

        iNext. iViewUp. iIntros "kS (oR & oL)". rewrite /Q.
        iApply ("Post" with "[] kS [oL oR]"); first auto.
        iDestruct (GPS_nSW_Reader_PrtSeen (IP := LLP P Γ i) γ _ Γ with "oR")
          as "#PSeen".
        iFrame (TF'). iSplitR "oR".
        + iLeft. iFrame (H0) "oL".
        + iSplitL ""; first done.
          iDestruct "oR" as "#oR". iIntros "!#". iViewUp. by iIntros "_".
      - iApply (GPS_nFWP_ExWrite (LLP P Γ i) γ (hist D' i) True (λ _ , True)%VP
                  with "[] kI kS [$oL SN Perm]");
          [rewrite HHi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "_".
          iModIntro. iSplitL ""; first auto. iNext.
          rewrite 2!LLP_unfold. iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HHi Zplus_0_r. iSplitL ""; first done.
          iExists γ'. iFrame (HI) "Perm". }

        iNext. iViewUp. iIntros "kS (oL & _)".
        iApply ("Post" with "[] kS [oL oD Perms]"); first auto.
        destruct (GPS_nFWP_PrtSeen (IP := LLP P Γ i) γ
                  (Z.to_pos (p + link)) (hist D' i) nf) as [PSeen].
        iDestruct (PSeen with "oL") as "#PSeen"; first reflexivity. clear PSeen.
        iFrame (TF'). iSplitR "".
        + iRight. iFrame (H0) "oD oL Perms".
        + iSplitL ""; first done. iIntros "!#". iIntros (??) "%". exfalso. omega.
    Qed.

    Lemma rcuNodeUpdate_spec q L L' p v0 x v1 v':
      {{{{ WriterSafe P q (L ++ (p,v0)::(x,v1)::L')%list ∗ □ P v' }}}}
        rcuNodeUpdate #q #x #p #v'
      {{{{ (x': Z), RET #x';
            WriterSafe P q (L ++ (p,v0)::(x',v')::L')%list }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS (WS & #P) Post".
      wp_lam. iNext. wp_value. wp_lam. iNext. wp_value. wp_lam. iNext.
      wp_value. wp_lam. iNext.
      wp_bind ([_]_at)%E.
      rewrite (_: L ++ (p, v0) :: (x, v1) :: L'
                  = (L ++ ((p,v0)::nil)) ++ (x,v1) :: L')%list; last first.
      { by rewrite -app_assoc. }

      iDestruct "WS" as (Γ D1 D2 D3)
                        "(% & F1 & F2 & F3 & F4 & [F5 TS] & % & F6)".
      iDestruct "F5" as %HHd.
      iDestruct "TS" as (La) "TS".
      iApply (rcuWriter_read_link_raw with "[%] kI kS [$F3 $TS]");
        first reflexivity.
      iNext. iViewUp. iIntros (p') "kS (MS & TS & % & LLPx)". wp_let. iNext.
      iDestruct "TS" as "[F5 TS]".
      iDestruct "F5" as %(EqLa & ND & BL' & ALive).

      rewrite -vPred_big_opL_fold 2!big_sepL_app
              big_sepL_singleton big_sepL_cons 2!vPred_big_opL_fold.
      iDestruct "TS" as "((TS & pi) & xi & Last)".
      iDestruct "pi" as (i γ) "[% Ti]".
      iDestruct "xi" as (ix γx) "Tx".
      iDestruct "Tx" as "[% [[% _]|[_ [Tx #Perms]]]]".
      { rewrite app_length /= in H4. omega. }

      iDestruct "LLPx" as (ix' γx') "[% LLPx]". rewrite Nat.add_0_r in H3.
      destruct (Trace_link_fact_same _ _ _ _ _ _ _ _ _ _ H3 H4) as (_&?&?).
      subst ix' γx'. clear H4.
      iDestruct "LLPx" as (Dx) "(SNx & % & LLPx)".
      destruct (hist D3 ix) as [|bj Lj] eqn:HHbj; first by iDestruct "LLPx" as %?.

      assert (HLi: i ∈ live D3).
      { apply ALive, (elem_of_list_lookup_2 _ (length L + 0)%nat), H2. }
      assert (HDi := elem_of_live'_dom _ _ HLi).
      iAssert (⌜wellformed D3⌝)%I with "[MS]" as %W.
      { iDestruct (master_wellformed with "MS") as %W.
        iPureIntro. by apply (RCUData_uwellformed_wellformed _ i W). }
      iDestruct (master_dictates_snapshot with "[$SNx $MS]") as %Dle.
      assert (HHi: hd_error (hist' (D3.1) i) = Some (Abs ix)).
      { destruct H2 as (H11 & H12 & H13 & H14 & [j' [H15 H16]]).
        - rewrite 2!app_length /=. omega.
        - destruct H3 as (H21 & H22 & H23). rewrite Nat.add_0_r in H15.
          rewrite app_length /= H15 in H22. inversion H22. by subst j'. }
      assert (HHix : hd_error (hist' (D3.1) ix) = Some bj).
      { rewrite /hist in HHbj. by rewrite HHbj. }
      assert (HLix : ix ∈ live D3).
      { destruct W as (_&_&_&_&_&LC).
        destruct (LC _ HLi) as [j2 [Eqj2 Eq]]. rewrite HHi in Eqj2.
        inversion Eqj2. subst j2. move : Eq => [//|[? [[->]]] //]. }
      assert (HDix := elem_of_live'_dom _ _ HLix).
      assert (Hix : i ≠ ix).
      { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H2 H3 ND).
        rewrite app_length /=. omega. }

      iAssert ((■ (bj = Null ∧ p' = 0)
          ∨ ∃ j γj, ■ (bj = Abs j ∧ 0 < p' ∧ info D3 j = Some (Z.to_pos p', γj))
              ∗ □ PermXList P Γ (Z.to_pos p') j γj)%VP V_l)%I
        with "[LLPx]" as "RSj".
      { destruct bj as [|j|]; [iLeft|iRight|by iDestruct "LLPx" as %?].
        - by iDestruct "LLPx" as %?.
        - iDestruct "LLPx" as (γj) "[% #Perm]".
          iExists j, γj. iSplit; [iPureIntro| by iIntros "!#"].
          destruct H5. repeat split; [omega|].
          rewrite (RCUData_info_lookup _ _ _ _ _ Dle H6); [done|by right]. }

      destruct (fmap_app_inv _ _ _ _ EqLa) as [L2 [L3 [EqL2 [EqL3 EqLa']]]].
      symmetry in EqL3.
      destruct (fmap_cons_inv _ _ _ _ EqL3) as [p4 [L4 [Eqp4 [EqL4 EqL4']]]].
      rewrite EqL4' in EqLa'. clear EqL4' EqL3.

      iAssert (⌜bj = Null ∨ ∃ j, bj = Abs j⌝)%I with "[RSj]" as %Hbj.
      { iDestruct "RSj" as "[[% _]|RSj]"; [by iLeft|iRight].
        iDestruct "RSj" as (j γj) "[[% _] _]". by iExists j. }

      (* allocation space for new node *)
      iClear "SNx". wp_bind (malloc _).
      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].

      iNext. iViewUp. iIntros (x') "kS oLs".
      rewrite -(vPred_big_opL_fold (seq _ _)) big_sepL_cons big_opL_singleton.
      iDestruct "oLs" as "(oL & oD)".
      wp_let. iNext. wp_op. iNext.

      (* init write for new node data *)
      wp_bind ([_]_na <- _)%E.
      iApply (na_write_frac_1 with "[] kI kS oD"); [done|done|].
      iNext. iViewUp. iIntros "kS oD".
      wp_seq. iNext. wp_op. iNext.

      wp_bind ([_]_na <- _)%E.

      (* setting up new history *)
      set j' : aloc := fresh (dom _ D3).
      set D3' : gname -> RCUData
        := λ γ', ((i, Abs j')::(j', bj)::(ix, Dead)::D3.1, (j',x',γ')::D3.2).
      set L1 := (L ++ (p, v0) :: (Z.pos x', v') :: L')%list.
      set L1' := (L2 ++ (j', (Z.pos x', v')) :: L4)%list.
      assert (HL1: L1 = L1'.*2).
      { by rewrite fmap_app fmap_cons -EqL2 -EqL4  /= (app_assoc_reverse L). }

      assert (HDisj': ∀ k, k ∈ dom (gset _) D3 → k ≠ j').
      { move => k ??. subst k. by apply (is_fresh (dom (gset _) D3)). }
      assert (i ≠ j'). { by apply HDisj'. }
      assert (ix ≠ j'). { by apply HDisj'. }
      assert (HNj': (j' ∉ (L2 ++ L4).*1)%list).
      { move => Hj'. apply (is_fresh (dom (gset _) D3)).
        apply elem_of_live'_dom, ALive.
        move : Hj'. rewrite EqLa' 2!fmap_app fmap_cons 2!elem_of_app.
        move => [?|?]; [by left|by right;right]. }
      assert (HIj: ∀ γ', info (D3' γ') j' = Some (x', γ')).
      { intros ?. by rewrite /info /info' list_filter_cons. }
      assert (BE := rcu_base_update (D3.1) i ix bj HHi HHix Hbj).
      assert (ED3': ∀ γ', D3 ⊑ (D3' γ')).
      { intros ?. split; simpl.
        - right. split; [by repeat apply suffix_cons_r|done].
        - by apply suffix_cons_r. }
      assert (HHj' : ∀ γ', hist (D3' γ') j' = bj :: nil).
      { intros ?. rewrite /hist. rewrite rcu_hist'_cons_next; last auto.
        rewrite rcu_hist'_cons_now. f_equal.
        apply rcu_hist'_nil. move => Hj'.
        apply (is_fresh (dom (gset _) D3)).
        move : Hj'. by rewrite (rcu_dom'_cons_in _ _ _ HDix). }

      iAssert (⌜∀ j : aloc, bj = Abs j →
                  ∃ γj,
              Trace_link_fact D3 ((L ++ ((p, v0)::nil)) ++ (x, v1) :: L')%list
                             (La.*1) p' (length (L ++ (p, v0)::nil) + 1) j γj⌝)%I
              with "[Last RSj]" as %HHj.
      { iIntros (j Eqj).
        iDestruct "RSj" as "[[% _]|RSj]"; first by subst bj.
        iDestruct "RSj" as (j2 γj) "[% _]". destruct H7 as (? & Hp' & Infp').
        subst bj. inversion H7. subst j2. clear H7.
        destruct H1 as [[? ]|[_ [L'' [v'' HL']]]]; first omega. iExists γj.
        rewrite HL' -vPred_big_opL_fold big_sepL_cons.
        iDestruct "Last" as "[Last _]".
        iDestruct "Last" as (j3 γ3) "[% _]". iPureIntro.
        destruct (H1) as (_&Hj3&Hj3'&_). destruct H3 as (_&_&_&_&H3).
        destruct H3 as [j4 [Hj41 Hj42]].
        { rewrite (app_length _ ((x, v1) :: _)) HL' /=. omega. }
        rewrite Hj3 in Hj41. inversion Hj41. subst j4. clear Hj41.
        rewrite HHix in Hj42. inversion Hj42. subst j3.
        rewrite Infp' in Hj3'. by inversion Hj3'. }

      assert (Hj : ∀ j , bj = Abs j → i ≠ j ∧ ix ≠ j ∧ j ∈ live D3).
      { move => j. specialize (HHj j).
        move :Hbj => [->//|[j2 Eqj2] Eqj3]. rewrite Eqj3 in Eqj2.
        inversion Eqj2. subst j2.
        destruct (HHj Eqj3) as [γ2 TF]. repeat split.
        - apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H2 TF ND).
          rewrite app_length /=. omega.
        - apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H3 TF ND).
          rewrite app_length /=. omega.
        - destruct W as (_&_&_&_&_&LC).
          destruct (LC _ HLix) as [j4 [Eqj4 Eq]]. rewrite HHix Eqj3 in Eqj4.
          inversion Eqj4. subst j4.
          move : Eq => [//|[? [[->]]] //]. }

      assert (Hjj' : ∀ j , bj = Abs j → j ≠ j').
      { move => j /Hj [_ [_ Live]].
        apply HDisj', (elem_of_live'_dom _ _ Live). }

      assert (SD3': ∀ γ', SinglePtr (D3' γ')).
      { intros ?. apply (rcu_SinglePtr_update (D3.1)); [apply W|..]; auto. }
      assert (WD3': ∀ γ', wellformed (D3' γ')).
      { intros ?. apply RCUData_wellformed_update; auto. apply ED3'. }
      assert (UD3' : ∀ γ', uwellformed (D3' γ')). { by right. }

      assert (HID : ∀ γ' k, k ≠ j' → info (D3' γ') k = info D3 k).
      { move => ???. by rewrite /info /info' list_filter_cons_not. }

      iDestruct "RSj" as "#RSj".
      (* init write for new node link *)
      iApply (GPS_nFWP_Init_strong (LLP P Γ j') (λ γ', hist (D3' γ') j')
               (λ γ', master Γ (D3' γ') ∗ snapshot Γ (D3' γ'))%VP
          with "[%] kI kS [$oL MS]"); [auto|reflexivity|..].
      { iIntros (γ').
        iSplitL "".
        - iModIntro. iNext. by rewrite LLP_unfold.
        - (* update global ghost history *)
          iMod (master_update Γ D3 (D3' γ') with "MS") as "[MS' #SN']";
            [apply UD3'|apply ED3'|].
          iFrame "MS' SN'".
          iModIntro. iNext. rewrite LLP_unfold.
          iExists (D3' γ'). iFrame "SN'". rewrite HHj'. iSplit; first done.
          iDestruct "RSj" as "[[% %]|RSj]"; first by subst bj.
          iDestruct "RSj" as (j γj) "[[% %] Perm]". subst bj.
          iExists γj. iFrame "Perm". rewrite (HID γ' j).
          + iPureIntro. destruct H8. split; [omega|auto].
          + move => ?. by apply (Hjj' j). }

      iNext. iViewUp. iIntros (γ') "kS [[MS' #SN'] jXP']".
      destruct (GPS_nFWP_PrtSeen (IP := LLP P Γ j') γ'
                  (ZplusPos 0 x') (hist (D3' γ') j') 1%Qp) as [PSeen].
      iDestruct (PSeen with "jXP'") as "#PSeen'"; first reflexivity.
      clear PSeen.
      wp_seq. iNext. wp_bind ([_]_at <- _)%E.

      specialize (HIj γ'). specialize (ED3' γ'). specialize (SD3' γ').
      specialize (WD3' γ'). specialize (UD3' γ'). specialize (Hj γ').
      specialize (HID γ'). specialize (HHj' γ').

      assert (Live': i ∈ live (D3' γ')).
      { apply (elem_of_rcu_live'_mono
                _ ((i, Abs j')::(j', bj)::(ix, Dead)::nil)); last auto.
         move =>? /elem_of_cons [[-> ]//|].
         move => /elem_of_cons [[//]|/elem_of_list_singleton[//]]. }
      assert (HHi' : hist (D3' γ') i = Abs j' :: hist D3 i).
      { rewrite /hist rcu_hist'_cons_now. f_equal.
        rewrite rcu_hist'_cons_next; last auto.
        by rewrite rcu_hist'_cons_next. }
      assert (Hxi' : hist' ((D3' γ').1) ix = Dead :: hist D3 ix).
      { do 2 (rewrite rcu_hist'_cons_next; last auto).
        by rewrite rcu_hist'_cons_now. }
      assert (HDxi : ix ∈ dead (D3' γ')).
      { apply elem_of_rcu_dead'. rewrite Hxi'. left. }

      assert (HHD : ∀ k, k ≠ i → k ≠ ix  → k ≠ j'
                    → hist (D3' γ') k = hist D3 k).
      { move => ????. do 2 (rewrite /hist rcu_hist'_cons_next; last auto).
        by rewrite rcu_hist'_cons_next. }
      assert (HD3: dom (gset _) (D3' γ') ≡ {[j']} ∪ dom (gset _) D3).
      { rewrite /dom !/RCUData_dom rcu_dom'_cons_in.
        - rewrite rcu_dom'_cons rcu_dom'_cons_in; auto.
        - by apply (elem_of_rcu_hist_dom_mono (_::_::nil)). }
      assert (HDix': ix ∈ dead (D3' γ')).
      { apply elem_of_rcu_dead'. rewrite Hxi'. by left. }
      assert (HLj': j' ∈ live (D3' γ')).
      { apply elem_of_rcu_live'_hist'.
        rewrite /dom /RCUData_dom in HD3. rewrite /hist in HHj'.
        rewrite HD3 elem_of_union elem_of_singleton
                HHj' elem_of_list_singleton.
        split; first by left. move : Hbj => [-> //|[? ->] //]. }
      assert (HL3: live (D3' γ') = {[j']} ∪ live D3 ∖ {[ix]}).
      { apply leibniz_equiv.
        move => k.
        rewrite elem_of_union elem_of_difference 2!elem_of_singleton.
        case (decide (i = k)) => [?|NEqk1].
        { subst k. split; [by right|auto]. }
        case (decide (j' = k)) => [?|NEqk2].
        { subst k. split; [by left|auto]. }
        rewrite /live /= -elem_of_rcu_live'_further; last done.
        rewrite -elem_of_rcu_live'_further; last done. split; last first.
        - move => [//|[InL NEq]].
          apply (elem_of_rcu_live'_mono _ (_::nil)); last done.
          move => ?. rewrite elem_of_list_singleton. move => [//].
        - move => Hk. right. by apply rcu_live'_cons_Dead. }

      (* allocate escrows for new node *)
      destruct (GPS_nFWP_mult_splitL_1
        (IP := LLP P Γ j') γ' (ZplusPos 0 x') (hist (D3' γ') j') n) as [Split].
      iDestruct (Split with "jXP'") as "[jWP' jRP']"; first reflexivity.
      clear Split.

      iDestruct (rcu_remain_txtokens_one Γ D3 (D3' γ') _ j' HD3 with "F6")
         as "(F6 & tTok)".
      { by apply is_fresh. }

      destruct (na_frac_mult_splitL_1 (ZplusPos data x') v' (n + 1)) as [Split].
      iDestruct (Split with "oD") as "oD"; first reflexivity. clear Split.
      rewrite (_: Pos.to_nat (n + 1) = S $ NPosn); last lia.
      rewrite seq_S.
      iAssert ((ZplusPos data x' ↦{nf} v')%VP V_l
        ∗ vPred_big_opL (seq 0 NPosn) (λ _ _, ZplusPos data x' ↦{nf} v')%VP V_l)%I
           (*([∗ list] _ ∈ seq 0 NPosn, ZplusPos data x' ↦{nf} v')%VP V_l)%I *)
        with "[oD]" as "[oDW oDR]".
      { rewrite -{1}vPred_big_opL_fold big_sepL_app
                big_sepL_singleton vPred_big_opL_fold.
        by iDestruct "oD" as "[$ $]". }
       iCombine "tTok" "oDR" as "OE". iCombine "OE" "jRP'" as "OE".

      iDestruct (rcu_escrows_alloc Γ x' (hist (D3' γ') j') j' γ' v'
                    with "[P OE]") as "OE".
      { iSplitL ""; first by iIntros "!#".
        rewrite -2!big_sepL_sepL.
        iApply (big_sepL_mono with "OE"). iIntros (???) "(($ & $) & $)". }

      iMod (fupd_mask_mono with "OE") as "OE"; first done. iClear "P".

      iAssert ((□ PermXList P Γ x' j' γ')%VP V_l) with "[OE]" as "OE".
      { iDestruct "OE" as "#OE".
        iIntros "!#". iIntros (t). iViewUp. iIntros (Ht).
        rewrite -vPred_big_opL_fold.
        iApply (big_sepL_elem_of with "OE"). by apply seq_elem_of. }
      iDestruct "OE" as "#OE".

      assert (HL2: (length (L2.*1) = length L + 1)%nat).
      { rewrite fmap_length -(fmap_length snd L2) -EqL2 app_length //. }

      (* add TraceSpine of new node *)
      iAssert (TraceSpine_elem P Γ (D3' γ') L1 (L1'.*1)
                                (length L + 1)%nat (Zpos x',v') j' γ' V_l)%I
        with "[jWP' oDW]" as "TSj".
      { iSplit.
        - iSplit; [auto|repeat iSplit].
          + iPureIntro. rewrite fmap_app /=.
            by apply (list_lookup_middle (L2.*1)).
          + by rewrite Pos2Z.id.
          + rewrite app_length HHj' /=.
            destruct H1 as [[? HL']|[? [L'' [v'' HL']]]]; rewrite HL'.
            * iDestruct "RSj" as "[[% _]|OS]"; first by subst bj.
              iDestruct "OS" as (??) "((_ & % & _) &_)". by omega.
            * iIntros (F). simpl in F. omega.
          + rewrite HHj' app_length /=.
            destruct H1 as [[? HL']|[? [L'' [v'' HL']]]]; rewrite HL'.
            * iIntros (F). simpl in F. omega.
            * iIntros (_). iDestruct "RSj" as "[[_ %]|OS]"; first omega.
              iDestruct "OS" as (j2 γ2) "[(%&_&%) _]". iPureIntro.
              destruct (HHj j2 H7) as [γj' TFj].
              assert (γj' = γ2).
              { destruct TFj as (_&_&HLk&_). rewrite H8 in HLk.
                by inversion HLk. } subst γj'.
              exists j2. rewrite H7. split; last auto.
              destruct TFj as (_&TFj&_).  move : TFj.
              rewrite app_length /= /L1' EqLa' 2!fmap_app 2!fmap_cons
                      (app_assoc _ (p4.1 :: nil)) (app_assoc _ (j' :: nil)).
              rewrite lookup_app_r; last (rewrite app_length /= HL2; omega).
              rewrite lookup_app_r; last (rewrite app_length /= HL2; omega).
              by rewrite 2!app_length.
        - iRight. iSplitL ""; first (iPureIntro; omega). iFrame "jWP'".
          rewrite (_ : Z.to_pos (Z.pos x' + data) = ZplusPos data x').
          + iFrame "oDW". iIntros "!#". by rewrite Pos2Z.id.
          + rewrite /ZplusPos. f_equal. omega. }
      iClear "RSj".

      (* update remaining TraceSpine to D3' L1 L1' *)
      iAssert (vPred_big_opL L (λ k a, ∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ') L1 (L1'.*1) k a j0 γ0)%VP V_l)
          (*([∗ list] k↦a ∈ L, ∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ') L1 (L1'.*1) k a j0 γ0)%VP V_l) *)
              with "[TS]" as "TS".
      { iApply (big_sepL_mono with "TS").
        iIntros (k a Hk). simpl.
        assert (Hk2 := lookup_lt_Some _ _ _ Hk).
        iIntros "TE". iDestruct "TE" as (j0 γ0) "(TF & TR)".
        iExists j0, γ0. iDestruct "TF" as %TF.
        assert (Hj01: j0 ≠ ix).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H3 ND).
          rewrite app_length /=. omega. }
        assert (Hj02: j0 ≠ i).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H2 ND).
          omega. }
        assert (Hj03: j0 ≠ j').
        { apply HDisj', elem_of_live'_dom, ALive,
                (elem_of_list_lookup_2 _ k), TF. }
        assert (HHj0 := HHD j0 Hj02 Hj01).
        iSplitL "".
        - iPureIntro. destruct TF as (TF1 & TF2 & TF4 & TF5 & TF6).
          split; [auto|repeat split].
          + move:TF2. rewrite EqLa' fmap_app lookup_app_l.
            * rewrite fmap_app /=. by apply lookup_app_l_Some.
            * rewrite fmap_length -(fmap_length snd) -EqL2 app_length. omega.
          + rewrite HID; [by rewrite -TF4|auto].
          + move => EqL1.
            assert (Llt:= plus_lt_compat_r _ _ 1 Hk2).
            rewrite EqL1 app_length /= in Llt. omega.
          + move => _. rewrite (HHj0 Hj03). destruct TF6 as [j2 [TF61 TF62]].
            * rewrite !app_length /=. omega.
            * exists j2. split; last auto.
              rewrite EqLa' fmap_app lookup_app_l in TF61.
              { rewrite fmap_app /=. by apply (lookup_app_l_Some _ _ _ _ TF61). }
              { rewrite fmap_length -(fmap_length snd) -EqL2 app_length /=.
                omega. }
        - by rewrite {2}/Trace_own_head {2}/Trace_own_tail HHj0. }

      iAssert (vPred_big_opL L' (λ k a, ∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ') L1 (L1'.*1)
                                  (length L + S (S k)) a j0 γ0)%VP V_l)
          (*([∗ list] k↦a ∈ L', ∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ') L1 (L1'.*1)
                                  (length L + S (S k)) a j0 γ0)%VP V_l) *)
              with "[Last]" as "Last".
      { iApply (big_sepL_mono with "Last").
        iIntros (k a Hk). simpl.
        assert (Hk2 := lookup_lt_Some _ _ _ Hk).
        iIntros "TE". iDestruct "TE" as (j0 γ0) "(TF & TR)".
        iExists j0, γ0. iDestruct "TF" as %TF.
        assert (Hj01: j0 ≠ ix).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H3 ND).
          omega. }
        assert (Hj02: j0 ≠ i).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H2 ND).
          rewrite app_length /=. omega. }
        assert (Hj03: j0 ≠ j').
        { apply HDisj', elem_of_live'_dom, ALive,
            (elem_of_list_lookup_2 _ (length (L ++ (p, v0)::nil) + S k)), TF. }
        assert (HHj0 := HHD j0 Hj02 Hj01).
        iSplitL "".
        - iPureIntro. destruct TF as (TF1 & TF2 & TF4 & TF5 & TF6).
          split; [auto|repeat split].
          + move :TF2.
            rewrite EqL2 fmap_length -(fmap_length fst L2) EqLa'
                    2!fmap_app 2!fmap_cons lookup_app_r; last omega.
            rewrite lookup_app_r; last (rewrite HL2; omega).
            rewrite (lookup_app_r (p4.1::nil)); last (simpl; omega).
            rewrite HL2 (lookup_app_r (j'::nil)); last (simpl; omega).
            rewrite (_: length L + 1 + S k = length L + S (S k))%nat;
              [done|omega].
          + rewrite HID; [by rewrite -TF4|auto].
          + move => EqL1. rewrite (HHD _ Hj02 Hj01 Hj03). apply TF5.
            move : EqL1. rewrite /L1 !app_length /=. move => ?. omega.
          + move => LeL1. rewrite (HHj0 Hj03). destruct TF6 as [j2 [TF61 TF62]].
            * move : LeL1. rewrite !app_length /=. move => ?. omega.
            * exists j2. split; last auto. move : TF61.
              rewrite app_length /= EqLa' 2!fmap_app 2!fmap_cons
                      lookup_app_r; last (rewrite HL2; omega).
              rewrite lookup_app_r; last (rewrite HL2; omega).
              rewrite (lookup_app_r (p4.1::nil)); last (simpl; omega).
              rewrite HL2 (lookup_app_r (j'::nil)); last (simpl; omega).
              rewrite (_: length L + 1 + S k = length L + S (S k))%nat;
                [done|omega].
        - iDestruct "TR" as "[[% _]|[_ [TR Perms]]]"; first omega.
          iRight. rewrite {2}/Trace_own_tail (HHj0 Hj03). iFrame "TR Perms".
          iPureIntro. omega. }

      assert (Hp4: p4.1 = ix).
      { destruct H3 as (_&H3&_). move :H3.
        rewrite EqLa' app_length /= -HL2 fmap_app fmap_cons list_lookup_middle;
          [by inversion 1|auto]. }
      assert (EqLa2: La = (L2 ++ (ix, (x, v1)) :: L4)%list).
      { rewrite -Hp4 Eqp4 EqLa'. by destruct p4. }
      (* relink node to last node *)
      do 3 (wp_op; iNext).
      iApply (rcuNodeUpdate_link_write Γ D3 (D3' γ') L L' L2 L4
                                       p v0 x v1 ix i γ j' (Zpos x') v' γ'
              with "[%] kI kS [$SN' Ti]"); auto.
      { rewrite HID; [apply H2|auto]. }
      { rewrite Pos2Z.id. split; [apply Z.lt_gt, Pos2Z.is_pos|auto]. }
      { reflexivity. }
      { rewrite Pos2Z.id. iSplitL ""; first (iIntros "!#"; iFrame "OE").
        rewrite (app_assoc _ ((p,v0)::nil)) Nat.add_0_r -EqLa2.
        rewrite Nat.add_0_r in H2. iFrame (H2) "Ti". }

      iClear "OE". iNext. iViewUp. iIntros "kS (TE & PrtS & OS)". wp_let. iNext.

      (* establish SnapshotValid for D3' *)
      iAssert (SnapshotValid P Γ (D3' γ') V_l)%I with "[F4 PrtS OS]" as "#Valid'".
      { iFrame "SN'". iDestruct "F4" as "(_ & F4)".
        iDestruct "F4" as (b) "(% & F41 & #F42)".
        iDestruct "F41" as (pb γb) "(% & #F4)".
        assert (Hb: b ∈ base (D3' γ')).
        { rewrite /base -BE elem_of_of_list. by apply hd_error_some_elem. }
        destruct WD3' as [B1 _].
        assert (Hb': ∀ b', b' ∈ base (D3' γ') → b' = b).
        { move => b' In'. apply (size_singleton_inv _ _ _ B1); done. }
        iExists b. iSplitL ""; last iSplitL "OS".
        - iPureIntro.
          destruct (basel (D3' γ')) as [|b' Lb'] eqn: HbD3.
          + exfalso. move : HbD3 B1. rewrite /base /basel /base'.
            move => -> //.
          + simpl. f_equal. apply Hb'.
            rewrite /base /base' -/basel' elem_of_of_list.
            apply hd_error_some_elem. rewrite /basel in HbD3. by rewrite HbD3.
        - iExists pb, γb.
          iDestruct "OS" as "#OS".
          destruct BL' as [b' [Eqb' Inb']].
          assert (b' = b). { apply Hb'. move : Inb'. by rewrite /base BE. }
          assert (b ≠ j').
          { apply HDisj', elem_of_live'_dom, rcu_base_live.
            rewrite elem_of_of_list. by apply hd_error_some_elem. }
          subst b'. iSplitL ""; first by rewrite HID.
          destruct H2 as (_&Eqi&Eqγi&_).
          case (decide (length L = 0)%nat) => [EqL1|NEqL].
          + assert (LNil := nil_length_inv _ EqL1).
            rewrite LNil /= in HHd. inversion HHd as [Hp].
            rewrite (_: Z.to_pos (Z.pos (lhd Γ) + link)
                        = ZplusPos link (lhd Γ)); last done.
            rewrite EqL1 /= in Eqi. rewrite Eqi in Eqb'. inversion Eqb'.
            subst b. destruct H8 as [H9 _]. rewrite H9 in Eqγi.
            inversion Eqγi. iApply ("OS" with "[%]"); [reflexivity|done].
          + assert (b ≠ i).
            { move => ?. subst i.
              assert(EqL0:= NoDup_lookup _ _ _ _ ND Eqi Eqb'). omega. }
            assert (b ≠ ix).
            { move => ?. subst b. destruct H3 as (_&H3&_).
              assert (HNLen := NoDup_lookup _ _ _ _ ND H3 Eqb').
              rewrite app_length /= in HNLen. omega. }
            by rewrite (HHD b).
        - iDestruct "PrtS" as "#PrtS".
          iIntros "!#". iIntros (k). iViewUp. iIntros (Hk).
          case (decide (k = i)) => [Eqk1|NEqk1];
            last case (decide (k = j')) => [Eqk2|NEqk2].
          + subst k. iExists (Z.to_pos p), γ. iFrame "PrtS".
            iPureIntro. rewrite HID; [apply H2|auto].
          + subst k. iExists x',γ'. iFrame (HIj) "PSeen'".
          + rewrite HL3 in Hk.
            move : Hk.
            rewrite elem_of_difference elem_of_union
                    elem_of_singleton elem_of_difference elem_of_singleton.
            move => [[//|[? ?]] ?].
            rewrite (HID _ NEqk2) (HHD _ NEqk1); [|auto|auto].
            iApply ("F42" $! k with " [%] [%]"); [reflexivity|..].
            by rewrite elem_of_difference. }

      iClear "PrtS PSeen' F4 OS".
      (* call deallocator *)
      destruct H as (Hq&HD1&HD2).
      wp_bind (_ #x)%E.
      rewrite (_: LitInt x = LitInt (Z.pos (Z.to_pos x))); last first.
      { rewrite Z2Pos.id; [auto|by apply H3]. }

      iApply (rcuDealloc_spec q Γ D1 D2 (D3' γ') (Z.to_pos x) Hq HD1
        with "[%] kI kS [$F1 $F2 $Valid' $MS' Tx]");
        [by etrans|reflexivity|..].
      { iExists ix, γx. iSplitL ""; last iSplitL "".
        - iPureIntro. split; last by (rewrite HID; [apply H3|auto]).
          rewrite elem_of_difference. split; first auto.
          apply elem_of_difference in HLix as [_ HLix].
          move => ?. by apply HLix, (rcu_dead_extend _ _ HD2).
        - by iIntros "!#".
        - iDestruct "Tx" as "[Tx1 Tx2]".
          iExists (hist D3 ix), v1.
          rewrite rcu_ZplusPos; last apply H3.
          rewrite rcu_ZplusPos; last apply H3.
          iFrame "Tx1 Tx2". }
      iClear "Perms".

      (* re-establish WriterSafe *)
      iNext. iViewUp. iIntros (r) "kS RS".
      iDestruct "RS" as (D2') "(% & RV & DB & MS)".

      wp_seq. iNext. wp_op. iNext.
      iApply ("Post" with "[%] kS [-]"); first done.
      iExists Γ, D2', (D3' γ'), (D3' γ').
      iFrame "RV DB MS Valid' F6". iFrame (SD3').
      iSplitL ""; first by destruct H.
      iSplitL ""; [iPureIntro|].
      - rewrite /L1 (app_assoc _ ((p,_)::nil)).
        move : HHd.
        have ? : (0 < length (L ++ (p, v0)::nil))%nat
          by (rewrite app_length /=; omega).
        rewrite lookup_app_l; [by rewrite (lookup_app_l _ (_::L'))|auto].
      - iExists L1'. iSplitL ""; first iPureIntro.
        + split; first auto. repeat split.
          * rewrite EqLa' in ND. move : ND.
            rewrite 2!fmap_app 2!fmap_cons 2!NoDup_app 2!NoDup_cons.
            move => [ND1 [ND2 [ND3 ND4]]]. repeat split; [auto| .. |auto].
            { move => k Ink /elem_of_cons [?|In4].
              - subst k. apply HNj'. rewrite fmap_app elem_of_app. by left.
              - apply (ND2 _ Ink). by right. }
            { move => ?. apply HNj'. rewrite fmap_app elem_of_app. by right. }
          * destruct BL' as [b' [Eqb' Inb']].
            exists b'. split; last by rewrite /base -BE.
            move : Eqb'. rewrite EqLa2 2!fmap_app.
            have ?: (0 < length (L2.*1))%nat.
            { rewrite fmap_length -(fmap_length snd) -EqL2 app_length /=. omega. }
            rewrite lookup_app_l; last auto. by rewrite lookup_app_l.
          * move => k. specialize (ALive k).
            rewrite HL3 fmap_app fmap_cons elem_of_app elem_of_cons
                    elem_of_union elem_of_difference 2!elem_of_singleton.
            move : ALive ND.
            rewrite EqLa' fmap_app fmap_cons
                          elem_of_app elem_of_cons Hp4
                          NoDup_app NoDup_cons.
            move => Hk1 [ND1 [ND2 [ND3 ND4]]].
            move => [In2|[->|In4]]; [right|by left|right]; split.
            { apply Hk1. by left. }
            { move => ?. subst k. apply (ND2 ix); [auto|by left]. }
            { apply Hk1. by right;right. }
            { move => ?. by subst k. }
        + rewrite -(vPred_big_opL_fold L1) big_sepL_app 2!big_sepL_cons.
          iFrame "TS Last". rewrite Nat.add_0_r.
          iSplitL "TE"; [by iExists i,γ|by iExists j',γ'].
    Qed.

    Lemma rcuNodeDelete_link_write Γ D D' L L' La Ln p v x vx ix i γ p' bj
      (HIi: info D' i = Some (Z.to_pos p, γ))
      (HHi: hist D' i = bj :: hist D i)
      (CASE: p' = 0 ∧ L' = nil ∨ 0 < p' ∧ (∃ L'' v', L' = (p', v') :: L''))
      (EqLa : (L ++ (p, v) ::nil)%list = La.*2) (EqLn : L' = Ln.*2)
      (HHj:  ∀ j, bj = Abs j
        → ∃ γj : gname,
         Trace_link_fact D (L ++ (p, v) :: (x, vx) :: L')
                           ((La ++ (ix, (x,vx)) :: Ln)%list.*1) p'
                           (length (L ++ (p, v) :: nil) + 1) j γj)
      (DLe: D ⊑ D') (UW: uwellformed D'):
      let L1 := (L ++ (p, v) :: L')%list in
      let L1' := (La ++ Ln)%list in
      {{{{ snapshot Γ D'
           ∗ TraceSpine_elem P Γ D (L ++ (p, v) :: (x, vx) :: L')
                                   ((La ++ (ix, (x,vx)) :: Ln)%list.*1)
                                   (length L) (p, v) i γ
           ∗ (  ■ (bj = Null ∧ p' = 0 )
              ∨ ∃ j γj,
                  ■ (bj = Abs j ∧ 0 < p' ∧ info D' j = Some (Z.to_pos p', γj))
                ∗ □ PermXList P Γ (Z.to_pos p') j γj) }}}}
        [ #(Z.to_pos (p + link)) ]_at <- #p'
      {{{{ RET #();
            TraceSpine_elem P Γ D' L1 (L1'.*1) (length L) (p, v) i γ
           ∗ PrtSeen γ (hist D' i)
           ∗ □ (■ (length L = 0)%nat
              -∗ [nXP Z.to_pos (p + link) in hist D' i @ (Γ,γ) | LLP P Γ i ]_R) }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS (SN & (% & TS) & OS) Post".
      assert (HLen: (length L < length (La.*1))%nat).
      { rewrite fmap_length -(fmap_length snd La) -EqLa app_length /=. omega. }
      iAssert (⌜Trace_link_fact D' L1 (L1'.*1) p (length L) i γ⌝)%I
        with "[OS]" as %TF'.
      { destruct H as (TF1 & TF2 & TF4 & TF5 & TF6).
         iSplit; [auto|repeat iSplit].
        - rewrite fmap_app (lookup_app_l _ _ _ HLen) in TF2.
          by rewrite fmap_app lookup_app_l.
        - by rewrite HIi.
        - rewrite HHi /L1 app_length /=.
          destruct CASE as [[? HL']|[? [L'' [v' HL']]]]; rewrite HL'.
          + iDestruct "OS" as "[[% _]|OS]"; first by subst bj.
            iDestruct "OS" as (??) "((_ & % & _) &_)". by omega.
          + iIntros (F). simpl in F. omega.
        - rewrite HHi /L1 app_length /=.
          destruct CASE as [[? HL']|[? [L'' [v' HL']]]]; rewrite HL'.
          + iIntros (F). simpl in F. omega.
          + iIntros (_). iDestruct "OS" as "[[_ %]|OS]"; first omega.
            iDestruct "OS" as (j γj) "[(%&_&%) _]". iPureIntro.
            destruct (HHj j H0) as [γj' TFj].
            assert (γj' = γj).
            { destruct TFj as (_&_&HLk&_). 
              rewrite (RCUData_info_lookup _ _ _ _ UW DLe HLk) in H1.
              by inversion H1. } subst γj'.
            exists j. rewrite H0. split; last auto.
            destruct TFj as (_&TFj&_).  move : TFj.
            assert (HLa: (length (La.*1) = length L + 1)%nat).
            { rewrite fmap_length -(fmap_length snd La) -EqLa app_length //. }
            rewrite 2!fmap_app fmap_cons app_length /= -HLa.
            rewrite (app_assoc _ (ix :: nil)).
            rewrite lookup_app_r; last (rewrite app_length /=; omega).
            rewrite lookup_app_r; last omega. rewrite app_length /=.
            rewrite (_ : (length (La.*1) + 1 - (length (La.*1) + 1))%nat
                         = (length (La.*1) - length (La.*1))%nat); [done|omega]. }
      iDestruct "TS" as "[[% oL]|(% & (oD & oL) & Perm)]".
      - rewrite /Trace_own_head.
        set Q : gname → pr_state RCULinkProtocol → vPred := λ γ s',
          [nXP Z.to_pos (p + link) in s' @ (Γ,γ) | LLP P Γ i]_W.
        iApply (GPS_nSW_ExWrite (LLP P Γ i) (γ:=γ) Γ (hist D' i) True Q
                  with "[] kI kS [$oL SN OS]");
          [rewrite HHi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "(_ & _ & $)".
          iModIntro. iNext. rewrite 2!LLP_unfold.
          iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HHi Zplus_0_r. iSplitL ""; first done.
          iDestruct "OS" as "[[% %]|OS]"; first by subst bj.
          iDestruct "OS" as (j γj) "[(% & % & %) #Perm]".
          subst bj. iExists γj. iSplitL ""; [iPureIntro|by iIntros "!#"].
          split; [omega|auto]. }

        iNext. iViewUp. iIntros "kS (oR & oL)". rewrite /Q.
        iApply ("Post" with "[] kS [oL oR]"); first auto.

        iDestruct (GPS_nSW_Reader_PrtSeen (IP := LLP P Γ i) γ _ Γ with "oR")
          as "#PSeen".
        iFrame (TF'). iSplitR "oR".
        + iLeft. iFrame (H0) "oL".
        + iSplitL ""; first done.
          iDestruct "oR" as "#oR". iIntros "!#". iViewUp. by iIntros "_".
      - iApply (GPS_nFWP_ExWrite (LLP P Γ i) γ (hist D' i) True (λ _ , True)%VP
                  with "[] kI kS [$oL SN OS]");
          [rewrite HHi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "_".
          iModIntro. iSplitL ""; first auto. iNext.
          rewrite 2!LLP_unfold. iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HHi Zplus_0_r. iSplitL ""; first done.
          iDestruct "OS" as "[[% %]|OS]"; first by subst bj.
          iDestruct "OS" as (j γj) "[(% & % & %) #Perm]".
          subst bj. iExists γj. iSplitL ""; [iPureIntro|by iIntros "!#"].
          split; [omega|auto]. }

        iNext. iViewUp. iIntros "kS (oL & _)".
        iApply ("Post" with "[] kS [oL oD Perm]"); first auto.
        destruct (GPS_nFWP_PrtSeen (IP := LLP P Γ i) γ
                  (Z.to_pos (p + 0)) (hist D' i) nf) as [PSeen].
        iDestruct (PSeen with "oL") as "#PSeen"; first reflexivity. clear PSeen.
        iFrame (TF'). iSplitR "".
        + iRight. iFrame (H0) "oD oL Perm".
        + iSplitL ""; first done. iIntros "!#". iIntros (??) "%". exfalso. omega.
    Qed.

    Lemma rcuNodeDelete_spec q L L' p v0 x v1:
      {{{{ WriterSafe P q (L ++ (p,v0)::(x,v1)::L')%list }}}}
        rcuNodeDelete #q #x #p
      {{{{ (r:Z), RET #r;
            WriterSafe P q (L ++ (p,v0)::L')%list }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS WS Post".
      wp_lam. iNext. wp_value. wp_lam. iNext. wp_value. wp_lam. iNext.

      wp_bind ([_]_at)%E.
      rewrite (_: L ++ (p, v0) :: (x, v1) :: L'
                  = (L ++ ((p,v0)::nil)) ++ (x,v1) :: L')%list; last first.
      { by rewrite -app_assoc. }

      iDestruct "WS" as (Γ D1 D2 D3)
                        "(% & F1 & F2 & F3 & F4 & [F5 TS] & % & F6)".
      iDestruct "F5" as %HHd.
      iDestruct "TS" as (La) "TS".
      iApply (rcuWriter_read_link_raw with "[%] kI kS [$F3 $TS]");
        first reflexivity.
      iNext. iViewUp. iIntros (p') "kS (MS & TS & % & LLPx)". wp_let. iNext.
      iDestruct "TS" as "[F5 TS]".
      iDestruct "F5" as %(EqLa & ND & BL' & ALive).

      rewrite -vPred_big_opL_fold 2!big_sepL_app
              big_sepL_singleton big_sepL_cons 2!vPred_big_opL_fold.
      iDestruct "TS" as "((TS & pi) & xi & Last)".
      iDestruct "pi" as (i γ) "[% Ti]".
      iDestruct "xi" as (ix γx) "Tx".
      iDestruct "Tx" as "[% [[% _]|[_ [Tx #Perms]]]]".
      { rewrite app_length /= in H4. omega. }

      iDestruct "LLPx" as (ix' γx') "[% LLPx]". rewrite Nat.add_0_r in H3.
      destruct (Trace_link_fact_same _ _ _ _ _ _ _ _ _ _ H3 H4) as (_&?&?).
      subst ix' γx'. clear H4.
      iDestruct "LLPx" as (Dx) "(#SNx & % & LLPx)".
      destruct (hist D3 ix) as [|bj Lj] eqn:HHbj; first by iDestruct "LLPx" as %?.

      assert (HLi: i ∈ live D3).
      { apply ALive, (elem_of_list_lookup_2 _ (length L + 0)%nat), H2. }
      assert (HDi := elem_of_live'_dom _ _ HLi).
      iDestruct (master_dictates_snapshot with "[$SNx $MS]") as %Dle.
      iAssert (⌜wellformed D3⌝)%I with "[MS]" as %W.
      { iDestruct (master_wellformed with "MS") as %W.
        iPureIntro. by apply (RCUData_uwellformed_wellformed _ i W). }

      iAssert ((■ (bj = Null ∧ p' = 0)
          ∨ ∃ j γj, ■ (bj = Abs j ∧ 0 < p' ∧ info D3 j = Some (Z.to_pos p', γj))
              ∗ □ PermXList P Γ (Z.to_pos p') j γj)%VP V_l)%I
        with "[LLPx]" as "#RSj".
      { destruct bj as [|j|]; [iLeft|iRight|by iDestruct "LLPx" as %?].
        - by iDestruct "LLPx" as %?.
        - iDestruct "LLPx" as (γj) "[% #Perm]".
          iExists j, γj. iSplit; [iPureIntro| by iIntros "!#"].
          destruct H5. repeat split; [omega|].
          rewrite (RCUData_info_lookup _ _ _ _ _ Dle H6); [done|by right]. }
      iClear "LLPx".
      destruct (fmap_app_inv _ _ _ _ EqLa) as [L2 [L3 [EqL2 [EqL3 EqLa']]]].
      symmetry in EqL3.
      destruct (fmap_cons_inv _ _ _ _ EqL3) as [p4 [L4 [Eqp4 [EqL4 EqL4']]]].
      rewrite EqL4' in EqLa'. clear EqL4' EqL3.
      set L1 := (L ++ (p, v0) :: L')%list.
      set L1' := (L2 ++ L4)%list.
      assert (HL1: L1 = L1'.*2).
      { by rewrite fmap_app -EqL2 -EqL4 app_assoc_reverse. }

      (* setting up new history *)
      set D3' : RCUData
        := ((i, bj) :: (ix, Dead) :: D3.1, D3.2).

      iAssert (⌜bj = Null ∨ ∃ j, bj = Abs j⌝)%I with "[]" as %Hbj.
      { iDestruct "RSj" as "[[% _]|RSj]"; [by iLeft|iRight].
        iDestruct "RSj" as (j γj) "[[% _] _]". by iExists j. }
      assert (HHi: hd_error (hist' (D3.1) i) = Some (Abs ix)).
      { destruct H2 as (H11 & H12 & H13 & H14 & [j' [H15 H16]]).
        - rewrite 2!app_length /=. omega.
        - destruct H3 as (H21 & H22 & H23). rewrite Nat.add_0_r in H15.
          rewrite app_length /= H15 in H22. inversion H22. by subst j'. }
      assert (HHix : hd_error (hist' (D3.1) ix) = Some bj).
      { rewrite /hist in HHbj. by rewrite HHbj. }

      assert (HLix : ix ∈ live D3).
      { destruct W as (_&_&_&_&_&LC).
        destruct (LC _ HLi) as [j2 [Eqj2 Eq]]. rewrite HHi in Eqj2.
        inversion Eqj2. subst j2. move : Eq => [//|[? [[->]]] //]. }
      assert (Hix : i ≠ ix).
      { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H2 H3 ND).
        rewrite app_length /=. omega. }
      have BE : base D3 ≡ base D3' by apply rcu_base_delete.
      assert (ED3': D3 ⊑ D3').
      { split; [right|done]. split; [by repeat apply suffix_cons_r|done]. }
      have SD3': SinglePtr D3' by apply rcu_SinglePtr_delete.

      iAssert (⌜∀ j : aloc, bj = Abs j →
                  ∃ γj,
              Trace_link_fact D3 ((L ++ ((p, v0)::nil)) ++ (x, v1) :: L')%list
                             (La.*1) p' (length (L ++ (p, v0)::nil) + 1) j γj⌝)%I
              with "[Last]" as %HHj'.
      { iIntros (j Eqj).
        iDestruct "RSj" as "[[% _]|RSj]"; first by subst bj.
        iDestruct "RSj" as (j' γj) "[% _]". destruct H5 as (? & Hp' & Infp').
        subst bj. inversion H5. subst j'. clear H5.
        destruct H1 as [[? ]|[_ [L'' [v' HL']]]]; first omega. iExists γj.
        rewrite HL' -vPred_big_opL_fold big_sepL_cons.
        iDestruct "Last" as "[Last _]".
        iDestruct "Last" as (j3 γ3) "[% _]". iPureIntro.
        destruct (H1) as (_&Hj3&Hj3'&_). destruct H3 as (_&_&_&_&H3).
        destruct H3 as [j4 [Hj41 Hj42]].
        { rewrite (app_length _ ((x, v1) :: _)) HL' /=. omega. }
        rewrite Hj3 in Hj41. inversion Hj41. subst j4. clear Hj41.
        rewrite HHix in Hj42. inversion Hj42. subst j3.
        rewrite Infp' in Hj3'. by inversion Hj3'. }

      assert (Hj' : ∀ j' , bj = Abs j' → i ≠ j' ∧ ix ≠ j' ∧ j' ∈ live D3).
      { move => j'. specialize (HHj' j').
        move :Hbj => [->//|[j2 Eqj2] Eqj3]. rewrite Eqj3 in Eqj2.
        inversion Eqj2. subst j2.
        destruct (HHj' Eqj3) as [γ2 TF]. repeat split.
        - apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H2 TF ND).
          rewrite app_length /=. omega.
        - apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ H3 TF ND).
          rewrite app_length /=. omega.
        - destruct W as (_&_&_&_&_&LC).
          destruct (LC _ HLix) as [j4 [Eqj4 Eq]]. rewrite HHix Eqj3 in Eqj4.
          inversion Eqj4. subst j4.
          move : Eq => [//|[? [[->]]] //]. }

      have WD3': wellformed D3' by (apply RCUData_wellformed_delete; auto).
      have UD3' : uwellformed D3' by right.

      assert (Live': i ∈ live D3').
      { apply (elem_of_rcu_live'_mono _ ((i, bj) :: (ix, Dead)::nil));
          last auto.
         move => ? /elem_of_cons [[-> ]|/elem_of_list_singleton[//]].
         move : Hbj => [->//|[? ->]//]. }
      assert (Hi' : hist D3' i = bj :: hist D3 i).
      { rewrite /hist rcu_hist'_cons_now. f_equal.
        by rewrite rcu_hist'_cons_next. }
      assert (Hxi' : hist' (D3'.1) ix = Dead :: hist D3 ix).
      { rewrite /hist rcu_hist'_cons_next; last auto.
        by rewrite rcu_hist'_cons_now. }
      assert (HDxi : ix ∈ dead D3').
      { apply elem_of_rcu_dead'. rewrite Hxi'. left. }
      assert (HHD : ∀ k, k ≠ i → k ≠ ix → hist D3' k = hist D3 k).
      { move => ???. rewrite /hist rcu_hist'_cons_next; last auto.
        by rewrite rcu_hist'_cons_next. }
      assert (HD3: dom (gset _) D3' = dom (gset _) D3).
      { apply leibniz_equiv. rewrite /dom !/RCUData_dom rcu_dom'_cons_in;
          first rewrite rcu_dom'_cons_in.
        - auto.
        - apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem.
        - apply (elem_of_rcu_hist_dom_mono (_::nil)).
          apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem. }
      assert (HDix: ix ∈ dead D3').
      { apply elem_of_rcu_dead'. rewrite Hxi'. by left. }
      assert (HL3: live D3' = live D3 ∖ {[ix]}).
      { apply leibniz_equiv. rewrite /D3' /live.
        move => j. rewrite elem_of_difference elem_of_singleton.
        case (decide (j = i)) => [->//|NEqj].
        split.
        - rewrite /dom /RCUData_dom in HD3.
          rewrite 2!elem_of_difference HD3. move => [? NInD].
          repeat split; first auto.
          + move => ?. by apply NInD, (rcu_dead_extend _ _ ED3').
          + move => ?. subst j. apply NInD, HDix.
        - rewrite -(elem_of_rcu_live'_further j i bj ((ix, Dead) :: D3.1) NEqj).
          move => [Hj1 Hj2].
          apply (elem_of_rcu_live'_mono _ ((ix,Dead) :: nil)); last done.
          move => ? /elem_of_list_singleton [_ ->] //. }

      (* update remaining TraceSpine to D3' L1 L1' *)
      iAssert (vPred_big_opL L (λ k a, ∃ j0 γ0,
                  TraceSpine_elem P Γ D3' L1 (L1'.*1) k a j0 γ0)%VP V_l)
      (*([∗ list] k↦a ∈ L, ∃ j0 γ0,
                  TraceSpine_elem P Γ D3' L1 (L1'.*1) k a j0 γ0)%VP V_l) *)
              with "[TS]" as "TS".
      { iApply (big_sepL_mono with "TS").
        iIntros (k a Hk). simpl.
        assert (Hk2 := lookup_lt_Some _ _ _ Hk).
        iIntros "TE". iDestruct "TE" as (j0 γ0) "(TF & TR)".
        iExists j0, γ0. iDestruct "TF" as %TF.
        assert (Hj01: j0 ≠ ix).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H3 ND).
          rewrite app_length /=. omega. }
        assert (Hj02: j0 ≠ i).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H2 ND).
          omega. }
        assert (HHj0 := HHD j0 Hj02 Hj01).
        iSplitL "".
        - iPureIntro. destruct TF as (TF1 & TF2 & TF4 & TF5 & TF6).
          split; [auto|repeat split].
          + move:TF2. rewrite EqLa' fmap_app lookup_app_l.
            * rewrite fmap_app /=. by apply lookup_app_l_Some.
            * rewrite fmap_length -(fmap_length snd) -EqL2 app_length. omega.
          + by rewrite -TF4.
          + move => EqL1.
            assert (Llt:= plus_lt_compat_r _ _ 1 Hk2).
            rewrite EqL1 app_length /= in Llt. omega.
          + move => _. rewrite HHj0. destruct TF6 as [j' [TF61 TF62]].
            * rewrite !app_length /=. omega.
            * exists j'. split; last auto.
              rewrite EqLa' fmap_app lookup_app_l in TF61.
              { rewrite fmap_app /=. by apply (lookup_app_l_Some _ _ _ _ TF61). }
              { rewrite fmap_length -(fmap_length snd) -EqL2 app_length /=.
                omega. }
        - by rewrite {2}/Trace_own_head {2}/Trace_own_tail HHj0. }

      assert (HL2: (length (L2.*1) = length L + 1)%nat).
      { rewrite fmap_length -(fmap_length snd L2) -EqL2 app_length //. }

      iAssert (vPred_big_opL L' (λ k a, ∃ j0 γ0,
                  TraceSpine_elem P Γ D3' L1 (L1'.*1)
                                  (length L + S k) a j0 γ0)%VP V_l)
        (* ([∗ list] k↦a ∈ L', ∃ j0 γ0,
                  TraceSpine_elem P Γ D3' L1 (L1'.*1)
                                  (length L + S k) a j0 γ0)%VP V_l) *)
              with "[Last]" as "Last".
      { iApply (big_sepL_mono with "Last").
        iIntros (k a Hk). simpl.
        assert (Hk2 := lookup_lt_Some _ _ _ Hk).
        iIntros "TE". iDestruct "TE" as (j0 γ0) "(TF & TR)".
        iExists j0, γ0. iDestruct "TF" as %TF.
        assert (Hj01: j0 ≠ ix).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H3 ND).
          omega. }
        assert (Hj02: j0 ≠ i).
        { apply (Trace_link_fact_different _ _ _ _ _ _ _ _ _ _ _ TF H2 ND).
          rewrite app_length /=. omega. }
        assert (HHj0 := HHD j0 Hj02 Hj01).
        iSplitL "".
        - iPureIntro. destruct TF as (TF1 & TF2 & TF4 & TF5 & TF6).
          split; [auto|repeat split].
          + move :TF2.
            rewrite EqL2 fmap_length -(fmap_length fst L2) EqLa'
              2!fmap_app fmap_cons lookup_app_r; last omega.
            rewrite lookup_app_r; last (rewrite HL2; omega).
            rewrite (lookup_app_r (p4.1::nil)); last (simpl; omega).
            rewrite (_: (length (L2.*1) + S k - length (L2.*1) - 1)%nat
                        = (length L + S k - length (L2.*1))%nat);
              [done|rewrite HL2; lia].
          + by rewrite -TF4.
          + move => EqL1. rewrite (HHD _ Hj02 Hj01). apply TF5.
            move : EqL1. rewrite /L1 !app_length /=. move => ?. omega.
          + move => LeL1. rewrite HHj0. destruct TF6 as [j' [TF61 TF62]].
            * move : LeL1. rewrite !app_length /=. move => ?. omega.
            * exists j'. split; last auto.
              move : TF61.
              rewrite EqL2 fmap_length -(fmap_length fst L2) EqLa'
                2!fmap_app fmap_cons lookup_app_r; last omega.
              rewrite lookup_app_r; last (rewrite HL2; omega).
              rewrite (lookup_app_r (p4.1::nil)); last (simpl; omega).
              rewrite (_: (length (L2.*1) + S k + 1 - length (L2.*1) - 1)%nat
                        = (length L + S k + 1 - length (L2.*1))%nat);
                [done|rewrite HL2; lia].
        - iDestruct "TR" as "[[% _]|[_ [TR Perms]]]"; first omega.
          iRight. rewrite {2}/Trace_own_tail HHj0. iFrame "TR Perms".
          iPureIntro. omega. }

      (* update global ghost history *)
      iMod (master_update Γ D3 D3' with "MS") as "[MS' #SN']";
        [apply UD3'|apply ED3'|].

      wp_op. iNext. wp_op. iNext. wp_bind ([_]_at <- _)%E.
      assert (Hp4: p4.1 = ix).
      { destruct H3 as (_&H3&_). move :H3.
        rewrite EqLa' app_length /= -HL2 fmap_app fmap_cons list_lookup_middle;
          [by inversion 1|auto]. }
      assert (EqLa2: La = (L2 ++ (ix, (x, v1)) :: L4)%list).
      { rewrite -Hp4 Eqp4 EqLa'. by destruct p4. }

      (* relink node to last node *)
      iApply (rcuNodeDelete_link_write Γ D3 D3' L L' L2 L4 p v0 x v1 ix i γ p' bj
              with "[%] kI kS [$SN' $RSj Ti]"); auto.
      { apply H2. }
      { rewrite EqLa2 in HHj'. by rewrite (app_assoc _ ((p,v0)::nil)). }
      { reflexivity. }
      { rewrite -EqLa2 (app_assoc _ ((p,v0)::nil)).
        rewrite Nat.add_0_r in H2. rewrite Nat.add_0_r. iFrame (H2) "Ti". }

      iClear "RSj".
      iNext. iViewUp. iIntros "kS (TE & PrtS & OS)". wp_let. iNext.

      (* establish SnapshotValid for D3' *)
      iAssert (SnapshotValid P Γ D3' V_l)%I with "[F4 PrtS OS]" as "#Valid'".
      { iFrame "SN'". iDestruct "F4" as "(_ & F4)".
        iDestruct "F4" as (b) "(% & F41 & #F42)".
        iDestruct "F41" as (pb γb) "(% & #F4)".
        assert (Hb: b ∈ base D3').
        { rewrite -BE elem_of_of_list. by apply hd_error_some_elem. }
        destruct WD3' as [B1 _].
        assert (Hb': ∀ b', b' ∈ base D3' → b' = b).
        { move => b' In'. apply (size_singleton_inv _ _ _ B1); done. }
        iExists b. iSplitL ""; last iSplitL "OS".
        - iPureIntro.
          destruct (basel D3') as [|b' Lb'] eqn: HbD3.
          + exfalso. move : HbD3 B1. rewrite /base /basel /base'.
            move => -> //.
          + simpl. f_equal. apply Hb'.
            rewrite /base /base' -/basel' elem_of_of_list.
            apply hd_error_some_elem. rewrite /basel in HbD3. by rewrite HbD3.
        - iExists pb, γb. iSplitL ""; first (iPureIntro; apply H6).
          iDestruct "OS" as "#OS".
          destruct BL' as [b' [Eqb' Inb']].
          assert (b' = b).
          { apply Hb'. move : Inb'. by rewrite BE. }
          subst b'. destruct H2 as (_&Eqi&Eqγi&_).
          case (decide (length L = 0)%nat) => [EqL1|NEqL].
          + assert (LNil := nil_length_inv _ EqL1).
            rewrite LNil /= in HHd. inversion HHd as [Hp].
            rewrite (_: Z.to_pos (Z.pos (lhd Γ) + link)
                        = ZplusPos link (lhd Γ)); last done.
            rewrite EqL1 /= in Eqi. rewrite Eqi in Eqb'. inversion Eqb'.
            subst b. destruct H6 as [H6 _]. rewrite H6 in Eqγi.
            inversion Eqγi. iApply ("OS" with "[%]"); [reflexivity|done].
          + assert (b ≠ i).
            { move => ?. subst i.
              assert(EqL0:= NoDup_lookup _ _ _ _ ND Eqi Eqb'). omega. }
            assert (b ≠ ix).
            { move => ?. subst b. destruct H3 as (_&H3&_).
              assert (HNLen := NoDup_lookup _ _ _ _ ND H3 Eqb').
              rewrite app_length /= in HNLen. omega. }
            by rewrite (HHD b).
        - iDestruct "PrtS" as "#PrtS".
          iIntros "!#". iIntros (k). iViewUp. iIntros (Hk).
          case (decide (k = i)) => [Eqk1|NEqk1].
          + subst k. iExists (Z.to_pos p), γ. iFrame "PrtS".
            iPureIntro. apply H2.
          + rewrite HL3 in Hk. rewrite (HHD _ NEqk1).
            * iApply ("F42" $! k with "[%] [%]"); [reflexivity|..].
              move : Hk. rewrite 3!elem_of_difference. by move => [[? _] ?].
            * move : Hk. rewrite 2!elem_of_difference elem_of_singleton.
              by move => [[_ ?] ?]. }

      (* call deallocator *)
      destruct H as (Hq&HD1&HD2).
      rewrite (_: LitInt x = LitInt (Z.pos (Z.to_pos x))); last first.
      { rewrite Z2Pos.id; [auto|by apply H3]. }
      iClear "SNx F4 SN' OS PrtS".
      iApply (rcuDealloc_spec q Γ D1 D2 D3' (Z.to_pos x) Hq HD1
        with "[%] kI kS [$F1 $F2 $Valid' $MS' Tx]");
        [by etrans|reflexivity|..].
      { iExists ix, γx. iSplitL ""; last iSplitL "".
        - iPureIntro. split; last by apply H3. rewrite elem_of_difference.
          split; first auto. apply elem_of_difference in HLix as [_ HLix].
          move => ?. by apply HLix, (rcu_dead_extend _ _ HD2).
        - by iIntros "!#".
        - iDestruct "Tx" as "[Tx1 Tx2]".
          iExists (hist D3 ix), v1.
          rewrite rcu_ZplusPos; last apply H3.
          rewrite rcu_ZplusPos; last apply H3.
          iFrame "Tx1 Tx2". }

      (* re-establish WriterSafe *)
      iNext. iViewUp. iIntros (r) "kS RS".
      iDestruct "RS" as (D2') "(% & RV & DB & MS)".
      iApply ("Post" with "[] kS [-]"); first auto.
      iExists Γ, D2', D3', D3'. rewrite /RemainingThreadTokens -HD3.
      iFrame "RV DB MS Valid' F6". iFrame (SD3').
      iSplitL ""; first by destruct H.
      iSplitL ""; [iPureIntro|].
      - rewrite /L1 (app_assoc _ ((p,_)::nil)).
        move : HHd.
        have ? : (0 < length (L ++ (p, v0)::nil))%nat
          by (rewrite app_length /=; omega).
        rewrite lookup_app_l; [by rewrite (lookup_app_l _ L')|auto].
      - iExists L1'. iSplitL ""; first iPureIntro.
        + split; first auto. repeat split.
          * rewrite EqLa' in ND. move : ND.
            rewrite 2!fmap_app fmap_cons 2!NoDup_app NoDup_cons.
            move => [ND1 [ND2 [ND3 ND4]]]. repeat split; [auto| |auto].
            move => ? /ND2. set_solver+.
          * destruct BL' as [b' [Eqb' Inb']].
            exists b'. split; last by rewrite -BE.
            move : Eqb'. rewrite EqLa2 2!fmap_app.
            have ?: (0 < length (L2.*1))%nat.
            { rewrite fmap_length -(fmap_length snd) -EqL2 app_length /=. omega. }
            rewrite lookup_app_l; last auto. by rewrite lookup_app_l.
          * move => k. move : (ALive k) ND.
            rewrite EqLa2 HL3 2!fmap_app fmap_cons /= NoDup_app NoDup_cons
                    2! elem_of_app elem_of_difference elem_of_singleton.
            move => Hk1 [ND1 [ND2 [ND3 ND4]]] Hk2. split.
            { apply Hk1. move : Hk2 => [?|?]; [by left|by right;right]. }
            { move => ?. subst k. move : Hk2 => [/ND2 Hk2|//].
              apply Hk2. by left. }
        + rewrite -(vPred_big_opL_fold L1) big_sepL_app big_sepL_cons.
          iFrame "TS Last". rewrite Nat.add_0_r. by iExists i,γ.
    Qed.

    Lemma rcuNodeAppend_link_write Γ D D' p v i x γi γ v' j L L'
      (Hi: hist D' i = Abs j :: hist D i)
      (HI: info D' j = Some (x, γ))
      (EqLen: length L' = length (L ++ (p, v)::nil))
      (HIi: info D' i = info D i):
      let L1 := (L ++ (p, v)::(Z.pos x, v')::nil)%list in
      let L1' := (L' ++ j::nil)%list in
      {{{{ (vPred_big_opL (seq 0 NPosn) (λ _ t, PermX P Γ x t j γ))
           (* ([∗ list] t ∈ seq 0 NPosn, PermX P Γ x t j γ) *)
           ∗ snapshot Γ D'
           ∗ TraceSpine_elem P Γ D (L ++ (p, v) :: nil) L'
                                   (length L + 0) (p, v) i γi }}}}
        [ #(Z.to_pos (p + link)) ]_at <- #(Z.pos x)
      {{{{ RET #();
            TraceSpine_elem P Γ D' L1 L1' (length L + 0) (p, v) i γi
           ∗ PrtSeen γi (hist D' i)
           ∗ □ (■ (length L = 0)%nat
              -∗ [nXP Z.to_pos (p + 0) in hist D' i @ (Γ,γi) | LLP P Γ i ]_R) }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS (#Perm & #SN & % & TR) Post".
      destruct H as (TF1 & TF2 & TF4 & TF5 & TF6).
      assert (Trace_link_fact D' L1 L1' p (length L + 0) i γi).
      { split; [auto|repeat split].
        - by apply (lookup_app_l_Some _ _ _ _ TF2).
        - by rewrite HIi.
        - rewrite /L1 app_length /=. omega.
        - move => _. exists j.
          rewrite app_length /= in EqLen.
          rewrite Hi -plus_n_O -EqLen.
          split; [by apply list_lookup_middle|auto]. }
      iAssert ((□ (∀ t, ■ (t < NPosn)%nat → PermX P Γ x t j γ))%VP V_l) 
        with "[]" as "#Perm2".
      { iIntros "!#". iIntros (t). iViewUp.
        iIntros (Lt). rewrite -vPred_big_opL_fold.
        iApply (big_sepL_elem_of with "Perm"). by apply seq_elem_of. }
      iDestruct "TR" as "[[% oL]|(% & (oD & oL) & Perms)]".
      - rewrite /Trace_own_head.
        set Q : gname → pr_state RCULinkProtocol → vPred := λ γ s',
          [nXP Z.to_pos (p + 0) in s' @ (Γ,γ) | LLP P Γ i]_W.
        iApply (GPS_nSW_ExWrite (LLP P Γ i) (γ:=γi) Γ (hist D' i) True Q
                  with "[] kI kS [$oL]");
          [rewrite Hi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "(_ & _ & $)".
          iModIntro. iNext. rewrite 2!LLP_unfold.
          iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HIi Zplus_0_r Hi.
          iSplitL ""; first done. iExists γ.
          iSplit; [iPureIntro|by iIntros "!#"]. split; [lia|auto]. }
        iNext. iViewUp. iIntros "kS (oR & oL)". rewrite /Q.
        iApply ("Post" with "[] kS [oL oR]"); first auto.
        iDestruct (GPS_nSW_Reader_PrtSeen (IP := LLP P Γ i) γi _ Γ with "oR")
          as "#PSeen".
        iFrame (H). iSplitR "oR".
        + iLeft. iFrame (H0) "oL".
        + iSplitL ""; first done.
          iDestruct "oR" as "#oR". iIntros "!#". iViewUp. by iIntros "_".
      - iApply (GPS_nFWP_ExWrite (LLP P Γ i) γi (hist D' i) True (λ _ , True)%VP
                  with "[] kI kS [$oL]");
          [rewrite Hi; by apply suffix_cons_r|auto|auto|done|..].
        { iNext. iIntros (v0). iViewUp. iIntros "_".
          iModIntro. iSplitL ""; first auto. iNext.
          rewrite 2!LLP_unfold. iSplitL ""; first auto.
          iExists D'. iFrame "SN". rewrite HIi Zplus_0_r Hi.
          iSplitL ""; first auto. iExists γ.
          iSplit; [iPureIntro|by iIntros "!#"]. split; [lia|auto]. }
        iNext. iViewUp. iIntros "kS (oL & _)".
        iApply ("Post" with "[] kS [oL oD Perms]"); first auto.
        destruct (GPS_nFWP_PrtSeen (IP := LLP P Γ i) γi
                  (Z.to_pos (p + 0)) (hist D' i) nf) as [PSeen].
        iDestruct (PSeen with "oL") as "#PSeen"; first reflexivity. clear PSeen.
        iFrame (H). iSplitR "".
        + iRight. iFrame (H0) "oD oL Perms".
        + iSplitL ""; first done. iIntros "!#". iIntros (??) "%". exfalso. omega.
    Qed.

    Lemma rcuNodeAppend_spec q L p v v':
      {{{{ WriterSafe P q (L ++ (p,v)::nil)%list ∗ □ P v' }}}}
        rcuNodeAppend #q #p #v'
      {{{{(x:Z), RET #x; WriterSafe P q (L ++ (p,v)::(x,v')::nil)%list }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS (WS & #P) Post".
      wp_lam. iNext. wp_value. wp_lam. iNext. wp_value. wp_lam. iNext.

      (* allocation space for new node *)
      wp_bind (malloc _).
      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].

      iNext. iViewUp. iIntros (x) "kS oLs".
      rewrite -vPred_big_opL_fold big_sepL_cons big_opL_singleton.
      iDestruct "oLs" as "(oL & oD)".
      wp_let. iNext. wp_op. iNext.

      (* init write for new node data *)
      wp_bind ([_]_na <- _)%E.
      iApply (na_write_frac_1 with "[] kI kS oD"); [done|done|].
      iNext. iViewUp. iIntros "kS oD".
      wp_seq. iNext. wp_op. iNext.

      wp_bind ([_]_na <- _)%E.
      iDestruct "WS" as (Γ D1 D2 D3)
                        "(% & F1 & F2 & F3 & F4 & (F5 & TS) & % & F6)".
      iDestruct "F5" as %HHd.
      iDestruct "TS" as (L') "[% TS]".
      destruct H1 as (EqL & ND & BL' & ALive).

      rewrite -vPred_big_opL_fold big_sepL_app
              big_sepL_singleton vPred_big_opL_fold.
      iDestruct "TS" as "(TS & Last)".
      iDestruct "Last" as (i γi) "(% & Last)".

      (* setting up new history *)
      set j : aloc := fresh (dom _ D3).
      set D3' : gname -> RCUData 
        := λ γ, ((i, Abs j) :: (j, Null) :: D3.1, (j,x,γ) :: D3.2).
      set L1 := (L ++ (p, v) :: (Z.pos x, v') :: nil)%list.
      set L1' := (L' ++ (j, (Z.pos x, v')) :: nil)%list.

      assert (HLi: i ∈ live D3).
      { apply ALive, (elem_of_list_lookup_2 _ (length L + 0)%nat), H1. }
      assert (HDi := elem_of_live'_dom _ _ HLi).
      assert (i ≠ j).
      { move => ?. subst i. by apply (is_fresh (dom (gset _) D3)). }
      assert (j ∉ L'.*1).
      { move => ?. apply (is_fresh (dom (gset _) D3)).
        by apply elem_of_live'_dom, ALive. }
      assert (Hj : ∀ γ, hist (D3' γ) j = Null :: nil).
      { intros ?. rewrite /hist. rewrite rcu_hist'_cons_next; last auto.
        rewrite rcu_hist'_cons_now. f_equal. apply rcu_hist'_nil_fresh. }
      assert (HIj: ∀ γ, info (D3' γ) j = Some (x, γ)).
      { intros ?. by rewrite /info /info' list_filter_cons. }
      iAssert (⌜wellformed D3⌝)%I with "[F3]" as %W.
      { iDestruct (master_wellformed with "F3") as %W.
        iPureIntro. by apply (RCUData_uwellformed_wellformed _ i W). }
      assert (BE := rcu_base_append (D3.1) i HLi).
      assert (ED3': ∀ γ, D3 ⊑ (D3' γ)).
      { intros ?. split; simpl.
        - right. split; [by repeat apply suffix_cons_r|done].
        - by apply suffix_cons_r. }
      assert (SD3': ∀ γ, SinglePtr (D3' γ)).
      { intros ?. apply (rcu_SinglePtr_append (D3.1)); [apply W|auto]. }
      assert (WD3': ∀ γ, wellformed (D3' γ)).
        { intros ?. apply RCUData_wellformed_append; [auto|auto|apply ED3']. }
      assert (UD3' : ∀ γ, uwellformed (D3' γ)). { by right. }

      (* init write for new node link *)
      iApply (GPS_nFWP_Init_strong (LLP P Γ j) (λ γ, hist (D3' γ) j)
               (λ γ, master Γ (D3' γ) ∗ snapshot Γ (D3' γ))%VP
          with "[%] kI kS [$oL F3]"); [auto|reflexivity|..].
      { iIntros (γ).
        iSplitL "".
        - iModIntro. iNext. by rewrite LLP_unfold.
        - (* update global ghost history *)
          iMod (master_update Γ D3 (D3' γ) with "F3") as "[MS' #SN']";
            [apply UD3'|apply ED3'|].
          iFrame "MS' SN'".
          iModIntro. iNext. rewrite LLP_unfold.
          iExists (D3' γ). iFrame "SN'". rewrite Hj. by iSplit. }

      iNext. iViewUp. iIntros (γ) "kS [[MS' #SN'] jP]".
      destruct (GPS_nFWP_PrtSeen (IP := LLP P Γ j) γ
                  (ZplusPos 0 x) (hist (D3' γ) j) 1%Qp) as [PSeen].
      iDestruct (PSeen with "jP") as "#PSeen"; first reflexivity. clear PSeen.
      wp_seq. iNext. wp_op. iNext. wp_op. iNext. wp_op. iNext.
      wp_bind ([_]_at <- _)%E.

      specialize (HIj γ). specialize (ED3' γ). specialize (SD3' γ).
      specialize (UD3' γ). specialize (Hj γ).
      assert (Live': i ∈ live (D3' γ)).
      { apply (elem_of_rcu_live'_mono _ ((i, Abs j)::(j, Null)::nil));
          last auto.
         move => ? /elem_of_cons [[-> //]|/elem_of_list_singleton[//]]. }
      assert (Hi' : hist (D3' γ) i = Abs j :: hist D3 i).
      { rewrite /hist rcu_hist'_cons_now. f_equal.
        by rewrite rcu_hist'_cons_next. }
      assert (HHD : ∀ k, k ≠ i → k ≠ j → hist (D3' γ) k = hist D3 k).
      { move => ???. rewrite /hist rcu_hist'_cons_next; last auto.
        by rewrite rcu_hist'_cons_next. }
      assert (HID : ∀ k, k ≠ j → info (D3' γ) k = info D3 k).
      { move => ??. by rewrite /info /info' list_filter_cons_not. }
      assert (HD3: dom (gset _) (D3' γ) ≡ {[j]} ∪ dom (gset _) D3).
      { rewrite /dom !/RCUData_dom rcu_dom'_cons_in.
        - by rewrite rcu_dom'_cons.
        - by apply (elem_of_rcu_hist_dom_mono ((j, Null)::nil)). }

      (* allocate escrows for new node *)
      destruct (GPS_nFWP_mult_splitL_1
        (IP := LLP P Γ j) γ (ZplusPos 0 x) (hist (D3' γ) j) n) as [Split].
      iDestruct (Split with "jP") as "[jWP jRP]"; first reflexivity.
      clear Split.

      iDestruct (rcu_remain_txtokens_one Γ D3 (D3' γ) _ j HD3 with "F6")
         as "(F6 & tTok)".
      { by apply is_fresh. }

      destruct (na_frac_mult_splitL_1 (ZplusPos data x) v' (n + 1)) as [Split].
      iDestruct (Split with "oD") as "oD"; first reflexivity. clear Split.
      rewrite (_: Pos.to_nat (n + 1) = S $ NPosn); last lia.
      rewrite seq_S.

      iAssert ((ZplusPos 1 x ↦{nf} v')%VP V_l
        ∗ vPred_big_opL (seq 0 NPosn) (λ _ _, ZplusPos 1 x ↦{nf} v')%VP V_l)%I
          (*([∗ list] _ ∈ seq 0 NPosn, ZplusPos 1 x ↦{nf} v')%VP V_l)%I*)
        with "[oD]" as "[oDW oDR]".
      { rewrite -{1}vPred_big_opL_fold big_sepL_app
                big_sepL_singleton vPred_big_opL_fold.
        by iDestruct "oD" as "[$ $]". }
       iCombine "tTok" "oDR" as "OE". iCombine "OE" "jRP" as "OE".

      iDestruct (rcu_escrows_alloc Γ x (hist (D3' γ) j) j γ v' with "[P OE]")
        as "OE".
      { iSplitL ""; first by iIntros "!#".
        rewrite -2!big_sepL_sepL.
        iApply (big_sepL_mono with "OE"). iIntros (???) "(($ & $) & $)". }

      iMod (fupd_mask_mono with "OE") as "#OE"; first done.

      (* add TraceSpine of new node *)
      iAssert (TraceSpine_elem P Γ (D3' γ) L1 (L1'.*1)
                                (length L + 1)%nat (Zpos x,v') j γ V_l)%I
        with "[jWP oDW]" as "TSj".
      { iSplit.
        - iPureIntro. split; [by apply Pos2Z.is_pos| repeat split].
          + rewrite fmap_app /=. apply (list_lookup_middle (L'.*1)).
            by rewrite fmap_length -(fmap_length snd L') EqL app_length /=.
          + by rewrite Pos2Z.id.
          + by rewrite Hj.
          + rewrite app_length /=. omega.
        - iRight. iSplitL ""; first (iPureIntro; omega). iFrame "jWP".
          rewrite -rcu_ZplusPos; last apply Pos2Z.is_pos.
          rewrite Pos2Z.id.
          iFrame "oDW".
          iIntros "!#". iIntros (t). iViewUp. iIntros (Ht).
          rewrite -vPred_big_opL_fold.
          iApply (big_sepL_elem_of with "OE"). by apply seq_elem_of. }

      (* update remaining TraceSpine to D3' L1 L1' *)
      iAssert (vPred_big_opL L (λ k a,∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ) L1 (L1'.*1) k a j0 γ0)%VP V_l)
         (* ([∗ list] k↦a ∈ L, ∃ j0 γ0,
                  TraceSpine_elem P Γ (D3' γ) L1 (L1'.*1) k a j0 γ0)%VP V_l) *)
              with "[TS]" as "TS".
      { iApply (big_sepL_mono with "TS").
        iIntros (k a Hk). simpl.
        assert (Hk2 := lookup_lt_Some _ _ _ Hk).
        iIntros "TE". iDestruct "TE" as (j0 γ0) "(TF & TR)".
        iExists j0, γ0. iDestruct "TF" as %(TF1 & TF2 & TF4 & TF5 & TF6).
        assert (Hj01: j0 ≠ j).
        { move => ?. subst j0. by apply elem_of_list_lookup_2 in TF2. }
        assert (Hj02: j0 ≠ i).
        { move => ?. subst j0.
          rewrite (NoDup_lookup _ _ (length L + 0) _ ND TF2) in Hk2;
            [omega|apply H1]. }
        assert (HHj0 := HHD j0 Hj02 Hj01).
        iSplitL "".
        - iPureIntro. split; [auto|repeat split].
          + rewrite fmap_app /=. by apply (lookup_app_l_Some _ _ _ _ TF2).
          + by rewrite (HID _ Hj01).
          + move => EqL1.
            assert (Llt:= plus_lt_compat_r _ _ 1 Hk2).
            rewrite EqL1 app_length /= in Llt. omega.
          + move => _. rewrite HHj0. destruct TF6 as [j' [TF61 TF62]].
            * rewrite app_length. by apply plus_lt_compat_r.
            * exists j'. split; last auto.
              rewrite fmap_app /=. by apply lookup_app_l_Some.
        - by rewrite {2}/Trace_own_head {2}/Trace_own_tail HHj0. }

      (* link new node to last node *)
      rewrite rcu_ZplusPos; last apply H1.

      iApply (rcuNodeAppend_link_write Γ D3 (D3' γ) p v i x γi γ v' j L (L'.*1) Hi'
              with "[] kI kS [$OE $SN' $Last]");
        [auto| |by rewrite HID|done|iFrame (H1)|].
      { by rewrite fmap_length -(fmap_length snd L') EqL app_length. }

      iNext. iViewUp. iIntros "kS (TP & PS & oHR)".
      iAssert (vPred_big_opL L1 (λ k a,
                ∃ j0 γ0, TraceSpine_elem P Γ (D3' γ) L1 (L1'.*1) k a j0 γ0)%VP V_l)
        (* ([∗ list] k↦a ∈ L1,
                  ∃ j0 γ0, TraceSpine_elem P Γ (D3' γ) L1 (L1'.*1) k a j0 γ0)%VP V_l) *)
      with "[TS TP TSj]" as "TS".
      { rewrite -!vPred_big_opL_fold big_sepL_app
                big_sepL_cons big_sepL_singleton.
        iFrame "TS". iSplitL "TP".
        - iExists i, γi. rewrite fmap_app. iFrame "TP".
        - iExists j, γ. iFrame "TSj". }

      wp_seq. iNext.

      (* establish SnapshotValid for D3' *)
      iAssert (SnapshotValid P Γ (D3' γ) V_l)%I with "[F4 oHR PS]" as "Valid'".
      { iFrame "SN'". iDestruct "F4" as "(_ & F4)".
        iDestruct "F4" as (b) "(% & F41 & #F42)".
        iDestruct "F41" as (pb γb) "(% & #F4)".
        assert (Hb: b ∈ base (D3' γ)).
        { rewrite /base -BE elem_of_of_list. by apply hd_error_some_elem. }
        destruct (WD3' γ) as [B1 _].
        assert (Hb': ∀ b', b' ∈ basel (D3' γ) → b' = b).
        { move => b' In'. apply (size_singleton_inv _ _ _ B1); last done.
          by rewrite /base /base' elem_of_of_list. }
        iExists b. iSplitL ""; last iSplitL "oHR".
        - iPureIntro.
          destruct (basel (D3' γ)) as [|b' Lb'] eqn: HbD3.
          + exfalso. move : HbD3 B1 .
            rewrite /base /basel /base'. move => -> //.
          + simpl. f_equal. apply Hb'. by left.
        - iExists pb, γb. iSplitR "oHR"; first iPureIntro.
          + rewrite HID ; first done.
            apply hd_error_some_elem in H4.
            move => ?. subst b. apply (is_fresh (dom (gset _) D3)).
            move : H4. rewrite elem_of_list_filter elem_of_of_list.
            by move => [_].
          + iDestruct "oHR" as "#oHR".
            destruct BL' as [b' [Eqb' Inb']].
            assert (b' = b).
            { apply Hb'. move : Inb'. by rewrite /base BE elem_of_of_list. }
            subst b'. destruct H1 as (_&Eqi&Eqγi&_).
            case (decide (length L = 0)%nat) => [EqL1|NEqL].
            * assert (LNil := nil_length_inv _ EqL1).
              rewrite LNil /= in HHd. inversion HHd as [Hp].
              rewrite EqL1 /= in Eqi. rewrite Eqi in Eqb'. inversion Eqb'.
              subst b. destruct H5 as [H5 _]. rewrite H5 in Eqγi.
              inversion Eqγi.
              iApply ("oHR" with "[]"); done.
            * assert (i ≠ b).
              { move => ?. subst i.
                assert(EqL0:= NoDup_lookup _ _ _ _ ND Eqi Eqb'). omega. }
              rewrite (HHD b); [iExact "F4"|auto|].
              move => ?. subst b. apply (is_fresh (dom (gset _) D3)).
              apply hd_error_some_elem in H4.
              move : H4. rewrite elem_of_list_filter elem_of_of_list.
              by move => [_].
        - iDestruct "PS" as "#PS".
          iIntros "!#". iIntros (k). iViewUp. iIntros (Hk).
          case (decide (k = i)) => [Eqk1|NEqk1];
            last case (decide (k = j)) => [Eqk2|NEqk2].
          + subst k. destruct H1 as (_&_&H1&_).
            iExists (Z.to_pos p), γi. rewrite (HID _ H2). iFrame (H1) "PS".
          + subst k. iExists x,γ. iFrame (HIj) "PSeen".
          + iDestruct ("F42" $! k with "[%] [%]") as (pk γk) "FP";
              [reflexivity|..].
            { move : Hk. rewrite 2!elem_of_difference.
              move => [Lk ?]. split; last auto.
              apply (elem_of_rcu_live'_noupdate _
                        ((i, Abs j)::(j, Null)::nil) (D3.1)); last auto.
              move => ?. rewrite elem_of_cons elem_of_list_singleton.
              move => [->|->]//. }
            iExists pk, γk. by rewrite (HHD _ NEqk1 NEqk2) (HID _ NEqk2). }

      (* re-establish WriterSafe *)
      wp_op. iNext.
      iApply ("Post" with "[] kS [-]"); first auto.
      iExists Γ, D1, D2, (D3' γ). iFrame "F1 F2 MS' F6".
      iSplitL "".
      - iPureIntro. destruct H as [? [? ?]].
        split; [auto|split]; [auto|etrans]; eauto.
      - iFrame (SD3') "Valid'". iSplitL ""; [iPureIntro|].
        + rewrite (app_assoc _ ((p,v)::nil)).
          apply (lookup_app_l_Some _ _ _ _ HHd).
        + iExists L1'. iFrame "TS". iPureIntro. rewrite /L1'.
          repeat split.
          * rewrite fmap_app EqL /=. by rewrite app_assoc_reverse.
          * rewrite fmap_app. apply NoDup_app.
            repeat split; [auto| |apply NoDup_singleton].
            move => k Ink /elem_of_list_singleton Eqk. by subst k.
          * destruct BL' as [b' [Eqb' Inb']].
            exists b'. rewrite fmap_app.
            split; first by apply lookup_app_l_Some.
            by rewrite /base -BE.
          * rewrite fmap_app.
            move => k /elem_of_app [Ink|/elem_of_list_singleton ->].
            { case (decide (i = k)) => [<-//|NEq].
              rewrite /live.
              apply (elem_of_rcu_live'_noupdate _
                      ((i, Abs j)::(j, Null)::nil) (D3.1)).
              - move => ? /elem_of_cons [->//|/elem_of_list_singleton -> ?].
                by subst k.
              - by apply ALive. }
            { apply elem_of_rcu_live'_hist'. rewrite elem_of_rcu_hist_dom.
              rewrite /hist in Hj. rewrite Hj. set_solver+. }
    Qed.

  End proof.

End RCU.
