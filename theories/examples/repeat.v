From iris.base_logic Require Import upred primitive derived.
From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From igps Require Export notation wp_tactics.
Import uPred.

(* Definition repeat (e: base.expr): base.expr := *)
Notation repeat e :=
  ((rec: "f" <> :=
     let: "v" := e in
      if: "v" = #false
      then "f" #()
      else "v") #())%E.

Lemma wp_eq' `{ownPG ra_lang Σ} E π n1 n2 P Φ (b : bool) :
  bin_op_eval EqOp n1 n2 = Some (LitInt b) →
  (bin_op_eval EqOp n1 n2 = Some (LitInt true)
   → P ⊢ ▷ Φ (LitV (LitInt true), π)) →
  (bin_op_eval EqOp n1 n2 = Some (LitInt false)
   → P ⊢ ▷ Φ (LitV (LitInt false), π)) →
  P ⊢ WP (BinOp EqOp (Lit $ n1) (Lit $ n2), π) @ E {{ Φ }}.
Proof.
  intros. destruct n1, n2; simpl in *; (try discriminate);
          try (apply wp_eq; intros A;
               [apply H0; by rewrite ?bool_decide_true|
                apply H1; by rewrite ?bool_decide_false])
        ; (destruct b;
             [rewrite <-wp_bin_op => //; by apply H0|rewrite -wp_bin_op //; by apply H1]).
Qed.

Lemma wp_repeat `{ownPG ra_lang Σ} E e π Φ `{base.is_closed nil e} :
  WP ((e, π) : ra_lang.expr) @ E 
    {{ v, ∃ z, ⌜v.1 = LitV $ LitInt z⌝ 
          ∧ if decide (z = 0) 
            then ▷ ▷ (WP (repeat e, v.2) @ E {{v, Φ v }}) else ▷ ▷ Φ (#z, v.2) }}
     ⊢ WP (repeat e, π) @ E {{ Φ }}.
Proof.
  iIntros "H".
  wp_rec. iNext.
  wp_bind e. iApply wp_mono; [|iExact "H"].
  iIntros (v) "H". iDestruct "H" as (l) "(% & R)". rewrite H0 {H0}.
  wp_lam. iNext.
  wp_op => [->{l}|?].
  - erewrite decide_True; last auto.
    iNext. iApply wp_if_true. by iNext.
  - erewrite decide_False; last auto.
    iNext. iApply wp_if_false. iNext. by wp_value.
Qed.
