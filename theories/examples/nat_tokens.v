From Coq Require Import Psatz.
From iris.algebra Require Import coPset.

From igps Require Import arith infrastructure.

(** Tokens with infinite number to be used in various examples **)

Definition Pos_upto_set (up: nat) : gset positive :=
  match up with
  | O => ∅
  | S i => map_gset Pos.of_nat (seq_set 1 up)
  end.

Definition coPset_from_ex (i: nat): coPset
  := ⊤ ∖  coPset.of_gset (Pos_upto_set i).

Lemma coPset_from_ex_gt i p:
  p ∈ coPset_from_ex i ↔ (i < Pos.to_nat p)%nat.
Proof.
  rewrite elem_of_difference coPset.elem_of_of_gset.
  destruct i as [|i].
  - split; move => _; [by apply Pos2Nat.is_pos|done].
  - rewrite elem_of_map_gset. split.
    + move => [_ /= NIn].
      apply: not_ge => ?. apply: NIn.
      exists (Pos.to_nat p). set_unfold; rewrite elem_of_seq_set. lia'.
    + move => Lt. split; first done.
      move => [n [Eqn /elem_of_seq_set [Ge1 Lt2]]].
      subst. lia'.
Qed.

Lemma coPset_from_insert i:
  coPset_from_ex i ≡ {[Pos.of_succ_nat i]} ∪ coPset_from_ex (S i).
Proof.
  move => p. rewrite elem_of_union elem_of_singleton !coPset_from_ex_gt. lia'.
Qed.

Lemma coPset_from_disjoint i:
  {[Pos.of_succ_nat i]} ⊥ coPset_from_ex (S i).
Proof.
  apply disjoint_singleton_l. rewrite coPset_from_ex_gt SuccNat2Pos.id_succ. lia.
Qed.


Lemma coPset_of_gset_union X Y :
  coPset.of_gset (X ∪ Y) = coPset.of_gset X ∪ coPset.of_gset Y.
Proof.
  apply leibniz_equiv.
  move => ?. by rewrite elem_of_union !coPset.elem_of_of_gset elem_of_union.
Qed.

Lemma coPset_of_gset_difference X Y:
  coPset.of_gset (X ∖ Y) = coPset.of_gset X ∖ coPset.of_gset Y.
Proof.
  apply leibniz_equiv. move => x.
  by rewrite elem_of_difference !coPset.elem_of_of_gset elem_of_difference.
Qed.

Lemma coPset_of_gset_difference_union (X Y Z: gset positive)
  (Disj: Y ⊥ Z) (Sub: Y ⊆ X):
  coPset.of_gset (X ∖ Z) = coPset.of_gset (X ∖ (Y ∪ Z)) ∪ coPset.of_gset Y.
Proof.
  apply leibniz_equiv. move => x.
  rewrite elem_of_union !coPset.elem_of_of_gset
          !elem_of_difference elem_of_union.
  split.
  - move => [InX NIn]. case (decide (x ∈ Y)) => [?|NInY].
    + by right.
    + left. split; first auto. by move => [|].
  - set_solver.
Qed.

Lemma coPset_of_gset_difference_disjoint (X Y Z: gset positive):
  coPset.of_gset (X ∖ (Y ∪ Z)) ⊥ coPset.of_gset Y.
Proof.
  rewrite elem_of_disjoint.
  move => x. rewrite !coPset.elem_of_of_gset elem_of_difference elem_of_union.
  set_solver.
Qed.

Lemma coPset_of_gset_top_difference (X Y: gset positive) (Disj: X ⊥ Y):
  ⊤ ∖  coPset.of_gset X = (⊤ ∖  coPset.of_gset (Y ∪ X)) ∪ coPset.of_gset Y.
Proof.
  apply leibniz_equiv. move => x.
  rewrite elem_of_union !elem_of_difference
          !coPset.elem_of_of_gset elem_of_union.
  split.
  - move => [_ NIn]. case (decide (x ∈ Y)) => [|?]; [by right|left]. set_solver.
  - set_solver.
Qed.

Lemma coPset_of_gset_top_disjoint (X Y: gset positive):
  (⊤ ∖  coPset.of_gset (Y ∪ X)) ⊥ coPset.of_gset Y.
Proof.
  rewrite elem_of_disjoint.
  move => x. rewrite elem_of_difference coPset_of_gset_union. set_solver.
Qed.

Lemma coPset_of_gset_empty :
  coPset.of_gset ∅ = ∅.
Proof. by apply leibniz_equiv. Qed.

Lemma coPset_difference_top_empty:
  ⊤ ∖ ∅ = ⊤.
Proof. by apply leibniz_equiv. Qed.
