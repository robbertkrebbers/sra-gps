From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import coPset.

From igps Require Export notation malloc escrows bigop.
From igps Require Import persistor viewpred weakestpre proofmode arith.
From igps.examples Require Import nat_tokens protocols.
From igps.gps Require Import singlewriter.

Import uPred.

(** This is the formalization of Circular Buffer from the original GPS paper.
  This proof is further simplified by using single writer protocols. **)

Section CircBuffer.

  Context (n: Z) (nGt1 : 1 < n).

  Local Notation wi := 0.
  Local Notation ri := 1.
  Local Notation b := 2.

  Definition newBuffer : base.val :=
    λ: <>,
      let: "q" := malloc (#n + #b) in
        ["q" + #ri]_na <- #0 ;;
        ["q" + #wi]_na <- #0 ;;
        "q".

  Definition tryProd : base.val :=
    λ: "q" "x",
      let: "w" := ["q" + #wi]_at in
      let: "r" := ["q" + #ri]_at in
      let: "w'" := ("w" + #1) `mod` #n in
        if: "w'" = "r"
        then #0
        else
          ["q" + #b + "w"]_na <- "x" ;;
          ["q" + #wi]_at <- "w'" ;;
          #1.

  Definition tryCons : base.val :=
    λ: "q",
      let: "w" := ["q" + #wi]_at in
      let: "r" := ["q" + #ri]_at in
        if: "w" = "r"
        then #0
        else
          let: "x" := ["q" + #b + "r"]_na in
            ["q" + #ri]_at <- (("r" + #1) `mod` #n) ;;
            "x".

  Notation circBufR := (prodUR (coPset_disjUR) (coPset_disjUR)).

  Class cbG Σ := CbG { cb_tokG :> inG Σ circBufR }.
  Definition cbΣ : gFunctors := #[GFunctor (constRF circBufR)].

  Instance subG_cbΣ {Σ} : subG cbΣ Σ → cbG Σ.
  Proof. solve_inG. Qed.

  Section Interpretation.
    Context `{fG: !foundationG Σ, aG: !absViewG Σ, cirbG: !cbG Σ}.

    Set Default Proof Using "Type".
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).

    Context (P: Z → vPred).

    Definition pTok (γ: gname) (i: nat)
      := own γ (CoPset {[Pos.of_succ_nat i]} , CoPset ∅).

    Definition pToks_from (γ: gname) (i: nat)
      := own γ (CoPset $ coPset_from_ex i, CoPset ∅).

    Definition cTok (γ: gname) (i: nat)
      := own γ (CoPset ∅, CoPset {[Pos.of_succ_nat i]}).

    Definition cToks_from (γ: gname) (i: nat)
      := own γ (CoPset ∅, CoPset $ coPset_from_ex i).

    Lemma pTok_exclusive γ i: pTok γ i ∗ pTok γ i ⊢ False.
    Proof.
      iIntros "Tok". rewrite -own_op pair_op.
      iDestruct (own_valid with "Tok") as %[Valid _]. simpl in Valid.
      rewrite -> coPset_disj_valid_op, disjoint_singleton_l in Valid.
      exfalso. by apply Valid, elem_of_singleton.
    Qed.

    Lemma cTok_exclusive γ i: cTok γ i ∗ cTok γ i ⊢ False.
    Proof.
      iIntros "Tok". rewrite -own_op pair_op.
      iDestruct (own_valid with "Tok") as %[_ Valid]. simpl in Valid.
      rewrite -> coPset_disj_valid_op, disjoint_singleton_l in Valid.
      exfalso. by apply Valid, elem_of_singleton.
    Qed.

    Lemma pToks_from_extract γ i :
      pToks_from γ i ⊢ pTok γ i ∗ pToks_from γ (S i).
    Proof.
      iIntros "Toks".
      rewrite /pToks_from /pTok. rewrite -own_op pair_op.
      rewrite !/op !/cmra_op /=.
      case (decide ({[Pos.of_succ_nat i]} ⊥ coPset_from_ex (S i))) => [_|ND];
        last first.
        { exfalso. apply ND, coPset_from_disjoint. }
      iApply (own_mono with "Toks"). apply prod_included.
      by rewrite /= !coPset_disj_included union_idemp -coPset_from_insert.
    Qed.

    Lemma cToks_from_extract γ i :
      cToks_from γ i ⊢ cTok γ i ∗ cToks_from γ (S i).
    Proof.
      iIntros "Toks".
      rewrite -own_op pair_op !/op !/cmra_op /=.
      case (decide ({[Pos.of_succ_nat i]} ⊥ coPset_from_ex (S i))) => [_|ND];
        last first.
        { exfalso. apply ND, coPset_from_disjoint. }
      iApply (own_mono with "Toks"). apply prod_included.
      by rewrite /= !coPset_disj_included union_idemp -coPset_from_insert.
    Qed.

    Lemma pToks_cToks_split γ i:
      own γ (CoPset $ coPset_from_ex i, CoPset $ coPset_from_ex i)
      ⊢ pToks_from γ i ∗ cToks_from γ i.
    Proof.
      iIntros "Toks".
      rewrite -own_op pair_op !/op !/cmra_op /=.
      case (decide (coPset_from_ex i ⊥ ∅)) =>[_|ND]; last first.
        { exfalso. apply ND. set_solver+. }
      case (decide (∅ ⊥ coPset_from_ex i)) =>[_|ND]; last first.
        { exfalso. apply ND. set_solver+. }
      iApply (own_mono with "Toks"). apply prod_included.
      rewrite /= !coPset_disj_included. set_solver+.
    Qed.

    Definition loc_at (q: loc) (i: nat) : loc :=
      (ZplusPos (b + i `mod` n) q).

    Definition PE (γ: gname) (q: loc) (i: nat): vPred
      := [es ☐ pTok γ i ⇝ own_loc (loc_at q i)].

    Definition CE (γ: gname) (q: loc) (i: nat): vPred
      := [es ☐ cTok γ i ⇝ ∃ v, P v ∗ (loc_at q i) ↦ v].

    Definition PP (γq: gname * loc) (i: nat) (v: Z): vPred
      := □ (■ (v = i `mod` n)
            ∗ ∀ (j: nat), ■ (j < i) → CE (γq.1) (γq.2) j).

    Definition CP (γq: gname * loc) (i: nat) (v: Z): vPred
      := □ (■ (v = i `mod` n)
            ∗ ∀ (j: nat), ■ (j < i + n) → PE (γq.1) (γq.2) j).

    Global Instance PP_persistent γ i v V: PersistentP (PP γ i v V) := _.
    Global Instance CP_persistent γ i v V: PersistentP (CP γ i v V).
    Proof. by iIntros "#?". Qed.

    Definition prodInt x γ : interpC Σ natProtocol :=
    (λ b _ s v, if b then True else PP (x,γ) s v)%VP.
    Definition consInt x γ : interpC Σ natProtocol :=
    (λ b _ s v, if b then True else CP (x,γ) s v)%VP.

    Lemma cb_escrows_list_alloc γ q V (ni: nat):
      (ni ≤ n) →
      ([∗ list] i ∈ seq (Z.to_nat b) ni, (ZplusPos i q) ↦ ?)%VP V
      ={↑escrowN}=∗ ([∗ list] i ∈ seq 0 ni, PE γ q i)%VP V.
    Proof.
      move => Le. iIntros "OL".
      iApply fupd_big_sepL.
      rewrite -vPred_big_opL_fold
        (Nat2Z.id 2) -2!fmap_seq -list_fmap_compose big_op.big_sepL_fmap.
      iApply (big_op.big_sepL_mono with "OL").
      iIntros (? i H) "ol".
      apply lookup_seq_inv in H as [H1 H2]. simpl in H1. subst k.
      rewrite (_: Z.of_nat ((S ∘ S) i) = b + i `mod` n)%Z.
      - destruct (escrow_alloc (☐ pTok γ i) (own_loc (loc_at q i))) as [EA].
        by iApply (EA V with "[$ol]").
      - rewrite 2!Nat2Z.inj_succ Zplus_succ_r_reverse Zmod_small; omega.
    Qed.

  End Interpretation.

  Section proof.
    Context `{aG : absViewG Σ} {cirbG : cbG Σ}.
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).

    Context (P: Z → vPred)
            `{fG : foundationG Σ}
            `{gpG: !gpsG Σ natProtocol _}
            `{persG : persistorG Σ}.

    Set Default Proof Using "Type".

    Definition Prod (q: loc) : vPred
      := ∃ (γ: gname) (i j: nat),
            ■ (i < j + n)
          ∗ [XP ZplusPos wi q in i | prodInt P γ q]_W
          ∗ [XP ZplusPos ri q in j | consInt γ q]_R
          ∗ ☐ pToks_from γ i.

    Definition Cons (q: loc) : vPred
      := ∃ (γ: gname) (i j: nat),
            ■ (j ≤ i)
          ∗ [XP ZplusPos wi q in i | prodInt P γ q]_R
          ∗ [XP ZplusPos ri q in j | consInt γ q]_W
          ∗ ☐ cToks_from γ j.

    Lemma newBuffer_spec:
      {{{{ ☐ PersistorCtx }}}}
          (newBuffer #())
      {{{{ (q: loc), RET #q; Prod q ∗ Cons q }}}}.
    Proof using nGt1.
      intros. iViewUp; iIntros "#kI kS #kP Post".
      wp_seq. iNext. wp_op. iNext. wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].

      iNext. iViewUp. iIntros (q) "kS oLs".

      iMod (own_alloc (CoPset $ coPset_from_ex 0, CoPset $ coPset_from_ex 0))
        as (γ) "Toks"; first done.

      iDestruct (pToks_cToks_split with "Toks") as "(Prods & Cons)".

      rewrite (_ : Z.to_nat (n + b) = S $ S $ Z.to_nat n); last first.
        { rewrite Z2Nat.inj_add; [|omega|omega].
          rewrite (_: Z.to_nat 2 = 2%nat); last auto. omega. }
      rewrite -vPred_big_opL_fold 2!big_op.big_sepL_cons
        vPred_big_opL_fold -/seq.
      iDestruct "oLs" as "(owi & ori & oLs)".

      iAssert (|={⊤}=> ([∗ list] i ∈ seq 0 (Z.to_nat n), PE γ q i)%VP V_l)%I
        with "[oLs]" as ">#PEs".
      { iApply (fupd_mask_mono (↑escrowN)); first auto.
        iApply (cb_escrows_list_alloc with "oLs"). lia'. }

      wp_let. iNext. wp_op. iNext. wp_bind ([_]_na <- _)%E.

      iApply (GPS_SW_Init (consInt γ q) (0)%nat
                  ([XP ZplusPos 1 q in 0%nat | consInt γ q ]_W)
                  with "[%] kI kS [$ori]"); [done|done|done| |].
      { iSplitL ""; first done. iViewUp. iIntros "$". iModIntro.
        iSplitL ""; [auto|iNext]. iSplitL ""; iIntros "!#".
        - by rewrite Zmod_0_l.
        - simpl. iIntros (j). iViewUp. iIntros "%".
          iDestruct (big_op.big_sepL_lookup with "PEs") as "$".
          rewrite lookup_seq; [done|]. apply Nat2Z.inj_lt. lia'. }

      iNext. iViewUp. iIntros "kS cW".

      wp_seq. iNext. wp_op. iNext. wp_bind ([_]_na <- _)%E.


      iApply (GPS_SW_Init (prodInt P γ q) (0)%nat
                          ([XP ZplusPos 0 q in 0%nat | prodInt P γ q ]_W)
        with "[%] kI kS [$owi]"); [done|done|done|..].
      { iSplitL ""; first done. iViewUp. iIntros "$". iModIntro.
        iSplitL ""; [auto|iNext]. iSplitL ""; iIntros "!#".
        - by rewrite Zmod_0_l.
        - iIntros (j V3) "_ %". lia'. }

      iNext. iViewUp; iIntros "kS pW".
      wp_seq. iNext.
      wp_value. iApply ("Post" with "[%] kS [Prods Cons cW pW]"); [done|].

      (* TODO: fix implicits or make it work with proofmode *)
      destruct (GPS_SW_Writer_Reader (l := ZplusPos 1 q) (s := 0%nat) 
                  (IP := consInt γ q)) as [cW].
      iDestruct (cW with "cW") as "[cW cR]"; [done|].

      destruct (GPS_SW_Writer_Reader (l := q) (s:= 0%nat) (IP := prodInt P γ q))
                  as [pW].
      iDestruct (pW with "pW") as "[pW pR]"; [done|].
      iSplitL "Prods pW cR"; iExists γ, O, O.
      - iFrame "Prods pW cR". iPureIntro. omega.
      - by iFrame "Cons cW pR".
    Qed.

    Lemma tryProd_spec q v:
      {{{{ Prod q ∗ P v }}}}
          (tryProd #q #v)
      {{{{ (b: Z), RET (#b);
            Prod q ∗ (■ (b ≠ 0) ∨ P v) }}}}.
    Proof using nGt1.
      intros; iViewUp; iIntros "#kI kS (Prod & P) Post".
      iDestruct "Prod" as (γ i j0) "(% & pW & cR & pToks)".

      wp_lam. iNext.
      wp_value. wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iApply (GPS_SW_Read_ex with "[%] kI kS [$pW]"); [done|done|done|..].
      { iNext. iIntros (? ?). iViewUp. unfold prodInt. iIntros "#?".
        iModIntro. by iSplitL. }

      iNext. iViewUp; iIntros (w) "kS (pW & #CEs)".
      iDestruct "CEs" as "(% & CEs)".


      wp_let. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iApply (GPS_SW_Read (consInt γ q) True (CP (γ,q))
                with "[%] kI kS [$cR]"); [done|done|done|..].
      { iNext. iSplitL.
        - iIntros (?). iViewUp. iIntros "_". iLeft. by iIntros (??) "_ (? & _)".
        - iIntros (? ? ?). iViewUp. iIntros "[? #?]". iModIntro. by iSplit. }

      iNext. iViewUp. iIntros (j r) "kS (% & #cR2 & #PEs)".
      iDestruct "PEs" as "(% & PEs)".

      wp_let. iNext. wp_op. iNext. wp_op. iNext. wp_let. iNext.
      wp_op => [Eqr|NEqr]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%]kS [pToks pW P]"); [done|].
        iSplitR "P"; last by iRight.
        iExists γ, i, j.
        iFrame "pToks pW cR2". iPureIntro.
        eapply Zlt_le_trans; first exact H. by apply Zplus_le_compat_r, inj_le.

      - iApply wp_if_false. iNext.
        wp_bind ([_]_na <- _)%E. wp_op. iNext. wp_op. iNext.

        assert (i + 1 < j + n). (* arithmetic *)
          { rewrite H0 H2 in NEqr. cbn in H1.
            assert (HLe : Z.succ i ≤ j + n) by omega.
            apply Zle_lt_or_eq in HLe as [Lt|Eq]; first auto.
            exfalso. apply NEqr.
            rewrite Z.add_mod_idemp_l; last omega.
            by rewrite -/(Z.succ _) Eq -{1}(Z.mul_1_l n) Z_mod_plus_full. }

        iDestruct (pToks_from_extract with "pToks") as "(iTok & pToks)".
        iAssert (PE γ q i _)%VP as "PEi".
        { iApply ("PEs" with "[]"); first auto. iPureIntro. omega. }

        iDestruct (escrow_apply (☐ pTok γ i) (own_loc (loc_at q i))
          with "[]") as "EA".
        { iIntros (??) "_ pTok". iApply (pTok_exclusive with "pTok"). }

        iSpecialize ("EA" with "[] [$PEi $iTok]"); first auto.

        iMod (fupd_mask_mono with "EA") as ">oL"; first auto.

        (* arithmetic *)
        rewrite (_: ZplusPos w (ZplusPos b q) = loc_at q i); last first.
        { rewrite H0 /loc_at /ZplusPos Z2Pos.id; [|lia']. f_equal. omega. }

        iApply (na_write with "[%] kI kS oL"); [done|done|].

        iNext. iViewUp. iIntros "kS oL".

        destruct (escrow_alloc (☐ cTok γ i) (∃ v : Z, P v ∗ loc_at q i ↦ v))
          as [EA].

        iDestruct (EA with "[oL P]") as "CEi"; [done|..].
          { iNext. iExists v. iFrame "P oL". }
        iMod (fupd_mask_mono with "CEi") as "#CEi"; first auto.

        wp_seq. iNext. wp_bind ([_]_at <- _)%E. wp_op. iNext.
        rewrite H0 Z.add_mod_idemp_l ; last omega.

        set Px : vPred := (□ ∀ j1 : nat, ■ (j1 < (i + 1)) → CE P γ q j1)%VP.
        iApply (GPS_SW_ExWrite (prodInt P γ q) (i + 1)%nat
                        Px (λ s', [XP ZplusPos 0 q in s' | prodInt P γ q ]_W)%VP
                  with "[%] kI kS [$pW]"); [|done|done|done|..].
        { rewrite Nat.add_1_r; apply Nat.le_succ_diag_r. }
        { iSplitR "". 
          - iNext. iIntros (v5). iViewUp. iIntros "(#Px & _ & $)".
            iModIntro. iNext. iSplitL ""; first done.
            iSplitL ""; iIntros "!#"; [by rewrite Nat2Z.inj_add|].
            cbn. iIntros (k). iViewUp. iIntros (Hk).
            iApply ("Px" with "[%] []" ); [done|]. iPureIntro. lia'.
          - iIntros "!#". iIntros (k). iViewUp; iIntros "%".
            apply Zlt_succ_le, Zle_lt_or_eq in H4 as [Lt|Eq].
            + by iApply ("CEs" with "[%] [%]").
            + apply Nat2Z.inj in Eq. by rewrite Eq. }

        iNext. iViewUp. iIntros "kS (_ & pW)".
        wp_seq. iNext. wp_value.
        iApply ("Post" with "[%] kS [pToks pW]"); [done|].
        iSplitR ""; last by iLeft.
        iExists γ, (i + 1)%nat, j. iSplitL ""; first by rewrite Nat2Z.inj_add.

        rewrite -Nat.add_1_r. iFrame "pW pToks cR2".
    Qed.

    Lemma tryCons_spec q:
      {{{{ Cons q }}}}
          (tryCons #q)
      {{{{ (v: Z), RET (#v);
           Cons q ∗ (■ (v = 0) ∨ P v) }}}}.
    Proof using nGt1.
      intros; iViewUp; iIntros "#kI kS Cons Post".
      iDestruct "Cons" as (γ i0 j) "(% & pR & cW & cToks)".

      wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iApply (GPS_SW_Read (prodInt P γ q) True (PP P (γ,q))
                with "[%] kI kS [$pR]"); [done|done|done|..].
      { iNext. iSplitL.
        - iIntros (?). iViewUp. iIntros (?). iLeft.
          iIntros (??) "_ (? & _)". by auto.
        - iIntros (? ? ?). iViewUp. iIntros "[? #?]". iModIntro. by iSplitL. }

      iNext. iViewUp. iIntros (i w) "kS (% & pR & #CEs)".
      iDestruct "CEs" as "(% & CEs)".

      wp_let. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      iApply (GPS_SW_Read_ex
                with "[%] kI kS [$cW]"); [done|done|done|..].
      { iNext. iIntros (? ?). iViewUp. iIntros "?". iModIntro. by iSplit. }

      iNext. iViewUp. iIntros (r) "kS (cW & #PEs)".
      iDestruct "PEs" as "(% & PEs)".

      wp_let. iNext. wp_op => [Eqr|NEqr]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%] kS [cToks pR cW]"); [done|].
        iSplitR ""; last by iLeft.
        iExists γ, i, j.
        iFrame "cToks pR cW". iPureIntro. etrans; eauto.
        by apply inj_le.

      - iApply wp_if_false. iNext.
        wp_bind ([_]_na)%E.
        wp_op. iNext. wp_op. iNext.

        assert (j < i). (* arithmetic *)
          { assert (HLe: j ≤ i). { etrans; [ exact H | by apply inj_le]. }
            apply Zle_lt_or_eq in HLe as [?|Eq]; first auto.
            exfalso. apply NEqr. by rewrite H1 H2 Eq. }

        iDestruct (cToks_from_extract with "cToks") as "(jTok & cToks)".

        iDestruct ("CEs" $! j with "[%] [%]") as "CEj"; [done|done|].

        iDestruct (escrow_apply (☐ cTok γ j) (∃ v, P v ∗ loc_at q j ↦ v)
          with "[]") as "EA".
        { iIntros (??) "_ cTok". iApply (cTok_exclusive with "cTok"). }

        iSpecialize ("EA" with "[] [$CEj $jTok]"); [done|].

        iMod (fupd_mask_mono with "EA") as (v) "(P & >oL)"; [done|].

        (* arithmetic *)
        rewrite (_: ZplusPos r (ZplusPos b q) = loc_at q j); last first.
        { rewrite H2 /loc_at /ZplusPos Z2Pos.id; [|lia']. f_equal. omega. }

        iApply (na_read with "[%] kI kS oL"); [done|done|].

        iNext. iViewUp. iIntros (x) "kS (% & oL)".

        destruct (escrow_alloc (☐ pTok γ (j + Z.to_nat n)%nat) 
                               (own_loc (loc_at q (j + Z.to_nat n)%nat)))
                  as [EA].
        iDestruct (EA with "[oL]") as "PEj"; [done| |].
          { rewrite (_: loc_at q (j + Z.to_nat n) = loc_at q j).
            - iFrame "oL".
            - rewrite /loc_at. do 2 f_equal.
              rewrite Nat2Z.inj_add Z2Nat.id; last omega.
              by rewrite -{1}(Z.mul_1_l n) Z_mod_plus_full. }
        iMod (fupd_mask_mono with "PEj") as "#PEj"; first auto.

        wp_let. iNext. wp_bind ([_]_at <- _)%E. wp_op. iNext.
        wp_op. iNext. wp_op. iNext.

        rewrite H2 Z.add_mod_idemp_l; last omega.

        set Px : vPred := (□ ∀ j1 : nat, ■ (j1 < (j + 1) + n)
                                    → PE γ q j1)%VP.

        iApply (GPS_SW_ExWrite (consInt γ q) (j + 1)%nat
                       Px (λ s', [XP ZplusPos 1 q in s' | consInt γ q ]_W)%VP
                  with "[%] kI kS [$cW]"); [|done|done|done|..].
        { rewrite Nat.add_1_r; apply Nat.le_succ_diag_r. }
        { iSplitR "".
          - iNext. iIntros (v5). iViewUp. iIntros "(#Px & _ & $)".
            iModIntro. iNext. iSplitL ""; first auto.
            iSplitL ""; iIntros "!#"; [by rewrite Nat2Z.inj_add|auto].
            cbn. iIntros (k). iViewUp. iIntros (Hk).
            iApply ("Px" with "[%] []" ); [done|]. iPureIntro. lia'.
          - iIntros "!#". iIntros (k). iViewUp. iIntros "%".
            rewrite Z.add_succ_l in H5.
            apply Zlt_succ_le, Zle_lt_or_eq in H5 as [Lt|Eq].
            + by iApply ("PEs" with "[%] [%]").
            + rewrite -(Nat2Z.id j) -Z2Nat.inj_add -?Eq ?Nat2Z.id;
                [done|..]; omega. }

        iNext. iViewUp. iIntros "kS (_ & cW)".
        wp_seq. iNext. wp_value. subst x.
        iApply ("Post" with "[%] kS [cToks pR cW P]"); [done|].
        iSplitR "P"; last by iRight.
        iExists γ, i, (j + 1)%nat. iSplitL ""; first (iPureIntro; lia').

        rewrite -Nat.add_1_r. iFrame "cW cToks pR".
    Qed.

  End proof.

End CircBuffer.
