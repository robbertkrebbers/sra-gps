From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import excl.
From iris.base_logic.lib Require Import invariants.

From igps.base Require Import ghosts alloc fork na_write na_read at_write at_read.
From igps.examples Require Import unit_token.
From igps Require Export repeat notation wp_tactics proofmode.
Import uPred.

Definition message_passing : base.expr :=
  let: "x" := Alloc in
  let: "y" := Alloc in
    ["x"]_na <- #0 ;;
    ["y"]_na <- #0 ;;
    Fork (["x"]_na <- #37 ;; ["y"]_at <- #1) ;;
    repeat (["y"]_at) ;;
    ["x"]_na.

Definition mpN : namespace := nroot .@ "mp".

Section proof.
  Context `{fG : !foundationG Σ} `{!uTokG Σ}.

  Definition Invx (x : loc) (γ: gname) (V37 : View) : iProp Σ
    := (own γ (Excl ()) ∨ Hist x {[VInj 37, V37]} ∗ ∃ vx, Info x 1 vx)%I.

  Definition Invy (x y: loc) (γ: gname) V0 : iProp Σ
    := (∃ h, Hist y h ∗ (∃ vy, Info y 1 vy) ∗ ⌜(VInj 0, V0) ∈ h⌝ ∗
        □ (∀ V1 v1, ⌜v1 ≠ 0 ∧ (VInj v1, V1) ∈ h⌝
                    → ∃ V37, ⌜V37 ⊑ V1⌝ ∗ inv mpN (Invx x γ V37)))%I.

  Lemma message_passing_base_spec V π:
    {{{ PSCtx ∗ Seen π V }}}
        (message_passing, V)
    {{{ V' (v: Z), RET (#v, V'); ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ⌜v = 37⌝ }}}.
  Proof.
    iIntros (Φ) "(#kI & kS) Post". rewrite /message_passing.

    wp_bind Alloc.
    iApply wp_mask_mono; [auto|]; last iApply (f_alloc with "[$kI $kS]").
    iNext. iIntros (x V1 hx vx) "(% & kS & Histx & Infox & %)".
    wp_seq. iNext.

    wp_bind Alloc.
    iApply wp_mask_mono; [auto| (iApply (f_alloc with "[$kI $kS]"))].
    iNext. iIntros (y V2 hy vy) "(% & kS & Histy & Infoy & %)".
    wp_seq. iNext.

    wp_bind ([_]_na <- _)%E.
    iApply wp_mask_mono; [auto|];
      last iApply (f_write_na with "[$kI $kS $Histx]").
    { iPureIntro. eapply alloc_local_Mono; last apply H0; auto. }
    iNext. iIntros (V3 hx2) "(% & kS & Histx & %)".
    wp_seq. iNext.

    wp_bind ([_]_na <- _)%E.
    iApply wp_mask_mono; [auto|];
      last iApply (f_write_na with "[$kI $kS $Histy]").
    { iPureIntro. eapply alloc_local_Mono; last apply H2; auto. }
    iNext. iIntros (V4 hy2) "(% & kS & Histy & %)".
    wp_seq. iNext.
    destruct H4 as (H41 & H42 & H43 & H44 & H45). subst hx2.
    destruct H6 as (_ & _ & _ & _ & H65). subst hy2.

    iMod (own_alloc (Excl ())) as (γ) "Tok"; first done.
    iMod (inv_alloc mpN top (Invy x y γ V4)%I with "[Histy Infoy]") as "#Invy".
    { iNext. iExists _. iFrame "Histy".
      iSplitL "Infoy"; first by iExists _.
      iSplitL ""; first by (iPureIntro; apply elem_of_singleton).
      iIntros "!#". iIntros (V' v') "%".
      destruct H4 as [Hv' HH'].
      apply elem_of_singleton_1 in HH'. inversion HH'. by subst v'. }

    wp_bind (Fork _).
    iApply (wp_mask_mono (↑physN)); first done.
    iCombine "Histx" "Infox" as "SndT".
    iApply (f_fork _ (Hist x {[VInj 0, V3]} ∗ Info x 1 vx)%I
      with "[$kI $kS $SndT Post Tok]").
    iSplitL "Post Tok"; iNext.
    - iIntros (V') "% kS".
      wp_seq. iNext. wp_bind ((rec: "f" <> := _ ) _)%E.
      move EqV4 : {1}V4 => V5.
      have H6: V4 ⊑ V5 by subst.
      have H7: V5 ⊑ V' by subst.
      clear EqV4.
      iLöb as "IH" forall (V' H4 V5 H6 H7) "Invy ∗".
      iApply wp_repeat; first auto.
      iInv mpN as (hy') "(Histy & Infoy & >% & #Invx)" "HClose".
      iApply wp_mask_mono; last
        iApply (f_read_at with "[$kI $kS $Histy]"); first solve_ndisj.
      { iPureIntro. by exists (VInj 0), V5. }
      iNext. iIntros (v V6) "(% & kS & Histy & _ & HRead)".
      iMod ("HClose" with "[Histy Infoy]") as "_".
      { iNext. iExists hy'. iFrame "Histy Infoy". iFrame (H8) "Invx". }
      iModIntro. iExists v. iSplit; first auto.
      case (decide (v = 0)) =>[Eq0|NEq0].
      { subst v. iNext. iNext.
        iApply ("IH" $! _ with "[%] [%] [%] [$Invy] Post Tok kS").
        - by rewrite H6 H7.
        - by rewrite H6.
        - by rewrite H7.
      }

      iClear "IH Invy". iNext. iNext. wp_seq. iNext.
      iDestruct "HRead" as (V_1) "%". destruct H10 as (H81 & H82 & H83).
      iDestruct ("Invx" $! V_1 v with "[%]") as (V37) "[% #Invx2]"; first done.
      iClear "Invx".
      iInv mpN as "Disj" "HClose".
      iDestruct "Disj" as "[>Tok2|>[Histx Infox]]".
      { iExFalso. iApply unit_tok_exclusive. by iFrame. }

      iApply wp_mask_mono; last iApply (f_read_na with "[$kI $kS $Histx]");
        first solve_ndisj.
      { iSplit; iPureIntro.
        - exists (VInj 37, V37). repeat split.
          + by apply elem_of_singleton.
          + move => ? /elem_of_singleton -> //.
          + simpl. by etrans.
        - exists (VInj 37), V37. repeat split.
          + by apply elem_of_singleton.
          + by etrans. }

      iNext. iIntros (v' V7) "(% & kS & Histx & _ & Hv37)".
      iMod ("HClose" with "[Tok]") as "_". { iNext. by iLeft. }
      iDestruct "Hv37" as (V'') "(_&_&%)".
      move: H12 => [vV [/elem_of_singleton -> [_ [Eqv EqV]]]]. subst v'.
      iApply ("Post" $! V7 with "[$kS]"). iSplit; last done.
      iPureIntro. repeat (etrans; eauto).
    - iIntros (ρ V') "% _ kS [Histx Infox]".
      wp_bind ([_]_na <- _)%E.
      iApply wp_mask_mono; [auto|];
        last iApply (f_write_na with "[$kI $kS $Histx]").
      { iPureIntro.
        apply: (alloc_local_Mono); last exact: alloc_init_local. by rewrite H5. }
      iNext. iIntros (V5 hx') "(% & kS & Histx & %)".
      destruct H7 as (_&_&_&_&H7). subst hx'.
      iMod (inv_alloc mpN top (Invx x γ V5)%I with "[Histx Infox]") as "Invx".
      { iNext. iRight. iFrame "Histx". by iExists _. }
      wp_seq. iNext.
      iInv mpN as (hy') "(Histy & Infoy & >% & #Invxs)" "HClose".
      iDestruct "Invx" as "#Invx".
      iApply wp_mask_mono; last iApply (f_write_at with "[$kI $kS $Histy]");
        first solve_ndisj.
      { iPureIntro. exists (VInj 0), V4; split; [|split]; auto. by rewrite H4.  }
      iNext. iIntros (V6 hy2) "(% & kS & Histy & %)".
      iMod ("HClose" with "[Histy Infoy]") as "_"; last done.
      iNext. iExists hy2. iFrame "Histy Infoy".
      destruct H9 as (H91 & H92 & H93 & H94 & H95). iSplit.
      + iPureIntro. rewrite H93 elem_of_union. by right.
      + iIntros "!#".
        iIntros (V'' v'). iIntros (HV).
        move: HV. rewrite H93 elem_of_union elem_of_singleton.
        move => [Hv [[Eqv EqV]|HV]].
        * subst V''. iExists V5. by iFrame "Invx".
        * by iApply ("Invxs" $! _ v').
  Qed.

End proof.