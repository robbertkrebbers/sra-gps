From iris.base_logic.lib Require Import iprop own.
From iris.algebra Require Import auth gmap coPset.
From igps Require Import infrastructure machine.

(** Logical data structure needed for the RCU proof,
      see rcu.v for more info about the original paper *)

Section RCUData.

  Definition aloc := positive.

  Inductive AbsLoc : Type :=
    | Null: AbsLoc
    | Abs: aloc -> AbsLoc
    | Dead : AbsLoc.

  Definition AbsLoc_to_sum : AbsLoc → _
    := λ al, match al with
             | Null => inl () 
             | Dead => inr (inl ())
             | Abs l => inr (inr l)
             end.
  Definition sum_to_AbsLoc : _ → AbsLoc
    := λ s, match s with
            | inl () => Null 
            | inr (inl ()) => Dead 
            | inr (inr v) => Abs v
            end.

  Global Instance AbsLoc_eqdec : EqDecision AbsLoc :=
    inj_dec_eq AbsLoc_to_sum (Some ∘ sum_to_AbsLoc) _.
  Proof.  by destruct 0. Qed.

  Global Instance AbsLoc_countable: Countable AbsLoc
    := inj_countable AbsLoc_to_sum (Some ∘ sum_to_AbsLoc) _.
  Proof. by destruct 0. Qed.

  Definition RCUInfo : Type := list (aloc * loc * gname).
  Definition RCUHist : Type := list (aloc * AbsLoc).
  Definition RCUData : Type := RCUHist * RCUInfo.

  Implicit Types (D: RCUData) (H: RCUHist) (M: RCUInfo) (i j: aloc).

  (* Base operations *)
  Definition hist' H i : list AbsLoc := snd <$> {[p <- H | p.1 = i]}.
  Definition hist D i := hist' (D.1) i.
  Definition info' M i : list (loc * gname)
    := ((λ p, (p.1.2, p.2)) <$> {[p <- M | p.1.1 = i]}).
  Definition infos D i : gset (loc * gname) := of_list (info' (D.2) i).
  Definition info D i : option (loc * gname) := hd_error (info' (D.2) i).

  Global Instance RCUInfo_dom : Dom RCUInfo (gset aloc)
    := λ M, of_list (fst ∘ fst <$> M).
  Global Instance RCUHist_dom : Dom RCUHist (gset aloc)
    := λ H, of_list (fst <$> H).
  Global Instance RCUData_dom : Dom RCUData (gset aloc)
    := λ D, dom _ (D.1).

  Definition dead' H : gset aloc
    := {[i <- dom _ H | Dead ∈ (hist' H i) ]}.
  Definition dead D := dead' (D.1).
  Definition live' H := dom _ H ∖ dead' H.
  Definition live D := live' (D.1).

  Definition is_base H b :=
    b ∈ live' H ∧ ∀ i, i ∈ dom (gset _) H → Abs b ∉ (hist' H i).

  Instance is_base_dec H b:
    Decision (is_base H b).
  Proof.
    apply and_dec; last apply set_Forall_dec; auto with typeclass_instances.
  Qed.

  Definition basel' H : list aloc :=  {[ i <- (fst <$> H) | is_base H i]}.
  Definition base' H : gset aloc := of_list (basel' H).
  Definition basel D := basel' (D.1).
  Definition base D := base' (D.1).

  Lemma rcu_base_live b D:
    b ∈ base D → b ∈ live D.
  Proof.
    rewrite /base /base' elem_of_of_list /basel' elem_of_list_filter.
    by move => [[? ?] ?].
  Qed.

  (* Extensions *)
  Global Instance RCUHist_ext : Extension RCUHist
    := λ H1 H2, H1 = nil ∨ H1 `suffix_of` H2 ∧ base' H1 ≡ base' H2.
  Global Instance RCUHist_ext_dec H1 H2: Decision (H1 ⊑ H2) := _.
  Global Instance RCUHist_ext_partialorder: @PartialOrder RCUHist (⊑).
  Proof.
    split; first split.
    - intros [|]; by [left|by right].
    - intros ? ? ? [?|[? ?]]; first by left.
      intros [?|[? ?]]; subst.
      + left. by apply suffix_nil_inv. 
      + right. split; by etrans.
    - intros ?? [?|[? _]] [?|[? _]]; subst; auto.
      + symmetry. by apply suffix_nil_inv. 
      + by apply suffix_nil_inv.
      + by apply: anti_symm.
  Qed.

  Global Instance RCUInfo_ext : Extension RCUInfo := suffix.
  Global Instance RCUInfo_ext_dec M1 M2: Decision (M1 ⊑ M2) := _.

  Global Instance RCUData_ext : Extension RCUData
    := λ D1 D2, D1.1 ⊑ D2.1 ∧ D1.2 ⊑ D2.2.
  Global Instance RCUData_ext_dec D1 D2: Decision (D1 ⊑ D2) := _.

  Global Instance RCUData_ext_partialorder: @PartialOrder RCUData (⊑).
  Proof.
    split; first split; first done.
    - intros [??] [??] [??] [??] [??]. split; by etrans.
    - intros [??] [??] [??] [??]. f_equal; by apply (anti_symm (⊑)).
  Qed.

  Lemma RCUData_nil_min (D: RCUData): (nil,nil) ⊑ D.
  Proof. split; [by left|apply suffix_nil]. Qed.

  Lemma RCUHist_sub_dom H1 H2 (Sub: H1 `suffix_of` H2):
    dom (gset _) H1 ⊆ dom _ H2.
  Proof.
    move => ?.
    rewrite !elem_of_of_list !elem_of_list_fmap.
    move => [b [? In]]. exists b. split; first auto.
    by eapply elem_of_suffix.
  Qed.

  Lemma RCUHist_ext_dom: ∀ H1 H2, H1 ⊑ H2 → dom (gset _) H1 ⊆ dom _ H2.
  Proof.
    move => ?? [->|[? _]].
    - set_solver+.
    - by apply RCUHist_sub_dom.
  Qed.

  Lemma RCUInfo_ext_dom: ∀ M1 M2, M1 ⊑ M2 → dom (gset _) M1 ⊆ dom _ M2.
  Proof.
    move => ????.
    rewrite !elem_of_of_list !elem_of_list_fmap.
    move => [b [??]]. exists b. split; first auto.
    by eapply elem_of_suffix.
  Qed.

  Lemma RCUData_ext_dom: ∀ D1 D2, D1 ⊑ D2 → dom (gset _) D1 ⊆ dom _ D2.
  Proof. move => ?? [? _]. by apply RCUHist_ext_dom. Qed.

  Lemma RCUHist_ext_piecewise D1 D2:
    D1 ⊑ D2 → ∀ i, hist D1 i `suffix_of` hist D2 i.
  Proof.
    move => [Le _] ?. apply suffix_fmap, suffix_filter.
    move: Le => [->|[? _]]; [apply suffix_nil|done].
    Qed.

  (* Wellformedness *)
  Definition SinglePtr' H :=
    ∀ i j k, hd_error (hist' H i) = Some $ Abs k
           → hd_error (hist' H j) = Some $ Abs k
           → i = j.
  Definition SinglePtr D := SinglePtr' (D.1).
  Definition no_single_cycle D := ∀ i, Abs i ∉ hist D i.

  Definition codom_in_dom H :=
    ∀ i, i ∈ dom (gset _) H →
          match (hd_error (hist' H i)) with
          | Some (Abs j) => j ∈ dom (gset _) H
          | _ => True
          end.

  Definition dead_once H :=
    ∀ i, i ∈ dead' H → ∃ L, hist' H i = Dead :: L ∧ Dead ∉ L.

  Definition live_closed H :=
    ∀ i, i ∈ live' H → ∃ b, hd_error (hist' H i) = Some b
                            ∧ (b = Null \/ ∃ j, b = Abs j ∧ j ∈ live' H).

  Definition wellformed D :=
      size (base D) = 1%nat
    ∧ dom (gset _) (D.1) ≡ dom (gset _) (D.2)
    ∧ (∀ i, i ∈ dom (gset _) (D.2) → length (info' (D.2) i) = 1%nat)
    ∧ codom_in_dom (D.1)
    ∧ dead_once (D.1)
    ∧ live_closed (D.1).

  Global Instance wellformed_dec D: Decision (wellformed D).
  Proof.
    repeat apply and_dec;
      [auto with typeclass_instances|apply mapset_equiv_dec|..];
      apply set_Forall_dec; intros i; first auto with typeclass_instances.
    - destruct (hist' (D.1) i) as [|[| |] L] => /=;
      auto with typeclass_instances.
    - destruct (hist' (D.1) i) as [|[| |] L].
      + right. by move => [? [? _]].
      + right. by move => [? [? _]].
      + right. by move => [? [? _]].
      + case (decide (Dead ∉ L)) => [In|NIn].
        * left. by eexists.
        * right. move => [L' [Eq NIn']]. inversion Eq. by subst.
   - destruct (hd_error (hist' (D.1) i)) as [[|j|]|].
      + left. exists Null. split; [auto|by left].
      + case (decide (j ∈ live D)) => [In|NIn].
        * left. exists (Abs j). split; [auto|right; by eexists].
        * right. move => [? [[<-] [//|[? [[<-] ?] //]]]].
      + right. move => [? [[<-] [//|[? [? _] //]]]].
      + right. by move => [? [? _]].
  Qed.

  Arguments dom _ _ _ _ /.

  Lemma elem_of_rcu_info' M i p:
    p ∈ info' M i ↔ (i,p.1,p.2) ∈ M.
  Proof.
    rewrite elem_of_list_fmap.
    setoid_rewrite elem_of_list_filter.
    split.
    - by move => [[[? ?] ?] [-> [<- ?]]].
    - move => ?. exists (i, p.1, p.2). by destruct p.
  Qed.

  Lemma elem_of_rcu_infos D i p:
    p ∈ infos D i ↔ (i,p.1,p.2) ∈ D.2.
  Proof. by rewrite elem_of_of_list elem_of_rcu_info'. Qed.

  Lemma rcu_dom'_cons H i b :
    dom (gset _) ((i,b)::H) ≡ {[i]} ∪ dom (gset _) H.
  Proof.
    rewrite /= /RCUHist_dom => x. by rewrite elem_of_of_list fmap_cons.
  Qed.

  Lemma rcu_dom'_cons_in H i b (In: i ∈ dom (gset _) H):
    dom (gset _) ((i,b)::H) ≡  dom (gset _) H.
  Proof. rewrite rcu_dom'_cons -subseteq_union. set_solver. Qed.

  Lemma elem_of_rcu_info_dom M i:
    i ∈ dom (gset _) M ↔ ∃ l γ, (i,l,γ) ∈ M.
  Proof.
    rewrite /= /RCUInfo_dom elem_of_of_list elem_of_list_fmap. split.
    - move => [[[??]?] [/= -> ?]]. by do 2 eexists.
    - move => [l [γ ?]]. by exists (i,l,γ).
  Qed.

  Lemma elem_of_rcu_hist_dom H i:
    i ∈ dom (gset _) H ↔ ∃ b, b ∈ hist' H i.
  Proof.
    rewrite /= /RCUHist_dom /hist' elem_of_of_list elem_of_list_fmap.
    setoid_rewrite elem_of_list_fmap.
    setoid_rewrite elem_of_list_filter.
    split.
    - move => [y [??]]. by exists (y.2), y.
    - move => [b [y [?[??]]]]. by exists y.
  Qed.

  Lemma elem_of_rcu_hist_dom_2 H i:
    i ∈ dom (gset _) H ↔ ∃ y, i = y.1 ∧ y ∈ H.
  Proof.
    by rewrite /= /RCUHist_dom /hist' elem_of_of_list elem_of_list_fmap.
  Qed.

  Lemma elem_of_rcu_hist'_mono H1 H2 i:
    hist' H2 i ⊆ hist' (H1 ++ H2)%list i.
  Proof.
    move => b. rewrite /hist' filter_app fmap_app elem_of_app. by right.
  Qed.

  Lemma elem_of_rcu_hist_dom_mono H1 H2:
    dom (gset _) H2 ⊆ dom (gset _) (H1 ++ H2)%list.
  Proof.
    move => b. rewrite /= /RCUHist_dom 2!elem_of_of_list fmap_app elem_of_app.
    by right.
  Qed.

  Lemma rcu_hist'_cons i b H:
    b ∈ (hist' ((i, b) :: H) i).
  Proof.
    rewrite /hist'.
    rewrite (_ : (i, b) :: H = (((i,b)::nil) ++ H)%list); last done.
    rewrite filter_app fmap_app list_filter_singleton; last done.
    apply elem_of_list_here.
  Qed.

  Lemma rcu_hist'_cons_next i b H (NEq: b.1 ≠ i):
    hist' (b :: H) i = hist' H i.
  Proof. rewrite /hist'. by rewrite list_filter_cons_not. Qed.

  Lemma rcu_hist'_cons_now i b H :
    hist' ((i,b) :: H) i = b :: hist' H i.
  Proof. rewrite /hist'. by rewrite list_filter_cons. Qed.

  Lemma rcu_hist'_cons_some i b H:
    hd_error (hist' ((i, b) :: H) i) = Some b.
  Proof. by rewrite rcu_hist'_cons_now. Qed.

  Lemma elem_of_rcu_hist'_further i j bi bj H (NEq: bj ≠ bi):
    bj ∈ hist' ((i,bi) :: H) j → bj ∈ hist' H j.
  Proof.
    case (decide (i = j)) => [->|NEq2].
    - rewrite rcu_hist'_cons_now elem_of_cons. move => [|]//.
    - rewrite rcu_hist'_cons_next; auto.
  Qed.

  Lemma rcu_hist'_nil i H:
    i ∉ dom (gset _) H ↔ hist' H i = nil.
  Proof.
    rewrite /hist'. split.
    - move => NIn. apply elem_of_nil_inv. move => x In2.
      apply NIn, elem_of_rcu_hist_dom. by exists x.
    - rewrite /= /RCUHist_dom. move => /fmap_nil_inv EqN.
      rewrite elem_of_of_list elem_of_list_fmap.
      move => [y [Eq In]]. apply (elem_of_nil y).
      by rewrite -EqN elem_of_list_filter.
  Qed.

  Lemma rcu_hist'_nil_fresh H:
    hist' H (fresh (dom (gset _) H)) = nil.
  Proof. apply rcu_hist'_nil, is_fresh. Qed.

  Lemma rcu_dead'_app H1 H2:
    dead' H2 ⊆ dead' (H1++H2)%list.
  Proof.
    move => i.
    rewrite /dead' !elem_of_filter /= /RCUHist_dom /hist'
            !elem_of_of_list filter_app !fmap_app !elem_of_app.
    move => [? ?]. split; by [left|right].
  Qed.

  Lemma rcu_dead_extend D1 D2 :
    D1 ⊑ D2 → dead D1 ⊆ dead D2.
  Proof.
    rewrite /dead.
    move => [[->|[[? ->] ?]] ?];[rewrite -(app_nil_r (D2.1))|];
      apply rcu_dead'_app.
  Qed.

  Lemma rcu_dead'_eq H1 H2
    (NoDead: ∀ p, p ∈ H1 → p.2 ≠ Dead):
    dead' H2 ≡ dead' (H1++H2)%list.
  Proof.
    move => i. split; first apply rcu_dead'_app.
    rewrite /dead' !elem_of_filter /hist' filter_app !fmap_app !elem_of_app.
    move => [[HD|HD] _].
    - exfalso. move : HD. rewrite elem_of_list_fmap.
      move => [y [EqD]]. rewrite elem_of_list_filter.
      move => [_ Iny]. by apply (NoDead _ Iny).
    - split; first auto. apply elem_of_rcu_hist_dom. by exists Dead.
  Qed.

  Lemma elem_of_live'_dom H:
    live' H ⊆ dom (gset _) H.
  Proof. move => ?. rewrite /live' elem_of_difference. intuition. Qed.

  Lemma elem_of_rcu_live'_mono i H1 H2 (NoDead: ∀ b, (i,b) ∈ H1 → b ≠ Dead) :
    i ∈ live' H2 → i ∈ live' (H1++H2)%list.
  Proof.
    rewrite /live' /= /RCUHist_dom !elem_of_difference.
    move => [In NIn]. split.
    - move : In. rewrite 2!elem_of_of_list fmap_app elem_of_app. by right.
    - move : NIn. rewrite /dead'.
      rewrite !elem_of_filter /hist' filter_app fmap_app elem_of_app.
      move => NIn [[HD|HD] _].
      + move :HD => / elem_of_list_fmap [[i' b] [Eq /elem_of_list_filter [? ?]]].
        subst i. by apply (NoDead b).
      + apply NIn. split; first auto.
        apply elem_of_rcu_hist_dom. by exists Dead.
  Qed.

  Lemma elem_of_rcu_live'_noupdate i H1 H2 (NoUp: ∀ p, p ∈ H1 → p.1 ≠ i) :
    i ∈ live' H2 ↔ i ∈ live' (H1++H2)%list.
  Proof.
    rewrite /live' /dead'/= /RCUHist_dom !elem_of_difference !elem_of_filter
            !elem_of_of_list /hist' filter_app 2!fmap_app 2!elem_of_app.
    split.
    - move => [In NIn]. split; first by right.
      move => [HD [In'|In']].
      + apply elem_of_list_fmap in In' as [y [??]]. by apply (NoUp y).
      + move: HD => [|HD]; last by apply NIn.
        rewrite elem_of_list_fmap. move => [y [Eq]].
        rewrite elem_of_list_filter. move => [? ?]. by apply (NoUp y).
    - move => [[In|In] NIn].
      + exfalso. apply elem_of_list_fmap in In as [y [??]]. by apply (NoUp y).
      + split; first auto. move => [HD _].
        apply NIn. split; by [right|right].
  Qed.

  Lemma elem_of_rcu_dead' H i:
    i ∈ dead' H ↔ Dead ∈ hist' H i.
  Proof.
    rewrite /dead' elem_of_filter. split; first by move => [? _].
    move => InD. split; first auto.
    apply elem_of_rcu_hist_dom. by eexists.
  Qed.

  Lemma elem_of_rcu_live'_further i j bi H (NEq: i ≠ j):
    i ∈ live' H ↔ i ∈ live' ((j,bi) :: H).
  Proof.
    apply (elem_of_rcu_live'_noupdate _ ((j,bi)::nil)).
    by move => p /elem_of_list_singleton ->.
  Qed.

  Lemma elem_of_rcu_live'_mono2 i H1 H2 :
    i ∈ dom (gset _) H2 → i ∈ live' (H1++H2)%list → i ∈ live' H2.
  Proof.
    move => Ind. rewrite /live' !elem_of_difference.
    move => [In1 NIn1]. split; first auto. move => NIn2.
    apply NIn1. by apply rcu_dead'_app.
  Qed.

  Lemma elem_of_rcu_live'_hist' i H :
    i ∈ live' H ↔ (i ∈ dom (gset _) H ∧ Dead ∉ hist' H i).
  Proof. by rewrite /live' elem_of_difference elem_of_rcu_dead'. Qed.

  Lemma rcu_hist_nil_dead_empty:
    dead' nil = empty.
  Proof. done. Qed.

  Lemma rcu_hist_nil_live_empty:
    live' nil = empty.
  Proof. rewrite /live' /RCUHist_dom. set_solver. Qed.

  Lemma rcu_hist_dom_app H1 H2:
    dom (gset _) (H1 ++ H2)%list ≡ dom (gset _) H1 ∪ dom (gset _) H2.
  Proof.
    move => i. by rewrite /= /RCUHist_dom elem_of_union
                          !elem_of_of_list fmap_app elem_of_app.
  Qed.

  Lemma rcu_hist_dom_cons i b H:
    dom (gset _) ((i,b) :: H) ≡ {[i]} ∪ dom (gset _) H.
  Proof. done. Qed.

  Lemma rcu_info_dom_app M1 M2:
    dom (gset _) (M1 ++ M2)%list ≡ dom (gset _) M1 ∪ dom (gset _) M2.
  Proof.
    move => i.
    by rewrite /= /RCUInfo_dom elem_of_union
               !elem_of_of_list fmap_app elem_of_app.
  Qed.

  Lemma rcu_info_dom_cons i x γ M:
    dom (gset _) ((i,x,γ) :: M) ≡ {[i]} ∪  dom (gset _) M.
  Proof. done. Qed.

  Lemma rcu_hist'_app H1 H2 i:
    hist' (H1 ++ H2)%list i = (hist' H1 i ++ hist' H2 i)%list.
  Proof. by rewrite /hist' filter_app fmap_app. Qed.

  Lemma rcu_live'_cons_Dead H k ix :
    k ∈ live' ((ix, Dead) :: H) → k ∈ live' H ∧ k ≠ ix.
  Proof.
    rewrite 2!elem_of_difference
            rcu_hist_dom_cons elem_of_union elem_of_singleton.
    move => [[->|Ink] NInD].
    - exfalso. apply NInD. apply elem_of_rcu_dead'.
      rewrite rcu_hist'_cons_now. by left.
    - repeat split; first done.
      + move => HDk. apply NInD, (rcu_dead'_app (_::nil) _ _ HDk).
      + move => ?. subst ix. apply NInD.
        apply elem_of_rcu_dead'. rewrite rcu_hist'_cons_now. by left.
  Qed.

  Lemma rcu_base_reserving H1 H2
    (NoRemove1: ∀ p, p ∈ H1 → p.2 = Dead → is_base H2 (p.1) → False)
    (NoRemove2: ∀ i p, p ∈ H1 → p.2 = Abs i → is_base H2 i → False)
    (NoAdd:  ∀ p, p ∈ H1 → p.1 ∉ dom (gset _) H2 →
                  ∃ j, (j, Abs (p.1)) ∈ H1) :
    base' H2 ≡ base' (H1 ++ H2)%list.
  Proof.
    move => i. rewrite /base' !elem_of_of_list /basel' !elem_of_list_filter.
    split; move => [IB Ini].
    - rewrite fmap_app elem_of_app.
      split; last by right. split.
      + apply elem_of_rcu_live'_mono; last by destruct IB.
        move => b Inb EqD. apply (NoRemove1 _ Inb EqD IB).
      + move => i' _.
        rewrite rcu_hist'_app elem_of_app. move => [|Ini2].
        * rewrite /hist' elem_of_list_fmap.
          move => [y [Eq /elem_of_list_filter [Eq' Iny]]].
          by apply (NoRemove2 i _ Iny).
        * destruct IB as [? IB]. apply (IB i'); last done.
          apply elem_of_rcu_hist_dom. by eexists.
    - assert (i ∈ H2.*1).
      { case (decide (i ∈ H2.*1)) => [|NIn2]; first auto.
        move : Ini. rewrite fmap_app elem_of_app.
        move => [|//]. rewrite elem_of_list_fmap.
        move => [y [Eqi Iny]]. exfalso.
        destruct (NoAdd _ Iny) as [j Inj].
        - rewrite -Eqi. by rewrite /= /RCUHist_dom elem_of_of_list.
        - assert (Abs i ∈ hist' H1 j).
          { rewrite Eqi elem_of_list_fmap. exists (j, Abs (y.1)).
            by rewrite elem_of_list_filter. }
          destruct IB as [? IB]. apply (IB j).
          + rewrite rcu_hist_dom_app elem_of_union. left.
            rewrite elem_of_rcu_hist_dom. exists (Abs (y.1)). by rewrite -Eqi.
          + rewrite rcu_hist'_app elem_of_app. by left. }
      split; last auto; split.
      + apply (elem_of_rcu_live'_mono2 _ H1).
        * by rewrite /= /RCUHist_dom elem_of_of_list.
        * by destruct IB.
      + destruct IB as [? IB].
        move => i' InD Ina. apply (IB i').
        * rewrite rcu_hist_dom_app elem_of_union. by right.
        * rewrite rcu_hist'_app elem_of_app. by right.
  Qed.

  Lemma rcu_fresh_not_base H: ¬ is_base H (fresh (dom (gset _) H)).
  Proof. move => [/elem_of_live'_dom F _]. by eapply is_fresh. Qed.

  Lemma rcu_base_append H i (InL: i ∈ live' H):
    let j := fresh (dom (gset aloc) H) in
    base' H ≡ base' ((i, Abs j) :: (j, Null) :: H).
  Proof.
    intros.
    rewrite (_ : (i, Abs j) :: (j, Null) ::H =
                 (((i, Abs j) :: (j, Null) :: nil) ++ H)%list); last done.
    apply rcu_base_reserving.
    - move => p. rewrite elem_of_cons elem_of_list_singleton.
      by move => [->|->].
    - move => i' p. rewrite elem_of_cons elem_of_list_singleton.
      move => [-> [<-] |->//]. apply rcu_fresh_not_base.
    - move => p. rewrite elem_of_cons elem_of_list_singleton.
      move => [-> NIn|-> _].
      + exfalso. by apply NIn, elem_of_live'_dom.
      + exists i. left.
  Qed.

  Lemma rcu_base_delete H i j bj
    (Hi: hd_error (hist' H i) = Some $ Abs j)
    (Hj: hd_error (hist' H j) = Some bj) (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    base' H ≡ base' ((i, bj) :: (j, Dead) :: H).
  Proof.
    rewrite (_ : (i, bj) :: (j, Dead) :: H =
                 (((i, bj) :: (j, Dead) :: nil) ++ H)%list); last done.
    apply rcu_base_reserving.
    - move => p. rewrite elem_of_cons elem_of_list_singleton.
      move => [->|-> _ [_ IB]].
      + move : bjE => [->|[? ->]]; done.
      + apply (IB i).
        * apply elem_of_rcu_hist_dom. exists (Abs j).
          by apply hd_error_some_elem.
        * by apply hd_error_some_elem.
    - move => i' p. rewrite elem_of_cons elem_of_list_singleton.
      move => [->|->//]. move : bjE => [->//|[j' bjE]]. subst bj.
      move => [<-] [_ IB]. apply (IB j).
      + apply elem_of_rcu_hist_dom. exists (Abs j').
        by apply hd_error_some_elem.
      + by apply hd_error_some_elem.
    - move => p. rewrite elem_of_cons elem_of_list_singleton.
      move => [-> In|->In]; exfalso; apply In, elem_of_rcu_hist_dom.
      + exists (Abs j). by apply hd_error_some_elem.
      + exists bj. by apply hd_error_some_elem.
  Qed.

  Lemma rcu_base_update H i j bj
    (Hi: hd_error (hist' H i) = Some $ Abs j)
    (Hj: hd_error (hist' H j) = Some bj) (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    let j0 := fresh (dom (gset aloc) H) in
    base' H ≡ base' ((i, Abs j0) :: (j0, bj) :: (j, Dead) :: H).
  Proof.
    intros.
    rewrite (_ : (i, Abs j0) :: (j0, bj) :: (j, Dead) :: H =
                 (((i, Abs j0) :: (j0, bj) :: (j, Dead) :: nil) ++ H)%list);
      last done.
    apply rcu_base_reserving.
    - move => p. rewrite 2!elem_of_cons elem_of_list_singleton.
      move => [-> //|[->|-> _ [_ IB]]].
      + move : bjE => [->|[? ->]]; done.
      + apply (IB i).
        * apply elem_of_rcu_hist_dom. exists (Abs j).
          by apply hd_error_some_elem.
        * by apply hd_error_some_elem.
    - move => i' p. rewrite 2!elem_of_cons elem_of_list_singleton.
      move => [-> [<-]|[->|->//]]; first apply rcu_fresh_not_base.
      move : bjE => [->//|[j' bjE]]. subst bj.
      move => [<-] [_ IB]. apply (IB j).
      + apply elem_of_rcu_hist_dom. exists (Abs j'). by apply hd_error_some_elem.
      + by apply hd_error_some_elem.
    - move => p. rewrite 2!elem_of_cons elem_of_list_singleton.
      move => [-> In|[->In|->In]].
      + exfalso. apply In, elem_of_rcu_hist_dom. exists (Abs j).
        by apply hd_error_some_elem.
      + exists i. left.
      + exfalso. apply In, elem_of_rcu_hist_dom. exists bj.
        by apply hd_error_some_elem.
  Qed.

  Lemma rcu_hist_head_not_fresh H i (CD: codom_in_dom H) :
    ¬ hd_error (hist' H i) = Some (Abs (fresh (dom (gset _) H))).
  Proof.
    specialize (CD i). move => HD. rewrite HD in CD.
    apply (is_fresh (dom (gset _) H)). apply CD, elem_of_rcu_hist_dom.
    eexists. by apply hd_error_some_elem.
  Qed.

  Lemma rcu_SinglePtr_append H i
    (CD: codom_in_dom H) (SH: SinglePtr' H):
    let j := fresh (dom (gset aloc) H) in
    SinglePtr' ((i, Abs j) :: (j, Null) :: H).
  Proof.
    move => j i1 i2 k.
    case (decide (i = i1)) => [<-|NEq1].
    - rewrite rcu_hist'_cons_some. move => [<-].
      case (decide (i = i2)) => [//|NEq2].
      rewrite rcu_hist'_cons_next; last exact NEq2.
      case (decide (j = i2)) => [<-|NEqj2];
        first by rewrite rcu_hist'_cons_some.
      rewrite rcu_hist'_cons_next; last exact NEqj2.
      move => HD. exfalso. apply (rcu_hist_head_not_fresh _ _ CD HD).
    - rewrite rcu_hist'_cons_next; last exact NEq1.
      case (decide (j = i1)) => [->|NEqj1]; first
        by rewrite rcu_hist'_cons_some.
      rewrite rcu_hist'_cons_next; last exact NEqj1.
      case (decide (i = i2)) => [<-|NEq2].
      + rewrite rcu_hist'_cons_some.
        move => HD [?]. subst k. exfalso.
        apply (rcu_hist_head_not_fresh _ _ CD HD).
      + rewrite rcu_hist'_cons_next; last exact NEq2.
        case (decide (j = i2)) => [->|NEqj2]; first
          by rewrite rcu_hist'_cons_some.
        rewrite rcu_hist'_cons_next; last exact NEqj2.
        apply SH.
  Qed.

  Lemma rcu_SinglePtr_delete  H i j bj
    (SH: SinglePtr' H)
    (Hj: hd_error (hist' H j) = Some bj) (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    SinglePtr' ((i, bj) :: (j, Dead) :: H).
  Proof.
    move => i1 i2 k.
    case (decide (i = i1)) => [<-|NEq1].
    - rewrite rcu_hist'_cons_some. move : bjE => [->//|[j' ?]].
      subst bj => [<-].
      case (decide (i = i2)) => [//|NEq2].
      rewrite rcu_hist'_cons_next; last exact NEq2.
      case (decide (j = i2)) => [<-|NEqj2];
        first by rewrite rcu_hist'_cons_some.
      rewrite rcu_hist'_cons_next; last exact NEqj2.
      move => HD. exfalso. apply NEqj2.
      apply (SH _ _ j'); [by rewrite Hj| exact HD].
    - rewrite rcu_hist'_cons_next; last exact NEq1.
      case (decide (j = i1)) => [<-|NEqj1];
        first by rewrite rcu_hist'_cons_some.
      rewrite rcu_hist'_cons_next; last exact NEqj1.
      case (decide (i = i2)) => [<-|NEq2].
      + rewrite rcu_hist'_cons_some. move : bjE => [->//|[j' ?]].
        subst bj => [<-].
        move => HD. exfalso. apply NEqj1.
        apply (SH _ _ j'); [by rewrite Hj|auto].
      + rewrite rcu_hist'_cons_next; last exact NEq2.
        case (decide (j = i2)) => [->|NEqj2]; first
          by rewrite rcu_hist'_cons_some.
        rewrite rcu_hist'_cons_next; last exact NEqj2.
        apply SH.
  Qed.

  Lemma rcu_SinglePtr_update H i j bj
    (CD: codom_in_dom H) (SH: SinglePtr' H)
    (Hj: hd_error (hist' H j) = Some bj) (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    let j0 := fresh (dom (gset aloc) H) in
    SinglePtr' ((i, Abs j0) :: (j0, bj) :: (j, Dead) :: H).
  Proof.
    move => j0 i1 i2 k.
    case (decide (i = i1)) => [<-|NEq1].
    - rewrite rcu_hist'_cons_some => [<-].
      case (decide (i = i2)) => [//|NEq2].
      rewrite rcu_hist'_cons_next; last exact NEq2.
      case (decide (j0 = i2)) => [<-|NEqj2].
      + rewrite rcu_hist'_cons_some.
        move : bjE => [->//|[j' ?]]. subst bj => [[Eq]]. subst j'.
        exfalso. apply (rcu_hist_head_not_fresh H j CD). by rewrite Hj.
      + rewrite rcu_hist'_cons_next; last exact NEqj2.
        case (decide (j = i2)) => [<-|NEqj3];
          first by rewrite rcu_hist'_cons_some.
        rewrite rcu_hist'_cons_next; last exact NEqj3.
        move => HD. exfalso. apply (rcu_hist_head_not_fresh _ _ CD HD).
    - rewrite rcu_hist'_cons_next; last exact NEq1.
      case (decide (j0 = i1)) => [<-|NEqj1].
      + rewrite rcu_hist'_cons_some.
        move : bjE => [->//|[j' ?]]. subst bj => [[<-]].
        case (decide (i = i2)) => [<-|NEq2].
        * rewrite rcu_hist'_cons_some. move => [?]. subst j'.
          exfalso. apply (rcu_hist_head_not_fresh H j CD). by rewrite Hj.
        * rewrite rcu_hist'_cons_next; last exact NEq2.
          case (decide (j0 = i2)) => [//|NEqj2].
          rewrite rcu_hist'_cons_next; last exact NEqj2.
          case (decide (j = i2)) => [<-|NEqj3];
            first by rewrite rcu_hist'_cons_some.
          rewrite rcu_hist'_cons_next; last exact NEqj3.
          move => HD. exfalso. apply NEqj3.
          apply (SH _ _ j'); [by rewrite Hj|auto].
      + rewrite rcu_hist'_cons_next; last exact NEqj1.
        case (decide (j = i1)) => [<-|NEqj2];
            first by rewrite rcu_hist'_cons_some.
        rewrite rcu_hist'_cons_next; last exact NEqj2.
        case (decide (i = i2)) => [<-|NEq2].
        * rewrite rcu_hist'_cons_some. move => HD [?]. subst k.
          exfalso. apply (rcu_hist_head_not_fresh _ _ CD HD).
        * rewrite rcu_hist'_cons_next; last exact NEq2.
          case (decide (j0 = i2)) => [<-|NEqj3].
          { move => HD. rewrite rcu_hist'_cons_some.
            move : bjE => [->//|[j' ?]]. subst bj => [[?]]. subst k.
            exfalso. apply NEqj2.
            apply (SH _ _ j'); [by rewrite Hj|auto]. }
          { rewrite rcu_hist'_cons_next; last exact NEqj3.
            case (decide (j = i2)) => [<-|NEqj4];
              first by rewrite rcu_hist'_cons_some.
            rewrite rcu_hist'_cons_next; last exact NEqj4. apply SH. }
  Qed.

  Lemma rcu_wellformed_info_lookup D i:
    wellformed D → i ∈ dom (gset _) D → ∃ p, info D i = Some p.
  Proof.
    move => [_ [domEq [Uniq _]]]. rewrite /= /RCUData_dom domEq.
    move => InD. specialize (Uniq _ InD).
    rewrite /info.
    destruct (info' (D.2) i) as [|p L]; first done.
    by exists p.
  Qed.

  Lemma rcu_hist_lookup D i:
    i ∈ dom (gset _) D → ∃ L, hist D i = L ∧ L ≠ nil.
  Proof.
    move => /elem_of_rcu_hist_dom [b Ib].
    exists (hist' (D.1) i). split; first auto.
    move => Eq. move : Ib. rewrite Eq. apply not_elem_of_nil.
  Qed.

  Lemma rcu_info_dom_eq M1 M2 (Le: M1 ⊑ M2)
    (dEq: dom (gset _) M1 ≡ dom (gset _) M2)
    (W: ∀ i, i ∈ dom (gset _) M2 → length (info' M2 i) = 1%nat):
    M1 = M2.
  Proof.
    destruct Le as [[|[[i l] γ] M3] Eq]; first done.
    exfalso.
    assert (In2: i ∈ dom (gset _) M2).
    { rewrite elem_of_rcu_info_dom Eq. exists l, γ. apply elem_of_list_here. }
    assert (In1: i ∈ dom (gset _) M1).
    { by rewrite dEq. }
    move: (W _ In2).
    rewrite Eq /info' /= (filter_app _ ((i, l, γ) :: nil)) fmap_app.
    rewrite {1}/filter /list_filter. rewrite decide_True /=; last done.
    move => [/nil_length_inv Eq0].
    apply elem_of_rcu_info_dom in In1 as [l' [γ' In']].
    apply (not_elem_of_nil (l', γ')).
    rewrite -Eq0 elem_of_list_fmap.
    exists (i,l',γ'). split; first auto.
    rewrite elem_of_list_filter elem_of_app. split; auto.
  Qed.

  Definition RCU_part_le D1 D2 := 
     (D1.1) `suffix_of` (D2.1) ∧ base' (D1.1) ≡ base' (D2.1) ∧ (D1.2) ⊑ (D2.2).

  Lemma RCUData_ext_share_total_1 D1 D2 D3
    (W1: wellformed D1) (W2: wellformed D2):
    RCU_part_le D1 D3 → RCU_part_le D2 D3 → D1 ⊑ D2 ∨ D2 ⊑ D1.
  Proof.
    move => [HLe1 [Eq1 MLe1]] [HLe2 [Eq2 MLe2]].
    destruct (suffix_share_total _ _ _ HLe1 HLe2) as [HLe|HLe];
    destruct (suffix_share_total _ _ _ MLe1 MLe2) as [MLe|MLe].
    - left. split; first (right; split); [auto|by etrans|auto].
    - left. split; first (right; split); [auto|by etrans|].
      destruct W1 as (_ & dEq1 & ? & _).
      destruct W2 as (_ & dEq2 & _).
      rewrite (rcu_info_dom_eq _ _ MLe); [done| |auto].
      apply (anti_symm (⊆)).
      + by apply RCUInfo_ext_dom.
      + rewrite -dEq1 -dEq2. apply RCUHist_ext_dom.
        right. split; [auto|by etrans].
    - right. split; first (right; split); [auto|by etrans|].
      destruct W1 as (_ & dEq1 & _).
      destruct W2 as (_ & dEq2 & ? & _).
      rewrite (rcu_info_dom_eq _ _ MLe); [done| |auto].
      apply (anti_symm (⊆)).
      + by apply RCUInfo_ext_dom.
      + rewrite -dEq1 -dEq2. apply RCUHist_ext_dom.
        right. split; [auto|by etrans].
    - right. split; first (right; split); auto. by etrans.
  Qed.

  Lemma RCUData_ext_share_total_2 D1 D2 D3
    (W1: wellformed D1) (W2: wellformed D2):
    D1 ⊑ D3 → D2 ⊑ D3 → D1 ⊑ D2 ∨ D2 ⊑ D1.
  Proof.
    move => [[EqN|?] MLe1] [Le2 MLe2].
    - left. split; first by left.
      move: W1 => [_ [EqD _]]. rewrite EqN in EqD.
      move: EqD. rewrite /= /RCUHist_dom /=. rewrite /RCUInfo_dom /=.
      move => EqD. symmetry in EqD. apply of_list_nil_eq, fmap_nil_inv in EqD.
      rewrite EqD. apply suffix_nil.
    - destruct Le2 as [EqN|?].
      + right. split; first by left.
      move: W2 => [_ [EqD _]]. rewrite EqN in EqD.
      move: EqD. rewrite /= /RCUHist_dom /=. rewrite /RCUInfo_dom /=.
      move => EqD. symmetry in EqD. apply of_list_nil_eq, fmap_nil_inv in EqD.
      rewrite EqD. apply suffix_nil.
      + apply (RCUData_ext_share_total_1 _ _ D3 W1 W2); by intuition.
  Qed.

  Definition uwellformed D := (D = (nil,nil)) ∨ wellformed D.

  Lemma RCUData_uwellformed_wellformed D i
    (UW: uwellformed D) (In: i ∈ dom (gset _) D) : wellformed D.
  Proof.
    move : UW => [UW|//]. subst D.
    exfalso. move : In. by rewrite /= /RCUData_dom /= /RCUHist_dom /=.
  Qed.

  Lemma RCUData_ext_share_total D1 D2 D3
    (W1: uwellformed D1) (W2: uwellformed D2):
    D1 ⊑ D3 → D2 ⊑ D3 → D1 ⊑ D2 ∨ D2 ⊑ D1.
  Proof.
    destruct W1 as [Eq|W1].
    - left. rewrite Eq. split; [by left|apply suffix_nil].
    - destruct W2 as [Eq|W2].
      + right. rewrite Eq. split; [by left|apply suffix_nil].
      + by apply RCUData_ext_share_total_2.
  Qed.

  Lemma RCUData_info_suffix M1 M2 i:
    M1 ⊑ M2 → info' M1 i `suffix_of` info' M2 i.
  Proof. move => Le. by apply suffix_fmap, suffix_filter. Qed.

  Lemma RCUData_info_lookup D1 D2 i p
    (W2: uwellformed D2):
    D1 ⊑ D2 → info D1 i = Some p → info D2 i = Some p.
  Proof.
    move => [Dle1 Dle2].
    move => /(hd_error_some_elem _)
            /(elem_of_suffix _ _(RCUData_info_suffix _ _ _ Dle2)).
    move: W2 => [-> /elem_of_nil //|[_ [_ [/(_ i) W2 _]]]] In.
    assert (Len: length (info' (D2.2) i) = 1%nat).
      { apply W2. rewrite /= /RCUInfo_dom elem_of_of_list elem_of_list_fmap.
        apply elem_of_rcu_info' in In. by exists (i, p.1, p.2). }
    rewrite /info. move: In.
    case (info' (D2.2) i) as [|p' L']
      => [/elem_of_nil //|/elem_of_cons [<-//|] /=].
    rewrite (_: L' = nil); first by rewrite elem_of_nil.
    apply length_zero_iff_nil. by inversion Len.
  Qed.

  Lemma rcu_hist'_singleton i b:
    hist' ((i, b) :: nil) i = b :: nil.
  Proof. by rewrite rcu_hist'_cons_now. Qed.

  Lemma rcu_hist'_singleton_neq i b j (NEq: i ≠ j):
    hist' ((i, b) :: nil) j = nil.
  Proof. by rewrite rcu_hist'_cons_next. Qed.

  Lemma RCUData_dom_singleton i L:
    dom _ ((i, Null) :: nil, L) = {[i]}.
  Proof. by rewrite /= /RCUData_dom /= /RCUHist_dom /= union_empty_r_L. Qed.

  Lemma RCUData_dead_singleton i L:
    dead ((i, Null) :: nil, L) = ∅.
  Proof.
    apply elem_of_equiv_empty_L. move => j.
    rewrite elem_of_rcu_dead' /=.
    case (decide (i = j)) => [->|?].
    - by rewrite rcu_hist'_singleton elem_of_list_singleton.
    - rewrite rcu_hist'_singleton_neq; last done. apply not_elem_of_nil.
  Qed.

  Lemma RCUData_basel_singleton i :
    basel' ((i, Null) :: nil) = i :: nil.
  Proof.
    rewrite /basel' /= list_filter_singleton; first auto.
    split.
    - rewrite elem_of_rcu_live'_hist' /= /RCUHist_dom /= union_empty_r_L.
      split; first by apply elem_of_singleton.
      by rewrite rcu_hist'_singleton elem_of_list_singleton.
    - move => i'. rewrite /= /RCUHist_dom /= union_empty_r_L elem_of_singleton.
      move => ->. by rewrite rcu_hist'_singleton elem_of_list_singleton.
  Qed.

  Lemma RCUData_base_singleton i L:
    base ((i, Null) :: nil, L) = {[i]}.
  Proof.
    by rewrite /base /base' RCUData_basel_singleton /= union_empty_r_L.
  Qed.

  Lemma RCUData_live_singleton i L:
    live ((i, Null) :: nil, L) = {[i]}.
  Proof.
    apply leibniz_equiv. intro j.
    rewrite elem_of_rcu_live'_hist'
            /RCUHist_dom /= union_empty_r_L elem_of_singleton.
    split.
    - by move => [->].
    - move => ->. by rewrite rcu_hist'_singleton elem_of_list_singleton.
  Qed.

  Lemma RCUData_SinglePtr_singleton i L:
    SinglePtr ((i, Null) :: nil, L).
  Proof.
   move => j1 j2 k.
   case (decide (j1 = i)) => [->|?].
   - by rewrite rcu_hist'_singleton.
   - by rewrite rcu_hist'_singleton_neq.
  Qed.

  Lemma RCUData_wellformed_init i p γ:
    wellformed ((i, Null) :: nil, (i,p,γ) :: nil).
  Proof.
    split; last split; last repeat split.
    - rewrite RCUData_base_singleton. apply size_singleton.
    - auto.
    - move => i'. rewrite /= /RCUInfo_dom /= union_empty_r_L elem_of_singleton.
      move => ->. by rewrite /info' list_filter_singleton /=.
    - move => i'. rewrite /= /RCUHist_dom /= union_empty_r_L elem_of_singleton.
      move => ->. by rewrite rcu_hist'_cons_now.
    - move => i' /= /elem_of_rcu_dead'.
      case (decide (i = i')) => [->|?].
      + by rewrite rcu_hist'_cons_now /hist' /= elem_of_list_singleton.
      + rewrite rcu_hist'_cons_next; last auto.
        rewrite /hist' /=. move => Nil. by apply not_elem_of_nil in Nil.
    - move => i' /= /elem_of_rcu_live'_hist' /=.
      rewrite /RCUHist_dom /= union_empty_r_L elem_of_singleton.
      move => [-> _]. exists Null. rewrite rcu_hist'_cons_now.
      split; [auto|by left].
  Qed.

  Lemma RCUData_wellformed_append D i x γ (W: wellformed D) (Live: i ∈ live D):
    let j := fresh (dom (gset aloc) D) in
    let D' := ((i,Abs j)::(j,Null):: D.1, (j,x,γ):: D.2) in
    D ⊑ D' → wellformed D'.
  Proof.
    move => j D' [[Le1|[Le1 Eq]] Le2].
    - exfalso. move : Live. by rewrite /live Le1 rcu_hist_nil_live_empty.
    - assert (HN: ∀ k, k ∈ dom (gset _) (D.1) → k ≠ j).
      { move => ?? Eqk. apply (is_fresh (dom (gset aloc) D)).
        by rewrite -/j -Eqk. }
      assert (i ≠ j). { apply HN. by apply elem_of_live'_dom. }
      assert (HNil: hist' (D.1) j = nil).
      { rewrite /hist' (_ : {[p <- D.1 | p.1 = j]} = nil); first done.
        apply list_filter_nil. move => [??]?. apply HN.
        apply elem_of_rcu_hist_dom_2. by eexists. }
      split; last split; last split; last repeat split.
      + rewrite /base -Eq. by apply W.
      + rewrite 2!rcu_hist_dom_cons rcu_info_dom_cons
                union_assoc_L (union_comm_L {[i]}) -union_assoc_L.
        rewrite (subseteq_union_1 {[i]} (dom _ (D.1))).
        * destruct W as (_ & W & _). by rewrite W.
        * by apply elem_of_subseteq_singleton, elem_of_live'_dom.
      + move => i'. rewrite rcu_info_dom_cons elem_of_union elem_of_singleton.
        move => [->|In].
        * rewrite /info' list_filter_cons; last auto.
          simpl. f_equal. apply length_zero_iff_nil.
          rewrite (_: {[p <- D.2 | p.1.1 = j]} = nil); first done.
          apply list_filter_nil.
          move => a Ina Eqj.
          apply (is_fresh (dom (gset aloc) D)).
          destruct W as (_ & W & _).
          rewrite {2}/dom /RCUData_dom W.
          rewrite /= /RCUInfo_dom elem_of_of_list elem_of_list_fmap.
          exists a. by rewrite -/j -Eqj.
        * rewrite /info' list_filter_cons_not; first by apply W.
          move => ?. subst i'. move : In.
          destruct W as (_ & W & _). rewrite -W. apply is_fresh.
      + move => k Ink.
        case (decide (i = k)) => [<-|NEq1].
        * rewrite rcu_hist'_cons_some 2!rcu_hist_dom_cons. set_solver+.
        * rewrite rcu_hist'_cons_next; last exact NEq1.
          case (decide (j = k)) => [<-|NEq2];
            first by rewrite rcu_hist'_cons_some.
          rewrite rcu_hist'_cons_next; last exact NEq2.
          destruct W as (_ & _ & _ & W & _).
          assert (Ink': k ∈ dom (gset aloc) (D.1)).
          { move: Ink. rewrite 2!rcu_hist_dom_cons. set_solver+NEq1 NEq2. }
          assert (Hk := W k Ink').
          repeat case_match; auto. rewrite 2!rcu_hist_dom_cons. set_solver+Hk.
      + move => k /=. rewrite elem_of_filter. move => [HD _].
        assert (Dk: Dead ∈ hist' (D.1) k).
        { by do 2 (apply elem_of_rcu_hist'_further in HD; last auto). }
        destruct W as (_&_&_&_&W&_). destruct (W k) as [L [HL1 HL2]].
        { apply elem_of_filter. split; first auto.
          apply elem_of_rcu_hist_dom. by eexists. }
        exists L. split; last auto.
        rewrite /hist' list_filter_cons_not;
          first rewrite list_filter_cons_not; first done.
        * move => ?. apply (HN k); last done.
          apply elem_of_rcu_hist_dom. rewrite HL1. exists Dead. by left.
        * move => ?. subst k. move : Live => /elem_of_difference [? Live].
          apply Live. rewrite /dead' elem_of_filter HL1. split; [left|auto].
      + move => k Ink.
        assert (j ∈ live' (D'.1)).
        { apply elem_of_difference. split.
          - apply elem_of_of_list, elem_of_list_fmap. exists (j,Null).
            split; first auto. right. left.
          - move => /elem_of_filter [HD _].
            apply elem_of_rcu_hist'_further in HD; last auto.
            apply elem_of_rcu_hist'_further in HD; last auto.
            rewrite HNil in HD. apply (not_elem_of_nil _ HD). }
        case (decide (k = j)) => [?|NEqk].
        * subst k. exists Null. split; last by left.
          rewrite rcu_hist'_cons_next; [by rewrite rcu_hist'_cons_now|auto].
        * case (decide (k = i)) => [?|NEqk2].
          { subst k. exists (Abs j). split; first by rewrite rcu_hist'_cons_now.
            right. by exists j. }
          { assert (Hk : k ∈ live' (D.1)).
            { by do 2 (apply elem_of_rcu_live'_further in Ink; last auto). }
            destruct W as (_&_&_&_&_&W).
            destruct (W  _ Hk) as [b [Hb1 Hb2]]. exists b. split.
            - rewrite rcu_hist'_cons_next; first rewrite rcu_hist'_cons_next;
                auto.
            - destruct Hb2 as [|[j' [? Hj']]]; first by left.
              right. exists j'. split; first done.
              apply (elem_of_rcu_live'_mono _ ((i, Abs j) :: (j, Null) :: nil));
                last done.
              move => p. rewrite elem_of_cons elem_of_list_singleton.
              move => [[_ ->]|[_ ->]] //. }
  Qed.

  Lemma RCUData_wellformed_delete D i j bj
    (W: wellformed D)
    (Live: i ∈ live D) (Livej: j ∈ live D)
    (NEqi: i ≠ j)
    (NEqi2: ∀ j', bj = Abs j' → i ≠ j' ∧ j ≠ j' ∧ j' ∈ live D)
    (SP: SinglePtr D)
    (Hi: hd_error (hist' (D.1) i) = Some $ Abs j)
    (Hj: hd_error (hist' (D.1) j) = Some bj)
    (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    let D' := ((i, bj) :: (j, Dead):: D.1, D.2) in
    D ⊑ D' → wellformed D'.
  Proof.
    move => D' [[Le1|[Le1 Eq]] Le2].
    - exfalso. move : Live. by rewrite /live Le1 rcu_hist_nil_live_empty.
    - assert (InDi : i ∈ dom (gset aloc) (D.1)).
      { apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem. }
      assert (InDj : j ∈ dom (gset aloc) (D.1)).
      { apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem. }
      assert (InDj' : ∀ j', bj = Abs j' → j' ∈ dom (gset aloc) (D.1)).
      { destruct W as (_&_&_&W&_). assert (In:=(W _ InDj)).
        rewrite Hj in In. move => j' ?. subst bj. apply In. }
      split; last split; last split; last repeat split.
      + rewrite /base -Eq. by apply W.
      + rewrite 2!rcu_hist_dom_cons union_assoc_L subseteq_union_1.
        * by apply W.
        * move => ?. rewrite elem_of_union !elem_of_singleton.
          move => [->|->] //.
      + apply W.
      + move => k Ink.
        case (decide (i = k)) => [?|NEq1].
        * subst k. rewrite rcu_hist'_cons_some.
          destruct bjE as [|[j']]; subst bj; first auto.
          rewrite 2!rcu_hist_dom_cons 2!elem_of_union. right. right. auto.
        * rewrite rcu_hist'_cons_next; last exact NEq1.
          case (decide (j = k)) => [<-|NEq2];
            first by rewrite rcu_hist'_cons_some.
          rewrite rcu_hist'_cons_next; last exact NEq2.
          destruct W as (_ & _ & _ & W & _).
          assert (Ink': k ∈ dom (gset aloc) (D.1)).
          { move: Ink. rewrite 2!rcu_hist_dom_cons. set_solver+NEq1 NEq2. }
          assert (Hk := W k Ink').
          repeat case_match; auto. rewrite 2!rcu_hist_dom_cons. set_solver+Hk.
      + move => k /=. rewrite elem_of_filter. move => [HD HD2].
        apply elem_of_rcu_hist'_further in HD;
          last by (destruct bjE as [|[j']]; subst bj).
        case (decide (i = k)) => [?|NEq].
        * subst k. exfalso. move : HD.
          rewrite rcu_hist'_cons_next; last auto. rewrite -elem_of_rcu_dead'.
          by apply elem_of_difference in Live as [_].
        * rewrite rcu_hist'_cons_next; last auto.
          case (decide (j = k)) => [?|NEq2].
          { subst k. exists (hist' (D.1) j). rewrite rcu_hist'_cons_now.
            split; first auto. rewrite -elem_of_rcu_dead'.
            by apply elem_of_difference in Livej as [_]. }
          { rewrite rcu_hist'_cons_next; last auto.
            destruct W as (_&_&_&_&W&_). apply W, elem_of_rcu_dead'.
            by rewrite rcu_hist'_cons_next in HD. }
      + move => k Ink.
        case (decide (i = k)) => [?|NEq].
        * subst k. exists bj. rewrite rcu_hist'_cons_now. split; first auto.
          destruct bjE as [|[j']]; [by left|subst bj; right].
          exists j'. split; first auto.
          apply elem_of_rcu_live'_further; first by destruct (NEqi2 j').
          destruct (NEqi2 j') as (?&?&Livej'); first auto.
          apply elem_of_rcu_live'_further; auto.
        * rewrite rcu_hist'_cons_next; last exact NEq.
          apply elem_of_rcu_live'_further in Ink; last auto.
          case (decide (j = k)) => [?|NEj].
          { subst k. exfalso. apply elem_of_difference in Ink as [_ Ink].
            apply Ink. apply elem_of_rcu_dead'. rewrite rcu_hist'_cons_now.
            by left. }
          { apply elem_of_rcu_live'_further in Ink; last auto.
            rewrite rcu_hist'_cons_next; last auto.
            destruct W as (_&_&_&_&_&W). destruct (W k Ink) as [b [Hb1 Hb2]].
            exists b. split; first auto.
            destruct Hb2 as [|[j0 [Hb2 Hb3]]]; [by left|right].
            exists j0. split; first auto.
            case (decide (j0 = i)) => [?|NEqj0].
            - subst j0.
              apply (elem_of_rcu_live'_mono _ ((i, bj) :: (j, Dead) ::nil));
                last auto.
              move => b'. rewrite elem_of_cons elem_of_list_singleton.
              move => [[->]|[? _]//]. by destruct bjE as [|[j']]; subst.
            - apply elem_of_rcu_live'_further; first auto.
              apply elem_of_rcu_live'_further; last auto.
              move => ?. subst b j0. apply NEq.
              apply (SP _ _ j); by [rewrite Hi|]. }
  Qed.

  Lemma RCUData_wellformed_update D i j bj x γ
    (W: wellformed D)
    (Live: i ∈ live D) (Livej: j ∈ live D)
    (NEqi: i ≠ j)
    (NEqi2: ∀ j', bj = Abs j' → i ≠ j' ∧ j ≠ j' ∧ j' ∈ live D)
    (SP: SinglePtr D)
    (Hi: hd_error (hist' (D.1) i) = Some $ Abs j)
    (Hj: hd_error (hist' (D.1) j) = Some bj)
    (bjE : bj = Null ∨ ∃ j', bj = Abs j'):
    let j0 := fresh (dom (gset aloc) D) in
    let D' := ((i, Abs j0) :: (j0, bj) :: (j, Dead) :: D.1, (j0,x,γ) :: D.2) in
    D ⊑ D' → wellformed D'.
  Proof.
    move => j0 D' [[Le1|[Le1 Eq]] Le2].
    - exfalso. move : Live. by rewrite /live Le1 rcu_hist_nil_live_empty.
    - assert (InDi : i ∈ dom (gset aloc) (D.1)).
      { apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem. }
      assert (InDj : j ∈ dom (gset aloc) (D.1)).
      { apply elem_of_rcu_hist_dom. eexists. by apply hd_error_some_elem. }
      assert (InDj': ∀ j', bj = Abs j' → j' ∈ dom (gset aloc) (D.1)).
      { destruct W as (_&_&_&W&_). assert (In:=(W _ InDj)).
        rewrite Hj in In. move => j' ?. subst bj. apply In. }
      assert (HN: ∀ k, k ∈ dom (gset _) (D.1) → k ≠ j0).
      { move => ?? Eqk. apply (is_fresh (dom (gset aloc) D)).
        by rewrite -/j0 -Eqk. }
      assert (i ≠ j0). { apply HN. by apply elem_of_live'_dom. }
      assert (HNil: hist' (D.1) j0 = nil).
      { rewrite /hist' (_ : {[p <- D.1 | p.1 = j0]} = nil); first done.
        apply list_filter_nil. move => [??]?. apply HN.
        apply elem_of_rcu_hist_dom_2. by eexists. }
      split; last split; last split; last repeat split.
      + rewrite /base -Eq. by apply W.
      + rewrite 3!rcu_hist_dom_cons rcu_info_dom_cons
                union_comm_L -union_assoc_L (union_comm_L _ {[i]})
                (union_assoc_L {[i]}).
        rewrite (subseteq_union_1 {[i; j]}).
        * destruct W as (_ & W & _). by rewrite W.
        * move => k. rewrite elem_of_union !elem_of_singleton.
          move => [->|->]; by apply elem_of_live'_dom.
      + move => i'. rewrite rcu_info_dom_cons elem_of_union elem_of_singleton.
        move => [->|In].
        * rewrite /info' list_filter_cons; last auto.
          simpl. f_equal. apply length_zero_iff_nil.
          rewrite (_: {[p <- D.2 | p.1.1 = j0]} = nil); first done.
          apply list_filter_nil.
          move => a Ina Eqj.
          apply (is_fresh (dom (gset aloc) D)).
          destruct W as (_ & W & _).
          rewrite {2}/dom /RCUData_dom W.
          rewrite /= /RCUInfo_dom elem_of_of_list elem_of_list_fmap.
          exists a. by rewrite -/j0 -Eqj.
        * rewrite /info' list_filter_cons_not; first by apply W.
          move => ?. subst i'. move : In.
          destruct W as (_ & W & _). rewrite -W. apply is_fresh.
      + move => k Ink.
        case (decide (i = k)) => [<-|NEq1].
        * rewrite rcu_hist'_cons_some 2!rcu_hist_dom_cons. set_solver+.
        * rewrite rcu_hist'_cons_next; last exact NEq1.
          case (decide (j0 = k)) => [<-|NEq2].
          { rewrite rcu_hist'_cons_some.
            destruct bjE as [|[j']]; subst bj; first auto.
            apply (elem_of_rcu_hist_dom_mono
                      ((i, Abs j0) :: (j0, Abs j') :: (j, Dead) :: nil) 
                      _ _ (InDj' j' eq_refl)). }
          { rewrite rcu_hist'_cons_next; last exact NEq2.
            case (decide (j = k)) => [->|NEq3];
              first by rewrite rcu_hist'_cons_now.
            rewrite rcu_hist'_cons_next; last auto.
            destruct W as (_ & _ & _ & W & _).
            assert (Ink': k ∈ dom (gset aloc) (D.1)).
            { move: Ink. rewrite 3!rcu_hist_dom_cons.
              set_solver+NEq1 NEq2 NEq3. }
            assert (Hk := W k Ink').
            repeat case_match; auto.
            apply (elem_of_rcu_hist_dom_mono
                      ((i, Abs j0) :: (j0, Abs a0) :: (j, Dead) :: nil)
                      _ _ Hk). }
      + move => k /=. rewrite elem_of_filter. move => [HD HD2].
        apply elem_of_rcu_hist'_further in HD; last done.
        apply elem_of_rcu_hist'_further in HD;
          last by (destruct bjE as [|[j']]; subst bj).
        case (decide (i = k)) => [?|NEq].
        * subst k. exfalso. move : HD.
          rewrite rcu_hist'_cons_next; last auto. rewrite -elem_of_rcu_dead'.
          by apply elem_of_difference in Live as [_].
        * rewrite rcu_hist'_cons_next; last auto.
          case (decide (j0 = k)) => [?|NEq2].
          { subst k. exfalso. move : HD.
            rewrite rcu_hist'_cons_next; last auto.
            rewrite HNil. apply not_elem_of_nil. }
          { rewrite rcu_hist'_cons_next; last auto.
            case (decide (j = k)) => [?|NEq3].
            - subst k. rewrite rcu_hist'_cons_now.
              exists (hist' (D.1) j). split; first auto.
              rewrite -elem_of_rcu_dead'.
              by apply elem_of_difference in Livej as [_].
            - rewrite rcu_hist'_cons_next; last exact NEq3.
              destruct W as (_&_&_&_&W&_). apply W, elem_of_rcu_dead'.
              by rewrite rcu_hist'_cons_next in HD. }
      + move => k Ink.
        assert (j0 ∈ live' (D'.1)).
        { apply elem_of_difference. split.
          - apply elem_of_of_list, elem_of_list_fmap. exists (j0, bj).
            split; first auto. right. left.
          - move => /elem_of_filter [HD _].
            apply elem_of_rcu_hist'_further in HD; last auto.
            apply elem_of_rcu_hist'_further in HD;
              last by (destruct bjE as [|[j']]; subst bj).
            rewrite rcu_hist'_cons_next in HD.
            + rewrite HNil in HD. apply (not_elem_of_nil _ HD).
            + apply HN. by apply elem_of_live'_dom. }
        case (decide (i = k)) => [?|NEq].
        * subst k. rewrite rcu_hist'_cons_now. exists (Abs j0).
          split; [auto|right]. by exists j0.
        * rewrite rcu_hist'_cons_next; last exact NEq.
          case (decide (j0 = k)) => [?|NEqj0].
          { subst k. rewrite rcu_hist'_cons_now. exists bj.
            split; first auto.
            destruct bjE as [|[j']]; [by left|subst bj; right].
            exists j'. split; first auto.
            destruct (NEqi2 j') as (?&?&?); first auto.
            apply elem_of_rcu_live'_further; first auto.
            apply elem_of_rcu_live'_further.
            - apply HN. by apply elem_of_live'_dom. 
            - apply elem_of_rcu_live'_further; auto. }
          { rewrite rcu_hist'_cons_next; last exact NEqj0.
            case (decide (j = k)) => [?|NEj].
            - subst k. exfalso. apply elem_of_difference in Ink as [_ Ink].
              apply Ink. apply elem_of_rcu_dead'.
              rewrite elem_of_list_fmap. setoid_rewrite elem_of_list_filter.
              exists (j, Dead). split; last split; auto. right. right. left.
            - rewrite rcu_hist'_cons_next; last auto.
              destruct W as (_&_&_&_&_&W).
              do 3 (apply elem_of_rcu_live'_further in Ink; last auto).
              destruct (W k Ink) as [b [Hb1 Hb2]].
              exists b. split; first auto.
              destruct Hb2 as [|[j1 [Hb2 Hb3]]]; [by left|right].
              exists j1. split; first auto.
              case (decide (j1 = i)) => [?|NEqj1].
              + subst j1.
                apply (elem_of_rcu_live'_mono _
                        ((i, Abs j0) :: (j0, bj) :: (j, Dead) :: nil));
                  last auto.
                move => b'. rewrite 2!elem_of_cons elem_of_list_singleton.
                move => [[->]//|[[_ ->]|[? //]]].
                by destruct bjE as [|[]]; subst.
              + case (decide (j1 = j0)) => [->|NEqj2]; first auto.
                do 2 (apply elem_of_rcu_live'_further; first auto).
                subst b. case (decide (j1 = j)) => [?|NEqj3].
                * subst. exfalso.
                  apply NEq, (SP _ _ j); by [rewrite Hi|rewrite Hb1].
                * apply elem_of_rcu_live'_further; auto. }
  Qed.

End RCUData.


Section cmra_RCU.

  Implicit Types (D: RCUData) (H: RCUHist) (M: RCUInfo).

  Inductive dlist :=
    | DList D (W: uwellformed D) : dlist
    | DListBot : dlist.

  Inductive dlist_equiv : Equiv dlist :=
    | DList_equiv D1 D2 (W1: uwellformed D1) (W2: uwellformed D2):
        D1 = D2 → DList D1 W1 ≡ DList D2 W2
    | DListBot_equiv : DListBot ≡ DListBot.

  Existing Instance dlist_equiv.
  Instance dlist_equiv_Equivalence : @Equivalence dlist equiv.
  Proof.
    split.
    - move => [|]; by constructor.
    - move => [??|] [??|]; inversion 1; subst; by constructor.
    - move => [??|] [??|] [??|];
      inversion 1; inversion 1; subst; by constructor.
  Qed.

  Canonical Structure dlistC : ofeT := discreteC dlist.

  Instance dlist_valid : Valid dlist :=
    λ x, match x with DList _ _ => True | DListBot => False end.

  Instance dlist_op : Op dlist := λ x y,
    match x, y with
    | DList D1 W1, DList D2 W2 =>
        if (decide (D1 ⊑ D2))
        then DList D2 W2
        else
          if (decide (D2 ⊑ D1))
          then DList D1 W1
          else DListBot
    | _, _ => DListBot
    end.

  Arguments op _ _ !_ !_ /.

  Instance dlist_PCore : PCore dlist := Some.

  Global Instance dlist_op_comm: Comm equiv dlist_op.
  Proof.
    intros [D1 W1|] [D2 W2|]; auto. simpl.
    case_decide; [case_decide|auto]; last auto.
    constructor. by apply: anti_symm.
  Qed.

  Global Instance dlist_op_idemp : IdemP eq dlist_op.
  Proof. intros [|]; [by simpl; rewrite decide_True|auto]. Qed.

  Lemma dlist_op_l D1 D2 W1 W2 (Le: D1 ⊑ D2) :
    DList D1 W1 ⋅ DList D2 W2 = DList D2 W2.
  Proof. simpl. case_decide; done. Qed.

  Lemma dlist_op_r D1 D2 W1 W2 (Le: D1 ⊑ D2) :
    DList D2 W2 ⋅ DList D1 W1 ≡ DList D2 W2.
  Proof. by rewrite (comm (op: Op dlist)) dlist_op_l. Qed.

  Global Instance dlist_op_assoc: Assoc equiv (op: Op dlist).
  Proof.
    intros [D1 W1|] [D2 W2|] [D3 W3|]; eauto; simpl.
    - repeat (case_decide; auto).
      + rewrite !dlist_op_l; auto. etrans; eauto.
      + simpl. repeat case_decide; last done; exfalso.
        * by destruct (RCUData_ext_share_total _ _ D3 W1 W2).
        * apply H1. by etrans.
      + rewrite dlist_op_l; [by rewrite dlist_op_r|auto].
      + rewrite !dlist_op_r; auto. by etrans.
      + simpl. rewrite !decide_False; auto.
      + simpl. rewrite !decide_False; auto.
      + simpl. case_decide.
        * exfalso. apply H. by etrans.
        * case_decide; last done. exfalso.
          by destruct (RCUData_ext_share_total _ _ D1 W2 W3).
    - simpl. repeat case_decide; auto.
  Qed.

  Lemma dlist_included D1 W1 D2 W2 :
    DList D1 W1 ≼ DList D2 W2 ↔ D1 ⊑ D2.
  Proof.
    split.
    - move => [[??|]]; simpl; last inversion 1.
      case_decide; first by (inversion 1; subst).
      case_decide; inversion 1. by subst.
    - intros. exists (DList D2 W2). by rewrite dlist_op_l.
  Qed.

  Lemma dlist_valid_op D1 D2 W1 W2 :
    ✓ (DList D1 W1 ⋅ DList D2 W2) → D1 ⊑ D2 ∨ D2 ⊑ D1.
  Proof. simpl. case_decide; first by left. case_decide; [by right|done]. Qed.

  Lemma dlist_core_self (X: dlist) : core X = X.
  Proof. done. Qed.

  Instance dlist_empty : Empty dlist := DList (nil,nil) (or_introl eq_refl).

  Definition dlist_ra_mixin : RAMixin dlist.
  Proof.
    apply ra_total_mixin; eauto.
    - intros [??|] [??|] [??|]; auto; inversion 1.
      subst. simpl. repeat case_decide; done.
    - by destruct 1; constructor.
    - by destruct 1.
    - apply dlist_op_assoc.
    - apply dlist_op_comm.
    - intros ?. by rewrite dlist_core_self idemp_L.
    - intros [|] [|]; simpl; done.
  Qed.

  Canonical Structure dlistR := discreteR dlist dlist_ra_mixin.

  Global Instance dlistR_cmra_discrete : CMRADiscrete dlistR.
  Proof. apply discrete_cmra_discrete. Qed.

  Definition dlist_ucmra_mixin : UCMRAMixin dlist.
  Proof.
    split; [done| |auto]. intros [|]; [simpl|done].
    rewrite decide_True; [auto|split]; [by left|apply suffix_nil].
  Qed.

  Canonical Structure dlistUR :=
    UCMRAT dlist dlist_ucmra_mixin.

  Lemma dlist_local_update D1 W1 X D2 W2 :
    D1 ⊑ D2 → (DList D1 W1, X) ~l~> (DList D2 W2, DList D2 W2).
  Proof.
    intros Le. rewrite local_update_discrete.
    move => [[D3 W3|]|] /= ? Eq; split => //; last first; move : Eq.
    - destruct X; by inversion 1.
    - destruct X; rewrite /cmra_op /= => Eq;
      repeat case_decide; auto; inversion Eq; subst.
      + constructor. by apply: anti_symm.
      + by exfalso.
      + constructor. apply : anti_symm; [done|by etrans].
      + exfalso. apply H2. by etrans.
  Qed.

  Definition rcuHistR := authR dlistUR.

  Definition rcuTokR := gmapUR nat (coPset_disjUR).

  Class rcuG Σ := RCUG { rcuHistG :> inG Σ rcuHistR ; rcuTokG :> inG Σ rcuTokR }.

End cmra_RCU.
