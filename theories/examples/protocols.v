From igps.gps Require Export shared.

Canonical Structure natProtocol : protocolT := ProtocolT _ le.

Instance natPrt_facts : protocol_facts (natProtocol).
Proof. esplit; apply _. Qed.

Definition boolProtocol : protocolT := ProtocolT _ implb.

Instance boolPrt_facts : protocol_facts (boolProtocol).
Proof. esplit; try apply _; [by intros []|by intros [] [] []]. Qed.

Definition unitProtocol : protocolT := ProtocolT unit (λ _ _, True).

Instance unitPrt_facts : protocol_facts (unitProtocol).
Proof. esplit; apply _. Qed.