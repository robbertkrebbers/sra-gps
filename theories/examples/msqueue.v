From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.

From igps Require Import notation malloc escrows repeat protocols.
From igps.gps Require Import shared plain recursive.
From igps.examples Require Import unit_token.
Import uPred.

(** Simplified formalization of Michael-Scott queue
      thanks to single-writer protocols **)

Section MSQueue.
  Local Notation link := 0.
  Local Notation data := 1.
  Local Notation head := 0.
  Local Notation tail := 1.

  Definition newQueue : base.val :=
    λ: <>,
      let: "s" := Alloc in
      ["s" + #link]_na <- #0  ;;
      let: "q" := malloc (#2) in
      ["q" + #head]_na <- ((int) "s") ;;
      ["q" + #tail]_na <- ((int) "s") ;;
      "q".

  Definition findTail : base.val :=
    λ: "q",
      let: "n" := ["q" + #tail]_at in
      let: "n'" := [((loc)"n") + #link]_at in
      if: "n'" = #0
      then "n"
      else
        ["q" + #tail]_at <- "n'" ;;
        #0.

  Definition tryEnq : base.val :=
    λ: "q" "x",
      let: "n" := malloc (#2) in
      ["n" + #data]_na <- "x" ;;
      ["n" + #link]_na <- #0;;
      let: "t" := repeat (findTail "q") in
      if: (CAS (((loc)"t") + #link) #0 ((int)"n"))
      then ["q" + #tail]_at <- ((int)"n") ;; #1
      else #0.

  Definition tryDeq : base.val :=
    λ: "q", 
      let: "s" := ["q" + #head]_at in
      let: "n" := [((loc)"s") + #link]_at in
      if: "n" = #0
      then #0
      else
        if: (CAS ("q" + #head) "s" "n")
        then [((loc)"n") + #data]_na
        else #0.

  Definition LinkState: Type := () + loc.

  Definition LSRel : relation LinkState :=
    λ a b, match a with
          | inl () => True
          | inr l =>
            match b with
            | inl () => False
            | inr l' => decide (l = l')
            end
          end.

  Canonical LinkProtocol : protocolT := ProtocolT _ LSRel.

  Instance LinkPrt_facts : protocol_facts LinkProtocol.
  Proof. esplit.
    - move => [[] //|l /=]. case (decide (l = l)) => //.
    - move => [[] //|l1] [[] //|l2] [[] //|l3] /=.
      case (decide (l1 = l2)) => [/= -> //|//].
    - apply (populate (inl ())).
    - apply sum_countable.
  Qed.

  Local Notation Null := (inl () : pr_state LinkProtocol).
  Local Notation Linked l := (inr l : pr_state LinkProtocol).

  Section Interp.
    Context `{fG : foundationG Σ}
              `{aG: absViewG Σ, msquG: !uTokG Σ}
              `{gpG: !gpsG Σ LinkProtocol _}
              `{agreeG : !gps_agreeG Σ LinkProtocol}
              `{persG : persistorG Σ}.

    Set Default Proof Using "Type".
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).

    Context (P : Z → vPred).

    Definition DEQ (l: loc) (γ γ': gname) : vPred
        := [es Tok γ ⇝ ∃ v, l ↦ v ∗ P v ∗ Tok γ'].

    Local Open Scope VP.
    Definition Link' : (gname -c> _) -> gname -c> interpC Σ LinkProtocol :=
      (fun F γ b l s z =>
        match s with
        | inl _ => z = 0
        | inr l' => (z = Z.pos l')
                    ∗ ∃ γ', DEQ (ZplusPos data l') γ γ' 
                            ∗ [PP (ZplusPos link l') in Null @ γ' | F γ']
        end).
    Close Scope VP.

    Global Instance Link'_contractive:
        Contractive (Link').
    Proof.
      intros ? ? ? H ? ? ? s ? ?.
      destruct s; [done|]. apply uPred.sep_ne; [done|].
      apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply vGPS_PP_contractive. destruct n => //.
      by apply H.
    Qed.

    Definition Link : ∀ γ, interpC Σ LinkProtocol :=
      [rec Link'].

    Instance Link'_persistent γ F b l s z (V: View):
      PersistentP ((Link' γ F b l s z) V).
    Proof. iIntros "Link". destruct s; by iDestruct "Link" as "#?". Qed.

    Global Instance Link_persistent γ b l s z (V: View) :
      PersistentP (Link γ b l s z V).
    Proof.
      rewrite /PersistentP (unf_int Link).
      apply (_ : PersistentP _).
    Qed.

    Local Open Scope VP.
    Definition Head : interpC Σ unitProtocol :=
      λ b _ _ x,
        if b
        then 
          ∃ γ, [PP (Z.to_pos (x + link)%Z) in Null @ γ | Link γ] ∗ Tok γ
        else
          ■ (0 < x) ∗ ∃ γ, [PP (Z.to_pos (x + link)%Z) in Null @ γ | Link γ].

    Definition Tail : interpC Σ unitProtocol :=
      λ b _ _ x,
        if b then True
        else
          ■ (0 < x) ∗ ∃ γ, [PP (Z.to_pos (x + link)%Z) in Null @ γ | Link γ].
    Close Scope VP.

  End Interp.

  Section proof.
    Context `{fG : foundationG Σ}
              `{aG: absViewG Σ, msquG: !uTokG Σ}
              `{gpG: !gpsG Σ LinkProtocol _}
              `{gpG2: !gpsG Σ unitProtocol _}
              `{agreeG : !gps_agreeG Σ LinkProtocol}
              `{agreeG2 : !gps_agreeG Σ unitProtocol}
              `{persG : persistorG Σ}.

    Set Default Proof Using "Type".
    Local Notation iProp := (iProp Σ).
    Local Notation vPred := (@vPred Σ).

    Context (P : Z → vPred).

    Definition Queue (q: loc) : vPred :=
      [PP (ZplusPos head q) in () | Head P] ∗ [PP (ZplusPos tail q) in () | Tail P].

    Instance Queue_persistent q V: PersistentP (Queue q V) := _.

    Lemma newQueue_spec:
      {{{{ ☐ PersistorCtx }}}}
          (newQueue #())
      {{{{ q, RET #q; Queue q }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS #kP Post".
      wp_lam. iNext.
      iMod (own_alloc (Excl ())) as (γ) "Tok"; first done.
      wp_bind (Alloc).
      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (s) "kS os".
      wp_let. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      rewrite (_: (ZplusPos 0 s) = s); last done.

      iApply (GPS_PP_Init (p:=γ) (Link P γ) Null
               with "[%] kI kS [$kP $os]"); [done|done|done|..].
      { iNext. rewrite !(unf_int (Link P)) /=. by iSplit. }
      iNext. iViewUp. iIntros "kS #Links".
      wp_seq. iNext. wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].
      iNext. iViewUp. iIntros (q) "kS oLs".
      rewrite -bigop.vPred_big_opL_fold big_op.big_sepL_cons big_op.big_opL_singleton.
      iDestruct "oLs" as "(oH & oT)".

      wp_let. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext. wp_op. iNext.

      iApply (GPS_PP_Init_default (Head P) ()
               with "[%] kI kS [$oH Tok]"); [done|done|done|..].
      { iSplitL ""; first done. iNext. iSplitL "Tok".
        - iExists γ. by iFrame "Tok".
        - iSplit; first done. by iExists γ. }

      iNext. iViewUp. iIntros "kS Head".
      wp_seq. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext. wp_op. iNext.
      iApply (GPS_PP_Init_default (Tail P) ()
               with "[%] kI kS [$oT]"); [done|done|done|..].
      { iSplitL ""; first done.
        iNext. iSplit; first done. iSplit; first done. by iExists γ. }

      iNext. iViewUp. iIntros "kS Tail".
      wp_seq. iNext. wp_value.
      by iApply ("Post" with "[%] kS [$Head $Tail]").
    Qed.

    Lemma findTail_spec q:
      {{{{ Queue q }}}}
        findTail #q
      {{{{ (n: Z), RET #n; ■(n = 0) ∨ 
            ■(n > 0) ∗ ∃ γ, [PP (Z.to_pos (n + link)%Z) in Null @ γ | Link P γ] }}}}.
    Proof.
      intros. iViewUp; iIntros "#kI kS #(oH & oT) Post".
      wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.

      set Q : pr_state unitProtocol → Z → vPred 
        := λ _ x, (■ (0 < x)
              ∗ (∃ γ : gname, [PP Z.to_pos (x + 0) in Null @ γ | Link P γ ]))%VP.
      iApply (GPS_PP_Read (p:=()) (Tail P) True Q with "[%] kI kS [$oT]");
        [done|done|done|..].
      { iNext. iSplitL "".
        - iIntros ([] ?) "_ _". iLeft. by iIntros (x ?) "_ ($ & _)".
        - iIntros (? [] x ?) "_ (_ & #?)". by iFrame "#". }
      iNext. iViewUp. iIntros ([] x) "kS (_ & _ & % & ox)".
      wp_seq. iNext. wp_bind ([_]_at)%E. wp_op. iNext. wp_op. iNext.
      rewrite (_ : ZplusPos 0 (Z.to_pos x) = Z.to_pos (x + 0)); last first.
      { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; last done. omega. }
      iDestruct "ox" as (γ) "#ox".
      set Q' : pr_state LinkProtocol → Z → vPred
      := λ s z, (■ (z ≠ 0) → (■ (0 < z)
                       ∗ ∃ γ', [PP Z.to_pos (z + link) in Null @ γ' | Link P γ']))%VP.
      iApply (GPS_PP_Read (p:=γ) (Link P γ) True Q' with "[%] kI kS [$ox]");
        [done|done|done|..].
      { iNext. iSplitL "".
        - iIntros (s'). iViewUp. iIntros "_". iLeft.
          iIntros (v). iViewUp. iIntros "(LinkInt & _)".
          rewrite (unf_int (Link P)).
          iModIntro. iViewUp. iIntros "%". destruct s' as [|l'].
          + by iDestruct "LinkInt" as "%". 
          + iDestruct "LinkInt" as "(% & LinkInt)".
            iDestruct "LinkInt" as (γ') "(_ & Link)".
            iSplitL ""; first (iPureIntro; lia).
            iExists γ'. subst v. iFrame "Link".
        - iIntros (? s v ?) "_ (_ & #?)". by iFrame "#". }

      iNext. iViewUp. iIntros (s n') "kS (% & _ & Q)".
      wp_seq. iNext. wp_op => [Eqr|NEqr]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%] kS [ox]"); [done|].
        iRight. iSplit; first (iPureIntro; omega).
        by iExists _.
      - iApply wp_if_false. iNext. wp_bind ([_]_at <- _)%E. wp_op. iNext.
        iApply (GPS_PP_Write (p:=()) (Tail P) () (Q' s n') (λ _, True)%VP
                with "[%] kI kS [Q oT]");
          [done|done|done|done|..].
        { iSplitR "Q"; last by iFrame "Q oT".
          iNext. iIntros (?) "_ (Q & _)".
          iSplitR "Q"; first done. iModIntro. iNext. iSplitR "Q"; first done.
          by iApply ("Q" with "[%]"). }

        iNext. iViewUp. iIntros "kS oT2".
        wp_seq. iNext. wp_value.
        iApply ("Post" with "[%] kS []"); [done|by iLeft].
    Qed.

    Lemma tryEnq_spec q x:
      {{{{ ☐ PersistorCtx ∗ Queue q ∗ P x }}}}
        tryEnq #q #x
      {{{{ (z: Z), RET #z; ■ (z ≠ 0) ∨ P x }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS (#kP & #(oH & oT) & P) Post".
      wp_lam. iNext. wp_value. wp_let. iNext.
      wp_bind (malloc _).

      iApply (wp_mask_mono); first auto.
      iApply (malloc_spec with "[%] kI kS []"); [omega|done|done|].
      iNext. iViewUp. iIntros (n) "kS oLs".
      rewrite -bigop.vPred_big_opL_fold
        big_op.big_sepL_cons big_op.big_opL_singleton.
      iDestruct "oLs" as "(ol & od)".
      wp_let. iNext. wp_bind ([_]_na <- _)%E. wp_op. iNext.
      iApply (na_write with "[%] kI kS od"); [done|done|].
      iNext. iViewUp. iIntros "kS od". wp_seq.
      iNext. wp_bind([_]_na <- _)%E. wp_op. iNext.

      iMod (own_alloc (Excl ())) as (γ') "Tok"; first done.
      iApply (GPS_PP_Init (p:=γ')  (Link P γ') Null
               with "[%] kI kS [$kP $ol]"); [done|done|done|..].
      { iNext. rewrite !(unf_int (Link P)). by iSplit. }

      iNext. iViewUp. iIntros "kS Link'". wp_seq. iNext.
      wp_bind ((rec: "f" <> := _) _)%E.
      (iLöb as "IH" forall (V_l) "kP oH oT").
      iApply wp_repeat; first auto.
      iApply (findTail_spec q with "[%] kI kS [$oH $oT]"); first done.
      iNext. iViewUp. iIntros (t) "kS Cases".
      iExists t. iSplit; first done.
      iDestruct "Cases" as "[%|[% Link]]".
      - erewrite decide_True; last done. do 2 iNext.
        iApply ("IH" $! _ with "P Post od Tok kS Link'").
        + iIntros "!#"; iFrame "kP".
        + iIntros "!#"; iFrame "oH".
        + iIntros "!#"; iFrame "oT".
      - iClear "IH". erewrite decide_False; last omega. do 2 iNext.
        wp_seq. iNext. wp_bind (CAS _ _ _)%E.
        wp_op. iNext. wp_op. iNext. wp_op. iNext.
        rewrite (_: ZplusPos 0 (Z.to_pos t) = Z.to_pos (t + link)); last first.
        { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; omega. }

        iDestruct "Link" as (γ) "Link".
        set P': vPred := (ZplusPos 1 n ↦ x ∗ P x ∗ Tok γ')%VP.
        set Q : pr_state LinkProtocol → vPred
          := λ s, (■ (s = Linked n))%VP.
        set R: pr_state LinkProtocol → Z → vPred
          := λ _ _, (P x ∗ ZplusPos 1 n ↦ x)%VP.
        iDestruct "Link'" as "#Link'".
        iApply (GPS_PP_CAS (p:=γ) (Link P γ) P' Q R
                  with "[%] kI kS [P Tok od Link]");
          [done|done|done|..].
        { iFrame "Tok Link". iSplitR "P od"; last iSplitR "P od"; last first.
          - iFrame "P od". iNext.
            iIntros (? ? ? ?) "_ (_ & #?)". by iFrame "#".
          - iNext. iIntros (???) "_ (_ & _ & _ & ? & ? & _)". by iFrame.
          - iNext. iIntros (s'). iViewUp. iIntros "(_ & LinkInt & P')".
            rewrite (unf_int (Link P)).
            destruct s' as [[]|l'].
            + iExists (Linked n).
              iSplitR "P'"; first done. iSplitR "P'"; first done.
              destruct (escrow_alloc (Tok γ)
                          (∃ v, ZplusPos data n ↦ v ∗ P v ∗ Tok γ')) as [EA].
              iDestruct (EA with "[P']") as "DEQ"; [done|..].
              { iNext. iExists x. by iFrame "P'". }
              iMod (fupd_mask_mono with "DEQ") as "#DEQ"; first solve_ndisj.
              iModIntro. iNext.
              rewrite !(unf_int (Link P)).
              iSplitL ""; (iSplitL ""; first done);
                iExists γ'; iFrame "DEQ"; iFrame "Link'".
            + iDestruct "LinkInt" as "(% & _)". exfalso. lia. }

        iNext. iViewUp. iIntros (s' [] v) "kS (_ & Link & IF)".
        + iApply wp_if_true. iNext.
          wp_bind ([_]_at <- _)%E. wp_op. iNext. wp_op. iNext.
          iApply (GPS_PP_Write (p:=()) (Tail P) () True%VP (λ _, True)%VP
                with "[%] kI kS [oT]");
                [done|done|done|done|..].
          { iFrame "oT". iNext. iSplitL; last done.
            iViewUp. iIntros "_". iModIntro. iSplitL; first done.
            iNext. iSplitL; first done. iSplitL.
            - iPureIntro. lia.
            - by iExists γ'. }
          iNext. iViewUp. iIntros "kS oT2".
          wp_seq. iNext. wp_value.
          iApply ("Post" with "[%] kS []"); [done|by iLeft].
        + iApply wp_if_false. iNext. wp_value.
          iApply ("Post" with "[%] kS [IF]"); [done|iRight].
          by iDestruct "IF" as "[$ ?]".
    Qed.

    Lemma tryDeq_spec q:
      {{{{ Queue q }}}}
        tryDeq #q
      {{{{ x, RET #x; ■ (x = 0) ∨ P x }}}}.
    Proof.
      intros. iViewUp. iIntros "#kI kS #(oH & oT) Post".
      wp_lam. iNext. wp_bind ([_]_at)%E. wp_op. iNext.
      set Q : pr_state unitProtocol → Z → vPred
        := λ _ x, (■ (0 < x)
              ∗ (∃ γ : gname, [PP Z.to_pos (x + 0) in Null @ γ | Link P γ ]))%VP.
      iApply (GPS_PP_Read (p:=()) (Head P) True Q with "[%] kI kS [$oH]");
        [done|done|done|..].
      { iNext. iSplitL.
        - iIntros ([] ?) "_ _". iLeft. by iIntros (x ?) "_ ($ & _)".
        - iIntros (? ? x ?) "_ (_ & #?)". by iFrame "#". }

      iNext. iViewUp. iIntros ([] s) "kS (_ & oH2 & % & os)".
      iDestruct "os" as (γ) "os".

      wp_seq. iNext. wp_bind ([_]_at)%E.
      wp_op. iNext. wp_op. iNext.
      rewrite (_: ZplusPos 0 (Z.to_pos s) = Z.to_pos (s + 0));
        last first. { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; omega. }

      set Q' : pr_state LinkProtocol → Z → vPred
        := λ s' n, 
          (■ (0 ≠ n)
              → (■ (0 < n) ∗ ∃ γ', DEQ P (Z.to_pos (n + data)) γ γ'
                      ∗ [PP Z.to_pos (n + link) in Null @ γ' | Link P γ']))%VP.
      iApply (GPS_PP_Read (p:=γ) (Link P γ) True Q' with "[%] kI kS [$os]");
        [done|done|done|..].
      { iNext. iSplitL.
        - iIntros (s'). iViewUp. iIntros "_". iLeft.
          iIntros (n). iViewUp. iIntros "(LinkInt & _)".
          rewrite (unf_int (Link P)).
          iModIntro. iViewUp. iIntros "%".
          destruct s' as [|l'].
          + by iDestruct "LinkInt" as "%".
          + iDestruct "LinkInt" as "(% & LinkInt)".
            iSplitR "LinkInt"; first (iPureIntro; lia).
            iDestruct "LinkInt" as (γ') "(DEQ & Link)".
            iExists γ'. rewrite H1.
            rewrite (_: Z.to_pos (Z.pos l' + 1) = ZplusPos 1 l'); last first.
            { rewrite /ZplusPos. f_equal. omega. }
            iFrame "DEQ Link".
        - iIntros (????) "_ (_ & #?)". by iFrame "#". }

      iNext. iViewUp. iIntros (s' n) "kS (_ & #os & Q')".
      wp_seq. iNext. wp_op => [Eqr|NEqr]; iNext.
      - iApply wp_if_true. iNext. wp_value.
        iApply ("Post" with "[%] kS []"); [done|by iLeft].
      - iApply wp_if_false. iNext. wp_bind (CAS _ _ _)%E. wp_op. iNext.
        iDestruct ("Q'" $! _ with "[%] [%]") as "(% & Q')"; [done|done|].
        iDestruct "Q'" as (γ') "(#DEQ & #on)".

        set Q1 : pr_state unitProtocol → vPred
          := λ _, (▷ ∃ x, (Z.to_pos (n + data)) ↦ x ∗ P x)%VP.
        iApply (GPS_PP_CAS (p:=()) (Head P) True Q1 (λ _ _, True)%VP
                  with "[%] kI kS []"); [done|done|done|..].
        { iSplitL; last iSplitL; last first.
          - iFrame "oH". iNext. iIntros (????) "_ (_ & #?)". by iFrame "#".
          - iNext. by iIntros (???) "_ _".
          - iNext. iIntros ([]). iViewUp.
            iIntros "(_ & Head & _)".
            iDestruct "Head" as (γ2) "(os2 & Tok)".
            iAssert (⌜γ2 = γ⌝)%I with "[#]" as %?.
            { iApply (GPS_PPs_param_agree γ2 γ with "[$os2 $os]"). }
            subst γ2.
            iDestruct ((escrow_apply (Tok γ)
                        (∃ x, (Z.to_pos (n + data)) ↦ x ∗ P x ∗ Tok γ'))
                      with "[]") as "EA".
            { iIntros (??) "_ ?". by iApply unit_tok_exclusive. }
            iSpecialize ("EA" $! _ _ with "[%] [$DEQ $Tok]"); first done.
            iDestruct (fupd_mask_mono _ (⊤ ∖ ↑persist_locN.@ZplusPos 0 q)
                        with "EA") as "EA"; first solve_ndisj.
            iMod "EA" as (x) "(od & P & >Tok)".
            iModIntro. iExists (). iSplitL ""; first auto.
            iSplitL "od P".
            + iExists x. iFrame "od P".
            + iNext. iSplitL "Tok".
              * iExists γ'. iFrame "on Tok".
              * iSplitL; first done. iExists γ'. iFrame "on". }

        iNext. iViewUp. iIntros ([] [] v) "kS (_ & _ & IF)".
        + iApply wp_if_true. iNext. wp_op. iNext. wp_op. iNext.
          rewrite (_ : ZplusPos 1 (Z.to_pos n) = Z.to_pos (n + 1)); last first.
          { rewrite /ZplusPos. f_equal. rewrite Z2Pos.id; [omega|auto]. }
          iDestruct "IF" as (x) "[ox P]".
          iApply (na_read with "[%] kI kS ox"); [done|done|].
          iNext. iViewUp. iIntros (z) "kS (% & ox)".
          subst z.
          iApply ("Post" with "[%] kS [P]"); [done|by iRight].
        + iApply wp_if_false. iNext.
          wp_value.
          iApply ("Post" with "[%] kS []"); [done|by iLeft].
    Qed.
  End proof.
End MSQueue.