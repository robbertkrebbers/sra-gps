From iris.proofmode Require Import tactics.
From iris.base_logic Require Import big_op.
From iris.base_logic.lib Require Import cancelable_invariants sts.
From iris.algebra Require Export cmra_big_op ofe.

Import uPred.

From igps Require Import persistor fractor proofmode weakestpre.
From igps Require Export notation na.
From igps.gps Require Import shared inst_shared plain fractional singlewriter.

Section RecInterp.
  Context {Σ : gFunctors} (Prtcl : protocolT). 
  Notation vPred := (@vPred Σ).
  Set Default Proof Using "Type".

  Global Instance IntType_cofe : Cofe (interpC Σ Prtcl) := _.
  Global Instance intpair_inhabited : Inhabited (interpC Σ Prtcl).
  Proof. constructor; repeat intro; exact True%VP. Qed.

  Section Instances.
    Context `{fG : !foundationG Σ} `{G : @gpsG Σ Prtcl PF}.
    Global Instance gps_inv_ne:
      Proper (dist n ==> dist n) (λ F : interpC _ _, (gps_inv F l γ γx)).
    Proof.
      intros n ? ? ? f1 f2 H.
      repeat apply uPred.exist_ne => ?. rewrite /= /gps_inv /ProtoInv.
      repeat (apply (_ : Proper (_ ==> dist n) _); repeat intro).
      repeat (apply (_ : Proper (_ ==> _ ==> dist n) _); repeat intro); auto;
      apply big_opS_ne; auto => ?; apply H.
    Qed.

    (* Global Instance inv_gps_inv_contractive:
      Contractive (λ F : interpC _ _, inv M (gps_inv F l γ γx)).
    Proof. intros ? ? ? ? n ???. f_contractive. by apply gps_inv_ne. Qed. *)

    Global Instance vGPS_RSP_contractive `{pG : persistorG Σ}:
      Contractive (λ F : interpC _ _, [XP l in s1 | F ]_R V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply uPred.sep_ne; [|done].
      f_contractive. do 2 apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_WSP_contractive `{pG : persistorG Σ}:
      Contractive (λ F : interpC _ _, [XP l in s1 | F ]_W V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply uPred.sep_ne; [|done].
      f_contractive. do 2 apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_FRP_contractive `{aG : cinvG Σ}:
      Contractive (λ F : interpC _ _, [FXP l in s1 | F ]_R_q V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply exist_ne => ?. repeat (apply sep_ne; [done|]).
      f_contractive. do 2 apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_FWP_contractive `{aG : cinvG Σ}:
      Contractive (λ F : interpC _ _, [FXP l in s1 | F ]_W_q V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply exist_ne => ?. repeat (apply sep_ne; [done|]).
      f_contractive. do 2 apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_nFRP_contractive `{aG : cinvG Σ}:
      Contractive (λ F : gname -c> interpC _ _, [nFXP l in s @ γ | F ]_R_q V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply exist_ne => ?. repeat (apply sep_ne; [done|]).
      f_contractive. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_nFWP_contractive `{aG : cinvG Σ}:
      Contractive (λ F : gname -c> interpC _ _, [nFXP l in s @ γ | F ]_W_q V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply exist_ne => ?. repeat (apply sep_ne; [done|]).
      f_contractive. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_nRSP_contractive `{pG : persistorG Σ}:
      Contractive (λ F : gname -c> interpC _ _, [nXP l in s @ γ | F ]_R V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply uPred.sep_ne; [|done].
      f_contractive. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_nWSP_contractive `{pG : persistorG Σ}:
      Contractive (λ F : gname -c> interpC _ _, [nXP l in s @ γ | F ]_W V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|].
      apply uPred.sep_ne; [|done].
      f_contractive. apply uPred.exist_ne => ?.
      apply uPred.sep_ne; [done|by apply gps_inv_ne].
    Qed.

    Global Instance vGPS_PP_contractive
      `{pG : persistorG Σ} `{aG : gps_agreeG Σ Prtcl} `{pC: Countable Param}:
      Contractive (λ F : interpC Σ Prtcl, [PP l in s1 @ p | F ] V).
    Proof.
      repeat intro. apply uPred.exist_ne => ?.
      apply sep_ne; [done|].
      apply sep_ne; [|done].
      apply inv_contractive. destruct n; first done.
      repeat apply exist_ne => ?.
      apply sep_ne; [done|].
      apply sep_ne; [by apply gps_inv_ne|done].
    Qed.

    Global Instance vGPS_FP_contractive `{aG : cinvG Σ}:
      Contractive (λ F : interpC _ _, [FP l in s1 | F ]_q V).
    Proof.
      repeat intro. apply exist_ne => ?. apply sep_ne; [done|].
      apply exist_ne => ?. repeat (apply sep_ne; [done|]).
      f_contractive. repeat apply exist_ne => ?.
      apply sep_ne; [done|].
      apply sep_ne; [by apply gps_inv_ne|done].
    Qed.
  End Instances.
End RecInterp.


Global Instance ProtoInv_proper {Σ Prtcl PF}:
  Proper ((≡) ==> (=) ==> (≡) ==> (=) ==> (≡)) (@ProtoInv Σ Prtcl PF).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ? ? H4.
  unfold ProtoInv. subst. apply uPred.sep_proper.
  - unfold BE_Inv, sEx, sBE.
    rewrite big_opS_proper; last first.
    + intros ??. apply H1.
    + f_equiv; set_solver.
  - rewrite /All_Inv.
    rewrite big_opS_proper; last first.
    + intros ??. apply H1.
    + f_equiv; set_solver.
Qed.

Global Instance gps_inv_proper {Σ fG Prtcl PF gpsG}:
  Proper ((≡) ==> (=) ==> (=) ==> (=) ==> (≡)) (@gps_inv Σ fG Prtcl PF gpsG).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ? ? H4. unfold gps_inv. subst.
  repeat (apply (_ : Proper (_ ==> (≡)) _); repeat intro).
  repeat (apply (_ : Proper (_ ==> _ ==> (≡)) _); repeat intro); auto.
  by apply ProtoInv_proper.
Qed.

Global Instance gpsSWRaw_proper {Σ Prtcl fG PF gpsG}:
  Proper ((≡) ==> (=) ==> (=) ==> (≡)) (@gpsSWRaw Σ Prtcl fG PF gpsG).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3. subst.
  unfold gpsSWRaw.
  repeat (apply (_ : Proper (_ ==> (≡)) _); repeat intro).
  by apply gps_inv_proper.
Qed.

Global Instance vGPS_WSP_proper {Σ Prtcl fG PF gpsG persG}:
  Proper ((≡) ==> (=) ==> (=) ==> (≡)) (@vGPS_WSP Σ Prtcl fG PF gpsG persG).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ?. subst.
  unfold vGPS_WSP. rewrite /vPred_app /= /GPS_WSP /=.
  f_equiv. intros ? ?. by rewrite (gpsSWRaw_proper _ _ H1). 
Qed.

Global Instance vGPS_RSP_proper {Σ Prtcl fG PF gpsG persG}:
  Proper ((≡) ==> (=) ==> (=) ==> (≡)) (@vGPS_RSP Σ Prtcl fG PF gpsG persG).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ?. subst.
  unfold vGPS_RSP. rewrite /vPred_app /= /GPS_RSP /=.
  f_equiv. intros ? ?. by rewrite (gpsSWRaw_proper _ _ H1). 
Qed.

Global Instance gpsPPRaw_proper {Σ Prtcl fG PF gpsG agreeG PType EqPT CountPT}:
  Proper ((≡) ==> (=) ==> (=) ==> (=) ==> (≡))
         (@gpsPRaw Σ Prtcl fG PF gpsG agreeG PType EqPT CountPT).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ?? H4. subst.
  unfold gpsPRaw.
  repeat (apply (_ : Proper (_ ==> (≡)) _); repeat intro).
  repeat (apply (_ : Proper (_ ==> _ ==> (≡)) _); repeat intro); auto.
  by apply gps_inv_proper.
Qed.

Global Instance vGPS_PP_proper {Σ Prtcl fG PF gpsG persG agreeG PType EqPT CountPT}:
  Proper ((≡) ==> (=) ==> (=) ==> (=) ==> (≡))
         (@vGPS_PP Σ Prtcl fG PF gpsG persG agreeG PType EqPT CountPT).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ? ? H4 ?. subst.
  unfold vGPS_PP. rewrite /vPred_app /= /GPS_PP /=.
  f_equiv. intros ? ?. by rewrite H1.
Qed.

Global Instance vGPS_FP_proper {Σ Prtcl fG PF gpsG persG}:
  Proper ((≡) ==> (=) ==> (=) ==> (≡)) (@vGPS_WSP Σ Prtcl fG PF gpsG persG).
Proof.
  intros ? ? H1 ? ? H2 ? ? H3 ?. subst.
  unfold vGPS_PP. rewrite /vPred_app /= /GPS_WSP /=.
  f_equiv. intros ? ?. by rewrite (gpsSWRaw_proper _ _ H1). 
Qed.

Structure OTag := otag { otagged :> ofeT }.
Definition oparam o := otag o.
Canonical ointerp o := oparam o.
Structure TTag := ttag { ttagged :> Type }.
Definition tparam T := ttag T.
Canonical tinterp T := tparam T.

Structure type_ofe :=
  mkto {
      ofe_of_ofe :> OTag;
      type_of_ofe : TTag ;
      _ : ttagged (type_of_ofe) = ofe_car ofe_of_ofe
    }.
(* Arguments type_of_ofe _. *)
Definition type_of_eq (to : type_ofe) : ttagged (type_of_ofe to) = ofe_car (ofe_of_ofe to) := ltac:(by destruct to).
Program Canonical type_of_interp Σ Prtcl : type_ofe :=
  mkto (ointerp (interpC Σ Prtcl)) (tinterp (bool -> loc -> pr_state Prtcl -> Z -> @vPred Σ)) (Logic.eq_refl).
Program Canonical type_of_param X (t : type_ofe) : type_ofe :=
  mkto (oparam (X -c> ofe_of_ofe t)) (tparam (X -> type_of_ofe t)) (f_equal (λ Y, X -> Y) (type_of_eq t)).

(* Global Instance Dist_type_of_ofe o (to : type_ofe o) : Dist (type_of_ofe to). *)
(* Proof. *)
(*   destruct to. *)
(*   intros ?. red. rewrite e. exact (@dist o _ H). *)
(* Defined. *)

Definition type_ofe_conv (to : type_ofe) : ∀ (F : type_of_ofe to -> type_of_ofe to), ofe_of_ofe to -c> ofe_of_ofe to :=
  ltac:(by rewrite type_of_eq).

Structure funC_of :=
  mk_funC_of { funC_ofe : type_ofe;
               _ : Cofe (funC_ofe);
               _ : Inhabited (funC_ofe);
               _ : ∀ F, Contractive (type_ofe_conv funC_ofe F) → ofe_of_ofe funC_ofe;
             }.
Definition funC_Cofe f : Cofe (funC_ofe f) := ltac:(by destruct f).
Definition funC_Inhabited f : Inhabited (funC_ofe f) := ltac:(by destruct f).
Definition funC_fix (f : funC_of) : ∀ F, Contractive (type_ofe_conv (funC_ofe f) F) → funC_ofe f := ltac:(by destruct f).

Canonical funC_interpC Σ Prtcl :=
  mk_funC_of (type_of_interp Σ Prtcl)
             _
             _
             (λ F HF, fixpoint (type_ofe_conv _ F) (Hf := HF))
.
Canonical funC_param (f : funC_of) (X : ofeT) :=
  mk_funC_of (type_of_param X (funC_ofe f))
             (@ofe_fun_cofe _ _ (funC_Cofe f))
             (populate (λ _, @inhabitant _ (funC_Inhabited f))) 
             (λ F HF,
              @fixpoint _ (@ofe_fun_cofe _ _ (funC_Cofe f)) (populate (λ _, @inhabitant _ (funC_Inhabited f))) (type_ofe_conv _ F) HF).

Tactic Notation "exvar" constr(type) tactic(cnt) :=
  let name := fresh in
  evar (name : type);
  let var := (eval unfold name in name) in
  clear name;
  cnt var.
Tactic Notation "recproto" uconstr(proto) tactic(cont) :=
  (* "at" integer_list(occs) := *)
  exvar (funC_of) ltac:(
    fun f =>
      exvar (type_of_ofe (funC_ofe f) → type_of_ofe (funC_ofe f)) ltac:(
        fun F =>
          exvar (Contractive (type_ofe_conv (funC_ofe f) F)) ltac:(
            fun C =>
              let t := type of proto in
              let delete := fresh "delete" in
              (assert (delete : type_of_ofe (funC_ofe f) = tinterp t) by exact: eq_refl);
              clear delete;
              (assert (delete : proto = (funC_fix f F C)) by exact: eq_refl);
              clear delete;
              let F := (eval lazy delta [id] in F) in
              let f := (eval lazy delta [id] in f) in
              let unf := uconstr:(@fixpoint_unfold (funC_ofe f) (funC_Cofe f) (funC_Inhabited f) F (C)) in
              let f' := f in
              let rec go f F unf proto :=
                  (* idtac f; idtac F; *)
                  lazymatch f with
                  | funC_interpC ?Σ ?Prtcl =>
                    cont uconstr:(unf _ _ _ _ : proto _ _ _ _ ≡ F _ _ _ _)
                  | funC_param ?f ?X =>
                    go f uconstr:(F _) uconstr:(unf _) uconstr:(proto _)
                  end
              in
              go f (F proto) unf proto
          )
      )
  ).

Ltac spine_of int :=
  match int with
  | funC_fix ?f ?F ?C => uconstr:((f,F,C))
  | _ => let int' := (eval red in int) in
         let r := spine_of int' in
         uconstr:(r)
  end.

Ltac unfold_lemma int :=
  let orig_int := int in
  let rec unfold_lemma int :=
  match int with
  | funC_fix ?f ?F ?C => 
    let rec go f F unf int :=
        (* idtac f; idtac F; *)
        lazymatch f with
        | funC_interpC ?Σ ?Prtcl =>
          uconstr:(unf _ _ _ _ _ : int _ _ _ _ _ ≡ F _ _ _ _ _)
        | funC_param ?f ?X =>
          go f uconstr:(F _) uconstr:(unf _) uconstr:(int _)
        end
    in
    let unf := uconstr:(@fixpoint_unfold (funC_ofe f) (funC_Cofe f) (funC_Inhabited f) (F) (C)) in
    go (f) (F orig_int) unf orig_int
  | _ => let int' := (eval red in int) in
         let r := unfold_lemma int' in
         uconstr:(r)
  end
    in unfold_lemma int.

Notation unf_int int := (ltac:(let e := unfold_lemma int in refine e)) (only parsing).

Notation "'[rec' F ]" := (funC_fix _ (F : _ -> _) _).

Section Example.
  Definition prot : protocolT := ProtocolT bool (implb).
  Context `{Σ : gFunctors}.
  Context `{fG : foundationG Σ} `{GPSG : !@gpsG Σ prot PF} `{cinvG Σ}.

  (* Typeclasses eauto := debug. *)
  Program Definition int : View -> View -> interpC Σ prot :=
    [rec (fun F V1 V2 b l s v => [FP l in s | F ∅ V1 ]_1)].
  Next Obligation. cbn.
    repeat intro. cbn. apply vGPS_FP_contractive.
    destruct n => //.
    by apply (H0).
  Qed.

  (* Demonstration of recproto_unfold *)
  Lemma int_Exclusive V1 V1' V2 V2' b b' l s s' v v' V V' :
    (int V1 V2 b l s v V) ∗ int V1' V2' b' l s' v' V' -∗ False.
  Proof.
    iIntros "?".
    rewrite !(unf_int int).
  Abort.
End Example.
