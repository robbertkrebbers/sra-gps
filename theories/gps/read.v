From iris.program_logic Require Export weakestpre.
From igps.gps Require Import shared.
From igps.base Require Import at_read.

Section Read.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F := (IP true l).
  Notation F_past := (IP false l).

  Lemma RawProto_Read (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s_r π V V_r v_r γ γ_x (E : coPset) (HEN : ↑physN ⊆ E) (VrV : V_r ⊑ V):
    {{{ (▷ (∀ s', ■ (s_r ⊑ s') →
                (∀ v, F_past s' v ∗ P ={E}=∗ Q s' v)
              ∨ (∀ s'' v, ■ (s' ⊑ s'') → F s'' v ∗ P ={E}=∗ False)))%VP V
        ∗ (▷ (∀ s' v, ■ (s_r ⊑ s') ∗ F_past s' v ={E}=∗ F_past s' v ∗ F_past s' v))%VP ∅
        ∗ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ P V
        ∗ Reader γ {[s_r, (v_r, V_r)]} }}}
      ([ #l ]_at, V) @ E
    {{{ s' v Vr V', RET (#v, V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ⌜s_r ⊑ s'⌝
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ Q s' v V'
        ∗ Reader γ {[s', (v, Vr)]} ∗ ⌜Vr ⊑ V'⌝ ∗ ⌜V !! l ⊑ Vr !! l⌝}}}.
  Proof.
    iIntros (Φ) "(VS & VSDup & #kI & Seen & oI & P & #oR) Post".
    iDestruct "oI" as (ζ' h e_x)
              "(>oA & ? & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".

    iDestruct (Reader_Auth_sub with "[$oA $oR]") as %?%elem_of_subseteq_singleton.
    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.
    iApply (f_read_at with "[$kI $Seen $Hist]").
    - iPureIntro. exists (VInj v_r), V_r. repeat split; last auto.
      rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)).
    - iNext.
      iIntros (v' V') "(% & Seen' & Hist & % & Pure)".
      iDestruct "Pure" as (V'') "(% & % & %)".

      rewrite /Consistent in H2.
      rewrite /value_at in H10.
      assert (InV' := H10). rewrite <- H2 in H10.
      apply elem_of_map_gset in H10 as [[s' [v Vr]] [Eq In]].
      simpl in Eq. inversion Eq. subst v' V''. clear Eq.

      iMod (Reader_fork_Auth_2 _ _ {[s', (v, Vr)]} with "[$oA]") as "[oA #oR']".
        { by apply elem_of_subseteq_singleton. }

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist]") as "(Hist & %)".

      assert (V_r !! l ⊑ Vr !! l).
        { rewrite H9. etrans; eauto. }

      assert (InV_r: (VInj v_r, V_r) ∈ h).
        { rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)). }

      assert (s_r ⊑ s').
        { by apply (H0  _ _ H6 In). }

      iDestruct ("VS" $! s' V with "[%] [%]") as "[VS|VS]"; [done|auto|..].
      + rewrite /All_Inv (big_op.big_sepS_delete _ _ _ In).
        iDestruct "oAI" as "[Fp oAI]".
        iMod ("VSDup" with "[%] [$Fp]") as "[Fp Fp']"; [done|auto|].
        iCombine "Fp'" "oAI" as "oAI".
        rewrite -(big_op.big_sepS_delete _ _ _ In).
        iSpecialize ("VS" $! v V' with "[%]"); [solve_jsl|].
        iMod ("VS" with "[$Fp $P]") as "Q".
        iApply ("Post" $! s' v Vr V').
        iFrame (H5 H8 H12) "Seen' Q oR'". iSplitR ""; last by rewrite H9.
        iModIntro. iNext.
        iExists ζ', h, e_x. iFrame. repeat (iSplit; first auto); auto.

      + set R:= λ e1 e2, st_time l e1 ⊑ st_time l e2.
        assert (Total R). (* TODO: ??? *)
          { move => [? [? V1]] [? [? V2]].
            rewrite /R /st_time /=.
            case (V1 !! l); case (V2 !! l); (try by left); last by right.
            move => t1 t2. cbn. move: (Pos.lt_total t1 t2) => [Lt|[->|Lt]].
            right. by apply Pos.lt_le_incl.
            by left. left. by apply Pos.lt_le_incl. }

        destruct (gset_nonempty_min (R:=flip R) ζ') as [e [Ine [He _]]].
          { apply (non_empty_inhabited _ _ In). }

        assert (Heb: e ∈ sEx l (sBE l ζ') e_x).
          { rewrite !elem_of_filter. repeat split; last auto.
            - by apply He.
            - move => e' Ine' Ne' [t [Eq Eq1]].
              assert (Le: st_time l e' ⊑ st_time l e).
                { by apply He. }
              rewrite Eq Eq1 in Le. move: Le. cbn => Le.
              apply (Pos.lt_irrefl t).
              eapply (Pos.lt_le_trans); last exact Le.
              by apply Pos.lt_add_r. }

        iAssert (⌜s' ⊑ st_prst e⌝)%I as "Hse".
          { iPureIntro. by apply (H0 _ _ In Ine), He. }

        rewrite /BE_Inv (big_op.big_sepS_delete _ _ _ Heb).
        iDestruct "oBEI" as "[F oBEI]".

        iSpecialize ("VS" $! (st_prst e) (st_val e) (st_view e ⊔ V ⊔ Vr)
          with "[%] Hse"); [|].
        { etrans; [apply join_ext_r|apply join_ext_l].
          (* TODO: make solve_jsl do this *) }

        iMod ("VS" $! _ with "[%] [F P]") as "?"; [reflexivity| |by iExFalso].
        iFrame "F P". iPureIntro.
        etrans; last by apply join_ext_l. apply join_ext_r.
  Qed.


  Lemma RawProto_Read_ex s π Vl v V γ γ_x (E : coPset) ζ
        (HEN : ↑physN ⊆ E) (VVl : V ⊑ Vl) (Max: maxTime l V ζ):
    {{{ (▷ (∀ v, F_past s v ={E}=∗ F_past s v ∗ F_past s v))%VP ∅
        ∗ PSCtx ∗ Seen π Vl
        ∗ ▷ (gps_inv IP l γ γ_x)
        ∗ exwr γ_x 1%Qp (s, (v, V))
        ∗ Reader γ {[s, (v, V)]}
        ∗ Writer γ ζ }}}
      ([ #l ]_at, Vl) @ E
    {{{ v' V', RET (#v', V') ;
        ⌜Vl ⊑ V'⌝ ∗ Seen π V'
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ F_past s v' V'
        ∗ exwr γ_x 1%Qp (s, (v, V))
        ∗ Reader γ {[s, (v, V)]}
        ∗ Writer γ ζ }}}.
  Proof.
    iIntros (Φ) "(VSDup & #kI & kS & oI & ofX & #oR & oW) Post".
    iDestruct "oI" as (ζ' h e_x)
              "(>oA & >oAX & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".

    iDestruct (ExWrite_agree with "[$oAX $ofX]") as %?. subst e_x.
    iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.
    iDestruct (Reader_Writer_sub with "[$oW $oR]") as %?%elem_of_subseteq_singleton.

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.
    iApply (f_read_at with "[$kI $kS $Hist]").
    - iPureIntro. exists (VInj v), V. repeat split; last auto.
      rewrite -H2 elem_of_map_gset. by exists (s, (v, V)).
    - iNext.
      iIntros (v' V') "(% & kS' & Hist & % & Pure)".
      iDestruct "Pure" as (V'') "(% & % & %)".
      rewrite /All_Inv (big_op.big_sepS_delete _ _ _ H6).
      iDestruct "oAI" as "[Fp oAI]".
      iMod ("VSDup" with "[%] [$Fp]") as "[Fp Fp']"; [done|].
      iCombine "Fp'" "oAI" as "oAI". rewrite -(big_op.big_sepS_delete _ _ _ H6).

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist]") as "(Hist & %)".

      assert (V !! l = V'' !! l).
      { apply: anti_symm; first (rewrite H9; etrans; eauto).
        move : H10. rewrite /value_at -H2 elem_of_map_gset.
        move => [e [Eqe Ine]]. inversion Eqe. apply (Max _ Ine). }

      destruct H11 as [_ HDisj].
      apply (HDisj (VInj v, V) (VInj v', V'')) in H12; last auto; last first.
        { rewrite -H2 elem_of_map_gset. by exists (s, (v,V)). }
      inversion H12. subst v' V''.
      rewrite [V in F_past _ _ V]H8.

      iApply ("Post" $! v V' with "[$kS' $ofX $oR $oW $Fp oA oAX oAI Hist oBEI]").
      iFrame (H5).
      iExists ζ, h, (s, (v, V)). iFrame (H H0 H1 H2 H3 H4) "oA oAX oAI Hist oBEI".
  Qed.
End Read.