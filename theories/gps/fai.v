From iris.program_logic Require Export weakestpre.
From igps.gps Require Import shared.
From igps.base Require Import at_fai.

Section FAI.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F := (IP true l).
  Notation F_past := (IP false l).

  Lemma RawProto_FAI_non_ex (C: positive)
    s_r s_x π (V V_r V_x: View) q (v_r v_x: Z) γ γ_x (E : coPset)
    (P : vPred) (Q: pr_state Prtcl → Z → vPred)
    (VxVr : V_x !! l ⊑ V_r !! l) (VrV : V_r ⊑ V) (HEN : ↑physN ⊆ E) :
    let mod1C (v: Z) := (v + 1) `mod` Z.pos C in
    {{{ ▷ (∀ v s', ■ (s_r ⊑ s') ∗ F s' v ∗ P
                      ={E}=∗ ∃ s'', ■ (s' ⊑ s'') ∗ Q s'' v
                          ∗ ▷ F s'' (mod1C v) ∗ ▷ F_past s'' (mod1C v))%VP V
        ∗ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ P V
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s_r, (v_r, V_r)]}
        ∗ (∃ ζ, Writer γ ζ) }}}
      (FAI C #l, V) @ E
    {{{ s'' (v: Z) Vr V', RET (#v, V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ⌜s_r ⊑ s''⌝
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ Q s'' v V'
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s'', ((mod1C v), Vr)]} ∗ ⌜Vr ⊑ V'⌝ ∗ ⌜V !! l ⊑ Vr !! l⌝
        ∗ (∃ ζ, Writer γ ζ) }}}.
  Proof.
    iIntros (mod1C Φ) "(VS & ##kI & kS & oI & P & ofX & #oR & oW) Post".

    iDestruct "oI" as (ζ h e_x)
              "(>oA & >oAX & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".
    iDestruct (ExWrite_agree with "[$oAX $ofX]") as %?. subst e_x.
    iDestruct (Reader_Auth_sub with "[$oA $oR]") as %?%elem_of_subseteq_singleton.
    rewrite /Consistent in H2.

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.

    iApply (f_FAI C with "[$kI $kS $Hist]").
    - iPureIntro. exists (VInj v_r), V_r. repeat split; auto.
      rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)).
    - iNext.
      iIntros (v V' h') "(% & kS' & Hist' & % & Pure)".

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist']") as "(Hist' & %)".

      iDestruct "Pure" as (V1 v') "Pure".
      iDestruct "Pure" as %(VrV' & InVr & VVr & Eqv'
                            & InV' & Hh' & HDisj & Hi' & HAdj & HNAdj).

      assert (InVr2 := InVr).
      rewrite /value_at in InVr. rewrite <- H2 in InVr.
      apply elem_of_map_gset in InVr as [[s' [v1 Vr]] [Eq InVr]].
      simpl in Eq. inversion Eq. subst v1 V1. clear Eq.

      assert (HSub: h ⊆ h').
        { rewrite -> Hh'. apply union_subseteq_r. }

      assert (V_r !! l ⊑ Vr !! l).
        { etrans; eauto. }

      assert (InV_r: (VInj v_r, V_r) ∈ h).
        { rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)). }

      assert (s_r ⊑ s').
        { by apply (H0  _ _ H6 InVr). }

      assert (Heb: (s', (v, Vr)) ∈ sEx l (sBE l ζ) (s_x, (v_x, V_x))).
        { rewrite !elem_of_filter. repeat split; last done.
          - etrans; eauto.
          - move => e' Ine' Ne' [t [Eq Eq1]].
            apply HNAdj. exists (VInj $ st_val e', st_view e'). split.
            + rewrite -H2 elem_of_map_gset. by exists e'.
            + exists t. by rewrite -Eq -Eq1. }

      rewrite /BE_Inv (big_op.big_sepS_delete _ _ _ Heb).
      iDestruct "oBEI" as "[F oBEI]".

      iMod ("VS" $! v s' V' with "[%] [$F $P]") as (s'') "(% & Q & F & Fp)";
        [auto|auto|].

      iDestruct "oW" as (ζ') "oW".
      iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.

      iMod (Writer_update _ _ ({[s'', (v', V')]} ∪ ζ)
          with "[$oA $oW]") as "(oA' & oW')".
        { apply union_subseteq_r. }
      iDestruct (Writer_fork_Reader _ _ {[s'', (v', V')]} with "[$oW']")
        as "[oW' #oR']".
        { apply union_subseteq_l. }

      iApply ("Post" $! s'' v V' V'). rewrite /mod1C -Eqv'.
      iFrame (H5) "kS' Q ofX oR'".
      iModIntro. iSplit; [iPureIntro; by etrans; eauto|].
      iSplitR "oW'"; last repeat iSplit; [|auto|auto|by iExists _].

      assert (HC': Consistent ({[s'', (v', V')]} ∪ ζ) h').
      { unshelve eapply (Consistent_proper _ ({[s'', (v', V')]} ∪ ζ) _ _ _ Hh');
          first done.
        by apply Consistent_insert. }

      assert (HV': (VInj v', V') ∈ h').
        { rewrite Hh' elem_of_union elem_of_singleton. by left. }
      assert (HS: SortInv l ({[s'', (v', V')]} ∪ ζ)).
        { move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                      /elem_of_union [/elem_of_singleton ->| In2];
            [done|..|by apply H0].
          - destruct H8 as [OK Disj].
            destruct (OK _ HV') as [t Eqt].
            move => Lt.
            apply (H3 _ t In2) ; rewrite -Eqt; [|auto|]; rewrite /st_time /=.
            + etrans; last by apply H5. etrans; last by apply VrV. done.
            + move => [s0 [v0 V0]] /= In0 Eqt2.
              assert ((VInj v0,V0) ∈ h).
                { rewrite -H2 elem_of_map_gset. by exists (s0, (v0, V0)). }
              assert (InV0: (VInj v0, V0) ∈ h').
                { rewrite Hh' elem_of_union. by right. }
              assert (EqvV := Disj _ _ InV0 HV' Eqt2).
              inversion EqvV. subst. by apply disjoint_singleton_r in HDisj.
          - move => Le.
            etrans; last by apply H11. apply (H0 _ _ In1 InVr).
            destruct HAdj as [t [Ht1 Ht2]].
            move : Le. rewrite /st_time /= Ht1 Ht2.
            assert (Ine1: (VInj $ st_val e1, st_view e1) ∈ h).
              { rewrite -H2 elem_of_map_gset. by exists e1. }
            destruct H8 as [OK Disj].
            destruct (OK _ (HSub _ Ine1)) as [t' Eqt]. rewrite Eqt.
            assert (NEqt: Some t' ≠ Some (t +1)%positive).
              { rewrite -Eqt -Ht2 /= => Eqt2.
                apply HNAdj.
                exists (VInj $ st_val e1, st_view e1).
                split; first exact Ine1.
                exists t. by rewrite Eqt2. }
            cbn. move => /Pos.lt_eq_cases [Le|?]; last by subst t'.
            apply Pos.lt_succ_r. by rewrite -Pos.add_1_r. }


      assert (HI: StateInjection ({[s'', (v', V')]} ∪ ζ)).
        { by apply (StateInjection_insert _ _ _ h). }

      assert (HF: FinalInv l (s_x, (v_x, V_x)) ({[s'', (v', V')]} ∪ ζ)).
      { move => e t /elem_of_union [/elem_of_singleton -> | ?] ? Le NEq.
        - move => e'. etrans; last by apply H11.
          apply (H3 _ t InVr); first auto.
          + destruct HAdj as [t' [Ht1 Ht2]].
            assert (NEqt: Some t ≠ Some (t' +1)%positive).
              { rewrite -Ht2 /= => Eqt2.
                apply (NEq (s'', (v', V'))).
                - rewrite elem_of_union elem_of_singleton. by left.
                - by rewrite Eqt2. }
            move : Le. rewrite /st_time /= Ht1 Ht2.
            cbn. move => /Pos.lt_eq_cases [Le|?]; last by subst t.
            apply Pos.lt_succ_r. by rewrite -Pos.add_1_r.
          + move => e2 In2. apply NEq, elem_of_union. by right.
        - apply (H3 e t); auto.
          move => e' In'. apply (NEq e'), elem_of_union. right.
          by apply elem_of_singleton. }

      iNext. iExists ({[s'', (v', V')]} ∪ ζ), h', (s_x, (v_x, V_x)).
      iFrame (Hi' HC' HI HS HF) "oA' oAX Hist'".
      iSplitL "oBEI oAI F Fp"; last first.
        { iPureIntro. by apply elem_of_union_r. }

      assert (Hs'': (s'', (v', V')) ∉ ζ).
        { move => In.
          apply disjoint_singleton_r in HDisj.
          apply HDisj. rewrite -H2. apply elem_of_map_gset.
          by exists (s'', (v', V')). }

      iSplitL "F oBEI"; last first.
        { rewrite /All_Inv.
          rewrite (big_op.big_sepS_insert _ _ _ Hs''). by iFrame. }

      rewrite /BE_Inv.
      iCombine "F" "oBEI" as "oBEI".
      rewrite -(big_op.big_sepS_insert _ _ (s'', (v', V'))); last first.
        { move => In.
          by apply gset_difference_subseteq,
            subseteq_gset_filter, subseteq_gset_filter in In. }

      set BE':= sEx l (sBE l ({[s'', (v', V')]} ∪ ζ)) (s_x, (v_x, V_x)).
      iApply (big_op.big_sepS_mono _ _ _ BE' with "[$oBEI]");
              last reflexivity.

      set P' := λ e, st_view (s_x, (v_x, V_x)) !! l ⊑ st_view e !! l.

      rewrite <- (gset_filter_singleton (s', (v, Vr)) (P:= P'));
        [| rewrite /P'; etrans; eauto].
      rewrite <- (gset_filter_singleton (s'', (v', V')) (P:= P'));
        [| rewrite /P'; do 2 (etrans; eauto)].
      rewrite -gset_filter_difference -gset_filter_union.
      apply gset_filter_subseteq.

      set fm := λ (e: state_sort), (VInj $ st_val e, st_view e).
      apply (gset_map_subseteq_inj fm).
      { move => ? ? Hin1 Hin2.
        apply HI;
        [abstract set_solver+Hin1 InVr by auto
          |abstract set_solver+Hin2 InVr by auto]. }

      rewrite gset_map_union.
      rewrite - gset_map_difference_inj; last first.
      { move => ? ? Hin1 Hin2.
        apply HI;
        [abstract set_solver+Hin1 InVr by auto
          |abstract set_solver+Hin2 InVr by auto]. }
      rewrite 2!gset_map_singleton.
      do 2!(rewrite -block_ends_fmap; [|done|done]).
      rewrite /Consistent in HC'.

      rewrite H2 HC' Hh'.
      apply gblock_ends_ins_sL_update;[| |done|..].
      * apply elem_of_filter. split; auto.
        move => p ? ? ?. apply HNAdj. by exists p.
      * move => In. apply subseteq_gset_filter in In.
        by apply disjoint_singleton_r in HDisj.
      * apply gblock_ends_ins_update; last done.
        apply (hTime_ok_mono _ _ h'); last done. by rewrite Hh'.
      * apply (hTime_pw_disj_mono _ _ h');
        [by rewrite Hh'|by apply H8].
  Qed.
End FAI.