From iris.proofmode Require Import tactics.
From igps.gps Require Export inst_shared.

Section Plain.
  Context {Σ : gFunctors} {Prtcl : protocolT} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          `{pCount : Countable Param}
          {IP : interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Notation F := (IP true).
  Notation F_past := (IP false).

  Implicit Types
           (l : loc)
           (s : pr_state Prtcl)
           (ζ : gset (state_sort Prtcl))
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View)
           (h : History)
           (e : state_sort Prtcl)
           (p : Param).

  Local Open Scope I.
  Definition gpsPRaw p l n : iProp :=
    ∃ (γ γ_x γx: gname) (e_x: state_sort Prtcl) ζ,
      ⌜n = encode (p,γ,γ_x,γx)⌝ ∗ gps_inv IP l γ γ_x
      ∗ Writer γ ζ ∗ exwr γ_x 1%Qp e_x ∗ own γx (DecAgree e_x).

  Definition RPRaw p s V l n : iProp :=
    ∃ (γ γ_x γx: gname) e_x,
      ⌜n = encode (p,γ,γ_x,γx)⌝ ∗ own γx (DecAgree e_x)
      ∗ ∃ Vr v, vSeen Vr V
              ∗ ⌜st_view e_x !! l ⊑ Vr !! l⌝ ∗ Reader γ {[s, (v, Vr)]} .

  Definition GPS_PP l p s V : iProp :=
    persisted l (gpsPRaw p) (RPRaw p s V).

  Definition vPred_GPS_PP_mono l p s:
    vPred_Mono (GPS_PP l p s).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ γ_x γx e_x) "(? & ? & RA)".
    iDestruct "RA" as (Vr v) "(% & % & ?)".
    iExists X. iFrame. iExists γ, γ_x, γx, e_x. iFrame.
    iExists Vr, v. iFrame. iSplitL ""; last auto. iPureIntro.
    etrans; last exact Mono. done.
  Qed.

  Canonical Structure vGPS_PP l p s :=
    Build_vPred _ (vPred_GPS_PP_mono l p s).

  Instance vGPS_PP_persistent l p s V : PersistentP (vGPS_PP l p s V) := _.

  Local Instance : Frame false (gps_inv IP l γ γ_x) (gpsPRaw p l (encode (p,γ,γ_x,γx)))
                         (Writer γ ζ ∗ exwr γ_x 1%Qp e_x ∗ own γx (DecAgree e_x)).
  Proof.
    intros. iIntros "[? ?]". iExists _, _. iFrame "∗". iExists _, _, _.
    by iFrame "∗".
  Qed.

  Close Scope I.

  Lemma GPS_PPs_agree l p s1 s2 V (E : coPset) (HE: ↑persist_locN .@ l ⊆ E):
    vGPS_PP l p s1 V ∗ vGPS_PP l p s2 V ={E}=∗ ⌜s1 ⊑ s2 ∨ s2 ⊑ s1⌝.
  Proof.
    iIntros "RP". rewrite persistor_splitjoin.
    iMod (persistor_open with "RP") as (X) "(oI & (oR1 & oR2) & HClose)";
      first auto.
    iDestruct "oR1" as (γ1 γ_x1 γx1 e_x1) "(% & #oD1 & #RA1)".
    iDestruct "oR2" as (γ2 γ_x2 γx2 e_x2) "(% & #oD2 & #RA2)".
    iDestruct "oI" as (γ γ_x γx e_x ζ) "(>% & oI & oI2)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.
    iDestruct "RA1" as (V1 v1) "(VV1 & Le1 & oR1)".
    iDestruct "RA2" as (V2 v2) "(VV2 & Le2 & oR2)".
    iMod (Reader_singleton_agree _ _ _ _ s1 _ _ s2 with "[$oI $oR1 $oR2]") as "[oI %]".
    iMod ("HClose" $! (λ l0 X, RPRaw p s1 V l0 X ∗ RPRaw p s2 V l0 X)%I
          with "[oI oI2]") as "oR"; last done.
    iSplitL "oI oI2"; last iSplitL ""; iExists γ, γ_x, γx.
    - iExists e_x, ζ. iFrame "oI oI2". by iNext.
    - iExists e_x1. iFrame "oD1". iSplitL; first auto.
      iExists V1, v1. iFrame "VV1 Le1 oR1".
    - iExists e_x2. iFrame "oD2". iSplitL; first auto.
      iExists V2, v2. iFrame "VV2 Le2 oR2".
  Qed.

  Lemma GPS_PP_Init l p s v (E : coPset)
    (HEN : ↑physN ⊆ E) (HNp : ↑persistN ⊆ E):
  {{{{ ☐ PersistorCtx ∗ own_loc l ∗ ▷ F l s v ∗ ▷ F_past l s v }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ RET #() ; vGPS_PP l p s }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (#kIp & oL & F & Fp) Post".
    iDestruct "oL" as (h i)  "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (RawProto_init IP l s v _ _
               with "[$kI $kS $Hist $Alloc $F $Fp]"); first auto. 

    iNext.
    iIntros (V' γ γ_x) "(#Le & kS' & oI & ex & #oR & oW)".

    iMod (own_alloc (DecAgree (s, (v, V')))) as (γx) "#oEx"; first done.

    iMod (persistor_inv_alloc E i (encode (p,γ,γ_x,γx)) l
          (gpsPRaw p) (RPRaw p s V')
          with "[-Post $kIp kS']") as "PP"; [auto|auto|..].
      { iFrame "kI Info oI oW ex oEx".
        repeat iExists _. iFrame "oEx". iSplitL; [done|].
        iExists _, _; iFrame "oR". by iSplit. }
    iApply ("Post" $! V' with "Le kS' [$PP]").
  Qed.

  Lemma GPS_PP_Read l p (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
  {{{{ ▷(∀ s', ■ (s ⊑ s') → 
              (∀ v, F_past l s' v ∗ P ={E ∖ ↑persist_locN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F l s'' v ∗ P
                            ={E ∖ ↑persist_locN .@ l}=∗ False))
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v
                    ={E ∖ ↑persist_locN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
      ∗ P ∗ vGPS_PP l p s }}}}
    [ #l ]_at @ E
  {{{{ s' v, RET #v ; ■ (s ⊑ s') ∗ vGPS_PP l p s' ∗ Q s' v }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod (persistor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first auto.
    iDestruct "RRaw" as (γ γ_x γx e_x) "(% & #Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' γx' e_x' ζ)
                          "(>% & oI & Writer & >Ex & >#Hex')".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x' γx'.

    iCombine "Hex" "Hex'" as "He".
    iDestruct (own_valid with "He") as %He%dec_agree_op_inv.
    inversion He. subst e_x'.

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_Read IP l P Q s _ _ V_r v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply "HClose". iSplitL "oI Writer Ex".
    + iNext. iExists γ, γ_x, γx, e_x, ζ. by iFrame "oI Writer Ex Hex".
    + iExists γ, γ_x, γx, e_x. iFrame "Hex". iSplitL ""; first auto.
      iExists Vr, v. iFrame (H2) "oR'".
      iPureIntro. do 3 (etrans; eauto).
  Qed.

  Lemma GPS_PP_Write l p s s' v (E : coPset)
      (P: vPred) (Q : pr_state Prtcl → vPred) 
      (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E)
      (HFinal: final_st s'):
  {{{{ ▷ (P ∗ vGPS_PP l p s' ={E ∖ ↑persist_locN .@ l}=∗
              Q s' ∗ ▷ (F l s' v) ∗ ▷ F_past l s' v)
      ∗ ▷ P ∗ vGPS_PP l p s }}}}
    ([ #l ]_at <- #v) @ E
  {{{{ RET #() ;  vGPS_PP l p s' ∗ Q s' }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod ((persistor_open') with "oW")
      as (X) "(gpsRaw & RRaw & #HT & HClose)"; [done|].
    iDestruct "RRaw" as (γ γ_x γx e_x) "(% & #Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' γx' e_x' ζ)
                          "(>% & oI & >wTok & >ex & >#Hex')".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x' γx'.

    iCombine "Hex" "Hex'" as "He".
    iDestruct (own_valid with "He") as %He%dec_agree_op_inv.
    inversion He. subst e_x'. destruct e_x as [s_x [v_x V_x]].

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_Write_non_ex IP l 
              s s_x s' _ _ V_r V_x 1%Qp v v_r v_x γ γ_x _ ζ
            with "[$oI $kI $kS $ex $oR $wTok]"); [solve_ndisj|auto|auto|auto|].
    iNext.
    iIntros (V') "(% & kS' & oI & ex & #oR' & wTok)".
    rewrite H2.
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    iAssert (vGPS_PP l p s' V') with "[#]" as "#oPP".
    { iApply "HT".
      iExists γ, γ_x, γx, (s_x, (v_x, V_x)).
      repeat iSplit; [auto|auto|].
      iExists V', v. repeat iSplit; [auto| |auto].
      iPureIntro. by rewrite H1 (H _). }
    iMod ("VS" with "[%] [$oP $oPP]") as "($ & oF & oFp)"; [done|].
    iSpecialize ("oI" with "[$oF $oFp]").
    iFrame "oPP". iApply "HClose".
    iNext. iExists γ, γ_x, γx, (s_x, (v_x, V_x)), ({[s', (v, V')]} ∪ ζ).
    by iFrame "oI wTok ex Hex".
  Qed.

  Lemma GPS_PP_CAS l p s v_o v_n (E : coPset)
    (P : vPred) (Q: pr_state Prtcl → vPred) (R : pr_state Prtcl → Z → vPred)
    (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E) :
  {{{{  ▷ (∀ s', ■ (s ⊑ s') ∗ F l s' v_o ∗ P
                      ={E ∖ ↑persist_locN .@ l}=∗ ∃ s'', ■ (s' ⊑ s'')
                        ∗ Q s'' ∗ ▷ (F l s'' v_n) ∗ ▷ F_past l s'' v_n)
      ∗ ▷(∀ s' v, ■ (s ⊑ s') ∗ ■ (v ≠ v_o) ∗ ▷ F_past l s' v ∗ P
                      ={E ∖ ↑persist_locN .@ l}=∗ R s' v)
      ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v 
                  ={E ∖ ↑persist_locN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
      ∗ P ∗ vGPS_PP l p s }}}}
    (CAS #l #v_o #v_n) @ E
  {{{{ s'' (b: bool) v, RET #b ;
      ■ (s ⊑ s'') ∗ vGPS_PP l p s'' ∗ (if b then Q s'' else R s'' v )}}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSF & VSDup & oP & oW) Post".
    iMod ((persistor_open) with "[$oW]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.
    iDestruct "RRaw" as (γ γ_x γx e_x) "(% & #Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' γx' e_x' ζ)
                          "(>% & oI & >wTok & >ex & >#Hex')".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x' γx'.

    iCombine "Hex" "Hex'" as "He".
    iDestruct (own_valid with "He") as %He%dec_agree_op_inv.
    inversion He. subst e_x'. destruct e_x as [s_x [v_x V_x]].

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_CAS_non_ex_weak IP l
              s s_x _ _ V_r V_x 1%Qp v_o v_n v_r v_x γ γ_x _ P Q R
            with "[$VS $VSF $kI $kS $oI $oP $ex $oR wTok VSDup]");
      [solve_ndisj|solve_ndisj|solve_ndisj|iSplitL "VSDup"; last by iExists _|].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s'' b v Vr V')
            "(#HV' & kS' & Hs' & oI & IF & ex & #oR' & % & % & wTok)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' IF".
    iApply "HClose". iSplitL "oI ex wTok".
    + iNext. iDestruct "wTok" as (ζ') "wTok".
      iExists γ, γ_x, γx, (s_x, (v_x, V_x)), _.
      by iFrame "oI wTok ex Hex".
    + iExists γ, γ_x, γx, (s_x, (v_x, V_x)).
      repeat iSplit; [auto|auto|].
      iExists Vr, v. repeat iSplit; [auto| |auto].
      iPureIntro. do 2 (etrans; eauto).
  Qed.


  Lemma GPS_PP_FAI C l p s (E : coPset)
    (P : vPred) (Q: pr_state Prtcl → Z → vPred)
    (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E) :
  let mod1C (v: Z) := ((v + 1) `mod` Z.pos C)%Z in
  {{{{ ▷ (∀ v s', ■ (s ⊑ s') ∗ F l s' v ∗ P
                      ={E ∖ ↑persist_locN .@ l}=∗ ∃ s'', ■ (s' ⊑ s'')
                        ∗ Q s'' v
                        ∗ ▷ (F l s'' (mod1C v)) ∗ ▷ F_past l s'' (mod1C v))
      ∗ P ∗ vGPS_PP l p s }}}}
    (FAI C #l) @ E
  {{{{ s'' (v: Z), RET #v ;
      ■ (s ⊑ s'') ∗ vGPS_PP l p s'' ∗ Q s'' v }}}}%C.
  Proof.
    iIntros (mod1C Φ). intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod ((persistor_open) with "[$oW]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.
    iDestruct "RRaw" as (γ γ_x γx e_x) "(% & #Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' γx' e_x' ζ)
                          "(>% & oI & >wTok & >ex & >#Hex')".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x' γx'.

    iCombine "Hex" "Hex'" as "He".
    iDestruct (own_valid with "He") as %He%dec_agree_op_inv.
    inversion He. subst e_x'. destruct e_x as [s_x [v_x V_x]].

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_FAI_non_ex IP _ _
              s s_x _ _ V_r V_x 1%Qp v_r v_x γ γ_x _ P Q
            with "[$VS $kI $kS $oI $oP $ex $oR wTok]");
      [auto|auto|solve_ndisj|by iExists _|].
    iNext.
    iIntros (s'' v Vr V')
            "(#HV' & kS' & Hs' & oI & Q & ex & #oR' & % & % & wTok)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' Q".
    iApply "HClose". iSplitL "oI ex wTok".
    + iNext. iDestruct "wTok" as (ζ') "wTok".
      iExists γ, γ_x, γx, (s_x, (v_x, V_x)), _.
      by iFrame "oI wTok ex Hex".
    + iExists γ, γ_x, γx, (s_x, (v_x, V_x)).
      repeat iSplit; [auto|auto|].
      iExists Vr, (mod1C v). repeat iSplit; [auto| |auto].
      iPureIntro. do 2 (etrans; eauto).
  Qed.

End Plain.

Section Plain_Default.
  Context {Σ : gFunctors} {Prtcl : protocolT}
          `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          {IP : interpC Σ Prtcl}.
  Set Default Proof Using "Type".

  Notation F := (IP true).
  Notation F_past := (IP false).

  Lemma GPS_PP_Init_default l s v (E : coPset)
    (HEN : ↑physN ⊆ E) (HNp : ↑persistN ⊆ E):
  {{{{ ☐ PersistorCtx ∗ own_loc l ∗ ▷ F l s v ∗ ▷ F_past l s v }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ RET #() ; vGPS_PP (IP := IP) l () s }}}}%C.
  Proof. by apply GPS_PP_Init. Qed.

End Plain_Default.

Lemma GPS_PPs_param_agree
  {Σ : gFunctors} {Prtcl : protocolT}
  `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
  `{persG : persistorG Σ}
  `{agreeG : gps_agreeG Σ Prtcl}
  `{pCount : Countable Param}
  {IP1 IP2 : interpC Σ Prtcl} l p1 p2 s1 s2 V:
  vGPS_PP (IP:=IP1) l p1 s1 V ∗ vGPS_PP (IP:= IP2) l p2 s2 V ⊢ ⌜p1 = p2⌝.
Proof.
  iIntros "RP". rewrite persistor_join_l persistor_drop.
  iDestruct "RP" as (X) "[oR1 oR2]".
  iDestruct "oR1" as (γ1 γ_x1 γx1 e_x1) "(% & _)".
  iDestruct "oR2" as (γ2 γ_x2 γx2 e_x2) "(% & _)".
  rewrite H0 in H. apply encode_inj in H. by inversion H.
Qed.

Arguments vGPS_PP [_ _ _ _ _ _ _ _ _ _] _ _ _.
Arguments GPS_PP_Init [_ _ _ _ _ _ _ _ _ _] _ [_ _] _ [_ _] _ _ [_ _ _].
Arguments GPS_PP_Read [_ _ _ _ _ _ _ _ _ _] _ [_ _] _ _ [_ _] _ _ [_ _ _].
Arguments GPS_PP_Write [_ _ _ _ _ _ _ _ _ _] _ [_ _ _] _ [_ _] _ _ _ _ _ [_ _ _].
Arguments GPS_PP_FAI [_ _ _ _ _ _ _ _ _ _] _ [_ _ _ _ _] _ _ _ _ [_ _ _].
Arguments GPS_PP_CAS [_ _ _ _ _ _ _ _ _ _] _ [_ _ _ _ _ _] _ _ _ _ _ [_ _ _].
Arguments GPS_PPs_agree [_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _].
Arguments GPS_PPs_param_agree [_ _ _ _ _ _ _ _ _ _ _ _ _].
Arguments GPS_PP_Init_default [_ _ _ _ _ _ _] _ [_ ] _ [_ _] _ _ [_ _ _].

Notation "'[PP' l 'in' s | F ]" :=
  (vGPS_PP F l () s)
    (format "[PP  l  in  s  |  F  ]").

Notation "'[PP' l 'in' s '@' p | F ]" :=
  (vGPS_PP F l p s)
    (format "[PP  l  in  s  @  p  |  F  ]").
