From iris.proofmode Require Import tactics.
From igps.gps Require Export inst_shared.
From igps Require Import bigop abs_view.


Section Gname_StrongSW.
  Context {Σ : gFunctors} {Prtcl : protocolT} 
          `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{CInvG : cinvG Σ}
          {IP : gname → interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Notation F γ := (IP γ true).
  Notation F_past γ := (IP γ false).

  Definition WAEx l γ γ_x s : vPred
    := ∃ v V_0 ζ, vSeen V_0 
        ∧ ☐ (Writer γ ζ ∗ exwr γ_x 1%Qp (s, (v, V_0))
                        ∗ Reader γ {[s, (v, V_0)]}
                        ∗ ⌜maxTime l V_0 ζ⌝).
  Arguments WAEx _ _ _ / _.

  Implicit Types
      (l : loc)
      (s : pr_state Prtcl)
      (ζ : gset (state_sort Prtcl))
      (S : pr_state Prtcl * (Z * View))
      (v : Z)
      (V : View)
      (h : History).

  Local Open Scope I.
  Definition gpsSWnRaw γ l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (γ,γ_x)⌝ ∗ gps_inv (IP γ) l γ γ_x.

  Definition ReaderSWnRaw γ s V l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (γ,γ_x)⌝ ∗ PrtSeen γ s V.

  Definition WriterSWnRaw γ s V l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (γ,γ_x)⌝ ∗ WAEx l γ γ_x s V.

  Definition GPS_nFRP γ l s (q: frac) V : iProp :=
    fractored l (gpsSWnRaw γ) (λ l q, ReaderSWnRaw γ s V l) q.

  Definition GPS_nFWP γ l s (q: frac) V : iProp :=
    fractored l (gpsSWnRaw γ) (λ l q, WriterSWnRaw γ s V l) q.

  Close Scope I.

  Definition vPred_GPS_nFRP_mono γ l s q:
    vPred_Mono (GPS_nFRP γ l s q).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ_x) "(? & RA)".
    iExists X. iFrame. iExists γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_nFRP γ l s q :=
    Build_vPred _ (vPred_GPS_nFRP_mono γ l s q).

  Definition vPred_GPS_nFWP_mono γ l s q:
    vPred_Mono (GPS_nFWP γ l s q).
  Proof.
    intros V V' Mono.
    iIntros "WP". iDestruct "WP" as (X) "(WPV & ?)".
    iDestruct "WPV" as (γ_x) "(? & WA)".
    iExists X. iFrame. iExists γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_nFWP γ l s q :=
    Build_vPred _ (vPred_GPS_nFWP_mono γ l s q).

  Local Instance :
    Frame false (gps_inv (IP γ) l γ γ_x)
          (gpsSWnRaw γ l (encode (γ, (γ_x)))) True.
  Proof. intros. iIntros "[? _]". iExists _. by iFrame "∗". Qed.

  Lemma GPS_nFWP_Init_strong l (s: gname -> pr_state Prtcl) v 
    (Q : gname -> vPred) (E : coPset) (HEN : ↑physN ⊆ E):
  {{{{ own_loc l ∗
        (∀ γ,
   |={E ∖ ↑persist_locN.@l}=> ▷ F γ l (s γ) v ∗ ▷ F_past γ l (s γ) v ∗ Q γ) }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ γ, RET #() ; Q γ ∗ vGPS_nFWP γ l (s γ) 1%Qp }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (oL & F) Post".
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (RawProto_init_strong IP l s
               with "[$kI $kS $Hist $Alloc]"); first auto.
    iNext.
    iIntros (V' γ γ_x) "(% & kS' & oI & ex & #oR & oW)".

    iSpecialize ("F" $! γ).
    iMod (fupd_mask_mono with "F") as "(F & Fp & Q)"; first solve_ndisj.

    iMod (fractor_alloc E i (encode(γ,γ_x)) l
                        (gpsSWnRaw γ) (λ l q, WriterSWnRaw γ (s γ) V' l)
           with "[-Post kS' Q]") as "FP"; first auto.
      { iFrame "kI Info". iSplitL "F Fp oI"; iExists γ_x.
        - iSplitL ""; [by iNext|]. iApply "oI". by iFrame "F Fp".
        - iSplitL ""; first done.
          iExists v, V', {[(s γ), (v, V')]}. iFrame "ex oW oR".
          iSplit; first auto. iPureIntro.
          by move => ? /elem_of_singleton ->. }
    by iApply ("Post" $! V' with "[%] kS' [$Q $FP]").
  Qed.

  Lemma GPS_nFWP_ExRead γ l s q (E : coPset)
      (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ ▷ ɐ (∀ v, F_past γ l s v ={E ∖ ↑fracN .@ l}=∗
                   F_past γ l s v ∗ F_past γ l s v ) ∗ vGPS_nFWP γ l s q }}}}
    ([ #l ]_at) @ E
  {{{{ v, RET #v ;
      vGPS_nFWP γ l s q ∗ F_past γ l s v }}}}.
  Proof.
    iIntros (V_l π Φ).
    iViewUp; iIntros "#kI kS (VSDup & oW) Post".
    iMod (fractor_open with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & oR & %)".

    iApply (RawProto_Read_ex (IP γ)
            with "[$kI $kS $oI $ex $oR $wTok VSDup]"); [solve_ndisj|done|done|..].
    { iNext. by iSpecialize ("VSDup" $! _ with ""). }
    iNext.
    iIntros (v V') "(#HV' & kS & oI & Fp & ex & oR' & wTok)".
    iApply ("Post" $! V' with "HV' kS"). iFrame "Fp".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ_x. by iFrame.
    + iExists γ_x. iSplitL ""; first auto.
      iExists v_x, V_x,_. iFrame "wTok ex oR'". iSplit; last auto.
      iDestruct "HV'" as "%".
      iPureIntro. etrans; eauto.
  Qed.

  Lemma GPS_nFWP_Writer_Reader γ l s q1 q2:
    vGPS_nFWP γ l s (q1 + q2) ⊧ vGPS_nFWP γ l s q1 ∗ vGPS_nFRP γ l s q2.
  Proof.
    constructor => V.
    iViewUp; iIntros "oW".
    iApply fractor_splitjoin.
    iApply (fractor_mono with "[$oW]").
    iIntros (X) "oW".
    iDestruct "oW" as (γ_x) "[#? WA]".
    iDestruct "WA" as (v V0 ζ) "(#? & ? & ? & #oR & #?)".
    iSplitR ""; iExists γ_x; (iSplitL ""; first auto).
    - iExists v, V0, ζ. iFrame. iSplit; [auto|by iSplitL ""].
    - iExists V0, v. by iFrame "oR".
  Qed.

  Lemma GPS_nFWP_Writer_Reader_join γ l s q1 q2:
    vGPS_nFWP γ l s q1 ∗ vGPS_nFRP γ l s q2 ⊧ vGPS_nFWP γ l s (q1 + q2).
  Proof.
    constructor => V.
    iViewUp; iIntros "(o1 & o2)". iCombine "o1" "o2" as "oW".
    rewrite fractor_splitjoin; last auto.
    iApply (fractor_mono with "[$oW]").
    by iIntros (X) "(oW & _)".
  Qed.

  Lemma GPS_nFRP_gname_agree γ1 γ2 l s1 s2 q1 q2 V:
    vGPS_nFRP γ1 l s1 q1 V ∗ vGPS_nFRP γ2 l s2 q2 V ⊢ ⌜γ1 = γ2⌝.
  Proof.
    rewrite fractor_join_l. iIntros "oR".
    iDestruct (fractor_drop with "oR") as (X) "[oR1 oR2]".
    iDestruct "oR1" as (γ_x1) "[% _]".
    iDestruct "oR2" as (γ_x2) "[% _]".
    subst X. apply encode_inj in H0. by inversion H0.
  Qed.

  Lemma GPS_nFWP_gname_agree γ1 γ2 l s1 s2 q1 q2 V:
    vGPS_nFWP γ1 l s1 q1 V ∗ vGPS_nFRP γ2 l s2 q2 V ⊢ ⌜γ1 = γ2⌝.
  Proof.
    rewrite fractor_join_l. iIntros "oW".
    iDestruct (fractor_drop with "oW") as (X) "[oW oR]".
    iDestruct "oW" as (γ_x1) "[% _]".
    iDestruct "oR" as (γ_x2) "[% _]".
    subst X. apply encode_inj in H0. by inversion H0.
  Qed.

  Lemma GPS_nFRP_join_l γ l s1 s2 q1 q2:
    vGPS_nFRP γ l s1 q1 ∗ vGPS_nFRP γ l s2 q2 ⊧ vGPS_nFRP γ l s1 (q1 + q2).
  Proof.
    constructor => V. iViewUp.
    iIntros "[oR1 oR2]". iCombine "oR1" "oR2" as "oR".
    rewrite fractor_join_l.
    iApply (fractor_mono with "[$oR]").
    by iIntros (X) "[R1 R2]".
  Qed.

  Lemma GPS_nFWP_join_writer γ l s1 s2 q1 q2 V:
    vGPS_nFWP γ l s1 q1 V ∗ vGPS_nFRP γ l s2 q2 V ⊢ vGPS_nFWP γ l s1 (q1 + q2) V.
  Proof.
    iIntros "[oW oR]". iCombine "oW" "oR" as "oW".
    rewrite fractor_join_l. iApply (fractor_mono with "[$oW]").
    by iIntros (X) "[R1 R2]".
  Qed.

  Lemma GPS_nFWP_writer_exclusive γ1 γ2 l s1 s2 q1 q2 V:
    vGPS_nFWP γ1 l s1 q1 V ∗ vGPS_nFWP γ2 l s2 q2 V ⊢ False.
  Proof.
    rewrite fractor_join_l. iIntros "oW".
    iDestruct (fractor_drop with "oW") as (X) "[oW1 oW2]".
    iDestruct "oW1" as (γ_x1) "[% oW1]".
    iDestruct "oW2" as (γ_x2) "[% oW2]".
    subst X. apply encode_inj in H0. inversion H0.
    iDestruct "oW1" as (???) "(_ & oW1 & _)".
    iDestruct "oW2" as (???) "(_ & oW2 & _)".
    iApply (Writer_Exclusive with "[$oW1 $oW2]").
  Qed.

  Lemma GPS_nFWP_join_writer_2 γ1 γ2 l s1 s2 q1 q2 V:
    vGPS_nFWP γ1 l s1 q1 V ∗ vGPS_nFRP γ2 l s2 q2 V ⊢ vGPS_nFWP γ1 l s1 (q1 + q2) V.
  Proof.
    iIntros "oW". iDestruct (GPS_nFWP_gname_agree with "oW") as %?.
    subst γ2. iApply (GPS_nFWP_join_writer with "oW").
  Qed.
  Global Instance GPS_nFRP_fractional γ l s V:
    Fractional (λ q, vGPS_nFRP γ l s q V).
  Proof.
    intros q1 q2. iSplit.
    - iIntros "oR". iApply fractor_splitjoin.
      iApply (fractor_mono with "[$oR]").
      iIntros (?) "#?". by iSplit.
    - iIntros "(oR1 & oR2)".
      iCombine "oR1" "oR2" as "oR".
      rewrite fractor_splitjoin; last auto.
      iApply (fractor_mono with "[$oR]"). by iIntros (X) "[R1 R2]".
  Qed.

  Global Instance GPS_nFRP_exists_fractional l γ V:
    Fractional (λ q, (∃ s, vGPS_nFRP γ l s q)%VP V).
  Proof.
    intros q1 q2. iSplit.
    - iIntros "oR". iDestruct "oR" as (s) "oR".
      rewrite GPS_nFRP_fractional. iDestruct "oR" as "[oR1 oR2]".
      iSplitL "oR1"; by iExists _.
    - iIntros "(oR1 & oR2)".
      iDestruct "oR1" as (s1) "oR1".
      iDestruct "oR2" as (s2) "oR2".
      destruct (GPS_nFRP_join_l γ l s1 s2 q1 q2) as [HJ].
      iDestruct (HJ with "[$oR1 $oR2]") as "oR"; first reflexivity.
      by iExists _.
  Qed.

  Lemma GPS_nFWP_dealloc γ l s V (E: coPset) (HNl: ↑fracN .@ l ⊆ E):
    vGPS_nFWP γ l s 1%Qp V ={E}=∗ own_loc l V.
  Proof.
    iIntros "FP".
    iMod (fractor_dealloc with "FP") as (n i) "(Info & WR & Inv)"; first auto.
    iDestruct "WR" as (γ_x) "(% & WA)".
    iDestruct "Inv" as (γ_x') "(>% & oI)".
    subst n. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "oI" as (ζ' h e') "(>oA & _ & >Hist & ? & ? & ? & ? & >% & ?)".

    iDestruct "WA" as (v Vr ζ) "(% & _ & _ & RA & _)".
    iDestruct (Reader_Auth_sub with "[$oA $RA]")
      as %Inζ%elem_of_subseteq_singleton.

    iExists h, i. iFrame "Hist Info". iPureIntro.
    exists (VInj v), Vr. repeat split; [|auto|auto].
    rewrite -H elem_of_map_gset. by exists (s, (v, Vr)).
  Qed.

  Lemma GPS_nFRP_exists_mult_joinL l (n: positive) q γ V:
       ([∗ list] _ ∈ seq 0 (Pos.to_nat n), ∃ s, vGPS_nFRP γ l s q)%VP V
    ⊢ ∃ s, vGPS_nFRP γ l s (Pos2Qp n * q) V.
  Proof.
    rewrite -vPred_big_opL_fold
            -(big_sepL_mult_fractional
                (Φ := (λ q, (∃ s, vGPS_nFRP γ l s q)%VP V))); last lia.
    by rewrite (Pos2Nat.id n).
  Qed.

  Lemma GPS_nFRP_join γ l s q1 q2:
    vGPS_nFRP γ l s q1 ∗ vGPS_nFRP γ l s q2 ⊧ vGPS_nFRP γ l s (q1 + q2).
  Proof.
    constructor => V. iViewUp.
    change (vGPS_nFRP γ l s (q1 + q2) V)
      with ((λ q, vGPS_nFRP γ l s q V) (q1 + q2)%Qp).
    by rewrite fractional.
  Qed.

  Lemma GPS_nFRP_split γ l s q1 q2:
     vGPS_nFRP γ l s (q1 + q2) ⊧ vGPS_nFRP γ l s q1 ∗ vGPS_nFRP γ l s q2.
  Proof.
    constructor => V. iViewUp.
    change (vGPS_nFRP _ _ _ _ _) with ((λ q, vGPS_nFRP γ l s q V) (q1 + q2)%Qp).
    by rewrite fractional.
  Qed.

  Lemma GPS_nFRP_mult_splitL γ l s (n: positive) q:
    vGPS_nFRP γ l s (Pos2Qp n * q) 
      ⊧ [∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_nFRP γ l s q.
  Proof.
    constructor => V. iViewUp.
    rewrite -{1}(Pos2Nat.id n).
    change (vGPS_nFRP _ _ _ _ _)
      with ((λ q, vGPS_nFRP γ l s q V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma GPS_nFRP_mult_joinL γ l s (n: positive) q:
    ([∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_nFRP γ l s q)
      ⊧ vGPS_nFRP γ l s (Pos2Qp n * q).
  Proof.
    constructor => V. iViewUp.
    rewrite -{2}(Pos2Nat.id n).
    change (vGPS_nFRP _ _ _ _ _)
      with ((λ q, vGPS_nFRP γ l s q V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma GPS_nFWP_mult_splitL_1 γ l s n:
    vGPS_nFWP γ l s 1
    ⊧ vGPS_nFWP γ l s (1/(n+1))
      ∗ [∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_nFRP γ l s (1/(n+1)).
  Proof.
    rewrite {1}(Pos2Qp_plus_mult n).
    constructor => V. iViewUp. iIntros "FWP".
    destruct (GPS_nFWP_Writer_Reader γ l s (1 / (n + 1)) (Pos2Qp n * (1 / (n + 1))))
     as [Split].
    iDestruct (Split with "FWP") as "[$ FRP]"; first done.
    destruct (GPS_nFRP_mult_splitL γ l s n (1 / (n + 1))) as [Split2].
    by iApply Split2.
  Qed.

  Lemma GPS_nFWP_ExWrite γ l s s' q v (E : coPset)
      (P: vPred) (Q : pr_state Prtcl → vPred)
      (Le: s ⊑ s') (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ ▷ (∀ v', P ∗ ▷ (F γ l s v')
            ={E ∖ ↑fracN .@ l}=∗ Q s' ∗ ▷ (F γ l s' v) ∗ ▷ F_past γ l s' v)
      ∗ P ∗ vGPS_nFWP γ l s q }}}}
    ([ #l ]_at <- #v) @ E
  {{{{ RET #() ; vGPS_nFWP γ l s' q ∗ Q s' }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod (fractor_open with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Write_ex (IP γ) l s s'
            with "[$oI $kI $kS $ex $oR $wTok]");
      [solve_ndisj|auto|auto|auto|..].
    iNext.
    iIntros (V') "(% & kS' & oI & oF & ex & #oR' & wTok & #Max)".
    rewrite H2.
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    iMod ("VS" with "[%] [$oP $oF]") as "(oQ & oF & oFp)"; [done|..].
    iFrame "oQ". iApply ("HClose"). iSplitL "oI oF oFp".
    - iExists γ_x. iSplitL ""; first auto.
      iApply ("oI" with "[$oF $oFp]").
    - iExists γ_x. iSplitL ""; first auto.
      iExists v, V', _. by iFrame "wTok ex oR' Max".
  Qed.

  Lemma GPS_nFRP_PrtSeen γ l s q:
    vGPS_nFRP γ l s q ⊧ PrtSeen γ s.
  Proof.
    constructor => V.
    iViewUp. iIntros "oR". iDestruct "oR" as (X) "[oR ?]".
    iDestruct "oR" as (γ_x) "[? $]".
  Qed.

  Lemma GPS_nFWP_PrtSeen γ l s q:
    vGPS_nFWP γ l s q ⊧ PrtSeen γ s.
  Proof.
    constructor => V.
    iViewUp. iIntros "oR". iDestruct "oR" as (X) "[oR ?]".
    iDestruct "oR" as (γ_x) "[? WA]".
    iDestruct "WA" as (v V_0 ζ) "(vS & oW & _ & oW3 & oW4)".
    iExists V_0, v. iFrame "vS oW3".
  Qed.

  Lemma GPS_nFRP_PrtSeen_update γ l q s1 s2 (Le: s1 ⊑ s2):
    vGPS_nFRP γ l s1 q ∗ PrtSeen γ s2 ⊧ vGPS_nFRP γ l s2 q.
  Proof.
    constructor => V.
    iViewUp. iIntros "[oR oS]". iDestruct "oR" as (X) "[oR Inv]".
    iDestruct "oR" as (γ_x) "[oX oS1]".
    iExists X. iFrame "Inv".
    iExists γ_x. iFrame "oX oS".
  Qed.

  Lemma GPS_nFRP_Read l γ q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ (▷ ∀ s', ■ (s ⊑ s') → 
              (∀ v, F_past γ l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F γ l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
         ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past γ l s' v
            ={E ∖ ↑fracN .@ l}=∗ F_past γ l s' v ∗ F_past γ l s' v )
      ∗ P ∗ vGPS_nFRP γ l s q  }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s ⊑ s') ∗ vGPS_nFRP γ l s' q ∗ Q s' v }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ_x) "(% & RA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "RA" as (V_r v_r) "(% & #oR)".

    iApply (RawProto_Read (IP γ) l P Q s _ _ V_r v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ_x. by iFrame "oI".
    + iExists γ_x. iSplit; first done. iExists Vr, v. by iFrame "oR'".
  Qed.

  Lemma GPS_nFRP_Read_PrtSeen_abs `{aG: absViewG Σ}
    l γ q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s1 s2 β (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ (▷ ∀ s', ■ (s1 ⊑ s' ∧ s2 ⊑ s') → 
              (∀ v, F_past γ l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F γ l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
         ∗ ▷ ɐ (∀ s' v, ■ (s1 ⊑ s' ∧ s2 ⊑ s') ∗ F_past γ l s' v
            ={E ∖ ↑fracN .@ l}=∗ F_past γ l s' v ∗ F_past γ l s' v )
      ∗ P ∗ aSeen β ∗ ⌞vGPS_nFRP γ l s1 q⌟ β ∗ PrtSeen γ s2  }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s1 ⊑ s' ∧ s2 ⊑ s') ∗ ⌞vGPS_nFRP γ l s1 q⌟ β ∗ Q s' v }}}}%C.
  Proof.
    intros. iViewUp. iIntros "#kI kS (VS & VSDup & oP & #aS & oR & oS2) Post".

    iDestruct "oR" as (Vβ) "[#aV oR]".
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ_x) "(% & oS1)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'. clear H0.

    iDestruct "oS1" as (V1 v1) "(% & #oR1)".
    iDestruct "oS2" as (V2 v2) "(% & #oR2)".

    iMod (Reader_singleton_agree _ _ _ _ s1 _ _ s2 with "[$oI $oR1 $oR2]") as "[oI %]".

    destruct H1 as [Hs2|Hs1].
    - iApply (RawProto_Read (IP γ) l P Q s2 _ _ V2 v2 γ γ_x
            with "[$oI $kI $kS $oP $oR2 VS VSDup]"); [solve_ndisj|auto|..].
      { iNext. iSplitL "VS".
        - iIntros (s'). iViewUp. iIntros (Hs).
          iApply ("VS" $! s' with "[%] [%]"); [done|].
          split; [by etrans|auto].
        - iIntros (s' v). iViewUp.
          iIntros "[% Fp]".
          iApply ("VSDup" $! _ s' v with "[%] [$Fp]"); first reflexivity.
          iPureIntro. split; [by etrans|auto]. }
      iNext. iIntros (s' v Vr V') "(#HV' & kS & % & oI & Q & oR' & % & %)".
      iApply ("Post" $! V' with "HV' kS").
      assert (Hs': s1 ⊑ s' ∧ s2 ⊑ s'). { split; [by etrans|auto]. }
      iFrame (Hs') "Q".
      iExists Vβ. iFrame "aV".
      iApply "HClose". iSplitL "oI".
      + iNext. iExists γ_x. by iFrame "oI".
      + iExists γ_x. iSplit; first done. iExists V1, v1. by iFrame "oR1".
    - iPoseProof "aS" as "aS1".
      iDestruct "aS1" as (Vβ') "[% aS1]".
      iDestruct (absView_agree with "aV aS1") as %?. subst Vβ'.
      iApply (RawProto_Read (IP γ) l P Q s1 _ _ V1 v1 γ γ_x
            with "[$oI $kI $kS $oP $oR1 VS VSDup]"); [solve_ndisj|by etrans|..].
      { iNext. iSplitL "VS".
        - iIntros (s'). iViewUp. iIntros (Hs).
          iApply ("VS" $! s' with "[%] [%]"); [done|].
          split; [auto|by etrans].
        - iIntros (s' v). iViewUp.
          iIntros "[% Fp]".
          iApply ("VSDup" $! _ s' v with "[%] [$Fp]"); first reflexivity.
          iPureIntro. split; [auto|by etrans]. }
      iNext. iIntros (s' v Vr V') "(#HV' & kS & % & oI & Q & oR' & % & %)".
      iApply ("Post" $! V' with "HV' kS").
      assert (Hs': s1 ⊑ s' ∧ s2 ⊑ s'). { split; [auto|by etrans]. }
      iFrame (Hs') "Q".
      iExists Vβ. iFrame "aV".
      iApply "HClose". iSplitL "oI".
      + iNext. iExists γ_x. by iFrame "oI".
      + iExists γ_x. iSplit; first done. iExists V1, v1. by iFrame "oR1".
  Qed.

  Lemma GPS_nFRP_Read_abs `{aG: absViewG Σ}
    l γ q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s β (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ (▷ ∀ s', ■ (s ⊑ s') →
              (∀ v, F_past γ l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F γ l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
         ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past γ l s' v
            ={E ∖ ↑fracN .@ l}=∗ F_past γ l s' v ∗ F_past γ l s' v )
      ∗ P ∗ aSeen β ∗ ⌞vGPS_nFRP γ l s q⌟ β  }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s ⊑ s') ∗ ⌞vGPS_nFRP γ l s q⌟ β ∗ Q s' v }}}}%C.
  Proof.
    intros. iViewUp. iIntros "#kI kS (VS & VSDup & oP & #aS & oR) Post".

    iDestruct "oR" as (Vβ) "[#aV oR]".
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ_x) "(% & oS)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'. clear H0.

    iDestruct "oS" as (V1 v1) "(% & #oR)".

    iPoseProof "aS" as "aS1".
    iDestruct "aS1" as (Vβ') "[% aS1]".
    iDestruct (absView_agree with "aV aS1") as %?. subst Vβ'.
    iApply (RawProto_Read (IP γ) l P Q s _ _ V1 v1 γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|by etrans|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iExists Vβ. iFrame "aV".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ_x. by iFrame "oI".
    + iExists γ_x. iSplit; first done. iExists V1, v1. by iFrame "oR".
  Qed.

End Gname_StrongSW.

Section Gname_StrongSW_2.
  Context {Σ : gFunctors} {Prtcl : protocolT} 
          `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{CInvG : cinvG Σ}
          {IP1 IP2 : gname → interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Lemma GPS_nFWP_writer_exclusive2 γ1 γ2 l s1 s2 q1 q2 V:
    vGPS_nFWP (IP := IP1) γ1 l s1 q1 V ∗ vGPS_nFWP (IP := IP2) γ2 l s2 q2 V
    ⊢ False.
  Proof.
    rewrite fractor_join_l. iIntros "oW".
    iDestruct (fractor_drop with "oW") as (X) "[oW1 oW2]".
    iDestruct "oW1" as (γ_x1) "[% oW1]".
    iDestruct "oW2" as (γ_x2) "[% oW2]".
    subst X. apply encode_inj in H0. inversion H0.
    iDestruct "oW1" as (???) "(_ & oW1 & _)".
    iDestruct "oW2" as (???) "(_ & oW2 & _)".
    iApply (Writer_Exclusive with "[$oW1 $oW2]").
  Qed.

End Gname_StrongSW_2.

Section Gname_StrongSW_Param.
  Context {Σ : gFunctors} {Prtcl : protocolT} 
          `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          `{pC: Countable Param}
          {IP : gname → interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Notation F γ := (IP γ true).
  Notation F_past γ := (IP γ false).

  Implicit Types
      (l : loc)
      (s : pr_state Prtcl)
      (ζ : gset (state_sort Prtcl))
      (S : pr_state Prtcl * (Z * View))
      (v : Z)
      (V : View)
      (h : History)
      (p : Param).

  Local Open Scope I.
  Definition gpsSWpnRaw p γ l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (p,γ,γ_x)⌝ ∗ gps_inv (IP γ) l γ γ_x.

  Definition ReaderSWpnRaw p γ s V l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (p,γ,γ_x)⌝ ∗ PrtSeen γ s V.

  Definition WriterSWpnRaw p γ s V l n : iProp :=
    ∃ (γ_x: gname),
      ⌜n = encode (p,γ,γ_x)⌝ ∗ WAEx l γ γ_x s V.

  Definition GPS_nRSP γ l p s V : iProp :=
    persisted l (gpsSWpnRaw p γ) (ReaderSWpnRaw p γ s V).

  Definition GPS_nWSP γ l p s V : iProp :=
    persisted l (gpsSWpnRaw p γ) (WriterSWpnRaw p γ s V).

  Definition vPred_GPS_nRSP_mono γ l p  s:
    vPred_Mono (GPS_nRSP γ l p s).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ_x) "(? & RA)".
    iExists X. iFrame. iExists γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_nRSP γ l p s :=
    Build_vPred _ (vPred_GPS_nRSP_mono γ l p s).

  Global Instance vGPS_nRSP_persistent γ l p s V
    : PersistentP (vGPS_nRSP γ l p s V) := _.

  Definition vPred_GPS_nWSP_mono γ l p s:
    vPred_Mono (GPS_nWSP γ l p s).
  Proof.
    intros V V' Mono.
    iIntros "WP". iDestruct "WP" as (X) "(WPV & ?)".
    iDestruct "WPV" as (γ_x) "(? & WA)".
    iExists X. iFrame. iExists γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_nWSP γ l p s :=
    Build_vPred _ (vPred_GPS_nWSP_mono γ l p s).

  Lemma GPS_nSW_Init_strong l p (s: gname -> pr_state Prtcl) v
      (Q : gname -> vPred) (E : coPset)
      (HEPers : ↑persistN ⊆ E) (HNPhys : ↑physN ⊆ E):
  {{{{ ☐ PersistorCtx ∗ own_loc l ∗
       (∀ γ, vGPS_nWSP γ l p (s γ) ={E ∖ ↑persist_locN.@l}=∗
              ▷ F γ l (s γ) v ∗ ▷ F_past γ l (s γ) v ∗ Q γ) }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ γ, RET #() ; Q γ }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (#kIp & oL & VS) Post".
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".

    iApply wp_fupd.
    iApply (RawProto_init_strong IP _ s
            with "[$kI $kS $Hist $Alloc]"); first auto.

    iNext.
    iIntros (V' γ γ_x) "(#Le & kS & oI & ex & #oR & oW)".
    iMod (persistor_inv_alloc' E i (encode (p,γ,γ_x)) l
          (gpsSWpnRaw p γ) (WriterSWpnRaw p γ (s γ) V')
          with "[$Info $kI $kIp]") as "[WP HClose]"; [auto|auto|..].
    iMod ("VS" $! γ with "Le [- Post HClose oI kS]") as "(F & Fp & Q)".
    { iApply "WP".
      iExists γ_x. iSplitL ""; first done.
      iExists v, V', {[s γ, (v, V')]}. iFrame "ex oW oR".
      iSplit; first auto. iPureIntro.
      by move => ? /elem_of_singleton ->. }
    iMod ("HClose" with "[F Fp oI]").
    { iExists γ_x. iSplit; [iNext; done|]. iApply "oI". iFrame. }
    iApply ("Post"  with "Le kS Q").
  Qed.

  Lemma GPS_nSW_ExWrite l γ p s s' v (E : coPset)
      (P: vPred) (Q : gname → pr_state Prtcl → vPred)
      (Le: s ⊑ s') (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
  {{{{ ▷ (∀ v', P ∗ F γ l s v' ∗ vGPS_nWSP γ l p s'
            ={E ∖ ↑persist_locN .@ l}=∗ Q γ s' ∗ ▷ (F γ l s' v) ∗ ▷ F_past γ l s' v)
      ∗ P ∗ vGPS_nWSP γ l p s }}}}
    ([ #l ]_at <- #v) @ E
  {{{{ RET #() ; vGPS_nRSP γ l p s' ∗ Q γ s' }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod ((persistor_open') with "[$oW]")
      as (X) "(gpsRaw & WRaw & #HT & HClose)"; first auto.
    iDestruct "WRaw" as (γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Write_ex (IP γ) l s s' _ _ v_x V_x γ γ_x v _ ζ
            with "[$oI $kI $kS $ex $oR $wTok]");
      [solve_ndisj|auto|auto|auto|..].
    iNext.
    iIntros (V') "(% & kS' & oI & oF & ex & #oR' & wTok & #?)".
    rewrite H2.
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    iMod ("VS" with "[%] [$oP $oF wTok ex]") as "(oQ & oF & oFp)"; [done|..].
    - iApply "HT".
      iExists γ_x. iSplitL ""; first auto.
      iExists v, V',_. iFrame "wTok ex oR'". by iSplit; auto.
    - iFrame "oQ".
      iMod ("HClose" with "[-]").
      iExists γ_x. iSplitL ""; first auto.
      iApply ("oI" with "[$oF $oFp]").
      iModIntro. iApply "HT".
      iExists γ_x. iSplitL ""; first auto.
      iExists V',_. by iFrame "oR'".
  Qed.

  Lemma GPS_nSW_ExRead l γ p s (E : coPset)
      (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
  {{{{ ▷ ɐ (∀ v, F_past γ l s v ={E ∖ ↑persist_locN .@ l}=∗
                   F_past γ l s v ∗ F_past γ l s v ) ∗ vGPS_nWSP γ l p s }}}}
    ([ #l ]_at) @ E (* TODO: can be non-atomic *)
  {{{{ v, RET #v ;
      vGPS_nWSP γ l p s ∗ F_past γ l s v }}}}.
  Proof.
    iIntros (V_l π Φ).
    iViewUp; iIntros "#kI kS (VSDup & oW) Post".
    iMod ((persistor_open) with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Read_ex (IP γ)
            with "[$kI $kS $oI $ex $oR $wTok VSDup]"); [solve_ndisj|done|done|..].
    { iNext. by iSpecialize ("VSDup" $! _ with ""). }
    iNext.
    iIntros (v V') "{oR} (#HV' & kS & oI & Fp & ex & #oR' & wTok)".
    iApply ("Post" $! V' with "HV' kS"). iFrame "Fp".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ_x. by iFrame.
    + iExists γ_x. iSplitL ""; first auto.
      iExists v_x, V_x,_. iFrame "wTok ex oR'". iSplit; last auto.
      iDestruct "HV'" as "%".
      iPureIntro. etrans; eauto.
  Qed.

  Lemma GPS_nSW_Read l γ p (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
    {{{{ ▷ (∀ s', ■ (s ⊑ s') →
              (∀ v, F_past γ l s' v ∗ P ={E ∖ ↑ persist_locN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F γ l s'' v ∗ P
                                       ={E ∖ ↑persist_locN .@ l}=∗ False))
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past γ l s' v
              ={E ∖ ↑persist_locN .@ l}=∗ F_past γ l s' v ∗ F_past γ l s' v )
           ∗ P ∗ vGPS_nRSP γ l p s }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s ⊑ s')
      ∗ vGPS_nRSP γ l p s' ∗ Q s' v }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod ((persistor_open) with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first auto.
    iDestruct "RRaw" as (γ_x) "(% & RA)".
    iDestruct "gpsRaw" as (γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ_x'.
    iDestruct "RA" as (V_r v_r) "(% & #oR)".

    iApply (RawProto_Read (IP γ) _ P Q s _ _ _ v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply ("HClose" with "[oI]").
    iSplitL "oI".
    - iExists γ_x. iFrame "oI". by iNext.
    - iExists γ_x. iSplitL ""; first done. iExists Vr, v. by iFrame "oR'".
  Qed.

  Lemma GPS_nSW_Writer_Reader γ l p s V:
    vGPS_nWSP γ l p s V ⊢ vGPS_nWSP γ l p s V ∗ vGPS_nRSP γ l p s V.
  Proof.
    iIntros "oW".
    iDestruct "oW" as (X) "[oW #?]".
    iDestruct "oW" as (γ_x) "[#? WA]".
    iDestruct "WA" as (v V0 ζ) "(#? & ? & ? & #oR & #?)".
    iSplitR ""; iExists X; (iSplitR ""; last auto);
      iExists γ_x; (iSplitL ""; first auto).
    - iExists v, V0, ζ. iFrame. iSplit; [auto|by iSplitL ""].
    - iExists V0, v. by iFrame "oR".
  Qed.

  Lemma GPS_nSW_nFRP_clash `{CInvG : cinvG Σ} {IP2 : gname → interpC Σ Prtcl}
    γ l p s1 s2 q V (E : coPset)
    (HNl1: ↑persist_locN .@ l ⊆ E) (HNl2: ↑fracN .@ l ⊆ E):
    vGPS_nRSP γ l p s1 V -∗ vGPS_nFRP (IP:=IP2) γ l s2 q V ={E}=∗ False.
  Proof.
    iIntros "oR1 oR2".
    iMod (persistor_open with "oR1") as (X1) "(oI1 & oR1 & HClose1)";
      first auto.
    iMod (fractor_open with "oR2")
      as (X2) "(oI2 & oR2 & HClose2)"; first solve_ndisj.
    iDestruct "oI1" as (?) "(_ & oI1)".
    iDestruct "oI1" as (???) "(_&_&>oH1&_)".
    iDestruct "oI2" as (?) "(_ & oI2)".
    iDestruct "oI2" as (???) "(_&_&>oH2&_)".
    by iDestruct (Hist_Exclusive with "[$oH1 $oH2]") as %?.
  Qed.

  Lemma GPS_nSW_Reader_PrtSeen γ l p s V:
    vGPS_nRSP γ l p s V ⊢ PrtSeen γ s V.
  Proof.
    iIntros "oR". iDestruct "oR" as (X) "[oR ?]".
    iDestruct "oR" as (γ_x) "[? $]".
  Qed.

  Lemma GPS_nSW_Reader_PrtSeen_update γ l p s1 s2 (Le: s1 ⊑ s2) V:
    vGPS_nRSP γ l p s1 V ∗ PrtSeen γ s2 V ⊢ vGPS_nRSP γ l p s2 V.
  Proof.
    iIntros "[oR oS]". iDestruct "oR" as (X) "[oR Inv]".
    iDestruct "oR" as (γ_x) "[oX oS1]".
    iExists X. iFrame "Inv". iExists γ_x. iFrame "oX oS".
  Qed.
End Gname_StrongSW_Param.


Section Gname_StrongSW_Param_Default.
  Context {Σ : gFunctors} {Prtcl : protocolT} 
          `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          {IP : gname → interpC Σ Prtcl}.
  Set Default Proof Using "Type".

  Notation F γ := (IP γ true).
  Notation F_past γ := (IP γ false).

  Set Default Proof Using "Type".

  Lemma GPS_nSW_Init_default l (s: gname -> pr_state Prtcl) v
      (Q : gname -> vPred) (E : coPset)
      (HEPers : ↑persistN ⊆ E) (HNPhys : ↑physN ⊆ E):
  {{{{ ☐ PersistorCtx ∗ own_loc l ∗
       (∀ γ, vGPS_nWSP (IP:=IP) γ l () (s γ) ={E ∖ ↑persist_locN.@l}=∗
              ▷ F γ l (s γ) v ∗ ▷ F_past γ l (s γ) v ∗ Q γ) }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ γ, RET #() ; Q γ }}}}.
  Proof. by apply GPS_nSW_Init_strong. Qed.

End Gname_StrongSW_Param_Default.

Lemma GPS_nSWs_param_agree
  {Σ : gFunctors} {Prtcl : protocolT} 
  `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
  `{persG : persistorG Σ}
  `{agreeG : gps_agreeG Σ Prtcl}
  `{pC: Countable Param}
  {IP1 IP2 : gname → interpC Σ Prtcl} l γ1 γ2 p1 p2 s1 s2 V:
  vGPS_nRSP (IP:=IP1) γ1 l p1 s1 V
  ∗ vGPS_nRSP (IP:=IP2) γ2 l p2 s2 V ⊢ ⌜p1 = p2 ∧ γ1 = γ2⌝.
Proof.
  iIntros "RP". rewrite persistor_join_l persistor_drop.
  iDestruct "RP" as (X) "[oR1 oR2]".
  iDestruct "oR1" as (γx1) "(% & _)".
  iDestruct "oR2" as (γx2) "(% & _)".
  rewrite H0 in H. apply encode_inj in H. by inversion H.
Qed.

Section SingleWriter.
  Context {Σ : gFunctors} {Prtcl : protocolT} `{fG : !foundationG Σ}
          `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          `{CInvG : cinvG Σ}
          {IP : interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Notation F := (IP true).
  Notation F_past := (IP false).

  Implicit Types
      (l : loc)
      (s : pr_state Prtcl)
      (ζ : gset (state_sort Prtcl))
      (S : pr_state Prtcl * (Z * View))
      (v : Z)
      (V : View)
      (h : History).

  Implicit Types (e : state_sort Prtcl).

  Local Open Scope I.
  Definition gpsSWRaw l n : iProp :=
    ∃ (γ γ_x: gname), 
      ⌜n = encode (γ,γ_x)⌝ ∗ gps_inv IP l γ γ_x.

  Definition ReaderSWRaw s V l n : iProp :=
    ∃ (γ γ_x: gname),
      ⌜n = encode (γ,γ_x)⌝ ∗ PrtSeen γ s V.

  Definition WriterSWRaw s V l n : iProp :=
    ∃ (γ γ_x: gname),
      ⌜n = encode (γ,γ_x)⌝ ∗ WAEx l γ γ_x s V.

  Definition GPS_RSP l s V : iProp :=
    persisted l (gpsSWRaw) (ReaderSWRaw s V).

  Definition GPS_WSP l s V : iProp :=
    persisted l (gpsSWRaw) (WriterSWRaw s V).

  Definition GPS_FRP l s (q: frac) V : iProp :=
    fractored l (gpsSWRaw) (λ l q, ReaderSWRaw s V l) q.

  Definition GPS_FWP l s (q: frac) V : iProp :=
    fractored l (gpsSWRaw) (λ l q, WriterSWRaw s V l) q.

  Local Close Scope I.

  Definition vPred_GPS_RSP_mono l s:
    vPred_Mono (GPS_RSP l s).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ γ_x) "(? & RA)".
    iExists X. iFrame. iExists γ, γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_RSP l s :=
    Build_vPred _ (vPred_GPS_RSP_mono l s).

  Global Instance vGPS_RSP_persistent l s V : PersistentP (vGPS_RSP l s V) := _.

  Definition vPred_GPS_WSP_mono l s:
    vPred_Mono (GPS_WSP l s).
  Proof.
    intros V V' Mono.
    iIntros "WP". iDestruct "WP" as (X) "(WPV & ?)".
    iDestruct "WPV" as (γ γ_x) "(? & WA)".
    iExists X. iFrame. iExists γ, γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_WSP l s :=
    Build_vPred _ (vPred_GPS_WSP_mono l s).

  Definition vPred_GPS_FRP_mono l s q:
    vPred_Mono (GPS_FRP l s q).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ γ_x) "(? & RA)".
    iExists X. iFrame. iExists γ, γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_FRP l s q :=
    Build_vPred _ (vPred_GPS_FRP_mono l s q).

  Definition vPred_GPS_FWP_mono l s q:
    vPred_Mono (GPS_FWP l s q).
  Proof.
    intros V V' Mono.
    iIntros "WP". iDestruct "WP" as (X) "(WPV & ?)".
    iDestruct "WPV" as (γ γ_x) "(? & WA)".
    iExists X. iFrame. iExists γ, γ_x. by iFrame.
  Qed.

  Canonical Structure vGPS_FWP l s q :=
    Build_vPred _ (vPred_GPS_FWP_mono l s q).

  Local Instance : 
    Frame false (gps_inv IP l γ γ_x) (gpsSWRaw l (encode (γ, (γ_x)))) True.
  Proof. intros. iIntros "[? _]". iExists _, _. by iFrame "∗". Qed.

  Lemma GPS_SW_Readers_agree l s1 s2 (E : coPset) (HE: ↑persist_locN .@ l ⊆ E):
    (vGPS_RSP l s1 ∗ vGPS_RSP l s2 ={E}=> ■ (s1 ⊑ s2 ∨ s2 ⊑ s1))%VP%C.
  Proof.
    constructor => V_l V' HV.
    iIntros "#RP".
    iDestruct (persistor_splitjoin with "RP") as "H".
    iMod (persistor_open with "H") as (X) "(oI & (oR1 & oR2) & HClose)";
      first auto.
    iDestruct "oR1" as (γ1 γ_x1) "(% & #RA1)".
    iDestruct "oR2" as (γ2 γ_x2) "(% & #RA2)".
    iDestruct "oI" as (γ γ_x) "(>% & oI)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.
    iDestruct "RA1" as (V1 v1) "(? & #oR1)".
    iDestruct "RA2" as (V2 v2) "(? & #oR2)".
    iMod (Reader_singleton_agree _ _ _ _ s1 _ _ s2 with "[$oI $oR1 $oR2]") as "[oI %]".
    iMod ("HClose" $!
          (λ l0 X, ReaderSWRaw s1 _ l0 X ∗ ReaderSWRaw s2 _ l0 X)%I
          with "[oI]") as "oR".
    - iSplitL "oI"; last iSplitL ""; iExists γ, γ_x;
        (iSplitL ""; first auto).
      + by iNext.
      + iExists V1, v1. by iFrame "oR1".
      + iExists V2, v2. by iFrame "oR2".
    - by iPureIntro.
  Qed.

  Lemma GPS_SW_Writer_max l s s' (E : coPset) (HE: ↑persist_locN .@ l ⊆ E):
     (vGPS_RSP l s ∗ ▷ vGPS_WSP l s' ={E}=> vGPS_WSP l s' ∗ ■ (s ⊑ s'))%VP%C.
  Proof.
    constructor => V_l V HV.
    iIntros "oRW".
    iMod (persistor_join_later_rl with "oRW") as "oRW".
    iMod (persistor_open with "oRW") as (X) "(oI & (oR & oW) & HClose)";
      first auto.
    iDestruct "oR" as (γ1 γ_x1) "(% & #RA)".
    iDestruct "oW" as (γ2 γ_x2) "(>% & >WA)".
    iDestruct "oI" as (γ γ_x) "(>% & oI)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.

    iDestruct "RA" as (V1 v1) "(? & oR)".
    iDestruct "WA" as (v2 V2 ζ) "(#vS & oW & ex & #oR2 & %)".
    iDestruct (Reader_Writer_sub with "[$oW $oR]")
      as %Ins%elem_of_subseteq_singleton.
    iDestruct (Reader_Writer_sub with "[$oW $oR2]")
      as %Ins'%elem_of_subseteq_singleton.

    assert (Le := H  _ Ins).
    iMod (Writer_sorted with "[$oI $oW]") as "(oI & oW & %)".
    iMod ("HClose" $! (λ l0 X, WriterSWRaw s' V l0 X)%I
          with "[$oI oW ex]") as "oW".
    { iExists γ, γ_x. iSplitL ""; first auto.
      iExists v2, V2, ζ. by iFrame (H) "oW ex oR2 vS". }
    iFrame "oW".
    iPureIntro.
    by apply (H2 _ _ Ins Ins').
  Qed.

  Close Scope I.

  Lemma GPS_SW_Read_ex l s (E : coPset)
      (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
  {{{{ ▷ ɐ (∀ v, F_past l s v ={E ∖ ↑persist_locN .@ l}=∗
                   F_past l s v ∗ F_past l s v ) ∗ vGPS_WSP l s }}}}
    ([ #l ]_at) @ E
  {{{{ v, RET #v ;
      vGPS_WSP l s ∗ F_past l s v }}}}.
  Proof.
    iIntros (V_l π Φ).
    iViewUp; iIntros "#kI kS (VSDup & oW) Post".
    iMod ((persistor_open) with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Read_ex IP l s _ _ v_x V_x γ γ_x _ ζ
            with "[$kI $kS $oI $ex $oR $wTok VSDup]"); [solve_ndisj|done|done|..].
    { iNext. by iSpecialize ("VSDup" $! _ with ""). }
    iNext.
    iIntros (v V') "{oR} (#HV' & kS & oI & Fp & ex & #oR' & wTok)".
    iApply ("Post" $! V' with "HV' kS"). iFrame "Fp".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ, γ_x. by iFrame.
    + iExists γ, γ_x. iSplitL ""; first auto.
      iExists v_x, V_x,_. iFrame "wTok ex oR'". iSplit; last auto.
      iDestruct "HV'" as "%".
      iPureIntro. etrans; eauto.
  Qed.

  Lemma GPS_SW_Init l s v (Q : vPred) (E : coPset)
        (HEPers : ↑persistN ⊆ E) (HNPhys : ↑physN ⊆ E):
  {{{{ ☐ PersistorCtx ∗ own_loc l ∗
       (vGPS_WSP l s ={E ∖ ↑persist_locN.@l}=∗ ▷ F l s v ∗ ▷ F_past l s v ∗ Q) }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ RET #() ; Q }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (#kIp & oL & VS) Post".
    iDestruct "oL" as (h i) "(Alloc & Hist & Info)".

    iApply wp_fupd.
    iApply (RawProto_init_strong (λ _, IP)
            with "[$kI $kS $Hist $Alloc]"); first auto.

    iNext.
    iIntros (V' γ γ_x) "(#Le & kS & oI & ex & #oR & oW)".
    iMod (persistor_inv_alloc' E i (encode (γ,γ_x)) l
          (gpsSWRaw) (WriterSWRaw s V')
          with "[$Info $kI $kIp]") as "[WP HClose]"; [auto|auto|..].
    iMod ("VS" with "Le [- Post HClose oI kS]") as "(F & Fp & Q)".
    { iApply "WP".
      iExists γ, γ_x. iSplitL ""; first done.
      iExists v, V', {[s, (v, V')]}. iFrame "ex oW oR".
      iSplit; first auto. iPureIntro.
      by move => ? /elem_of_singleton ->. }
    iMod ("HClose" with "[F Fp oI]").
    { iExists γ, γ_x. iSplit; [iNext; done|]. iApply "oI". iFrame. }
    by iDestruct ("Post" with "Le kS Q") as "$".
  Qed.

  Lemma GPS_SW_Read l (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
    {{{{ ▷ (∀ s', ■ (s ⊑ s') → 
              (∀ v, F_past l s' v ∗ P ={E ∖ ↑ persist_locN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F l s'' v ∗ P
                                       ={E ∖ ↑persist_locN .@ l}=∗ False))
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v 
              ={E ∖ ↑persist_locN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
           ∗ P ∗ vGPS_RSP l s }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s ⊑ s')
      ∗ vGPS_RSP l s' ∗ Q s' v }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod ((persistor_open) with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first auto.
    iDestruct "RRaw" as (γ γ_x) "(% & RA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "RA" as (V_r v_r) "(% & #oR)".

    iApply (RawProto_Read IP l P Q s _ _ _ v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply ("HClose" with "[$oI]").
    + iExists γ, γ_x. iSplitL ""; first auto.
      iExists Vr, v. by iFrame "oR'".
  Qed.

  Lemma GPS_SW_ExWrite l s s' v (E : coPset)
      (P: vPred) (Q : pr_state Prtcl → vPred)
      (Le: s ⊑ s') (HN : ↑physN ⊆ E) (HNl: ↑persist_locN .@ l ⊆ E):
  {{{{ ▷ (∀ v', P ∗ F l s v' ∗ vGPS_WSP l s'
            ={E ∖ ↑persist_locN .@ l}=∗ Q s' ∗ ▷ (F l s' v) ∗ ▷ F_past l s' v)
      ∗ P ∗ vGPS_WSP l s }}}}
    ([ #l ]_at <- #v) @ E
  {{{{ RET #() ; vGPS_RSP l s' ∗ Q s' }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod ((persistor_open') with "[$oW]")
      as (X) "(gpsRaw & WRaw & #HT & HClose)"; first auto.
    iDestruct "WRaw" as (γ γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Write_ex IP l s s' _ _ v_x V_x γ γ_x v _ ζ
            with "[$oI $kI $kS $ex $oR $wTok]");
      [solve_ndisj|auto|auto|auto|..].
    iNext.
    iIntros (V') "(% & kS' & oI & oF & ex & #oR' & wTok & #?)".
    rewrite H2.
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    iMod ("VS" with "[%] [$oP $oF wTok ex]") as "(oQ & oF & oFp)"; [done|..].
    - iApply "HT". 
      iExists γ, γ_x. iSplitL ""; first auto.
      iExists v, V',_. iFrame "wTok ex oR'". by iSplit; auto.
    - iFrame "oQ".
      iMod ("HClose" with "[-]"). 
      iExists γ, γ_x. iSplitL ""; first auto.
      iApply ("oI" with "[$oF $oFp]").
      iModIntro. iApply "HT".
      iExists γ, γ_x. iSplitL ""; first auto.
      iExists V',_. by iFrame "oR'".
  Qed.

  Lemma GPS_SW_Writer_Reader l s:
    vGPS_WSP l s ⊧ vGPS_WSP l s ∗ vGPS_RSP l s.
  Proof.
    constructor => V.
    iViewUp; iIntros "oW".
    iDestruct "oW" as (X) "[oW #?]".
    iDestruct "oW" as (γ γ_x) "[#? WA]".
    iDestruct "WA" as (v V0 ζ) "(#? & ? & ? & #oR & #?)".
    iSplitR ""; iExists X; (iSplitR ""; last auto);
      iExists γ, γ_x; (iSplitL ""; first auto).
    - iExists v, V0, ζ. iFrame. iSplit; [auto|by iSplitL ""].
    - iExists V0, v. by iFrame "oR".
  Qed.

  Lemma GPS_FRPs_agree l s1 s2 q1 q2 (E : coPset) (HE: ↑fracN .@ l ⊆ E):
    (vGPS_FRP l s1 q1 ∗ vGPS_FRP l s2 q2 
      ={E}=> ■ (s1 ⊑ s2 ∨ s2 ⊑ s1) ∗ vGPS_FRP l s1 q1 ∗ vGPS_FRP l s2 q2)%VP%C.
  Proof.
    constructor => V.
    iViewUp; iIntros "RP".
    iDestruct (fractor_splitjoin l with "RP") as "H".
    iMod (fractor_open with "H") as (X) "(oI & (oR1 & oR2) & HClose)";
      first auto.
    iDestruct "oR1" as (γ1 γ_x1) "(% & #RA1)".
    iDestruct "oR2" as (γ2 γ_x2) "(% & #RA2)".
    iDestruct "oI" as (γ γ_x) "(>% & oI)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.
    iDestruct "RA1" as (V1 v1) "(? & oR1)".
    iDestruct "RA2" as (V2 v2) "(? & oR2)".
    iMod (Reader_singleton_agree _ _ _ _ s1 _ _ s2 with "[$oI $oR1 $oR2]") as "[oI %]".
    iMod ("HClose" $!
          (λ l0 _ X, ReaderSWRaw s1 _ l0 X ∗ ReaderSWRaw s2 _ l0 X)%I
          with "[oI]") as "oR".
    - iSplitL "oI"; last iSplitL ""; iExists γ, γ_x;
        (iSplitL ""; first auto).
      + by iNext.
      + iExists V1, v1. by iFrame "oR1".
      + iExists V2, v2. by iFrame "oR2".
    - iDestruct (fractor_splitjoin l _
                   (λ l0 _ X, ReaderSWRaw s1 V l0 X)
                   (λ l0 _ X, ReaderSWRaw s2 V l0 X)
                   with "oR") as "[o1 o2]".
      by iFrame "o1 o2".
  Qed.

  Lemma GPS_FWP_Init l s v (E : coPset)
    (HEN : ↑physN ⊆ E):
  {{{{ own_loc l ∗ ▷ F l s v ∗ ▷ F_past l s v }}}}
    ([ #l ]_na <- #v) @ E
  {{{{ RET #() ; vGPS_FWP l s 1%Qp }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (oL & F & Fp) Post".
    iDestruct "oL" as (h i)  "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (RawProto_init IP l s v _ h
               with "[$kI $kS $Hist $Alloc $F $Fp]"); first auto.
    iNext.
    iIntros (V' γ γ_x) "(#Le & kS' & oI & ex & #oR & oW)".

    iMod (fractor_alloc E i (encode(γ,γ_x)) l
                        (gpsSWRaw) (λ l q, WriterSWRaw s V' l)
           with "[-Post kS']") as "FP"; first auto.
      { iFrame "kI Info". iSplitL "oI"; iExists γ, γ_x.
        - iSplitL ""; [by iNext|done].
        - iSplitL ""; first done.
          iExists v, V', {[s, (v, V')]}. iFrame "ex oW oR".
          iSplit; first auto. iPureIntro.
          by move => ? /elem_of_singleton ->. }
    by iApply ("Post" $! V' with "Le kS' [$FP]").
  Qed.

  Lemma GPS_FRP_Read l q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
    s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ (▷ ∀ s', ■ (s ⊑ s') → 
              (∀ v, F_past l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
            ∨ (∀ s'' v, ■ (s' ⊑ s'') → F l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
         ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v
            ={E ∖ ↑fracN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
      ∗ P ∗ vGPS_FRP l s q  }}}}
    ([ #l ]_at) @ E
  {{{{ s' v, RET #v ;
      ■ (s ⊑ s') ∗ vGPS_FRP l s' q ∗ Q s' v }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ γ_x) "(% & RA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "RA" as (V_r v_r) "(% & #oR)".

    iApply (RawProto_Read IP l P Q s _ _ V_r v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ, γ_x. by iFrame "oI".
    + iExists γ, γ_x.
      iSplit; first done.
      iExists Vr, v. by iFrame "oR'".
  Qed.

  Lemma GPS_FWP_ExRead l s q (E : coPset)
      (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ ▷ ɐ (∀ v, F_past l s v ={E ∖ ↑fracN .@ l}=∗
                   F_past l s v ∗ F_past l s v ) ∗ vGPS_FWP l s q }}}}
    ([ #l ]_at) @ E
  {{{{ v, RET #v ;
      vGPS_FWP l s q ∗ F_past l s v }}}}.
  Proof.
    iIntros (V_l π Φ).
    iViewUp; iIntros "#kI kS (VSDup & oW) Post".
    iMod (fractor_open with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & oR & %)".

    iApply (RawProto_Read_ex IP l s _ _ v_x V_x γ γ_x _ ζ
            with "[$kI $kS $oI $ex $oR $wTok VSDup]"); [solve_ndisj|done|done|..].
    { iNext. by iSpecialize ("VSDup" $! _ with ""). }
    iNext.
    iIntros (v V') "(#HV' & kS & oI & Fp & ex & oR' & wTok)".
    iApply ("Post" $! V' with "HV' kS"). iFrame "Fp".
    iApply "HClose". iSplitL "oI".
    + iNext. iExists γ, γ_x. by iFrame.
    + iExists γ, γ_x. iSplitL ""; first auto.
      iExists v_x, V_x,_. iFrame "wTok ex oR'". iSplit; last auto.
      iDestruct "HV'" as "%".
      iPureIntro. etrans; eauto.
  Qed.

  Lemma GPS_FWP_Writer_max l s s' q1 q2 (E : coPset) (HE: ↑fracN .@ l ⊆ E):
     (vGPS_FRP l s q1 ∗ ▷ vGPS_FWP l s' q2 ={E}=>
        vGPS_FRP l s q1 ∗ vGPS_FWP l s' q2 ∗ ■ (s ⊑ s'))%VP%C.
  Proof.
    constructor => V_l V HV.
    iIntros "oRW".
    iMod (fractor_join_later_rl with "oRW") as "oRW".
    iMod (fractor_open with "oRW") as (X) "(oI & (oR & oW) & HClose)";
      first auto.
    iDestruct "oR" as (γ1 γ_x1) "(% & #RA)".
    iDestruct "oW" as (γ2 γ_x2) "(>% & >WA)".
    iDestruct "oI" as (γ γ_x) "(>% & oI)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.

    iDestruct "RA" as (V1 v1) "(? & oR)".
    iDestruct "WA" as (v2 V2 ζ) "(#vS & oW & ex & #oR2 & %)".
    iDestruct (Reader_Writer_sub with "[$oW $oR]")
      as %Ins%elem_of_subseteq_singleton.
    iDestruct (Reader_Writer_sub with "[$oW $oR2]")
      as %Ins'%elem_of_subseteq_singleton.

    assert (Le := H  _ Ins).
    iMod (Writer_sorted with "[$oI $oW]") as "(oI & oW & %)".
    iMod ("HClose" $! (λ l0 _ X, ReaderSWRaw s V l0 X ∗ WriterSWRaw s' V l0 X)%I
          with "[$oI oW ex]") as "oW".
    { iSplitL ""; iExists γ, γ_x; (iSplitL ""; first auto).
      - iExists V1, v1. iFrame "oR RA".
      - iExists v2, V2, ζ. by iFrame "vS oW ex oR2". }
    iDestruct (fractor_splitjoin l _
                   (λ l0 _ X, ReaderSWRaw s V l0 X)
                   (λ l0 _ X, WriterSWRaw s' V l0 X)
                   with "oW") as "[o1 o2]".
    iFrame "o1 o2".
    iPureIntro. by apply (H2 _ _ Ins Ins').
  Qed.

  Lemma GPS_FWP_Writer_Reader l s q1 q2:
    vGPS_FWP l s (q1 + q2) ⊧ vGPS_FWP l s q1 ∗ vGPS_FRP l s q2.
  Proof.
    constructor => V.
    iViewUp; iIntros "oW".
    iApply fractor_splitjoin.
    iApply (fractor_mono with "[$oW]").
    iIntros (X) "oW".
    iDestruct "oW" as (γ γ_x) "[#? WA]".
    iDestruct "WA" as (v V0 ζ) "(#? & ? & ? & #oR & #?)".
    iSplitR ""; iExists γ, γ_x; (iSplitL ""; first auto).
    - iExists v, V0, ζ. iFrame. iSplit; [auto|by iSplitL ""].
    - iExists V0, v. by iFrame "oR".
  Qed.

  Lemma GPS_FWP_Writer_Reader_join l s q1 q2:
    vGPS_FWP l s q1 ∗ vGPS_FRP l s q2 ⊧ vGPS_FWP l s (q1 + q2).
  Proof.
    constructor => V.
    iViewUp; iIntros "(o1 & o2)". iCombine "o1" "o2" as "oW".
    rewrite fractor_splitjoin; last auto.
    iApply (fractor_mono with "[$oW]").
    by iIntros (X) "(oW & _)".
  Qed.

  Global Instance GPS_FRP_fractional l s V:
    Fractional (λ q, vGPS_FRP l s q V).
  Proof.
    intros q1 q2. rewrite /vGPS_FRP /= /GPS_FRP. iSplit.
    - iIntros "oR". iApply fractor_splitjoin.
      iApply (fractor_mono with "[$oR]").
      iIntros (?) "#?". by iSplit.
    - iIntros "(oR1 & oR2)".
      iCombine "oR1" "oR2" as "oR".
      rewrite fractor_splitjoin; last auto.
      iApply (fractor_mono with "[$oR]"). by iIntros (X) "[R1 R2]".
  Qed.

  Lemma GPS_FRP_join l s q1 q2:
    vGPS_FRP l s q1 ∗ vGPS_FRP l s q2 ⊧ vGPS_FRP l s (q1 + q2).
  Proof.
    constructor => V. iViewUp.
    change (vGPS_FRP l s (q1 + q2) V)
      with ((λ q, vGPS_FRP l s q V) (q1 + q2)%Qp).
    by rewrite fractional.
  Qed.

  Lemma GPS_FRP_split l s q1 q2:
     vGPS_FRP l s (q1 + q2) ⊧ vGPS_FRP l s q1 ∗ vGPS_FRP l s q2.
  Proof.
    constructor => V. iViewUp.
    change (vGPS_FRP _ _ _ _) with ((λ q, vGPS_FRP l s q V) (q1 + q2)%Qp).
    by rewrite fractional.
  Qed.

  Lemma GPS_FRP_mult_splitL l s (n: positive) q:
    vGPS_FRP l s (Pos2Qp n * q)
      ⊧ [∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_FRP l s q.
  Proof.
    constructor => V. iViewUp.
    rewrite -{1}(Pos2Nat.id n).
    change (vGPS_FRP l s _ V)
      with ((λ q, vGPS_FRP l s q V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma GPS_FRP_mult_joinL l s (n: positive) q:
    ([∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_FRP l s q)
      ⊧ vGPS_FRP l s (Pos2Qp n * q).
  Proof.
    constructor => V. iViewUp.
    rewrite -{2}(Pos2Nat.id n).
    change (vGPS_FRP l s _ V)
      with ((λ q, vGPS_FRP l s q V) ((Pos2Qp (Pos.of_nat (Pos.to_nat n))
                                      * q)%Qp)).
    rewrite big_sepL_mult_fractional; [auto|lia].
  Qed.

  Lemma GPS_FWP_mult_splitL_1 l s n:
    vGPS_FWP l s 1
    ⊧ vGPS_FWP l s (1/(n+1))
      ∗ [∗ list] _ ∈ seq 0 (Pos.to_nat n), vGPS_FRP l s (1/(n+1)).
  Proof.
    rewrite {1}(Pos2Qp_plus_mult n).
    constructor => V. iViewUp. iIntros "FWP".
    destruct (GPS_FWP_Writer_Reader l s (1 / (n + 1)) (Pos2Qp n * (1 / (n + 1))))
     as [Split].
    iDestruct (Split with "FWP") as "[$ FRP]"; first done.
    destruct (GPS_FRP_mult_splitL l s n (1 / (n + 1))) as [Split2].
    by iApply Split2.
  Qed.

  Lemma GPS_FWP_ExWrite l s s' q v (E : coPset)
      (P: vPred) (Q : pr_state Prtcl → vPred)
      (Le: s ⊑ s') (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
  {{{{ ▷ (∀ v', P ∗ ▷ (F l s v')
            ={E ∖ ↑fracN .@ l}=∗ Q s' ∗ ▷ (F l s' v) ∗ ▷ F_past l s' v)
      ∗ P ∗ vGPS_FWP l s q }}}}
    ([ #l ]_at <- #v) @ E
  {{{{ RET #() ; vGPS_FWP l s' q ∗ Q s' }}}}.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod (fractor_open with "[$oW]")
      as (X) "(gpsRaw & WRaw & HClose)"; first auto.
    iDestruct "WRaw" as (γ γ_x) "(% & WA)".
    iDestruct "gpsRaw" as (γ' γ_x') "(>% & oI)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "WA" as (v_x V_x ζ) "(% & wTok & ex & #oR & %)".

    iApply (RawProto_Write_ex IP l s s'
            with "[$oI $kI $kS $ex $oR $wTok]");
      [solve_ndisj|auto|auto|auto|..].
    iNext.
    iIntros (V') "(% & kS' & oI & oF & ex & #oR' & wTok & #Max)".
    rewrite H2.
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    iMod ("VS" with "[%] [$oP $oF]") as "(oQ & oF & oFp)"; [done|..].
    iFrame "oQ". iApply ("HClose"). iSplitL "oI oF oFp".
    - iExists γ, γ_x. iSplitL ""; first auto.
      iApply ("oI" with "[$oF $oFp]").
    - iExists γ, γ_x. iSplitL ""; first auto.
      iExists v, V', _. by iFrame "wTok ex oR' Max".
  Qed.

End SingleWriter.

Arguments vGPS_WSP [_ _ _ _ _ _] _ _ _.
Arguments vGPS_RSP [_ _ _ _ _ _] _ _ _.
Arguments GPS_SW_Init [_ _ _ _ _ _] _ [_] _ [_] _ [_] _ _ [_ _ _].
Arguments GPS_SW_Read [_ _ _ _ _ _] _ [_] _ _ [_ _] _ _ [_ _ _].
Arguments GPS_SW_Read_ex [_ _ _ _ _ _] _ [_ _ _] _ _ [_ _ _].
Arguments GPS_SW_ExWrite [_ _ _ _ _ _] _ [_ _] _ [_ _] _ _ _ _ _ [_ _ _].
(* Arguments GPS_SW_FAI [_ _] [_ _ _ _ _ _ _] [_] _ [_ _] _ _ _ _ [_ _ _]. *)
(* Arguments GPS_SW_CAS [_ _] [_ _ _ _ _ _ _] [_] _ [_ _ _] _ _ _ _ _ [_ _ _]. *)
Arguments GPS_SW_Writer_Reader [_ _] [_ _ _ _ _ _ _].
Arguments GPS_SW_Readers_agree [_ _] [_ _ _ _ _ _ _ _ _] _.

Arguments vGPS_FWP [_ _ _ _ _ _] _ _ _ _.
Arguments vGPS_FRP [_ _ _ _ _ _] _ _ _ _.
Arguments GPS_FWP_Init [_ _ _ _ _ _] _ [_] _ [_ _] _ [_ _ _].
Arguments GPS_FRP_Read [_ _ _ _ _ _] _ [_ _] _ _ [_ _] _ _ [_ _ _].
Arguments GPS_FWP_ExRead [_ _ _ _ _ _] _ [_ _ _ _] _ _ [_ _ _].
Arguments GPS_FWP_ExWrite [_ _ _ _ _ _] _ [_ _] _ [_ _ _] _ _ _ _ _ [_ _ _].

Arguments vGPS_nWSP [_ _ _ _ _ _ _ _ _] _ _ _ _.
Arguments vGPS_nRSP [_ _ _ _ _ _ _ _ _] _ _ _ _.
Arguments GPS_nSW_Init_strong [_ _ _ _ _ _ _ _ _] _ [_] _ _ [_] _ [_] _ _ [_ _ _].
Arguments GPS_nSW_ExRead [_ _ _ _ _ _ _ _ _] _ [_ _] _ [ _ _] _ _ [_ _ _].
Arguments GPS_nSW_ExWrite [_ _ _ _ _ _ _ _ _] _ [_ _] _ [_] _ [_ _] _ _ _ _ _ [_ _ _].
Arguments GPS_nSW_Read [_ _ _ _ _ _ _ _ _] _ [_ _] _ _ _ [_ _] _ _ [_ _ _].
Arguments GPS_nSW_Init_default [_ _ _ _ _ _] _ [ _] _ [_] _ [_] _ _ [_ _ _].
Arguments GPS_nSWs_param_agree [_ _ _ _ _ _ _ _ _ _ _ _ _].
Arguments GPS_nSW_nFRP_clash [_ _ _ _ _ _ _ _ _ _ _ _].

Arguments vGPS_nFWP [_ _ _ _ _ _] _ _ _ _ _.
Arguments vGPS_nFRP [_ _ _ _ _ _] _ _ _ _ _.
Arguments GPS_nFWP_Init_strong [_ _ _ _ _ _] _ [_] _ [_] _ [_] _ [_ _ _].
Arguments GPS_nFWP_ExRead [_ _ _ _ _ _] _ _ [_] _ [ _ _] _ _ [_ _ _].
Arguments GPS_nFWP_ExWrite [_ _ _ _ _ _] _ _ [_ _] _ [_ _ _] _ _ _ _ _ [_ _ _].
Arguments GPS_nFRP_Read [_ _ _ _ _ _] _ [_] _ [_] _ _ [_ _] _ _ [_ _ _].

Notation "'[XP' l 'in' s | p ']_W'" :=
  (vGPS_WSP p l s)
    (format "[XP  l  in  s  |  p  ]_W").
Notation "'[XP' l 'in' s | p ']_R'" :=
  (vGPS_RSP p l s)
    (format "[XP  l  in  s  |  p  ]_R").


Notation "'[FXP' l 'in' s | p ']_W_' q" :=
  (vGPS_FWP p l s q)
    (at level 0,
     format "[FXP  l  in  s  |  p  ]_W_ q").

Notation "'[FXP' l 'in' s | p ']_R_' q" :=
  (vGPS_FRP p l s q)
    (at level 0,
     format "[FXP  l  in  s  |  p  ]_R_ q").

Notation "'[nXP' l 'in' s '@' γ | F ']_W'" :=
  (vGPS_nWSP F γ l () s)
    (format "[nXP  l  in  s  @  γ  |  F  ]_W").

Notation "'[nXP' l 'in' s '@' γ | F ']_R'" :=
  (vGPS_nRSP F γ l () s)
    (format "[nXP  l  in  s  @  γ  |  F  ]_R").

Notation "'[nXP' l 'in' s '@' '(' p ',' γ ')' | F ']_W'" :=
  (vGPS_nWSP F γ l p s)
    (format "[nXP  l  in  s  @  ( p , γ )  |  F  ]_W").

Notation "'[nXP' l 'in' s '@' '(' p ',' γ ')' | F ']_R'" :=
  (vGPS_nRSP F γ l p s)
    (format "[nXP  l  in  s  @  ( p , γ )  |  F  ]_R").

Notation "'[nFXP' l 'in' s '@' γ | p ']_W_' q" :=
  (vGPS_nFWP p γ l s q)
    (at level 0,
     format "[nFXP  l  in  s @ γ  |  p  ]_W_ q").

Notation "'[nFXP' l 'in' s '@' γ | p ']_R_' q" :=
  (vGPS_nFRP p γ l s q)
    (at level 0,
     format "[nFXP  l  in  s @ γ  |  p  ]_R_ q").
