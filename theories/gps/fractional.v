From iris.proofmode Require Import tactics.
From igps.gps Require Export inst_shared.
From igps Require Import abs_view.

Section Fractional.
  Context `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          `{persG : persistorG Σ}
          `{agreeG : gps_agreeG Σ Prtcl}
          `{CInvG : cinvG Σ}
          {IP : interpC Σ Prtcl}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).

  Notation F := (IP true).
  Notation F_past := (IP false).

  Implicit Types
           (l : loc)
           (s : pr_state Prtcl)
           (ζ : gset (state_sort Prtcl))
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View)
           (h : History).

  Notation RA γ s := (∃ V_0 v, vSeen V_0 ∧ ☐ (Reader γ {[s, (v, V_0)]}))%VP.

  Implicit Types (e : state_sort Prtcl).

  Local Open Scope I.
  Definition gpsFRaw l n : iProp :=
    ∃ (γ γ_x: gname) ζ,
      ⌜n = encode(γ,γ_x)⌝ ∗ gps_inv IP l γ γ_x ∗ Writer γ ζ.

  Definition RFRaw s V l (q: frac) n : iProp :=
    ∃ (γ γ_x: gname) e_x,
      ⌜n = encode(γ,γ_x)⌝ ∗ exwr γ_x q e_x
                 ∗ ∃ Vr v, vSeen Vr V
                                 ∗ ⌜st_view e_x !! l ⊑ Vr !! l⌝ ∗ Reader γ {[s, (v, Vr)]}.

  Definition GPS_FP l s (q: frac) V : iProp :=
    fractored l (gpsFRaw) (RFRaw s V) q.

  Definition vPred_GPS_FP_mono l s q:
    vPred_Mono (GPS_FP l s q).
  Proof.
    intros V V' Mono.
    iIntros "RP". iDestruct "RP" as (X) "(RPV & ?)".
    iDestruct "RPV" as (γ γ_x e_x) "(? & ? & RA)".
    iDestruct "RA" as (Vr v) "(% & ? & ?)".
    iExists X. iFrame. iExists γ, γ_x, e_x. iFrame.
    iExists Vr, v. iFrame. iPureIntro.
    etrans; last exact Mono. done.
  Qed.

  Canonical Structure vGPS_FP l s q :=
    Build_vPred _ (vPred_GPS_FP_mono l s q).

  Local Close Scope I.

  Lemma GPS_FPs_agree l s1 s2 q1 q2 (E : coPset) (HE: ↑fracN .@ l ⊆ E):
    (vGPS_FP l s1 q1 ∗ vGPS_FP l s2 q2 
     ={E}=> ■ (s1 ⊑ s2 ∨ s2 ⊑ s1) ∗ vGPS_FP l s1 q1 ∗ vGPS_FP l s2 q2)%VP%C.
  Proof.
    constructor => V.
    iViewUp; iIntros "RP".
    iDestruct (fractor_splitjoin l with "RP") as "H".
    iMod (fractor_open with "H") as (X) "(oI & (oR1 & oR2) & HClose)";
    first auto.
    iDestruct "oR1" as (γ1 γ_x1 e_x1) "(% & oe1 & #RA1)".
    iDestruct "oR2" as (γ2 γ_x2  e_x2) "(% & oe2 & #RA2)".
    iDestruct "oI" as (γ γ_x ζ) "(>% & oI & oI2)". subst.
    apply encode_inj in H0. inversion H0. subst.
    apply encode_inj in H1. inversion H1. subst.
    iDestruct "RA1" as (V1 v1) "(? & ? & oR1)".
    iDestruct "RA2" as (V2 v2) "(? & ? & oR2)".
    iMod (Reader_singleton_agree _ _ _ _ s1 _ _ s2 with "[$oI $oR1 $oR2]") as "[oI %]".
    iMod ("HClose" $! (λ l0 _ X, RFRaw s1 V l0 q1 X ∗ RFRaw s2 V l0 q2 X)%I
          with "[oI oI2 oe1 oe2]") as "oR".
    - iSplitL "oI oI2"; last iSplitL "oe1"; iExists γ, γ_x.
      + iExists ζ. iFrame "oI oI2". by iNext.
      + iExists e_x1. iFrame "oe1". iSplit; first done.
        iExists V1, v1. by repeat iSplit.
      + iExists e_x2. iFrame "oe2". iSplit; first done.
        iExists V2, v2. by repeat iSplit.
    - iDestruct (fractor_splitjoin l with "oR") as "[o1 o2]".
        by iFrame "o1 o2".
  Qed.


  Lemma GPS_FP_Init l s v (E : coPset) V
        (HEN : ↑physN ⊆ E):
    {{{{ own_loc l ∗ ▷ F l s v ∗ ▷ F_past l s v }}}}
      ([ #l ]_na <- #v) @ E
      {{{{ RET #() ; vGPS_FP l s 1%Qp }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (oL & F & Fp) Post".
    iDestruct "oL" as (h i)  "(Alloc & Hist & Info)".
    iApply wp_fupd.
    iApply (RawProto_init IP l s v _ h
            with "[$kI $kS $Hist $Alloc $F $Fp]"); first auto.
    iNext.
    iIntros (V' γ γ_x) "(#Le & kS' & oI & ex & #oR & oW)".

    iMod (fractor_alloc E i (encode(γ,γ_x)) l (gpsFRaw) (RFRaw s V')
          with "[-Post kS']") as "FP"; first auto.
    { iFrame "kI Info". iSplitL "oI oW"; iExists γ, γ_x.
      - iExists {[s, (v, V')]}. iFrame "oI oW". by iNext.
      - iExists (s, (v, V')). iFrame "ex". iSplitL ""; first done.
        iExists V', v. iFrame "oR". by iSplit. }
      by iApply ("Post" $! V' with "Le kS' [$FP]").
  Qed.

  Lemma GPS_FP_Read l q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
        s (E : coPset) (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
    {{{{ (▷ ∀ s', ■ (s ⊑ s') → 
                  (∀ v, F_past l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
                  ∨ (∀ s'' v, ■ (s' ⊑ s'') → F l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v
                          ={E ∖ ↑fracN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
           ∗ P ∗ vGPS_FP l s q  }}}}
      ([ #l ]_at) @ E
      {{{{ s' v, RET #v ;
           ■ (s ⊑ s') ∗ vGPS_FP l s' q ∗ Q s' v }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR) Post".
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ γ_x e_x) "(% & Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' ζ) "(>% & oI & Writer)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_Read IP l P Q s _ _ V_r v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|auto|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iApply "HClose". iSplitL "oI Writer".
    + iNext. iExists γ, γ_x, ζ. by iFrame "oI Writer".
    + iExists γ, γ_x, e_x. iFrame "Hex".
      iSplit; first done.
      iExists Vr, v. iFrame "oR'". iSplit; first done.
      iPureIntro. do 3 (etrans; eauto).
  Qed.

  Lemma GPS_FP_Read_abs `{absG : absViewG Σ}
      l q (P : vPred) (Q : pr_state Prtcl → Z → vPred)
      s (E : coPset) β (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E):
    {{{{ (▷ ∀ s', ■ (s ⊑ s') →
                  (∀ v, F_past l s' v ∗ P ={E ∖ ↑fracN .@ l}=∗ Q s' v)
                  ∨ (∀ s'' v, ■ (s' ⊑ s'') → F l s'' v ∗ P ={E ∖ ↑fracN .@ l}=∗ False))
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v
                          ={E ∖ ↑fracN .@ l}=∗ F_past l s' v ∗ F_past l s' v )
           ∗ P ∗ ⌞vGPS_FP l s q⌟ β ∗ aSeen β }}}}
      ([ #l ]_at) @ E
      {{{{ s' v, RET #v ;
           ■ (s ⊑ s') ∗ ⌞vGPS_FP l s q⌟ β ∗ Q s' v }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSDup & oP & oR & Seen) Post".
    iDestruct "oR" as (V0) "(abs & oR)".
    iDestruct "Seen" as (V1) "(% & abs2)".
    iDestruct (absView_agree with "abs abs2") as %?. subst V1.
    iMod (fractor_open with "[$oR]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.

    iDestruct "RRaw" as (γ γ_x e_x) "(% & Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' ζ) "(>% & oI & Writer)".
    subst X. apply encode_inj in H1. inversion H1. subst γ' γ_x'.

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_Read IP l P Q s _ _ V_r v_r γ γ_x
            with "[$VS $oI $kI $kS $oP $oR VSDup]"); [solve_ndisj|by etrans|..].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s' v Vr V') "(#HV' & kS' & #Hs' & oI & oQ & #oR' & % & %)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' oQ".
    iExists V0. iFrame "abs".
    iApply "HClose". iSplitL "oI Writer".
    + iNext. iExists γ, γ_x, ζ. by iFrame "oI Writer".
    + iExists γ, γ_x, e_x. iFrame "Hex".
      iSplit; first done.
      iExists V_r, v_r. iFrame (H0 H2) "oR".
  Qed.

  Lemma GPS_FP_Write l q s s' v (E : coPset)
        (P: vPred) (Q : pr_state Prtcl → vPred) 
        (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E) (HFinal : final_st s'):
    {{{{ ▷ (P ∗ vGPS_FP l s' q ={E ∖ ↑fracN .@ l}=∗
                                                 Q s' ∗ ▷ (F l s' v) ∗ ▷ F_past l s' v)
           ∗ P ∗ vGPS_FP l s q }}}}
      ([ #l ]_at <- #v) @ E
      {{{{ RET #() ;
           Q s' }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod (fractor_open' with "[$oW]")
      as (X) "(gpsRaw & RRaw & HT & HClose)"; first auto.
    iDestruct "RRaw" as (γ γ_x e_x) "(% & Hex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' ζ) "(>% & oI & >wTok)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.

    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".
    destruct e_x as [s_x [v_x V_x]].

    iApply (RawProto_Write_non_ex IP l
                                  s s_x s' _ _ V_r V_x q v v_r v_x γ γ_x _ ζ
            with "[$kI $kS $oI $Hex $oR $wTok]");
    [solve_ndisj|auto|auto|auto|..].
    iNext.
    iIntros (V') "(% & kS' & oI & ex & #oR' & wTok)".
    iApply ("Post" $! V' with "[%] kS'"); [done|].
    rewrite [V in P V]H2.
    iAssert (vGPS_FP l s' q V') with "[HT ex]" as "oW".
    { iApply "HT".
      iExists γ, γ_x, (s_x, (v_x, V_x)). iFrame "ex".
      iSplit; first done.
      iExists V', v. iFrame "oR'". iSplit; first done.
      iPureIntro. do 2 (etrans; eauto).
    }
    iMod ("VS" with "[%] [$oP $oW]") as "($ & oF & oFp)"; [done|].
    iSpecialize ("oI" with "[$oF $oFp]").
    iMod ("HClose" with "[-]") as "$"; [|done].
    iNext. iExists γ, γ_x, ({[s', (v, V')]} ∪ ζ).
    iFrame "wTok". by iSplit.
  Qed.

  Lemma GPS_FP_FAI C l q s (E : coPset)
        (P : vPred) (Q: pr_state Prtcl → Z → vPred)
        (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E) :
    let mod1C (v: Z) := ((v + 1) `mod` Z.pos C)%Z in
    {{{{  ▷ (∀ v s', ■ (s ⊑ s') ∗ F l s' v ∗ P
                     ={E ∖ ↑fracN .@ l}=∗ ∃ s'', ■ (s' ⊑ s'') 
                                                   ∗ Q s'' v
                                                   ∗ ▷ (F l s'' (mod1C v)) ∗ ▷ F_past l s'' (mod1C v))
            ∗ P ∗ vGPS_FP l s q }}}}
      (FAI C #l) @ E
      {{{{ s'' (v: Z), RET #v ;
           ■ (s ⊑ s'')∗ vGPS_FP l s'' q ∗ Q s'' v }}}}%C.
  Proof.
    iIntros (mod1C Φ). intros; iViewUp; iIntros "#kI kS (VS & oP & oW) Post".
    iMod ((fractor_open) with "[$oW]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.
    iDestruct "RRaw" as (γ γ_x e_x) "(% & ex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' ζ) "(>% & oI & >wTok)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.

    destruct e_x as [s_x [v_x V_x]].
    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_FAI_non_ex IP _ _
                                s s_x _ _ V_r V_x q%Qp v_r v_x γ γ_x _ P Q
            with "[$VS $kI $kS $oI $oP $ex $oR wTok]");
    [auto|auto|solve_ndisj|by iExists _|].
    iNext.
    iIntros (s'' v Vr V')
            "(#HV' & kS' & Hs' & oI & Q & ex & #oR' & % & % & wTok)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' Q".
    iApply "HClose". iSplitL "oI wTok".
    - iNext. iDestruct "wTok" as (ζ') "wTok".
      iExists γ, γ_x, _.
        by iFrame "oI wTok".
    - iExists γ, γ_x, (s_x, (v_x, V_x)). iFrame "ex".
      repeat iSplit; first done.
      iExists Vr, (mod1C v). repeat iSplit; [auto| |auto].
      iPureIntro. do 2 (etrans; eauto).
  Qed.

  Lemma GPS_FP_CAS l q s v_o v_n (E : coPset)
        (P : vPred) (Q: pr_state Prtcl → vPred) (R : pr_state Prtcl → Z → vPred) 
        (HN : ↑physN ⊆ E) (HNl: ↑fracN .@ l ⊆ E) :
    {{{{ ▷ (∀ s', ■ (s ⊑ s') ∗ F l s' v_o ∗ P
                  ={E ∖ ↑fracN .@ l}=∗ ∃ s'', ■ (s' ⊑ s'') 
                                                ∗ Q s'' ∗ ▷ (F l s'' v_n) ∗ ▷ F_past l s'' v_n)
           ∗ ▷ (∀ s' v, ■ (s ⊑ s') ∗ ■ (v ≠ v_o) ∗ ▷ F_past l s' v ∗ P
                        ={E ∖ ↑fracN .@ l}=∗ R s' v)
           ∗ ▷ ɐ (∀ s' v, ■ (s ⊑ s') ∗ F_past l s' v ={E ∖ ↑fracN .@ l}=∗
                                                                       F_past l s' v ∗ F_past l s' v )
           ∗ P ∗ vGPS_FP l s q }}}}
      (CAS #l #v_o #v_n) @ E
      {{{{ s'' (b: bool) v, RET #b ;
           ■ (s ⊑ s'')
             ∗ vGPS_FP l s'' q ∗ (if b then Q s'' else R s'' v) }}}}%C.
  Proof.
    intros; iViewUp; iIntros "#kI kS (VS & VSF & VSDup & oP & oW) Post".
    iMod ((fractor_open) with "[$oW]")
      as (X) "(gpsRaw & RRaw & HClose)"; first exact HNl.
    iDestruct "RRaw" as (γ γ_x e_x) "(% & ex & RA)".
    iDestruct "gpsRaw" as (γ' γ_x' ζ) "(>% & oI & >wTok)".
    subst X. apply encode_inj in H0. inversion H0. subst γ' γ_x'.

    destruct e_x as [s_x [v_x V_x]].
    iDestruct "RA" as (V_r v_r) "(% & % & #oR)".

    iApply (RawProto_CAS_non_ex_weak IP l
                                     s s_x _ _ V_r V_x q v_o v_n v_r v_x γ γ_x _ P Q R
            with "[$VS $VSF $kI $kS $oI $oP $ex $oR wTok VSDup]");
    [solve_ndisj|solve_ndisj|solve_ndisj|iSplitL "VSDup"; last by iExists _|].
    { iNext. by iSpecialize ("VSDup" $! _). }
    iNext.
    iIntros (s'' b v Vr V')
            "(#HV' & kS' & Hs' & oI & IF & ex & #oR' & % & % & wTok)".
    iApply ("Post" $! V' with "HV' kS'"). iFrame "Hs' IF".
    iApply "HClose". iSplitL "oI wTok".
    + iNext. iDestruct "wTok" as (ζ') "wTok".
      iExists γ, γ_x, _.
        by iFrame "oI wTok".
    + iExists γ, γ_x, (s_x, (v_x, V_x)). iFrame "ex".
      repeat iSplit; first done.
      iExists Vr, v. repeat iSplit; [auto| |auto].
      iPureIntro. do 2 (etrans; eauto).
  Qed.

  Global Instance GPS_FP_fractional l s V:
    Fractional (λ q, vGPS_FP l s q V).
  Proof.
    intros q1 q2.
    rewrite /vGPS_FP /= /GPS_FP.
    rewrite fractor_splitjoin; last auto.
    iSplit; iIntros "FR";
    iApply (fractor_mono with "[$FR]"); iIntros (X).
    - iIntros "R". iDestruct "R" as (γ γ_x e_x) "(% & (ex1 & ex2) & #R)".
      iSplitL "ex1"; iExists γ, γ_x, e_x; iFrame "#"; by iFrame.
    - iIntros "[R1 R2]".
      iDestruct "R1" as (? ? ?) "(% & ex1 & R1)".
      iDestruct "R2" as (γ γ_x e_x) "(% & ex2 & R2)".
      subst X. apply encode_inj in H3. inversion H3. subst.
      iExists γ, γ_x, e_x. iFrame "R2". iSplit; first auto.
      iCombine "ex1" "ex2" as "ex".
      iDestruct (own_valid with "ex") as %Val%auth_valid_discrete.
      move: Val => [_ /= /dec_agree_op_inv Eqe].
      inversion Eqe. subst.
      iDestruct "ex" as "[ex1 ex2]". rewrite dec_agree_idemp.
      iSplitL "ex1"; iFrame.
  Qed.

  Lemma GPS_FP_splitjoin l s q1 q2 V:
    vGPS_FP l s q1 V ∗ vGPS_FP l s q2 V ⊣⊢ vGPS_FP l s (q1 + q2) V.
  Proof. by rewrite GPS_FP_fractional. Qed.

  Lemma GPS_FP_dealloc l s V (E: coPset) (HNl: ↑fracN .@ l ⊆ E):
    vGPS_FP l s 1%Qp V ={E}=∗ own_loc l V.
  Proof.
    iIntros "FP".
    iMod (fractor_dealloc with "FP") as (n i) "(Info & RR & Inv)"; first auto.
    iDestruct "RR" as (γ γ_x e_x) "(% & ex & RA)".
    iDestruct "Inv" as (γ' γ_x' ζ) "(>% & oI & _)".
    subst n. apply encode_inj in H0. inversion H0. subst γ' γ_x'.
    iDestruct "oI" as (ζ' h e') "(>oA & _ & >Hist & ? & ? & ? & ? & >% & ?)".

    iDestruct "RA" as (Vr v) "(% & _ & RA)".
    iDestruct (Reader_Auth_sub with "[$oA $RA]")
      as %Inζ%elem_of_subseteq_singleton.

    iExists h, i. iFrame "Hist Info". iPureIntro.
    exists (VInj v), Vr. repeat split; [|auto|auto].
    rewrite -H elem_of_map_gset. by exists (s, (v, Vr)).
  Qed.

End Fractional.

Arguments vGPS_FP [_ _ _ _ _ _] _ _ _ _.
Arguments GPS_FP_Init [_ _ _ _ _ _] _ [_] _ [_] _ _ _ [_ _ _].
Arguments GPS_FP_Read [_ _ _ _ _ _] _ [_ _] _ _ [_ _] _ _ [_ _ _].
Arguments GPS_FP_Write [_ _ _ _ _ _] _ [_ _ _ _] [_ _] _ _ _ _ _ [_ _ _].
Arguments GPS_FP_FAI [_ _ _ _ _ _] _ [_ _] [_ _ _] _ _ _ _ [_ _ _].
Arguments GPS_FP_CAS [_ _ _ _ _ _] _ [_ _] [_ _ _ _] _ _ _ _ _ [_ _ _].
Arguments GPS_FPs_agree [_ _ _ _ _ _ _ _ _ _ _ _ _] _.
Arguments GPS_FP_splitjoin [_ _ _ _ _ _ _ _ _ _ _ _].

Notation "'[FP' l 'in' s | p ]_ q" :=
  (vGPS_FP p l s q)
    (at level 0, 
     format "[FP  l  in  s  |  p  ]_ q").
