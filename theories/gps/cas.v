From iris.program_logic Require Export weakestpre.
From igps.gps Require Import shared.
From igps.base Require Import at_cas.

Section CAS.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F := (IP true l).
  Notation F_past := (IP false l).

  Lemma RawProto_CAS_non_ex
    s_r s_x π (V V_r V_x: View) q (v_o v_n v_r v_x: Z) γ γ_x (E E' : coPset)
    (P : vPred) (Q: pr_state Prtcl → vPred) (R : pr_state Prtcl → Z → vPred)
    (T: pr_state Prtcl → pr_state Prtcl → vPred)
    (VxVr : V_x !! l ⊑ V_r !! l) (VrV : V_r ⊑ V)
    (HEN : ↑physN ⊆ E)
    (HE : E' ⊆ E):
    {{{ (▷ (∀ s', ■ (s_r ⊑ s') ∗ F s' v_o ∗ P
                      ={E, E'}=∗ ∃ s'', ■ (s' ⊑ s'') ∗ T s' s''
                                  ∗ ☐ (∃ ζ, Writer γ ζ) ))%VP V
        ∗ (▷ (∀ s' s'', ■ (s_r ⊑ s') ∗ ■ (s' ⊑ s'') ∗ T s' s''
                         ∗ ☐ (∃ ζ, Writer γ ζ) ={E', E}=∗
                            Q s'' ∗ ▷ (F s'' v_n) ∗ ▷ F_past s'' v_n))%VP V
        ∗ (▷ (∀ s' v, ■ (s_r ⊑ s') ∗ F_past s' v ={E}=∗ F_past s' v ∗ F_past s' v))%VP ∅
        ∗ (▷ (∀ s' v, ■ (s_r ⊑ s') ∗ ■ (v ≠ v_o) ∗ F_past s' v ∗ P
                         ={E}=∗ R s' v))%VP V
        ∗ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ P V
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s_r, (v_r, V_r)]} }}}
      (CAS #l #v_o #v_n, V) @ E
    {{{ s'' (b: bool) v Vr V', RET (#b, V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ⌜s_r ⊑ s''⌝
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ (if b then Q s'' else R s'' v) V'
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s'', (v, Vr)]} ∗ ⌜Vr ⊑ V'⌝ ∗ ⌜V !! l ⊑ Vr !! l⌝}}}.
  Proof.
    iIntros (Φ) "(VS1 & VS2 & VSDup & VSF & #kI & Seen & oI & P & ofX & #oR) Post".
    iDestruct "oI" as (ζ h e_x)
              "(>oA & >oAX & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".
    iDestruct (ExWrite_agree with "[$oAX $ofX]") as %?. subst e_x.
    iDestruct (Reader_Auth_sub with "[$oA $oR]") as %?%elem_of_subseteq_singleton.
    rewrite /Consistent in H2.

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.

    iApply (f_CAS with "[$kI $Seen $Hist]").
    - iPureIntro. exists (VInj v_r), V_r. repeat split; auto.
      rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)).
    - iNext.
      iIntros (b V' h') "(% & Seen' & Hist' & % & Pure)".

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist']") as "(Hist' & %)".

      iDestruct "Pure" as (V1 v1) "Pure".
      iDestruct "Pure" as %(VrV' & InVr & VVr & CSF).

      assert (InVr2 := InVr).
      rewrite /value_at in InVr. rewrite <- H2 in InVr.
      apply elem_of_map_gset in InVr as [[s' [v Vr]] [Eq InVr]].
      simpl in Eq. inversion Eq. subst v1 V1. clear Eq.

      assert (HSub: h ⊆ h').
        { move: CSF => [[_ [_ [_ [Eqh _]]]]|[_ [_ [-> // _]]]].
          rewrite -> Eqh. apply union_subseteq_r. }

      assert (V_r !! l ⊑ Vr !! l).
        { etrans; eauto. }

      assert (InV_r: (VInj v_r, V_r) ∈ h).
        { rewrite -H2 elem_of_map_gset. by exists (s_r, (v_r, V_r)). }

      assert (s_r ⊑ s').
        { by apply (H0  _ _ H6 InVr). }

      assert (VrVrV : Vr ⊑ Vr ⊔ V) by apply join_ext_l.

      move : CSF => [   [-> [Eqv [InV' [Hh' [HDisj [Hi' [HAdj HNAdj]]]]]]]
                      | [-> [NEqv [Hh' EqVrV]]]].
      + assert (Heb: (s', (v, Vr)) ∈ sEx l (sBE l ζ) (s_x, (v_x, V_x))).
          { rewrite !elem_of_filter. repeat split; last auto.
            - etrans; eauto.
            - move => e' Ine' Ne' [t [Eq Eq1]].
              apply HNAdj. exists (VInj $ st_val e', st_view e'). split.
              + rewrite -H2 elem_of_map_gset. by exists e'.
              + exists t. by rewrite -Eq -Eq1. }

        rewrite /BE_Inv (big_op.big_sepS_delete _ _ _ Heb).
        iDestruct "oBEI" as "[F oBEI]".
        iSpecialize ("VS1" $! s' V' with "[%]"); [done|].

        rewrite -Eqv.
        iMod ("VS1" with "[$F $P]") as (s'') "(% & T & oW)"; first auto.

        iDestruct "oW" as (ζ') "oW".
        iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.

        iMod (Writer_update _ _ ({[s'', (v_n, V')]} ∪ ζ)
            with "[$oA $oW]") as "(oA' & oW')".
        { apply union_subseteq_r. }
        iDestruct (Writer_fork_Reader _ _ {[s'', (v_n, V')]} with "[$oW']")
          as "[oW' #oR']".
          { apply union_subseteq_l. }

        iAssert (⌜s' ⊑ s''⌝)%I as "#Hs''". { by done. }
        iAssert (⌜V' ⊑ V'⌝)%I as "#HV'". { by done. }
        iSpecialize ("VS2" $! s' s'' V' with "[%]"); [done|].
        iMod ("VS2" with "[$T oW']") as "(Q & F & Fp)".
          { iFrame (H10 H11). by iExists _. }

        iApply ("Post" $! s'' true v_n V' V').
        iFrame (H5) "Seen' Q ofX oR' HV'".
        iSplitL ""; first by iPureIntro; by etrans; eauto.
        iSplitR ""; last done.

        iModIntro. iNext. iExists ({[s'', (v_n, V')]} ∪ ζ), h', (s_x, (v_x, V_x)).
        iFrame "oA' oAX Hist'".

        assert (HC': Consistent ({[s'', (v_n, V')]} ∪ ζ) h').
        { unshelve eapply (Consistent_proper _ ({[s'', (v_n, V')]} ∪ ζ) _ _ _ Hh');
            first done.
          by apply Consistent_insert. }

        assert (HV': (VInj v_n, V') ∈ h').
          { rewrite Hh' elem_of_union elem_of_singleton. by left. }
        assert (HS: SortInv l ({[s'', (v_n, V')]} ∪ ζ)).
          { move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                        /elem_of_union [/elem_of_singleton ->| In2];
              [done|..|by apply H0].
            - destruct H8 as [OK Disj].
              destruct (OK _ HV') as [t Eqt].
              move => Lt.
              apply (H3 _ t In2) ; rewrite -Eqt; [|auto|]; rewrite /st_time /=.
              + etrans; last by apply H5. etrans; last by apply VrV. done.
              + move => [s0 [v0 V0]] /= In0 Eqt2.
                assert ((VInj v0,V0) ∈ h).
                  { rewrite -H2 elem_of_map_gset. by exists (s0, (v0, V0)). }
                assert (InV0: (VInj v0, V0) ∈ h').
                  { rewrite Hh' elem_of_union. by right. }
                assert (EqvV := Disj _ _ InV0 HV' Eqt2).
                inversion EqvV. subst. by apply disjoint_singleton_r in HDisj.
            - move => Le.
              etrans; last by apply H11. apply (H0 _ _ In1 InVr).
              destruct HAdj as [t [Ht1 Ht2]].
              move : Le. rewrite /st_time /= Ht1 Ht2.
              assert (Ine1: (VInj $ st_val e1, st_view e1) ∈ h).
                { rewrite -H2 elem_of_map_gset. by exists e1. }
              destruct H8 as [OK Disj].
              destruct (OK _ (HSub _ Ine1)) as [t' Eqt]. rewrite Eqt.
              assert (NEqt: Some t' ≠ Some (t +1)%positive).
                { rewrite -Eqt -Ht2 /= => Eqt2.
                  apply HNAdj.
                  exists (VInj $ st_val e1, st_view e1).
                  split; first exact Ine1. exists t. by rewrite Eqt2. }
              cbn. move => /Pos.lt_eq_cases [Le|?]; last by subst t'.
              apply Pos.lt_succ_r. by rewrite -Pos.add_1_r. }

        assert (HI: StateInjection ({[s'', (v_n, V')]} ∪ ζ)).
          { by apply (StateInjection_insert _ _ _ h). }

        assert (HF: FinalInv l (s_x, (v_x, V_x)) ({[s'', (v_n, V')]} ∪ ζ)).
        { move => e t /elem_of_union [/elem_of_singleton -> | ?] ? Le NEq.
          - move => e'. etrans; last by apply H11.
            apply (H3 _ t InVr); [auto| |].
            + destruct HAdj as [t' [Ht1 Ht2]].
              assert (NEqt: Some t ≠ Some (t' +1)%positive).
                { rewrite -Ht2 /= => Eqt2.
                  apply (NEq (s'', (v_n, V'))).
                  - rewrite elem_of_union elem_of_singleton. by left.
                  - by rewrite Eqt2. }
              move : Le. rewrite /st_time /= Ht1 Ht2.
              cbn. move => /Pos.lt_eq_cases [Le|?]; last by subst t.
              apply Pos.lt_succ_r. by rewrite -Pos.add_1_r.
            + move => e2 In2. apply NEq, elem_of_union. by right.
          - apply (H3 e t); auto.
            move => e' In'. apply (NEq e'), elem_of_union. right.
            by apply elem_of_singleton. }

        iFrame (Hi' HS HC' HI HF).
        iSplitL "oBEI oAI F Fp"; last first.
          { iPureIntro. by apply elem_of_union_r. }

        assert (Hs'': (s'', (v_n, V')) ∉ ζ).
          { move => In.
            apply disjoint_singleton_r in HDisj.
            apply HDisj. rewrite -H2. apply elem_of_map_gset.
            by exists (s'', (v_n, V')). }

        iSplitL "F oBEI"; last first.
          { rewrite /All_Inv.
            rewrite (big_op.big_sepS_insert _ _ _ Hs''). by iFrame. }

        rewrite /BE_Inv.
        iCombine "F" "oBEI" as "oBEI".
        rewrite -(big_op.big_sepS_insert _ _ (s'', (v_n, V'))); last first.
          { move => In.
            by apply gset_difference_subseteq,
              subseteq_gset_filter, subseteq_gset_filter in In. }

        set BE':= sEx l (sBE l ({[s'', (v_n, V')]} ∪ ζ)) (s_x, (v_x, V_x)).
        iApply (big_op.big_sepS_mono _ _ _ BE' with "[$oBEI]");
                last reflexivity.

        set P' := λ e, st_view (s_x, (v_x, V_x)) !! l ⊑ st_view e !! l.

        rewrite <- (gset_filter_singleton (s', (v, Vr)) (P:= P'));
          [| rewrite /P'; etrans; eauto].
        rewrite <- (gset_filter_singleton (s'', (v_n, V')) (P:= P'));
          [| rewrite /P'; do 2 (etrans; eauto)].
        rewrite -gset_filter_difference -gset_filter_union.
        apply gset_filter_subseteq.

        set fm := λ (e: state_sort), (VInj $ st_val e, st_view e).
        apply (gset_map_subseteq_inj fm).
        { move => ? ? Hin1 Hin2.
          apply HI;
          [abstract set_solver+Hin1 InVr by auto
            |abstract set_solver+Hin2 InVr by auto]. }

        rewrite gset_map_union.
        rewrite - gset_map_difference_inj; last first.
        { move => ? ? Hin1 Hin2.
          apply HI;
          [abstract set_solver+Hin1 InVr by auto
            |abstract set_solver+Hin2 InVr by auto]. }
        rewrite 2!gset_map_singleton.
        do 2!(rewrite -block_ends_fmap; [|done|done]).
        rewrite /Consistent in HC'.

        rewrite H2 HC' Hh'.
        apply gblock_ends_ins_sL_update;[| |done|..].
        * apply elem_of_filter. split; auto.
          move => p ? ? ?. apply HNAdj. by exists p.
        * move => In. apply subseteq_gset_filter in In.
          by apply disjoint_singleton_r in HDisj.
        * apply gblock_ends_ins_update; last done.
          apply (hTime_ok_mono _ _ h'); last done. by rewrite Hh'.
        * apply (hTime_pw_disj_mono _ _ h');
          [by rewrite Hh'|by apply H8].

      (* Case: CAS fails *)
      + rewrite /All_Inv (big_op.big_sepS_delete _ _ _ InVr).
        iDestruct "oAI" as "[Fp oAI]".
        iMod ("VSDup" with "[%] [$Fp]") as "[Fp Fp']"; [done|done|].
        iCombine "Fp'" "oAI" as "oAI". rewrite -(big_op.big_sepS_delete _ _ _ InVr).
        iSpecialize ("VSF" $! s' v V' with "[%]"); [done|].
        iMod ("VSF" with "[$Fp $P]") as "R"; [auto|].

        iMod (Reader_fork_Auth_2 _ _ {[s', (v, Vr)]} with "[$oA]") as "[oA #oR']".
          { by apply elem_of_subseteq_singleton. }

        iApply ("Post" $! s' false v Vr V' with "[- $Seen' $R $ofX $oR']").
        iFrame(H5 H10 VrV' VVr).
        iNext. iExists ζ, h, (s_x, (v_x, V_x)). subst h'.
        iFrame "oA oAX Hist' oBEI oAI". repeat (iSplit; first auto); auto.
  Qed.

  Lemma RawProto_CAS_non_ex_weak
    s_r s_x π (V V_r V_x: View) q (v_o v_n v_r v_x: Z) γ γ_x (E : coPset)
    (P : vPred) (Q: pr_state Prtcl → vPred) (R : pr_state Prtcl → Z → vPred)
    (VxVr : V_x !! l ⊑ V_r !! l) (VrV : V_r ⊑ V)
    (HEN : ↑physN ⊆ E) :
    {{{ ▷ (∀ s', ■ (s_r ⊑ s') ∗ F s' v_o ∗ P
                      ={E}=∗ ∃ s'', ■ (s' ⊑ s'')
                          ∗ Q s'' ∗ ▷ (F s'' v_n) ∗ ▷ F_past s'' v_n)%VP V
        ∗ (▷ (∀ s' v, ■ (s_r ⊑ s') ∗ F_past s' v ={E}=∗ F_past s' v ∗ F_past s' v))%VP ∅
        ∗ ▷ (∀ s' v, ■ (s_r ⊑ s') ∗ ■ (v ≠ v_o) ∗ ▷ F_past s' v ∗ P
                         ={E}=∗ R s' v)%VP V
        ∗ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ P V
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s_r, (v_r, V_r)]}
        ∗ (∃ ζ, Writer γ ζ) }}}
      (CAS #l #v_o #v_n, V) @ E
    {{{ s'' (b: bool) v Vr V', RET (LitV $ LitInt b, V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ⌜s_r ⊑ s''⌝
        ∗ ▷ (gps_inv IP l γ γ_x) ∗ (if b then Q s'' else R s'' v) V'
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s'', (v, Vr)]} ∗ ⌜Vr ⊑ V'⌝ ∗ ⌜V !! l ⊑ Vr !! l⌝
        ∗ (∃ ζ, Writer γ ζ) }}}.
  Proof.
    iIntros (Φ) "(VS & VSDup & VSF & #kI & kS & oI & P & ofX & #oR & oW) Post".

    set P': vPred := (P ∗ ☐ (∃ ζ , Writer γ ζ))%VP.
    set Q': pr_state Prtcl → vPred := λ s, (Q s  ∗ ☐ (∃ ζ , Writer γ ζ))%VP.
    set R': pr_state Prtcl → Z → vPred
      := λ s v, (R s v  ∗ ☐ (∃ ζ , Writer γ ζ))%VP.
    set T : pr_state Prtcl → pr_state Prtcl → vPred
      := λ s' s'', (Q s'' ∗ ▷ F s'' v_n ∗ ▷ F_past s'' v_n)%VP.

    iApply (RawProto_CAS_non_ex s_r s_x _ _ _ _ _ _ _ _ _ _ _ E E P' Q' R' T
      with "[$kI $kS $oI $ofX $oR VS VSDup VSF $P oW]");
    [done|done|done|done|..].
    { (* TODO: something wrong with vPred Frame *)
      iFrame "VSDup".
      iSplitL "VS"; last iSplitR "VSF oW"; last iSplitR "oW"; last done; iNext.
      - iIntros (s' V1) "#VV1 (Hsr & F & P & oW)".
        iMod ("VS" $! s' V1 with "VV1 [$Hsr $F $P]")
          as (s'') "(Hs' & Q & F & Fp)".
        iExists s''. by iFrame.
      - iIntros (? ? ?) "_ (_ & _ & (? & ? & ?) & ?)". by iFrame.
      - iIntros (s' v V1) "#VV1 (Hsr & Hv & Fp & P & oW)".
        iMod ("VSF" $! s' v V1 with "VV1 [$Hsr $Hv $Fp $P]") as "R".
        by iFrame. }

    iNext.
    iIntros (s'' b v Vr V') "(VV' & kS' & Hs'' & oI & IF & ofX & oR' & VrV & VVr)".
    iApply ("Post" $! s'' b v Vr V'
            with "[$VV' $kS' $Hs'' $oI $ofX $oR' $VrV $VVr IF]").
    destruct b; iDestruct "IF" as "[? ?]"; by iFrame.
  Qed.
End CAS.