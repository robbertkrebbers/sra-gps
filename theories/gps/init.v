From iris.program_logic Require Export weakestpre.
From igps.gps Require Import shared.
From igps.base Require Import na_write.

Section Init_Strong.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} 
          (IP : gname -> interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F γ := (IP γ true l).
  Notation F_past γ := (IP γ false l).

  Lemma RawProto_init_strong
    (s: gname -> pr_state Prtcl) v V h π (E : coPset)
      (HEN : ↑physN ⊆ E):
    {{{ PSCtx ∗ Seen π V
        ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝ }}}
      ([ #l ]_na <- #v, V) @ E
    {{{ V' γ γ_x, RET (#(), V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V'
         ∗ (▷ F γ (s γ) v V' ∗ ▷ F_past γ (s γ) v V' -∗ ▷ (gps_inv (IP γ) l γ γ_x))
         ∗ exwr γ_x 1%Qp ((s γ), (v, V'))
         ∗ Reader γ {[(s γ), (v, V')]}
         ∗ Writer γ {[(s γ), (v, V')]} }}}.
  Proof.
    iIntros (Φ) "(#kI & kS & oH & #Ha) Post".

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.

    iApply (f_write_na with "[$kI $kS $oH $Ha]").
    iNext.
    iIntros (V' h') "(% & kS' & oH' & % & % & % & % & %)".

    iMod (own_alloc (● (● ∅ ⋅ ◯ ∅) ⋅ ◯ (● ∅ ⋅ ◯ ∅))) as (γ) "oA".
      { apply auth_valid_discrete_2. split; first auto.
        by apply auth_valid_discrete_2. }

    set ζ : gset state_sort := {[(s γ), (v, V')]}.
    iMod (own_update γ _ (● (● ζ ⋅ ◯ ζ) ⋅ ◯ (● ζ ⋅ ◯ ζ)) with "oA") as "[oA oW]".
    { apply auth_update, auth_local_update; [|auto|done].
      by apply gset_local_update. }

    iMod (own_alloc (● Some (1%Qp, DecAgree ((s γ), (v, V')))
                        ⋅ ◯ Some (1%Qp, DecAgree ((s γ), (v, V')))))
         as (γ_x) "[exA ex]".
      { by apply auth_valid_discrete_2. }

    iDestruct (Writer_fork_Reader _ _ ζ with "oW") as "[oW #oR]"; first auto.
    iApply ("Post" with "[- $kS' $ex $oR $oW]"). iFrame (H).
    iIntros "[F Fp]".
    iExists ζ, h', ((s γ), (v, V')). iNext.
    iFrame (H2) "oA exA oH'".
    iSplitR ""; last first.
      { repeat iSplitL ""; iPureIntro.
        - by move => ? ? /elem_of_singleton -> /elem_of_singleton ->.
        - by apply elem_of_singleton.
        - by rewrite H4 /Consistent gset_map_singleton.
        - move => ? t /elem_of_singleton -> Le Ge Ne.
          exfalso.
          apply (Ne ((s γ), (v, V'))); first by apply elem_of_singleton.
          by apply: anti_symm.
        - by move => ? ? /elem_of_singleton -> /elem_of_singleton ->. }

    iSplitL "F".
    - rewrite /BE_Inv.
      set ΦF := λ e, F γ (st_prst e) (st_val e) (st_view e).
      rewrite (_: (F γ (s γ) v) V' = ΦF ((s γ), (v, V'))); last done.
      rewrite  -(big_op.big_sepS_singleton ΦF ((s γ), (v, V'))).
      iApply (big_op.big_sepS_mono ΦF ΦF with "F"); last by reflexivity.
      etrans; first by apply subseteq_gset_filter.
      by apply subseteq_gset_filter.
    - by rewrite /All_Inv big_op.big_sepS_singleton.
  Qed.
End Init_Strong.

Section Init.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF}
          (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F := (IP true l).
  Notation F_past := (IP false l).

  Lemma RawProto_init
      s v V h π (E : coPset)
      (HEN : ↑physN ⊆ E):
    {{{ PSCtx ∗ Seen π V
        ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝
        ∗ ▷ F s v V ∗ ▷ F_past s v V}}}
      ([ #l ]_na <- #v, V) @ E
    {{{ V' γ γ_x, RET (#(), V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V'
        ∗ ▷ (gps_inv IP l γ γ_x)
        ∗ exwr γ_x 1%Qp (s, (v, V'))
        ∗ Reader γ {[s, (v, V')]}
        ∗ Writer γ {[s, (v, V')]} }}}.
  Proof.
    iIntros (Φ) "(#kI & kS & oH & #Ha & Fp) Post".

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.
    iApply (RawProto_init_strong (λ _, IP) l (λ _, s)
              with "[$kI $kS $oH $Ha]");[done|].
    iNext. iIntros (V' γ γ_x) "(% & kS' & Inv & ?)".
    iApply ("Post" $! V' γ γ_x). iFrame (H) "∗ #". iApply "Inv".
    rewrite [V in (_ V ∗ _ V)%I]H. iModIntro. by iNext.
  Qed.
End Init.