From iris.algebra Require Export auth frac excl gmap gset local_updates.
From iris.program_logic Require Import ectx_lifting.
From iris.base_logic Require Import lib.sts big_op.

From igps Require Export viewpred tactics notation.
From igps Require Export blocks_generic blocks proofmode viewpred.
From igps.base Require Export ghosts.

Structure protocolT :=
   ProtocolT {
      pr_state : Type;
      pr_steps: relation pr_state;
    }.
Class protocol_facts (p : protocolT) :=
  {
      pr_steps_refl :> Reflexive (pr_steps p);
      pr_steps_trans :> Transitive (pr_steps p);
      pr_inhab :> Inhabited (pr_state p);
      pr_eqdec :> EqDecision (pr_state p);
      pr_count :> Countable (pr_state p);
  }.
Arguments pr_state _ : clear implicits.
Arguments pr_steps _ _ _ : clear implicits.

Notation interpC Σ Prtcl := (bool -c> loc -c> pr_state Prtcl -c> Z -c> @vPred_ofe Σ).

Notation state_sort Prtcl := (pr_state Prtcl * (Z * View))%type.
Notation state_type Prtcl := (gset (state_sort Prtcl)).

Section STS.
  Context {Σ : gFunctors} {fG : foundationG Σ} {Prtcl : protocolT} `{PF : !protocol_facts Prtcl}.
  Set Default Proof Using "Type*".

  Global Instance pr_Extension : Extension (pr_state Prtcl)
    := λ s_0 s_1, pr_steps _ s_0 s_1.
  (* Global Instance pr_PreOrder : PreOrder ((⊑) : relation (pr_state Prtcl)). *)
  (* Proof using PF. *)
  (*   econstructor. *)
  (*   - exact: pr_steps_refl. *)
  (*   - exact: pr_steps_trans. *)
  (* Qed. *)

  Notation stR := (gsetUR (state_sort Prtcl)).
  Definition stateR := authR (authUR stR).
  Global Instance gsetR_singleton `{Countable T} : Singleton T (gsetR T).
  Proof. unfold gsetR. intros x. unfold cmra_car. apply {[x]}. Defined.

  Global Instance stateR_IsOp (S1 S2 : stR) :
    @IsOp stateR (◯ (● S1 ⋅ ◯ S2)) (◯ (● S1)) (◯ (◯ S2)) := _.

  Global Instance stateR_IsOp_ (S1 S2 : stR) :
    @IsOp stateR (◯ (Auth (ExclBot') ∅)) (◯ (● S1)) (◯ (● S2)).
  Proof.
    unfold IsOp. rewrite -auth_frag_op. done.
  Qed.

  Definition st_prst : pr_state Prtcl * (Z * View) -> _ := fst.
  Definition st_val : pr_state Prtcl * (Z * View) -> _ := fst ∘ snd.
  Definition st_view : pr_state Prtcl * (Z * View) -> _ := snd ∘ snd.
  Definition st_time := λ x s, st_view s !! x.


  Global Instance st_time_adj_dec l :
    ∀ a b : state_sort Prtcl, Decision (rel_of (st_time l) adj_opt a b).
  Proof. move => a b /=.
    case: (st_time l a); last first.
    - right. move => [? [H ?]]. by inversion H.
    - case: (st_time _ _); last first.
      + right. move => [? [? H]]. by inversion H.
      + move => t2 t1. case (decide (t2 = t1 + 1)%positive) => [->|H].
        * left. by eexists.
        * right. move => [? [[?] [?]]]. by subst.
  Qed.

End STS.

Class gpsG Σ Prtcl (PF : protocol_facts Prtcl) :=
  {
    gps_inG :> inG Σ stateR;
    gps_ExWrG :> inG Σ (authR (optionUR (prodR fracR (dec_agreeR (state_sort Prtcl)))))
  }.
Definition gpsΣ Prtcl (PF : protocol_facts Prtcl) : gFunctors :=
  #[ GFunctor (constRF stateR); GFunctor (constRF (authR (optionUR (prodR fracR (dec_agreeR (state_sort Prtcl)))))) ].

Instance subG_gpsG {Σ} Prtcl PF : subG (gpsΣ Prtcl PF) Σ → gpsG Σ Prtcl PF.
Proof. solve_inG. Qed.

Arguments st_prst [_] _ /.
Arguments st_val [_] _ /.
Arguments st_view [_] _ /.
(* Arguments st_time [_] _ _ /. *)
(* Arguments sts.tok [_] _ /. *)
Arguments gpsG _ _ _ : clear implicits.
(* Arguments gpsG _ _ _ _ _ : clear implicits. *)
(* Arguments stateR _ _. *)


Section Setup.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View)
           (h : History).

  Notation F := (IP true l).
  Notation F_past := (IP false l).


  Section Assertions.

    Definition sBE ζ := block_ends (rel_of (st_time l) (adj_opt)) ζ.
    Definition sEx ζ (e_x: state_sort) : gset state_sort
      := {[ e <- ζ | st_view e_x !! l ⊑ st_view e !! l]}.

    Definition BE_Inv ζ e_x :=
      ([∗ set] b ∈ sEx (sBE ζ) e_x, F (st_prst b) (st_val b) (st_view b))%I.

    Definition All_Inv ζ :=
      ([∗ set] b ∈ ζ, F_past (st_prst b) (st_val b) (st_view b))%I.

    Definition ProtoInv ζ e_x := (BE_Inv ζ e_x ∗ All_Inv ζ)%I.

    Definition SortInv l ζ : Prop :=
      ∀ S1 S2,
        S1 ∈ ζ → S2 ∈ ζ →
        st_time l S1 ⊑ st_time l S2 → st_prst S1 ⊑ st_prst S2.

    Definition Consistent ζ h : Prop :=
      @equiv _ collection_equiv {[ (VInj (st_val e), st_view e) | e <- ζ]} h.

    Instance Consistent_proper:
      Proper (collection_equiv ==> collection_equiv ==> (flip impl) ) Consistent.
    Proof.
      unfold Consistent.
      move => X Y HXY h1 h2 Hh12 ?.
      rewrite -/(equiv _) in HXY. rewrite -/(equiv _) in Hh12. by rewrite -> HXY, Hh12.
    Qed.

    Lemma Consistent_insert ζ h s v V:
      Consistent ζ h → Consistent ({[s, (v, V)]} ∪ ζ) ({[VInj v, V]} ∪ h).
    Proof.
      rewrite /Consistent. move => <-.
      rewrite (_: {[VInj v, V]} =
                  {[(VInj (st_val e), st_view e) | e <- {[s, (v, V)]}]}).
        by rewrite -gset_map_union.
        apply set_unfold_2. split.
      - move => ->. by exists (s, (v, V)).
                              - by move => [? [-> ->]].
    Qed.

    Definition exwr γ_x p e := own γ_x (◯ (Some (p, DecAgree e))).

    Definition final_st s := ∀ s', s' ⊑ s.

    Definition FinalInv (e_x : state_sort) ζ :=
      ∀ e t, e ∈ ζ -> st_time l e_x ⊑ Some t -> Some t ⊑ st_time l e ->
             (∀ e', e' ∈ ζ -> st_time l e' ≠ Some t) -> final_st (st_prst e).

    Definition StateInjection ζ :=
      ∀ e1 e2, e1 ∈ ζ → e2 ∈ ζ →
               (VInj (st_val e1), st_view e1) = (VInj (st_val e2), st_view e2) ↔ e1 = e2.

    Definition gps_inv γ γ_x : iProp
      := (∃ ζ h e_x, own γ (● (● ζ ⋅ ◯ ζ))
                         ∗ own γ_x (● (Some (1%Qp, DecAgree e_x)))
                         ∗ Hist l h ∗ ProtoInv ζ e_x
                         ∗ ⌜init l h⌝ ∗ ⌜SortInv l ζ⌝ ∗ ⌜e_x ∈ ζ⌝
                         ∗ ⌜Consistent ζ h⌝ ∗ ⌜FinalInv e_x ζ⌝ ∗ ⌜StateInjection ζ⌝)%I.

    Notation gps_own := (λ γ aζ, @own _ _(gps_inG) γ (◯ aζ)).
    Definition Writer γ ζ : iProp := (gps_own γ (● ζ ⋅ ◯ ζ)).
    Definition Reader γ ζ : iProp := (gps_own γ (◯ ζ)).

    Definition maxTime V ζ : Prop := ∀ e, e ∈ ζ → (st_view e) !! l ⊑ V !! l.

    Arguments Writer _ _ /.
    Arguments Reader _ _ /.

    (* WIP: exposing state ghost name and observance *)
    Definition PrtSeen γ s := (∃ V_0 v, vSeen V_0 ∧ ☐ (Reader γ {[s, (v, V_0)]}))%VP.

    Global Instance PrtSeen_persistent γ s V :
      PersistentP (PrtSeen γ s V) := _.

    Lemma StateInjection_insert s v V h ζ
          (Cons: Consistent ζ h) (SI: StateInjection ζ) (HDisj: h ⊥ {[VInj v,V]})
      : StateInjection ({[s, (v, V)]} ∪ ζ).
    Proof.
      move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                 /elem_of_union [/elem_of_singleton ->| In2];
              [done|..|by apply SI].
      - split; last by move => <-.
        move => Eq. exfalso. apply disjoint_singleton_r in HDisj.
        apply HDisj. rewrite Eq -Cons elem_of_map_gset. by exists e2.
      - split; last move => -> //.
        move => Eq. exfalso. apply disjoint_singleton_r in HDisj.
        apply HDisj. rewrite -Eq -Cons elem_of_map_gset. by exists e1.
    Qed.

    Section Agreement.
      Lemma Writer_latest γ ζ1 ζ2
      : Reader γ ζ1 ∗ Writer γ ζ2
               ⊢ ⌜ζ1 ⊆ ζ2⌝.
      Proof.
        iIntros "(Reader & Writer)".
        iCombine "Writer" "Reader" as "oS".
        iDestruct (own_valid with "oS") as %[V1 V2].
        iPureIntro. move: (V1 O) => /=.
        rewrite gset_op_union /includedN.
        abstract set_solver+.
      Qed.

      Lemma Writer_Exclusive γ ζ1 ζ2
        : Writer γ ζ1 ∗ Writer γ ζ2 ⊢ False.
      Proof.
        iIntros "(Writer1 & Writer2)".
        iCombine "Writer1" "Writer2" as "oW".
          by iDestruct (own_valid with "oW") as %[].
      Qed.

      Instance Reader_persistent γ ζ: PersistentP (Reader γ ζ) := _.

      Lemma Reader_extract γ ζ1 ζ2 (Sub: ζ2 ⊆ ζ1) :
        Reader γ ζ1 ⊢ Reader γ ζ2.
      Proof.
        rewrite (union_difference_L _ _ Sub).
        rewrite (_: ζ2 ∪ ζ1 ∖ ζ2 = ζ2 ⋅ (ζ1 ∖ ζ2)) => //.
        rewrite /Reader !auth_frag_op.
        iIntros "[$ _]".
      Qed.


      Lemma Writer_to_Reader γ ζ:
        Writer γ ζ ⊢ Reader γ ζ.
      Proof. iIntros "[_ $]". Qed.

      Lemma Reader_fork_Auth γ ζ:
        own γ (● (● ζ ⋅ ◯ ζ)) ⊢ |==> own γ (● (● ζ ⋅ ◯ ζ)) ∗ Reader γ ζ.
      Proof.
        iIntros "oA".
        iMod (own_update _ _ (● (● ζ ⋅ ◯ ζ) ⋅ ◯ (◯ ζ)) with "[$oA]")
          as "[oA oR]"; last by iFrame.
        apply auth_update_alloc.
        move => n /=.
        move => [c|] [/= Le Val] [/= Ha Hb] /=;
        rewrite /op /cmra_op /= in Ha;
        rewrite ->!(left_id _ _) in Hb;
        rewrite ->(left_id _ _) in Le; last first.
        - repeat split => //=; by rewrite ->(left_id _ _).
        - destruct c as [ac bc]. rewrite -!auth_both_op. repeat split => //=.
          + apply union_subseteq_l.
          + simpl in Hb. rewrite -Hb. abstract set_solver+.
      Qed.

      Lemma Reader_fork_Auth_2 γ ζ ζ' (Sub: ζ' ⊆ ζ):
        own γ (● (● ζ ⋅ ◯ ζ)) ⊢ |==> own γ (● (● ζ ⋅ ◯ ζ)) ∗ Reader γ ζ'.
      Proof.
        iIntros "oA".
        iMod (Reader_fork_Auth with "[$oA]") as "[oA oR]".
        iModIntro. iFrame.
          by iApply (Reader_extract _ ζ).
      Qed.

      Lemma Writer_fork_Reader γ ζ ζ' (Sub: ζ' ⊆ ζ):
        Writer γ ζ ⊢ Writer γ ζ ∗ Reader γ ζ'.
      Proof.
        iIntros "Writer".
        iDestruct (Writer_to_Reader with "[$Writer]") as "#Reader".
        iFrame. by iApply (Reader_extract _ ζ).
      Qed.

      Lemma Writer_exact γ ζ ζ':
        own γ (● (● ζ' ⋅ ◯ ζ')) ∗ Writer γ ζ ⊢ ⌜ζ' = ζ⌝.
      Proof.
        iIntros "[oA oW]".
        iCombine "oA" "oW" as "oS".
        iDestruct (own_valid with "oS") as %[V1 _].
        iPureIntro.
        move: {V1} (V1 O) => /cmra_discrete_included_iff /=.
        rewrite auth_included; move => [/Excl_included /leibniz_equiv_iff Eq _].
          by subst.
      Qed.

      Lemma Writer_extract γ ζ:
        own γ (● (● ζ ⋅ ◯ ζ)) ∗ own γ (◯ (● ζ))
            ⊢ |==> own γ (● (● ζ ⋅ ◯ ζ)) ∗ Writer γ ζ.
      Proof.
        rewrite -2!own_op {2}(auth_both_op ζ ∅).
        apply own_update, auth_update, auth_local_update => //.
          by apply gset_local_update.
      Qed.

      Lemma Writer_update γ ζ ζ' (Sub: ζ ⊆ ζ'):
        own γ (● (● ζ ⋅ ◯ ζ)) ∗ Writer γ ζ
            ⊢ |==> own γ (● (● ζ' ⋅ ◯ ζ')) ∗ Writer γ ζ'.
      Proof.
        rewrite /Writer -2!own_op.
        apply own_update, auth_update, auth_local_update => //.
          by apply gset_local_update.
      Qed.

      Lemma Writer_update_2 γ ζ0 ζ ζ' (Sub: ζ ⊆ ζ'):
        own γ (● (● ζ0 ⋅ ◯ ζ0)) ∗ Writer γ ζ
            ⊢ |==> own γ (● (● ζ' ⋅ ◯ ζ')) ∗ Writer γ ζ'.
      Proof.
        iIntros "(oA & Writer)".
        iDestruct (Writer_exact with "[$oA $Writer]") as %Eq.
        rewrite Eq.
          by iApply (Writer_update with "[$oA $Writer]").
      Qed.

      Lemma Reader_Writer_sub γ ζ ζ':
        Writer γ ζ' ∗ Reader γ ζ ⊢ ⌜ζ ⊆ ζ'⌝.
      Proof.
        rewrite -own_op. iIntros "oA".
        iDestruct (own_valid with "oA") as %Valid%auth_valid_discrete.
        iPureIntro.
        move : Valid => /= /auth_valid_discrete /=.
        move => [/gset_included H _]. move: H.
        rewrite -> (left_id _ _). abstract set_solver+.
      Qed.

      Lemma Reader_Auth_sub γ ζ ζ':
        own γ (● (● ζ' ⋅ ◯ ζ')) ∗ Reader γ ζ ⊢ ⌜ζ ⊆ ζ'⌝.
      Proof.
        rewrite -own_op.
        iIntros "oA".
        iDestruct (own_valid with "oA") as %Valid%auth_valid_discrete.
        iPureIntro.
        move : Valid => /=.
        rewrite auth_included /= => [[[_ /gset_included Incl] _]].
        set_solver+Incl.
      Qed.

      Lemma Reader_singleton_agree γ γ_x s1 v1 V1 s2 v2 V2 E:
        ▷ gps_inv γ γ_x ∗
          Reader γ {[s1, (v1, V1)]} ∗ Reader γ {[s2, (v2, V2)]}
          ⊢ |={E}=> ▷ gps_inv γ γ_x ∗ ⌜s1 ⊑ s2 ∨ s2 ⊑ s1⌝.
      Proof.
        iIntros "(oI & #oR1 & #oR2)". unfold gps_inv.
        iDestruct "oI" as (ζ h e_x) "(>oA & ? & ? & ? & ? & >% & ?)".
        iDestruct (Reader_Auth_sub with "[$oA $oR1]")
          as %In1%elem_of_subseteq_singleton.
        iDestruct (Reader_Auth_sub with "[$oA $oR2]")
          as %In2%elem_of_subseteq_singleton.
        iModIntro. iSplitR "".
        - iExists ζ, h, e_x. iFrame. by iNext.
        - iPureIntro.
          case (decide (V1 !! l ⊑ V2 !! l)) => [Le|NLe].
          + left. apply (H _ _ In1 In2 Le).
          + right. apply (H _ _ In2 In1).
            rewrite /st_time /=. move : NLe.
            case: (V1 !! l); case : (V2 !! l) => //. cbn.
            move => t1 t2 /Pos.lt_nle NLe. by apply Pos.lt_le_incl.
      Qed.


      Lemma Writer_sorted γ γ_x ζ E:
        ▷ gps_inv γ γ_x ∗ Writer γ ζ
          ⊢ |={E}=> ▷ gps_inv γ γ_x ∗ Writer γ ζ ∗ ⌜SortInv l ζ⌝.
      Proof.
        iIntros "[oI oW]".
        iDestruct "oI" as (ζ' h e_x) "(>oA & oEx & Hist & oPr & ? & >% & ?)".
        iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.
        iFrame (H) "oW".
        iModIntro. iNext.
        iExists ζ, h, e_x. by iFrame.
      Qed.

      Lemma ExWrite_agree γ e1 e2 q1 q2:
        own γ (● Some (q1, DecAgree e1)) ∗ exwr γ q2 e2 ⊢ ⌜e1 = e2⌝.
      Proof.
        rewrite -own_op. iIntros "oX".
        iDestruct (own_valid with "oX")
          as %[oXIn%Some_included oX]%auth_valid_discrete_2.
          by move: oXIn => [[_ []]|/prod_included[_ /DecAgree_included]].
      Qed.

    End Agreement.
  End Assertions.
End Setup.
