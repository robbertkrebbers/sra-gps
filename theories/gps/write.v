From iris.program_logic Require Export weakestpre.
From igps.gps Require Import shared.
From igps.base Require Import at_write.

Section Write.
  Context {Σ} `{fG : !foundationG Σ} `{GPSG : !gpsG Σ Prtcl PF} (IP : interpC Σ Prtcl) (l : loc).
  Set Default Proof Using "Type".
  Notation state_sort := (pr_state Prtcl * (Z * View))%type.
  Implicit Types
           (s : pr_state Prtcl)
           (ζ : state_type Prtcl)
           (e : state_sort)
           (S : pr_state Prtcl * (Z * View))
           (v : Z)
           (V : View).

  Notation F := (IP true l).
  Notation F_past := (IP false l).

  Lemma RawProto_Write_non_ex
      s_r s_x s' π (V V_r V_x: View) q v v_r v_x γ γ_x (E : coPset) ζ
      (HEN : ↑physN ⊆ E)
      (VxVr : V_x !! l ⊑ V_r !! l) (VrV : V_r ⊑ V) :
    {{{ ⌜final_st s'⌝
        ∗ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x)
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s_r, (v_r, V_r)]}
        ∗ Writer γ ζ }}}
      ([ #l ]_at <- #v, V) @ E
    {{{ V', RET (#(), V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V'
        ∗ (▷ F s' v V' ∗ ▷ F_past s' v V' -∗ ▷ (gps_inv IP l γ γ_x))
        ∗ exwr γ_x q (s_x, (v_x, V_x))
        ∗ Reader γ {[s', (v, V')]}
        ∗ Writer γ ({[s', (v, V')]} ∪ ζ)
        }}}.
  Proof.
    iIntros (Φ) "(% & #kI & Seen & oI & ofX & #oR & oW) Post".
    iDestruct "oI" as (ζ' h e_x)
              "(>oA & >oAX & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".

    iDestruct (ExWrite_agree with "[$oAX $ofX]") as %?. subst e_x.
    iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.
    iDestruct (Reader_Writer_sub with "[$oW $oR]") as %?%elem_of_subseteq_singleton.

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.
    iApply (f_write_at with "[$kI $Seen $Hist]").
    - iPureIntro. exists (VInj v_r), V_r. repeat split => //.
      rewrite -H3 elem_of_map_gset. exists (s_r, (v_r, V_r)); split => //=.
    - iNext.
      iIntros (V' h') "(% & Seen' & Hist' & % & Pure)".
      iDestruct "Pure" as %(HInit & Hh' & HDisj & Hval).

      iMod (Writer_update _ _ ({[s', (v, V')]} ∪ ζ)
            with "[$oA $oW]") as "(oA' & oW')".
        { apply union_subseteq_r. }
      iDestruct (Writer_fork_Reader _ _ {[s', (v, V')]} with "[$oW']")
        as "[oW' #oR']".
        { apply union_subseteq_l. }

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist']") as "(Hist' & %)".
      iApply ("Post" $! V'). iFrame (H6) "Seen' oR' ofX oW'".

      iModIntro. iIntros "[F Fp]".
      iExists ({[s', (v, V')]} ∪ ζ), h', (s_x, (v_x, V_x)).
      iFrame "Hist' oA' oAX".

      assert ((s', (v, V')) ∉ ζ).
        { move => In.
          apply disjoint_singleton_r in HDisj.
          apply HDisj. rewrite -H3. apply elem_of_map_gset.
          by exists (s', (v, V')). }

      assert (HC': Consistent ({[s', (v, V')]} ∪ ζ) h').
        { unshelve eapply (Consistent_proper _ ({[s', (v, V')]} ∪ ζ) _ _ _ Hh');
            first done.
          by apply Consistent_insert. }

      iNext.

      assert (HV': (VInj v, V') ∈ h').
        { rewrite Hh' elem_of_union. left. by apply elem_of_singleton. }
      assert (HS': SortInv l ({[s', (v, V')]} ∪ ζ)).
        { move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                        /elem_of_union [/elem_of_singleton ->| In2] => //.
          - destruct H9 as [OK Disj].
            destruct (OK (VInj v,V') HV') as [t Eqt].
            move => Lt.
            apply (H4 _ t In2); rewrite -Eqt /st_time //=.
            + etrans; last by apply H6.
              etrans; last by apply VrV. done.
            + move => [s0 [v0 V0]] /= In0 Eqt2.
              assert ((VInj v0,V0) ∈ h).
                { rewrite -H3 elem_of_map_gset. by exists (s0, (v0, V0)). }
              assert (InV0: (VInj v0, V0) ∈ h').
                { rewrite Hh' elem_of_union. by right. }
              assert (EqvV := Disj _ _ InV0 HV' Eqt2).
              inversion EqvV. subst. by apply disjoint_singleton_r in HDisj.
          - by apply H1. }

      assert (HI: StateInjection ({[s', (v, V')]} ∪ ζ)).
        { move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                        /elem_of_union [/elem_of_singleton ->| In2]
            => //=; last by apply H5.
          - split; last move => <- //.
            move => Eq. exfalso. apply disjoint_singleton_r in HDisj.
            apply HDisj. rewrite Eq -H3 elem_of_map_gset. by exists e2.
          - split; last move => -> //.
            move => Eq. exfalso. apply disjoint_singleton_r in HDisj.
            apply HDisj. rewrite -Eq -H3 elem_of_map_gset. by exists e1. }

      assert (FinalInv l (s_x, (v_x, V_x)) ({[s', (v, V')]} ∪ ζ)).
        { move => e t /elem_of_union [/elem_of_singleton -> //= | ?] ? ? NEq.
          apply (H4 e t) => //.
          move => e' In'. apply (NEq e'), elem_of_union. right.
          by apply elem_of_singleton. }

      iFrame (HC' HS' H11 HI HInit).
      iSplit; last first.
        { iPureIntro. rewrite elem_of_union. by right. }

      iSplitL "F oBEI"; last first.
      + rewrite /All_Inv.
        rewrite (big_op.big_sepS_insert _ _ (s', (v, V'))) => //. by iFrame.
      + rewrite /BE_Inv.

        iCombine "F" "oBEI" as "oBEI".
        rewrite -(big_op.big_sepS_insert _ _ (s', (v, V'))); last first.
          { move => In.
            by apply subseteq_gset_filter, subseteq_gset_filter in In. }

        set BE':= sEx l (sBE l ({[s', (v, V')]} ∪ ζ)) (s_x, (v_x, V_x)).
        iApply (big_op.big_sepS_mono _ _ _ BE' with "[$oBEI]");
                last reflexivity.

        set P' := λ e, st_view (s_x, (v_x, V_x)) !! l ⊑ st_view e !! l.

        rewrite <- (gset_filter_singleton (s', (v, V')) (P:= P'));
          [| rewrite /P'; do 2 (etrans; eauto)].
        rewrite -gset_filter_union.
        apply gset_filter_subseteq.

        set fm := λ (e: state_sort), (VInj $ st_val e, st_view e).
        apply (gset_map_subseteq_inj fm).
        { move => ? ? Hin1 Hin2.
          apply HI;
          [abstract set_solver+Hin1 by auto
            |abstract set_solver+Hin2 by auto]. }

        rewrite gset_map_union.
        rewrite gset_map_singleton.
        do 2!(rewrite -block_ends_fmap; [|done|done]).
        rewrite /Consistent in HC' H3.
        rewrite H3 HC' Hh' /fm /=.
        by apply block_ends_ins_mono.
  Qed.

  Lemma RawProto_Write_ex_strong s_x s' π V v_x V_x γ γ_x v (E : coPset) ζ
        (HEN : ↑physN ⊆ E)
        (VxV : V_x ⊑ V) (Le: s_x ⊑ s')
        (Max: maxTime l V_x ζ):
    {{{ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x)
        ∗ exwr γ_x 1%Qp (s_x, (v_x, V_x))
        ∗ Reader γ {[s_x, (v_x, V_x)]}
        ∗ Writer γ ζ }}}
      ([ #l ]_at <- #v, V) @ E
    {{{ V', RET (#(), V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V'
         ∗ (▷ F s' v V' ∗ ▷ F_past s' v V' -∗ ▷ (gps_inv IP l γ γ_x))
         ∗ F s_x v_x V'
         ∗ exwr γ_x 1%Qp (s', (v, V'))
         ∗ Reader γ {[s', (v, V')]}
         ∗ Writer γ ({[s', (v, V')]} ∪ ζ)
         ∗ ⌜maxTime l V' ({[s', (v, V')]} ∪ ζ)⌝

    }}}.
  Proof.
    iIntros (Φ) "(#kI & kS & oI & ofX & #oR & oW) Post".
    iDestruct "oI" as (ζ' h e_x)
              "(>oA & >oAX & Hist & [oBEI oAI] & >% & >% & >% & >% & >% & >%)".

    iDestruct (ExWrite_agree with "[$oAX $ofX]") as %?. subst e_x.
    iDestruct (Writer_exact with "[$oA $oW]") as %?. subst ζ'.
    iDestruct (Reader_Writer_sub with "[$oW $oR]") as %?%elem_of_subseteq_singleton.

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.
    iApply (f_write_at with "[$kI $kS $Hist]").
    - iPureIntro. exists (VInj v_x), V_x. repeat split; [|auto|auto].
      rewrite -H2 elem_of_map_gset. by exists (s_x, (v_x, V_x)).
    - iNext.
      iIntros (V' h') "(% & kS' & Hist' & % & Pure)".
      iDestruct "Pure" as %(HInit & Hh' & HDisj & Hval).

      iMod (Writer_update _ _ ({[s', (v, V')]} ∪ ζ)
            with "[$oA $oW]") as "(oA' & oW')".
        { apply union_subseteq_r. }
      iDestruct (Writer_fork_Reader _ _ {[s', (v, V')]} with "[$oW']")
        as "[oW' #oR']".
        { apply union_subseteq_l. }

      iCombine "oAX" "ofX" as "oA".
      iMod (own_update _ _
                (● Some (1%Qp, DecAgree (s', (v, V')))
                   ⋅ ◯ Some (1%Qp, DecAgree (s', (v, V'))))
           with "[$oA]") as "[oAX' ofX']".
        { by apply auth_update, option_local_update, exclusive_local_update. }

      assert (Heb: (s_x, (v_x, V_x)) ∈ sEx l (sBE l ζ) (s_x, (v_x, V_x))).
        { rewrite !elem_of_filter. repeat split; auto.
          move => e' /Max /= Le' Ne' [t [Eq Eq1]].
          rewrite /st_time /= in Eq, Eq1.
          rewrite Eq Eq1 in Le'. cbn in Le'.
          apply (Pos.lt_irrefl t).
          eapply (Pos.lt_le_trans); last exact Le'.
          by apply Pos.lt_add_r. }

      rewrite /BE_Inv (big_op.big_sepS_delete _ _ _ Heb).
      iDestruct "oBEI" as "[F oBEI]".


      iDestruct (vPred_mono _ V' with "F") as "F"; first by etrans.

      iMod (Hist_hTime_ok _ _ _ HEN with "[$kI $Hist']") as "(Hist' & %)".
      iApply ("Post" $! V'). iFrame (H5) "kS' F ofX' oR' oW'".

      iSplitL; last first.
      { iPureIntro.
        move => e. rewrite elem_of_union elem_of_singleton.
        move => [-> //|Ine]. etrans; last by apply H5.
        etrans; last by apply VxV. by apply Max. }

      iModIntro. iIntros "[F Fp]".
      iExists ({[s', (v, V')]} ∪ ζ), h', (s', (v, V')).
      iFrame "Hist' oA' oAX'".
      iNext.

      assert ((s', (v, V')) ∉ ζ).
        { move => In.
          apply disjoint_singleton_r in HDisj.
          apply HDisj. rewrite -H2. apply elem_of_map_gset.
          by exists (s', (v, V')). }

      assert (HC': Consistent ({[s', (v, V')]} ∪ ζ) h').
        { unshelve eapply (Consistent_proper _ ({[s', (v, V')]} ∪ ζ) _ _ _ Hh');
            first done.
          by apply Consistent_insert. }

      assert (HV': (VInj v, V') ∈ h').
        { rewrite Hh' elem_of_union. left. by apply elem_of_singleton. }
      assert (HS': SortInv l ({[s', (v, V')]} ∪ ζ)).
        { move => e1 e2 /elem_of_union [/elem_of_singleton ->| In1]
                        /elem_of_union [/elem_of_singleton ->| In2] => //.
          - destruct H8 as [OK Disj].
            destruct (OK (VInj v,V') HV') as [t Eqt].
            move => Lt.
            apply (H3 _ t In2); rewrite -Eqt /st_time //=.
            + etrans; last by apply H5. done.
            + move => [s0 [v0 V0]] /= In0 Eqt2.
              assert ((VInj v0,V0) ∈ h).
                { rewrite -H2 elem_of_map_gset. by exists (s0, (v0, V0)). }
              assert (InV0: (VInj v0, V0) ∈ h').
                { rewrite Hh' elem_of_union. by right. }
              assert (EqvV := Disj _ _ InV0 HV' Eqt2).
              inversion EqvV. subst. by apply disjoint_singleton_r in HDisj.
          - move => Lt. etrans; last by apply Le.
            by apply (H0 _ _ In1 H6), Max.
          - by apply H0. }

      assert (HI: StateInjection ({[s', (v, V')]} ∪ ζ)).
        { by apply (StateInjection_insert _ _ _ h). }

      assert (FinalInv l (s', (v, V')) ({[s', (v, V')]} ∪ ζ)).
        { move => e t /elem_of_union Ine /=  ? Lee NEq.
          exfalso. apply (NEq (s', (v, V'))).
          - rewrite elem_of_union elem_of_singleton. by left.
          - move : Ine => [/elem_of_singleton ?|Ine].
            + subst e. by apply: anti_symm.
            + apply: anti_symm; first by etrans; eauto.
              etrans; first by apply Lee.
              etrans; last by apply H5.
              etrans; last by apply VxV. by apply Max. }

      iFrame (HC' HS' HI H10 HInit).
      iSplitR ""; last first.
        { iPureIntro. by apply elem_of_union_l, elem_of_singleton. }

      iSplitL "F oBEI"; last first.
      + rewrite /All_Inv.
        rewrite (big_op.big_sepS_insert _ _ (s', (v, V'))); last auto. by iFrame.
      + rewrite /BE_Inv.
        iCombine "F" "oBEI" as "oBEI".
        rewrite -(big_op.big_sepS_insert _ _ (s', (v, V'))); last first.
          { move => In.
            by apply gset_difference_subseteq,
                      subseteq_gset_filter, subseteq_gset_filter in In. }

        set BE':= sEx l (sBE l ({[s', (v, V')]} ∪ ζ)) (s', (v, V')).
        iApply (big_op.big_sepS_mono _ _ _ BE' with "[$oBEI]");
                last reflexivity.

        etrans; last apply union_subseteq_l.
        rewrite /BE'.
        move => e. rewrite elem_of_filter elem_of_singleton.

        move => [Lte Ine].
        apply elem_of_filter in Ine as [NAdj Ine].
        apply elem_of_union in Ine as [Eqe|Ine].
        * by apply elem_of_singleton_1 in Eqe.
        * apply HI; [by apply elem_of_union_r
                      | by apply elem_of_union_l, elem_of_singleton |].
          simpl. apply H8; [|auto|].
          { rewrite -HC' elem_of_map_gset.
            exists e. split; [auto|by apply elem_of_union_r]. }
          { apply: anti_symm; last by apply Lte.
            etrans; last by apply H5. etrans; last by apply VxV.
            by apply Max. }
  Qed.

  Lemma RawProto_Write_ex s_x s' π V v_x V_x γ γ_x v (E : coPset) ζ
        (HEN : ↑physN ⊆ E)
        (VxV : V_x ⊑ V) (Le: s_x ⊑ s')
        (Max: maxTime l V_x ζ):
    {{{ PSCtx ∗ Seen π V
        ∗ ▷ (gps_inv IP l γ γ_x)
        ∗ exwr γ_x 1%Qp (s_x, (v_x, V_x))
        ∗ Reader γ {[s_x, (v_x, V_x)]}
        ∗ Writer γ ζ }}}
      ([ #l ]_at <- #v, V) @ E
    {{{ V', RET (#(), V') ;
        ⌜V ⊑ V'⌝ ∗ Seen π V'
        ∗ (▷ F s' v V' ∗ ▷ F_past s' v V' -∗ ▷ (gps_inv IP l γ γ_x))
        ∗ F s_x v_x V'
        ∗ exwr γ_x 1%Qp (s', (v, V'))
        ∗ Reader γ {[s', (v, V')]}
        ∗ Writer γ ({[s', (v, V')]} ∪ ζ)
        ∗ ⌜maxTime l V' ({[s', (v, V')]} ∪ ζ)⌝ }}}.
  Proof.
    iIntros (Φ) "(#kI & kS & oI & ofX & #oR & oW) Post".
    iApply (RawProto_Write_ex_strong s_x s' _ _ v_x V_x γ γ_x _ _ _
            with "[$kI $kS $oI $ofX $oR $oW]"); [auto|auto|auto|auto|..].
    iNext. iIntros (V') "(VV' & kS' & oI & Q & ofX' & oR' & oW' & Max)".
    iApply ("Post" $! V' with "[$VV' $kS' $oI $Q $ofX' $oR' $oW' $Max]").
  Qed.
End Write.