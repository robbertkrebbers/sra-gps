From iris.base_logic.lib Require Import own invariants.
From iris.algebra Require Import agree.
From stdpp Require Import gmap.
From iris.proofmode Require Import tactics.

From igps Require Import viewpred proofmode.
From igps.base Require Import ghosts.
From igps Require Export abs_view.

Definition escrowN : namespace := nroot .@ "escrow".

Section Escrows.

  Context `{!foundationG Σ, !absViewG Σ}.
  Set Default Proof Using "Type".
  Local Notation iProp := (iProp Σ).

  Definition exchange_aux (P Q: vPred) β (V: View) : iProp :=
    (∃ V0, ⌜V0 ⊑ V⌝ ∗ absView β V0 ∗ inv escrowN (P V0 ∨ Q V0))%I.

  Lemma exchange_aux_mono P Q β:
    vPred_Mono (exchange_aux P Q β).
  Proof.
    move => V1 V2 Le.
    iIntros "Ex". iDestruct "Ex" as (V0) "(% & ? & ?)".
    iExists V0. iFrame. iPureIntro. by etrans.
  Qed.

  Canonical Structure vPred_exchange P Q β
    := Build_vPred _ (exchange_aux_mono P Q β).

  Global Instance vPred_exchange_proper :
    Proper ((≡) ==> (≡) ==> (=) ==> (≡)) (vPred_exchange).
  Proof.
    move => ?? H ?? H1 ?? H2 ?. subst. rewrite /= /exchange_aux.
    apply uPred.exist_proper => ?. do 4!f_equiv; auto.
  Qed.

  Global Instance exchange_persistent P Q β V :
    PersistentP (vPred_exchange P Q β V) := _.

  Definition vPred_escrow P Q : vPred :=
    ∃ β, vPred_exchange (vPred_obj P) Q β.

  Global Instance vPred_escrow_proper : Proper ((≡) ==> (≡) ==> (≡)) vPred_escrow.
  Proof. intros ?? H ?? H2 ?. apply vPred_exists_proper => ?. by rewrite H H2. Qed.

  Global Instance absView_ne:
    Proper (dist n ==> dist n ==> dist n) absView.
  Proof. solve_proper. Qed.

End Escrows.

(* "↭"	U21AD	 	# LEFT RIGHT WAVE ARROW *)
Notation "[ex P ↭ Q ]" := (vPred_exchange P Q) 
  (at level 99, format "[ex  P  ↭  Q ]"): vPred_scope.

Notation "[es P ⇝ Q ]" := (vPred_escrow P Q) 
  (at level 99, format "[es  P  ⇝  Q ]"): vPred_scope.

Section proof.

  Context `{!foundationG Σ, !absViewG Σ}.

  Set Default Proof Using "Type".

  Lemma exchange_aSeen P Q β:
    [ex P ↭ Q] β ⊧ aSeen β.
  Proof.
    constructor. intros.
    iIntros "ex". iDestruct "ex" as (?) "(? & ? & _)".
    iExists _. by iFrame.
  Qed.

  Lemma exchange_alloc_l P Q:
    (▷ P ={↑escrowN}=> ∃ β, [ex P ↭ Q] β)%VP.
  Proof.
    constructor => V V_l HV.
    iIntros "P".
    iMod (absView_alloc V_l) as (β) "?".
    iExists β, V_l.
    iMod (inv_alloc escrowN (↑escrowN) (P V_l ∨ Q V_l)%I with "[P]") as "?".
      { iNext. by iLeft. }
    by iFrame.
  Qed.

  Lemma exchange_alloc_r P Q:
    (▷ Q ={↑escrowN}=> ∃ β, [ex P ↭ Q] β)%VP.
  Proof.
    constructor => V V_l HV.
    iIntros "Q".
    iMod (absView_alloc V_l) as (β) "?".
    iExists β, V_l.
    iMod (inv_alloc escrowN (↑ escrowN) (P V_l ∨ Q V_l)%I with "[Q]") as "?".
    { iNext. by iRight. }
    by iFrame.
  Qed.

  Lemma exchange_elim_l P Q β:
    (P ∗ P → False)%VP ⊩ ([ex P ↭ Q] β ∗ ▷ ⌞P⌟ β ={↑escrowN}=∗ ▷ ⌞Q⌟ β)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(#Ex & P)".
    iDestruct "Ex" as (V1) "(% & abs1 & ?)".
    iDestruct "P" as (V2) "(abs2 & P)".
    iAssert (▷ P V1)%I with "[P abs2]" as "P".
     { iNext. iDestruct (absView_agree with "abs2 abs1") as %?.
       by subst V2. }
    iInv escrowN as "Inv" "HClose".
    iAssert (▷ Q V1 ∗ ▷ (P V1 ∨ Q V1))%I with "[P Inv ND]" as "[Q Inv]".
      { iNext. iDestruct "Inv" as "[Inv|$]".
        - iExFalso. by iApply ("ND" with "[%] [$P $Inv]").
        - by iLeft. }
    iMod ("HClose" with "Inv").
    iModIntro. iExists _. iFrame. by iNext.
  Qed.

  Lemma exchange_left P Q β:
    (P ∗ P → False)%VP ⊩ ([ex P ↭ Q] β ∗ ▷ ⌞P⌟ β ={↑escrowN}=∗ ▷ Q)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(#Ex & P)".
    destruct (exchange_aSeen P Q β) as [ES].
    iDestruct (ES with "Ex") as "aSeen"; first done.
    iDestruct (exchange_elim_l with "ND [%] [$Ex $P]") as ">Q"; first done.
    destruct (aSeen_elim Q β) as [ES2].
    iModIntro. iNext.
    by iDestruct (ES2 with "[$aSeen $Q]") as "$".
  Qed.

  Lemma exchange_elim_r P Q β:
    (Q ∗ Q → False)%VP ⊩ ([ex P ↭ Q] β ∗ ▷ ⌞Q⌟ β ={↑escrowN}=∗ ▷ ⌞P⌟ β)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(#Ex & Q)".
    iDestruct "Ex" as (V1) "(% & abs1 & ?)".
    iDestruct "Q" as (V2) "(abs2 & Q)".
    iAssert (▷ Q V1)%I with "[Q abs2]" as "Q".
      { iNext. iDestruct (absView_agree with "abs2 abs1") as %?.
        by subst V2. }
    iInv escrowN as "Inv" "HClose".
    iAssert (▷ P V1 ∗ ▷ (P V1 ∨ Q V1))%I with "[Q Inv ND]" as "[P Inv]".
      { iNext. iDestruct "Inv" as "[$|Inv]".
        - by iRight.
        - iExFalso. by iApply ("ND" with "[%] [$Q $Inv]"). }
    iMod ("HClose" with "Inv").
    iModIntro. iExists _. iFrame. by iNext.
  Qed.

  Lemma exchange_right P Q β:
    (Q ∗ Q → False)%VP ⊩ ([ex P ↭ Q] β ∗ ▷ ⌞Q⌟ β ={↑escrowN}=∗ ▷ P)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(#Ex & Q)".
    destruct (exchange_aSeen P Q β) as [ES].
    iDestruct (ES with "Ex") as "aSeen"; first done.
    iDestruct (exchange_elim_r with "ND [%] [$Ex $Q]") as ">P"; first done.
    destruct (aSeen_elim P β) as [ES2].
    iModIntro. iNext.
    by iDestruct (ES2 with "[$aSeen $P]") as "$".
  Qed.

  Lemma escrow_alloc P Q:
    (▷ Q ={↑escrowN}=> [es P ⇝ Q])%VP.
  Proof. by apply exchange_alloc_r. Qed.

  Lemma escrow_apply_obj P Q:
    (P ∗ P → False)%VP ⊩ ([es P ⇝ Q] ∗ ▷ ▲P ={↑escrowN}=∗ ▷ Q)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(Esc & oP)".
    iDestruct "Esc" as (β V') "(% & abs1 & ?)".
    iInv escrowN as "Inv" "HClose".
    iAssert (▷ Q V' ∗ ▷ ((▲P)%VP V' ∨ Q V'))%I with "[ND oP Inv]" as "[Q Inv]".
      { iNext. iDestruct "Inv" as "[Inv|$]".
        - iExFalso.
          iDestruct "oP" as (V1) "P1". 
          iDestruct "Inv" as (V2) "P2".
          iApply ("ND" $! _ (V1 ⊔ V2) with "[]"); first done.
          iSplitL "P1"; iApply vPred_mono; eauto; solve_jsl.
        - by iLeft. }
    iMod ("HClose" with "Inv").
    iModIntro. iNext. by iApply (vPred_mono V').
  Qed.

  Lemma escrow_apply P Q:
    (P ∗ P → False)%VP ⊩ ([es P ⇝ Q] ∗ ▷ P ={↑escrowN}=∗ ▷ Q)%VP.
  Proof.
    iIntros "ND".
    iIntros (V0 V Le) "(Esc & oP)".
    iDestruct (escrow_apply_obj with "ND") as "AP".
    iApply ("AP" with "[]"); first done.
    iFrame. iExists V; auto.
  Qed.

End proof.
