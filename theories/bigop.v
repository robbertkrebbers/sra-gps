From iris.base_logic Require Import big_op lib.iprop lib.fractional.
From iris.proofmode Require Import tactics.
From igps Require Import viewpred proofmode.


Program Definition Pos2Qp (n: positive) : Qp := mk_Qp (Zpos n) _.
Next Obligation. intros n. rewrite /Qcanon.Qclt /QArith_base.Qlt /=. lia. Qed.

Lemma Pos2Qp_1: Pos2Qp 1 = 1%Qp.
Proof. by apply Qp_eq, Z2Qc_inj_1. Qed.

Lemma Pos2Qp_succ n : 
  Pos2Qp (Pos.succ n) = (Pos2Qp n + 1)%Qp.
Proof. apply Qp_eq. rewrite /= -Z2Qc_inj_1 -Z2Qc_inj_add. f_equal. lia. Qed.

Lemma Pos2Qp_plus_mult (n: positive) :
  (1 = 1/(n+1) + (Pos2Qp n * (1/(n+1))))%Qp.
Proof.
  rewrite -{1}(Qp_mult_1_l (1/(n+1))) -Qp_mult_plus_distr_l.
  apply Qp_eq. 
  rewrite /= (_: Qcanon.Qcplus 1 (Qc_of_Z (Z.pos n)) = Z.pos (n + 1))%Qc.
  - rewrite Qcanon.Qcmult_div_r; done.
  - rewrite -Z2Qc_inj_1 -Z2Qc_inj_add. f_equal. lia.
Qed.

Lemma Pos2Qp_1_div n:
  (1 = (Pos2Qp n) * (1/n))%Qp.
Proof. apply Qp_eq. rewrite /= Qcanon.Qcmult_div_r; done. Qed.

Lemma big_sepL_mult_fractional
  {M : ucmraT} {Φ : Qp → uPred M} `{!Fractional Φ} (j: nat) q (Pj: 0 < j):
  Φ ((Pos2Qp $ Pos.of_nat j) * q)%Qp ⊣⊢ [∗ list] _ ∈ seq 0 j, Φ q.
Proof.
  induction j; first omega.
  rewrite /= -fmap_seq.
  destruct j as [|j].
  - by rewrite big_sepL_nil Pos2Qp_1 Qp_mult_1_l uPred.sep_True.
  - rewrite Nat2Pos.inj_succ; last omega.
    rewrite Pos2Qp_succ Qp_mult_plus_distr_l Qp_mult_1_l.
    rewrite fractional uPred.sep_comm.
    rewrite big_sepL_fmap IHj; [auto|omega].
Qed.

Section BigOp.
  Context {Σ : gFunctors}.
  Notation iProp := (iProp Σ).
  Notation vPred := (@vPred Σ).
  Implicit Type (V : View).
  Set Default Proof Using "Type*".

  Lemma big_opL_vmono A (l: list A) (Ps : nat → A → vPred) :
    vPred_Mono (CofeMor (λ V, big_opL uPred_sep (fun n a => Ps n a V) l)).
  Proof.
    iIntros (?) "". iView. iIntros "P".
    iApply (big_sepL_impl with "[$P]").
    iIntros "!# *". rewrite HV. auto.
  Qed.

  Canonical Structure vPred_big_opL {A} l Ps
    := Build_vPred _ (big_opL_vmono A l Ps).

  Lemma big_opS_vmono (A: Type) `{Countable A} (M: gset A) (Ps : (A -> vPred)) :
    vPred_Mono (CofeMor (λ V, big_opS uPred_sep (fun m => Ps m V) M)).
  Proof.
    iIntros (?) "". iView. iIntros "P".
    iApply (big_sepS_impl with "[$P]").
    iIntros "!# *". rewrite HV. auto.
  Qed.

  Canonical Structure vPred_big_opS {A: Type} `{Countable A} M Ps
    := Build_vPred _ (big_opS_vmono A M Ps).

  Lemma vPred_big_opL_fold {A} l (Ps : nat → A → vPred) V:
    big_opL uPred_sep (fun n a => Ps n a V) l ≡ vPred_big_opL l Ps V.
  Proof. by rewrite /vPred_big_opL {2}/vPred_app /=. Qed.

  Lemma vPred_big_opS_fold {A: Type} `{Countable A} M (Ps : A → vPred) V:
    big_opS uPred_sep (fun m => Ps m V) M ≡ vPred_big_opS M Ps V.
  Proof. by rewrite /vPred_big_opS {2}/vPred_app /=. Qed.

End BigOp.

(* Notations *)
(* Notation "'[∗]' Ps" := (big_op (M:=uPredUR _) Ps) (at level 20) : uPred_scope. *)

Notation "'[∗' 'list' ] k ↦ x ∈ l , P" := (vPred_big_opL l (λ k x, P))
  (at level 200, l at level 10, k, x at level 1, right associativity,
   format "[∗  list ]  k ↦ x  ∈  l ,  P") : vPred_scope.
Notation "'[∗' 'list' ] x ∈ l , P" := (vPred_big_opL l (λ _ x, P))
  (at level 200, l at level 10, x at level 1, right associativity,
   format "[∗  list ]  x  ∈  l ,  P") : vPred_scope.

(* Notation "'[∗' 'map' ] k ↦ x ∈ m , P" := (big_opM (M:=uPredUR _) m (λ k x, P)) *)
(*   (at level 200, m at level 10, k, x at level 1, right associativity, *)
(*    format "[∗  map ]  k ↦ x  ∈  m ,  P") : uPred_scope. *)
(* Notation "'[∗' 'map' ] x ∈ m , P" := (big_opM (M:=uPredUR _) m (λ _ x, P)) *)
(*   (at level 200, m at level 10, x at level 1, right associativity, *)
(*    format "[∗  map ]  x  ∈  m ,  P") : uPred_scope. *)

Notation "'[∗' 'set' ] x ∈ X , P" := (vPred_big_opS X (λ x, P))
  (at level 200, X at level 10, x at level 1, right associativity,
   format "[∗  set ]  x  ∈  X ,  P") : vPred_scope.

(* Notation "'[∗' 'mset' ] x ∈ X , P" := (big_opMS (M:=uPredUR _) X (λ x, P)) *)
(*   (at level 200, X at level 10, x at level 1, right associativity, *)
(*    format "[∗  mset ]  x  ∈  X ,  P") : uPred_scope. *)