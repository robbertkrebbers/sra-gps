From igps Require Export lang.
From stdpp Require Import fin_maps.
Import base ra_lang.

(** We define an alternative representation of expressions in which the
embedding of values and closed0 expressions is explicit. By reification of
expressions into this type we can implementation substitution, closed0ness
checking, atomic checking, and conversion into values, by computation. *)
Module W.
Inductive expr0 :=
  | Val (v : base.val)
  | ClosedExpr (e : base.expr) `{!base.Closed [] e}
  (* Base lambda calculus *)
  | Var (x : string)
  | Rec (f x : binder) (e : expr0)
  | App (e1 e2 : expr0)
  (* Base types and their operations *)
  | Lit (l : base_lit)
  | UnOp (op : un_op) (e : expr0)
  | BinOp (op : bin_op) (e1 e2 : expr0)
  | If (e0 e1 e2 : expr0)
  (* Concurrency *)
  | Fork (e : expr0)
  (* Heap *)
  | Alloc
  | Dealloc (e: expr0)
  | Load (acc : access) (e : expr0)
  | Store (acc : access) (e1 : expr0) (e2 : expr0)
  | CAS (e0 : expr0) (e1 : expr0) (e2 : expr0)
  | FAI (C: positive) (e: expr0).

Definition expr : Type := expr0 * View.

Fixpoint to_expr' (e : expr0) : base.expr :=
  match e return base.expr with
  | Val v => base.of_val v
  | ClosedExpr e _ => e
  | Var x => base.Var x
  | Rec f x e => base.Rec f x (to_expr' e)
  | App e1 e2 => base.App (to_expr' e1) (to_expr' e2)
  | Lit l => base.Lit l
  | UnOp op e => base.UnOp op (to_expr' e)
  | BinOp op e1 e2 => base.BinOp op (to_expr' e1) (to_expr' e2)
  | If e0 e1 e2 => base.If (to_expr' e0) (to_expr' e1) (to_expr' e2)
  | Fork e => base.Fork (to_expr' e)
  | Alloc => base.Alloc
  | Dealloc e => base.Dealloc (to_expr' e)
  | Load acc e => base.Load acc (to_expr' e)
  | Store acc e1 e2 => base.Store acc (to_expr' e1) (to_expr' e2)
  | CAS e0 e1 e2 => base.CAS (to_expr' e0) (to_expr' e1) (to_expr' e2)
  | FAI C e => base.FAI C (to_expr' e)
  end.

Definition to_expr (e : expr) : ra_lang.expr := (to_expr' (e.1), e.2).

Fixpoint to_expr0 (e : base.expr) : expr0 :=
  match e with
  | base.Var x => Var x
  | base.Rec f x e => Rec f x (to_expr0 e)
  | base.App e1 e2 => App (to_expr0 e1) (to_expr0 e2)
  | base.Lit l => Lit l
  | base.UnOp op e => UnOp op (to_expr0 e)
  | base.BinOp op e1 e2 => BinOp op (to_expr0 e1) (to_expr0 e2)
  | base.If e0 e1 e2 => If (to_expr0 e0) (to_expr0 e1) (to_expr0 e2)
  | base.Fork e => Fork (to_expr0 e)
  | base.Alloc => Alloc
  | base.Dealloc e => Dealloc (to_expr0 e)
  | base.Load acc e => Load acc (to_expr0 e)
  | base.Store acc e1 e2 => Store acc (to_expr0 e1) (to_expr0 e2)
  | base.CAS e0 e1 e2 => CAS (to_expr0 e0) (to_expr0 e1) (to_expr0 e2)
  | base.FAI C e => FAI C (to_expr0 e)
  end.

Lemma to_expr_eq e: to_expr' (to_expr0 e) = e.
Proof.
  induction e => //=; try (rewrite IHe) => //;
    try (rewrite IHe1 IHe2) => //; by rewrite IHe3.
Qed.

Ltac of_expr0 e :=
  lazymatch e with
  | base.Var ?x => constr:(Var x)
  | base.Rec ?f ?x ?e => let e := of_expr0 e in constr:(Rec f x e)
  | base.App ?e1 ?e2 =>
     let e1 := of_expr0 e1 in let e2 := of_expr0 e2 in constr:(App e1 e2)
  | base.Lit ?l => constr:(Lit l)
  | base.UnOp ?op ?e => let e := of_expr0 e in constr:(UnOp op e)
  | base.BinOp ?op ?e1 ?e2 =>
     let e1 := of_expr0 e1 in let e2 := of_expr0 e2 in constr:(BinOp op e1 e2)
  | base.If ?e0 ?e1 ?e2 =>
     let e0 := of_expr0 e0 in let e1 := of_expr0 e1 in let e2 := of_expr0 e2 in
     constr:(If e0 e1 e2)
  | base.Alloc => constr:(Alloc)
  | base.Dealloc ?e => let e := of_expr0 e in constr:(Dealloc e)
  | base.Fork ?e => let e := of_expr0 e in constr:(Fork e)
  | base.Load ?acc ?e => let e := of_expr0 e in constr:(Load acc e)
  | base.Store ?acc ?e1 ?e2 =>
     let e1 := of_expr0 e1 in let e2 := of_expr0 e2 in constr:(Store acc e1 e2)
  | base.CAS ?e0 ?e1 ?e2 =>
     let e0 := of_expr0 e0 in let e1 := of_expr0 e1 in let e2 := of_expr0 e2 in
     constr:(CAS e0 e1 e2)
  | base.FAI ?C ?e => let e := of_expr0 e in constr:(FAI C e)
  | to_expr ?e => e
  | base.of_val ?v => constr:(Val v)
  | _ => constr:(ltac:(
     match goal with H : Is_true (base.is_closed [] e) |- _ => exact (@ClosedExpr e H) end))
  end.

Fixpoint is_closed0 (X : list string) (e : expr0) : bool :=
  match e with
  | Val _ | ClosedExpr _ _ => true
  | Var x => bool_decide (x ∈ X)
  | Rec f x e => is_closed0 (f :b: x :b: X) e
  | Lit _ | Alloc => true
  | UnOp _ e | Fork e | Load _ e | FAI _ e | Dealloc e =>
     is_closed0 X e
  | App e1 e2 | BinOp _ e1 e2 | Store _ e1 e2 =>
     is_closed0 X e1 && is_closed0 X e2
  | If e0 e1 e2 | CAS e0 e1 e2 =>
     is_closed0 X e0 && is_closed0 X e1 && is_closed0 X e2
  end.
Definition is_closed X (e : expr) : bool := is_closed0 X (e.1).
Lemma is_closed_correct0 X e : is_closed0 X e → base.is_closed X (to_expr' e).
Proof.
  revert X.
  induction e; try abstract naive_solver eauto using base.is_closed_of_val, base.is_closed_weaken_nil.
Qed.
Lemma is_closed_correct X e : is_closed X e → ra_lang.is_closed X (to_expr e).
Proof. destruct e. by move/is_closed_correct0. Qed.

(* We define [to_val (ClosedExpr _)] to be [None] since [ClosedExpr]
constructors are only generated for closed expressions of which we know nothing
about apart from being closed. Notice that the reverse implication of
[to_val_Some] thus does not hold. *)
Fixpoint to_val0 (e : expr0) : option base.val :=
  match e with
  | Val v => Some v
  | Rec f x e =>
     if decide (is_closed0 (f :b: x :b: []) e) is left H
     then Some (@RecV f x (to_expr' e) (is_closed_correct0 _ _ H)) else None
  | Lit l => Some (LitV l)
  | _ => None
  end.
Definition to_val (e : expr) : option val :=
  (λ v0, (v0, e.2)) <$> to_val0 (e.1).
Lemma to_val0_Some e v :
  to_val0 e = Some v → base.to_val (to_expr' e) = Some v.
Proof.
  revert v. induction e; intros; simplify_option_eq; rewrite ?to_of_val; auto.
  - by rewrite base.to_of_val.
  - do 2 f_equal. apply proof_irrel.
  - exfalso. unfold base.Closed in *; eauto using is_closed_correct0.
Qed.
Lemma to_val_Some e v :
  to_val e = Some v → ra_lang.to_val (to_expr e) = Some v.
Proof.
  destruct e, v. cbv -[to_val0 base.to_val to_expr'].
  case H: (to_val0 e) => //.
  erewrite to_val0_Some; by eauto.
Qed.

Lemma to_val0_is_Some e :
  is_Some (to_val0 e) → is_Some (base.to_val (to_expr' e)).
Proof. intros [v ?]; exists v; eauto using to_val0_Some. Qed.

Lemma to_val_is_Some e :
  is_Some (to_val e) → is_Some (ra_lang.to_val (to_expr e)).
Proof. move/fmap_is_Some => H. apply/fmap_is_Some. exact: to_val0_is_Some. Qed.

Fixpoint subst0 (x : string) (es : expr0) (e : expr0)  : expr0 :=
  match e with
  | Val v => Val v
  | ClosedExpr e H => @ClosedExpr e H
  | Var y => if decide (x = y) then es else Var y
  | Rec f y e =>
     Rec f y $ if decide (BNamed x ≠ f ∧ BNamed x ≠ y) then subst0 x es e else e
  | App e1 e2 => App (subst0 x es e1) (subst0 x es e2)
  | Lit l => Lit l
  | UnOp op e => UnOp op (subst0 x es e)
  | BinOp op e1 e2 => BinOp op (subst0 x es e1) (subst0 x es e2)
  | If e0 e1 e2 => If (subst0 x es e0) (subst0 x es e1) (subst0 x es e2)
  | Fork e => Fork (subst0 x es e)
  | Alloc => Alloc
  | Dealloc e => Dealloc (subst0 x es e)
  | Load acc e => Load acc (subst0 x es e)
  | Store acc e1 e2 => Store acc (subst0 x es e1) (subst0 x es e2)
  | CAS e0 e1 e2 => CAS (subst0 x es e0) (subst0 x es e1) (subst0 x es e2)
  | FAI C e => FAI C (subst0 x es e)
  end.
Definition subst x es (e : expr) := (subst0 x es (e.1), e.2).

Lemma to_expr_subst0 x er e :
  to_expr' (subst0 x er e) = base.subst x (to_expr' er) (to_expr' e).
Proof.
  induction e; simpl; repeat case_decide;
    f_equal; auto using base.is_closed_nil_subst, base.is_closed_of_val, eq_sym.
Qed.

Lemma to_expr_subst x (er : expr0) e :
  to_expr (subst x er e) = ra_lang.subst x (to_expr' er) (to_expr e).
Proof.
  destruct e. cbv -[to_expr' subst0 base.subst].
  by rewrite to_expr_subst0.
Qed.

Definition atomic0 (e : expr0) :=
  match e with
  | Load acc e => bool_decide (is_Some (to_val0 e))
  | Store acc e1 e2 => bool_decide (is_Some (to_val0 e1) ∧ is_Some (to_val0 e2))
  | CAS e0 e1 e2 =>
     bool_decide (is_Some (to_val0 e0) ∧ is_Some (to_val0 e1) ∧ is_Some (to_val0 e2))
  | FAI _ e | Dealloc e => bool_decide (is_Some (to_val0 e))
  (* Make "skip" atomic *)
  | Fork e => true
  | App (Rec _ _ (Lit _)) (Lit _) => true
  | Alloc => true
  | _ => false
  end.
Definition atomic (e : expr) := atomic0 (e.1).

Lemma atomic_correct e π : atomic0 e  → language.atomic (to_expr' e, π).
Proof.
  intros He. apply ectx_language_atomic.
  - intros σ [e' ρ'] σ' ef. cbn. unfold ra_lang.to_val. move => STEP.
    apply language.val_irreducible.
    apply/fmap_is_Some.
    destruct e; simpl; try done; repeat (case_match; try done);
    inversion STEP; (inversion BaseStep || inversion ExprStep); rewrite ?to_of_val; eauto. subst.
    unfold subst'; repeat (case_match || contradiction || simplify_eq/=); eauto.
  - apply ectxi_language_sub_values=> /= Ki e' Hfill.
    apply/fmap_is_Some. revert He. inversion Hfill as [Hfill']; subst; clear Hfill. simpl in *.
    destruct e; simpl; try done; repeat (case_match; try done);
    rewrite ?bool_decide_spec;
    destruct Ki; inversion Hfill'; subst; clear Hfill';
    try naive_solver eauto using to_val0_is_Some.
    unfold base.to_val. case_match; first by eauto. by move/bool_decide_spec: (n) => //=.
Qed.
End W.

Ltac solve_closed :=
  match goal with
  | |- Closed ?X (?e, ?π) =>
     let e_red := eval cbv in e in
     let e' := W.of_expr0 e_red in change (Closed X (W.to_expr e', π));
     apply W.is_closed_correct; vm_compute; exact I
  end.
Hint Extern 0 (Closed _ _) => solve_closed : typeclass_instances.

Ltac solve_base_closed :=
  match goal with
  | |- base.Closed ?X ?e =>
     let e' := W.of_expr0 e in change (base.Closed X (W.to_expr' e'));
     apply W.is_closed_correct0; vm_compute; exact I
  end.
Hint Extern 0 (base.Closed _ _) => solve_base_closed : typeclass_instances.

Ltac solve_base_to_val :=
    try match goal with
    | |- context E [language.to_val ?e] =>
          let X := context E [to_val e] in change X
    end;
    match goal with
    | |- base.to_val ?e = Some ?v =>
      let e' := W.of_expr0 e in
      change (base.to_val (W.to_expr0 (e')) = Some v);
      apply W.to_val0_Some; simpl; unfold W.to_expr0; reflexivity
    | |- is_Some (base.to_val ?e) =>
      let e' := W.of_expr0 e in
      change (is_Some (base.to_val (W.to_expr' e')));
      apply W.to_val0_is_Some, (bool_decide_unpack _); vm_compute; exact I
    end.
Ltac solve_to_val :=
    try match goal with
    | |- context E [language.to_val ?e] =>
          let X := context E [to_val e] in change X
    end;
    match goal with
    | |- to_val (?e, ?π) = Some ?v =>
      let e' := W.of_expr0 e in
      change (to_val (W.to_expr (e', π)) = Some v);
      apply W.to_val_Some; simpl; unfold W.to_expr; reflexivity
    | |- is_Some (to_val (?e, ?π)) =>
      let e' := W.of_expr0 e in
      change (is_Some (to_val (W.to_expr (e', π))));
      apply W.to_val_is_Some, (bool_decide_unpack _); vm_compute; exact I
    end.

Ltac solve_atomic :=
  match goal with
  | |- language.atomic (base.Fork ?e0, ?π) =>
    rewrite -(W.to_expr_eq e0);
    change (language.atomic (W.to_expr' (W.Fork (W.to_expr0 e0)), π));
    apply W.atomic_correct; vm_compute; exact I
  | |- language.atomic (?e, ?π) =>
    simpl e; 
     let e' := W.of_expr0 e in change (language.atomic (W.to_expr' e', π));
     apply W.atomic_correct; vm_compute; exact I
  end.
Hint Extern 10 (language.atomic _) => solve_atomic.
(* For the side-condition of elim_vs_pvs_wp_atomic *)
Hint Extern 10 (language.atomic _) => solve_atomic : typeclass_instances.

(** Substitution *)
Ltac simpl_subst :=
    lazy [subst'];
    repeat match goal with
           | |- context [base.subst ?x ?er ?e] =>
             let er' := W.of_expr0 er in
             let e' := W.of_expr0 e in
             change (base.subst x er e) with (base.subst x (W.to_expr' er') (W.to_expr' e'));
             rewrite <-(W.to_expr_subst0 x); simpl
           end;
  unfold W.to_expr'.
Arguments W.to_expr : simpl never.
Arguments base.subst : simpl never.
Arguments subst : simpl never.

(** The tactic [inv_head_step] performs inversion on hypotheses of the
shape [head_step]. The tactic will discharge head-reductions starting
from values, and simplifies hypothesis related to conversions from and
to values, and finite map operations. This tactic is slightly ad-hoc
and tuned for proving our lifting lemmas. *)
Ltac inv_head_step :=
  repeat match goal with
  | _ => progress simplify_map_eq/= (* simplify memory stuff *)
  | H : to_val _ = Some _ |- _ => apply of_to_val in H
  | H : context [to_val (of_val _)] |- _ => rewrite to_of_val in H
  | H : head_step ?e _ _ _ _ |- _ =>
     try (is_var e; fail 1); (* inversion yields many goals if [e] is a variable
     and can thus better be avoided. *)
     inversion H; subst; clear H
  end.

(** The tactic [reshape_expr e tac] decomposes the expression [e] into an
evaluation context [K] and a subexpression [e']. It calls the tactic [tac K e']
for each possible decomposition until [tac] succeeds. *)
Ltac reshape_val e tac :=
  let rec go e :=
  lazymatch e with
  | base.of_val ?v => v
  | Rec ?f ?x ?e => constr:(RecV f x e)
  | Lit ?l => constr:(LitV l)
  end in let v := go e in tac v.

Ltac reshape_expr e tac :=
  let rec go K e :=
  match e with
  | _ => tac K e
  | App ?e1 ?e2 => reshape_val e1 ltac:(fun v1 => go (AppRCtx v1 :: K) e2)
  | App ?e1 ?e2 => go (AppLCtx e2 :: K) e1
  | UnOp ?op ?e => go (UnOpCtx op :: K) e
  | BinOp ?op ?e1 ?e2 =>
     reshape_val e1 ltac:(fun v1 => go (BinOpRCtx op v1 :: K) e2)
  | BinOp ?op ?e1 ?e2 => go (BinOpLCtx op e2 :: K) e1
  | If ?e0 ?e1 ?e2 => go (IfCtx e1 e2 :: K) e0
  | Dealloc ?e => go (DeallocCtx :: K) e
  | Load ?acc ?e => go (LoadCtx acc :: K) e
  | Store ?acc ?e1 ?e2 => reshape_val e1 ltac:(fun v1 => go (StoreRCtx acc v1 :: K) e2)
  | Store ?acc ?e1 ?e2 => go (StoreLCtx acc e2 :: K) e1
  | CAS ?e0 ?e1 ?e2 => reshape_val e0 ltac:(fun v0 => first
     [ reshape_val e1 ltac:(fun v1 => go (CasRCtx v0 v1 :: K) e2)
     | go (CasMCtx v0 e2 :: K) e1 ])
  | CAS ?e0 ?e1 ?e2 => go (CasLCtx e1 e2 :: K) e0
  | FAI ?C ?e => go (FaiCtx C :: K) e
  end in go (@nil ectx_item) e.

(** The tactic [do_head_step tac] solves goals of the shape [head_reducible] and
[head_step] by performing a reduction step and uses [tac] to solve any
side-conditions generated by individual steps. *)
Tactic Notation "do_head_step" tactic3(tac) :=
  try match goal with |- head_reducible _ _ => eexists _, _, _ end;
  simpl;
  match goal with
  | |- head_step ?e1 ?σ1 ?e2 ?σ2 ?ef =>
     first [idtac|econstructor];
       (* solve [to_val] side-conditions *)
       first [rewrite ?to_of_val; reflexivity|simpl_subst; tac; fast_done]
  end.