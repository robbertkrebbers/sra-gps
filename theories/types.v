From Coq Require Export Utf8.
From stdpp Require Export gmap finite.
From igps Require Import infrastructure.

Reserved Notation "'<' x → v @ t , R >" (at level 20, format "< x → v @ t , R >",
                                         x at level 21, v at level 21, t at level 21, R at level 21).


Section Definitions.
  Set Default Proof Using "Type* Z DecZ CountZ".
  Inductive Val :=
  | A | D | VInj (v : Z).
  (* Definition isval : _ -> Prop := λ v, match v with V _ => True | _ => False end. *)
  Inductive isval : ∀ (v : Val), Prop := val_is_val v : isval (VInj v).

  Lemma NEqAD : A ≠ D. Proof. by inversion 1. Qed.
  
  Definition View := gmap positive positive.
  Record message :=
    mkMessage
      { mloc  : positive;
        mval  : Val;
        mtime : positive;
        mview    : View;
      }.
  Global Arguments mkMessage _ _ _%positive _.

  Definition message_tuple : Type := positive * Val * positive * View.
  Definition msg_to_tuple (m : message) : message_tuple := (mloc m, mval m, mtime m, mview m).
  Definition tuple_to_msg (m : message_tuple) : message :=
    match m with
    | (l, v, t, R) => mkMessage l v t R
    end.

  Definition val_to_sum : Val -> _ := λ v, match v with | A => inl () | D => inr (inl ()) | VInj v => inr (inr v) end.
  Definition sum_to_val : _ -> Val := λ s, match s with | inl () => A | inr (inl ()) => D | inr (inr v) => VInj v end.
  Global Instance Val_dec_eq : ∀ v1 v2 : Val, Decision (v1 = v2) :=
    inj_dec_eq val_to_sum (Some ∘ sum_to_val) _.
  Proof. by destruct 0. Qed.
  Global Instance Val_countable : Countable Val :=
    inj_countable val_to_sum (Some ∘ sum_to_val) _.
  Proof. by destruct 0. Qed.

  Global Instance msg_dec_eq : ∀ x y : message, Decision (x = y) :=
    inj_dec_eq msg_to_tuple (Some ∘ tuple_to_msg) _.
  Proof. by destruct 0. Qed.
  Global Instance msg_countable : Countable message :=
    inj_countable msg_to_tuple (Some ∘ tuple_to_msg) _.
  Proof. by destruct 0. Qed.

  Global Instance message_dec_eq : ∀ (m1 m2 : message), Decision (m1 = m2) := _.

  Global Instance message_countable : Countable message := _.


  Implicit Type (x t : positive) (R V : View) (m : message).

  Section Glue.
    Reserved Notation "'at'".
    Inductive access : Type := na | at_hack.
    Notation "'at'" := at_hack.

    Definition thread := positive.
    Implicit Type (π : thread).

    Inductive event :=
    | ERead : access -> positive -> Val -> event
    | EWrite : access -> positive -> Val -> event
    | EUpdate : positive -> Val -> Val -> event
    | EFork : View -> event
    .

    Definition acc_of : event -> option access :=
      λ evt, match evt with
             | ERead acc _ _ | EWrite acc _ _ => Some acc
             | EUpdate _ _ _ => Some at_hack
             | _ => None
             end.

    Definition loc_of : event -> option positive :=
      λ evt, match evt with
             | ERead _ x _ | EWrite _ x _ | EUpdate x _ _ => Some x
             | _ => None
             end.

    Lemma loc_acc_of evt acc : acc_of evt = Some acc -> ∃ x, loc_of evt = Some x.
    Proof. by destruct evt; eauto. Qed.

    Definition acc_to_sum : access -> _ 
      := λ acc, match acc with | na => inl () | at_hack => inr () end.
    Definition sum_to_acc : _ -> access 
      := λ s, match s with | inl () => na | inr () => at_hack end.
    Global Instance access_dec_eq : EqDecision access | 20 :=
      inj_dec_eq acc_to_sum (Some ∘ sum_to_acc) _.
    Proof. by destruct 0. Qed.
    Global Instance access_countable : Countable access | 20 :=
      inj_countable acc_to_sum (Some ∘ sum_to_acc) _.
    Proof. by destruct 0. Qed.

  End Glue.

End Definitions.

Global Instance View_Extension : Extension View | 0 := _.

Notation "'<' x → v @ t , R >" :=
  (mkMessage x v t R)
    (at level 20, format "< x → v @ t , R >",
     x at level 21, v at level 21, t at level 21, R at level 21).
