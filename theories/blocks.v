From igps Require Import blocks_generic types lang.
From igps.base Require Import ghosts.

Section MHist.
  Implicit Type (M : gset message).
  Definition time_disj m m' := mtime m ≠ mtime m'.
  Definition time_disj_pw M := ∀ m m', m ∈ M → m' ∈ M → time_disj m m'.
  Definition time_disj_M M m := ∀ m', m' ∈ M → time_disj m m'.
  Definition adj := (λ m m', mtime m' = (mtime m + 1)%positive).
  Global Instance adj_dec : ∀ m m', Decision (adj m m') := _.

  Lemma hist_block_ends_msgs_ins (M : gset message) m
        (pdisj: time_disj_pw M)
        (disj: time_disj_M M m)
    : block_ends_ins_spec adj M m (block_ends adj M) (block_ends adj ({[m]} ∪ M)).
  Proof.
    apply block_ends_ins.
    - move => a b1 b2. rewrite /adj.
      case: (decide (b1 ∈ M));
      case: (decide (b2 ∈ M)); auto.
      + move => ? ? ?. intros. exfalso. exact: pdisj.
    - move => a b1 b2. rewrite /adj.
      case: (decide (b1 ∈ M));
      case: (decide (b2 ∈ M)); auto.
      + move => ? ? ?. intros. exfalso. exact: pdisj.
    - move => ?. exact: disj.
  Qed.

End MHist.

Section GHist.
  Implicit Type (x : loc) (t : positive) (V : View)
           (m : message) (M: gset message) (h: History) (v: Val).

  Definition gadj x (p p' : Val * View) := adj_opt (p.2 !! x) (p'.2 !! x).
  Global Instance gadj_dec x : ∀ p p', Decision (gadj x p p').
  Proof.
    move => p p'. rewrite /gadj.
    case H': (p'.2 !! x) => [t'|] //;
    case H: (p.2 !! x) => [t|] //;
    try case: (decide ((t' = (t + 1)%positive))) => [Eq|NEq].
    - left. exists t; split => //. by f_equal.
    - right => Eq. apply: NEq. by move : Eq => [t1 [[<-] [->]]].
    - right. move => [? [//]].
    - right. move => [? [//]].
    - right. move => [? [//]]. 
  Qed.

  Definition gblock_ends x h := block_ends (gadj x) h.
  Definition gblock_ends_ins x := block_ends_ins_spec (gadj x).

  Global Instance gblock_ends_Proper x : Proper ((≡) ==> (≡)) (gblock_ends x).
  Proof. solve_proper. Qed.

  Lemma gblock_ends_ins_update x h p
        (OK: hTime_ok x ({[p]} ∪ h))
        (Fresh: h ⊥ {[p]})
  : gblock_ends_ins x h p (gblock_ends x h) (gblock_ends x ({[p]} ∪ h)).
  Proof.
    apply block_ends_ins.
    - move => a b1 b2. rewrite /gadj.
      case: (decide (b1 ∈ h));
      case: (decide (b2 ∈ h)); [|auto|auto|auto].
      move => In2 In1 _ H1 H2. left.
      apply: (OK.2); [ set_solver+In1 | set_solver+In2 |].
      destruct H1 as [T1 [Eq1 Eq1']], H2 as [T2 [Eq2 Eq2']].
      destruct b1, b2. simpl in *. rewrite Eq2 in Eq1. simplify_eq.
      by rewrite Eq1'.
    - move => a b1 b2. rewrite /gadj.
      case: (decide (b1 ∈ h));
      case: (decide (b2 ∈ h)); [|auto|auto|auto].
      move => In2 In1 _ H1 H2. left.
      apply: (OK.2); [set_solver+In1|set_solver+In2|].
      destruct H1 as [T1 [Eq1 Eq1']], H2 as [T2 [Eq2 Eq2']].
      destruct b1, b2. simpl in *. rewrite Eq2' in Eq1'. simplify_eq.
      rewrite -!Pplus_one_succ_r in Eq1'.
      move/Pos.succ_inj : Eq1'. by rewrite Eq1 Eq2 => ->.
    - set_solver+Fresh.
  Qed.

  Lemma gblock_ends_fmap x M
        (Disj : time_disj_pw M)
        (ViewTime : ∀ m, m ∈ M → mview m !! x = Some $ mtime m)
       (EqLoc : ∀ m, m ∈ M → mloc m = x) :
    block_ends (gadj x) {[ (mval m, mview m) | m <- M ]}
    ≡ {[ (mval m, mview m) | m <- block_ends adj M ]}.
  Proof.
    apply block_ends_fmap.
    - move => m1 m2 ? ?. rewrite /adj /gadj.
      split.
      + move => [t /=].
        rewrite !ViewTime //. by move => [[->] [->]].
      + move => Eq /=. eexists (mtime m1). by rewrite !ViewTime // Eq.
    - move => [? ? ? ?] [? ? ? ?] ? ?. split.
      + move => [Eq1 Eq2]. exfalso. exact: Disj.
      + by move => ->.
  Qed.

  Lemma gblock_ends_ins_sL_update l (h: History) p p'
    (In': p' ∈ gblock_ends l h)
    (NIn': p ∉ gblock_ends l h)
    (Adj: adj_opt (p'.2 !! l) (p.2 !! l))
    (GBE: gblock_ends_ins l h p (gblock_ends l h) (gblock_ends l ({[p]} ∪ h)))
    (Disj: hTime_pw_disj l ({[p]} ∪ h))
    : gblock_ends l ({[p]} ∪ h) ⊆ {[p]} ∪ gblock_ends l h ∖ {[p']}.
  Proof.
    assert (p' ∈ h).
      { apply (elem_of_subseteq (gblock_ends l h) h); last auto.
        by apply subseteq_gset_filter. }
    inversion GBE.
    - exfalso. by apply (nL p').
    - assert (Eqb: b = p').
        { eapply adj_eq; eauto. eapply hTime_pw_disj_mono; last by apply Disj.
          apply union_subseteq_r. }
      by rewrite -Eqb BE.
    - exfalso. apply (nL p') => //.
    - assert (Eqb: b = p').
        { eapply adj_eq; eauto. eapply hTime_pw_disj_mono; last by apply Disj.
          apply union_subseteq_r. }
      rewrite BE Eqb. apply union_subseteq_r.
  Qed.

End GHist.
