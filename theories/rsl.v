From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From stdpp Require Import functions.
From iris.base_logic Require Import big_op.
From iris.base_logic.lib Require Import saved_prop sts cancelable_invariants.
From iris.algebra Require Import auth frac sts.
From iris.proofmode Require Import tactics.

Import uPred.

From igps Require Export lang blocks iris_lemmas.
From igps Require Import rsl_sts notation viewpred na.
From igps.base Require Import base.

Set Bullet Behavior "Strict Subproofs".

Implicit Types (I : gset gname) (l: loc) (V: View) (s: state) (q: frac).

Class rslG Σ := RSLG {
  rsl_stsG :> stsG Σ rsl_sts;
  rsl_savedPropG :> savedPropG Σ (ofe_funCF Z (ofe_funCF View idCF));
}.

Definition rslΣ : gFunctors
 := #[stsΣ rsl_sts; savedPropΣ (ofe_funCF Z (ofe_funCF View idCF))].

Instance subG_rslΣ {Σ} : subG rslΣ Σ → rslG Σ.
Proof. solve_inG. Qed.

Section proof.

Context `{!foundationG Σ, !rslG Σ}.

Set Default Proof Using "Type".

Local Notation iProp := (iProp Σ).

Implicit Types (Ψ: gname → Z → View → iProp) (φ: Z → View → iProp).


Local Open Scope I.

Definition sconv φ Ψ I : iProp 
  := □ ∀ v V, φ v V → ([∗ set] i ∈ I, Ψ i v V).

Definition conv φ φ' : iProp := □ ∀ v V, φ v V → φ' v V.

Definition ress φ Ψ I : iProp :=
  ▷ (sconv φ Ψ I) ∗ [∗ set] i ∈ I, saved_prop_own i (Ψ i).

Definition ress2 φ Ψ I : iProp :=
  ([∗ set] i ∈ I, saved_prop_own i (Ψ i))
  ∗ ▷ □ ∀ i v V, ⌜i ∈ I⌝ → (Ψ i v V ↔ True).

Definition rsl_inv l φ φd s: iProp :=
  Hist l (rhist s) ∗
  match s with
  | aUninit I0 h =>
      ⌜uninit h⌝ ∗ ∃ Ψ, ress φ Ψ I0%I
  | aInit I1 I2 h Sub =>
      ⌜init l h⌝ ∗ ∃ Ψ, ress φ Ψ I1
      ∗ ▷ ([∗ set] p ∈ h, match p with
                          | (VInj v,V) => ([∗ set] i ∈ I2, Ψ i v V)
                          | (_,_) => True
                          end)
      ∗ ▷ ([∗ set] p ∈ h, match p with
                          | (VInj v,V) => φd v V
                          | (_,_) => True
                          end)
  | cUninit I0 h =>
      ⌜uninit h⌝ ∗ ∃ Ψ, ress2 φ Ψ I0%I
  | cInit I0 h =>
      ⌜init l h⌝
      ∗ ▷ ([∗ set] p ∈ gblock_ends l h, match p with
                                        | (VInj v,V) => φ v V
                                        | (_,_) => True
                                        end)
      ∗ ▷ ([∗ set] p ∈ h, match p with
                          | (VInj v,V) => φd v V
                          | (_,_) => True
                          end)
      ∗ ∃ Ψ, ress2 φ Ψ I0%I
  end.

Definition RSLInv φ φd l jγ : iProp 
  := ∃ (j γ: gname), ⌜jγ = encode (j,γ)⌝ ∗ sts_inv γ (rsl_inv l φ φd).

Definition RelRaw V (φ: Z → vPred) φR jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅ ∗ ⌜alloc_local (rhist s) V⌝
        ∗ ▷ conv φ φR ∗ saved_prop_own j φR.

Definition AcqRaw V (φ: Z → vPred) jγ : iProp
  := ∃ s (j γ: gname) i Ψi, ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s {[ Change i ]}
        ∗ ⌜s ∈ i_states i⌝
        ∗ ⌜alloc_local (rhist s) V⌝ ∗ ▷ conv Ψi φ
        ∗ saved_prop_own i Ψi.

Definition RMWAcqRaw V jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅ 
        ∗ ∃ I0 h, ⌜s = cUninit I0 h ∨ s = cInit I0 h⌝
        ∗ ⌜alloc_local h V⌝.

Definition InitRaw V jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅ 
        ∗ ⌜∃ h, ((∃ I1 I2 Sub, s = aInit I1 I2 h Sub) ∨ (∃ I0, s = cInit I0 h))
                  ∧ init_local h V⌝.

Instance RelRaw_persistent V (φ: Z → vPred) φR jγ
  : PersistentP (RelRaw V φ φR jγ) := _.

Instance RMWAcqRaw_persistent V jγ : PersistentP (RMWAcqRaw V jγ) := _.

Instance InitRaw_persistent V jγ : PersistentP (InitRaw V jγ) := _.

  Section Helpers.
  Set Default Proof Using "Type".

  Lemma big_sepS_gblock_ends_ins_update 
    l (h: History) p0 (Ψ: Val * View → iProp) 
    (Disj: (h ⊥ {[p0]})%C)
    (GB: gblock_ends_ins l h p0 (gblock_ends l h) (gblock_ends l ({[p0]} ∪ h)))
    : Ψ p0 ∗ ([∗ set] p ∈ gblock_ends l h, Ψ p)
      ⊢ ([∗ set] p ∈ gblock_ends l ({[p0]} ∪ h), Ψ p).
  Proof.
    apply blocks_generic.block_ends_ins_update; last auto.
    by apply disjoint_singleton_r in Disj.
  Qed.


  Lemma sts_auth_frag_in_up s T s' T' γ:
    own γ (sts_auth s T) ∗ sts_own γ s' T' ⊢ ⌜s ∈ sts.up s' T'⌝.
  Proof.
    rewrite /sts_own -own_op. iIntros "Own".
    by iDestruct (own_valid with "Own") as %?%sts_auth_frag_valid_inv.
  Qed.

  Lemma rsl_saved_pred_unfold i (Ψ1 Ψ2: (Z -c> View -c> ∙)%CF iProp):
    saved_prop_own i Ψ1 ∗ saved_prop_own i Ψ2 ⊢ □ ▷ (∀ v V, Ψ1 v V ≡ Ψ2 v V).
  Proof.
    iIntros "(Saved1 & Saved2)". 
    iDestruct (saved_prop_agree i with "Saved1 Saved2") as "#Eq".
    by do 2 setoid_rewrite ofe_funC_equivI.
  Qed.

  Lemma rsl_saved_pred_unfold_big_sepS
    I0 i Ψi Ψ (In: i ∈ I0):
    (saved_prop_own i Ψi ∗ [∗ set] i0 ∈ I0, saved_prop_own i0 (Ψ i0))
    ⊢ □ ▷ (∀ v V, Ψi v V ≡ Ψ i v V).
  Proof.
    iIntros "(#Saved & #SSaved)".
    iDestruct (big_sepS_dup _ _
                (λ i, saved_prop_own i (Ψ i : (Z -c> View -c> ∙)%CF iProp)) In 
                with "[$SSaved]") as "(#SΨi1 & _)".
      { iIntros "#?". by iSplit. }
    iApply (rsl_saved_pred_unfold i with "[]"). iFrame "Saved SΨi1".
  Qed.

  Lemma rsl_ress2_insert_c φ I0 i (NI: i ∉ I0):
    saved_prop_own i ((λ _ _, True): Z → View → iProp) ∗ (∃ Ψ, ress2 φ Ψ I0)
    ⊢ ∃ Ψ, ress2 φ Ψ ({[i]} ∪ I0).
  Proof.
    iIntros "(#Si & ress2)".
    iDestruct "ress2" as (Ψ) "(Saved & #True)".
    iExists (<[i:= (λ _ _, True)]> Ψ). iSplitL "Saved".
    - rewrite (big_sepS_fn_insert
                (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp)));
       last exact NI. by iFrame.
    - iNext. rewrite always_elim. iIntros "!#".
      iIntros (i' v' V') "In".
      iDestruct "In" as %[->%elem_of_singleton_1|In]%elem_of_union.
      + rewrite fn_lookup_insert. iSplit; by iIntros "_".
      + rewrite fn_lookup_insert_ne.
        * by iApply "True".
        * move => ?. by subst.
  Qed.

  Lemma rsl_inv_insert_c I0 h i s l φ φd
    (Eqc: s = cUninit I0 h ∨ s = cInit I0 h) (NI: i ∉ rISet s)
    : saved_prop_own i ((λ _ _, True): Z → View → iProp)
      ∗ rsl_inv l φ φd s ⊢ rsl_inv l φ φd (state_ISet_insert i s).
  Proof.
    iIntros "(#Saved & Inv)".
    move : Eqc NI => [->|->] /= NI;
      [iDestruct "Inv" as "(? & ? & ress2)"|
        iDestruct "Inv" as "(? & ? & ? & ? & ress2)"];
      iFrame; by iApply (rsl_ress2_insert_c with "[$Saved $ress2]").
  Qed.

  Lemma conv_id φ: True ⊢ conv φ φ.
  Proof.
    iIntros "!#". by iIntros (? ?) "?".
  Qed.

  Lemma conv_switch_a i Ψi Ψ (φ: Z → vPred) v:
    conv Ψi φ ∗ □ (∀ (v0 : Z) (V0 : View), Ψi v0 V0 ≡ Ψ i v0 V0)
    ⊢ conv (<[v:=λ _ : View, True]> (Ψ i)) (<[v:=True%VP]> φ).
  Proof.
    iIntros "(#Conv & #Eq)".
    iIntros "!#".
    iIntros (v2 V2).
    case (decide (v = v2)) => [->|NEqv].
    + rewrite 2!fn_lookup_insert. by iIntros "_".
    + rewrite (fn_lookup_insert_ne); last auto.
      rewrite (fn_lookup_insert_ne); last auto.
      iIntros "Ψi". iApply "Conv".
      by iRewrite ("Eq" $! v2 V2).
  Qed.

  Lemma sconv_switch_a i i' Ψ φ v I1 (NI: i' ∉ I1) (In: i ∈ I1):
    sconv φ Ψ I1
    ⊢ sconv φ (<[i':=<[v:=λ _ : View, True]> (Ψ i)]> Ψ) (ISet_switch i i' I1).
  Proof.
    iIntros "#SConv".
    iIntros "!#". iIntros (v2 V2) "HφR".
    iDestruct ("SConv" $! v2 V2 with "HφR") as "HφR".
    rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v2 V2));
      last first.
      { by move => /elem_of_difference [? _]. }
    rewrite (big_sepS_delete _ _ _ In).
    iDestruct "HφR" as "(? & ?)". iFrame.
    case (decide (v = v2)) => [->|?].
    - by rewrite fn_lookup_insert.
    - by rewrite fn_lookup_insert_ne.
  Qed.


  Lemma saved_pred_switch_a i i' Ψ v I1 (NI: i' ∉ I1) (In: i ∈ I1):
    ([∗ set] x ∈ I1, saved_prop_own x (Ψ x))
    ∗ saved_prop_own i' (<[v:=λ _ : View, True]> (Ψ i))
    ⊢ [∗ set] i0 ∈ ISet_switch i i' I1, 
                saved_prop_own i0 (<[i':=<[v:=λ _ : View, True]> (Ψ i)]> Ψ i0).
  Proof.
    iIntros "(#Saved & #?)".
    rewrite (big_sepS_fn_insert
                (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp)));
      last first.
      { by move => /elem_of_difference [? _]. }
    rewrite (big_sepS_delete _ _ _ In).
    iDestruct "Saved" as "(? & ?)". by iFrame "#".
  Qed.

  End Helpers.

  Lemma RelRaw_write l (φ φd φR: Z → vPred) v V n E π:
    nclose physN ⊆ E →
      {{{ PSCtx ∗ ▷ Seen π V ∗ ▷ φ v V ∗ ▷ φd v V 
            ∗ ▷ RSLInv φR φd l n ∗ RelRaw V φ φR n }}}
        ([(#l)]_at <- #v, V) @ E
      {{{ V', RET (#(), V') ;
            ⌜V ⊑ V'⌝ ∗ Seen π V' 
            ∗ ▷ RSLInv φR φd l n ∗ RelRaw V' φ φR n ∗ InitRaw V' n}}}%C.
  Proof.
    intros.
    iIntros "(#kI & Seen & Itpr &ItprD & oI & Rel) Post".
    iDestruct "Rel" as (s j γ) "(% & Own & % & #Conv & #Stored)".
    iDestruct "oI" as (j1 γ1) "(>% & Inv)". subst n. apply encode_inj in H2.
    inversion H2. subst j1 γ1.

    iMod (sts_acc with "[$Inv $Own]") as "IO".
    iDestruct "IO" as (s') "(Step & Inv & HClose)".
    iDestruct "Step" as %Step%frame_steps_steps.

    assert (alloc_local (rhist s') V).
      { apply: alloc_local_mono_rsl_state; eauto. }

    iDestruct "Inv" as "(Hist & Inv)".
    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first auto.

    iApply (f_write_at with "[$kI $Hist $Seen]"); first auto.

    iNext. iIntros (V' h') "(% & Seen' & Hist & % & % & % & % & %)".
    iMod (Hist_hTime_ok with "[$kI $Hist]") as "(Hist & %)"; first done.

    assert (alloc_local h' V').
      { by exists (VInj v), V'. }

    pose s'' := state_hist_update h' s'.

    iMod ("HClose" $! s'' (∅: sts.tokens rsl_sts)
        with "[Itpr ItprD Inv Hist]") as "(Inv & #Own)".
      { iSplitL "".
        - iPureIntro. by apply (state_hist_update_steps _ v V').
        - rewrite /rsl_inv. iNext.
          iSplitL "Hist"; first by rewrite state_hist_update_hist.
          destruct s' as [I0 h|I1 I2 h|I0 h|I0 h];
            iDestruct "Inv" as "(% & ress)"; simpl; iFrame (H5).
          + iDestruct "ress" as (Ψ) "ress".
            iExists Ψ. iDestruct "ress" as "(#EConv & #Saved)".
            destruct H11 as [V'' EqV]. rewrite -EqV in H6.
            iFrame "EConv Saved". iNext. iSplitL "Itpr".
            { iApply (big_sepS_mono _ _ ({[(VInj v, V');(A, V'')]}: History)).
              - by rewrite -H6.
              - reflexivity.
              - rewrite big_sepS_insert; last by rewrite elem_of_singleton.
                rewrite big_sepS_singleton.
                iSplitR ""; last auto. iApply "EConv". iApply "Conv".
                by iApply (vPred_mono V). }
            { iApply (big_sepS_mono _ _ ({[(VInj v, V');(A, V'')]}: History)).
              - by rewrite -H6.
              - reflexivity.
              - rewrite big_sepS_insert; last by rewrite elem_of_singleton.
                rewrite big_sepS_singleton.
                iSplitR ""; last auto. by iApply (vPred_mono V). }
          + iDestruct "ress" as (Ψ) "ress". (* TODO: duplicated proofs *)
            iExists Ψ. iDestruct "ress" as "[[#EConv #Saved] [ress ressD]]".
            iFrame "EConv Saved". iNext.
            pose p : Val * View := (VInj v, V').
            iSplitL "Itpr ress".
            * iApply (big_sepS_mono _ _  ({[VInj v, V']} ∪ h));
                [by rewrite H6|reflexivity|].
              rewrite big_sepS_insert;
                last by apply disjoint_singleton_r in H7.
              iFrame "ress".
              iApply (big_sepS_mono _ _ I1); [auto|auto|].
              iApply "EConv". iApply "Conv". by iApply (vPred_mono V).
            * iApply (big_sepS_mono _ _  ({[VInj v, V']} ∪ h));
                [by rewrite H6|reflexivity|].
              rewrite big_sepS_insert;
                last by apply disjoint_singleton_r in H7.
              iFrame "ressD". by iApply (vPred_mono V).
          + iDestruct "ress" as (Ψ) "#ress2".
            destruct H11 as [V'' EqV]. rewrite -EqV in H6.
            iSplitL "Itpr"; last iSplitL "ItprD".
            { iNext.
              iApply (big_sepS_mono _ _ ({[(VInj v, V');(A, V'')]}: History)).
              * rewrite -H6. by apply subseteq_gset_filter.
              * reflexivity.
              * rewrite big_sepS_insert; last by rewrite elem_of_singleton.
                rewrite big_sepS_singleton. iSplitR ""; last done.
                iApply "Conv". by iApply (vPred_mono V). }
            { iNext.
              iApply (big_sepS_mono _ _ ({[(VInj v, V');(A, V'')]}: History)).
              * by rewrite -H6.
              * reflexivity.
              * rewrite big_sepS_insert; last by rewrite elem_of_singleton.
                rewrite big_sepS_singleton.
                iSplitR ""; last done. by iApply (vPred_mono V). }
            { iExists Ψ. iDestruct "ress2" as "(? & ?)". by iFrame "ress2". }
          + iDestruct "ress" as "(ress & ressD & ress2)". iFrame "ress2".
            pose p : Val * View := (VInj v, V'). iNext.
            iSplitL "Itpr ress".
            { iApply (big_sepS_mono _ _  (gblock_ends l ({[VInj v, V']} ∪ h)));
                [by rewrite H6|reflexivity|].
              iApply (big_sepS_gblock_ends_ins_update); [auto|..|iFrame].
              * apply blocks.gblock_ends_ins_update; last auto.
                apply (hTime_ok_mono _ _ h'); last auto. by rewrite H6.
              * iApply "Conv". by iApply (vPred_mono V). }
            { iApply (big_sepS_mono _ _  ({[VInj v, V']} ∪ h));
                [by rewrite H6|reflexivity|].
              rewrite big_sepS_insert;
                last by apply disjoint_singleton_r in H7.
              by iFrame. } }

      iApply ("Post" $! V').
      iFrame (H3) "Seen'".
      iSplitL "Inv".
      - iExists j, γ. by iFrame "Inv".
      - iSplitL ""; iExists s'', j, γ; iFrame "Own"; (iSplitL ""; first auto).
        + iFrame "Conv Stored". by rewrite state_hist_update_hist.
        + iPureIntro. exists h'. split; last auto.
           destruct s'; [left|left|right|right]; by do 3 eexists.
  Qed.


  Lemma AcqRaw_read l (φ φd φR: Z → vPred) V n E π:
    nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V ∗ (∀ V v, φd v V -∗ φd v V ∗ φd v V)
        ∗ ▷ RSLInv φR φd l n ∗ AcqRaw V φ n ∗ InitRaw V n }}}
      ([(#l)]_at, V) @ E
    {{{ (v:Z) V', RET (#v, V');
        ⌜V ⊑ V'⌝ ∗ Seen π V'
        ∗ ▷ RSLInv φR φd l n
        ∗ AcqRaw V' (<[v:=True%VP]>φ) n ∗ InitRaw V' n
        ∗ ▷ φ v V' ∗ ▷ φd v V'}}}%C.
  Proof.
    iIntros (? Φ) "(#kI & kS & Dup & oI & AR & IR) Post".
    iDestruct "AR" as (s1 j γ i Ψi) "(% & Own1 & Hs1 & Alloc & #Conv & #SΨi)".
    iDestruct "Hs1"  as %Hs1.
    iDestruct "Alloc" as %Alloc1.
    iDestruct "IR" as (s2 j1 γ1) "(% & #Own2 & IR)".
    subst n. apply encode_inj in H1. inversion H1. subst j1 γ1.
    iDestruct "IR" as %[h2 [IR Init2]].
    rewrite {1}/RSLInv /sts_inv.
    iDestruct "oI" as (j2 γ2) "(>% & Inv)".
    apply encode_inj in H0. inversion H0. subst j2 γ2.

    iDestruct "Inv" as (s) "(>Own & Hist & Inv)".
    iDestruct (sts_auth_frag_in_up with "[$Own $Own1]") as %Up1.
    iDestruct (sts_auth_frag_in_up with "[$Own $Own2]") as %Up2.

    assert (HInit: init_local (rhist s) V).
      { apply (init_local_mono_rsl_state s2).
        - move: IR => [[? [? [? ->]]]|[? ->]] //=.
        - by apply (frame_steps_steps _ _ ∅). }
    assert (HAlloc := alloc_init_local _ _ HInit).
    assert (HSub := sts.up_subseteq s1 _ _ (i_states_closed i) Hs1).

    destruct (frame_steps_Init_branch s2 s ∅)
      as [h Hh]; [auto|by eexists|].

    iApply (wp_mask_mono (↑physN)); first auto.
    iApply wp_fupd.

    iApply (f_read_at with "[$Hist $kS $kI]"); first auto.
    iNext.
    iIntros (v V') "(% & kS' & Hist & % & vV)".
    iDestruct "vV" as (V1) "(% & % & %)".

    destruct Hh as [[I1 [I2 [Sub Hs]]]|[I1 Hs]].

    - rewrite Hs /= in HInit, HAlloc.
      rewrite {2}Hs.
      iDestruct "Inv" as "(% & ress)".
      iDestruct "ress" as (Ψ) "(#Saved & ress & ressD)".
      iDestruct "Saved" as "(SConv & Saved)".

      iMod (saved_prop_alloc_strong 
                  (<[v:=(λ _, True)]> (Ψ i): (Z -c> View -c> ∙)%CF iProp)
                  (rISet s))
      as (i') "[% #SΨi']".
      rewrite Hs /= in H8.

      (* State update *)

      pose s' := state_ISet_switch i i' s.

      rewrite /sts_own.
      iCombine "Own" "Own1" as "Own".
      rewrite sts_op_auth_frag;
        [|auto|by apply sts.closed_up, tok_disj_state_singleton].

      assert (His: i ∈ I2).
        { rewrite (_: I2 = rISet2 s). apply HSub in Up1; auto.
          by rewrite Hs. }

      iMod (own_update _ _ (sts_auth s' {[Change i']}) with "[$Own]") as "Own".
        { apply sts_update_auth.
          apply state_ISet_switch_steps; by rewrite Hs. }

      rewrite -2!sts_op_auth_frag_up.
      iDestruct "Own" as "[[Own #Own1] Own']".
      rewrite Hs /= in H6.
      (* get dup interpretation *)
      iDestruct (big_sepS_dup_later _ _ _ H6 with "[$ressD Dup]")
        as "(Hφd & ressD)".
        { iNext. iIntros "?". by iApply "Dup". }

      iAssert (▷ (φd v) V') with "[Hφd]" as "Hφd".
        { iNext. by iApply (vPred_mono V1). }

      (* Predicate equality *)
      iDestruct (rsl_saved_pred_unfold_big_sepS with "[$SΨi $Saved]") as "#Eq".
        { by apply Sub. }

      (* get full interpretation *)

      rewrite (big_sepS_delete _ _ _ H6).
      iDestruct "ress" as "(Hφ & ress)".

      rewrite (big_sepS_delete _ _ _ His).
      iDestruct "Hφ" as "(Hφi & Hφ)".

      iAssert (▷ (φ v) V') with "[Hφi]" as "Hφi".
        { iNext. iApply (vPred_mono V1); first auto.
          iApply "Conv". by iRewrite ("Eq" $! v V1). }

      iMod (Hist_hTime_ok with "[$Hist $kI]") as "(Hist & %)"; first auto.

      assert (Hi': i' ∉ I2 ∖ {[i]}).
        { move => In. by apply gset_difference_subseteq, Sub in In. }


      iApply ("Post" $! v V').
      iFrame (H2) "kS' Hφd Hφi".
      iSplitR "Own'"; last iSplitR "".
      + iModIntro. iNext. rewrite /RSLInv /sts_inv. iExists j, γ. iSplitL ""; first auto.
        iExists s'. iFrame "Own". rewrite {2}/s' Hs /=.
        iFrame (H7) "Hist ressD".
        iExists ((<[i':= <[v:=λ _ : View, True]> (Ψ i)]>)Ψ).
        iSplitL ""; first iSplitL "".
        * iNext. iApply (sconv_switch_a with "[$SConv]"); [auto|by apply Sub].
        * iApply (saved_pred_switch_a with "[$Saved $SΨi']");
            [auto|by apply Sub].
        * iNext.
          rewrite (big_sepS_delete _ _ _ H6). iSplitL "Hφ".
          { rewrite (big_sepS_fn_insert
                      (λ _ (f: Z → View → iProp), f v V1)); last exact Hi'.
            rewrite /= fn_lookup_insert. by iFrame. }
          { iApply (big_sepS_mono _ _ (h ∖ {[VInj v, V1]}) with "[$ress]");
              first auto.
            move => [[| |v2] V2] //= /elem_of_difference [In2 _].
            rewrite (big_sepS_fn_insert
                      (λ _ (f: Z → View → iProp), f v2 V2)); last exact Hi'.
            rewrite (big_sepS_delete _ _ _ His).
            iIntros "(HΨ & HSet)". iFrame.
            case (decide (v = v2)) => [->|?].
            - by rewrite fn_lookup_insert.
            - by rewrite fn_lookup_insert_ne. }
      + iExists s', j, γ, i', (<[v:=(λ _, True)]> (Ψ i)).
        rewrite /sts_own. iFrame. rewrite /s' /=. iFrame "SΨi'".
        repeat iSplitL ""; first auto.
        * iPureIntro. by apply state_ISet_switch_included.
        * iPureIntro. apply state_ISet_switch_alloc_local.
          rewrite Hs. by apply (alloc_local_Mono _ _ V).
        * iModIntro. iNext. by iApply (conv_switch_a with "[$Conv]").
      + iExists s',j,γ. rewrite /sts_own.  iFrame "Own1".
        iSplitL ""; first auto. iPureIntro. exists h. split.
        * left. rewrite /s' Hs. by do 3 eexists.
        * by apply (init_local_Mono _ _ V).

    - rewrite Hs /= in HInit, HAlloc.
      rewrite {2}Hs.
      iDestruct "Inv" as "(% & ress & ressD & Saved)".
      iDestruct "Saved" as (Ψ) "(#Saved & #True)".
      rewrite Hs /= in H6.
      (* get dup interpretation *)
      iDestruct (big_sepS_dup_later _ _ _ H6 with "[$ressD Dup]")
        as "(Hφd & ressD)".
        { iNext. iIntros "?". by iApply "Dup". }

      iAssert (▷ (φd v) V') with "[Hφd]" as "Hφd".
        { iNext. by iApply (vPred_mono V1). }

      (* Predicate equality *)
      assert (His: i ∈ I1).
        { apply HSub in Up1. move: Up1. by rewrite Hs. }

      iDestruct (rsl_saved_pred_unfold_big_sepS with "[$SΨi $Saved]")
        as "#Eq"; first exact His.

      (* get state ghost *)

      rewrite /sts_own.
      iCombine "Own" "Own1" as "Own".
      rewrite sts_op_auth_frag;
        [|auto| by apply sts.closed_up, tok_disj_state_singleton].

      rewrite -2!sts_op_auth_frag_up.
      iDestruct "Own" as "[[Own #Own1] Own']".

      iApply ("Post" $! v V').
      iFrame (H2) "kS' Hφd". iSplitR "Own'"; last repeat iSplitR "".
      + iModIntro. iNext. rewrite /RSLInv /sts_inv. iExists j,γ. iSplitL ""; first auto.
        iExists s. rewrite Hs. iFrame (H7) "Own ress Hist ressD".
        iExists Ψ. iFrame "Saved". by iNext.
      + iExists s,j,γ,i, Ψi. rewrite /sts_own. iFrame "SΨi Own'".
        repeat iSplitL ""; [auto|..].
        * iPureIntro. by apply HSub.
        * iPureIntro. rewrite Hs /=. by apply (alloc_local_Mono _ _ V).
        * iModIntro. iNext. iIntros "!#". iIntros (v2 V2) "?".
          case (decide (v = v2)) => [->|NEqv].
          { by rewrite fn_lookup_insert. }
          { rewrite (fn_lookup_insert_ne); last auto. by iApply "Conv". }
      + iExists s,j,γ. rewrite /sts_own. iFrame "Own1".
        iSplitL ""; first auto. iPureIntro. exists h. split.
        * right. by eexists.
        * by apply (init_local_Mono _ _ V).
      + iModIntro. iNext. iApply "Conv". iRewrite ("Eq" $! v V').
        iDestruct ("True" $! i v V' with "[#]") as "EC"; [done|].
        by iApply "EC".
  Qed.

  Lemma RMWAcqRaw_split l (φ φd: Z → vPred) V n E:
    ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n
    ⊢ |={E}=> ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n ∗ AcqRaw V (λ _, True)%VP n.
  Proof.
    iIntros "(Inv & #RR)".
    iDestruct "RR" as (s j γ) "(% & #Own & RR)".
    iDestruct "RR" as (I0 h) "(% & %)".
    iDestruct "Inv" as (j1 γ1) "(>% & Inv)". subst n.
    apply encode_inj in H2. inversion H2. subst j1 γ1.

    iMod (sts_acc with "[$Inv $Own]") as (s') "(% & Inv & SClose)".
    iMod (saved_prop_alloc_strong 
                  ((λ _ _, True): (Z -c> View -c> ∙)%CF iProp)
                  (rISet s'))
      as (i) "[% #Hi]".
    destruct (frame_steps_RMW_branch _ _ _ H _ _ H0) as [I' [h' Eq]].

    iMod ("SClose" $! (state_ISet_insert i s') ({[Change i]})
              with "[Inv]") as "(Inv & Own')". 
        { iSplitL "".
          - iPureIntro. by apply state_ISet_insert_steps.
          - iNext. by iApply (rsl_inv_insert_c with "[$Hi $Inv]"). }

    iModIntro.
    iSplitL "Inv"; last iSplitR "Own'".
    - iNext. iExists j,γ. by iFrame.
    - iExists s,j,γ. iFrame "Own". iSplitL ""; first auto.
      iExists I0, h. by iSplitL "".
    - iExists _, j,γ, i, (λ _ : Z, True%VP). iFrame "Own' Hi".
      repeat iSplitL ""; first auto.
      + iPureIntro. by apply state_ISet_insert_included.
      + iPureIntro.
        apply state_ISet_insert_alloc_local.
        apply (alloc_local_mono_rsl_state s).
        * destruct H0 as [Hs|Hs]; by rewrite Hs.
        * apply (frame_steps_steps _ _ _ H).
      + iNext. by rewrite -conv_id.
  Qed.

  Lemma AcqRaw_alloc_vs l h (φ φd: Z → vPred) V:
    Hist l h ∗ ⌜alloc_local h V⌝ ∗ ⌜uninit h⌝
    ⊢ |==> ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ AcqRaw V φ n.
  Proof.
    iIntros "(Hist & % & %)".
    iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp))
      as (i) "#SΨi".

    iAssert (rsl_inv l φ φd (aUninit {[i]} h)) with "[Hist]" as "Inv".
      { iFrame (H0) "Hist".
        iExists (<[i := (φ: Z → View → iProp)]> (λ _ _ _,True)).
        iSplitL "".
        - iNext. iIntros "!#". iIntros (v1 V1) "Hφ".
          by rewrite big_sepS_singleton fn_lookup_insert.
        - by rewrite big_sepS_singleton fn_lookup_insert. }

    iMod (own_alloc (sts_auth (aUninit {[i]} h) {[Change i]})) 
      as (γ) "Own".
      { apply sts_auth_valid, tok_disj_state_singleton => /=.
        by apply elem_of_singleton. }
    rewrite -2!sts_op_auth_frag_up.
    iDestruct "Own" as "[[OwnA #Own1] Own2]".

    iAssert (RSLInv φ φd l (encode (i,γ))) with "[Inv OwnA]" as "Inv".
      { iExists _,_. iSplitL ""; first auto.
        rewrite /sts_inv. iExists _. iFrame. }
    iModIntro.
    iExists (encode (i, γ)). iFrame "Inv".
    iSplitR "Own2".
    - iExists _,i,γ. rewrite /sts_own. iFrame "SΨi Own1".
      iFrame (H). iSplitL ""; first auto.
      iNext. by rewrite -conv_id.
    - iExists _, i, γ, i, _. rewrite /sts_own. iFrame "SΨi Own2".
      repeat iSplitL "";[auto| |auto|].
      + iPureIntro. apply i_states_in. simpl. by apply elem_of_singleton.
      + iNext. by rewrite -conv_id.
  Qed.

  Lemma AcqRaw_alloc (φ φd: Z → vPred) E π V:
    nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V }}}
      (Alloc, V) @ E
    {{{ (l: loc) V' n X, RET (LitV $ LitLoc l, V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Info l 1 X
        ∗ ▷ RSLInv φ φd l n ∗ RelRaw V' φ φ n ∗ AcqRaw V' φ n }}}%C.
  Proof.
    iIntros (? Φ) "(kI & kS) Post".
    iApply (wp_mask_mono (↑physN)); first auto.
    iApply wp_fupd.

    iApply (f_alloc with "[$kI $kS]"). iNext.
    iIntros (l V' h X) "(VV' & kS' & Hist & Info & _ & Alloc & Uninit)".

    iMod (AcqRaw_alloc_vs l h φ φd V' with "[$Hist $Alloc $Uninit]")
      as (n) "IRA".
    iApply ("Post" $! l V' n X with "[$VV' $kS' $Info $IRA]").
  Qed.


  Lemma RMWAcqRaw_alloc_vs l h (φ φd: Z → vPred) V:
    Hist l h ∗ ⌜alloc_local h V⌝ ∗ ⌜uninit h⌝
    ⊢ |==> ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ RMWAcqRaw V n.
  Proof.
    iIntros "(Hist & % & %)".
    iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp))
      as (i) "#SΨi".

    iAssert (rsl_inv l φ φd (cUninit ∅ h)) with "[$Hist]" as "Inv".
      { iFrame (H0). iExists (λ _ _ _,True).
        iSplitL "".
        - by rewrite big_sepS_empty.
        - iNext. iIntros "!#". iIntros (? ? ?) "_". by iSplit; iIntros "_". }

    iMod (own_alloc (sts_auth (cUninit ∅ h) ∅))
      as (γ) "Own".
      { apply sts_auth_valid. set_solver+. }
    rewrite -sts_op_auth_frag_up.
    iDestruct "Own" as "(OwnA & #Own')".

    iAssert (RSLInv φ φd l (encode(i,γ))) with "[Inv OwnA]" as "Inv".
      { iExists _,_. iSplitL ""; first auto.
        rewrite /sts_inv. iExists _. iFrame. }

    iModIntro. iExists _. iFrame "Inv".
    iSplitL ""; iExists _,i,γ; rewrite /sts_own; (iSplitL ""; first auto).
    - iFrame "SΨi Own'". iFrame (H). iNext. by rewrite -conv_id.
    - iFrame "Own'". iExists ∅, h. iFrame (H). by iLeft.
  Qed.

  Lemma RMWAcqRaw_alloc (φ φd: Z → vPred) E π V:
    nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V }}}
      (Alloc, V) @ E
    {{{ (l: loc) V' n X, RET (LitV $ LitLoc l, V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Info l 1 X
        ∗ ▷ RSLInv φ φd l n ∗ RelRaw V' φ φ n ∗ RMWAcqRaw V' n }}}%C.
  Proof.
    iIntros (? Φ) "(kI & kS) Post".
    iApply (wp_mask_mono (↑physN)); [done|].
    iApply wp_fupd.

    iApply (f_alloc with "[$kI $kS]").
    iNext.
    iIntros (l V' h X) "(VV' & kS' & Hist & Info & _ & Alloc & Uninit)".
    iMod (RMWAcqRaw_alloc_vs l h φ φd V' with "[$Hist $Alloc $Uninit]")
      as (n) "IRA".
    iApply ("Post" $! l V' n X with "[$VV' $kS' $Info $IRA]").
  Qed.

  Lemma AcqRaw_init_vs l h (φ φd: Z → vPred) v V:
    Hist l h ∗ ⌜alloc_local h V⌝ ∗ ⌜init l h⌝ ∗ ⌜h = {[VInj v, V]}⌝
    ∗ ▷ φ v V ∗ ▷ φd v V
    ⊢ |==> ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ AcqRaw V φ n ∗ InitRaw V n.
  Proof.
    iIntros "(Hist & % & % & % & Hφ & Hφd)".
    iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp))
      as (i) "#SΨi".

    set s:= aInit {[i]} {[i]} h (reflexivity {[i]}).
    iAssert (rsl_inv l φ φd s)
      with "[Hist Hφ Hφd]" as "Inv".
      { iFrame (H0) "Hist".
        iExists (<[i := (φ: Z → View → iProp)]> (λ _ _ _,True)).
        iSplitL ""; first iSplitL ""; last iSplitL "Hφ".
        - iNext. iIntros "!#". iIntros (v1 V1) "Hφ".
          by rewrite big_sepS_singleton fn_lookup_insert.
        - by rewrite big_sepS_singleton fn_lookup_insert.
        - by rewrite H1 !big_sepS_singleton fn_lookup_insert.
        - by rewrite H1 big_sepS_singleton. }

    iMod (own_alloc (sts_auth s {[Change i]})) 
      as (γ) "Own".
      { apply sts_auth_valid, tok_disj_state_singleton => /=.
        by apply elem_of_singleton. }
    rewrite -2!sts_op_auth_frag_up.
    iDestruct "Own" as "[[OwnA #Own1] Own2]".

    iAssert (RSLInv φ φd l (encode (i,γ))) with "[Inv OwnA]" as "Inv".
      { iExists _,_. iSplitL ""; first auto.
        rewrite /sts_inv. iExists _. iFrame. }
    iModIntro.
    iExists (encode (i, γ)). iFrame "Inv".
    iSplitL ""; last iSplitL "Own2"; iExists s,i,γ; rewrite /sts_own.
    - iFrame (H) "SΨi Own1". iSplitL ""; first auto. iNext. by rewrite -conv_id.
    - iExists i, _. iFrame (H) "SΨi Own2".
      repeat iSplitL ""; first auto.
      + iPureIntro. apply i_states_in. simpl. by apply elem_of_singleton.
      + iNext. by rewrite -conv_id.
    - iFrame "Own1". iSplitL ""; first auto.
      iExists h. iPureIntro. split.
      + left. by do 3 eexists.
      + exists (VInj v), V. by rewrite H1 elem_of_singleton.
  Qed.

  Lemma RMWAcqRaw_init_vs l h (φ φd: Z → vPred) v V:
    Hist l h ∗ ⌜alloc_local h V⌝ ∗ ⌜init l h⌝ ∗ ⌜h = {[VInj v, V]}⌝
    ∗ ▷ φ v V ∗ ▷ φd v V
    ⊢ |==> ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ RMWAcqRaw V n ∗ InitRaw V n.
  Proof.
    iIntros "(Hist & % & % & % & Hφ & Hφd)".
    iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp))
      as (i) "#SΨi".

    set s:= cInit ∅ h.
    iAssert (rsl_inv l φ φd s)
      with "[Hist Hφ Hφd]" as "Inv".
      { iFrame (H0) "Hist".
        iSplitL "Hφ"; last iSplitL "Hφd".
        - iNext.
          iApply (big_sepS_mono _ _ h with "[Hφ]");
            [by apply subseteq_gset_filter |reflexivity|].
          by rewrite H1 big_sepS_singleton.
        - by rewrite H1 big_sepS_singleton.
        - iExists (λ _ _ _,True). iSplitL "".
          + by rewrite big_sepS_empty.
          + iNext. iIntros "!#". iIntros (? ? ?) "_". by iSplit; iIntros "_". }

    iMod (own_alloc (sts_auth (cInit ∅ h) ∅))
      as (γ) "Own".
      { apply sts_auth_valid. set_solver+. }
    rewrite -sts_op_auth_frag_up.
    iDestruct "Own" as "(OwnA & #Own')".

    iAssert (RSLInv φ φd l (encode(i,γ))) with "[Inv OwnA]" as "Inv".
      { iExists _,_. iSplitL ""; first auto.
        rewrite /sts_inv. iExists _. iFrame. }

    iModIntro. iExists _. iFrame "Inv".
    iSplitL ""; last iSplitL ""; iExists s,i,γ; rewrite /sts_own;
      (iSplitL ""; first auto); iFrame "Own'".
    - iFrame (H) "SΨi". iNext. by rewrite -conv_id.
    - iExists _, h. iFrame (H). by iRight.
    - iExists h. iPureIntro. split.
      + right. by eexists.
      + exists (VInj v), V. by rewrite H1 elem_of_singleton.
  Qed.


  Lemma AcqRaw_init l v h (φ φd: Z → vPred) E π V:
    nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V
        ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝
        ∗ ▷ φ v V ∗ ▷ φd v V}}}
      ([ #l ]_na <- #v, V) @ E
    {{{ V' n, RET (#(), V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ▷ RSLInv φ φd l n
          ∗ RelRaw V' φ φ n ∗ AcqRaw V' φ n ∗ InitRaw V' n }}}%C.
  Proof.
    iIntros (? Φ) "(#kI & kS & Hist & #Alloc & Hφ & Hφd) Post".
    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.

    iApply (f_write_na with "[$kI $kS $Hist $Alloc]").
    iNext.
    iIntros (V' h') "(% & kS' & Hist' & % & % & #HInit & % & #Hh')".

    iDestruct (vPred_mono V V' with "Hφ") as "Hφ"; first auto.
    iDestruct (vPred_mono V V' with "Hφd") as "Hφd"; first auto.
    iMod (AcqRaw_init_vs l h' φ φd v V' with "[$Hist' $Hh' $HInit $Hφ $Hφd]")
      as (n) "(Inv & Rel & Acq & Init)".
      { iPureIntro. by apply alloc_init_local. }

    by iApply ("Post" $! V' n with "[$kS' $Inv $Rel $Acq $Init]").
  Qed.

  Lemma RMWAcqRaw_init l v h (φ φd: Z → vPred) E π V:
    nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V
        ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝
        ∗ ▷ φ v V ∗ ▷ φd v V}}}
      ([ #l ]_na <- #v, V) @ E
    {{{ V' n, RET (#(), V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ▷ RSLInv φ φd l n 
          ∗ RelRaw V' φ φ n ∗ RMWAcqRaw V' n ∗ InitRaw V' n }}}%C.
  Proof.
    iIntros (? Φ) "(#kI & kS & Hist & #Alloc & Hφ & Hφd) Post".
    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first done.

    iApply (f_write_na with "[$kI $kS $Hist $Alloc]").
    iNext.
    iIntros (V' h') "(% & kS' & Hist' & % & % & #HInit & % & #Hh')".

    iDestruct (vPred_mono V V' with "Hφ") as "Hφ"; first auto.
    iDestruct (vPred_mono V V' with "Hφd") as "Hφd"; first auto.
    iMod (RMWAcqRaw_init_vs l h' φ φd v V' with "[$Hist' $Hh' $HInit $Hφ $Hφd]")
      as (n) "(Inv & Rel & RMW & Init)".
      { iPureIntro. by apply alloc_init_local. }

    by iApply ("Post" $! V' n with "[$kS' $Inv $Rel $RMW $Init]").
  Qed.

  Lemma RMWAcqRaw_CAS
    l (φ φd: Z → vPred) (P: vPred) (R: bool → Z → vPred) (v_r v_w: Z) V n E π:
    nclose physN ⊆ E →
    □ (∀ V v, φd v V -∗ φd v V ∗ φd v V)
    ⊢
    {{{ (P ∗ ▷ φ v_r ∗ ▷ φd v_r ={ E }=∗ ▷ φ v_w∗ ▷ φd v_w ∗ R true v_r)%VP V
        ∗ (∀ v, ■ (v ≠ v_r) → P ∗ ▷ φd v ={ E }=∗ R false v)%VP V
        ∗ inv physN PSInv ∗ ▷ Seen π V 
        ∗ ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n ∗ InitRaw V n ∗ P V}}}
        (CAS #l #v_r #v_w, V) @ E
    {{{ (b: bool) (v': Z) V', RET (LitV $ LitInt b, V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' 
          ∗ ▷ RSLInv φ φd l n ∗ RMWAcqRaw V' n ∗ InitRaw V' n ∗ R b v' V'}}}.
  Proof.
    iIntros (?) "#Dup !#".
    iIntros (Φ) "(CSucc & CFail & #kI & kS & Inv & #RR & #IR & P) Post".
    iDestruct "RR" as (s1 j γ) "(% & #Own1 & RR)".
    iDestruct "RR" as (I1 h1) "(R1 & R2)".
    iDestruct "R1" as %Hs1.
    iDestruct "R2" as %Alloc1.
    iDestruct "IR" as (s2 j1 γ1) "(% & #Own2 & IR)". subst n.
    apply encode_inj in H1. inversion H1. subst j1 γ1.
    iDestruct "IR" as %[h2 [IR Init2]].
    rewrite {1}/RSLInv /sts_inv.
    iDestruct "Inv" as (j2 γ2) "(>% & Inv)".
    apply encode_inj in H0. inversion H0. subst j2 γ2.

    iDestruct "Inv" as (s) "(>Own & Inv)".
    iDestruct (sts_auth_frag_in_up with "[$Own $Own1]") as %Up1.
    iDestruct (sts_auth_frag_in_up with "[$Own $Own2]") as %Up2.
    assert (∃ I' h', s = cInit I' h')%type as [I0 [h Hs]].
      { eapply (frame_steps_RMW_Init_branch _ s1 s2 ∅ ∅); [auto|auto|..].
        - by do 2 eexists.
        - by eexists. }
    assert (HInit: init_local (rhist s) V).
      { apply (init_local_mono_rsl_state s2).
        - move: IR => [[? [? [? ->]]]|[? ->]] //=.
        - by apply (frame_steps_steps _ _ ∅). }
    assert (HAlloc: alloc_local (rhist s) V).
      { apply (alloc_local_mono_rsl_state s1).
        - move: Hs1 => [->|->] //=.
        - by apply (frame_steps_steps _ _ ∅). }
    rewrite {2}Hs.

    iDestruct "Inv" as "(Hist & >% & ress & ressD & ress2)".

    iApply wp_fupd.
    iApply (wp_mask_mono (↑physN)); first auto.
    iApply (f_CAS with "[$kI $kS $Hist]").
      { iPureIntro.
        rewrite (_: h = (rhist s)); last by rewrite Hs. exact HInit. }
    iNext.
    iIntros (b V' h') "(% & kS' & Hist' & % & CCase)".
    iDestruct "CCase" as (V1 v) "(% & % &% & CCase)".

    (* get dup interpretation *)

    iDestruct (big_sepS_dup_later _ _ _ H6 with "[$ressD Dup]")
      as "(Hφd & ressD)".
      { iNext. iIntros "?". by iApply "Dup". }

    iAssert (▷ (φd v) V') with "[Hφd]" as "Hφd".
      { iNext. by iApply (vPred_mono V1). }

    iDestruct (vPred_mono V V' with "P") as "P"; first auto.

    iDestruct "CCase" as "[CS|CF]".
    - iDestruct "CS" as "(% & % & % & % & % & % & % & %)".
      subst v.
      iMod (Hist_hTime_ok with "[$kI $Hist']") as "(Hist' & %)"; first auto. 

      assert (HO: hTime_ok l ({[VInj v_w, V']} ∪ h)).
        { apply (hTime_ok_mono _ _ h'); [by rewrite H11|auto]. }
      assert (Hp': ((VInj v_r), V1) ∈ gblock_ends l h).
        { apply elem_of_filter. split; last auto.
          move => p0 In0 Eq0 [t0 [Eqt1 Eqt2]].
          apply H15. exists p0. split; [auto|by eexists]. }

      assert (Hp: (VInj v_w, V') ∉ gblock_ends l h).
        {  move => Inm0.
           apply (elem_of_subseteq (gblock_ends l h) h) in Inm0;
           last by apply subseteq_gset_filter.
           by apply disjoint_singleton_r in H12. }

      (* ghost state update *)
      pose s' := cInit I0 h'.

      iMod (own_update _ _ (sts_auth s' ∅) with "[$Own]") as "Own".
        { apply sts_update_auth, rtc_once. rewrite Hs /s'.
          constructor => //; first by econstructor.
          - apply disjoint_empty_r.
          - apply disjoint_empty_r. }
      rewrite -sts_op_auth_frag_up.
      iDestruct "Own" as "(Own & #Own')". 

      (* get full interpretation *)
      rewrite (big_sepS_delete _ _ _ Hp').
      iDestruct "ress" as "(Hφ & ress)".
      iAssert (▷ (φ v_r) V') with "[Hφ]" as "Hφ".
        { iNext. by iApply (vPred_mono V1). }

      iMod ("CSucc" $! V' with "[] [$P $Hφ $Hφd]") as "(Hφ & Hφd & R)";
        first auto.

      iCombine "Hφd" "ressD" as "ressD".
      rewrite -(big_sepS_insert _ _ (VInj v_w, V')); last first.
        { by apply disjoint_singleton_r in H12. }

      iCombine "Hφ" "ress" as "ress".
      rewrite -(big_sepS_insert _ _ (VInj v_w, V')); last first.
        { move => In. by apply gset_difference_subseteq in In. }

      iApply ("Post" $! _ v_r V').
      rewrite H8. iFrame (H3) "kS' R".
      iSplitR ""; last iSplitL "".
      + rewrite /RSLInv /sts_inv.
        iExists j, γ. iModIntro. iSplitL ""; first by iNext.
        iExists s'. iFrame "Own Hist' ress2".
        iNext. iSplit; first auto. iNext. iSplitR "ressD"; last first.
        * iApply (big_sepS_mono _ _ _ h' with "[$ressD]");
            last reflexivity.
          by rewrite H11 union_comm_L.
        * iApply (big_sepS_mono _ _ _ (gblock_ends l h') with "[$ress]");
            last reflexivity.
          rewrite H11.
          apply gblock_ends_ins_sL_update => //; last by apply HO.
          by apply gblock_ends_ins_update.
      + iExists s',j,γ. rewrite /sts_own.
        iFrame "Own'". iSplitL ""; first auto.
        iExists I0, h'. iSplitL ""; iPureIntro; first by right.
        by exists (VInj v_w), V'.
      + iExists s',j,γ. rewrite /sts_own. iFrame "Own'". iSplitL ""; first auto.
        iPureIntro. exists h'. split.
        * right. by eexists.
        * by exists (VInj v_w), V'.

    - iDestruct "CF" as "(% & #NE & % & %)".
      iMod ("CFail" $! v V' with "[] NE [] [$P $Hφd]") as "R";
        [auto|auto|].

      rewrite -sts_op_auth_frag_up.
      iDestruct "Own" as "(Own & #Own')".

      iApply ("Post" $! _ v V'). rewrite H8. iFrame (H3) "kS' R".
      iSplitR ""; last iSplitL "".
      + rewrite /RSLInv /sts_inv. iModIntro. iNext. iExists j,γ.
        iSplitL ""; first auto. iExists s. iFrame.
        rewrite Hs H9 /rsl_inv. by iFrame.
      + iExists s,j,γ. rewrite /sts_own. iFrame "Own'". iSplitL ""; first auto.
        iExists I0, h. iSplitL ""; iPureIntro; first by right.
        rewrite Hs in HAlloc. by apply (alloc_local_Mono _ _ V).
      + iExists s,j,γ. rewrite /sts_own. iFrame "Own'". iSplitL ""; first auto.
        iPureIntro. exists h. split.
        * right. by eexists.
        * rewrite Hs in HInit. by apply (init_local_Mono _ _ V).
  Qed.

  Section RSL_SplitJoin.

  Lemma ress_inv_join i1 i2 i I1 I2 Ψi Ψi1 Ψi2 φR Ψ:
      I2 ⊆ I1 →
      i ∉ I1 → i1 ∈ I2 → i2 ∈ I2 → i1 ≠ i2 →
      saved_prop_own i1 Ψi1 ∗ saved_prop_own i2 Ψi2
        ∗ saved_prop_own i Ψi
        ∗ conv (λ v V, Ψi1 v V ∗ Ψi2 v V) Ψi
        ∗ ress φR Ψ I2
        ⊢ ress φR (<[i:=(Ψi: Z → View → iProp)]>Ψ) ({[i]} ∪ I2 ∖ {[i1; i2]}).
  Proof.
    move => Sub Ini Ini1 Ini2 iEq.
    assert (i ∉ I2 ∖ {[i1; i2]}).
      { rewrite elem_of_difference. move => [? _]. by apply Ini, Sub. }
    assert (i2 ∈ I2 ∖ {[i1]}).
      { by rewrite elem_of_difference elem_of_singleton. }
    assert (HSub: I2 ∖ {[i1; i2]} ⊆ I2 ∖ {[i1]} ∖ {[i2]}).
      { by rewrite difference_twice_union. }

    iIntros "(#Hi1 & #Hi2 & #Hi & #Conv & ress)".
    iDestruct "ress" as "(#sConv & #Saved)".
    iDestruct (big_sepS_delete _ _ i1 with "Saved")
      as "[#SΨi1 #Saved1]"; [done|].
    iDestruct (big_sepS_delete _ _ i2 with "Saved1")
      as "[#SΨi2 #Saved2]"; [done|].
    iSplit.
    - iDestruct (rsl_saved_pred_unfold i1 Ψi1 (Ψ i1) with "[$Hi1 $SΨi1]") as "#Heq1".
      iDestruct (rsl_saved_pred_unfold i2 Ψi2 (Ψ i2) with "[$Hi2 $SΨi2]") as "#Heq2".
      iNext. iIntros "!#". iIntros (v' V') "ress".
      iAssert ([∗ set] i0 ∈ I2, Ψ i0 v' V') with "[ress]" as "ress".
        { by iApply "sConv". }
      iDestruct (big_sepS_delete _ _ i1 with "ress")
        as "[rΨi1 ress]"; [done|].
      iDestruct (big_sepS_delete _ _ i2 with "ress")
        as "[rΨi2 ress]"; [done|].
      iAssert (Ψi v' V') with "[rΨi1 rΨi2]" as "rΨi".
        { iApply "Conv". iSplitL "rΨi1".
          - by iRewrite ("Heq1" $! v' V').
          - by iRewrite ("Heq2" $! v' V'). }
      rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v' V')); [|done].
      iFrame.
      by iApply (big_sepS_mono _ _ _ _ HSub with "[$ress]").
    - rewrite (big_sepS_fn_insert
                (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp))); [|done].
      iSplit; [done|].
      by iApply (big_sepS_mono _ _ _ _ HSub).
  Qed.

  Lemma ress2_inv_join i1 i2 i I1 Ψi Ψi1 Ψi2 φR:
    i ∉ I1 → i1 ∈ I1 → i2 ∈ I1 → i1 ≠ i2 →
    saved_prop_own i1 Ψi1 ∗ saved_prop_own i2 Ψi2
      ∗ saved_prop_own i Ψi
      ∗ conv (λ v V, Ψi1 v V ∗ Ψi2 v V) Ψi
      ∗ (∃ Ψ , ress2 φR Ψ I1)
      ⊢ ∃ Ψ , ress2 φR Ψ ({[i]} ∪ I1 ∖ {[i1; i2]}).
  Proof.
    move => Ini Ini1 Ini2 iEq.
    iIntros "(#Hi1 & #Hi2 & #Hi & #Conv & ress2)".
    iDestruct "ress2" as (Ψ) "(#Saved & #EConv)".
    assert (i ∉ I1 ∖ {[i1; i2]}).
      { rewrite elem_of_difference. move => [? _]. by apply Ini. }
    assert (i2 ∈ I1 ∖ {[i1]}).
      { by rewrite elem_of_difference elem_of_singleton. }
    assert (HSub: I1 ∖ {[i1; i2]} ⊆ I1 ∖ {[i1]} ∖ {[i2]}).
      { by rewrite difference_twice_union. }

    iDestruct (big_sepS_delete _ _ i1 with "Saved")
        as "[#SΨi1 #Saved1]"; [done|].
    iDestruct (big_sepS_delete _ _ i2 with "Saved1")
        as "[#SΨi2 #Saved2]"; [done|].

    iExists (<[i:=(Ψi: Z → View → iProp)]> Ψ).

    iSplit.
    - rewrite big_sepS_insert ?fn_lookup_insert; [|done]. iSplit; [done|].
      iApply (big_sepS_mono
                (λ (i: gname), saved_prop_own i (Ψ i: (Z -c> View -c> ∙)%CF iProp))).
      + apply HSub.
      + move => i' In' /=. apply gset_difference_subseteq in In'.
        rewrite fn_lookup_insert_ne => //.
        move => ?; subst. by apply Ini. 
      + done.
    - iDestruct (rsl_saved_pred_unfold i1 Ψi1 (Ψ i1) with "[$Hi1 $SΨi1]") as "#Heq1".
      iDestruct (rsl_saved_pred_unfold i2 Ψi2 (Ψ i2) with "[$Hi2 $SΨi2]") as "#Heq2".
      iNext. iIntros "!#". iIntros (i' v' V') "Ini'".
      iDestruct "Ini'" as %[->%elem_of_singleton_1|Ini']%elem_of_union.
      + rewrite fn_lookup_insert.
        iSplit; iIntros "_"; [done|]. iApply "Conv".
        iSplitL.
        * iRewrite ("Heq1" $! v' V').
          iDestruct ("EConv" $! i1 v' V' with "[#]") as "EC"; [done|].
          by iApply "EC".
        * iRewrite ("Heq2" $! v' V').
          iDestruct ("EConv" $! i2 v' V' with "[#]") as "EC"; [done|].
          by iApply "EC".
      + rewrite fn_lookup_insert_ne; last first.
          { move => ?. by subst. }
        iApply "EConv".
        by apply elem_of_difference in Ini' as [].
  Qed.

  Lemma rsl_inv_join l i1 i2 i s Ψi Ψi1 Ψi2 (φd: Z → vPred) φR:
      i ∉ rISet s → i1 ∈ rISet2 s → i2 ∈ rISet2 s → i1 ≠ i2 →
        saved_prop_own i1 Ψi1 ∗ saved_prop_own i2 Ψi2
        ∗ saved_prop_own i Ψi
        ∗ conv (λ v V, Ψi1 v V ∗ Ψi2 v V) Ψi
        ∗ rsl_inv l φR φd s ⊢ rsl_inv l φR φd (state_ISet_join i1 i2 i s).
  Proof.
    iIntros (Ini Ini1 Ini2 ?) "(#Hi1 & #Hi2 & #Hi & #Conv & Inv)".
    rewrite /rsl_inv /state_ISet_join /ISet_join.
      destruct s as [I0 h|I1 I2 h Sub|I0 h|I0 h].
      - iDestruct "Inv" as "(? & ? & ress)". iFrame.
        iDestruct "ress" as (Ψ) "ress".
        iExists (<[i:=(Ψi: Z → View → iProp)]> Ψ).
        iApply ress_inv_join; try done; [].
        iFrame. repeat iSplit; done.
      - iDestruct "Inv" as "(? & ? & ress)". iFrame.
        iDestruct "ress" as (Ψ) "((#sConv & #Saved) & RS & RSd)".
        iExists (<[i:=(Ψi: Z → View → iProp)]> Ψ).
        iFrame "RSd".
        iDestruct (big_sepS_delete _ _ i1 with "Saved")
        as "[#SΨi1 #Saved1]".
          { by apply Sub. }
        iDestruct (big_sepS_delete _ _ i2 with "Saved1")
        as "[#SΨi2 #Saved2]".
          { apply elem_of_difference.
            split. by apply Sub.
            move => /elem_of_singleton ?. by subst. }

        iDestruct (rsl_saved_pred_unfold i1 Ψi1 (Ψ i1) with "[$Hi1 $SΨi1]") as "#Heq1".
        iDestruct (rsl_saved_pred_unfold i2 Ψi2 (Ψ i2) with "[$Hi2 $SΨi2]") as "#Heq2".
        iSplitL "".
          * iApply ress_inv_join; try done. by apply Sub. by apply Sub.
            iSplitL; first by iApply "Hi1".
            iSplitL; first by iApply "Hi2". 
            by iFrame "#".
          * iNext. iApply (big_sepS_impl _ _ h with "[$RS]").
            iIntros "!#". iIntros ([[| |v'] V']) "% ress"; [done|done|..].
            iDestruct (big_sepS_delete _ _ i1 with "ress") as "[rΨi1 ress]"; [done|].
            iDestruct (big_sepS_delete _ _ i2 with "ress") as "[rΨi2 ress]".
              { apply elem_of_difference. split => //.
                move => /elem_of_singleton ?. by subst. }
            rewrite big_sepS_insert; last first.
              { move => In1. apply gset_difference_subseteq in In1.
                by apply Ini, Sub. }
            rewrite fn_lookup_insert.
            iSplitL "rΨi1 rΨi2".
              { iApply "Conv". iSplitL "rΨi1".
                - by iRewrite ("Heq1" $! v' V').
                - by iRewrite ("Heq2" $! v' V'). }
            iApply (big_sepS_mono (λ (i: gname), Ψ i v' V')); last by iFrame.
              { by rewrite difference_twice_union. }
            move => i' In' /=. apply gset_difference_subseteq in In'.
            rewrite fn_lookup_insert_ne => //.
            move => ?; subst. by apply Ini, Sub.
      - iDestruct "Inv" as "(? & ? & ress2)". iFrame.
        iApply ress2_inv_join; try done; []. by iFrame "∗ #".
      - iDestruct "Inv" as "(? & ? & ? & ? & ress2)". iFrame.
        iApply ress2_inv_join; try done; []. by iFrame "∗ #".
    Qed.

    Lemma AcqRaw_join l (φ1 φ2 φR φd: Z → vPred) n V E:
      ▷ RSLInv φR φd l n ∗ AcqRaw V φ1 n ∗ AcqRaw V φ2 n
      ⊢ |={E}=> ▷ RSLInv φR φd l n ∗ AcqRaw V (λ v, φ1 v ∗ φ2 v)%VP n.
    Proof.
      iIntros "(Inv & RR1 & RR2)".
      rewrite {1}/RSLInv.
      iDestruct "RR1" as (s1 j γ i1 Ψi1) "(% & Own1 & % & % & #Conv1 & #SP1)".
      iDestruct "RR2" as (s2 j2 γ2 i2 Ψi2) "(% & Own2 & % & % & #Conv2 & #SP2)".
      subst n. apply encode_inj in H2. inversion H2. subst j2 γ2.
      rewrite /sts_inv /sts_own.
      iDestruct "Inv" as (j3 γ3) "(>% & Inv)".
      iDestruct "Inv" as (s) "(>OwnA & RSLInv)".
      apply encode_inj in H. inversion H. subst j3 γ3.
      iDestruct (own_valid with "Own1") as %[C1 _]%sts_frag_valid.
      iDestruct (own_valid with "Own2") as %[C2 _]%sts_frag_valid.
      iCombine "Own1" "Own2" as "Own".
      iDestruct (own_valid with "Own") as %C3.
      destruct C3 as [_ [_ C3]]. inversion C3 as [? ? ? ? ? C4 | |].
      rewrite -sts_op_frag; [|auto|auto|auto].

      iCombine "OwnA" "Own" as "OwnA".
      iDestruct (own_valid with "OwnA") 
        as %[Hs1 Hs2]%sts_auth_frag_valid_inv%elem_of_intersection.
      assert (Hi1: s ∈ i_states i1).
        { eapply (sts.up_subseteq s1); eauto. by apply i_states_closed. }
      assert (Hi2: s ∈ i_states i2).
        { eapply (sts.up_subseteq s2); eauto. by apply i_states_closed. }
      rewrite sts_op_auth_frag; [|done|by apply sts.closed_op].

      pose Ψi := (λ v V, Ψi1 v V ∗ Ψi2 v V).
      iMod (saved_prop_alloc_strong (Ψi: (Z -c> View -c> ∙)%CF iProp) (rISet s))
        as (i) "[% #Hi]".
      pose s' := state_ISet_join i1 i2 i s.

      assert (alloc_local (rhist s') V).
        { apply state_ISet_join_alloc_local.
          eapply alloc_local_mono_rsl_state; first exact H4.
          by apply (frame_steps_steps _ _ {[Change i2]}). }

      iMod (own_update _ _ (sts_auth s' {[Change i]}) with "OwnA") as "OwnA".
        { by apply sts_update_auth, state_ISet_join_steps. }

      rewrite -sts_op_auth_frag_up. iDestruct "OwnA" as "(OwnA & Owni)".
      rewrite /RSLInv /sts_inv.
      iModIntro. iSplitR "Owni".
      - iNext. iExists j,γ. iSplitL ""; first auto. iExists s'. iFrame "OwnA".
        assert (i1 ≠ i2).
        { move => ?. subst. set_solver+C4. }
        iApply rsl_inv_join; try done; []. iFrame "∗ #".
        iIntros "!#". iIntros (? ?) "(? & ?)". by iFrame.
      - iExists s', j,γ, i, Ψi. rewrite /sts_own.
        iFrame (H11) "Owni Hi". repeat iSplitL ""; first auto.
        + iPureIntro. by apply state_ISet_join_included.
        + iNext. iIntros "!#". iIntros (? ?) "(HΨ1 & HΨ2)".
          iSplitL "HΨ1".
          * by iApply "Conv1".
          * by iApply "Conv2".
    Qed.

    Lemma RelRaw_split (φ1 φ2 φR: Z → vPred) n V:
      RelRaw V (λ v, φ1 v ∨ φ2 v)%VP φR n
      ⊢  RelRaw V φ1 φR n ∗ RelRaw V φ2 φR n.
    Proof.
      iIntros "#RR". iDestruct "RR" as (s j γ) "(% & #oF & % & #Conv & #Saved)".
      iSplitL ""; iExists s,j,γ; iFrame (H H0) "oF Saved";
        iNext; iIntros "!#"; iIntros (? ?) "?";
        iApply "Conv"; [by iLeft|by iRight].
    Qed.

    Lemma RelRaw_join (φ1 φ2 φR1 φR2: Z → vPred) n V:
      RelRaw V φ1 φR1 n ∗ RelRaw V φ2 φR2 n
      ⊢  RelRaw V (λ v, φ1 v ∨ φ2 v)%VP φR1 n.
    Proof.
      iIntros "(RR1 & RR2)".
      iDestruct "RR1" as (s1 j1 γ1) "(% & #Rel1 & % & #Conv1 & #SP1)".
      iDestruct "RR2" as (s2 j2 γ2) "(% & #Rel2 & % & #Conv2 & #SP2)".
      subst n. apply encode_inj in H1. inversion H1. subst.
      iDestruct (rsl_saved_pred_unfold j2 (λ x, φR1 x) (λ x, φR2 x) with "[$SP1 $SP2]") as "#LEq".
      iExists s1,j2,γ2. iFrame (H0) "Rel1 SP1". iSplitL ""; first auto.
      iNext. iIntros "!#". iIntros (v' V') "[?|?]".
      - by iApply "Conv1".
      - iRewrite ("LEq" $! v' V'). by iApply "Conv2".
    Qed.

    Lemma ress_inv_split_1 i1 i2 i I0 I2 Ψi (φ1 φ2 : Z → vPred) φR Ψ:
      I2 ⊆ I0 →
      i ∈ I2 → i1 ∉ I0 → i2 ∉ I0 → i1 ≠ i2 →
      saved_prop_own i1 φ1 ∗ saved_prop_own i2 φ2
        ∗ saved_prop_own i Ψi
        ∗ conv Ψi (λ v, φ1 v ∗ φ2 v)%VP
        ∗ ress φR Ψ I2
        ⊢ ress φR (<[i2:=(φ2: Z → View → iProp)]> (<[i1:=(φ1: Z → View → iProp)]> Ψ))
                  ({[i1; i2]} ∪ I2 ∖ {[i]}).
    Proof.
      move => Sub Ini Ini1 Ini2 iEq.
      iIntros "(#Hi1 & #Hi2 & #Hi & #Conv & ress)".
      iDestruct "ress" as "(#sConv & #Saved)".
      assert (i2 ∉ {[i1]} ∪ I2 ∖ {[i]}).
        { rewrite elem_of_union elem_of_difference elem_of_singleton.
          move => [?|[? _]]; first by subst. by apply Ini2, Sub. }
      assert (i1 ∉ I2 ∖ {[i]}).
        { rewrite elem_of_difference. move => [? _]. by apply Ini1, Sub. }
      assert ({[i1; i2]} ∪ I2 ∖ {[i]} ⊆ {[i2]} ∪ ({[i1]} ∪ I2 ∖ {[i]})).
        { rewrite union_assoc. apply union_mono_r.
          by rewrite union_comm. }

      iDestruct (big_sepS_delete _ _ i with "Saved")
          as "[#SΨi #Saved']"; [done|].
      iSplit.
      - iDestruct (rsl_saved_pred_unfold i Ψi (Ψ i) with "[$Hi $SΨi]") as "#Heq".
        iNext. iIntros "!#". iIntros (v' V').

        iIntros "ress".
        iAssert ([∗ set] i0 ∈ I2, Ψ i0 v' V') with "[ress]" as "ress".
          { by iApply "sConv". }
        iDestruct (big_sepS_delete _ _ i with "ress")
          as "[rΨi ress]"; [done|].
        iAssert (Ψi v' V') with "[rΨi]" as "rΨi".
          { by iRewrite ("Heq" $! v' V'). }
        iDestruct ("Conv" $! v' V' with "rΨi") as "(R1 & R2)".

        iApply (big_sepS_mono _ _  ({[i2]} ∪ ({[i1]} ∪ I2 ∖ {[i]}))); [done|done|].
        rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v' V')); [|done].
        rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v' V')); [|done].
        by iFrame.
      - iApply (big_sepS_mono _ _  ({[i2]} ∪ ({[i1]} ∪ I2 ∖ {[i]}))); [done|done|].
        rewrite (big_sepS_fn_insert
                  (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp))); [|done].
        rewrite (big_sepS_fn_insert
                  (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp))); [|done].
        by repeat iSplit.
    Qed.

    Lemma ress2_inv_split i1 i2 i I0 Ψi (φ1 φ2 : Z → vPred) φR:
      i ∈ I0 → i1 ∉ I0 → i2 ∉ I0 → i1 ≠ i2 →
      saved_prop_own i1 φ1 ∗ saved_prop_own i2 φ2
        ∗ saved_prop_own i Ψi
        ∗ conv Ψi (λ v, φ1 v ∗ φ2 v)%VP
        ∗ (∃ Ψ , ress2 φR Ψ I0)
        ⊢ ∃ Ψ , ress2 φR Ψ ({[i1; i2]} ∪ I0 ∖ {[i]}).
    Proof.
      move => Ini Ini1 Ini2 iEq.
      iIntros "(#Hi1 & #Hi2 & #Hi & #Conv & ress2)".
      iDestruct "ress2" as (Ψ) "(#Saved & #EConv)".

      assert (i2 ∉ {[i1]} ∪ I0 ∖ {[i]}).
        { rewrite elem_of_union elem_of_difference elem_of_singleton.
          move => [?|[? _]]; first by subst. by apply Ini2. }
      assert (i1 ∉ I0 ∖ {[i]}).
        { rewrite elem_of_difference. move => [? _]//. }
      assert ({[i1; i2]} ∪ I0 ∖ {[i]} ⊆ {[i2]} ∪ ({[i1]} ∪ I0 ∖ {[i]})).
        { rewrite union_assoc. apply union_mono_r.
          by rewrite union_comm. }

      iDestruct (big_sepS_delete _ _ i with "Saved")
          as "[#SΨi #Saved']"; [done|].

      iExists (<[i2:=(φ2: Z → View → iProp)]> (<[i1:=(φ1: Z → View → iProp)]> Ψ)).

      iSplit.
      - iApply (big_sepS_mono _ _  ({[i2]} ∪ ({[i1]} ∪ I0 ∖ {[i]}))); [done|done|..].
        rewrite (big_sepS_fn_insert
                  (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp))); [|done].
        rewrite (big_sepS_fn_insert
                   (λ i0 f, saved_prop_own i0 (f: (Z -c> View -c> ∙)%CF iProp))); [|done].
        by iFrame "#".
      - iDestruct (rsl_saved_pred_unfold i Ψi (Ψ i) with "[$Hi $SΨi]") as "#Heq".
        iNext. iIntros "!#". iIntros (i' v' V') "%".
        iDestruct ("EConv" $! i v' V' with "[#]") as "EC"; [done|].
        case (decide (i' = i1)) => [->|?].
          { rewrite fn_lookup_insert_ne ?fn_lookup_insert; [|done].
            iSplit; iIntros "_"; [done|].
            iAssert (Ψi v' V') as "rΨi".
              { iRewrite ("Heq" $! v' V'). by iApply "EC". }
            by iDestruct ("Conv" $! v' V' with "[$rΨi]") as "(? & _)". }
        case (decide (i' = i2)) => [->|?].
          { rewrite fn_lookup_insert.
            iSplit; iIntros "_"; [done|].
            iAssert (Ψi v' V') as "rΨi".
              { iRewrite ("Heq" $! v' V'). by iApply "EC". }
            by iDestruct ("Conv" $! v' V' with "[$rΨi]") as "(_ & ?)". }
        rewrite !fn_lookup_insert_ne; [|auto|auto].
        case (decide (i' = i)) => [->|?]; first auto.
        iApply "EConv". iPureIntro. move : H2.
        rewrite -assoc_L !elem_of_union elem_of_difference !elem_of_singleton.
        move => [?|[?|[? _]]] => //.
    Qed.

    Lemma rsl_inv_split l i1 i2 i s Ψi (φ1 φ2 φd: Z → vPred) φR:
      i ∈ rISet2 s → i1 ∉ rISet s → i2 ∉ rISet s → i1 ≠ i2 →
        saved_prop_own i1 φ1 ∗ saved_prop_own i2 φ2
        ∗ saved_prop_own i Ψi
        ∗ conv Ψi (λ v, φ1 v ∗ φ2 v)%VP
        ∗ rsl_inv l φR φd s ⊢ rsl_inv l φR φd (state_ISet_split i1 i2 i s).
    Proof.
      move => Ini Ini1 Ini2 iEq.
      iIntros "(#Hi1 & #Hi2 & #Hi & #Conv & Inv)".
      rewrite /rsl_inv /state_ISet_split /ISet_split.
      destruct s as [I0 h|I1 I2 h Sub|I0 h|I0 h].
      - iDestruct "Inv" as "(? & ? & ress)". iFrame.
        iDestruct "ress" as (Ψ) "ress".
        iExists _. iApply ress_inv_split_1; try done; [].
        by iFrame "∗ #".
      - iDestruct "Inv" as "(? & ? & ress)". iFrame.
        iDestruct "ress" as (Ψ) "((#sConv & #Saved) & RS & RSd)".
        iExists (<[i2:=(φ2: Z → View → iProp)]> (<[i1:=(φ1: Z → View → iProp)]> Ψ)).
        iFrame "RSd".
        iDestruct (big_sepS_delete _ _ i with "Saved")
        as "[#SΨi #Saved']".
          { by apply Sub. }
        iDestruct (rsl_saved_pred_unfold i Ψi (Ψ i) with "[$Hi $SΨi]") as "#Heq".
        iSplitL "".
          * iApply ress_inv_split_1; try done; [by apply Sub|].
            by iFrame "Hi #".
          * iNext. iApply (big_sepS_impl _ _ h with "[$RS]").
            iIntros "!#". iIntros ([[| |v'] V']) "% ress"; [done|done|].
            iDestruct (big_sepS_delete _ _ i with "ress") as "[rΨi ress]"; [done|].
            iAssert (Ψi v' V') with "[rΨi]" as "rΨi".
              { by iRewrite ("Heq" $! v' V'). }
            iDestruct ("Conv" $! v' V' with "rΨi") as "(R1 & R2)".
            iApply (big_sepS_mono _ _  ({[i2]} ∪ ({[i1]} ∪ I2 ∖ {[i]}))); try done; [|].
              { rewrite union_assoc. apply union_mono_r.
                by rewrite union_comm. }
            rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v' V'));
              last first.
              { rewrite elem_of_union elem_of_difference elem_of_singleton.
                move => [?|[? _]]; first by subst i2. by apply Ini2, Sub. }
            rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v' V'));
              last first.
              { rewrite elem_of_difference. move => [? _].
                by apply Ini1, Sub. }
            by iFrame.
      - iDestruct "Inv" as "(? & ? & ress2)". iFrame.
        iApply ress2_inv_split; try done; []. by iFrame "∗ #".
      - iDestruct "Inv" as "(? & ? & ? & ? & ress2)". iFrame.
        iApply ress2_inv_split; try done; []. by iFrame "∗ #".
    Qed.

    Lemma AcqRaw_split l (φ1 φ2 φR φd: Z → vPred) V n E:
      ▷ RSLInv φR φd l n ∗ AcqRaw V (λ v : Z, (φ1 v ∗ φ2 v)%VP) n 
      ⊢ |={E}=> ▷ RSLInv φR φd l n ∗ AcqRaw V φ1 n ∗ AcqRaw V φ2 n.
    Proof.
      iIntros "[Inv AC]".
      iDestruct "AC" as (s j γ i Ψi) "(% & Own & % & % & #Conv & #Saved)".
      iDestruct "Inv" as (j1 γ1) "(>% & Inv)". subst n.
      apply encode_inj in H2. inversion H2. subst j1 γ1.
      rewrite {1}/RSLInv.
      iMod (sts_acc with "[$Inv $Own]") as "IO".
      iDestruct "IO" as (s') "(Step & Inv & HClose2)".
      iDestruct "Step" as %Step.
      assert (s' ∈ i_states i).
        { move: Step. apply sts.closed_steps; last auto.
          by apply i_states_closed. }

      iMod (saved_prop_alloc_strong (φ1: (Z -c> View -c> ∙)%CF iProp) (rISet s'))
        as (i1) "[% #Hi1]".
      iMod (saved_prop_alloc_strong (φ2: (Z -c> View -c> ∙)%CF iProp) (rISet s' ∪ {[i1]}))
        as (i2) "[Hi2' #Hi2]"; iDestruct "Hi2'" as %Hi2.
      rewrite ->not_elem_of_union, elem_of_singleton in Hi2; destruct Hi2.

      pose s'' := state_ISet_split i1 i2 i s'.

      assert (alloc_local (rhist s'') V).
        { apply state_ISet_split_alloc_local.
          eapply alloc_local_mono_rsl_state; first exact H1.
          by eapply frame_steps_steps. }


      iMod ("HClose2" $! s'' {[Change i1; Change i2]} with "[Inv]")
        as "(Inv & Own)".
        { iSplitL "".
          - iPureIntro. by apply state_ISet_split_steps.
          - iNext.
            by iApply (rsl_inv_split with "[$Inv $Conv $Saved $Hi1 $Hi2]"). }

      assert (sts.closed (sts.up s'' {[Change i1]}) {[Change i1]}).
        { apply sts.closed_up, state_ISet_split_token_disj_1. }

      assert (sts.closed (sts.up s'' {[Change i2]}) {[Change i2]}).
        { apply sts.closed_up, state_ISet_split_token_disj_2. }

      iAssert (|==> sts_own γ s'' {[Change i1]} ∗ sts_own γ s'' {[Change i2]})
        with "[Own]" as ">(O1 & O2)".
        { rewrite -sts_ownS_op; [..|auto|auto].
          - iApply (sts_own_weaken with "Own"); [auto| |by apply sts.closed_op].
            apply elem_of_intersection. split; apply sts.elem_of_up.
          - apply disjoint_singleton_l.
            move => /elem_of_singleton. inversion 1. by subst. }

      iModIntro. iSplitL "Inv"; last iSplitL "O1".
      - iNext. iExists j,γ. by iFrame.
      - iExists _, j, γ, i1, φ1. iFrame (H6) "O1 Hi1 #".
        iSplitL; [auto|iSplitL].
        + iPureIntro. apply state_ISet_split_included_1.
        + iNext. iIntros "!#". iIntros (? ?). auto.
      - iExists _, j, γ, i2, φ2. iFrame (H6) "O2 Hi2".
        iSplitL; [auto|iSplitL].
        + iPureIntro. apply state_ISet_split_included_2.
        + iNext. iIntros "!#". iIntros (? ?). auto.
    Qed.

  End RSL_SplitJoin.

  Close Scope I.
End proof.
