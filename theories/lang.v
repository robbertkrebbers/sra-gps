From iris.program_logic Require Export ectx_language ectxi_language.
From stdpp Require Export strings gmap.
From iris.algebra Require Import ofe.

From igps Require Export machine.

Module base.
  Open Scope Z_scope.

  (** Expressions and vals. *)

  Inductive base_lit : Set :=
    | LitInt (n : Z) | LitUnit | LitLoc (l : loc).
  Inductive un_op : Set :=
    | NegOp | MinusUnOp | CastIntOp | CastLocOp.
  Inductive bin_op : Set :=
    | PlusOp | MinusOp | ModOp | LeOp | LtOp | EqOp.

  Inductive binder := BAnon | BNamed : string → binder.
  Delimit Scope binder_scope with bind.
  Bind Scope binder_scope with binder.
  Definition cons_binder (mx : binder) (X : list string) : list string :=
    match mx with BAnon => X | BNamed x => x :: X end.
  Infix ":b:" := cons_binder (at level 60, right associativity).
  Instance binder_dec_eq (x1 x2 : binder) : Decision (x1 = x2).
  Proof.
    (* infinite loop yeah yeah yeah *)
    (* solve_decision. *)
    destruct x1,x2; try (by left); try by right.
    case: (decide (s = s0)) => [->|]; [by left|by right => [[]]].
  Defined.

  Instance set_unfold_cons_binder x mx X P :
    SetUnfold (x ∈ X) P → SetUnfold (x ∈ mx :b: X) (BNamed x = mx ∨ P).
  Proof.
    constructor. rewrite -(set_unfold (x ∈ X) P).
    destruct mx; rewrite /= ?elem_of_cons; abstract naive_solver.
  Qed.

  Inductive expr :=
    (* Base lambda calculus *)
    | Var (x : string)
    | Rec (f x : binder) (e : expr)
    | App (e1 e2 : expr)
    (* Base types and their operations *)
    | Lit (l : base_lit)
    | UnOp (op : un_op) (e : expr)
    | BinOp (op : bin_op) (e1 e2 : expr)
    | If (e0 e1 e2 : expr)
    (* Concurrency *)
    | Fork (e : expr)
    (* Memory *)
    | Alloc
    | Dealloc (e : expr)
    | Load (acc : access) (e : expr)
    | Store (acc : access) (e1 : expr) (e2 : expr)
    | CAS (e0 : expr) (e1 : expr) (e2 : expr)
    | FAI (C: positive) (e0 : expr).

  Bind Scope expr_scope with expr.
  Delimit Scope expr_scope with E.
  Arguments Rec _ _ _%E.
  Arguments App _%E _%E.
  Arguments Lit _.
  Arguments UnOp _ _%E.
  Arguments BinOp _ _%E _%E.
  Arguments If _%E _%E _%E.
  Arguments Fork _%E.
  Arguments Load _ _%E.
  Arguments Store _ _%E _%E.
  Arguments CAS _%E _%E _%E.
  Arguments FAI _ _%E.

  Fixpoint is_closed (X : list string) (e : expr) : bool :=
    match e with
    | Var x => bool_decide (x ∈ X)
    | Rec f x e => is_closed (f :b: x :b: X) e
    | Lit _ | Alloc => true
    | UnOp _ e | Fork e | Load _ e | FAI _ e | Dealloc e =>
       is_closed X e
    | App e1 e2 | BinOp _ e1 e2 | Store _ e1 e2 =>
       is_closed X e1 && is_closed X e2
    | If e0 e1 e2 | CAS e0 e1 e2 =>
       is_closed X e0 && is_closed X e1 && is_closed X e2
    end.

  Class Closed (X : list string) (e : expr) := closed : is_closed X e.
  Instance closed_proof_irrel env e : ProofIrrel (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.
  Instance closed_decision env e : Decision (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.

  Inductive val :=
    | RecV (f x : binder) (e : expr) `{!Closed (f :b: x :b: []) e}
    | LitV (l : base_lit).

  Bind Scope val_scope with val.
  Delimit Scope val_scope with V.

  Fixpoint of_val (v : val) : expr :=
    match v with
    | RecV f x e _ => Rec f x e
    | LitV l => Lit l
    end.

  Fixpoint to_val (e : expr) : option val :=
    match e with
    | Rec f x e =>
       if decide (Closed (f :b: x :b: []) e) then Some (RecV f x e) else None
    | Lit l => Some (LitV l)
    | _ => None
    end.

  (** Evaluation contexts *)
  Inductive ectx_item :=
    | AppLCtx (e2 : expr)
    | AppRCtx (v1 : val)
    | UnOpCtx (op : un_op)
    | BinOpLCtx (op : bin_op) (e2 : expr)
    | BinOpRCtx (op : bin_op) (v1 : val)
    | IfCtx (e1 e2 : expr)
    | DeallocCtx
    | LoadCtx (acc : access)
    | StoreLCtx (acc : access) (e2 : expr)
    | StoreRCtx (acc : access) (v1 : val)
    | CasLCtx (e1 : expr)  (e2 : expr)
    | CasMCtx (v0 : val) (e2 : expr)
    | CasRCtx (v0 : val) (v1 : val)
    | FaiCtx (C: positive).

  Definition fill_item (Ki : ectx_item) (e : expr) : expr :=
    match Ki with
    | AppLCtx e2 => App e e2
    | AppRCtx v1 => App (of_val v1) e
    | UnOpCtx op => UnOp op e
    | BinOpLCtx op e2 => BinOp op e e2
    | BinOpRCtx op v1 => BinOp op (of_val v1) e
    | IfCtx e1 e2 => If e e1 e2
    | DeallocCtx => Dealloc e
    | LoadCtx acc => Load acc e
    | StoreLCtx acc e2 => Store acc e e2
    | StoreRCtx acc v1 => Store acc (of_val v1) e
    | CasLCtx e1 e2 => CAS e e1 e2
    | CasMCtx v0 e2 => CAS (of_val v0) e e2
    | CasRCtx v0 v1 => CAS (of_val v0) (of_val v1) e
    | FaiCtx C => FAI C e
    end.

  (** Substitution *)
  Fixpoint subst (x : string) (es : expr) (e : expr)  : expr :=
    match e with
    | Var y => if decide (x = y) then es else Var y
    | Rec f y e =>
       Rec f y $ if decide (BNamed x ≠ f ∧ BNamed x ≠ y) then subst x es e else e
    | App e1 e2 => App (subst x es e1) (subst x es e2)
    | Lit l => Lit l
    | UnOp op e => UnOp op (subst x es e)
    | BinOp op e1 e2 => BinOp op (subst x es e1) (subst x es e2)
    | If e0 e1 e2 => If (subst x es e0) (subst x es e1) (subst x es e2)
    | Fork e => Fork (subst x es e)
    | Alloc => Alloc
    | Dealloc e => Dealloc (subst x es e)
    | Load acc e => Load acc (subst x es e)
    | Store acc e1 e2 => Store acc (subst x es e1) (subst x es e2)
    | CAS e0 e1 e2 => CAS (subst x es e0) (subst x es e1) (subst x es e2)
    | FAI C e => FAI C (subst x es e)
    end.

  Definition subst' (mx : binder) (es : expr) : expr → expr :=
    match mx with BNamed x => subst x es | BAnon => id end.

  (** The stepping relation *)
  Definition un_op_eval (op : un_op) (l : base_lit) : option base_lit :=
    match op, l with
    | MinusUnOp, LitInt n => Some (LitInt (- n))
    | CastIntOp, LitLoc n => Some (LitInt (Z.pos n))
    | CastLocOp, LitInt n => Some (LitLoc (Z.to_pos n))
    | _, _ => None
    end.

  Definition ZplusPos (n: Z) (p: positive) : positive :=
    Z.to_pos (n + Zpos p).

  Definition bin_op_eval (op : bin_op) (l1 l2 : base_lit) : option base_lit :=
    match op, l1, l2 with
    | PlusOp, (LitInt n1), (LitInt n2) => Some $ LitInt (n1 + n2)
    | PlusOp, (LitInt n), (LitLoc l) => Some $ LitLoc $ ZplusPos n l
    | PlusOp, (LitLoc l), (LitInt n) => Some $ LitLoc $ ZplusPos n l
    | MinusOp, (LitInt n1), (LitInt n2) => Some $ LitInt (n1 - n2)
    | ModOp,  (LitInt n1), (LitInt n2) => Some $ LitInt (n1 `mod` n2)
    | LeOp, (LitInt n1), (LitInt n2) => Some $ LitInt $ bool_decide (n1 ≤ n2)
    | LtOp, (LitInt n1), (LitInt n2) => Some $ LitInt $ bool_decide (n1 < n2)
    | EqOp, (LitInt n1), (LitInt n2) => Some $ LitInt $ bool_decide (n1 = n2)
    | EqOp, (LitLoc n1), (LitLoc n2) => Some $ LitInt $ bool_decide (n1 = n2)
    | _, _, _ => None
    end.

  Inductive head_step : expr → expr → Prop :=
    | BetaS f x e1 e2 v2 e' :
       to_val e2 = Some v2 →
       Closed (f :b: x :b: []) e1 →
       e' = subst' x (of_val v2) (subst' f (Rec f x e1) e1) →
       head_step (App (Rec f x e1) e2) e'
    | UnOpS op l l' :
       un_op_eval op l = Some l' →
       head_step (UnOp op (Lit l)) (Lit l')
    | BinOpS op l1 l2 l' :
       bin_op_eval op l1 l2 = Some l' →
       head_step (BinOp op (Lit l1) (Lit l2)) (Lit l')
    | IfTrueS e1 e2 :
       head_step (If (Lit $ LitInt true) e1 e2) e1
    | IfFalseS e1 e2 :
       head_step (If (Lit $ LitInt false) e1 e2) e2.

  (** Basic properties about the language *)
  Lemma to_of_val v : to_val (of_val v) = Some v.
  Proof.
    by induction v; simplify_option_eq; repeat f_equal; try apply (proof_irrel _).
  Qed.

  Lemma of_to_val e v : to_val e = Some v → of_val v = e.
  Proof.
    revert v; induction e; intros v ?; simplify_option_eq; auto with f_equal.
  Qed.

  Instance of_val_inj : Inj (=) (=) of_val.
  Proof. by intros ?? Hv; apply (inj Some); rewrite -!to_of_val Hv. Qed.

  Instance fill_item_inj Ki : Inj (=) (=) (fill_item Ki).
  Proof. destruct Ki; intros ???; simplify_eq/=; auto with f_equal. Qed.

  Lemma fill_item_val Ki e :
    is_Some (to_val (fill_item Ki e)) → is_Some (to_val e).
  Proof. intros [v ?]. destruct Ki; simplify_option_eq; eauto. Qed.

  Lemma val_stuck e1 e2 : head_step e1 e2 → to_val e1 = None.
  Proof. destruct 1; abstract naive_solver. Qed.

  Lemma head_ctx_step_val Ki e e2 :
    head_step (fill_item Ki e) e2 → is_Some (to_val e).
  Proof. destruct Ki; inversion_clear 1; simplify_option_eq; by eauto. Qed.

  Lemma fill_item_no_val_inj Ki1 Ki2 e1 e2 :
    to_val e1 = None → to_val e2 = None →
    fill_item Ki1 e1 = fill_item Ki2 e2 → Ki1 = Ki2.
  Proof.
    destruct Ki1, Ki2; intros; try discriminate; simplify_eq/=;
      repeat match goal with
      | H : to_val (of_val _) = None |- _ => by rewrite to_of_val in H
      end; auto.
  Qed.

  (** Closed expressions *)
  Lemma is_closed_weaken X Y e : is_closed X e → X ⊆ Y → is_closed Y e.
  Proof. revert X Y; induction e; abstract naive_solver (eauto; set_solver). Qed.

  Lemma is_closed_weaken_nil X e : is_closed [] e → is_closed X e.
  Proof. intros. by apply is_closed_weaken with [], list_subseteq_nil. Qed.

  Lemma is_closed_subst X e x es : is_closed X e → x ∉ X → subst x es e = e.
  Proof.
    revert X. induction e=> X /=; rewrite ?bool_decide_spec ?andb_True=> ??;
      repeat case_decide; simplify_eq/=; f_equal; intuition eauto with set_solver.
  Qed.

  Lemma is_closed_nil_subst e x es : is_closed [] e → subst x es e = e.
  Proof. intros. apply is_closed_subst with []; set_solver. Qed.

  Lemma is_closed_of_val X v : is_closed X (of_val v).
  Proof. apply is_closed_weaken_nil. induction v; simpl; auto. Qed.

  (** Equality and other typeclass stuff *)
  Instance base_lit0_dec_eq (l1 l2 : base_lit) : Decision (l1 = l2).
  Proof. solve_decision. Defined.
  Instance base_lit_dec_eq (l1 l2 : base_lit) : Decision (l1 = l2).
  Proof. 
    destruct l1, l2; try (by right); try (by left);
    eauto with typeclass_instances.
  Defined.
  Instance un_op_dec_eq (op1 op2 : un_op) : Decision (op1 = op2).
  Proof. solve_decision. Defined.
  Instance bin_op_dec_eq (op1 op2 : bin_op) : Decision (op1 = op2).
  Proof. solve_decision. Defined.
  Instance expr_dec_eq (e1 e2 : expr) : Decision (e1 = e2).
  Proof. solve_decision. Defined.
  Instance val_dec_eq (v1 v2 : val) : Decision (v1 = v2).
  Proof.
   refine (cast_if (decide (of_val v1 = of_val v2))); abstract naive_solver.
  Defined.

  Instance expr_inhabited : Inhabited (expr) := populate (Lit LitUnit).
  Instance val_inhabited : Inhabited val := populate (LitV LitUnit).

  Canonical Structure stateC := leibnizC state.
  Canonical Structure valC := leibnizC val.
  Canonical Structure exprC := leibnizC expr.
End base.

Module ra_lang.
  Definition expr : Type := base.expr * View.
  Definition val : Type := base.val * View.
  Definition ectx_item := base.ectx_item.
  Definition fill_item : _ -> _ -> expr := λ Ki e, (base.fill_item Ki (e.1), e.2).
  Definition of_val : val -> expr := λ v, (base.of_val (v.1), v.2).
  Definition to_val : expr -> option val := λ e,  (λ v, (v, (e.2))) <$> base.to_val (e.1).


  Definition is_closed (X : list string) (e : expr) : bool :=
    base.is_closed X (e.1).

  Class Closed (X : list string) (e : expr) := closed : is_closed X e.
  Instance closed_proof_irrel env e : ProofIrrel (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.
  Instance closed_decision env e : Decision (Closed env e).
  Proof. rewrite /Closed. apply _. Qed.

  Definition subst x es (e : expr) : expr := (base.subst x es (e.1), e.2).

  (* Instance lit_dec_eq : ∀ l1 l2 : base.base_lit, Decision (l1 = l2) := _. *)

  (* Definition state := @machine.state base.loc _ _ Z _ _. *)
  (* Definition event := @machine.event base.loc Z. *)

  Inductive step (V : View)
  : ∀ (e : base.expr) (evt : event) (e' : base.expr) (ef : list expr), Prop :=
  | ForkS e V' : (* π ≠ ρ ? *)
      step V
           (base.Fork e)
           (EFork V')
           (base.Lit base.LitUnit)
           [(e, V')]
   | AllocS l:
       step V
            base.Alloc
            (EWrite na l A)
            (base.Lit $ base.LitLoc l)
            []
  | DeallocS l :
      step V
           (base.Dealloc (base.Lit $ base.LitLoc l))
           (EWrite na l D)
           (base.Lit $ base.LitUnit)
           []
  | LoadS (l : loc) (v : Z) acc :
      step V
           (base.Load acc (base.Lit $ base.LitLoc l))
           (ERead acc l (VInj v))
           (base.of_val (base.LitV $ base.LitInt v))
           []
  | StoreS l e v acc :
      base.to_val e = Some (base.LitV $ base.LitInt v) -> 
      step V
           (base.Store acc (base.Lit $ base.LitLoc l) e)
           (EWrite acc l (VInj v))
           (base.Lit $ base.LitUnit)
          []
  | CasFailS l e1 v1 e2 v2 vl :
      base.to_val e1 = Some (base.LitV $ base.LitInt v1) →
      base.to_val e2 = Some (base.LitV $ base.LitInt v2) →
      vl ≠ v1 →
      step V
           (base.CAS (base.Lit $ base.LitLoc l) e1 e2)
           (ERead at_hack l (VInj vl))
           (base.Lit $ base.LitInt false)
           []
  | CasSucS l e1 v1 e2 v2 :
      base.to_val e1 = Some (base.LitV $ base.LitInt v1) →
      base.to_val e2 = Some (base.LitV $ base.LitInt v2) →
      step V
           (base.CAS (base.Lit $ base.LitLoc l) e1 e2)
           (EUpdate l (VInj v1) (VInj v2))
           (base.Lit $ base.LitInt true)
           []
  | FAIS l C (v: Z):
      step V
           (base.FAI C (base.Lit $ base.LitLoc l))
           (EUpdate l (VInj v) (VInj ((v + 1) `mod` Z.pos C)))
           (base.Lit $ base.LitInt v)
           []
  .

  Inductive head_step :
  ∀ (e : expr) (σ : state) (e' : expr) (σ' : state) (ef : list expr), Prop :=
  | pure_step σ π e e' (BaseStep : base.head_step e e')
    : head_step (e, π) σ (e', π) σ []
  | impure_step σ σ' V V' e e' ef evt 
                (ExprStep : step V e evt e' ef) 
                (MachStep : machine_red σ evt V σ' V')
    : head_step (e, V) σ (e', V') σ' ef
  .

  Lemma to_of_val v : to_val (of_val v) = Some v.
  Proof. destruct v. cbv -[base.to_val base.of_val]. by rewrite base.to_of_val. Qed.

  Lemma of_to_val e v : to_val e = Some v → of_val v = e.
  Proof.
    destruct e, v. cbv -[base.to_val base.of_val]. case C : (base.to_val e) => //.
    move => [<- <-]. f_equal. exact: base.of_to_val. 
  Qed.

  Lemma val_stuck σ1 e1 σ2 e2 ef : head_step e1 σ1 e2 σ2 ef → to_val e1 = None.
  Proof.
    inversion 1; subst; last inversion ExprStep;
      first (cbv -[base.to_val]; by erewrite base.val_stuck; last eassumption);
      reflexivity.
  Qed.

  Lemma fill_item_val Ki e :
    is_Some (to_val (fill_item Ki e)) → is_Some (to_val e).
  Proof.
    move/fmap_is_Some/base.fill_item_val => H.
    exact/fmap_is_Some.
  Qed.

  Lemma fill_item_no_val_inj Ki1 Ki2 e1 e2 :
    to_val e1 = None → to_val e2 = None → fill_item Ki1 e1 = fill_item Ki2 e2 → Ki1 = Ki2.
  Proof.
    move => /fmap_None H1 /fmap_None H2 [] H3 ?.
    exact: base.fill_item_no_val_inj H1 H2 H3.
  Qed.

  Lemma head_ctx_step_val Ki e σ e2 σ2 ef : 
    head_step (fill_item Ki e) σ e2 σ2 ef → is_Some (to_val e).
  Proof.
    inversion 1; subst; last inversion ExprStep; subst; simplify_option_eq;
      first
        (apply/fmap_is_Some; exact: base.head_ctx_step_val);
      apply/fmap_is_Some;
      first by destruct Ki.
    - destruct Ki; try (by eauto);
        inversion H1; subst; by eauto.
    - destruct Ki; try (by eauto);
        inversion H1; subst; by eauto.
    - destruct Ki; try (by eauto);
        inversion H1; subst; by eauto.
    - destruct Ki; try (by eauto);
        inversion H0; subst; by eauto.
    - destruct Ki; try (by eauto);
        inversion H0; subst; by eauto.
    - destruct Ki; try (by eauto);
        inversion H0; subst; by eauto.
    - destruct Ki; try (by eauto).
        inversion H1; subst; by eauto.
  Qed.

  Instance of_val_inj : Inj (=) (=) of_val.
  Proof. by intros ?? Hv; apply (inj Some); rewrite -!to_of_val Hv. Qed.

  Instance fill_item_inj Ki : Inj (=) (=) (fill_item Ki).
  Proof. destruct Ki; move => [? ?] [? ?] [? ?]; simplify_eq/=; auto with f_equal. Qed.

  Lemma is_closed_weaken X Y e : is_closed X e → X ⊆  Y → is_closed Y e.
  Proof.
    destruct e. cbv -[base.is_closed].
    exact: base.is_closed_weaken.
  Qed.

  Lemma is_closed_weaken_nil X e : is_closed [] e → is_closed X e.
  Proof. intros. destruct e. exact: base.is_closed_weaken_nil. Qed.

  Lemma is_closed_subst X e x es : is_closed X e → x ∉ X → subst x es e = e.
  Proof. intros. destruct e. cbv -[base.subst]. f_equal. exact: base.is_closed_subst. Qed.

  Lemma is_closed_nil_subst e x es : is_closed [] e → subst x es e = e.
  Proof. intros. destruct e. cbv -[base.subst]. f_equal. exact: base.is_closed_nil_subst. Qed.

  Lemma is_closed_of_val X v : is_closed X (of_val v).
  Proof. exact: base.is_closed_of_val. Qed.

End ra_lang.

(** Language *)
Program Instance ra_ectxi_lang :
  EctxiLanguage
    (ra_lang.expr) ra_lang.val ra_lang.ectx_item state := {|
  of_val := ra_lang.of_val; to_val := ra_lang.to_val;
  fill_item := ra_lang.fill_item; head_step := ra_lang.head_step
|}.
Solve Obligations with eauto using ra_lang.to_of_val, ra_lang.of_to_val,
  ra_lang.val_stuck, ra_lang.fill_item_val, ra_lang.fill_item_no_val_inj,
  ra_lang.head_ctx_step_val.

Canonical Structure ra_lang := ectx_lang (ra_lang.expr).

Export base.
Export ra_lang.