From iris.program_logic Require Export weakestpre.
From iris.base_logic Require Export lib.invariants.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import gmap.

From igps Require Import weakestpre proofmode.
From igps Require Export viewpred wp_tactics.
From igps Require Import notation bigop na.
From igps.base Require Import ghosts alloc dealloc.

Definition mdealloc : base.val :=
  (λ: "l" "n",
    (rec: "mdel" "i" :=
      if: "n" ≤ "i"
      then #()
      else
        Dealloc ("l" + "i") ;;
        "mdel" ("i" + #1%Z)) #0)%V.

Definition malloc : base.val :=
  (λ: "n",
  (rec: "malloc" <> :=
    let: "l" := Alloc in
      (rec: "mall" "i" :=
        if: "i" = "n"
        then "l"
        else
          let: "li" := Alloc in
            if: "li" = "l" + "i"
            then "mall" ("i" + #1)
            else mdealloc "l" "i" ;; Dealloc "li" ;; "malloc" #()) #1) #())%V.

Global Opaque mdealloc malloc.

Section proof.
  Context `{fG : !foundationG Σ}.
  Set Default Proof Using "Type*".

  Local Open Scope I.

  Lemma alloc (E : coPset) :
    ↑physN ⊆ E →
    {{{{ True }}}}
      (Alloc) @ E 
    {{{{ (l: loc), RET (#l); (l ↦ A) }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS _ Post".
    iApply wp_mask_mono; last (iApply (f_alloc with "[$kI $kS]")); first auto.
    iNext. iIntros (l V' h' i) "(% & kS & oH & oI & % & % & %)". subst.
    destruct H3; subst.
    iApply ("Post" $! _ with "[%] kS [oH oI]"); [done|].
    iExists _, _. by iFrame "oH oI".
  Qed.

  Lemma dealloc l :
    {{{{ l ↦ ? }}}}
      (Dealloc #l) @ ↑physN
    {{{{ RET (#()); True }}}}%C.
  Proof.
    intros. iViewUp. iIntros "#kI kS oL Post".
    iDestruct "oL" as (h i) "(Alloc & oH & oI)".
    iApply (f_dealloc with "[$kI $kS $oH $oI $Alloc]").
    iNext. iIntros (?) "(#? & ?)".
    by iApply ("Post" with "[#] [-]").
  Qed.

  Lemma mdealloc_spec (l: loc) (n : Z) :
    {{{{ ▷ ([∗ list] i ∈ seq 0 (Z.to_nat n), ZplusPos i l ↦ ?) }}}}
      (mdealloc #l #n) @ ↑physN
    {{{{ RET (#()); True }}}}.
  Proof.
    intros. iViewUp. iIntros "#kI kS Big Post".
    wp_bind (mdealloc (#l)%E)%E.
    wp_lam.
    iNext.
    wp_value.
    wp_lam. iNext.
    move Eqmax : {-}n => max.
    rewrite [in X in _ ⊢ X]Eqmax.
    have Lemaxn: (n ≤ max)%Z by omega.
    move Eqz: {-}(0) => z.
    rewrite [in X in WP (_ X,_) @ _ {{ _ }}]Eqz.
    have Eqzn : (z + n = max)%Z by omega.
    have Le0z : 0 ≤ z by omega.
    rewrite (_ : 0%nat = Z.to_nat z); last by rewrite -Eqz.
    clear Eqz Eqmax.
    (iLöb as "IH" forall (V_l n z Lemaxn Le0z Eqzn)).
    wp_rec.
    iNext. wp_op => [Lezmax|?].
    - iApply wp_if_true. do 2!iNext. wp_value.
      by iApply ("Post" $! _ with "[%] kS").
    - iApply wp_if_false. do 2!iNext.
      wp_bind (Dealloc _). wp_op. iNext.
      have ?: (1 ≤ n) by omega.
      rewrite (_ : Z.to_nat n = S (Z.to_nat (n-1))); last first.
      { rewrite -Z2Nat.inj_succ; last omega. f_equal; omega. }
      rewrite [seq _ _]/=. 
      iDestruct "Big" as "[B0 Big]". rewrite vPred_big_opL_fold.
      rewrite Z2Nat.id; last omega.
      iApply (dealloc with "[%] kI kS B0"); [reflexivity|].
      iNext. iViewUp; iIntros "kS' _".
      wp_lam. iNext.
      rewrite -Z2Nat.inj_succ; last omega.
      wp_op. iNext.
      iApply ("IH" $! _ (n-1) (z+1) with "[%] [%] [%] kS' Big [$Post]");
        omega.
  Qed.

  Lemma malloc_spec (n : Z) (Le1n : 1 ≤ n):
    {{{{ True }}}}
      (malloc #n) @ ↑physN
    {{{{ (l : loc), RET #l;
        [∗ list] i ∈ seq 0 (Z.to_nat n), ZplusPos i l ↦ ? }}}}.
  Proof.
    iIntros (V π Φ). iViewUp. iIntros "#kI kS _ Post".
    wp_lam. iNext.
    set malloc'' := {-}(rec: "malloc" <> := _)%E.
    set malloc' := ltac:(let e := eval unfold malloc'' in malloc'' in reshape_val e ltac:(fun x => exact x)).
    let e := eval unfold malloc' in malloc' in
        match e with
        | context f [n] =>
          set malloc := (fun (n : Z) => ltac:(let x := context f[n] in exact x));
          unfold malloc' in malloc;
          clear malloc'' malloc'
        end.
    rewrite (eq_refl : (rec: "malloc" <> := _)%E = base.of_val (malloc n)).
    rewrite (lock (malloc n)).
    (iLöb as "IHmalloc" forall (V n Le1n)).
    rewrite -lock.
    wp_rec.
    rewrite (eq_refl : (rec: "malloc" <> := _)%E = base.of_val (malloc n)) (lock (malloc n)).
    iNext.
    wp_bind (Alloc)%E.
    iApply (alloc with "[] kI kS []"); [done|done|done|].
    iNext.
    iViewUp.
    iIntros (l) "kS oL".
    wp_lam. iNext.
    rewrite -/(malloc _).
    move Eqz: {-}1%Z => z.
    rewrite [in X in _ ⊢ WP (_ X, _) @ _ {{ _ }}]Eqz.
    have Lez1: (1 ≤ z)%Z by omega.
    move Eqmax : {-}(n) => max.
    rewrite [in X in seq _ X]Eqmax.
    have Eqzn: (z + (n-1) = max)%Z by omega.
    rewrite [in X in wp _ X _]Eqmax.

    iAssert (([∗ list] i ∈ seq 0 (Z.to_nat z), ZplusPos i l ↦ ?)%VP V)
      with "[oL]" as "Big".
    { rewrite -Eqz /= -vPred_big_opL_fold big_op.big_sepL_singleton.
      iFrame "oL". }

    clear Eqz Eqmax.

    (iLöb as "IHmall" forall (V z n Lez1 Le1n Eqzn) "kS").
    wp_rec. simpl_subst.
    iNext. wp_op => [EQmax|NEQmax]; iNext.
    - iClear "IHmalloc IHmall". subst max.
      iApply wp_if_true. iNext.
      repeat (iApply wp_value; first reflexivity).
      iApply ("Post" with "[] kS"); [done|by rewrite -EQmax].
    - iApply wp_if_false. iNext. wp_bind Alloc.
      iApply (alloc with "[%] kI kS []"); [done|done|done|].
      iNext. iViewUp. iIntros (l') "kS oL".

      wp_lam. iNext. wp_op. iNext. wp_bind (_ = _)%E.
      case: (decide (l' = ZplusPos z l)) => [EQl'|?].
      + subst. iApply wp_bin_op; first auto. iNext.
        rewrite bool_decide_true; last by auto. iApply wp_if_true. iNext.
        wp_op. iNext.

        iApply ("IHmall" $! _ (z+1) (n-1) with "[%] [%] [%] [$Post] [Big oL] kS");
          [omega|omega|omega|..].

        rewrite Z2Nat.inj_succ; last omega.
        rewrite seq_S -(vPred_big_opL_fold (_ ++ _)%list)
                big_op.big_sepL_app big_op.big_sepL_singleton.
        iFrame "Big".
        rewrite /= Z2Nat.id; last omega. by iFrame "oL".

      + subst. iApply wp_bin_op; first auto. iNext.
        rewrite bool_decide_false; last by auto. iApply wp_if_false. iNext.
        wp_bind ((mdealloc _) _)%E.
        iApply (mdealloc_spec with "[%] kI kS [$Big]"); [done|].
        iNext. iViewUp. iIntros "kS _".
        wp_lam. iNext. wp_bind (Dealloc _)%E.
        iApply (dealloc with "[%] kI kS [oL]"); [done|iFrame|].
        iNext. iViewUp. iIntros "kS _".
        wp_lam. iNext. iApply ("IHmalloc" with "[%] kS [$Post]"). omega.
  Qed.

End proof.