From iris.algebra Require Import sts.
From iris.base_logic.lib Require Import own.
From stdpp Require Export gmap.

From igps Require Import types infrastructure.
From igps.base Require Export ghosts.

Implicit Types (I : gset gname) (h: History) (i: gname).

Inductive state := 
    aUninit I h
  | aInit I1 I2 h (Sub: I2 ⊆ I1)
  | cUninit I h
  | cInit I h.

Inductive token := Change i.

Global Instance stateT_inhabited: Inhabited state := populate (aUninit ∅ ∅).

Global Instance Change_inj : Inj (=) (=) Change.
Proof. by injection 1. Qed.


Inductive prim_step : relation state :=
  | ChangeU I2 I1 h: prim_step (aUninit I1 h) (aUninit I2 h)
  | ChangeS I V v h h' (HUpd: h' ≡ {[(VInj v, V)]} ∪ h) (Sub: I ⊆ I)
      : prim_step (aUninit I h) (aInit I I h' Sub)
  | ChangeI I1 I2 I1' I2' h (Sub: I2 ⊆ I1) (Sub': I2' ⊆ I1')
      : prim_step (aInit I1 I2 h Sub) (aInit I1' I2' h Sub')
  | ChangeIW I1 I2 V v h h' (HUpd: h' ≡ {[(VInj v, V)]} ∪ h) (Sub: I2 ⊆ I1)
      : prim_step (aInit I1 I2 h Sub) (aInit I1 I2 h' Sub)
  | ChangeUc I2 I1 h: prim_step (cUninit I1 h) (cUninit I2 h)
  | ChangeSc I V v h h' (HUpd: h'≡ {[(VInj v, V)]} ∪ h)
      : prim_step (cUninit I h) (cInit I h')
  | ChangeIc I I' h: prim_step (cInit I h) (cInit I' h)
  | ChangeIWc I V v h h' (HUpd: h' ≡ {[(VInj v, V)]} ∪ h)
      : prim_step (cInit I h) (cInit I h').

Definition rhist (s: state) :=
  match s with
  | aUninit _ h => h
  | aInit _ _ h _ => h
  | cUninit _ h => h
  | cInit _ h => h
  end.

Definition rISet (s: state) :=
  match s with
  | aUninit I0 _ => I0
  | aInit I1 _ _ _  => I1
  | cUninit I0 _ => I0
  | cInit I0 _ => I0
  end.

Definition rISet2 (s: state) :=
  match s with
  | aUninit I0 _ => I0
  | aInit _ I2 _ _ => I2
  | cUninit I0 _ => I0
  | cInit I0 _ => I0
  end.


Lemma rISet2_subseteq s:
  rISet2 s ⊆ rISet s.
Proof. by destruct s. Qed.

Definition tok s: set token 
  := {[ t | ∃ i, t = Change i ∧ i ∉ rISet2 s ]}.

Global Arguments tok !_ /.

Canonical Structure rsl_sts := sts.STS prim_step tok.


Definition i_states i : set state := {[ s | i ∈ rISet2 s ]}.


Lemma i_states_closed i : sts.closed (i_states i) {[ Change i ]}.
Proof.
  split; first (intros [| | |] ; set_solver+).
  intros s1 s2 Hs1 [T1 T2 Hdisj Hstep'].
  inversion_clear Hstep' as [? ? ? ? Htrans _ _ Htok].
  destruct Htrans; done || (abstract set_solver).
Qed.

Lemma i_states_in s i: i ∈ rISet2 s → s ∈ i_states i.
Proof. done. Qed.

Lemma frame_step_step s s' T: sts.frame_step T s s' -> prim_step s s'.
Proof.
  inversion 1. by inversion H1.
Qed.

Lemma frame_steps_steps s s' T : sts.frame_steps T s s' -> rtc prim_step s s'.
Proof.
  induction 1 => //.
  econstructor; last by eauto.
  by apply: frame_step_step.
Qed.

Lemma frame_steps_RMW_branch s s' T: 
  sts.frame_steps T s s' →
  ∀ I0 h, (s = cUninit I0 h ∨ s = cInit I0 h)
    → ∃ I' h', s' = cUninit I' h' ∨ s' = cInit I' h'.
Proof.
  induction 1.
  - naive_solver.
  - move => I0 h Hs. inversion H. inversion H2. subst.
    destruct Hs; subst; inversion H7; subst.
    + apply (IHrtc I2 h). by left.
    + apply (IHrtc I0 h'). by right.
    + apply (IHrtc I' h). by right.
    + apply (IHrtc I0 h'). by right.
Qed.

Lemma frame_steps_Init_branch s s' T: 
  sts.frame_steps T s s' →
   (∃ h, ((∃ I1 I2 (Sub: I2 ⊆ I1), s = aInit I1 I2 h Sub) ∨ (∃ I1, s = cInit I1 h)))
    → ∃ h', 
      (∃ I1' I2' (Sub' : I2' ⊆ I1'), (s' = aInit I1' I2' h' Sub'))
         ∨ (∃ I1', s' = cInit I1' h').
Proof.
  induction 1 => //.
  move => [h Hyp]. apply IHrtc. clear IHrtc.
  destruct Hyp as [[I1 [I2 [Sub Eq]]]|[I1 Eq]]; rewrite Eq in H;
    inversion H; inversion H2; inversion H7; subst; eexists;
      [left|left|right|right]; naive_solver.
Qed.

Lemma frame_steps_RMW_Init_branch s s1 s2 T1 T2:
  sts.frame_steps T1 s1 s →
  sts.frame_steps T2 s2 s →
  (∃ I1 h1, s1 = cUninit I1 h1 ∨ s1 = cInit I1 h1) →
  (∃ h2, ((∃ I1 I2 (Sub: I2 ⊆ I1), s2 = aInit I1 I2 h2 Sub)
                                    ∨ (∃ I1, s2 = cInit I1 h2))) →
  ∃ I' h', s = cInit I' h'.
Proof.
  move => Step1 Step2 Eq1 Eq2.
  destruct Eq1 as [I1 [h1 Eq]].
  destruct (frame_steps_RMW_branch _ _ _ Step1 _ _ Eq) as [I'1 [h'1 H1]].
  destruct (frame_steps_Init_branch _ _ _ Step2 Eq2) 
    as [h'2 [[I1' [I2' [Sub' H2]]]|[I1' H2]]].
  - rewrite H2 in H1. destruct H1 as [H1|H1]; by inversion H1.
  - by do 2 eexists.
Qed.

Lemma steps_hist_subseteq s1 s2:
  rtc prim_step s1 s2 -> rhist s1 ⊆ rhist s2.
Proof.
  induction 1 => //.
  etrans; last exact IHrtc.
  inversion H => /= //; rewrite HUpd; apply union_subseteq_r.
Qed.

Lemma alloc_local_mono_rsl_state s s' V
  (Safe: alloc_local (rhist s) V)
  (Steps: rtc prim_step s s'):
  alloc_local (rhist s') V.
Proof.
  apply steps_hist_subseteq in Steps.
  destruct Safe as [? [? [? ?]]].
  do 2 eexists. split => //. by apply Steps.
Qed.

Lemma init_local_mono_rsl_state s s' V
  (Init: init_local (rhist s) V)
  (Steps: rtc prim_step s s'):
  init_local (rhist s') V.
Proof.
  apply steps_hist_subseteq in Steps.
  destruct Init as [? [? [? ?]]].
  do 2 eexists. split => //. by apply Steps.
Qed.

(** Read **)

Lemma tok_disj_state_singleton s i:
  i ∈ rISet2 s → sts.tok s ⊥ {[Change i]}.
Proof. intros. set_solver. Qed.

(** State Updates **)

(* ISet update *)

(* Join *)
Definition ISet_join i1 i2 i I0 :=
  {[i]} ∪ I0 ∖ {[i1; i2]}.

(* Split *)
Definition ISet_split i1 i2 i I0 :=
  {[i1; i2]} ∪ I0 ∖ {[i]}.

(* Switch *)
Definition ISet_switch i i' I0 :=
  {[i']} ∪ I0 ∖ {[i]}.


Lemma ISet_join_subseteq i1 i2 i I1 I2 :
  I2 ⊆ I1 → ISet_join i1 i2 i I2 ⊆ ISet_join i1 i2 i I1.
Proof.
  move => ?. rewrite /ISet_join.
  apply union_mono_l. abstract set_solver.
Qed.

Lemma ISet_split_subseteq i1 i2 i I1 I2 :
  I2 ⊆ I1 → ISet_split i1 i2 i I2 ⊆ ISet_split i1 i2 i I1.
Proof.
  move => ?. rewrite /ISet_split.
  apply union_mono_l. abstract set_solver.
Qed.

Lemma ISet_switch_subseteq i i' I1 I2 :
  I2 ⊆ I1 → ISet_switch i i' I2 ⊆ ISet_switch i i' I1.
Proof.
  move => ?. rewrite /ISet_switch.
  by apply union_mono_l, gset_difference_mono.
Qed.


(* State Updates *)
Definition state_ISet_join i1 i2 i s :=
  match s with
  | aUninit I0 h => aUninit (ISet_join i1 i2 i I0) h
  | aInit I1 I2 h Sub => aInit (ISet_join i1 i2 i I1)
                                (ISet_join i1 i2 i I2) h 
                                (ISet_join_subseteq i1 i2 i I1 I2 Sub)
  | cUninit I0 h => cUninit (ISet_join i1 i2 i I0) h
  | cInit I0 h => cInit (ISet_join i1 i2 i I0) h
  end.


Definition state_ISet_split i1 i2 i s :=
  match s with
  | aUninit I0 h => aUninit (ISet_split i1 i2 i I0) h
  | aInit I1 I2 h Sub => aInit (ISet_split i1 i2 i I1)
                                (ISet_split i1 i2 i I2) h 
                                (ISet_split_subseteq i1 i2 i I1 I2 Sub)
  | cUninit I0 h => cUninit (ISet_split i1 i2 i I0) h
  | cInit I0 h => cInit (ISet_split i1 i2 i I0) h
  end.


Definition state_ISet_switch i i' s :=
  match s with
  | aUninit I0 h => aUninit (ISet_switch i i' I0) h
  | aInit I1 I2 h Sub => aInit (ISet_switch i i' I1)
                                (ISet_switch i i' I2) h 
                                (ISet_switch_subseteq i i' I1 I2 Sub)
  | cUninit I0 h => cUninit (ISet_switch i i' I0) h
  | cInit I0 h => cInit (ISet_switch i i' I0) h
  end.

Definition state_ISet_insert i s :=
  match s with
  | aUninit I0 h => aUninit ({[i]} ∪ I0) h
  | aInit I1 I2 h Sub => aInit ({[i]} ∪ I1)
                               ({[i]} ∪ I2) h 
                               (union_mono_l {[i]} I2 I1 Sub)
  | cUninit I0 h => cUninit ({[i]} ∪ I0) h
  | cInit I0 h => cInit ({[i]} ∪ I0) h
  end.


Definition state_hist_update h' s :=
  match s with
  | aUninit I0 h => aInit I0 I0 h' (reflexivity I0)
  | aInit I1 I2 h Sub => aInit I1 I2 h' Sub
  | cUninit I0 h => cInit I0 h'
  | cInit I0 h => cInit I0 h'
  end.

Lemma state_hist_update_steps h' v V s:
  h' ≡  {[(VInj v, V)]} ∪ rhist s→
  sts.steps (s, ∅) (state_hist_update h' s, ∅).
Proof.
  move => Sub.
  apply rtc_once.
  constructor.
  - destruct s; simpl in *.
    + by eapply ChangeS.
    + by eapply ChangeIW.
    + by eapply ChangeSc.
    + by eapply ChangeIWc.
  - set_solver+.
  - set_solver+.
  - by destruct s.
Qed.

Lemma state_hist_update_hist h' s:
  rhist (state_hist_update h' s) = h'.
Proof. by destruct s. Qed.

(** Join **)
Lemma state_ISet_join_tokens i1 i2 i I0:
  i ∉ I0 → i1 ∈ I0 → i2 ∈ I0 →
  {[ t | ∃ i0 : gname, t = Change i0 ∧ i0 ∉ I0 ]} ∪ {[Change i1; Change i2]}
  ≡ {[ t | ∃ i0 : gname, t = Change i0 ∧ i0 ∉ {[i]} ∪ I0 ∖ {[i1; i2]} ]} ∪ {[Change i]}.
Proof.
  intros. apply set_unfold_2. split.
  - move => [[i' [? ?]]|[?|?]].
    + case (decide (i' = i)) => [<-|?].
        by right.
        left. eexists. split => //. move => ?. naive_solver.
    + left. naive_solver.
    + left. naive_solver.
  - move => [[i' [? NE]]|?].
    + case (decide (i' = i1)) => [<-|?].
        right. by left.
        case (decide (i' = i2)) => [<-|?].
          right. by right.
          left. eexists. split => //.
          move => ?. apply NE. right. naive_solver.
    + left. eexists. naive_solver.
Qed.


Lemma state_ISet_join_steps i1 i2 i s:
  i ∉ rISet s → i1 ∈ rISet2 s → i2 ∈ rISet2 s →
  sts.steps (s, {[Change i1; Change i2]})
            (state_ISet_join i1 i2 i s, {[Change i]}).
Proof.
  move => In In1 In2.
  apply rtc_once.
  constructor.
  - destruct s; constructor.
  - move: In1 In2. destruct s; abstract set_solver+.
  - move: In. destruct s => /=; abstract set_solver+.
  - destruct s; simpl in *; rewrite /ISet_join /=;
      try apply state_ISet_join_tokens => //.
    move => ?. by apply In, Sub.
Qed.

Lemma state_ISet_join_alloc_local i1 i2 i s V:
  alloc_local (rhist s) V
  → alloc_local (rhist (state_ISet_join i1 i2 i s)) V.
Proof.
  move => [v' [V' [In [? ?]]]].
  exists v', V'. repeat split => //.
  move : In.
  by destruct s => /=.
Qed.

Lemma state_ISet_join_included i1 i2 i s:
  state_ISet_join i1 i2 i s ∈ i_states i.
Proof.
  apply i_states_in.
  destruct s; rewrite /= /ISet_join elem_of_union;
     left; by apply elem_of_singleton.
Qed.

(** Split **)


Lemma state_ISet_split_token_disj_1 i1 i2 i s:
  sts.tok (state_ISet_split i1 i2 i s) ⊥ {[Change i1]}.
Proof.
  destruct s; apply tok_disj_state_singleton; rewrite /= /ISet_split;
    apply elem_of_union; left; apply elem_of_union; left;
    by apply elem_of_singleton.
Qed.

Lemma state_ISet_split_token_disj_2 i1 i2 i s:
  sts.tok (state_ISet_split i1 i2 i s) ⊥ {[Change i2]}.
Proof.
  destruct s; apply tok_disj_state_singleton; rewrite /= /ISet_split;
    apply elem_of_union; left; apply elem_of_union; right;
    by apply elem_of_singleton.
Qed.

Lemma state_ISet_split_included_1 i1 i2 i s:
  state_ISet_split i1 i2 i s ∈ i_states i1.
Proof.
  apply i_states_in.
  destruct s; rewrite /= /ISet_split elem_of_union;
     left; apply elem_of_union; left; by apply elem_of_singleton.
Qed.

Lemma state_ISet_split_included_2 i1 i2 i s:
  state_ISet_split i1 i2 i s ∈ i_states i2.
Proof.
  apply i_states_in.
  destruct s; rewrite /= /ISet_split elem_of_union;
     left; apply elem_of_union; right; by apply elem_of_singleton.
Qed.

Lemma state_ISet_split_alloc_local i1 i2 i s V:
  alloc_local (rhist s) V
  → alloc_local (rhist (state_ISet_split i1 i2 i s)) V.
Proof.
  move => [v' [V' [In [? ?]]]].
  exists v', V'. repeat split => //.
  move : In.
  by destruct s => /=.
Qed.

Lemma state_ISet_split_tokens i1 i2 i I0:
  i ∈ I0 → i1 ∉ I0 → i2 ∉ I0 →
  {[ t | ∃ i0 : gname, t = Change i0 ∧ i0 ∉ I0 ]} ∪ {[Change i]}
  ≡ {[ t | ∃ i0 : gname, t = Change i0 ∧ i0 ∉ {[i1; i2]} ∪ I0 ∖ {[i]} ]}
    ∪ {[Change i1; Change i2]}.
Proof.
  intros. apply set_unfold_2. split.
  - move => [|?].
    + move => [i' [? ?]].
      case (decide (i' = i1)) => [<-|?].
        right. by left.
        case (decide (i' = i2)) => [<-|?].
          right. by right.
          left. eexists. split => //.
          move => [[|]|[? _]] //.
    + left. eexists. naive_solver.
  - move => [|[?|?]].
    + move => [i' [? ?]].
      case (decide (i' = i)) => [<-|?].
        by right.
        left. eexists. split => //. move => ?. naive_solver.
    + left. by eexists.
    + left. by eexists.
Qed.

Lemma state_ISet_split_steps i1 i2 i s:
  i ∈ rISet2 s → i1 ∉ rISet s → i2 ∉ rISet s →
  sts.steps (s, {[Change i]})
            (state_ISet_split i1 i2 i s, {[Change i1; Change i2]}).
Proof.
  move => In In1 In2.
  apply rtc_once.
  constructor.
  - destruct s; constructor.
  - move: In. destruct s; abstract set_solver+.
  - move: In1 In2. destruct s => /=; abstract set_solver+.
  - destruct s; simpl in *; rewrite /ISet_split /=;
      try apply state_ISet_split_tokens => //;
    move => ?; [apply In1|apply In2]; by apply Sub.
Qed.


(** Switch **)

Lemma state_ISet_switch_tokens i i' I1 I2
  (Sub : I2 ⊆ I1) (Ini : i ∈ I2) (NIn' : i' ∉ I1):
  ({[ t | (∃ i0, t = Change i0 ∧ i0 ∉ I2) ]} ∪ {[Change i]}
   ≡ {[ t | (∃ i0, t = Change i0 ∧ i0 ∉ {[i']} ∪ I2 ∖ {[i]}) ]} ∪ {[Change i']}).
Proof.
  apply set_unfold_2.
  move => ?. split; move => [[i1 [-> NE]]|->].
  - case (decide (i1 = i')) => [->|?]; first by right.
    left. exists i1. split => //. move => [? //|[? _]//].
  - case (decide (i = i')) => [->|?]; first by right.
    left. exists i. split => //. move => [? //|[_ ?]//].
  - case (decide (i1 = i)) => [->|]; first by right.
    left. exists i1. split => //.
    move => ?. apply NE. by right.
  - case (decide (i = i')) => [->|?]; first by right.
    left. exists i'. split => //. move => ?. by apply NIn', Sub.
Qed.

Lemma state_ISet_switch_steps i i' s:
  i ∈ rISet2 s → i' ∉ rISet s →
  sts.steps (s, {[Change i]}) (state_ISet_switch i i' s, {[Change i']}).
Proof.
  move => In1 NIn2. apply rtc_once.
  constructor.
  - destruct s; constructor.
  - destruct s; by apply tok_disj_state_singleton.
  - destruct s; apply tok_disj_state_singleton, elem_of_union;
      left; by apply elem_of_singleton.
  - move: In1 NIn2. destruct s; eapply state_ISet_switch_tokens => //.
Qed.

Lemma state_ISet_switch_included i i' s:
  state_ISet_switch i i' s ∈ i_states i'.
Proof.
  apply i_states_in.
  destruct s; rewrite /= elem_of_union;
     left; by apply elem_of_singleton.
Qed.


Lemma state_ISet_switch_alloc_local i i' s V:
  alloc_local (rhist s) V
  → alloc_local (rhist (state_ISet_switch i i' s)) V.
Proof.
  move => [v' [V' [In [? ?]]]].
  exists v', V'. repeat split => //.
  move : In.
  by destruct s => /=.
Qed.

(** Insert **)

Lemma state_ISet_insert_tokens i I' (NIn : i ∉ I'):
  ({[ t | ∃ i0, t = Change i0 ∧ i0 ∉ I' ]} ∪ ∅
  ≡ {[ t | ∃ i0, t = Change i0 ∧ i0 ∉ {[i]} ∪ I']} ∪ {[Change i]}).
Proof.
  apply set_unfold_2. split.
  - move => [[i' [-> NE]]|//].
    case (decide (i' = i)) => [->|?]; first by right.
    left. exists i'. split => //. move => [? //|? //].
  - move =>  [[i' [-> NE]]|?].
    + case (decide (i' = i)) => [|?].
      * right. apply NE. by left.
      * left. exists i'. split => //. move => ?. apply NE. by right.
    + left. by exists i.
Qed.

Lemma state_ISet_insert_steps i s (NI: i ∉ rISet s):
  sts.steps (s, ∅) (state_ISet_insert i s, {[Change i]}).
Proof.
  apply rtc_once.
  constructor.
  + destruct s; constructor.
  + destruct s; by apply disjoint_empty_r.
  + destruct s; apply tok_disj_state_singleton, elem_of_union;
      left; by apply elem_of_singleton.
  + destruct s; apply state_ISet_insert_tokens => //.
    set_solver.
Qed.


Lemma state_ISet_insert_included i s:
  state_ISet_insert i s ∈ i_states i.
Proof.
  apply i_states_in.
  destruct s; rewrite /= elem_of_union;
     left; by apply elem_of_singleton.
Qed.

Lemma state_ISet_insert_alloc_local i s V:
  alloc_local (rhist s) V
  → alloc_local (rhist (state_ISet_insert i s)) V.
Proof.
  move => [v' [V' [In [? ?]]]].
  exists v', V'. repeat split => //.
  move : In.
  by destruct s => /=.
Qed.
